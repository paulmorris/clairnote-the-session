\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bayswater"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/333#setting13117"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/333#setting13117" {"https://thesession.org/tunes/333#setting13117"}}}
	title = "Miller's Maggot, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major     g'4 ^"G"   a'8    b'4    a'8  \bar "|"   b'4    a'8 
     b'8 ^"Em"   g'8    e'8  \bar "|"   g'4 ^"G"   a'8      b'4 ^"G/F\#m"   a'8 
 \bar "|"   b'4 ^"Em"   e''8      e''4. ^"C" \bar "|"     g'4 ^"G"   a'8    b'4 
   a'8  \bar "|"   b'4    a'8      b'8 ^"Em"   g'8    e'8  \bar "|"   g''8 ^"C" 
  fis''8    e''8      d''8 ^"G"   b'8    a'8  \bar "|"   b'4 ^"Em"   e''8    
e''4.  }   \repeat volta 2 {   e''4 ^"C"   fis''8    g''4    e''8  \bar "|"   
d''4 ^"G"   b'8    b'8    a'8    b'8  \bar "|"   e''4 ^"C"   fis''8    g''4.  
\bar "|"   a''8 ^"D"   g''8    a''8      b''4. ^"Em" \bar "|"     e''4 ^"C"   
fis''8    g''4    e''8  \bar "|"   d''4 ^"G"   b'8    b'8    a'8    g'8  
\bar "|"   a'4 ^"Am"   b'8    c''8    b'8    a'8  \bar "|"   b'8 ^"Em"   e''8   
 e''8    e''8    d''8    b'8  }   b'8 ^"Em"   e''8    e''8      e''4 ^"A7"   r8 
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
