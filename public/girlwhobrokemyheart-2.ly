\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ger the Rigger"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/669#setting13709"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/669#setting13709" {"https://thesession.org/tunes/669#setting13709"}}}
	title = "Girl Who Broke My Heart, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \mixolydian   a'8    b'8    a'8    g'8    f'8    d'8  <<   c'8 
   d'8   >> f'8    \bar "|"   d'8    g'8  \grace {    a'8  }   g'8    f'8    
g'8    a'8    b'8    c''8    \bar "|"   d''8    e''8    f''8    d''8    c''8    
a'8    g'8    f'8    \bar "|"   d'8    f'8  \grace {    g'8  }   f'8    e'8    
f'8    a'8    d''8    c''8  \bar "|"   a'8    b'8    a'8    g'8    f'8    d'8  
<<   c'8    d'8   >> f'8    \bar "|"   d'8    g'8  \grace {    a'8  }   g'8    
f'8    g'8    a'8    b'8    c''8    \bar "|"   d''8    e''8    f''8    d''8    
c''8    a'8    g'8    f'8    \bar "|"   d'4    g'4  <<   g2    g'2   >>   
\bar "|"   a'8    b'8    a'8    g'8    f'8    d'8  <<   c'4    d'4   >>   
\bar "|"   d'8    g'4. ^"~"    g'8    a'8    b'8    c''8    \bar "|"   d''8    
e''8    f''8    d''8    c''8    a'8    g'8    f'8    \bar "|"   d'8    f'8  
\grace {    g'8  }   f'8    e'8    f'8    a'8    d''8    c''8  \bar "|"   
\tuplet 3/2 {   a'8    c''8    d''8  }   a'8    g'8    f'8    d'8  <<   c'8    
d'8   >> f'8    \bar "|"   d'8    g'8  \grace {    a'8  }   g'8    f'8    g'8   
 a'8    b'8    c''8    \bar "|"   d''8    e''8    f''8    d''8    c''8    a'8   
 g'8    f'8    \bar "|"   d'8    g'8  \grace {    a'8  }   g'8    f'8    g'8    
d'8    e'8    fis'8  \bar "|"   \repeat volta 2 {   g'4    b'8    d''8    g''8  
  d''8    b'8    g'8    \bar "|"   \tuplet 3/2 {   f'8    f'8    f'8  }   a'8    
c''8    f''8    c''8    a'8    f'8    \bar "|"   f'8    g'8    b'8    d''8    
g''8    d''8    b'8    d''8  \bar "|"   c''4 ^"~"    b'8    d''8    c''8    a'8 
   f'8    a'8    \bar "|"   g'4 ^"~"    b'8    d''8    g''8    d''8    b'8    
g'8    \bar "|"   e'8    f'8    a'8    c''8    f''8    c''8    a'8    f'8    
\bar "|"   g'8    a'8    b'8    d''8    c''8    a'8    g'8    f'8    
} \alternative{{   d'8    g'8  \grace {    a'8  }   g'8    f'8    g'8    d'8    
e'8    fis'8  } {   d'8    g'8  \grace {    a'8  }   g'8    f'8    g'8    a'8   
 d''8    c''8  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
