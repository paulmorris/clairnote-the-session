\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/830#setting21447"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/830#setting21447" {"https://thesession.org/tunes/830#setting21447"}}}
	title = "Brian O'Lynn"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {   b'8    \bar "|"   c''8    a'8    
d''8    c''8    a'8    g'8    \bar "|"   e'8    dis'8    e'8    g'8    a'8    
b'8    \bar "|"   c''8    a'8    d''8    c''4    g'8    \bar "|"   a'8    d''16 
   c''16    d''8    f''8    e''8    d''8    \bar "|"     c''8    a'8    d''8    
c''4    g'8    \bar "|"   e'8    g'8    e'8    g'8    a'8    b'8    \bar "|"   
c''8    d''8    e''8    f''8    e''8    d''8    \bar "|"   c''8    a'8    gis'8 
   a'4    }     \repeat volta 2 {   e''16    fis''16    \bar "|"   g''8    a''8 
   g''8    g''8    e''8    d''8    \bar "|"   c''8    a'8    b'8    c''4    
d''8    \bar "|"   e''8    a''8    a''8    a''8    g''8    e''8    \bar "|"   
e''8    d''8    cis''8    d''8    e''8    fis''8    \bar "|"     g''8    g''16  
  g''16    g''8    g''8    e''8    d''8    \bar "|"   c''16    b'16    a'8    
b'8    c''8    d''8    e''8    \bar "|"   f''8    e''8    d''8    c''8    a'8   
 g'8    \bar "|"   e'8    a'8    gis'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
