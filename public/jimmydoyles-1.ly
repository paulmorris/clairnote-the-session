\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "woman of the house"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/542#setting542"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/542#setting542" {"https://thesession.org/tunes/542#setting542"}}}
	title = "Jimmy Doyle's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   b'8    d''8    d''8    e''8  \bar "|"   d''8    b'8   
 b'8    a'16    b'16  \bar "|"   d''8    b'8    a'8    b'8  \bar "|"   d''8    
b'8    g'8.    a'16  \bar "|"   b'8    d''8    d''8    e''8  \bar "|"   d''8    
b'8    b'8    a'16    b'16  \bar "|"   d''8    g'8    b'8    a'8  \bar "|"   
a'8    g'8    g'8.    a'16  \bar ":|."   a'8    g'8    g'4  \bar "||"     
\bar ".|:"   d''8    g''8    g''8    a''16    g''16  \bar "|"   fis''8    e''8   
 e''8.    fis''16  \bar "|"   e''8    d''8    b'8    d''8  \bar "|"   e''8    
d''8    b'8    a'8  \bar "|"   d''8    g''8    g''8    a''16    g''16  \bar "|" 
  fis''8    e''8    e''16    fis''16    g''16    e''16  \bar "|"   d''8    g'8  
  b'8    a'8  \bar "|"   a'8    g'8    g'4  \bar ":|."   a'8    g'8    g'8.    
a'16  \bar "||"         \bar ".|:"   b'8 ^"Version 2:"   d''8    d''8    e''8  
\bar "|"   d''8    b'8    b'16    a'16    b'16    c''16  \bar "|"   d''8    b'8 
   a'8    b'8  \bar "|"   d''8    b'8    g'8.    a'16  \bar "|"   b'8    d''8   
 d''8    e''8  \bar "|"   d''8    b'8    b'16    a'16    b'16    c''16  
\bar "|"   d''8    g'8    b'8    a'8  \bar "|"   a'8    g'8    g'8.    a'16  
\bar ":|."   a'8    g'8    g'8    b'8  \bar "||"     \bar ".|:"   d''8    e''16  
  fis''16    g''8    g''8  \bar "|"   fis''8    e''8    e''8.    fis''16  
\bar "|"   e''8    d''8    b'8    d''8  \bar "|"   e''8    d''8    b'8    d''8  
\bar "|"   d''8    e''16    fis''16    g''8    g''8  \bar "|"   fis''8    e''8  
  e''8.    e''16  \bar "|"   d''8    g'8    b'8    a'8  \bar "|"   a'8    g'8   
 g'4  \bar ":|."   a'8    g'8    g'8.    a'16  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
