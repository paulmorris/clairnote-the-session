\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Bowser"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/727#setting22189"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/727#setting22189" {"https://thesession.org/tunes/727#setting22189"}}}
	title = "Brenda Stubbert's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'8    \repeat volta 2 {   b'4    b'4    a'8    b'8   
 cis''8    a'8    \bar "|"   b'4    a'8    e''8    fis''8    e''8    e''8    
cis''8    \bar "|"   a'4    cis''8    b'8    cis''8    a'8    a'8    cis''8    
\bar "|"   d''4    cis''8    b'8    cis''8    a'8    fis'8    a'8    \bar "|"   
  b'2    a'8    b'8    b'8    a'8    \bar "|"   b'4    cis''8    e''8    fis''8 
   e''8    e''4    \bar "|"   a''8    fis''8    e''8    cis''8    a'8    b'8    
cis''8    e''8    \bar "|"   fis''4    e''8    cis''8    fis''8    b'8    b'8   
 a'8  }     \tuplet 3/2 {   b'8    b'8    b'8  }   b''4    b'4    a''4    
\bar "|"   fis''8    b''8    a''8    fis''8    b''8    a''8    fis''8    a''8   
 \bar "|"   a'4    cis''8    b'8    cis''8    a'8    a'8    cis''8    \bar "|"  
 d''4    cis''8    b'8    cis''8    a'8    a'4    \bar "|"     b'4    b''4    
b'4    a''4    \bar "|"   fis''8    b''8    a''8    fis''8    b''8    a''8    
fis''8    b''8    \bar "|"   a''8    fis''8    e''8    cis''8    a'8    b'8    
cis''8    e''8    \bar "|"   fis''4    e''8    cis''8    fis''8    b'8    b'4   
 \bar "|"     b'4    b''4    b'4    a''4    \bar "|"   fis''8    b''8    a''8   
 fis''8    b''8    a''8    fis''8    a''8    \bar "|"   a'4    cis''8    b'8    
cis''8    a'8    a'8    cis''8    \bar "|"   d''4    cis''8    b'8    cis''8    
b'8    b'8    a'8    \bar "|"     b'4    cis''8    b'8    a'8    b'8    cis''8  
  a'8    \bar "|"   b'4    cis''8    e''8    fis''8    e''8    e''4    \bar "|" 
  a''8    fis''8    e''8    cis''8    a'8    b'8    cis''8    e''8    \bar "|"  
 fis''4    e''8    cis''8    fis''8    b'8    b'8    a'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
