\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1228#setting1228"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1228#setting1228" {"https://thesession.org/tunes/1228#setting1228"}}}
	title = "Banana Feet"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   d'8    a8    a4 ^"~"    fis'8    g'8    a'8    
fis'8  \bar "|"   g'4. ^"~"    a'8    g'8    e'8    c'8    e'8  \bar "|"   d'8  
  a8    a4 ^"~"    fis'8    g'8    a'8    d''8  \bar "|"   c''8    a'8    g'8   
 c''8    a'8    d'8    d'8    e'8  \bar ":|."   c''8    a'8    g'8    c''8    
a'8    d'8    d'8    d''8  \bar "||"     \bar ".|:"   c''8    a'8    g'8    a'8  
  fis'4. ^"~"    d''8  \bar "|"   c''8    a'8    g'8    c''8    a'8    d'8    
d'8    d''8  \bar "|"   c''8    d''8    b'8    g'8    a'8    g'8    a'8    b'8  
\bar "|"   c''4. ^"~"    a'8    g'8    c''8    c''4 ^"~"  \bar "|"     e'8    
c''8    e''8    c''8    d''4. ^"~"    e''8  \bar "|"   d''8    b'8    g'8    
b'8    a'8    cis''8    d''8    e''8  \bar "|"   fis''8    g''8    fis''8    
e''8    d''8    fis''8    fis''4 ^"~"  \bar "|"   d''8    c''!8    a'8    fis'8 
   g'8    e''8    d''8    e''8  \bar ":|."   \key d \major   \bar ".|:"   fis''8 
   d''8    d''4 ^"~"    a'8    d''8    fis''8    d''8  \bar "|"   e''4. ^"~"    
fis''8    g''8    e''8    cis''8    e''8  \bar "|"   d''4. ^"~"    e''8    d''8 
   cis''8    a'8    d''8  \bar "|"   c''8    a'8    g'8    e'8    e'8    d'8    
d'8    e''8  \bar "|"     fis''4. ^"~"    g''8    a''8    fis''8    d''8    
fis''8  \bar "|"   e''8    cis''!8    cis''4 ^"~"    a'8    cis''8    e''8    
cis''8  \bar "|"   d''8    g'8    b'8    d''8    g''8    b''8    a''8    g''8  
\bar "|"   \tuplet 3/2 {   fis''8    e''8    d''8  }   e''8    cis''8    d''8    
cis''8    d''8    e''8  \bar ":|."   \tuplet 3/2 {   fis''8    e''8    d''8  }   
e''8    cis''8    d''8    a'8    a'8    b'8  \bar "||"   \key d \mixolydian   
\bar ".|:"   c''4. ^"~"    e'8    fis'8    g'8    a'8    b'8  \bar "|"   c''8    
a'8    fis'8    c''8    b'8    g'8    b'8    d''8  \bar "|"   c''8    e'8    
e'4 ^"~"    fis'8    g'8    a'8    b'8  \bar "|"   c''4. ^"~"    a'8    g'8    
b'8    b'4 ^"~"  \bar ":|."   c''4. ^"~"    a'8    g'8    e'8    c'8    e'8  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
