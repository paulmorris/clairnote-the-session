\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JeffK627"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/279#setting279"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/279#setting279" {"https://thesession.org/tunes/279#setting279"}}}
	title = "Humours Of Ballymanus, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key d \major   a'4    a'8    a'8    g'8    fis'8    a'8    g'8    
fis'8    \bar "|"   a'4    a'8    a'8    g'8    fis'8    b'8    cis''8    d''8  
  \bar "|"   a'4    a'8    a'8    g'8    fis'8    a'8    g'8    fis'8    
\bar "|"   b'4 ^"~"    b'8    b'4    a'8    b'8    cis''8    d''8    }     
\repeat volta 2 {   d''8    fis''8    g''8    a''8    fis''8    d''8    cis''8  
  b'8    a'8    \bar "|"   d''8    fis''8    g''8    a''8    fis''8    d''8    
fis''8    g''8    a''8    \bar "|"   d''8    fis''8    g''8    a''8    fis''8   
 d''8    cis''8    b'8    a'8    \bar "|"   b'4 ^"~"    b'8    b'4    a'8    
b'8    cis''8    d''8    }     \repeat volta 2 {   fis'8    a'8    fis'8    
d''8    a'8    fis'8    d''8    a'8    g'8    \bar "|"   fis'8    a'8    fis'8  
  d''8    a'8    fis'8    b'8    cis''8    d''8    \bar "|"   fis'8    a'8    
fis'8    d''8    a'8    fis'8    d''8    a'8    g'8    \bar "|"   b'4 ^"~"    
b'8    b'4    a'8    b'8    cis''8    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
