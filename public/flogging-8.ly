\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Madelyn"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/195#setting29300"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/195#setting29300" {"https://thesession.org/tunes/195#setting29300"}}}
	title = "Flogging, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   b'8    g'8    g'16    a'16    g'8    b'8   
 g'8    d''8    g'8  \bar "|"   b'8    g'8    g'8    a'8    b'8    d''8    g''8 
   d''8  \bar "|"   b'8    g'8    g'8    fis'8    fis'8    g'8    b'8    c''8  
\bar "|"   a'8    g'8    fis'8    g'8    a'8    b'8    c''8    a'8  \bar "|"    
 b'8    d'8    g'8    a'8    b'8    g'8    c''8    g'8  \bar "|"   b'8    d'8   
 g'8    a'8    b'8    d''8    g''8    d''8  \bar "|"   b'16    d''16    b'8    
a'8    c''8    a'8    b'8    c''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8 
   a'8    b'8    c''8    a'8  \bar "||"     b'8    d''8    g''8    d''8    b'4. 
   d''8  \bar "|"   fis''8    g''8    a''8    g''8    fis''8    g''8    a''8    
fis''8  \bar "|"   g''8    fis''8    g''8    d''8    a'8    b'8    c''8    b'8  
\bar "|"   a'8    g'8    fis'8    g'8    a'8    b'8    c''8    a'8  \bar "|"    
 b'8    d''8    g''8    d''8    b'8    g'8    b'8    d''8  \bar "|"   g''16    
a''16    g''8    e''8    g''8    fis''8    g''8    a''8    fis''8  \bar "|"   
g''4    g''8    fis''8    g''8    e''8    d''8    b'8  \bar "|"   c''8    b'8   
 a'8    g'8    fis'8    g'8    a'8    c''8  \bar "||"     b'8    d''8    g''8   
 d''8    e''8    d''8    g''8    d''8  \bar "|"   b'8    d''8    g''8    d''8   
 b'8    g'8    g'8    b'8  \bar "|"   a'8    c''8    fis''8    c''8    a'8    
c''8    fis''8    c''8  \bar "|"   a'16    b'16    c''8    fis''8    c''8    
b'8    d'8    g'8    a'8  \bar "|"     b'8    d''8    g''8    d''8    e''8    
d''8    g''8    d''8  \bar "|"   b'8    d''8    e''8    fis''8    g''4.    a''8 
 \bar "|"   b''16    a''16    g''8    a''16    g''16    fis''8    g''8    e''8  
  d''8    b'8  \bar "|"   c''8    b'8    a'8    g'8    fis'8    g'8    a'8    
c''8  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
