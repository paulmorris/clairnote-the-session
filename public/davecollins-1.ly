\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/354#setting354"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/354#setting354" {"https://thesession.org/tunes/354#setting354"}}}
	title = "Dave Collins'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key f \major   \grace {    a'8  }   bes'4    f'8    d'8    f'8    
bes'8    \bar "|" \grace {    c''8    d''8  }   c''4    bes'8    c''8    bes'8  
  g'8    \bar "|"   bes'4    g'8  \grace {    a'8  }   g'8    f'8    d'8    
\bar "|"   d'8    f'8    d'8    c'4. ^"~"    \bar "|"     bes4. ^"~"    d'8    
bes8    d'8    \bar "|"   bes8    d'8    f'8    g'8    f'8    d'8    \bar "|"   
f'16    g'16    a'8    bes'8    c''8    a'8    f'8    \bar "|"   bes'8    d''8  
  c''8    bes'4.  }     |
 d''4. ^"~"    c''4. ^"~"    \bar "|"   bes'8    d''8    g''8    f''8    d''8   
 f''8    \bar "|" \grace {    g''8    a''8  }   g''4    f''8    d''8    c''8    
d''8    \bar "|"   f''8    g''8    a''8    g''4 ^"~"    a''8    \bar "|"     
bes''8    a''8    g''8    a''8    g''8    f''8    \bar "|"   g''8    f''8    
d''8    c''8    a'8    f'8    \bar "|"   f'16    g'16    a'8    bes'8    c''8   
 a'8    f'8    \bar "|"   bes'8    d''8    c''8    bes'4    c''8    \bar "|"    
 d''4. ^"~"    c''4. ^"~"    \bar "|"   bes'8    d''8    g''8    f''8    d''8   
 f''8    \bar "|" \grace {    g''8    a''8  }   g''4    f''8    d''8    c''8    
bes'8    \bar "|"   g'8    f'8    d'8    c'4. ^"~"    \bar "|"     bes4. ^"~"   
 d'8    bes8    d'8    \bar "|"   bes8    d'8    f'8    g'8    f'8    d'8    
\bar "|"   f'16    g'16    a'8    bes'8    c''8    a'8    f'8    \bar "|"   
bes'8    d''8    c''8    bes'4.    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
