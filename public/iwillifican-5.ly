\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Loughcurra"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1248#setting14552"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1248#setting14552" {"https://thesession.org/tunes/1248#setting14552"}}}
	title = "I Will If I Can"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   d'8  \bar "|"   g'8    b'8    d''8    g''8    d''8    
b'8  \bar "|"   d''8.    e''16    d''8    d''4    b'8  \bar "|"   g'8    b'8    
d''8    g''8    d''8    b'8  \bar "|"   a'8.    b'16    a'8    a'4    d'8  
\bar "|"   g'8    b'8    d''8    g''8    d''8    b'8  \bar "|"   d''8    e''8   
 fis''8  \grace {    a''8  }   g''8    fis''8    g''8  \bar "|"   e''8    d''8  
  b'8  \grace {    d''8  }   c''8    b'8    a'8  \bar "|"   g'8    a'8    g'8   
 g'4  }   \repeat volta 2 {   d''8  \bar "|"   g''4    d''8  \grace {    fis''8 
 }   e''8    d''8    b'8  \bar "|"   d''8.    e''16    d''8    d''8    e''8    
fis''8  \bar "|"   g''4    d''8  \grace {    fis''8  }   e''8    d''8    b'8  
\bar "|"   a'8.    b'16    a'8    a'4    d''8  \bar "|"   g''4    d''8  
\grace {    fis''8  }   e''8    d''8    b'8  \bar "|"   d''8    e''8    fis''8  
  g''8    fis''8    e''8  \bar "|"   d''8    c''8    b'8  \grace {    d''8  }   
c''8    b'8    a'8  \bar "|"   g'8.    a'16    g'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
