\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1118#setting16925"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1118#setting16925" {"https://thesession.org/tunes/1118#setting16925"}}}
	title = "High Road To Linton, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \repeat volta 2 {   cis''8    e''8    e''8    
fis''8    a''4    a''8    e''8    \bar "|"   fis''8    g''8    fis''8    e''8   
 a''4    a''8    e''8    \bar "|"   cis''8    e''8    e''8    fis''8    a''4    
a''8    e''8    \bar "|"   fis''8    a''8    e''8    cis''8    b'4    a'4    }  
 \repeat volta 2 {   cis''8    d''8    e''8    g''8    fis''8    d''8    d''8   
 fis''8    \bar "|"   e''8    cis''8    cis''8    e''8    fis''8    b'8    b'8  
  fis''8    \bar "|"   cis''8    d''8    e''8    g''8    fis''8    d''8    d''8 
   fis''8    \bar "|"   e''16    gis''16    a''8    e''8    cis''8    b'4    
a'4    }   \repeat volta 2 { \grace {    gis''8  }   a''4    a''4    e''8    
fis''8    a''4    \bar "|"   e''8    fis''8    a''8    e''8    fis''8    a''8   
 e''8    fis''8    \bar "|" \grace {    gis''8  }   a''4    a''4    e''8    
fis''8    a''8    e''8    \bar "|"   fis''8    a''8    e''8    cis''8    b'4    
a'4    }   \repeat volta 2 {   cis''8    d''8    e''8    cis''8    d''8    e''8 
   fis''8    d''8    \bar "|"   cis''8    d''8    e''8    cis''8    b'8    
cis''8    d''8    b'8    \bar "|"   cis''8    d''8    e''8    cis''8    d''8    
e''8    fis''8    gis''8    \bar "|"   a''8    fis''8    e''8    cis''8    b'4  
  a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
