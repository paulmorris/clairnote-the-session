\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/336#setting13122"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/336#setting13122" {"https://thesession.org/tunes/336#setting13122"}}}
	title = "Piper's Despair, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   e'8    fis'8    g'8    a'8    
b'8    a'8    fis'8    a'8    \bar "|"   b'4. ^"~"    cis''8    d''8    b'8    
a'8    fis'8    \bar "|"   d'4    b'8    d'8    a'8    d'8    fis'16    g'16    
a'8    \bar "|"   b'8    d''8    cis''8    e''8    d''8    b'8    a'8    fis'8  
  \bar "|"     e'8    fis'8    g'8    a'8    b'8    a'8    fis'8    a'8    
\bar "|"   b'16    cis''16    d''8    e''8    cis''8    d''8    b'8    a'8    
fis'8    \bar "|"   d'8    a'8    fis'8    a'8    d''8    fis''8    a''8    
fis''8    \bar "|"   g''8    fis''8    e''8    d''8    b'8    e''8    e''8    
d''8    }     \repeat volta 2 {   e''8    fis''8    g''8    e''8    b''8    
e''8    g''8    e''8    \bar "|"   b''8    e''8    g''8    e''8    b''8    e''8 
   fis''8    e''8    \bar "|"   d''4    d''16    d''16    d''8    a''8    d''8  
  e''8    d''8    \bar "|"   b'8    d''8    d''16    d''16    d''8    fis''8    
g''8    fis''8    d''8    \bar "|"     e''8    b'8    b'16    b'16    b'8    
b''8    b'8    g''8    fis''8    \bar "|"   e''8    fis''8    g''8    a''8    
b''8    g''8    a''8    fis''8    \bar "|"   d''8    e''8    fis''8    d''8    
g''8    a''8    a''8    fis''8    \bar "|"   g''8    fis''8    e''8    d''8    
b'8    e''8    e''8    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
