\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1413#setting1413"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1413#setting1413" {"https://thesession.org/tunes/1413#setting1413"}}}
	title = "Touching Cloth"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \major   \repeat volta 2 {   b'8    e'4. ^"~"    cis''8    b'8 
   gis'8    cis''8  \bar "|"   b'8    cis''8    e''8    gis''8    fis''8    
gis''8    e''8    cis''8  \bar "|"   b'8    e'4. ^"~"    cis''8    b'8    gis'8 
   cis''8  \bar "|"   e''8    fis''8    gis''8    e''8    fis''4    e''8    
cis''8  \bar "|"     \bar "|"   b'8    e'4. ^"~"    cis''8    b'8    gis'8    
cis''8  \bar "|"   b'8    cis''8    e''8    gis''8    fis''8    a''8    gis''8  
  a''8  \bar "|"   b''8    gis''8    e''8    gis''8    fis''8    gis''8    e''8 
   cis''8  \bar "|"   b'8    e''8    gis''8    e''8    fis''8    gis''8    e''8 
   cis''8  }     \repeat volta 2 {   |
 b'8    e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   b'8    e''8    gis''8  
  e''8  \bar "|"   b'8    e''8    gis''8    e''8    fis''8    gis''8    e''8    
cis''8  \bar "|"   b'8    e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   b'8  
  e''8    gis''8    e''8  \bar "|" \tuplet 3/2 {   fis''8    gis''8    a''8  }   
gis''8    e''8    fis''8    gis''8    e''8    cis''8  \bar "|"     \bar "|"   
b'8    e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   b'8    e''8    gis''8   
 e''8  \bar "|"   b'8    e''8    gis''8    e''8    fis''8    a''8    gis''8    
a''8  \bar "|"   b''8    gis''8    e''8    gis''8    fis''8    gis''8    e''8   
 cis''8  \bar "|"   b'8    e''8    gis''8    e''8    fis''8    gis''8    e''8   
 cis''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
