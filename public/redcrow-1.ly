\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/184#setting184"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/184#setting184" {"https://thesession.org/tunes/184#setting184"}}}
	title = "Red Crow, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   a4    e'8    a8    b8    g8    
g8    b8  \bar "|" \tuplet 3/2 {   a8    a8    a8  }   e'8    g'8    fis'8    
d'8    e'8    g'8  \bar "|"   a'8    g'8    e'8    d'8    e'8    a8    a8    
d'8  \bar "|"   e'8    c'8    d'8    c'8    b8    g8    g8    b8  \bar "|"     
\bar "|" \tuplet 3/2 {   a8    a8    a8  }   e'8    a8    b8    g8    g8    b8  
\bar "|" \tuplet 3/2 {   a8    a8    a8  }   e'8    g'8    \tuplet 3/2 {   fis'8  
  fis'8    d'8  }   e'8    g'8  \bar "|"     } \alternative{{   a'8    g'8    
e'8    d'8    e'8    a8    a8    c'8  \bar "|"   d'8    b8    g8    b8    a4    
\tuplet 3/2 {   g8    a8    b8  } } {   a'8    g'8    e'8    d'8    e'8    a8    
a8    c'8    d'8    b8    g8    b8    a4    \tuplet 3/2 {   a'8    a'8    a'8  } 
\bar "|"     \bar "|"   a'4    e''8    a'8    c''8    e''8    a'8    c''8  
\bar "|"   e''8    a'8    c''8    e''8    d''8    c''8    b'8    a'8  \bar "|" 
\tuplet 3/2 {   g'8    g'8    g'8  }   d''8    c''8    b'8    d''8    g'8    b'8 
 \bar "|"   d''8    g'8    b'8    d''8    e''8    d''8    c''8    b'8  \bar "|" 
    \tuplet 3/2 {   a'8    a'8    a'8  }   e''8    a'8    c''8    e''8    a'8    
c''8  \bar "|"   e''8    a'8    c''8    b''8    a''8    g''8    e''8    g''8  
\bar "|"   a''8    g''8    e''8    cis''8    d''8    f''8    e''8    d''8  
\bar "|" \tuplet 3/2 {   c''8    b'8    a'8  }   b'8    g'8    a'4    a'8    g'8 
 \bar "|"     \bar "|"   a'4    e''8    a'8    c''8    e''8    a'8    c''8  
\bar "|"   e''8    a'8    c''8    e''8    d''8    c''8    b'8    a'8  \bar "|" 
\tuplet 3/2 {   g'8    g'8    g'8  }   d''8    c''8    b'8    d''8    g'8    b'8 
 \bar "|"   d''8    g'8    b'8    d''8    e''8    d''8    c''8    b'8  \bar "|" 
    \bar "|" \tuplet 3/2 {   a'8    a'8    a'8  }   a''8    a'8    g''8    a'8   
 fis''8    a'8  \bar "|" \tuplet 3/2 {   a'8    a'8    a'8  }   c''8    a'8    
b'8    a'8    g'8    b'8  \bar "|" \tuplet 3/2 {   c''8    b'8    a'8  }   b'8   
 g'8    a'8    g'8    e'8    d'8  \bar "|"   e'8    c'8    d'8    b8    c'8    
a8    b8    g8  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
