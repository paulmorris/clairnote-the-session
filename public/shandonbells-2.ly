\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1200#setting14486"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1200#setting14486" {"https://thesession.org/tunes/1200#setting14486"}}}
	title = "Shandon Bells"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'8  \bar "|"   a'8.    fis'16    d'8    d'8    fis'8 
   a'8  \bar "|"   d''8.    e''16    d''8    cis''8    b'8    a'8  \bar "|"   
b'8.    g'16    e'8    e'8    fis'8    a'8  \bar "|"   b'4    a'8    b'8    
cis''8    d''8  \bar "|"   a'8.    fis'16    d'8    d'8    fis'8    a'8  
\bar "|"   d''8.    e''16    d''8    cis''8    b'8    a'8  \bar "|"   b'8    
cis''8    d''8    e''8    cis''8    a'8  \bar "|"   d''4.    d''8    cis''8    
b'8  \bar ":|."   d''4.    d''4  \bar "|"   g''8  \bar "|"   fis''4    d''8 (   
d''8  -)   e''8    d''8  \bar "|"   fis''8    a''8    a''8    a''8    fis''8    
d''8  \bar "|"   cis''8.    a'16    a'8    e''8.    a'16    a'8  \bar "|"   
cis''8    b'8    cis''8    e''8    fis''8    g''8  \bar "|"   fis''4    d''8 (  
 d''8  -)   e''8    d''8  \bar "|"   fis''8    a''8    a''8    a''8    fis''8   
 d''8  \bar "|"   b'8    cis''8    d''8    e''8    cis''8    a'8  \bar "|"   
d''4.    d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
