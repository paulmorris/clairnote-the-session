\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Aidan Crossey"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1363#setting1363"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1363#setting1363" {"https://thesession.org/tunes/1363#setting1363"}}}
	title = "Thirty Bottle Hughie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'8    a'8  \bar "|"   b'8    g'8    e'8    g'8    
d'8    g'8    e'8    g'8  \bar "|"   c''8    a'8    a'8    a'16    b'16    c''8 
   a'8    a'4  \bar "|"   b'8    g'8    e'8    g'8    d'8    g'8    e'8    g'8  
\bar "|"   c''8    b'8    a'8    b'8    g'4    g'8    a'8      b'8    g'8    
e'8    g'8    d'8    g'8    e'8    g'8  \bar "|"   c''8    a'8    a'8    a'16   
 b'16    c''8    a'8    a'4  \bar "|"   b'8    g'8    e'8    g'8    d'8    g'8  
  e'8    g'8  \bar "|"   c''8    b'8    a'8    b'8    g'4  \bar "||"     g'8    
a'8  \bar "|"   b'8    g'8    d''8    g'8    b'8    g'8    d''8    g'8  
\bar "|"   c''8    a'8    fis'8    a'8    c''8    a'8    fis'8    a'8  \bar "|" 
  b'8    g'8    d''8    g'8    b'8    g'8    d''8    g'8  \bar "|"   c''8    
a'8    fis'8    a'8    g'4    g'8    a'8  \bar "|"     b'8    g'8    d''8    
g'8    b'8    g'8    d''8    g'8  \bar "|"   c''8    a'8    fis'8    a'8    e'8 
   a'8    d'8    a'8  \bar "|"   b'8    g'8    d''8    g'8    b'8    g'8    
d''8    g'8  \bar "|"   c''8    a'8    fis'8    a'8    g'4  \bar "||"     g'8   
 a'8  \bar "|"   b'8    g'8    d''8    g'8    e''8    g'8    d''8    g'8  
\bar "|"   c''8    a'8    a'8    a'16    b'16    c''8    a'8    fis'8    a'8  
\bar "|"   b'8    g'8    d''8    g'8    e''8    g'8    d''8    g'8  \bar "|"   
c''8    a'8    fis'8    a'8    g'4    g'8    a'8  \bar "|"     b'8    g'8    
d''8    g'8    e''8    g'8    d''8    g'8  \bar "|"   c''8    a'8    a'8    
a'16    b'16    c''8    a'8    fis'8    a'8  \bar "|"   b'8    g'8    d''8    
g'8    e''8    g'8    d''8    g'8  \bar "|"   c''8    a'8    fis'8    a'8    
g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
