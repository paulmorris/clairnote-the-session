\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dfay"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/872#setting6861"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/872#setting6861" {"https://thesession.org/tunes/872#setting6861"}}}
	title = "Fisher's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \tuplet 3/2 {   a'8    b'8    cis''8  }   \bar "|"   
d''8    a'8    fis'8    d'8    g'8    b'8    a'8    g'8    \bar "|"   fis'8    
g'8    e'8    fis'8    g'8    b'8    a'8    g'8    \bar "|"   a'8    fis'8    
d'8    a'8    b'8    g'8    e'8    b'8    \bar "|"   a'8    fis'8    d'8    a'8 
   b'8    a'8    b'8    cis''8  \bar "|"     d''8    a'8    \tuplet 3/2 {   
fis'8    e'8    d'8  }   g'8    b'8    a'8    g'8    \bar "|" \tuplet 3/2 {   
fis'8    g'8    fis'8  }   e'8    fis'8    g'8    b'8    a'8    g'8  \bar "|"   
fis'8    a'8    \tuplet 3/2 {   cis''8    b'8    a'8  }   d''8    b'8    cis''8  
  d''8  \bar "|"   e''8    d''8  \grace {    e''8  }   d''8    cis''8    d''4   
 \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"     d''8    a'8    fis'8    
d'8    g'8    b'8    a'8    g'8    \bar "|"   fis'8    a'8    fis'8    d'8    
g'8    b'8    a'8    g'8  \bar "|"   fis'8    d'8    d'8    fis'8    g'8    e'8 
   e'8    g'8  \bar "|"   fis'8    d'8    d'8    fis'8    e'8    d'8    b8    
cis''8  \bar "|"     d''8    a'8    fis'8    d'8    g'8    b'8    a'8    g'8    
\bar "|" \tuplet 3/2 {   fis'8    e'8    d'8  } \grace {    g'8  }   fis'8    
d'8    g'8    b'8    b'8    g'8  \bar "|"   a'8    b'8    cis''8    a'8    e''8 
   cis''8    a'8    cis''8    \bar "|"   e''8    d''8  \grace {    e''8  }   
d''8    cis''8    d''4    cis''8    d''8  \bar "|"     e''8    cis''8    a'8    
cis''8    e''8    fis''8    g''8    e''8  \bar "|"   fis''8    d''4. ^"~"    
fis''8    a''8    g''8    fis''8  \bar "|"   e''8    cis''8    a'8    cis''8    
e''8    fis''8    g''8    e''8  \bar "|" \tuplet 3/2 {   e''8    fis''8    e''8  
}   \tuplet 3/2 {   d''8    cis''8    b'8  }   a'4    a'8    cis''8  \bar "|"    
 b'8    g'8    d'8    g'8    b'8    cis''8    d''8    b'8  \bar "|"   a'8    
fis'8    d'8    fis'8    a'8    b'8    cis''8    a'8  \bar "|" \tuplet 3/2 {   
b'8    cis''8    d''8  }   e''8    fis''8    g''8    e''8    cis''8    d''8  
\bar "|"   e''8    d''8    cis''8    e''8    d''4    cis''8    d''8  \bar "|"   
  e''8    cis''8    a'8    cis''8    e''8    fis''8    g''8    e''8  \bar "|"   
fis''8    d''4. ^"~"    a''8    d''8    fis''8    d''8  \bar "|"   e''8    
cis''8    a'8    cis''8    e''8    fis''8    g''8    e''8  \bar "|" 
\tuplet 3/2 {   e''8    fis''8    e''8  }   \tuplet 3/2 {   d''8    cis''8    b'8 
 }   a'8    cis''4. ^"~"  \bar "|"     b'8    g'4. ^"~"    d''8    g'8    b'8   
 g'8  \bar "|"   a'8    fis'8    d'8    fis'8    a'8    b'8    cis''8    a'8  
\bar "|" \tuplet 3/2 {   b'8    cis''8    d''8  }   cis''8    b'8    a'8    g'8  
  fis'8    e'8  \bar "|" \tuplet 3/2 {   d'8    e'8    d'8  }   cis'8    e'8    
d'4    \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"     d''8    a'8    fis'8 
   d'8    g'8    b'8    a'8    g'8  \bar "|"   fis'8    g'8    e'8    fis'8    
g'8    b'8    a'8    g'8    \bar "|"   a'8    fis'8    d'8    fis'8    b'8    
g'8    e'8    g'8    \bar "|"   a'8    fis'8    d'8    fis'8    g'8    e'8    
cis'8    e'8  \bar "|"     d''8    a'8    fis'8    d'8    g'8    b'8    a'8    
g'8  \bar "|" \tuplet 3/2 {   fis'8    e'8    d'8  }   \tuplet 3/2 {   fis'8    
e'8    d'8  }   g'8    b'8    a'8    g'8  \bar "|"   a'8    b'8    cis''8    
a'8  \grace {    a''8  }   g''8    e''8    cis''8    d''8  \bar "|"   e''8    
d''8  \grace {    e''8  }   d''8    cis''8    d''4    \tuplet 3/2 {   a'8    b'8 
   cis''8  } \bar "|"     d''8    a'8    fis'8    d'8    g'8    b'8    a'8    
g'8    \bar "|"   fis'8    a'8    fis'8    d'8    g'8    b'8    a'8    g'8  
\bar "|"   fis'8    d'8    d'8    fis'8    g'8    e'8    e'8    g'8  \bar "|"   
fis'8    d'8    d'8    fis'8    e'8    d'8    b8    cis''8  \bar "|"     d''8   
 a'8    fis'8    d'8    g'8    b'8    a'8    g'8  \bar "|"   fis'8    a'8    
fis'8    d'8    g'8    b'8    a'8    g'8  \bar "|"   fis'8    a'8    
\tuplet 3/2 {   cis''8    b'8    a'8  }   d''8    b'8    cis''8    d''8  
\bar "|"   e''8    d''8  \grace {    e''8  }   d''8    cis''8    d''4    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
