\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/666#setting13705"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/666#setting13705" {"https://thesession.org/tunes/666#setting13705"}}}
	title = "Alexander's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"   
d''8 ^"D"   a'8    fis'8    a'8      d'8 ^"D"   fis'8    a'8    d''8  \bar "|"  
 fis''8 ^"D"   d''8    cis''8    d''8      a'8 ^"A"   cis''8    e''8    fis''8  
\bar "|"   g''4. ^"Em"^"~"    e''8      fis''8 ^"D"   d''8    cis''8    d''8  
\bar "|" \tuplet 3/2 {   e''8 ^"A"   fis''8    e''8  }   \tuplet 3/2 {   d''8    
cis''8    b'8  }     a'4 ^"D"   \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|" 
    d''8 ^"D"   a'8    fis'8    a'8      d'8 ^"D"   fis'8    a'8    d''8  
\bar "|"   fis''8 ^"D"   d''8    cis''8    d''8      a'8 ^"A"   cis''8    e''8  
  fis''8  \bar "|"   g''4. ^"Em"^"~"    e''8      fis''8 ^"D"   d''8    cis''8  
  d''8  \bar "|" \tuplet 3/2 {   e''8 ^"A"   fis''8    e''8  }   d''8    cis''8  
    d''4 ^"D" }   \repeat volta 2 {   a'8    g'8  \bar "|"   fis'8 ^"D"   a'8   
 d''8    a'8      fis'8 ^"D"   a'8    d''8    a'8  \bar "|"   g'8 ^"G"   b'8    
d''8    b'8      g'8 ^"G"   b'8    d''8    b'8  \bar "|"   a'8 ^"A"   cis''8    
e''8    cis''8      a'8 ^"A"   cis''8    e''8    cis''8  \bar "|"   d''8 ^"D"   
fis''8    a''8    fis''8    \tuplet 3/2 {   g''8 ^"D"   fis''8    e''8  }   d''8 
   a'8  \bar "|"     fis'8 ^"D"   a'8    d''8    a'8      fis'8 ^"D"   a'8    
d''8    a'8  \bar "|"   g'8 ^"G"   b'8    d''8    b'8      g'8 ^"G"   b'8    
d''8    b'8  \bar "|"   a'8 ^"A"   cis''8    e''8    fis''8      g''8 ^"G"   
e''8    cis''8    d''8  \bar "|" \tuplet 3/2 {   e''8 ^"A"   fis''8    e''8  }   
d''8    cis''8      d''4 ^"D" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
