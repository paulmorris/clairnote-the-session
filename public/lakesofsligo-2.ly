\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/393#setting1792"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/393#setting1792" {"https://thesession.org/tunes/393#setting1792"}}}
	title = "Lakes Of Sligo, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key d \major   fis'8    a'8    a'8    b'8  \bar "|"   d''8.    e''16 
   d''8    cis''8  \bar "|"   b'8    a'8    b'16    cis''16    d''8  \bar "|"   
e''8.    d''16    e''8    fis''8  \bar "|"     fis'8    a'8    a'8    b'8  
\bar "|"   d''8    e''8    fis''8.    e''16  \bar "|"   d''8    b'8    a'8    
fis''8  \bar "|"   e''8    fis''16    e''16    d''4  }     \repeat volta 2 {   
fis''8    a''8    d''8    e''8  \bar "|"   fis''8    a''8    a''8    g''16    
fis''16  \bar "|"   g''8    b''8    e''8    fis''8  \bar "|"   g''8    b''8    
b''8    a''8  \bar "|"     fis''8    a''8    e''8    fis''8  \bar "|"   d''8    
e''8    fis''8.    e''16  \bar "|"   d''8    b'8    a'8    fis''8  \bar "|"   
e''8    fis''16    e''16    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
