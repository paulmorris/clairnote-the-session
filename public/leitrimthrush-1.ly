\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "laura nesbit"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/822#setting822"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/822#setting822" {"https://thesession.org/tunes/822#setting822"}}}
	title = "Leitrim Thrush, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key c \major   a'4.    b'8    c''8    a'8    g'8    b'8  \bar "|"   
a'8    c''8    a'8    g'8    e'8    f'8    g'8    c''8  \bar "|"   a'8    g'8   
 a'8    b'8    c''8    d''8    e''8    f''8  \bar "|"   g''8    e''8    a''8    
g''8    e''8    c''8    d''8    b'8  \bar "|"     a'8    g'8    a'8    b'8    
c''8    a'8    g'8    b'8  \bar "|"   a'8    c''8    a'8    g'8    e'8    b8    
c'8    c''8  \bar "|"   a'8    g'8    a'8    b'8    c''8    d''8    e''8    
f''8  \bar "|" \tuplet 3/2 {   g''8    f''8    e''8  }   a''8    g''8      e''8 
^"H"   c''8    c''4  \bar "||"     \tuplet 3/2 {   g''8    a''8    g''8  }   
e''8    c''8    g''8    c''8    e''8    c''8  \bar "|"   g''8    c''8    e''8   
 c''8    b'8    c''8    d''4  \bar "|"   g''8    c''8    e''8    c''8    g''8   
 c''8    e''8    c''8  \bar "|"   g''8    a''8    g''8    f''8    e''8    c''8  
  c''4  \bar "|"     \tuplet 3/2 {   g''8    a''8    g''8  }   e''8    c''8    
g''8    c''8    e''8    c''8  \bar "|"   g''8    c''8    e''8    c''8    b'8    
c''8    d''4  \bar "|"   g''8    a''8    g''8    e''8    \tuplet 3/2 {   d''8    
e''8    d''8  }   c''8    a'8  \bar "|"   g'8    a'8    a''8    g''8      e''8 
^"D.C."   c''8    d''8    c''8  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
