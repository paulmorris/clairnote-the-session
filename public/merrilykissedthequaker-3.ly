\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Nikita Pfister"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/70#setting12536"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/70#setting12536" {"https://thesession.org/tunes/70#setting12536"}}}
	title = "Merrily Kissed The Quaker"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key g \major   d'8  \bar "|"   g'4    b'8    d''4    d''8  \bar "|" 
  e''8    d''8    b'8    d''8    b'8    a'8  \bar "|"   g'4    b'8    d''8    
b'8    g'8  \bar "|"   a'8    b'8    a'8    a'4    d'8  \bar "|"   g'4    b'8   
 d''4    d''8  \bar "|"   e''8    d''8    b'8    d''8    b'8    a'8  \bar "|"   
g'8    a'8    b'8    d'4    e'8  \bar "|"   g'8    a'8    g'8    g'4    d'8  
\bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
