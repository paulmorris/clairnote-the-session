\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1182#setting1182"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1182#setting1182" {"https://thesession.org/tunes/1182#setting1182"}}}
	title = "Batchelder's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key f \major   f''4    a''8    f''8    e''8    f''8    g''8    e''8  
\bar "|"   f''4    c''4    c''4    b'8    c''8  \bar "|"   d''8    c''8    
bes'8    a'8    bes'8    a'8    g'8    f'8  \bar "|"   e'8    f'8    g'8    a'8 
   bes'8    c''8    d''8    e''8  \bar "|"     f''4    a''8    f''8    e''8    
f''8    g''8    e''8  \bar "|"   f''4    c''4    \tuplet 3/2 {   c''8    d''8    
c''8  }   b'8    c''8  \bar "|"   d''8    c''8    bes'8    a'8    bes'8    a'8  
  g'8    f'8  \bar "|"   e'8    f'8    g'8    e'8    f'2  }     
\repeat volta 2 {   a'4    c''8    a'8    c''8    a'8    c''8    a'8  \bar "|"  
 bes'4    d''8    bes'8    d''8    bes'8    d''8    bes'8  \bar "|"   a'4    
c''8    a'8    c''8    a'8    c''8    a'8  \bar "|"   bes'8    a'8    g'8    
f'8    e'8    f'8    g'8    bes'8  \bar "|"     a'4    c''8    a'8    c''8    
a'8    c''8    a'8  \bar "|"   bes'4    d''8    bes'8    d''8    bes'8    d''8  
  bes'8  \bar "|"   a'8    c''8    f''8    a''8    bes''8    g''8    e''8    
g''8  \bar "|"   f''4    a''4    f''4    c''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
