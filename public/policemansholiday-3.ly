\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/927#setting27715"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/927#setting27715" {"https://thesession.org/tunes/927#setting27715"}}}
	title = "Policeman's Holiday, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   \repeat volta 2 {   g''8    e''8  \bar "|"   d''8    
b'8    g'8    b8    d'8    g'8  \bar "|"   b'8    d''8    d''8    d''8    b'8   
 g'8  \bar "|"   e''8    c''8    a'8    a8    c'8    e'8  \bar "|"   a'8    
c''8    e''8    g''8    fis''8    e''8  \bar "|"     d''8    b'8    g'8    b8   
 d'8    g'8  \bar "|"   b'8    d''8    d''8    d''8    c''8 ^"+"   b'8  
\bar "|"   a'8    c''8    e''8    a''8    g''8    d''8  \bar "|"   fis''8    
e''8    d''8    d''8  }     e''8    fis''8  \bar "|"   g''8    e''8    a''8    
fis''8    d''8    g''8  \bar "|"   e''8    c''8 ^"+"   e''8    d''8    b'8    
g'8  \bar "|"   g'8    b'8    d''8    g''8    fis''8    e''8  \bar "|"   d''8   
 b'8    g'8    g'8    b'8    d''8  \bar "|"     g''8    e''8    a''8    fis''8  
  d''8    g''8  \bar "|"   e''8    c''8 ^"+"   e''8    d''8    c''8    b'8  
\bar "|"   a'8    c''8    e''8    a''8    g''8    d''8  \bar "|"   fis''8    
e''8    d''8    d''8  }     \repeat volta 2 {   c''8    c''!8  \bar "|"   b'8   
 c''!8    cis''8    d''8    b''8    fis''8  \bar "|"   a''8    g''8    d''8    
fis''8    e''8    dis''8  \bar "|"   d''8    b''8    fis''8    a''8    g''8    
d''8  \bar "|"   fis''8    e''8    d''8    b''8    g''8    e''8  \bar "|"     
d''8    b''8    fis''8    a''8    g''8    d''8  \bar "|"   b'8    a''8    g''8  
  g''8    d''8    b'8  \bar "|"   a'8    c''8    e''8    a''8    g''8    d''8  
\bar "|"   fis''8    e''8    d''8    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
