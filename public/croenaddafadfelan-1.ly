\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dafydd Monks"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1212#setting1212"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1212#setting1212" {"https://thesession.org/tunes/1212#setting1212"}}}
	title = "Croen A Ddafad Felan"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   g'4    a'4    \bar "|"   b'4    c''4    \bar "|"   
d''4    d''4    \bar "|"   c''4    b'4    \bar "|"   c''4    a'4    \bar "|"   
d''4    d''4    \bar "|"     c''4    b'4    \bar "|"   a'4    a'4    \bar "|"   
b'4    a'4    \bar "|"   g'4    g'4    \bar "|"   a'4    g'4    \bar "|"   
fis'4    g'4    \bar "|"   a'4    a'4    \bar "||"     g'4    a'4    \bar "|"   
b'4    c''4    \bar "|"   d''4    d''4    \bar "|"   c''4    b'4    \bar "|"   
c''4    a'4    \bar "|"   d''4    d''4    \bar "|"     c''4    b'4    \bar "|"  
 a'4    a'4    \bar "|"   b'4    a'4    \bar "|"   g'4    g'4    \bar "|"   a'4 
   a'4    \bar "|"   d''4    d''4    \bar "|"   g'4    g'4    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
