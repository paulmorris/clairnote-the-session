\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Musicalbison"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/880#setting880"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/880#setting880" {"https://thesession.org/tunes/880#setting880"}}}
	title = "Da New Rigged Ship"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   e''8    g''8  \bar "|"   a''4    a''8    b''8    
a''8    g''8    e''8    g''8  \bar "|"   a''4    a''8    b''8    a''8    g''8   
 e''8    fis''8  \bar "|"   g''4    g''8    g''8    fis''8    g''8    a''8    
g''8  \bar "|"   fis''4    d''4    d''4    e''8    g''8  \bar "|"     a''4    
a''8    b''8    a''8    g''8    e''8    g''8  \bar "|"   a''4    a''8    b''8   
 a''8    g''8    e''8    fis''8  \bar "|"   g''4    g''8    g''8    fis''8    
g''8    a''8    g''8  \bar "|"   fis''4    d''4    d''4    e''8    d''8  
\bar "||"     cis''8    a'8    cis''8    a'8    b'4    e''8    d''8  \bar "|"   
cis''8    a'8    cis''8    a'8    e'4    e''8    d''8  \bar "|"   cis''8    a'8 
   cis''8    a'8    b'4    e''8    d''8  \bar "|"   cis''4    a'4    a'4    
e''8    d''8  \bar "|"     cis''8    a'8    cis''8    a'8    b'4    e''8    
d''8  \bar "|"   cis''8    a'8    cis''8    a'8    e'4    e''8    d''8  
\bar "|"   cis''8    a'8    cis''8    a'8    b'4    e''8    d''8  \bar "|"   
cis''4    a'4    a'4    a'8    b'8  \bar "||"     \repeat volta 2 {   c''8    
d''8    e''8    c''8    b'8    c''8    d''8    b'8  \bar "|"   a'8    b'8    
a'8    fis'8    g'8    fis'8    e'4  \bar "|"   c''8    d''8    e''8    c''8    
b'8    c''8    d''8    b'8  } \alternative{{   c''4    a'4    a'4    a'8    b'8 
 } {   c''4    a'4    a'4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
