\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/544#setting13502"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/544#setting13502" {"https://thesession.org/tunes/544#setting13502"}}}
	title = "Garrett Barry's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   a8    b8    c'8    \bar "|"   d'8 
   e'8    fis'8    g'4. ^"~"  \bar "|"   a'8    g'8    e'8    c''4    a'8  
\bar "|"   d''8    c''8    a'8    d''4    e''8  \bar "|"   fis''8    e''8    
d''8    c''8    a'8    g'8  \bar "|"   f'4. ^"~"    g'8    f'8    g'8  \bar "|" 
  a'8    g'8    e'8    b'16    c''16    d''8    e''8  \bar "|"   d''8    c''8   
 a'8    g'8    e'8    c''8  \bar "|"   d'8    e'8    d'8    }   d'4.    
\bar "|"   \repeat volta 2 {   d''8    c''8    a'8    d''4    e''8  \bar "|"   
fis''8    e''8    d''8    \tuplet 3/2 {   e''8    fis''8    g''8  }   e''8  
\bar "|"   d''8    c''8    a'8    c''4    d''8  \bar "|"   e''8    f''8    d''8 
   e''8    c''8    a'8  \bar "|"   d''8    c''8    a'8    d''4    e''8    
} \alternative{{   fis''8    e''8    d''8    e''16    fis''16    g''8    e''8  
\bar "|"   d''8    c''8    a'8    g'8    e'8    c''8  \bar "|"   d'8    e'8    
d'8    d'4    a'8    } {   f''8    d''8    a'8    g'8    c''8    e''8    
\bar "|"   d''8    c''8    a'8    g'8    e'8    c''8    \bar "|"   d'8    e'8   
 d'8    a8    b8    c'8    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
