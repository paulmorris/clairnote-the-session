\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Daemco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/719#setting13792"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/719#setting13792" {"https://thesession.org/tunes/719#setting13792"}}}
	title = "Rip The Calico"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   \bar "|"   d''4    \tuplet 3/2 {   d''8    d''8   
 d''8  }   b'4    \tuplet 3/2 {   d''8    d''8    e''8  } \bar "|"   e''8.    
c''16    d''8.    b'16    g'8    e'4.  \bar "|"   d''4    \tuplet 3/2 {   d''8   
 d''8    d''8  }   b'4  \tuplet 3/2 {   d''8    d''8    e''8  } \bar "|"   e''8. 
   c''16    d''8.    b'16    g'8    e'4.  \bar "|"   g'8.    b'16    c''8.    
d''16    e''4    fis''8.    g''16  \bar "|"   a''8.    fis''16    g''8.    
e''16    d''2  \bar "|" \tuplet 3/2 {   c''8    a'8    a'8  }   a'4    
\tuplet 3/2 {   c''8    a'8    a'8  }   a'4  \bar "|" \tuplet 3/2 {   g'8    e'8  
  e'8  }   e'4    \tuplet 3/2 {   g'8    e'8    e'8  }   e'4  \bar "||" 
\tuplet 3/2 {   c''8    a'8    a'8  }   a'4    \tuplet 3/2 {   c''8    a'8    a'8 
 }   a'4  \bar "|"   a'8.    c''16    d''8.    c''16    d''2  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
