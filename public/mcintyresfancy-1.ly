\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/819#setting819"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/819#setting819" {"https://thesession.org/tunes/819#setting819"}}}
	title = "McIntyre's Fancy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {     c''8 ^"Am"   d''8    e''8      
a'8 ^"F"   g'8    e'8    \bar "|"     g'8 ^"Em7"   a'8    b'8      a'4. ^"Am"   
\bar "|"     e''16 ^"Am"   g''16    a''8    g''8      e''8 ^"G6"   d''8    c''8 
   \bar "|"     e''8 ^"F"   a'8    b'8      a'8 ^"Em7"   g'8    e'8    \bar "|" 
      c''8 ^"Am"   d''8    e''8      d''8 ^"F"   c''8    a'8    \bar "|"     
g'4. ^"Em7"^"~"      g'8 ^"G6"   c''8    d''8    \bar "|"     e''8 ^"Am"   g''8 
   a''8      g''16 ^"G6"   fis''16    e''8    d''8    \bar "|"     c''8 ^"F"   
a'8    g'8      a'4. ^"Am"   }     \repeat volta 2 {     a''4. ^"Am"     g''8 
^"G6"   e''8    d''8    \bar "|"     e''8 ^"Em"   d''8    g''8      e''8 ^"Am"  
 d''8    c''8    \bar "|"     a'4. ^"F"^"~"      g'8 ^"Am7"   a'8    c''8    
\bar "|"     d''8 ^"G6"   e''8    c''8      d''8 ^"Em7"   e''8    g''8    
\bar "|"       a''8 ^"Am"   e''8    g''8      d''8 ^"G6"   e''8    c''8    
\bar "|"     a'4. ^"F"^"~"      g'8 ^"Em"   e'8    g'8    \bar "|"     c''8 
^"Am9"   d''8    e''8      g''16 ^"G6"   fis''16    e''8    d''8    \bar "|"    
 c''8 ^"F"   a'8    g'8      a'4. ^"Am" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
