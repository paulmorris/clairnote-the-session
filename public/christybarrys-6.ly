\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/843#setting21446"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/843#setting21446" {"https://thesession.org/tunes/843#setting21446"}}}
	title = "Christy Barry's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major \time 12/8   \repeat volta 2 {   d'8    \bar "|"   g'4  
  a'8    b'8    a'8    g'8    a'8    b'8    d''8    e''4    fis''8    \bar "|"  
 g''8    e''8    d''8    b'8    a'8    g'8    a'8    b'8    g'8    fis'8    e'8 
   d'8    \bar "|"     g'4    a'8    b'4    g'8    a'8    b'16    c''16    d''8 
   e''4.    \bar "|"   g''8    e''8    d''8    b'4    g'8    a'8    g'8    
fis'8    g'4    }     d''8    \bar "|"   g''4    g''8    g''8    fis''8    g''8 
   a''8    g''8    fis''8    d''4    e''8    \bar "|"   g''8    e''8    d''8    
b'4    g'8    a'8    b'16    c''16    d''8    e''4    a''8    \bar "|"     
g''4.    g''8    b''8    g''8    a''8    g''8    fis''8    d''4.    \bar "|"   
g''8    e''8    d''8    b'4    g'8    a'8    g'8    fis'8    g'8    b'8    d''8 
   \bar "|"     g''8    b''16    a''16    g''8    g''8    fis''8    g''8    
a''8    g''8    fis''8    d''4    e''8    \bar "|"   g''8    e''8    d''8    
b'4    g'8    a'8    b'16    a'16    g'8    fis'8    fis'16    e'16    d'8    
\bar "|"     g'4    a'8    b'4    g'8    a'8    b'8    d''8    e''4    fis''8   
 \bar "|"   g''8    e''8    d''8    b'4.    a'8    g'8    fis'8    g'4    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
