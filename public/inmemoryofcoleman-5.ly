\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Edward Ebel"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/619#setting29638"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/619#setting29638" {"https://thesession.org/tunes/619#setting29638"}}}
	title = "In Memory Of Coleman"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \minor   d'4.    d''8    c''8    bes'8    g'8    f'8    
\bar "|"   d'8    f'4. ^"~"    g'8    f'8    d'8    f'8    \bar "|"   g'4    
bes'8    c''8    d''8    f''8    g''8    a''8    \bar "|"   bes''8    a''8    
g''8    f''8    d''8    f''8    g''8    a''8    \bar "|"     bes''8    a''8    
g''8    f''8    g''8    f''8    d''8    c''8    \bar "|"   d''4    bes'8    g'8 
   f'8    d'8    c'8    d'8    \bar "|"   f'8    g'8    bes'8    d''8    c''8   
 a'8    bes'8    c''8    \bar "|"   d''8    g''8    d''8    c''8    bes'8    
g'8    g'8    f'8    \bar ":|."   d''8    g''8    d''8    c''8    bes'8    g'8  
  g'8    a'8    \bar "||"     bes'4. ^"~"    d''8    f''8    g''8    f''8    
g''8    \bar "|"   ees''8    c''4. ^"~"    a'8    f'4. ^"~"    \bar "|" <<   
g'8    bes'8   >> d''8    g''8    a''8    f''8    d''8    c''8    a'8    
\bar "|"   f'8    g'8    d''8    c''8    bes'8    g'8    g'8    a'8    \bar "|" 
    bes'4 ^"~"    f''8    bes'8    f'8    bes'8    f''8    bes'8    \bar "|"   
d'8    bes'8    f''8    bes'8    a'8    f'8    f'8    a'8    \bar "|"   g'8    
a'8    bes'8    c''8    d''8    g''8    a''8    g''8    \bar "|"   f''8    d''8 
   c''8    a'8    bes'8    g'8    g'8    a'8    \bar "|"     bes'4. ^"~"    
d''8    f''8    g''8    f''8    g''8    \bar "|"   ees''8    c''4. ^"~"    a'8  
  f'4. ^"~"    \bar "|" <<   g'8    bes'8   >> d''8    g''8    a''8    f''8    
d''8    c''8    a'8    \bar "|"   f'8    g'8    d''8    c''8    bes'8    g'8    
g'8    a'8    \bar "|"     bes'4 ^"~"    f''8    bes'8    f'8    bes'8    f''8  
  bes'8    \bar "|"   a''8    bes'8    f''8    bes'8    a'8    f'8    f'8    
a'8    \bar "|"   g'8    a'8    bes'8    c''8    d''8    g''8    a''8    g''8   
 \bar "|"   f''8    d''8    c''8    a'8    bes'8    g'8    g'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
