\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "glauber"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/921#setting921"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/921#setting921" {"https://thesession.org/tunes/921#setting921"}}}
	title = "Knotted Cord, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   a'4. ^"~"    b'8    a'8    e'8    e'4 ^"~"    
\bar "|"   a'4    b'8    d''8    e''8    d''8    b'8    a'8    \bar "|"   g'4. 
^"~"    a'8    g'8    e'8    d'8    e'8    \bar "|"   g'4    b'8    g'8    d''8 
   g'8    b'8    g'8    \bar "|"     a'4. ^"~"    b'8    a'8    e'8    e'4 
^"~"    \bar "|"   a'4    b'8    d''8    e''8    d''8    b'8    a'8    \bar "|" 
  g'8    a'8    b'8    e''8    e''8    g''8  \grace {    a''8  }   g''8    e''8 
   \bar "|"   d''8    b'8    g'8    a'8    b'8    a'8    a'4    }     
\repeat volta 2 {   e''8    a''8    a''4 ^"~"    e''8    a''8    a''4 ^"~"    
\bar "|"   e''8    a''8    a''8    b''8    a''8    g''8    e''8    d''8    
\bar "|"   e''8    g''8    g''4 ^"~"    e''8    g''8    g''4 ^"~"    \bar "|"   
e''8    g''8    g''8    a''8    g''8    e''8    d''8    g''8    \bar "|"     
e''8    a''8    a''4 ^"~"    e''8    a''8    a''4 ^"~"    \bar "|"   e''8    
a''8    a''8    b''8    a''8    g''8    e''8    d''8    \bar "|"   b'8    a'8   
 b'8    d''8    e''8    g''8  \grace {    a''8  }   g''8    a''8    \bar "|"   
d''8    b'8    g'8    a'8    b'8    a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
