\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thady Quill"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1392#setting28887"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1392#setting28887" {"https://thesession.org/tunes/1392#setting28887"}}}
	title = "Rollicking Boys Of Tandragee, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   \repeat volta 2 {   b'8    a'8    b'8    e'4    b'8   
 \bar "|"   b'8    a'8    b'8    d''4    a'8    \bar "|"   b'8    a'8    fis'8  
  d'4    e'8    \bar "|"   fis'8    e'8    d'8    e'8    fis'8    a'8    
\bar "|"     \bar "|"   b'8    a'8    b'8    e'4    b'8    \bar "|"   b'8    
a'8    b'8    d''8    e''8    fis''8    \bar "|"   e''8    d''8    b'8    a'8   
 fis'8    a'8    \bar "|"   b'8    e'8    e'8    e'4.    }     
\repeat volta 2 {   a'8    \bar "|"   b'8    d''8    e''8    fis''4    e''8    
\bar "|"   d''8    e''8    fis''8    a''8    fis''8    e''8    \bar "|"   
fis''4    e''8    d''8    b'8    a'8    \bar "|"   b'4    cis''8    d''8    b'8 
   a'8  \bar "|"     \bar "|"   b'8    d''8    e''8    fis''4    e''8    
\bar "|"   d''8    e''8    fis''8    a''8    fis''8    e''8    \bar "|"   
fis''8    d''8    b'8    a'8    fis'8    a'8    \bar "|"   b'8    e'8    e'8    
e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
