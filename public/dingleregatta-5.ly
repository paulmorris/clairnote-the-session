\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/23#setting24672"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/23#setting24672" {"https://thesession.org/tunes/23#setting24672"}}}
	title = "Dingle Regatta, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key g \major \key g \major \time 6/8 \repeat volta 2 {   g''8    
\bar "|"   fis''8.    e''16    d''8    d''8    e''8    fis''8    g''4.    d''4  
  b'8    \bar "|"   c''4    d''8    b'4    c''8    a'4    a'8    a'4    fis''8  
  \bar "|"     fis''8.    e''16    d''8    d''8    e''8    fis''8    g''4.    
e''4    d''8    \bar "|"   cis''4    cis''8    cis''8    b'8    cis''8    d''4. 
   d''4    }     b'16    c''16    \bar "|"   d''4    d''8    e''4    d''8    
b'4    b'8    d''4    c''8    \bar "|"   a'4    a'8    a'8    g'8    a'8    
b'4.    g'4    b'16    c''16    \bar "|"     d''4    d''8    e''4    d''8    
b'4    b'8    d''4    c''8    \bar "|"   a'4    a'8    a'8    b'8    a'8    
g'4.    g'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
