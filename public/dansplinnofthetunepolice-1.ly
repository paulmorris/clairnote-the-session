\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1325#setting1325"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1325#setting1325" {"https://thesession.org/tunes/1325#setting1325"}}}
	title = "Dans Plinn Of The Tune Police"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   a'8    e''8    e''8    d''8    cis''8    b'8    
cis''8    a'8  \bar "|"   b'8    cis''8    d''4    b'4    cis''8    a'8  
\bar "|"   b'8    cis''8    d''4    cis''8    b'8    cis''8.    d''16  \bar "|" 
    e''8    fis''8    e''8    d''8    cis''8    b'8    cis''8    a'8  \bar "|"  
 b'8    cis''8    d''4    b'4    cis''8    a'8  \bar "|"   b'8    cis''8    
d''4    cis''8    b'8    cis''4  \repeat volta 2 {     a'16    b'16    cis''8   
 d''8    cis''8    b'8    a'8    g'8    a'8  \bar "|"   b'8    fis''8    e''4   
 d''4    cis''8    a'8  \bar "|"     cis''8    b'8    d''8    cis''8    b'8    
a'8    g'8    a'8  \bar "|"   b'8    d''8    d''8    e''16    d''16    cis''8   
 b'8    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
