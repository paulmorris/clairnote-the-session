\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tøm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/200#setting20801"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/200#setting20801" {"https://thesession.org/tunes/200#setting20801"}}}
	title = "Splendid Isolation"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   \repeat volta 2 {   e'16 (   f'16    g'8  -)   a'8   
 f'8    g'8    d''8    c''8    a'8  \bar "|"   g'16 -.   g'16 -.   g'8 -.   a'8 
   g'8    f'8    g'8    a'8    c''8  \bar "|"   d''8    a'8    c''8    a'8    
g'8    a'8    bes'8    c''8  \bar "|"   d''8    g''8    g''8    f''8    g''4    
g''8    a''8  \bar "|"     bes''8    g''8    a''8    g''8    f''8    d''8    
d''4 ^"~"  \bar "|"   f''4. ^"~"    g''8    f''8    d''8    c''8    a'8  
\bar "|"   g'8    c''8    a'8    g'8    f'8    g'8    a'8    c''8  \bar "|"   
d''8    a'8    c''8    a'8    g'2  }     \repeat volta 2 {   g''4 ^"~"    a''8  
  g''8    f''8    d''8    d''4 ^"~"  \bar "|"   g''8    f''8    d''8    c''8    
a'16 (   bes'16    c''8  -)   d''8    c''8  \bar "|"   a'8    g'8    g'4 ^"~"   
 a'8    c''8    d''8    c''8  \bar "|"   a'8    g'8    g'4 ^"~"    f'4 -.   f'4 
^"~"  \bar "|"     f'4 ^"~"    a'8    f'8    g'8    d''8    c''8    a'8  
\bar "|"   g'16 -.   g'16 -.   g'8 -.   a'8    g'8    f'8    g'8    a'8    c''8 
 \bar "|"   d''8    g''8    g''4 ^"~"    f''8    g''8    a''8    g''8  \bar "|" 
  f''8    d''8    c''8    a'8    g'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
