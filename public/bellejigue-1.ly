\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "graphix"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1494#setting1494"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1494#setting1494" {"https://thesession.org/tunes/1494#setting1494"}}}
	title = "Belle Jigue"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   d'8  \repeat volta 2 {   g'8    fis'8    g'8    b'8   
 a'8    g'8  \bar "|"   c''8    b'8    c''8    e''8    d''8    b'8  \bar "|"   
d''8    b'8    b'8  \grace {    d''8  }   b'8    a'8    g'8  \bar "|"   fis'8   
 d'8    a'8    fis'8    e'8    d'8  \bar "|"     g'8    fis'8    g'8    b'8    
a'8    g'8  \bar "|"   c''8    b'8    c''8    e''8    d''8    b'8  \bar "|"   
d''8    b'8    d''8    a'8    b'8    c''8  } \alternative{{   d''8    b'8    
g'8  \grace {    a'8  }   g'4    d'8  } {   d''8    b'8    g'8  \grace {    a'8 
 }   g'8    e''8    fis''8  \bar "||"     g''8    a''8    g''8  \grace {    
fis''8    g''8  }   fis''8    e''8    d''8  \bar "|"   c''8    b'8    c''8    
e''8    d''8    b'8  \bar "|"   d''8    b'8    b'8  \grace {    d''8  }   b'8   
 a'8    g'8  \bar "|"   fis'8    d'8    a'8    fis'8    e'8    d'8  \bar "|"    
 g''8    a''8    g''8  \grace {    fis''8    g''8  }   fis''8    e''8    d''8  
\bar "|"   c''8    b'8    c''8    e''8    d''8    b'8  \bar "|"   d''8    b'8   
 d''8    a'8    b'8    c''8  \bar "|"   d''8    b'8    g'8  \grace {    a'8  }  
 g'8    e''8    fis''8  \bar "|"     g''8    a''8    g''8  \grace {    fis''8   
 g''8  }   fis''8    e''8    d''8  \bar "|"   c''8    b'8    c''8    e''8    
d''8    b'8  \bar "|"   d''8    b'8    b'8  \grace {    d''8  }   b'8    a'8    
g'8  \bar "|"   fis'8    d'8    a'8    fis'8    e'8    d'8  \bar "|"     g'8    
fis'8    g'8    b'8    a'8    g'8  \bar "|"   c''8    b'8    c''8    e''8    
d''8    b'8  \bar "|"   d''8    b'8    d''8    a'8    b'8    c''8  \bar "|"   
d''8    b'8    g'8  \grace {    a'8  }   g'4    d'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
