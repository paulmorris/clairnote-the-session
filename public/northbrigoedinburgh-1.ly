\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/1435#setting1435"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1435#setting1435" {"https://thesession.org/tunes/1435#setting1435"}}}
	title = "North Brig O' Edinburgh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key b \minor   b'8.    a'16  \bar "|"   b'4    b'8.    fis'16    
b'8.    b'16    d''4  \bar "|"   cis''8.    b'16    a'8.    cis''16    e''16    
d''16    cis''16    b'16    a'8.    cis''16  \bar "|"   b'4    b'8.    fis'16   
 b'8.    b'16    d''4  \bar "|"   d''8.    b'16    cis''16    a'8.    b'4  }    
 b'8.    cis''16  \bar "|"   d''4    d''8.    fis''16    d''8.    d''16    
fis''8.    d''16  \bar "|"   cis''8.    b'16    a'8.    cis''16    e''16    
d''16    cis''16    b'16    a'8.    cis''16  \bar "|"   d''4    d''8.    
fis''16    d''8.    d''16    d''4  \bar "|"   d''8.    b'16    cis''16    a'8.  
  b'4    b'8.    cis''16  \bar "|"     d''4    d''8.    fis''16    d''8.    
d''16    fis''8.    d''16  \bar "|"   cis''8.    b'16    a'8.    cis''16    
e''16    d''16    cis''16    b'16    a'8.    cis''16  \bar "|"   d''4    d''8.  
  b'16    a'8.    cis''16    e''8.    cis''16  \bar "|"   d''8.    b'16    
cis''16    a'8.    b'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
