\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thistledowne"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/750#setting28522"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/750#setting28522" {"https://thesession.org/tunes/750#setting28522"}}}
	title = "A Fig For A Kiss"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \dorian   \repeat volta 2 {     g'4 ^"Em"   b'8    e'4    b'8  
  b'8    a'8    g'8    \bar "|"     fis'4 ^"D"   a'8    d'4    a'8    a'8    
g'8    fis'8    \bar "|"     g'4 ^"Em"   b'8    e'4    b'8    b'8    a'8    g'8 
   \bar "|"     b'8 ^"G"   d''8    b'8      a'8 ^"D"   g'8    fis'8      e'4. 
^"Em"   }       g''4 ^"Em"   e''8    g''4    e''8    e''8    d''8    b'8    
\bar "|"     fis''4 ^"D"   d''8    d''8    cis''8    d''8    fis''8    e''8    
d''8    \bar "|"     g''4 ^"Em"   e''8    g''4    e''8    e''8    d''8    b'8   
 \bar "|"     d''4 ^"G"   b'8    a'8    g'8    b'8    d''8      e''4 ^"C"   
fis''8    \bar "|"       g''4 ^"Em"   e''8    g''4    e''8    e''8    d''8    
b'8    \bar "|"     fis''4 ^"D"   d''8    d''8    cis''8    d''8    fis''8    
e''8    d''8    \bar "|"     g''8 ^"C"   fis''8    e''8      fis''8 ^"D"   e''8 
   d''8      e''8 ^"Em"   d''8    b'8    \bar "|"   b'8    d''8    b'8      a'8 
^"D"   g'8    fis'8      e'4. ^"Em"   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
