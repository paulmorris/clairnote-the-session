\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "shanaway"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1288#setting1288"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1288#setting1288" {"https://thesession.org/tunes/1288#setting1288"}}}
	title = "Abbeyleix, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   \repeat volta 2 {   d'4    e'8    d'8    c'8    a8   
 \tuplet 3/2 {   a8    a8    a8  } \bar "|"   f'8    e'8    f'8    g'8    a'8    
b'8    \tuplet 3/2 {   c''8    b'8    a'8  } \bar "|"   d''4    e''8    d''8    
c''8    a'8    d''8    c''8  \bar "|"   a'8    c''8    g'8    e'8    e'8    d'8 
   d'8    c'8  \bar "|"     d'4    e'8    d'8    c'8    a8    \tuplet 3/2 {   a8 
   a8    a8  } \bar "|"   f'8    e'8    f'8    g'8    a'8    b'8    
\tuplet 3/2 {   c''8    b'8    a'8  } \bar "|"   d''4    e''8    d''8    c''8    
a'8    d''8    c''8  \bar "|"   a'8    c''8    g'8    e'8    e'8    d'8    d'4  
}     \repeat volta 2 {   d''4    f''8    d''8    a''8    d''8    f''8    d''8  
\bar "|"   g''4    f''8    g''8    e''8    a'8    b'8    c''8  \bar "|"   d''8  
  d'8    c''8    d'8    b'8    d'8    a'8    f'8  \bar "|"   g'8    e'8    f'8  
  d'8    e'8    a8    a8    a'8  \bar "|"   d''4    f''8    d''8    a''8    
d''8    f''8    d''8  \bar "|"     g''4    f''8    g''8    e''8    a'8    b'8   
 c''8  \bar "|"   d''8    d'8    c''8    d'8    b'8    d'8    a'8    f'8  
\bar "|"   g'8    b'8    a'8    g'8    f'8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
