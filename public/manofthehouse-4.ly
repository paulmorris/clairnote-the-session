\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/222#setting12907"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/222#setting12907" {"https://thesession.org/tunes/222#setting12907"}}}
	title = "Man Of The House, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   e'4    b'8    e'8    g'8    a'8  
  b'8    g'8    \bar "|"   e'16    e'16    e'8    b'8    g'8    fis'8    d'8    
a'8    fis'8    \bar "|"   e'4    b'8    e'8    g'8    a'8    b'8    e''8    
} \alternative{{   d''8    b'8    a'8    fis'8    b'8    a'8    g'8    fis'8    
} {   d''8    b'8    a'8    fis'8    b'8    e'8    e'8    b'8    \bar "||"   
\bar "|"   e''8    fis''8    g''8    e''8    fis''8    g''8    a''8    fis''8   
 \bar "|"   g''8    fis''8    e''8    d''8    e''8    d''8    b'8    d''8    
\bar "|"   e''8    fis''8    g''16    fis''16    e''8    fis''8    g''8    a''8 
   fis''8    \bar "|"   g''8    fis''8    e''8    d''8    b'8    e''8    e''8   
 d''8    \bar "|"   \bar "|"   e''8    fis''8    g''8    e''8    fis''8    g''8 
   a''8    fis''8    \bar "|"   g''8    fis''8    e''8    d''8    e''8    
fis''8    g''8    a''8    \bar "|"   b''8    g''8    a''16    g''16    fis''8   
 g''8    fis''8    e''8    d''8    \bar "|"   e''8    d''8    b'8    a'8    g'8 
   e'8    fis'8    d'8    \bar "||"   \bar "|"   e''8    fis''8    g''8    e''8 
   fis''8    g''8    a''8    fis''8    \bar "|"   g''8    fis''8    e''8    
d''8    e''8    d''8    b'16    cis''16    d''8    \bar "|"   e''8    fis''8    
g''8    e''8    fis''8    g''8    a''8    fis''8    \bar "|"   g''8    fis''8   
 e''8    d''8    b'8    e''8    e''16    e''16    e''8    \bar "|"   \bar "|"   
e''8    fis''8    g''8    e''8    fis''8    a''8  \grace {    b''8  }   a''8    
fis''8    \bar "|"   g''8    fis''8    e''8    d''8    e''8    fis''8    g''16  
  g''16    a''8    \bar "|"   b''8    g''8  \grace {    a''8  }   g''8    
fis''8    g''8    fis''8    e''8    d''8    \bar "|"   b'16    cis''16    d''8  
  b'8    a'8    g'8    e'8    fis'8    d'8    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
