\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1053#setting6843"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1053#setting6843" {"https://thesession.org/tunes/1053#setting6843"}}}
	title = "Kitty McGee"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   d''8    \bar "|"   d''4    a'8    
a'8    fis'8    a'8    \bar "|"   d''4.    e''8    fis''8    g''8    \bar "|"   
fis''4    d''8    e''4    d''8    \bar "|"   cis''4    a'8    a'8    b'8    
cis''8    \bar "|"     d''4    a'8    a'8    fis'8    a'8    \bar "|"   d''4.   
 e''8    fis''8    g''8    \bar "|"   fis''4.    g''8    e''8    cis''8    
\bar "|"   d''8    e''8    d''8    d''4    }     \repeat volta 2 {   a'8    
\bar "|"   d''4    fis''8    a''4    fis''8    \bar "|"   e''4    fis''8    
g''4.    \bar "|"   fis''4    d''8    e''4    d''8    \bar "|"   cis''4    a'8  
  a'8    fis''8    g''8    \bar "|"     a''4.    g''4    fis''8    \bar "|"   
e''4    fis''8    g''4.    \bar "|"   fis''4    d''8    e''4    cis''8    
\bar "|"   d''8    e''8    d''8    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
