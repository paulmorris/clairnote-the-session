\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fynnjamin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1169#setting14434"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1169#setting14434" {"https://thesession.org/tunes/1169#setting14434"}}}
	title = "I'll Buy Boots For Maggie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   a8.    b16    c'8    d'8    
\bar "|"   e'8    g'8    g'8    a'8    \bar "|"   e'8    g'16    e'16    d'8    
b8    \bar "|"   a8    b8    g8    b8    \bar "|"   a8.    b16    c'8    d'8    
\bar "|"   e'8    g'8    g'8    a'8    \bar "|"   e'8    g'16    e'16    d'8    
b8    \bar "|"   b8    a8    a4    }   \repeat volta 2 {   e'4    a'8    b'8    
\bar "|"   c''8    b'8    a'8    b'8    \bar "|"   f'4    a'8    b'8    
\bar "|"   c''8    b'8    a'8    b'8    \bar "|"   fis'!4    a'8    b'8    
\bar "|"   c''8    b'8    a'8    b'8    \bar "|"   e'8    g'16    e'16    d'8   
 b8    \bar "|"   b8    a8    a4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
