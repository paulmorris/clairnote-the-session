\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SPeak"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/914#setting914"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/914#setting914" {"https://thesession.org/tunes/914#setting914"}}}
	title = "Dance Of The Welsh Vicar"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key a \minor   a''4    a'8    a'4    g''8    a'4    f''8  \bar "|"   
e''4.   ~    e''4    a'8    a'4    a'8  \bar "|"   b'4    c''8    d''4    b'8   
 g'4    a'8  \bar "|"   b'4    g'8    e'4    f'8    g'4    e'8  \bar "|"     
a'4    a''8    a'4    g''8    a'4    f''8  \bar "|"   e''4.   ~    e''4    a'8  
  a'4    a'8  \bar "|"   b'4    c''8    d''4    b'8    g'4    a'8  \bar "|"   
b'8    c''8    d''8    c''4    b'8    a'4    a'8  }     \repeat volta 2 {   b'8 
   c''8    c''8    a'4    c''8    g'4    c''8  \bar "|"   f'4    g'8    e'8    
g'8    c''8    e''4    c''8  \bar "|"   d''4    e''8    f''4    d''8    b'4    
c''8  \bar "|"   d''4    b'8    g'4    a'8    b'4    g'8  \bar "|"     b'8    
c''8    c''8    a'4    c''8    g'4    c''8  \bar "|"   f'4    g'8    e'8    g'8 
   c''8    e''4    c''8  \bar "|"   d''4    e''8    f''4    d''8    b'4    d''8 
 \bar "|"   c''8    d''8    e''8    f''8    e''8    d''8    c''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
