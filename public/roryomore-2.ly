\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1189#setting14465"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1189#setting14465" {"https://thesession.org/tunes/1189#setting14465"}}}
	title = "Rory O'More"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key f \major   d''8  \bar "|"   c''8    a'8    f'8    a'8    f'8    
f'8  \bar "|"   a'8    f'8    f'8    f'4    d''8  \bar "|"   c''8    a'8    f'8 
   a'8    g'8    f'8  \bar "|"   e'8    g'8    g'8    g'4    d''8  \bar "|"   
c''8    a'8    f'8    a'8    g'8    f'8  \bar "|"   a'8    bes'8    c''8    
d''4    c''16    bes'16  \bar "|"   a'8    bes'8    c''8    c''8    d''8    
e''8  \bar "|"   f''8    f'8    f'8    f'4  \bar "||"   f''8  \bar "|"   f''8   
 e''8    d''8    d''8    c''8    a'8  \bar "|"   bes'8    a'8    g'8    f'4    
e'8  \bar "|"   d'8    e'8    f'8    f'8    g'8    a'8  \bar "|"   bes'8    
d''8    c''8    c''4    f''8  \bar "|"   f''8    e''8    d''8    d''8    c''8   
 a'8  \bar "|"   bes'8    c''8    d''8    d''4    c''8  \bar "|"   a'8    bes'8 
   c''8    c''8    d''8    e''8  \bar "|"   f''8    f'8    f'8    f'4  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
