\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kuddel"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1136#setting1136"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1136#setting1136" {"https://thesession.org/tunes/1136#setting1136"}}}
	title = "Trip To Lerwick, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   d''4    d''8    e''8    d''8    
b'8  \bar "|"   d''4    b'8    a'8    b'8    g'8  \bar "|"   g'8    d''8 -.   
d''8 -.   e''8    d''8    b'8  \bar "|"   g'8    a'8    b'8    d'8    e'8    
fis'8  \bar "|"     g'8    fis'8    g'8    a'8    g'8    a'8  \bar "|"   b'8    
a'8    b'8    e''8 (   g''4  -) \bar "|"   e''8    d''8    b'8    g'8    a'8    
b'8  \bar "|"   a'8    g'8    e'8    g'4.  }     \repeat volta 2 {   |
 d''8    g''8    c'''8    b''8    a''8    g''8  \bar "|"   d''8    e''8    g''8 
   b'8    a'8    g'8  \bar "|"   d''8    g''8    c'''8    b''8    a''8    g''8  
\bar "|"   e''8    d''8    e''8    g''4.  \bar "|"     d'8    fis'8    a'8    
g'8    b'8    d''8  \bar "|"   a'8    cis''8    e''8    b'8    dis''8    fis''8 
 \bar "|"   e''8    g''8    c'''8    b''8    a''8    g''8  \bar "|"   d''8    
e''8    fis''8    g''4.  }     \repeat volta 2 {   |
 b''8    a''8    g''8    e''8    d''8    e''8  \bar "|"   c'''8    b''8    g''8 
   e''8    d''8    e''8  \bar "|"   b'8    d''8    fis''8    a''8    b''8    
a''8  \bar "|"   g''8    fis''8    e''8    b'4.  \bar "|"     b''8    a''8    
g''8    e''8    d''8    e''8  \bar "|"   c'''8    b''8    g''8    e''8    d''8  
  e''8  \bar "|"   d'8    fis'8    a'8    g'8    b'8    d''8  \bar "|"   e''8   
 d''8    b'8    g''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
