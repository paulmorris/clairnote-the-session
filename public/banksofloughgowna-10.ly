\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "benhockenberry"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/775#setting28975"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/775#setting28975" {"https://thesession.org/tunes/775#setting28975"}}}
	title = "Banks Of Lough Gowna, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key b \minor   \repeat volta 2 {   b'4. ^"~"    b'8    a'8    fis'8  
\bar "|"   a'4. ^"~"    a'8    fis'8    a'8  \bar "|"   b'4. ^"~"    b'8    a'8 
   b'8  \bar "|"   d''4    e''8    fis''8    e''8    fis''8  \bar "|"     b'4. 
^"~"    b'8    a'8    fis'8  \bar "|"   a'4. ^"~"    a'8    fis'8    a'8  
\bar "|"   d''8    e''8    fis''8    a''8    fis''8    e''8  \bar "|"   fis''8  
  d''8    b'8    b'4. ^"~"  }     \repeat volta 2 {   d''8    e''8    fis''8    
a''4. ^"~"  \bar "|"   b''8    a''8    b''8    a''8    fis''8    e''8  \bar "|" 
  d''8    e''8    fis''8    a''4. ^"~"  \bar "|"   b''8    a''8    fis''8    
e''8    d''8    b'8  \bar "|"     d''8    e''8    fis''8    a''4. ^"~"  
\bar "|"   b''8    a''8    b''8    a''8    fis''8    e''8  \bar "|"   fis''4. 
^"~"    fis''8    e''8    fis''8  \bar "|"   d''8    b'8    a'8    b'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
