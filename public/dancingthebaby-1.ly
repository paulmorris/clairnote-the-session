\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1088#setting1088"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1088#setting1088" {"https://thesession.org/tunes/1088#setting1088"}}}
	title = "Dancing The Baby"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   fis'8    a'16    a'16    g'8    a'16    a'16    fis'8 
   a'16    a'16    g'16    fis'16    e'8  \bar "|"   fis'8    a'16    a'16    
g'8    a'16    a'16    d'8    d'16    e'16    fis'16    e'16    d'8  \bar "|"   
  fis'8    a'16    a'16    g'8    a'16    a'16    fis'8    a'16    a'16    g'16 
   fis'16    e'8  \bar "|"   d'8    d'16    fis'16    e'8    e'16    e'16    
d'8    d'16    d'16    d'8    d'16    d'16  }     \repeat volta 2 {   d''8    
fis'16    fis'16    a'8    fis'16    fis'16    d''8    fis'16    fis'16    g'16 
   fis'16    e'8  \bar "|"   d''8    fis'16    fis'16    a'8    fis'16    
fis'16    d'8    d'16    e'16    fis'16    e'16    d'8  \bar "|"     d''8    
fis'16    fis'16    a'8    fis'16    fis'16    d''8    fis'16    fis'16    g'16 
   fis'16    e'8  \bar "|"   d'8    d'16    fis'16    e'8    e'16    e'16    
d'8    d'16    d'16    d'8    d'16    d'16  }     \repeat volta 2 {   fis''8    
a''16    a''16    g''8    a''16    a''16    fis''8    a''16    a''16    g''16   
 fis''16    e''8  \bar "|"   fis''8    a''16    a''16    g''16    b''16    
a''16    g''16    fis''16    e''16    d''8    cis''16    a'16    a'8  \bar "|"  
   fis''8    a''16    a''16    g''8    a''16    a''16    fis''8    a''16    
a''16    g''16    fis''16    e''8  \bar "|"   d''8    fis''16    d''16    e''16 
   cis''16    a'8    d''16    d''16    d''8    d''16    d''16    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
