\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "donnchad"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/685#setting13733"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/685#setting13733" {"https://thesession.org/tunes/685#setting13733"}}}
	title = "King's Fancy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key c \major   \repeat volta 2 {   c''16    d''16  \bar "|"   e''8   
 d''8    c''8    c''8    b'8    c''8  \bar "|"   a'8    c''8    f''8    g'8    
c''8    e''8  \bar "|"   f'8    a'8    d''8    e'8    g'8    c''8  \bar "|"   
d''8    b'8    g'8    a''8    g''8    f''8  \bar "|"   e''8    d''8    c''8    
c''8    b'8    c''8  \bar "|"   a'8    c''8    f''8    g'8    c''8    e''8  
\bar "|"   f'8    a'8    d''8    e'8    g'8    c''8  \bar "|"   d''8    b'8    
g'8    c''4  }   \repeat volta 2 {   c''16    d''16  \bar "|"   e''8    c''8    
a'8    a'8    gis'8    a'8  \bar "|"   e'8    a'8    c''8    e''8    c''8    
a'8  \bar "|"   b'8    gis'8    e'8    d''8    b'8    gis'8  \bar "|"   e''8    
f''8    e''8    d''8    c''8    b'8  \bar "|"   e''8    c''8    a'8    a'8    
gis'8    a'8  \bar "|"   e'8    a'8    c''8    e''8    c''8    a'8  \bar "|"   
b'8    gis'8    e'8    e''8    f''8    e''8  \bar "|"   d''8    c''8    b'8    
a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
