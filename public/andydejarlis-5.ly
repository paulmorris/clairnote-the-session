\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/838#setting22117"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/838#setting22117" {"https://thesession.org/tunes/838#setting22117"}}}
	title = "Andy De Jarlis'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \major   fis'8    \bar "|"     e'8 ^"E"   gis'8    b'8    e'8  
  gis'8    b'8    \bar "|"     e'8 ^"E"   gis'8    b'8      cis''4 ^"A"   b'8   
 \bar "|"     e'8 ^"E"   gis'8    b'8    e'8    gis'8    b'8    \bar "|"     
d'8 ^"D"   fis'8    a'8      b'4 ^"Bm"   a'8    \bar "|"       e'8 ^"E"   gis'8 
   b'8    e'8    gis'8    b'8    \bar "|"     e'8 ^"E"   gis'8    b'8      
cis''4 ^"A"   a''8    \bar "|"     gis''4 ^"E"   fis''8      b'8 ^"B"   cis''8  
  dis''8    \bar "|"     e''4. ^"E"   e''4    }     \repeat volta 2 {   a''8    
\bar "|"     gis''8 ^"E"   b''8    gis''8    e''8    fis''8    gis''8    
\bar "|"     a''8 ^"A"   b''8    a''8    cis''4    e''8    \bar "|"     fis''4 
^"F\#m"   gis''8    fis''4    e''8    \bar "|"     dis''8 ^"B"   cis''8    b'8  
    cis''4 ^"A"   b'8    \bar "|"       gis''8 ^"E"   b''8    gis''8    e''8    
fis''8    gis''8    \bar "|"     a''8 ^"A"   b''8    a''8    cis''4    a''8    
\bar "|"     gis''4 ^"E"   fis''8      b'8 ^"B"   cis''8    dis''8    \bar "|"  
   e''4. ^"E"   e''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
