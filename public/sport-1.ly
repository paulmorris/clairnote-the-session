\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/870#setting870"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/870#setting870" {"https://thesession.org/tunes/870#setting870"}}}
	title = "Spórt"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   a'8    d''8    fis'8    a'4. ^"~"    \bar "|"   g'8   
 b'8    e'8    g'8    fis'8    g'8    \bar "|"   a'8    d''8    fis'8    a'4. 
^"~"    \bar "|"   g'8    b'8    e'8    cis''8    d''8    e''8    \bar "|"     
a'8    d''8    fis'8    a'4. ^"~"    \bar "|"   g'8    b'8    e'8    g'8    
fis'8    g'8    \bar "|"   cis''8    d''8    cis''8    a'4    g'8    \bar "|"   
e'8    a'8    a'8    d'4    fis'8    \bar ":|."   e'8    a'8    a'8    d'4    
\bar "||"     \bar ".|:"   d''8    \bar "|"   cis''4. ^"~"    d''4. ^"~"    
\bar "|"   cis''8    e''8    cis''8    a'4    g''8    \bar "|"   fis''8    e''8 
   d''8    g''8    e''8    d''8    \bar "|"   cis''8    e''8    cis''8    a'4   
 e''8    \bar "|"     fis''4. ^"~"    g''4. ^"~"    \bar "|"   a''8    g''8    
e''8    d''4    e''8    \bar "|"   fis''8    d''8    fis'8    a'4    fis'8    
\bar "|"   g'8    e'8    a'8    d'4    \bar ":|."   r8   \bar "|"     \bar ".|:" 
  a'8    g'8    g'8    a'8    g'8    e'8    \bar "|"   a'8    g'8    e'8    e'8 
   d'8    d'8    \bar "|"   a'8    g'8    e'8    cis''8    d''8    e''8    
\bar "|"   d''8    cis''8    a'8    g'4. ^"~"    \bar "|"     a'8    g'8    g'8 
   a'8    g'8    e'8    \bar "|"   a'8    g'8    e'8    e'8    d'8    d'8    
\bar "|"   cis''8    d''8    cis''8    a'4    g'8    \bar "|"   e'8    a'8    
a'8    d'4.    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
