\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "radriano"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/488#setting488"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/488#setting488" {"https://thesession.org/tunes/488#setting488"}}}
	title = "Graine's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   b'8    \bar "|"   d''8    g'8    fis'8    g'8    d'8  
  b8    \bar "|"   g8    a8    g8    d'8    b8    d'8    \bar "|"   e'8    c''8 
   b'8    a'8    b'8    a'8    \bar "|"   fis'8    e'8    d'8    c''8    b'8    
a'8    \bar "|"     b'8    cis''8    d''8    b'8    g'4.    \bar "|"   e'8    
fis'8    g'8    e'8    c'8    e'8    g'8    \bar "|"   d'8    g'8    c''8    
b'8    d'8    fis'8    \bar "|"   a'8    g'8    fis'8    g'4    }     
\repeat volta 2 {   a'8    \bar "|"   b'4. ^"~"    g'4. ^"~"    \bar "|"   e''8 
   b'8    c''8    a'8    d'8    e'8    \bar "|"   fis'4. ^"~"    fis''4. ^"~"   
 \bar "|"   e''8    d''8    c''8    d''4    c''8    \bar "|"   b'8    c''8    
d''8    b'8    g'8    g''8    fis''8    \bar "|"     e''8    c''8    a'8    
e'4.    \bar "|"   d'8    b'8    c''8    b'8    d'8    fis'8    \bar "|"   a'8  
  g'8    fis'8    g'4    } \repeat volta 2 {   d'8    \bar "|"   g'8    b'8    
d''8    g''4. ^"~"    \bar "|"   e''8    d''8    b'8    c''8    b'8    a'8    
\bar "|"     b'8    c''8    d''8    b'8    a''4    g''8    \bar "|"   fis''8    
d''8    c''8    d''4    fis''8    \bar "|"   g''8    d''8    b'8    g'8    g''8 
   fis''8    \bar "|"   e''8    c''8    a'8    e'4. ^"~"    \bar "|"   d'8    
g'8    c''8    b'8    d'8    fis'8    \bar "|"   a'8    g'8    fis'8    g'4    
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
