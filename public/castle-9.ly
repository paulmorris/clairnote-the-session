\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/273#setting25649"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/273#setting25649" {"https://thesession.org/tunes/273#setting25649"}}}
	title = "Castle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {   b'8  \bar "|"   c''8    b'8    
a'8    b'8    a'8    g'8  \bar "|"   a'8    g'8    e'8    d'8    b8    g8  
\bar "|"   a4. ^"~"    e'8    d'8    b8  \bar "|"   d'8    e'8    g'8    a'8    
g'8    e'8  \bar "|"     c''4    a'8    b'16    c''16    d''8    b'8  \bar "|"  
 a'8    g'8    e'8    d'8    b8    g8  \bar "|"   a4. ^"~"    e'8    cis'8    
e'8  \bar "|"   d'8    b8    g8    a4  }     \repeat volta 2 {   b'8  \bar "|"  
 c''8    b'8    a'8    a''4. ^"~"  \bar "|"   b''8    a''8    g''8    e''8    
d''8    b'8  \bar "|"   g'8    b'8    d''8    g''4. ^"~"  \bar "|"   g8    b8   
 d'8    g'8    b'8    g'8  \bar "|"     c''8    b'8    c''8    d''8    cis''8   
 d''8  \bar "|"   e''8    d''8    e''8    g''8    a''8    b''8  \bar "|"   a''8 
   g''8    e''8    d''8    b'8    g'8  \bar "|"   e'8    fis'!8    g'8    a'4  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
