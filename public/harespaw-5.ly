\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Phantom Button"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1462#setting14855"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1462#setting14855" {"https://thesession.org/tunes/1462#setting14855"}}}
	title = "Hare's Paw, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'4. ^"~"    a'8    b'8    e'8    e'4 ^"~"  \bar "|"  
 d''8    b'8    a'8    c''8    b'8    e'8    e'4 ^"~"  \bar "|"   g'4. ^"~"    
a'8    b'8    a'8    b'8    d''8  \bar "|"   e''8    g''8    fis''8    g''8    
e''8    d''8    b'8    a'8  \bar "|"   g'4. ^"~"    a'8    b'8    e'8    e'4 
^"~"  \bar "|"   d''8    b'8    a'8    c''8    b'8    e'8    e'4 ^"~"  \bar "|" 
  g'4. ^"~"    a'8    b'8    a'8    b'8    d''8  \bar "|"   e''8    g''8    
fis''8    g''8    e''8    d''8    b'16    c''16    d''8  \bar "||"   g''4    
a''8    fis''8    g''8    fis''8    e''8    d''8  \bar "|"   b'16    c''16    
d''8    g''8    d''8    b'16    c''16    d''8    e''8    fis''8  \bar "|"   
g''4    a''8    fis''8    g''8    fis''8    e''8    d''8  \bar "|"   b'16    
c''16    d''8    e''8    fis''8    e''4.    fis''8  \bar "|"   g''4    a''8    
fis''8    g''8    fis''8    e''8    d''8  \bar "|"   b'16    c''16    d''8    
g''8    d''8    b'16    c''16    d''8    g''8    a''8  \bar "|"   b''8    g''8  
  a''8    fis''8    g''8    fis''8    e''8    d''8  \bar "|"   e''8    g''8    
fis''8    g''8    e''8    d''8    b'8    a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
