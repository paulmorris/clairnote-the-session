\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Conán McDonnell"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1300#setting1300"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1300#setting1300" {"https://thesession.org/tunes/1300#setting1300"}}}
	title = "Reel De L'aveugle"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   fis''8    d''8    d''16    d''16    d''8   
 e''8    d''8    b'8    d''8  \bar "|"   a'8    d''8    fis''8    a''8    b''8  
  a''8    fis''8    d''8  \bar "|"   cis''8    e''8    a''8    b''8    cis'''8  
  b''8    a''8    g''8  \bar "|"   fis''8    a''8    d'''8    e'''8    fis'''8  
  d'''8    a''8    g''8  \bar "|"     \bar "|"   fis''8    fis''8 ^"~"    
fis''8    d''8    e''8    d''8    b'8    d''8  \bar "|"   a'8    d''8    fis''8 
   a''8    b''8    a''8    fis''8    d''8  \bar "|"   cis''8    e''8    a''8    
b''8    cis'''8    b''8    a''8    fis''8  \bar "|"   d''8    d'''8    a''8    
fis''8    d''8    b''8    a''8    g''8  \bar "|"     \bar "|"   fis''8    d''8  
  d''16    d''16    d''8    e''8    d''8    b'8    d''8  \bar "|"   a'8    d''8 
   fis''8    a''8    b''8    a''8    fis''8    d''8  \bar "|"   cis''8    e''8  
  a''8    b''8    cis'''8    b''8    a''8    g''8  \bar "|"   fis''8    a''8    
d'''8    e'''8    fis'''8    d'''8    a''8    g''8  \bar "|"     \bar "|"   
fis''8    fis''8 ^"~"    fis''8    d''8    e''8    d''8    b'8    d''8  
\bar "|"   a'8    d''8    fis''8    a''8    b''8    a''8    fis''8    d''8  
\bar "|"   cis''8    e''8    a''8    b''8    cis'''8    b''8    a''8    fis''8  
\bar "|"   d''8    d'''8    a''8    fis''8    d''8    r8 d'''8    e'''8  
\bar "|"     \bar "|"   fis'''8    d'''8    b''16    a''16    fis''8    d'''8   
 a''8    fis''8    a''8  \bar "|"   b''8    a''8    gis''8    b''8    e'''8    
d'''8    cis'''8    b''8  \bar "|"   a''8    cis'''8    d'''8    e'''8    
fis'''8 ^"~"    fis'''8    g'''8    fis'''8  \bar "|"   fis'''8    e'''8    
dis'''8    fis'''8    e'''8    a'''8    aes'''8    g'''8  \bar "|"     \bar "|" 
  fis'''8    d'''8    a''8    fis''8    d'''8    a''8    fis''8    a''8  
\bar "|"   b''8    a''8    gis''8    b''8    e'''8    d'''8    cis'''8    b''8  
\bar "|"   a''8    fis'''8    e'''8    d'''8    cis'''8    a''8    g''8    e''8 
 \bar "|"   d''8    d'''8    a''8    fis''8    d''4    d'''8    e'''8  \bar "|" 
    \bar "|"   fis'''8    d'''8    a''8    fis''8    d'''8    a''8    fis''8    
a''8  \bar "|"   b''8 ^"~"    b''8    gis''8    b''8    e'''8    d'''8    
cis'''8    b''8  \bar "|"   a''8    cis'''8    d'''8    e'''8    fis'''8 ^"~"   
 fis'''8    g'''8    fis'''8  \bar "|"   fis'''8    e'''8    dis'''8    fis'''8 
   e'''8    a'''8    aes'''8    g'''8  \bar "|"     \bar "|"   fis'''8    d'''8 
   a''8    fis''8    d'''8    a''8    fis''8    a''8  \bar "|"   a''8    b''8   
 gis''8    b''8    e'''8    d'''8    cis'''8    b''8  \bar "|"   a''8    
fis'''8    e'''8    d'''8    cis'''8    a''8    g''8    e''8  \bar "|"   d''8   
 d'''8    a''8    fis''8    d''4    r4 \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
