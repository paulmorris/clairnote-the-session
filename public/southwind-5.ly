\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/601#setting21888"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/601#setting21888" {"https://thesession.org/tunes/601#setting21888"}}}
	title = "South Wind, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   \repeat volta 2 {   c''4    \bar "|"   b'4.    a'8    
g'4    \bar "|"   b'4    c''4    d''4    \bar "|"   a'4.    b'8    a'8    gis'8 
   \bar "|"   a'2    d''8    c''8    \bar "|"     b'4    b'8    a'8    g'4    
\bar "|"   e'4.    d'8    e'4    \bar "|"   g'2.   ~    \bar "|"   g'2    }     
d''4    \bar "|"   g''4.    a''8    g''8    fis''8    \bar "|"   g''4    
fis''16    g''16    fis''8    e''4    \bar "|"   d''4.    e''8    d''4    
\bar "|"   d''2    c''4    \bar "|"     b'4.    a'8    g'4    \bar "|"   b'4    
c''4    d''4    \bar "|"   a'4.    b'8    a'4    \bar "|"   a'2    d''16    
e''16    fis''8    \bar "|"     g''4    g''8    a''8    g''8    fis''8    
\bar "|"   g''4    fis''4    e''4    \bar "|"   d''4.    e''8    d''4    
\bar "|"   d''2    c''8    d''8    \bar "|"     b'4.    d''8    g'4    \bar "|" 
  a'8    c''8   ~    c''4    fis'4    \bar "|"   g'4.    a'8    g'8    fis'8    
\bar "|"   g'2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
