\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Cheeky Elf"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/896#setting21245"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/896#setting21245" {"https://thesession.org/tunes/896#setting21245"}}}
	title = "Lakes Of Ponchartrain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   \repeat volta 2 {   d'4  \bar "|"   g'2    e''4  
\bar "|"   d''2    e''8    d''8  \bar "|"   b'4    a'4    d''4  \bar "|"   b'2  
  a'4  \bar "|"     g'8    e'4.    fis'4  \bar "|"   g'2    g'4  \bar "|"   
g'2. (   \bar "|"   g'2  -)   d''4  \bar "|"     d''2    b'4  \bar "|"   d''4   
 e''4    fis''4  \bar "|"   g''2    g''4  \bar "|"   fis''4    e''4    d''4  
\bar "|"     b'2    a'4  \bar "|"   b'4    c''4    d''4  \bar "|"   e''2. (   
\bar "|"   e''2  -)   e''4  \bar "|"     d''2    b'4  \bar "|"   d''4    e''4   
 fis''4  \bar "|"   g''2    g''4  \bar "|"   fis''4    e''4    d''4  \bar "|"   
  b'2    a'4  \bar "|"   b'4    c''4    d''4  \bar "|"   e''2. (   \bar "|"   
e''2  -)   g'4  \bar "|"     g'2    e''4  \bar "|"   d''2    e''8    d''8  
\bar "|"   b'4    a'4    d''4  \bar "|"   b'2    a'4  \bar "|"     g'8    e'4.  
  fis'4  \bar "|"   g'2    g'4  \bar "|"   g'2. (   \bar "|"   g'2  -)   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
