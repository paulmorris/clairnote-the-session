\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1033#setting23166"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1033#setting23166" {"https://thesession.org/tunes/1033#setting23166"}}}
	title = "Gale, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \repeat volta 2 {   a'8 ^"Am"   e'8    e'4      a'4 
^"Am"   a'8    b'8  \bar "|"   c''8 ^"Am"   b'8    c''8    e''8      a''2 ^"Am" 
\bar "|"   f''8 ^"Dm"   d''8    d''4      e''8 ^"Am"   c''8    c''4  \bar "|"   
b'8 ^"E"   a'8    b'8    c''8      b'4 ^"E"   gis''4  \bar "|"       a''8 ^"Am" 
  e''8    e''4      a''4 ^"Am"   a''8    b''8  \bar "|"   a''8 ^"Am"   g''8    
e''8    d''8      c''4 ^"Am"   c''4  \bar "|"   b'8 ^"E"   c''8    b'8    c''8  
    b'8 ^"E"   e''8    b'8    e''8  \bar "|"   b'8 ^"E"   gis''8    b'8    
gis''8      a''2 ^"Am" }     \repeat volta 2 {   a''8 ^"A"   e''8    cis''8    
e''8      a''8 ^"A"   e''8    cis''8    e''8  \bar "|"   a''8 ^"Dm"   f''8    
d''8    f''8      a''8 ^"Dm"   f''8    d''8    f''8  \bar "|"   g''8 ^"G"   
d''8    b'8    d''8      g''8 ^"G"   d''8    b'8    d''8  \bar "|"   g''8 ^"C"  
 e''8    c''8    e''8      g''8 ^"C"   e''8    c''8    e''8  \bar "|"       
f''8 ^"Dm"   d''8    a'8    d''8      f''8 ^"Dm"   d''8    a'8    d''8  
\bar "|"   e''8 ^"Am"   c''8    a'8    c''8      e''8 ^"Am"   c''8    a'8    
c''8  \bar "|"   b'8 ^"E"   c''8    b'8    c''8      b'8 ^"E"   e''8    b'8    
e''8  \bar "|"   b'8 ^"E"   gis''8    b'8    gis''8      a''2 ^"Am" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
