\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1073#setting14301"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1073#setting14301" {"https://thesession.org/tunes/1073#setting14301"}}}
	title = "Muriel's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   \bar "|"   fis'4    d''4    d''4  \bar "|"   cis''4   
 d''4    e''4  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
