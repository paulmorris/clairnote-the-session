\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Alastair Wilson"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1129#setting1129"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1129#setting1129" {"https://thesession.org/tunes/1129#setting1129"}}}
	title = "Rose Of Aranmore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/4 \key d \major   a'8    fis'8  \bar "|"   d'2    fis'4  \bar "|"   a'4 
   fis'4    d'4  \bar "|"   g'2    b'4  \bar "|"   d''4    cis''4    b'4  
\bar "|"     a'2    b'4  \bar "|"   a'4    fis'4    d'4  \bar "|"   e'2.     ~  
  \bar "|"   e'4    a'4    fis'4  \bar "|"     d'2    fis'4  \bar "|"   a'4    
fis'4    d'4  \bar "|"   g'2    b'4  \bar "|"   d''4    cis''4    b'4  \bar "|" 
    a'2    fis'4  \bar "|"   g'4    fis'4    e'4  \bar "|"   d'2.     ~    
\bar "|"   d'2    d'4  \bar "||"     g'2    b'4  \bar "|"   d''4    cis''4    
b'4  \bar "|"   a'2    b'4  \bar "|"   a'4    fis'4    d'4  \bar "|"     g'2    
b'4  \bar "|"   a'2    fis'4  \bar "|"   e'2.     ~    \bar "|"   e'4    a'4    
fis'4  \bar "|"     d'2    fis'4  \bar "|"   a'4    fis'4    d'4  \bar "|"   
g'2    b'4  \bar "|"   d''4    cis''4    b'4  \bar "|"     a'2    fis'4  
\bar "|"   g'4    fis'4    e'4  \bar "|"   d'2.     ~    \bar "|"   d'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
