\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/675#setting13722"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/675#setting13722" {"https://thesession.org/tunes/675#setting13722"}}}
	title = "Paddy Canny's Toast"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian 
%  Variations Bar 6: |dcAG FDCF| or |dDcD BAFC|
% Variation Last Bar: |2 BGAF DEFE||
   d'8    g'8    g'8    a'8    d''8    d'8    c''8    d'8  \bar "|"   d''8    
d'8    c''8    d'8    a'8    g'8    f'8    c'8  \bar "|"   d'8    g'8    g'8    
a'8    d''8    d'8    c''8    d'8  \bar "|"   d''8    c''8    a'8    f'8    a'8 
   g'8    g'8    f'8  \bar "|"   d'8    g'8    g'8    a'8    d''4 ^"~"    c''8  
  a'8  \bar "|"   d''4 ^"~"    c''8    a'8    g'8    f'8    d'8    c'8  
\bar "|"   d'8    g'8    g'8    a'8    d''8    d'8    c''8    d'8  \bar "|"   
d''8    c''8    a'8    f'8    a'8    g'8    g'8    a'8  \bar "||"   
\repeat volta 2 {   bes'4. ^"~"    c''8    d''8    f''8    f''4 ^"~"  \bar "|"  
 g''4. ^"~"    e''8    f''8    e''8    d''8    c''8  \bar "|"   bes'4. ^"~"    
c''8    d''8    f''8    f''4 ^"~"  \bar "|"   g''8    f''8    d''8    bes'8    
c''8    ees''8    d''8    c''8  \bar "|"   bes'4. ^"~"    c''8    d''8    f''8  
  f''4 ^"~"  \bar "|"   g''8    a''8    g''8    d''8    f''4 ^"~"    d''8    
f''8  \bar "|"   g''8    f''8    d''8    c''8    bes'8    d''8    c''8    d''8  
} \alternative{{   bes'8    g'8    a'8    f'8    d'8    g'8    g'8    a'8  } {  
 bes'8    g'8    a'8    f'8    d'8    g'8    g'8    f'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
