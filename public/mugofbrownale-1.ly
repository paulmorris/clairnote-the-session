\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/888#setting888"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/888#setting888" {"https://thesession.org/tunes/888#setting888"}}}
	title = "Mug Of Brown Ale, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   g''16    fis''16  \repeat volta 2 {   e''8    a'8    
a'8    fis''8    a'8    a'8  \bar "|"   g''4. ^"~"    a''8    g''8    e''8  
\bar "|"   d''8    b'8    g'8    g'8    a'8    g'8  \bar "|"   b'4. ^"~"    b'8 
   c''8    d''8  \bar "|"     e''8    a'8    a'8    fis''8    a'8    a'8  
\bar "|"   g''4. ^"~"    a''8    g''8    e''8  \bar "|"   d''8    b'8    d''8   
 g''8    d''8    b'8  \bar "|"   a'8    b'8    a'8    a'8    b'8    d''8  }     
e''8    fis''8    g''8    a''4. ^"~"  \bar "|"   a''8    b''8    a''8    a''8   
 g''8    e''8  \bar "|"   d''8    b'8    d''8    g''4. ^"~"  \bar "|"   g''8    
b''8    a''8    g''8    e''8    d''8  \bar "|"     e''8    fis''8    g''8    
g''4. ^"~"  \bar "|"   a''8    b''8    a''8    a''8    g''8    e''8  \bar "|"   
d''8    b'8    d''8    g''8    d''8    b'8  \bar "|"   a'8    b'8    a'8    a'8 
   b'8    d''8  \bar "|"     e''8    fis''8    g''8    a''4. ^"~"  \bar "|"   
a''8    b''8    a''8    a''8    g''8    e''8  \bar "|"   d''8    b'8    d''8    
g''4. ^"~"  \bar "|"   g''8    b''8    a''8    g''8    e''8    d''8  \bar "|"   
  e''8    fis''8    g''8    a''4    b''8  \bar "|"   cis'''8    a''8    cis'''8 
   a''8    g''8    e''8  \bar "|"   d''8    b'8    d''8    g''8    d''8    b'8  
\bar "|"   a'8    b'8    a'8    a'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
