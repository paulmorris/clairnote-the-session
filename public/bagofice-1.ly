\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/796#setting796"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/796#setting796" {"https://thesession.org/tunes/796#setting796"}}}
	title = "Bag Of Ice, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'8    e'8    \bar "|"   d'8    
g'8    b'8    g'8    d''8    g'8    b'8    g'8    \bar "|"   d'8    g'8    b'8  
  g'8    a'8    g'8    e'8    g'8    \bar "|"   f'4 ^"~"    a'8    f'8    c''8  
  f'8    a'8    f'8    \bar "|"   d''8    c''8    a'8    fis'8    g'4    }     
\repeat volta 2 {   |
 a'8    b'8    \bar "|"   c''4 ^"~"    b'8    c''8    c''8    d''8    e''8    
fis''8    \bar "|"   g''8    b'8    b'4 ^"~"    g'8    a'8    b'8    d''8    
\bar "|"   c''4 ^"~"    b'8    c''8    c''8    d''8    e''8    fis''8    
\bar "|"   g''8    e''8    d''8    b'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
