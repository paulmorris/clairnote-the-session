\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Emmanuel Delahaye"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/119#setting12721"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/119#setting12721" {"https://thesession.org/tunes/119#setting12721"}}}
	title = "Wren's Nest, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   \repeat volta 2 {   d'8  \bar "|"   g'8    a'8    
b'8    d''8    e''8    d''8    \bar "|"   cis''8    a'8    a'8    a'4    cis''8 
   \bar "|"   b'8    g'8    e'8    e'8    fis'8    g'8    \bar "|"   a'8    
fis'8    d'8    a'8    fis'8    d'8    \bar "|"   g'8    a'8    b'8    d''8    
e''8    d''8    \bar "|"   cis''8    a'8    a'8    a'8    b'8    cis''8    
\bar "|"   b'8    g'8    e'8    a'8    fis'8    d'8    \bar "|"   g'8    e'8    
e'8    e'4    }   \repeat volta 2 {   d'8  \bar "|"   e'8    g'8    e''8    
d''8    e''8    b'8    \bar "|"   cis''8    a'8    a'8    b'4 ^"~"    a'8  
\bar "|"   g'8    a'8    b'8    d''8    e''8    d''8    \bar "|"   cis''8    
a'8    a'8    b'8    g'8    d'8    \bar "|"   e'8    g'8    e''8    d''8    
e''8    b'8    \bar "|"   cis''8    a'8    a'8    b'4 ^"~"    a'8  \bar "|"   
g'8    a'8    b'8    cis''8    a'8    b'8    \bar "|"   g'8    e'8    e'8    
fis'8    e'8    }   \repeat volta 2 {   d'8  \bar "|"   b8    d'8    e'8    g'8 
   e'8    d'8  \bar "|"   b8    e'8    e'8    e'4    d'8  \bar "|"   b8    d'8  
  e'8    g'8    a'8    b'8  \bar "|"   a'8    fis'8    d'8    fis'8    e'8    
d'8    \bar "|"   b8    d'8    e'8    g'8    e'8    d'8  \bar "|"   b8    e'8   
 e'8    e'4    fis'8  \bar "|"   g'8    a'8    b'8    cis''8    a'8    b'8    
\bar "|"   g'8    e'8    e'8    e'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
