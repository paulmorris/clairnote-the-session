\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1439#setting14819"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1439#setting14819" {"https://thesession.org/tunes/1439#setting14819"}}}
	title = "Old Black Guard, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   \tuplet 3/2 {   d''8    e''8    
fis''8  }   \bar "|"   g''4    b'4    d''4    e''8.    d''16    \bar "|"   c''4 
   fis'4    a'4   ~    a'8.    g'16    \bar "|"   fis'8.    g'16    a'8.    
b'16    c''8.    d''16    e''8.    fis''16    \bar "|"   \tuplet 3/2 {   g''8    
b''8    a''8  }   \tuplet 3/2 {   g''8    fis''8    e''8  }   d''4    
\tuplet 3/2 {   d''8    e''8    fis''8  }   \bar "|"     g''4    b'4    d''4    
e''8.    d''16    \bar "|"   c''4    fis'4    a'4    a'8.    g'16    \bar "|"   
fis'8.    g'16    a'8.    b'16    c''8.    d''16    e''8.    fis''16    
\bar "|"   g''4    b''4    g''4    }     \repeat volta 2 {   d''8.    b'16    
\bar "|"   g'8.    a'16    \tuplet 3/2 {   b'8    c''8    d''8  }   g''8.    
fis''16    a''8.    g''16    \bar "|"   fis''4    e''4    c''4   ~    c''8.    
e''16    \bar "|"   fis''4    e''4    c''8.    b'16    c''8.    d''16    
\bar "|"   e''4    d''4    b'8.    c''16    b'8.    a'16    \bar "|"     g'8.   
 a'16    b'8.    d''16    g''8.    fis''16    a''8.    g''16    \bar "|"   
fis''4    e''4    c''4    a''8.    b''16    \bar "|"   c''8.    b''16    a''8.  
  g''16    fis''8.    d''16    e''8.    fis''16    \bar "|"   g''4    d''4    
g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
