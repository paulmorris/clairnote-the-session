\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1042#setting14272"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1042#setting14272" {"https://thesession.org/tunes/1042#setting14272"}}}
	title = "Macroom Lasses, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   d'8    g'4. ^"~"    a'4    g''8    e''8    \bar "|"   
d''8    b'8    c''8    a'8    b'8    g'8    e'8    g'8    \bar "|"   d'8    g'8 
   e'16    fis'16    g'8    a'4 ^"~"    g''8    e''8    \bar "|"   d''8    b'8  
  a'8    c''8    b'8    g'8    g'8    }   g''8    d''8    e''8    g''8    d''4  
  b'8    d''8    \bar "|"   c''8    a'8    b'8    g'8    a'8    e'4. ^"~"    
\bar "|"   g''8    d''8    e''8    g''8    d''8    g''8    b''8    g''8    
\bar "|"   a''8    g''8    a''8    b''8    g''4    g''8    a''8    \bar "|"   
b''8    g''8    e''8    g''8    d''16    e''16    d''8    b'16    c''16    d''8 
   \bar "|"   c''8    a'8    b'8    g'8    a'8    g'8    e'16    fis'16    g'8  
  \bar "|"   d'8    g'4. ^"~"    a'16    b'16    a'8    g''8    e''8    
\bar "|"   d''8    b'8    c''16    b'16    a'8    b'8    g'8    g'8    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
