\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jdave"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/96#setting12648"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/96#setting12648" {"https://thesession.org/tunes/96#setting12648"}}}
	title = "Scotch Mary"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   c''8    d''8    e''8    d''8    c''8    d''8    e''8 
   d''8  \bar "|"   b'8    e'8    e'8    fis'8    g'4    a'8    b'8  \bar "|"   
c''8    d''8    e''8    d''8    c''8    d''8    e''8    d''8  \bar "|"   c''8   
 a'8    b'8    g'8    a'4    a'8    b'8  \bar "|"   c''8    d''8    e''8    
d''8    c''8    d''8    e''8    d''8  \bar "|"   b'8    e'8    e'8    fis'8    
g'4    a'8    b'8  \bar "|"   c''8    d''8    e''8    g''8    fis''4    e''8    
d''8  \bar "|"   c''8    a'8    b'8    g'8    a'4    a'8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
