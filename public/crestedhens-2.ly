\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Alastair Chisholm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/562#setting25780"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/562#setting25780" {"https://thesession.org/tunes/562#setting25780"}}}
	title = "Crested Hens"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   e'4.    g'8    fis'8    e'8    \bar "|"   b'4. (   
b'8  -)   b'8    cis''8    \bar "|"   d''8    cis''8    b'8    e''8    d''8    
cis''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    fis'8    \bar "|"   
  e'4.    g'8    fis'8    e'8    \bar "|"   b'4. (   b'8  -)   b'8    cis''8    
\bar "|"   d''8    cis''8    b'8    a'8    g'8    a'8    \bar "|"   b'4.    g'8 
   fis'8    d'8    \bar "|"     e'4.    g'8    fis'8    e'8    \bar "|"   b'4. 
(   b'8  -)   b'8    cis''8    \bar "|"   d''8    cis''8    b'8    e''8    d''8 
   cis''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    fis'8    
\bar "|"     e'4.    g'8    fis'8    e'8    \bar "|"   b'4. (   b'8  -)   b'8   
 cis''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    a'8    \bar "|"   
b'4. (   b'8  -)   e''8    fis''8    \bar "||"     g''8    b'8    b'8    g''8   
 g''8    b'8    \bar "|"   c''4. (   c''8  -)   e''8    fis''8    \bar "|"   
g''8    fis''8    a''8    g''8    fis''8    e''8    \bar "|"   dis''4    e''4   
 fis''4    \bar "|"     g''8    b'8    b'8    g''8    g''8    b'8    \bar "|"   
c''4. (   c''8  -)   e''8    fis''8    \bar "|"   g''8    fis''8    a''8    
g''8    fis''8    d''8    \bar "|"   e''4. (   e''8  -)   e''8    fis''8    
\bar "|"     g''8    b'8    b'8    g''8    g''8    b'8    \bar "|"   c''4. (   
c''8  -)   e''8    fis''8    \bar "|"   g''8    fis''8    a''8    g''8    
fis''8    e''8    \bar "|"   dis''4    e''4    fis''4    \bar "|"     g''8    
b'8    b'8    g''8    g''8    b'8    \bar "|"   c''4. (   c''8  -)   e''8    
fis''8    \bar "|"   g''8    fis''8    a''8    g''8    fis''8    d''8    
\bar "|"   e''4.    g'8    fis'8    d'8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
