\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Markmoloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/33#setting12431"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/33#setting12431" {"https://thesession.org/tunes/33#setting12431"}}}
	title = "Farewell To Erin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   a'4.    c''8    e''4.    fis''8  \bar "|"   g''8    
e''8    d''8    b'8    g'8    b'8    d''8    b'8  \bar "|"   a'4.    c''8    
e''4.    fis''8  \bar "|"   g''8    e''8    d''8    fis''8    e''8    a'8    
a'8    g'8  \bar "|"   a'4.    c''8    e''4.    fis''8  \bar "|"   g''8    e''8 
   d''8    b'8    d''4    g''4  \bar "|"   e''8    d''8    e''8    g''8    a''4 
   b''8    a''8  \bar "|"   g''8    e''8    d''8    b'8    b'8    a'8    a'4  } 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
