\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Nigel Gatherer"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/86#setting12607"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/86#setting12607" {"https://thesession.org/tunes/86#setting12607"}}}
	title = "Rakish Paddy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \mixolydian   \repeat volta 2 {   c''4    c''8    b'8    c''4  
  b'8    d''8    \bar "|"   a'8    d''8    g'8    d''8    a'8    d'8    d'8    
c''8    \bar "|"   d''8    d''8    d''8    cis''8    d''4.    e''8    \bar "|"  
 fis''8    d''8    e''8    g''8    fis''8    d''8    e''8    d''8    \bar "|"   
c''8    d''8    e''8    d''8    \tuplet 3/2 {   c''8    d''8    c''8  }   b'8    
d''8    \bar "|"   a'8    d''8    g'8    d''8    a'8    d'8    d'8    c'8    
\bar "|"   d'8    e'8    fis'8    g'8    a'8    d''8    d''8    e''8    
\bar "|"   c''8    b'8    a'8    d''8    a'8    d'8    d'4    }   e''8    
fis''8    g''8    b''8    \tuplet 3/2 {   a''8    g''8    fis''8  }   g''4    
\bar "|"   g''8    a''8    g''8    fis''8    e''8    b'8    c''8    e''8    
\bar "|"   d''4    a''8    d''8    b''8    d''8    a''8    d''8    \bar "|"   
a''8    b''8    a''8    g''8    fis''8    d''8    d''8    fis''8    \bar "|"   
e''8    c''8    g''8    c''8    a''8    c''8    g''8    fis''8    \bar "|"   
g''8    fis''8    g''8    a''8    g''8    fis''8    e''8    cis''8    \bar "|"  
 d''8    e''8    fis''8    g''8    a''8    b''8    a''8    g''8    \bar "|"   
fis''8    a''8    g''8    e''8    d''8    cis''8    d''8    e''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
