\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1260#setting1260"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1260#setting1260" {"https://thesession.org/tunes/1260#setting1260"}}}
	title = "Jack Broke Da Prison Door"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g'4    b'8    g'8    b'8    d''8    b'8    g'8  
\bar "|"   a'8    b'8    a'8    b'8    d''8    b'8    a'8    b'8  \bar "|"   
g'4    b'8    g'8    b'8    d''8    b'8    g'8  \bar "|"   a'8    b'8    a'8    
g'8  \grace {    e'8    fis'8  }   e'4    d'4  }     \repeat volta 2 {   g''4   
 g''8    d''8    e''8    d''8    b'8    d''8  \bar "|"   g''4    g''8    d''8   
 e''8    a''8  \grace {    b''8  }   a''8    fis''8  \bar "|"   g''4    g''8    
d''8    e''8    d''8    b'8    g'8  \bar "|"   a'8    b'8    a'8    g'8  
\grace {    e'8    fis'8  }   e'4    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
