\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ElaineT"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/900#setting14084"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/900#setting14084" {"https://thesession.org/tunes/900#setting14084"}}}
	title = "Scartaglen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d'8    fis'8    e'8    fis'8    d'4    a'8    d''8    
\bar "|"   cis''8    a'8    g'8    fis'8    e'8    fis'8    g'8    d'8    
\bar "|"   fis'4.    e'8    d'8    fis'8    a'8    d''8    \bar "|"   cis''8    
a'8    g'8    fis'8    e'8    fis'8    g'8    e'8    \bar "|"   d'8    fis'8    
e'8    fis'8    d'4    a'8    d''8    \bar "|"   cis''8    a'8    g'8    fis'8  
  e'8    fis'8    g'8    d'8    \bar "|"   a'8    d''8    d''8    cis''8    
d''4    e''8    d''8    \bar "|"   \bar "|"   cis''8    a'8    g'8    fis'8    
e'8    fis'8    g'8    e'8    \bar ":|."   cis''8    a'8    g'8    e'8    e'8   
 d'8    d'4    \bar ":|."   \bar ".|:"   fis''4.    e''8    fis''8    a''8    
g''8    e''8    \bar "|"   fis''4    d''8    fis''8    a''8    fis''8    d''8   
 e''8    \bar "|"   fis''4.    e''8    fis''8    a''8    g''8    e''8    
\bar "|"   fis''8    d''8    e''8    cis''8    a'8    d''8    d''4    \bar "|"  
 fis''4.    e''8    fis''8    a''8    g''8    e''8    \bar "|"   fis''4    d''8 
   fis''8    a''8    fis''8    d''8    e''8    \bar "|"   fis''8    a''8    
g''8    e''8    d''4    e''8    d''8    \bar "|"   cis''8    a'8    g'8    
fis'8    e'8    fis'8    g'8    e'8    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
