\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "cat"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/855#setting855"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/855#setting855" {"https://thesession.org/tunes/855#setting855"}}}
	title = "Afton Water"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   d'4    \bar "||"   g'4    b'4    a'8    g'8    
\bar "|"   a'8    b'8    c''4    b'8    a'8    \bar "|"   g'4    b'4    a'8    
g'8    \bar "|"   g'4    fis'4    d'4    \bar "|"     g'4    b'4    a'8    g'8  
  \bar "|"   a'8    b'8    d''4    g''8    e''8    \bar "|"   d''4    c''8    
b'8    a'8    b'8    \bar "|"   g'2    d''8    e''8    \bar "|"     f''4    
g''8    f''8    e''8    d''8    \bar "|"   d''4    e''4    g''4    \bar "|"   
g''4    b'4    a'8    g'8    \bar "|"   g'4    fis'4    d'4    \bar "|"     g'4 
   b'4    a'8    g'8    \bar "|"   a'8    b'8    d''4    g''8    e''8    
\bar "|"   d''4    c''8    b'8    a'8    b'8    \bar "|"   g'2.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
