\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Manu Novo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1018#setting14239"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1018#setting14239" {"https://thesession.org/tunes/1018#setting14239"}}}
	title = "Miss Susan Cooper"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \tuplet 3/2 {   a'16    b'16    cis''16  } \bar "|"   
d''16 ^"D"   fis''16    e''16    d''16      b'8 ^"G"   d''16    b'16  \bar "|"  
 b'16 ^"D"   a'16    fis'16    a'16    d'16    a'16    fis'16    a'16  \bar "|" 
  d''16    cis''16  \bar "|"   b'16 ^"Bm"   cis''16    d''16    e''16    
fis''16    b'16    b'16    cis''16  \bar "|"   d''16 ^"Bm"   e''16    fis''16   
 gis''16      a''8 ^"E7"   gis''16    fis''16  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
