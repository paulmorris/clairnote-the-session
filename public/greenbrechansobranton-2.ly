\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1217#setting14512"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1217#setting14512" {"https://thesession.org/tunes/1217#setting14512"}}}
	title = "Green Brechans O' Branton, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key b \minor   g''8  \repeat volta 2 {   fis''4    b'8    b'8    
cis''8    b'8  \bar "|"   e''4    a'8    a'8    b'8    a'8  \bar "|"   fis''4   
 b'8    b'8    cis''8    b'8  \bar "|"   d''4.    g''4.  \bar "|"   fis''4    
b'8    b'8    cis''8    b'8  \bar "|"   e''4    a'8    a'8    b'8    a'8  
\bar "|"   b'8    cis''8    d''8    e''8    cis''8    a'8  \bar "|"   d''4.    
g''4.  }   \repeat volta 2 {   fis''8    d''8    b'8    fis''8    d''8    b'8  
\bar "|"   e''8    cis''8    a'8    e''8    cis''8    a'8  \bar "|"   fis''8    
d''8    b'8    fis''8    d''8    b'8  \bar "|"   d''4.    g''4.  \bar "|"   
fis''8    d''8    b'8    fis''8    d''8    b'8  \bar "|"   e''8    cis''8    
a'8    e''8    cis''8    a'8  \bar "|"   b'8    cis''8    d''8    e''8    
cis''8    a'8  \bar "|"   d''4.    g''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
