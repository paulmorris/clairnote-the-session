\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "timmy!"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1348#setting14697"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1348#setting14697" {"https://thesession.org/tunes/1348#setting14697"}}}
	title = "Boys Of Ballinahinch, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   fis''8    d''4. ^"~"    e''8    b'4. ^"~"  
\bar "|"   d''8    cis''8    d''8    e''8    fis''8    a''8    a''8    g''8  
\bar "|"   fis''8    d''8    d''8    c''8    a'8    b'8    c''!8    b'8  
\bar "|"   a'8    fis'8    d''8    fis'8    e'8    fis'8    d'4  \bar "|"   
d''4.    fis''8    e''8    b'4. ^"~"  \bar "|"   d''8    cis''8    d''8    e''8 
   fis''8    a''8    a''8    g''8  \bar "|"   fis''8    d''8    d''8    c''8    
a'8    b'8    c''!8    b'8  \bar "|"   a'8    fis'8    d''8    fis'8    e'8    
fis'8    d'4  \bar "|"   \bar "|"   fis''8    a''8    a''8    b''8    a''8    
fis''8    d''8    fis''8  \bar "|"   a''8    fis''8    d''8    fis''8    g''8   
 fis''8    e''8    d''8  \bar "|"   fis''8    a''8    a''8    b''8    a''8    
fis''8    d''8    fis''8  \bar "|"   g''8    fis''8    e''8    d''8    b'8    
cis''8    d''8    e''8  \bar "|"   fis''8    a''8    a''8    b''8    a''8    
fis''8    d''8    fis''8  \bar "|"   a''8    b''8    a''8    fis''8    g''4    
g''8    a''8  \bar "|" \tuplet 3/2 {   a''8 (   g''8    fis''8  -) }   g''8    
fis''8    g''8    fis''8    e''8    fis''8  \bar "|"   g''4    g''8    fis''8   
 g''8    a''8    a''8    g''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
