\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "manxygirl"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/84#setting22230"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/84#setting22230" {"https://thesession.org/tunes/84#setting22230"}}}
	title = "Rakes Of Kildare, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key b \minor   fis'8    \bar "|"   fis'4    b'8    b'4    cis''8    
\bar "|"   d''4    cis''8    d''8    e''8    fis''8    \bar "|"   b''4    
fis''8    fis''4    e''8    \bar "|"   d''8    b'8    b'8    b'4    fis'8    
\bar "|"     fis'4    b'8    b'4    cis''8    \bar "|"   d''4    cis''8    d''8 
   e''8    fis''8    \bar "|"   b''4    fis''8    fis''4    e''8    \bar "|"   
d''8    b'8    b'8    b'4    b''8    \bar "|"     b''4    b''8    b''4    b''8  
  \bar "|"   b''4    b''8    d'''8    cis'''8    b''8    \bar "|"   a''4    
fis''8    fis''4    e''8    \bar "|"   fis''4    e''8    d''8    cis''8    b'8  
\bar "|"     b''4    b''8    b''4    b''8    \bar "|"   b''4    b''8    d'''8   
 cis'''8    b''8    \bar "|"   a''4    fis''8    fis''4    e''8    \bar "|"   
d''8    b'8    b'8    b'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
