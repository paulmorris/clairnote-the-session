\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/660#setting24462"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/660#setting24462" {"https://thesession.org/tunes/660#setting24462"}}}
	title = "Burning Of The Piper's Hut, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   \repeat volta 2 {   fis'8    \bar "|"   b'8    cis''8 
   d''8    e''8    fis''2    \bar "|"   fis''8.    e''16    fis''16    a''8.    
fis''4    e''8    d''8    \bar "|"   cis''4    a'4    e''4    a'4    \bar "|"   
cis''8    d''8    e''8    fis''8    e''4    d''8    cis''8    \bar "|"     b'8  
  cis''8    d''8    e''8    fis''2    \bar "|"   fis''8.    e''16    fis''16    
a''8.    fis''4    e''8    d''8    \bar "|"   cis''4    a'4    fis''4    e''8   
 d''8    \bar "|"   cis''4    b'8    a'8    b'4.    }     \repeat volta 2 {   
fis'8    \bar "|"   b'8    cis''8    d''8    e''8    fis''8    b'8    b'4 ^"~"  
  \bar "|"   a''8    b'8    b'4 ^"~"    fis''4    e''8    d''8    \bar "|"   
cis''4    a'4    e''4    a'4    \bar "|"   cis''8    d''8    e''8    fis''8    
e''4    d''8    cis''8    \bar "|"     b'8    cis''8    d''8    e''8    fis''8  
  b'8    b'4 ^"~"    \bar "|"   a''8    b'8    b'4 ^"~"    fis''4    e''8    
d''8    \bar "|"   cis''4    a'4    fis''4    e''8    d''8    \bar "|"   cis''4 
   b'8    a'8    b'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
