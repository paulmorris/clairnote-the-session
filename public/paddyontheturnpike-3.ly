\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/338#setting13127"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/338#setting13127" {"https://thesession.org/tunes/338#setting13127"}}}
	title = "Paddy On The Turnpike"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   d'8    g'8    g'8    f'8    g'4. ^"~"    a'8  
\bar "|"   bes'8    g'8    g'4 ^"~"    d''8    g'8    bes'8    g'8  \bar "|"   
d'8    f'8    f'8    e'8    f'4. ^"~"    bes'8  \bar "|"   a'8    f'8    c''8   
 f'8    d''8    f'8    c''8    f'8  \bar "|"   d'8    g'8    g'8    f'8    g'4. 
^"~"    a'8  \bar "|"   bes'8    g'8    g'8    a'8    bes'8    c''8    d''8    
e''8  \bar "|"   f''4. ^"~"    d''8    c''8    a'8    f'8    a'8  \bar "|"   
bes'8    g'8    a'8    f'8    g'4. ^"~"    f'8  \bar ":|."   bes'8    g'8    
a'8    f'8    g'4. ^"~"    c''8  \bar "||"   \bar ".|:"   d''8    g''8    g''8   
 f''8    g''4. ^"~"    a''8  \bar "|"   bes''8    g''8    g''4 ^"~"    bes''8   
 g''8    a''8    f''8  \bar "|"   d''8    f''8    f''8    e''8    f''4. ^"~"    
g''8  \bar "|"   a''8    f''8    f''4 ^"~"    a''8    f''8    g''8    f''8  
\bar "|"   d''8    g''8    g''8    f''8    g''4. ^"~"    a''8  \bar "|"   
bes''8    g''8    g''4 ^"~"    g''8    f''8    d''8    e''8  \bar "|"   f''4. 
^"~"    d''8    c''8    a'8    f'8    a'8  \bar "|"   bes'8    g'8    a'8    
f'8    g'4. ^"~"    c''8  \bar ":|."   bes'8    g'8    a'8    f'8    g'4. ^"~"  
  f'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
