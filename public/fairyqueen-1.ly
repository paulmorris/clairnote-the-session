\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "glauber"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/243#setting243"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/243#setting243" {"https://thesession.org/tunes/243#setting243"}}}
	title = "Fairy Queen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   d''4    d''4    b'8    a'8    \bar "|"   d''2    b'8  
  a'8    \bar "|"   d''4    d''4    b'8    a'8    \bar "|"   d''4  \grace {    
fis''8  }   e''4    fis''4    \bar "|"   d''4    d''4    a'8    b'8    \bar "|" 
  fis'2    d''8    cis''8    \bar "|"     b'2    d''8    cis''8    \bar "|"   
d''4.    g''8    fis''16    g''16    fis''16    e''16    \bar "|"   d''4.    
fis''8    e''16    fis''16    e''16    d''16    \bar "|"   cis''4.    fis''8    
e''16    fis''16    e''16    d''16    \bar "|"     b'4.    cis''8    b'8    a'8 
   \bar "|"   fis'4.    g''8    fis''8    e''8    \bar "|" \grace {    a'8  }   
b'4.    d''8    cis''8    b'8    \bar "|"   a'4    b'4    d''8    cis''8    
\bar "|"   d''2.    \bar "|"   d'2    \bar "||"     d''8    e''8    \bar "|"   
fis''4. ^"~"    e''16    fis''16    fis''4    \bar "|"   fis''8    e''8    
fis''8    g''8    fis''8    g''8    \bar "|" \grace {    fis''8  }   e''2    
e''4    \bar "|"   e''4    fis''4    a''4    \bar "|"   d''2    a''4    
\bar "|"     cis''2    a''8    cis''8    \bar "|"   b'4.    cis''8    b'16    
cis''16    b'16    a'16    \bar "|"   fis'4.    a'8    b'8    cis''8    
\bar "|"   d''4.    g'8    fis'16    g'16    fis'16    e'16    \bar "|"   e'4.  
  d''8    cis''8    d''8    \bar "|"     e''4.    d''8    cis''16    d''16    
cis''16    b'16    \bar "|"   a'4.    g''8    fis''8    d''8    \bar "|"   b'4. 
   d''8    cis''16    d''16    cis''16    b'16    \bar "|"   a'4    b'4    d''8 
   cis''8    \bar "|"   d''2.    \bar "|"   d'2    \bar "||"     b'8    cis''8  
  \bar "|"   d''4    d''4    a'4    \bar "|"   d''4.    cis''8    b'16    
cis''16    b'16    a'16    \bar "|"   fis'4.    g'8    fis'8    e'8    \bar "|" 
  d'2    b'8    cis''8    \bar "|"   d''8    cis''8    d''8    e''8    d''8    
cis''8    \bar "|"     b'8    a'8    b'8    cis''8    d''8    b'8    \bar "|"   
e''4.    d''8    cis''16    d''16    cis''16    b'16    \bar "|"   a'2    d''8  
  e''8    \bar "|"   fis''4    fis''4    b'4    \bar "|"   fis''8    e''8    
fis''8    g''8    fis''8    a'8    \bar "|"     \grace {    cis''8    d''8  }   
e''4.    d''8    cis''16    d''16    cis''16    b'16    \bar "|"   a'2    
fis''4    \bar "|"   b'8    cis''8    b'8    cis''8    d''8    e''8    \bar "|" 
  a'4    b'4    d''8    cis''8    \bar "|" \grace {    cis''8  }   d''2.    
\bar "|"   d'2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
