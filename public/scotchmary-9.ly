\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/96#setting22968"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/96#setting22968" {"https://thesession.org/tunes/96#setting22968"}}}
	title = "Scotch Mary"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 { \tuplet 3/2 {   g'8    a'8    b'8  
} \bar "|"   c''8    e'8    e'4    c''8    d''8    e''8    d''8  \bar "|"   
c''8    e'8    e'4    g'4    a'8    b'8  \bar "|"   c''8    e'8    e'4    c''8  
  d''8    e''8    d''8  \bar "|"   c''8    a'8    b'8    g'8    a'4    
\tuplet 3/2 {   g'8    a'8    b'8  } \bar "|"     c''8    e'8    e'4    c''8    
d''8    e''8    d''8  \bar "|"   c''8    e'8    e'4    g'4    a'8    b'8  
\bar "|"   c''8    e'8    e'4    c''8    d''8    e''8    d''8  \bar "|"   c''8  
  a'8    b'8    g'8    a'4    \tuplet 3/2 {   b'8    c''8    d''8  } \bar "||"   
  \bar "|"   e''8    a''8    a''8    fis''8    g''8    fis''8    e''8    d''8  
\bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }   e''8    fis''8    g''4    
fis''8    g''8  \bar "|"   e''8    a''8    a''8    fis''8    g''8    fis''8    
e''8    d''8  \bar "|"   c''8    a'8    b'8    g'8    a'4    \tuplet 3/2 {   b'8 
   c''8    d''8  } \bar "|"     e''8    a''8    a''8    fis''8    g''8    
fis''8    e''8    d''8  \bar "|"   \tuplet 3/2 {   b'8    c''8    d''8  }   e''8 
   fis''8    g''4    fis''8    g''8  \bar "|"   a''8    fis''8    \tuplet 3/2 {  
 g''8    fis''8    e''8  }   fis''8    d''8    e''8    d''8  \bar "|"   
\tuplet 3/2 {   c''8    b'8    a'8  }   b'8    g'8    a'4  \bar "||"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
