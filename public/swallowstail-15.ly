\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "J. A. Cerro"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/105#setting29327"
	arranger = "Setting 15"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/105#setting29327" {"https://thesession.org/tunes/105#setting29327"}}}
	title = "Swallow's Tail, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   a''8    g''8  \bar "|"     e''4 (^"~"    e''8  -)   
d''8    e''8    a'8    a'4 ^"~"  \bar "|"     e''4 (^"~"    e''8  -)   fis''8   
   g''4 (^"~"    g''8  -)   fis''8  \bar "|"   g''8    e''8    d''8    b'8    
d''8    g'8    g'4 ^"~"  \bar "|"   d''8    g'8    a'16    b'16    d''8    g''4 
^"~"    a''8    g''8  \bar "|"       e''4 (^"~"    e''8  -)   d''8    e''8    
a'8    a'4 ^"~"  \bar "|"     e''4 (^"~"    e''8  -)   fis''8      g''4 (^"~"   
 g''8  -)   a''8  \bar "|"   a''8    g''8    g''4 ^"~"    a''8    g''8    e''8  
  d''8  \bar "|"   e''4 ^"~"    d''8    b'8  \grace {    g'16  }   a'4    a''8  
  g''8  \bar "|"     a''8    g''8    e''8    d''8    e''8    a'8    a'4 ^"~"  
\bar "|"     e''4 (^"~"    e''8  -)   fis''8      g''4 (^"~"    g''8  -)   
fis''8  \bar "|"   g''8    e''8    d''8    b'8    d''8    g'8    g'4 ^"~"  
\bar "|"   d''8    g'8    a'16    b'16    d''8    g''4 ^"~"    a''8    g''8  
\bar "|"       e''4 (^"~"    e''8  -)   d''8    e''8    a'8    a'4 ^"~"  
\bar "|"     e''4 (^"~"    e''8  -)   fis''8      g''4 (^"~"    g''8  -)   a''8 
 \bar "|"   a''8    g''8    g''4 ^"~"    a''8    g''8    e''8    d''8  \bar "|" 
  e''4 ^"~"    d''8    b'8  \grace {    g'16  }   a'4    cis''8    d''8  
\bar "|"     \repeat volta 2 {   e''8    a''8    a''8    gis''8      a''4 
(^"~"    a''8  -)   g''!8  \bar "|"     e''4 (^"~"    e''8  -)   fis''8      
g''4 (^"~"    g''8  -)   fis''8  \bar "|"   g''8    e''8    d''8    b'8    d''8 
   g'8    g'4 ^"~"  \bar "|"   d''8    g'8    a'16    b'16    d''8    g''4    
a''8    g''8  \bar "|"       e''4 (^"~"    e''8  -)   d''8    e''8    a'8    
a'4 ^"~"  \bar "|"     e''4 (^"~"    e''8  -)   fis''8      g''4 (^"~"    g''8  
-)   a''8  \bar "|"   a''8    g''8    g''4 ^"~"    a''8    g''8    e''8    d''8 
 } \alternative{{   e''4 ^"~"    d''8    b'8  \grace {    g'16  }   a'4    b'16 
   cis''16    d''8  } {   e''4 ^"~"    d''8    b'8  \grace {    g'16  }   a'2  
\bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
