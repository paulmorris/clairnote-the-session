\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jaychoons"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1374#setting14731"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1374#setting14731" {"https://thesession.org/tunes/1374#setting14731"}}}
	title = "Cucanandy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \dorian   \repeat volta 2 {   b'4.    b'4    a'8    g'4    a'8 
 \bar "|"   b'4    d''8    d''4    cis''8    d''4.  \bar "|"   b'4.    b'4    
a'8    g'4    a'8  \bar "|"   b'4    e''8    e''4    d''8    e''4.  }   
\bar "|"   e''4    fis''8    g''4    fis''8    g''4.  \bar "|"   b'4    d''8    
d''4    cis''8    d''4.  \bar "|"   e''4    fis''8    g''4    fis''8    g''4    
d''8  \bar "|"   b'4    e''8    e''4    d''8    e''4.  \bar "|"   e''4    
fis''8    g''4    fis''8    g''4.  \bar "|"   b'4    d''8    d''4    cis''8    
d''4.  \bar "|"   e''4    b'8    b'4    a'8    g'4    a'8  \bar "|"   b'4    
e''8    e''4    d''8    e''4.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
