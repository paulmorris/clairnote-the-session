\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "donnchad"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/685#setting685"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/685#setting685" {"https://thesession.org/tunes/685#setting685"}}}
	title = "King's Fancy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \minor   \repeat volta 2 {   bes'16    c''16    \bar "|"   
d''8    c''8    bes'8    bes'8    a'8    bes'8    \bar "|"   g'8    bes'8    
ees''8    f'8    bes'8    d''8    \bar "|"   ees'8    g'8    c''8    d'8    f'8 
   bes'8    \bar "|"   c''8    a'8    f'8    g''8    f''8    ees''8    \bar "|" 
    d''8    c''8    bes'8    bes'8    a'8    bes'8    \bar "|"   g'8    bes'8   
 ees''8    f'8    bes'8    d''8    \bar "|"   ees'8    g'8    c''8    d'8    
f'8    bes'8    \bar "|"   c''8    a'8    f'8    bes'4    }     
\repeat volta 2 {   bes'16    c''16    \bar "|"   d''8    bes'8    g'8    g'8   
 fis'8    g'8    \bar "|"   d'8    g'8    bes'8    d''8    bes'8    g'8    
\bar "|"   a'8    fis'8    d'8    c''8    a'8    fis'8    \bar "|"   d''8    
ees''8    d''8    c''8    bes'8    a'8    \bar "|"     d''8    bes'8    g'8    
g'8    fis'8    g'8    \bar "|"   d'8    g'8    bes'8    d''8    bes'8    g'8   
 \bar "|"   a'8    fis'8    d'8    d''8    ees''8    d''8    \bar "|"   c''8    
bes'8    a'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
