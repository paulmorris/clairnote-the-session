\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Weejie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1191#setting14476"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1191#setting14476" {"https://thesession.org/tunes/1191#setting14476"}}}
	title = "Speed The Plough"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \minor     e'8 ^"Em"   fis'8    g'8    a'8    b'8    c''8    
b'8    g'8    \bar "|"   b'8    c''8    b'8    g'8    b'8    c''8    b'8    g'8 
   \bar "|"     a'4 ^"Am"   c''8    a'8      g'4 ^"Em"   b'8    g'8    \bar "|" 
  a'4    fis'4    fis'2    \bar "|"     e'8 ^"Em"   fis'8    g'8    a'8    b'8  
  c''8    b'8    g'8    \bar "|"   b'8    c''8    b'8    g'8    b'8    c''8    
b'8    g'8    \bar "|"     a'4 ^"Am"   c''8    a'8      g'4 ^"Em"   b'8    g'8  
  \bar "|"   fis'4    d'4      e'2 ^"Em"   } \repeat volta 2 {     e''4 ^"Em"   
e''4    e''8    b'8    g'8    b'8    \bar "|"   e''4    d''8    c''8    b'4    
g'4    \bar "|"     a'4 ^"Am"   c''8    a'8      g'4 ^"Em"   b'8    g'8    
\bar "|"   a'4    fis'4    fis'2    \bar "|"     e''4 ^"Em"   e''4    e''8    
b'8    g'8    b'8    \bar "|"   e''4    d''8    c''8    b'4    g'4    \bar "|"  
   a'4 ^"Am"   c''8    a'8      g'4 ^"Em"   b'8    g'8    \bar "|"     fis'4 
^"B7"   d'4      e'2 ^"Em"   \bar "|."   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
