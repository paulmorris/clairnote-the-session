\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/213#setting12884"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/213#setting12884" {"https://thesession.org/tunes/213#setting12884"}}}
	title = "Moth In The Lantern, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key b \minor   \repeat volta 2 {   fis'8    b'8    d''8    fis''4 
^"~"  \bar "|"   fis'8    b'8    d''8    e''4 ^"~"  \bar "|"   fis'8    b'8    
d''8    fis''4 ^"~"  \bar "|"   e''4. ^"~"    fis''4 ^"~"  \bar "|"   fis'8    
b'8    d''8    fis''4 ^"~"  \bar "|"   fis'8    b'8    d''8    e''4 ^"~"  
\bar "|"   fis''8    e''8    d''8    e''8    fis''16    g''16  \bar "|"   
fis''8    d''8    cis''8    b'4  }   \repeat volta 2 {   fis'8    b'8    d''8   
 cis''4 ^"~"  \bar "|"   d''8    cis''8    d''8    e''4 ^"~"  \bar "|"   fis'8  
  b'8    d''8    cis''4 ^"~"  \bar "|"   d''8    cis''8    b'8    c''16    b'16 
   g'8  \bar "|"   fis'8    b'8    d''8    cis''4 ^"~"  \bar "|"   d''8    
cis''8    d''8    e''4 ^"~"  \bar "|"   fis''8    e''8    d''8    e''8    
fis''8    g''8  \bar "|"   fis''8    d''8    cis''8    b'4 ^"~"  }   fis''8    
b'16    b'16    b'8    d''8    b'16    b'16  \bar "|"   e''8    a'16    a'16    
a'8    cis''8    a'16    a'16  \bar "|"   fis''8    b'16    b'16    b'8    d''8 
   b'16    b'16  \bar "|"   e''8    fis''16    fis''16    g''8    e''16    
e''16    g''8  \bar "|"   fis''8    b'16    b'16    b'8    d''8    b'16    b'16 
 \bar "|"   e''8    a'16    a'16    a'8    cis''8    a'16    a'16  \bar "|"   
fis''8    e''8    d''8    e''8    fis''16    g''16  \bar "|"   fis''16    
fis''16    d''8    cis''8    b'8    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
