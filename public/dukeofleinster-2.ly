\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1385#setting14745"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1385#setting14745" {"https://thesession.org/tunes/1385#setting14745"}}}
	title = "Duke Of Leinster, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major <<   g'4    g4   >> b'8    g'8    d''8    g'8    b'8    
g'8  \bar "|"   d''8    b'8    a'8    b'8    d''8    b'8    a'8    b'8  
\bar "|" <<   g'4.    g4.   >> b'8    d''4    e''8    b'8  \bar "|"   d''8    
b'8    a'8    b'8    g'8    e'8    d'8    e'8  \bar "|" <<   g'4    g4   >> b'8 
   g'8    d''8    g'8    b'8    g'8  \bar "|"   d''8    b'8    a'8    b'8    
d''8    b'8    a'8    b'8  \bar "|"   g'8    b'8  \grace {    c''8  }   
\tuplet 3/2 {   b'8    a'8    b'8  }   d''8    b'8  \grace {    c''8  }   
\tuplet 3/2 {   b'8    a'8    b'8  } \bar "|" \tuplet 3/2 {   d''8    c''8    b'8 
 }   a'8    b'8    g'8    e'8    d'8    e'8  }   \repeat volta 2 {   d''8    
e''8    g''8    a''8    b''8    g''8  \grace {    g''8    a''8  }   g''4  
\bar "|"   a''8    g''8    b''8    g''8    a''8    g''8    e''8    g''8  
\bar "|"   d''8    e''8    g''8    a''8    b''8    g''8  \grace {    a''8  }   
g''8    e''8  \bar "|" \tuplet 3/2 {   d''8    c''8    b'8  }   a'8    b'8    
g'8    e'8    d'8    e'8  \bar "|"   d''8    e''8    g''8    a''8    b''8    
g''8  \grace {    g''8    a''8  }   g''4  \bar "|"   a''8    g''8    b''8    
g''8    a''8    g''8    e''8    fis''8  \bar "|"   g''8    a''8    \tuplet 3/2 { 
  b''8    a''8    g''8  }   e''8    fis''8    \tuplet 3/2 {   g''8    fis''8    
e''8  } \bar "|"   d''8    b'8    a'8    b'8    g'8    e'8    d'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
