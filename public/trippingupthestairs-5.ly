\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/111#setting28407"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/111#setting28407" {"https://thesession.org/tunes/111#setting28407"}}}
	title = "Tripping Up The Stairs"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   fis'8 ^"D"   a'8    a'8      g'8 
^"G"   b'8    b'8  \bar "|"   fis'8 ^"D"   a'8    d''8    fis''8    e''8    
d''8  \bar "|"   cis''8 ^"A"   b'8    cis''8    a'8    b'8    cis''8  \bar "|"  
 d''8 ^"D"   fis''8    e''8      d''8 ^"G"   a'8    g'8  \bar "|"       fis'8 
^"D"   a'8    a'8      g'8 ^"G"   b'8    b'8  \bar "|"   fis'8 ^"D"   a'8    
d''8    fis''8    e''8    d''8  \bar "|"   cis''4 ^"A"   cis''8    a'8    b'8   
 cis''8  \bar "|"   d''8 ^"D"   fis''8    e''8    d''4    a'8  }     
\repeat volta 2 {   d''8 ^"Bm"   b'8    b'8    fis''8    b'8    b'8  \bar "|"   
d''8 ^"Bm"   b'8    b'8    fis''8    e''8    d''8  \bar "|"   cis''8 ^"A"   a'8 
   a'8    e''8    a'8    a'8  \bar "|"   e''8 ^"A"   fis''8    e''8    e''8    
d''8    cis''8  \bar "|"       d''8 ^"Bm"   b'8    b'8    fis''8    b'8    b'8  
\bar "|"   fis''8 ^"Bm"   g''8    fis''8    fis''8    e''8    d''8  \bar "|"   
cis''8 ^"A"   b'8    cis''8    a'8    b'8    cis''8  \bar "|"   d''8 ^"D"   
fis''8    e''8    d''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
