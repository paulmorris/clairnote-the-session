\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bsykes62"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/638#setting638"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/638#setting638" {"https://thesession.org/tunes/638#setting638"}}}
	title = "Lord Mayo"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key g \major   a'8    g'8    a'8    b'8  \bar "|"   e''4    e''16    
d''16    b'16    d''16  \bar "|"   e''8    a'8    a'8    b'16    a'16  \bar "|" 
  g'16    a'16    g'16    fis'16    e'8    d'8  \bar "|"     a'8    g'8    a'8  
  b'16    d''16  \bar "|"   e''8.    fis''16    g''8.    fis''16  \bar "|"   
e''8.    fis''16    g''16    d''16    b'16    g'16  \bar "|"   a'2  }     |
 a''4    g''16    e''16    g''16    a''16  \bar "|"   b''4    b''8    a''16    
g''16  \bar "|"   e''8    d''8    d''16    e''16    g''8  \bar "|"   g'8.    
a'16    g'8    e'8  \bar "|"     a''4    g''16    e''16    g''16    a''16  
\bar "|"   b''4    b''8    a''16    g''16  \bar "|"   e''8    d''8    d''16    
e''16    g''8  \bar "|"   a''2  \bar "||"     a''4    g''16    e''16    g''16   
 a''16  \bar "|"   b''4    b''8    a''16    g''16  \bar "|"   e''8    d''8    
d''16    e''16    g''8  \bar "|"   g'8.    a'16    g'8    e'8  \bar "|"     
a'8.    b'16    d''8.    b'16  \bar "|"   a'16    g'16    a'16    b'16    g''4  
\bar "|"   e''8.    fis''16    g''16    d''16    b'16    g'16  \bar "|"   a'2  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
