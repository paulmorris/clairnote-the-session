\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Nigel Gatherer"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1036#setting24242"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1036#setting24242" {"https://thesession.org/tunes/1036#setting24242"}}}
	title = "Swan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   cis''8.    b'16    \bar "|"   a'4    \tuplet 3/2 {   
cis''8    b'8    a'8  }   e'8.    a'16    cis''8.    e''16    \bar "|"   a''4   
 gis''8.    a''16    fis''8.    a''16    e''8.    cis''16    \bar "|"   
\tuplet 3/2 {   d''8    d''8    d''8  }   fis''8.    d''16    cis''8.    e''16   
 a'8.    cis''16    \bar "|"   b'8.    e'16    gis'8.    b'16    e''8.    d''16 
   cis''8.    b'16    \bar "|"     a'4    \tuplet 3/2 {   cis''8    b'8    a'8  
}   e'8.    a'16    cis''8.    e''16    \bar "|"   a''4    a''8.    gis''16    
a''8.    e''16    cis''8.    e''16    \bar "|"   d''4    fis''8.    d''16    
cis''8.    e''16    a'8.    cis''16    \bar "|"   b'8.    e'16    gis'8.    
b'16    a'4    }     cis''8.    e''16    \bar "|"   a''8.    a'16    cis''8.    
a''16    gis''8.    b'16    d''8.    fis''16    \bar "|"   fis''8.    e''16    
cis''8.    a'16    b'8.    e'16    gis'8.    b'16    \bar "|"   a'8.    cis''16 
   e''8.    cis''16    d''8.    fis''16    b''8.    a''16    \bar "|"   gis''8. 
   a''16    fis''8.    gis''16    e''4      cis''8.    e''16    \bar "|"   
a''8.    a'16    cis''8.    a''16    gis''8.    b'16    d''8.    fis''16    
\bar "|"   fis''8.    e''16    cis''8.    a'16    b'8.    e'16    gis'8.    
b'16    \bar "|"   a'8.    cis''16    e''8.    cis''16    d''8.    fis''16    
b''8.    a''16    \bar "|"   \tuplet 3/2 {   gis''8    fis''8    e''8  }   
\tuplet 3/2 {   d''8    cis''8    b'8  }   a'4    \bar "||"     cis''8.    e''16 
   \bar "|"   a''8.    gis''16    a''8.    b''16    a''8.    e''16    cis''8.   
 e''16    \bar "|"   d''8.    cis''16    b'8.    a'16    gis'8.    b'16    
e''8.    d''16    \bar "|"   cis''8.    e''16    a''8.    gis''16    fis''8.    
d''16    b''8.    a''16    \bar "|"   gis''8.    a''16    fis''8.    gis''16    
e''4      cis''8.    e''16    \bar "|"   a''8.    a'16    cis''8.    a''16    
gis''8.    b'16    d''8.    fis''16    \bar "|"   fis''8.    e''16    cis''8.   
 a'16    b'8.    e'16    gis'8.    b'16    \bar "|"   a'8.    cis''16    e''8.  
  cis''16    d''8.    fis''16    b''8.    a''16    \bar "|"   \tuplet 3/2 {   
gis''8    fis''8    e''8  }   \tuplet 3/2 {   d''8    cis''8    b'8  }   a'4    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
