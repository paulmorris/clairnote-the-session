\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/993#setting14199"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/993#setting14199" {"https://thesession.org/tunes/993#setting14199"}}}
	title = "Maire Rua"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   d'4    g'8    g'8    a'8    g'8    fis'8    g'8    
a'8  \bar "|"   b'4.    g'8    a'8    b'8    c''8  <<   b'8    d''8   >> <<   
a'8    e''8   >> \bar "|"   d'4    g'8    g'8    a'8    g'8    fis'8    g'8    
a'8  \bar "|"   b'8    d''8    b'8    c''8    a'8    fis'8    g'8    e'8    g'8 
 \bar ":|."   b'8    d''8    b'8    c''8    a'8    fis'8    g'8    d''8    c''8 
 \bar "||"   d''4. ^"~"    c''8    a'8    g'8    fis'8    e'8    d'8  \bar "|"  
 d''8    e''8    d''8    c''8    a'8    b'8    c''4.  \bar "|"   d''4. ^"~"    
c''8    a'8    g'8    fis'8    g'8    a'8  \bar "|"   b'8    d''8    b'8    
c''8    a'8    fis'8    g'8    d''8    e''8  \bar ":|."   b'8    d''8    b'8    
c''8    a'8    fis'8    g'8    e'8    g'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
