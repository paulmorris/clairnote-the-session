\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1353#setting14700"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1353#setting14700" {"https://thesession.org/tunes/1353#setting14700"}}}
	title = "Across The Hill"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \mixolydian   \bar "|"   d'8    d''8    d'8    c''8    d'8    
a'8  \bar "|"   a'8    b'8    c''8    c''8    d''8    a'8  \bar "|"   a'8    
d'8    b'8    c''8    d''8    e''8  \bar "|"   d''8    r8 a'8    g'8    c''8    
a'8  \bar "|"   a'8    d'8    c''8    c''8    d''8    a'8  \bar "|"   a'8    
b'8    c''8    c''8    d''8    e''8  \bar "|"   d''8    r8 d''8    c''8    a'8  
  g'8  \bar "|"   a'8    c''8    a'8    g'8    c''8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
