\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/835#setting24772"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/835#setting24772" {"https://thesession.org/tunes/835#setting24772"}}}
	title = "March Of The Kings Of Laois"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \mixolydian   a'4    fis'8    a'4    d'8  \bar "|"   a'4    
fis'8    a'4    d'8  \bar "|"   b'4    g'8    a'4    fis'8  \bar "|"   g'4    
fis'8    e'8.    fis'16    g'8  \bar "|"     a'4    fis'8    a'4    d'8  
\bar "|"   a'4    fis'8    a'4    d''8  \bar "|"   b'4    d''8    a'8.    d''16 
   g'8  \bar "|"   fis'8.    d''16    fis'8    e'4    d'8  \bar "|"   fis'8.    
e'16    d'8    g'8.    fis'16    e'8  \bar "|"     a'4.    a'8.    g'16    
fis'8  \bar "|"   e'8.    c''16    g'8    e'4.  \bar "|"   g'8.    c''16    g'8 
   e'4.  \bar "|"   d'4.    d'4.  \bar "|"   d'4.    d'4    d'8  }     
\repeat volta 2 {   b'4    g'8    d''4    g'8  \bar "|"   b'4    g'8    d''4    
g'8  \bar "|"   a'4    d''8    a'8    g'8    fis'8  \bar "|"   a'4    d''8    
fis'4    a'8  \bar "|"     b'4    d''8    d''8.    b'16    a'8  \bar "|"   g'8. 
   fis'16    g'8    e'8.    fis'16    g'8  \bar "|"   fis'8.    e'16    d'8    
g'8.    a'16    b'8  \bar "|"   a'4.    a'8    g'8    fis'8  \bar "|"     e'8.  
  c''16    g'8    e'4.  \bar "|"   g'8.    c''16    g'8    e'4.  \bar "|"   
d'4.    d'4.  \bar "|"   d'4.    d'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
