\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "edl"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/480#setting13375"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/480#setting13375" {"https://thesession.org/tunes/480#setting13375"}}}
	title = "Mrs. Kinney's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major \key d \major 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
