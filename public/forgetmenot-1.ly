\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/233#setting233"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/233#setting233" {"https://thesession.org/tunes/233#setting233"}}}
	title = "Forget Me Not"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key c \major   \grace {    b'8  }   c''4    b'8    c''8    a'8    
g'8    e'8    d'8    \bar "|"   c'8    a8    a4 ^"~"    g8    a8    c'8    d'8  
  \bar "|"   e'8    g'8    c''8    e''8    d''8    c''8    a'8    g'8    
\bar "|"     a'8    d''4. ^"~"    d''8    f''8    e''8    d''8    \bar ":|."   
e'8    g'8    c''8    e''8    d''8    c''8    a'8    g'8    \bar "|"   f'8    
e'8    d'8    f'8    e'8    c'8    c'4    \bar "||"     \grace {    b'8  }   
c''4    g''8    c''8    a''8    c''8    g''8    c''8    \bar "|"   f''8    d''8 
   e''8    c''8    d''8    c''8    b'8    c''8    \bar "|"   g'8    c''8    
c''4 ^"~"    g'8    c''8    e''8    g''8    \bar "|"   a''8    e''8    g''8    
e''8    d''4    e''8    g''8    \bar "|"     a''8    e''8  \tuplet 3/2 {   e''8  
  e''8    e''8  }   g''8    e''8    d''8    c''8    \bar "|"   c''8    d''8  
\tuplet 3/2 {   e''8    d''8    c''8  }   d''8    c''8    a'8    g'8    \bar "|" 
  e'8    g'8    c''8    e''8    d''8    c''8    a'8    g'8    \bar "|"   f'8    
e'8    d'8    f'8    e'8    c'8    c'4    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
