\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "marcpipes"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/46#setting23298"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/46#setting23298" {"https://thesession.org/tunes/46#setting23298"}}}
	title = "Humours Of Whiskey, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key b \minor   \repeat volta 2 {   g''8 ^"Em"   fis''8    e''8      
fis''8 ^"Bm"   b'8    b'8    fis''8    b'8    b'8  \bar "|"   g''8 ^"Em"   
fis''8    e''8      fis''8 ^"Bm"   b'8    b'8      fis''8 ^"F\#m"   g''8    
a''8  \bar "|"   g''8 ^"Em"   fis''8    e''8      fis''8 ^"Bm"   b'8    b'8     
 e''8 ^"G"   fis''8    g''8  \bar "|"     a''8 ^"A"   g''8    fis''8    e''8    
fis''8    d''8    cis''8    b'8    a'8  }   \key d \major   \repeat volta 2 {   
d''4 ^"D"   e''8    fis''8    d''8    fis''8      e''8 ^"A"   cis''8    a'8  
\bar "|"   d''4 ^"D"   e''8    fis''8    d''8    d''8      g''8 ^"G"   fis''8   
 e''8  \bar "|"   d''4 ^"D"   e''8    fis''8    d''8    fis''8      e''8 ^"G"   
fis''8    g''8  \bar "|"     a''8 ^"A"   g''8    fis''8    e''8    fis''8    
d''8    cis''8    b'8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
