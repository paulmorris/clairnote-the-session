\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "stanton135"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/16#setting21856"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/16#setting21856" {"https://thesession.org/tunes/16#setting21856"}}}
	title = "Clumsy Lover, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \repeat volta 2 {   d''4    \bar "|"   cis''8    
e''8    e''8    fis''8    e''8    cis''8    b'8    a'8    \bar "|"   d''8    
fis''8    fis''8    d''8    a''8    fis''8    d''8    fis''8    \bar "|"   
cis''8    e''8    e''8    fis''8    e''8    cis''8    b'8    a'8    \bar "|"   
d''4    b'8    cis''8    d''8    e''8    cis''8    b'8    \bar "|"     cis''8   
 e''8    e''8    fis''8    e''8    cis''8    b'8    a'8    \bar "|"   d''8    
fis''8    fis''8    d''8    g''8    fis''8    e''8    d''8    \bar "|"   cis''8 
   e''8    e''8    cis''8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8 
   b'8    a'4    }     \repeat volta 2 {   d''4    \bar "|"   cis''8    e''8    
e''8    a''8    a''8    e''8    cis''8    a'8    \bar "|"   d''8    a'8    d''8 
   fis''8    a''8    fis''8    d''8    fis''8    \bar "|"   cis''8    e''8    
e''8    a''8    a''8    e''8    cis''8    a'8    \bar "|"   d''4    b'8    
cis''8    d''8    e''8    cis''8    b'8    \bar "|"     cis''8    e''8    e''8  
  a''8    a''8    e''8    cis''8    a'8    \bar "|"   d''8    a'8    d''8    
fis''8    a''8    fis''8    d''8    fis''8    \bar "|"   cis''8    e''8    e''8 
   cis''8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8    b'8    a'4   
 }     \repeat volta 2 {   d''4    \bar "|"   cis''8    e''8    e''8    cis''8  
  e''8    e''8    cis''8    e''8    \bar "|"   d''8    fis''8    fis''8    d''8 
   fis''8    fis''8    d''8    fis''8    \bar "|"   cis''8    e''8    e''8    
cis''8    e''8    e''8    cis''8    e''8    \bar "|"   d''8    b'8    b'8    
cis''8    d''8    e''8    cis''8    b'8    \bar "|"     cis''8    e''8    e''8  
  cis''8    e''8    e''8    cis''8    e''8    \bar "|"   d''8    fis''8    
fis''8    d''8    fis''8    fis''8    d''8    fis''8    \bar "|"   cis''8    
e''8    e''8    cis''8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8    
b'8    a'4    }     \repeat volta 2 {   d''4    \bar "|"   cis''8    a''8    
a''8    e''8    a''8    e''8    cis''8    a'8    \bar "|"   d''8    a''8    
a''8    d''8    a''8    a'8    d''8    a''8    \bar "|"   cis''8    a''8    
a''8    e''8    a''8    e''8    cis''8    a'8    \bar "|"   d''4    b'8    
cis''8    d''8    e''8    cis''8    b'8    \bar "|"     cis''8    a''8    a''8  
  e''8    a''8    e''8    cis''8    a'8    \bar "|"   d''8    a''8    a''8    
d''8    a''8    a'8    d''8    a''8    \bar "|"   cis''8    e''8    e''8    
cis''8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8    b'8    a'4    } 
    \repeat volta 2 {   d''4    \bar "|"   cis''8    e''8    e''8    a'8    
e''4    a'8    e''8    \bar "|"   d''4.    fis''8    g''8.    fis''16    e''8   
 d''8    \bar "|"   cis''8    e''8    e''8    a'8    e''4    a'8    e''8    
\bar "|"   d''8    b'8    b'8    cis''8    d''8    e''8    cis''8    b'8    
\bar "|"     cis''8    e''8    e''8    a'8    e''4    a'8    e''8    \bar "|"   
d''4.    fis''8    g''8.    fis''16    e''8    d''8    \bar "|"   cis''8    
e''8    e''8    a'8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8    
b'8    a'4    }     \repeat volta 2 {   d''4    \bar "|"   cis''8    a''8    
a''8    cis''8    a''8    a''8    cis''8    a''8    \bar "|"   d''8    a''8    
a''8    d''8    a''8    a''8    d''8    a''8    \bar "|"   cis''8    a''8    
a''8    cis''8    a''8    a''8    cis''8    a''8    \bar "|"   b'8    a''8    
cis''8    a''8    d''8    a''8    e''8    a''8    \bar "|"     cis''8    a''8   
 a''8    e''8    a''8    a''8    cis''8    a''8    \bar "|"   d''8    a''8    
a''8    fis''8    a''8    a''8    d''8    a''8    \bar "|"   cis''8    e''8    
e''8    cis''8    d''4    g'8    b'8    \bar "|"   b'8    a'8    a'8    b'8    
a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
