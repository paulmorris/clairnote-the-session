\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "EldKatt"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1030#setting1030"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1030#setting1030" {"https://thesession.org/tunes/1030#setting1030"}}}
	title = "Comb Your Hair And Curl It"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \dorian   \repeat volta 2 {   b'4    e'8    e'4    fis'8    
g'4    a'8    \bar "|"   b'8    cis''8    d''8    e''4    cis''8    d''4.  
\bar "|"   b'4    e'8    e'4    fis'8    g'4    a'8    \bar "|"   b'4. ^"~"    
a'8    b'8    g'8    fis'8    e'8    d'8  }     \repeat volta 2 {   g''4    
e''8    e''4    d''8    e''8    fis''8    g''8    \bar "|"   fis''4    d''8    
d''4    cis''8    d''8    e''8    fis''8  \bar "|"   g''4    e''8    e''4    
d''8    e''4    cis''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    
fis'8    g'4    e'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
