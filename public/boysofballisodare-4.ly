\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1340#setting26011"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1340#setting26011" {"https://thesession.org/tunes/1340#setting26011"}}}
	title = "Boys Of Ballisodare, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   d'4    g'8    g'4    a'8    b'4   
 d''8  \bar "|"   e''8    g''8    e''8    d''4    e''8    g''4.  \bar "|"   d'4 
   g'8    g'4    b'8    d''4    b'8  \bar "|"   a'8    g'8    a'8    b'4    g'8 
   e'4    g'8  \bar "|"     d'4    g'8    g'4    a'8    b'4    d''8  \bar "|"   
e''8    g''8    e''8    d''4    e''8    g''4    a''8  \bar "|"   b''4    a''8   
 g''4    e''8    d''4    b'8  \bar "|"   a'8    g'8    a'8    b'4    g'8    e'4 
   g'8  }     \repeat volta 2 {   b'4    d''8    d''4    b'8    d''4    b'8  
\bar "|"   d''4.    d''8    e''8    fis''8    g''4.  \bar "|"   b'4    d''8    
d''4    b'8    d''4    b'8  \bar "|"   a'8    g'8    a'8    b'4    g'8    e'4   
 g'8    \bar "|"     b'4    d''8    d''4    b'8    d''4    b'8  \bar "|"   
d''4.    d''8    e''8    fis''8    g''4    a''8    \bar "|"   b''4    a''8    
g''4    e''8    d''4    b'8  \bar "|"   a'8    g'8    a'8    b'4    g'8    e'4  
  g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
