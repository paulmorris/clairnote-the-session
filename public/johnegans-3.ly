\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "robinlew"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/909#setting14094"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/909#setting14094" {"https://thesession.org/tunes/909#setting14094"}}}
	title = "John Egan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   \bar "|"   c''8    fis'8    fis'4    a'8    
fis'8    e'8    fis'8    \bar "|"   a'8    fis'8    e'8    fis'8    a'4    a'8  
  b'8    \bar "|"   c''8    fis'8    fis'4    a'8    fis'8    e'8    fis'8    
\bar "|"   a'8    c''8    e''8    c''8    b'4    a'8    b'8    \bar "|"   c''8  
  fis'8    fis'4    a'8    fis'8    e'8    fis'8    \bar "|"   a'8    fis'8    
e'8    fis'8    a'4    a'8    b'8    \bar "|"   c''8    fis'8    fis'4    a'8   
 fis'8    e'8    fis'8    \bar "|"   a'8    c''8    e''8    c''8    b'4    c''8 
   b'8    \bar "||"   a'4    a'8    a'8    a'8    b'8    c''8    b'8    
\bar "|"   a'8    b'8    c''8    b'8    a'8    fis'8    e'8    fis'8    
\bar "|"   a'4    a'8    a'8    a'8    b'8    c''8    e''8    \bar "|"   fis''8 
   a''8    e''8    c''8    b'4    c''8    b'8    \bar "|"   a'4    a'8    a'8   
 a'8    b'8    c''8    b'8    \bar "|"   a'8    b'8    c''8    b'8    a'8    
fis'8    e'8    fis'8    \bar "|"   a'4    a'8    a'8    a'8    b'8    c''8    
e''8    \bar "|"   fis''8    a''8    e''8    c''8    b'4    a'8    b'8    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
