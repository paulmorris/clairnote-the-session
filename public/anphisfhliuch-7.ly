\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "myles"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/879#setting30065"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/879#setting30065" {"https://thesession.org/tunes/879#setting30065"}}}
	title = "An Phis Fhliuch"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \mixolydian   a'8  \bar "|"   fis'8    g'8    a'8  \grace {    
b'8  }   a'8    fis'8    a'8    c''4  \grace {    d''8  }   a'8  \bar "|"   b'8 
   a'8    g'8    e'8    fis'8    g'8  \grace {    a'8  }   g'8    e'8    d'8  
\bar "|"   fis'8    g'8    a'8  \grace {    cis''8  }   a'8    fis'8    a'8    
d''4    a'8  \bar "|"   d''8    g''16    fis''16    e''8    d''8    c''8    a'8 
   g'8    e'8    d'8  \bar "|"     \bar "|"   fis'8    g'8    a'8  \grace {    
b'8  }   a'8    fis'8    d''8    c''4    d''8  \bar "|"   b'8    a'8    g'8    
e'8    fis'8    g'8  \grace {    a'8  }   g'8    e'8    d'8  \bar "|"   fis'8   
 g'8    a'8    a'8    fis'8    a'8    d''4    a'8  \bar "|"   d''8    g''16    
fis''16    e''8    d''8    c''8    a'8    g'8    e'8    d'8  \bar "|"     
\repeat volta 2 {   d''8    cis''8    d''8    e''16    fis''16    g''8    e''8  
  c''!8  \grace {    d''8  }   c''8    a'8  \bar "|"   d''8    a'16    cis''16  
  d''8    fis''8    d''8    fis''8    g''8    fis''8    e''8  \bar "|"   a'8    
b'8    a'8    fis''8    e''8    d''8    c''8  \grace {    d''8  }   c''8    a'8 
 \bar "|"   b'8    a'8    g'8    e'8    fis'8    g'8  \grace {    a'8  }   g'8  
  e'8    d'8  }     \repeat volta 2 {   fis'8    g'8    a'8  \grace {    cis''8 
 }   a'8    fis'8    d''8    a'8    fis'8    d''8  \bar "|"   a'8    fis'8    
d''8    a'8    fis'8    d''8    g'8    e'8    d'8  \bar "|"   fis'8    g'8    
a'8  \grace {    b'8  }   a'8    fis'8    a'8    c''4  \grace {    d''8  }   
a'8  \bar "|"   b'8    a'8    g'8    e'8    fis'8    g'8  \grace {    a'8  }   
g'8    e'8    d'8  }     \bar "|"   d'4. ^"~"    d'4. ^"~"    c''4.  \bar "|"   
c''4    b'8    c''4    a'8    g'8    e'8    d'8  \bar "|"   d'4. ^"~"    d'4. 
^"~"    d''4    cis''8  \bar "|"   d''8    g''16    fis''16    e''8    d''8    
c''8    a'8    g'8    e'8    d'8  \bar "|"     \bar "|"   d'4. ^"~"    d'4. 
^"~"    c''4    r8 \bar "|"   c''4    b'8    c''8    r8   a'8    g'8    e'8    
d'8  \bar "|"   d'8    a'8    d'8  \grace {    a'8  }   d'8    a'8    d'8    
d''4    cis''8  \bar "|"   d''8    g''16    fis''16    e''8    d''8    c''8    
a'8    g'8    e'8    d'8  \bar "|"     \repeat volta 2 {   d''8    cis''8    
d''8    e''16    fis''16    g''8    e''8    c''!8  \grace {    d''8  }   c''8   
 a'8  \bar "|"   d''8    a'16    cis''16    d''8    fis''8    d''8    fis''8    
g''8    fis''8    e''8  \bar "|"   a'8    b'8    a'8    fis''8    e''8    d''8  
  c''8  \grace {    d''8  }   c''8    a'8  \bar "|"   b'8    a'8    g'8    e'8  
  fis'8    g'8  \grace {    a'8  }   g'8    e'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
