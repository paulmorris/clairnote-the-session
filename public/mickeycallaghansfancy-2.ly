\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Toni Ribas"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/774#setting13895"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/774#setting13895" {"https://thesession.org/tunes/774#setting13895"}}}
	title = "Mickey Callaghan's Fancy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   b'8    a'8    \bar "|"   g'8    e'8    d'8    e'8    
g'8    a'8    b'8    a'8    \bar "|"   g'8    e'8    d'8    e'8    g'8    a'8   
 b'8    c''8    \bar "|"   d''4.    e''8    d''8    b'8    g'8    b'8    
\bar "|"   a'4    a'8    g'8    a'4    b'8    a'8    \bar "|"   g'8    e'8    
d'8    e'8    g'8    a'8    b'8    a'8    \bar "|"   g'8    e'8    d'8    e'8   
 g'8    a'8    b'8    c''8    \bar "|"   d''4.    e''8    d''8    b'8    g'8    
b'8    \bar "|"   a'4    g'4    g'4    } \repeat volta 2 {   b'8    d''8    
\bar "|"   e''4.    fis''8    e''8    d''8    b'8    c''8    \bar "|"   d''4.   
 e''8    d''8    b'8    g'8    b'8    \bar "|"   c''4    b'8    a'8    b'4    
a'8    g'8    \bar "|"   e'8    a'8    a'8    g'8    a'4    b'8    a'8    
\bar "|"   g'8    e'8    d'8    e'8    g'8    a'8    b'8    a'8    \bar "|"   
g'8    e'8    d'8    e'8    g'8    a'8    b'8    c''8    \bar "|"   d''4.    
e''8    d''8    b'8    g'8    b'8    \bar "|"   a'4    g'4    g'4    }   r1   
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
