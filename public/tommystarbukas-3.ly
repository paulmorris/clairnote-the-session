\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Earl Adams"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/140#setting12762"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/140#setting12762" {"https://thesession.org/tunes/140#setting12762"}}}
	title = "Tommy's Tarbukas"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \minor   \repeat volta 2 {   e''4    d''8    e''8    c''8    
a'8    a'8    c''8    \bar "|"   b'8    g'8    d''8    g'8    e''8    g'8    
d''8    g'8    \bar "|"   e''4    d''8    e''8    c''8    a'8    a'8    c''8    
\bar "|"   b'8    g''8    d''8    b'8    c''8    a'8    a'8    g'8    \bar "|"  
 \bar "|"   a'4    e''8    d''8    c''8    a'8    a'8    d''8    \bar "|"   b'8 
   g'8    d''8    g'8    e''8    g'8    d''8    g''8    \bar "|"   e''4    d''8 
   e''8    c''8    a'8    a'8    c''8    \bar "|"   b'8    g''8    d''8    b'8  
  c''8    a'8    a'8    d''8    }   \bar "|"   e'8    a'8    c''8    a'8    f'8 
   a'8    c''8    a'8    \bar "|"   g'8    b'8    d''8    c''8    b'8    g''8   
 d''8    b'8    \bar "|"   e'8    a'8    c''8    a'8    f'8    a'8    c''8    
a'8    \bar "|"   b'8    g''8    d''8    b'8    c''8    a'8    a'8    c''8    
\bar "|"   \bar "|"   e'8    a'8    c''8    a'8    f'8    a'8    c''8    a'8    
\bar "|"   g'8    b'8    d''8    c''8    b'8    g''8    g''8    gis''8    
\bar "|"   a''8    e''8    d''8    e''8    c''8    a'8    g'8    f'8    
\bar "|"   e'8    a'8    g'8    b'8    c''8    a'8    a'4    \bar "|"   
\bar "|"   e'8    a'8    c''8    a'8    f'8    a'8    c''8    a'8    \bar "|"   
g'8    b'8    d''8    c''8    b'8    g''8    d''8    b'8    \bar "|"   e'8    
a'8    c''8    a'8    f'8    a'8    c''8    a'8    \bar "|"   b'8    g''8    
d''8    b'8    c''8    a'8    a'8    c''8    \bar "|"   \bar "|"   e'8    a'8   
 c''8    a'8    f'8    a'8    c''8    a'8    \bar "|"   g'8    b'8    d'8    
c''8    b'8    g''8    g''8    gis''8    \bar "|"   a''8    \tuplet 3/2 {   a''8 
   a''8    a''8  }   a''8    f''8    e''8    d''8    \bar "|"   e''8    d''8    
c''8    b'8    a'4    b'8    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
