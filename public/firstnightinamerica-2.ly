\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/285#setting26019"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/285#setting26019" {"https://thesession.org/tunes/285#setting26019"}}}
	title = "First Night In America, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   g'4. ^"~"    a'8    fis'8    d'8  \bar "|"   g'4. 
^"~"    b'8    c''8    d''8  \bar "|"   e''4    g''8    d''8    b'8    g'8  
\bar "|"   a'8    b'8    g'8    a'8    fis'8    d'8  \bar "|"     g'4. ^"~"    
a'8    fis'8    d'8  \bar "|"   g'4. ^"~"    b'8    c''8    d''8  \bar "|"   
e''4    g''8    d''8    b'8    g'8  \bar "|"   a'8    g'8    fis'8    g'8    
b'8    d'8  \bar ":|."   a'8    g'8    fis'8    g'8    b'8    d''8  \bar "||"   
  g''4. ^"~"    b''8    g''8    e''8  \bar "|"   d''8    cis''8    d''8    e''8 
   cis''8    a'8  \bar "|"   b'4    g''8    g''8    fis''8    g''8  \bar "|"   
a''8    b''8    g''8    a''8    fis''8    d''8  \bar "|"     g''4. ^"~"    b''8 
   g''8    e''8  \bar "|"   d''4. ^"~"    e''8    cis''8    a'8  \bar "|"   b'8 
   d''8    g''8    d''8    b'8    g'8  \bar "|"   a'8    g'8    fis'8    g'8    
b'8    d''8  \bar ":|."   a'8    g'8    fis'8    g'4.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
