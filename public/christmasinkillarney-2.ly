\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SueH"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/434#setting13297"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/434#setting13297" {"https://thesession.org/tunes/434#setting13297"}}}
	title = "Christmas In Killarney"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'8  \repeat volta 2 {   a'4    d'8    d'4    b'8  
\bar "|"   a'4    d'8    d'4    a'8  \bar "|"   b'8    a'8    g'8    b'8    a'8 
   g'8  \bar "|"   a'4    d'8    d'4    a'8  \bar "|"   b'4    cis''8    d''4   
 cis''8  \bar "|"   b'4    a'8    fis'4    d'8  \bar "|"   e'8    fis'8    e'8  
  d'4    cis'8  } \alternative{{   d'4.    d'4    b'8  } {   d'4.    d'4    e'8 
 \bar "||"   fis'4    b'8    b'4    cis''8  \bar "|"   d''4    cis''8    b'4    
cis''8  \bar "|"   d''8    cis''8    b'8    cis''4    a'8  \bar "|"   fis'4.    
a'4    fis'8  \bar "|"   e'4    a'8    a'4    b'8  \bar "|"   cis''4    e'8    
a'4    cis''8  \bar "|"   d''8    e''8    d''8    cis''4    b'8  \bar "|"   
a'4.    a'4    b'8  \bar "|"   a'4    d'8    d'4    b'8  \bar "|"   a'4    d'8  
  d'4    a'8  \bar "|"   b'8    a'8    g'8    b'8    a'8    g'8  \bar "|"   a'4 
   d'8    d'4    a'8  \bar "|"   b'4    cis''8    d''4    cis''8  \bar "|"   
b'4    a'8    fis'4    d'8  \bar "|"   fis'4    d'8    fis'8    g'8    a'8  
\bar "|"   b'4.    b'4    cis''8  \bar "|"   d''4    cis''8    d''4    b'8  
\bar "|"   a'4    d''8    fis'4    d'8  \bar "|"   e'8    fis'8    e'8    d'4   
 cis'8  \bar "|"   d'2.  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
