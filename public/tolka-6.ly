\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jesse"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1251#setting30036"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1251#setting30036" {"https://thesession.org/tunes/1251#setting30036"}}}
	title = "Tolka, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   a'8.    b'16    c''8    g''8    \bar "|"   d''8    
e''8    c''8    a'8    \bar "|"   b'16    c''16    b'16    a'16    g'8    d''8  
  \bar "|"   a'8    b'8    g'8    e'8    \bar "|"     a'8.    b'16    c''8    
g''8    \bar "|"   d''8    e''8    c''8    a'8    \bar "|"   b'16    c''16    
b'16    a'16    g'8    d''8    \bar "|"   b'8    a'8    a'4    \bar "||"     
d''8.    e''16    fis''8    a''8    \bar "|"   g''8    a''8    fis''8    d''8   
 \bar "|"   g''16    e''16    d''16    c''16    b'8    d''16    c''16    
\bar "|"   b'16    a'16    g'8    b'16    a'16    g'16    b'16    \bar "|"     
a'8.    b'16    c''8    g''8    \bar "|"   d''8    e''8    c''8    a'8    
\bar "|"   b'16    c''16    b'16    a'16    g'8    d''8    \bar "|"   b'8    
a'8    a'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
