\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "airport"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/217#setting12899"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/217#setting12899" {"https://thesession.org/tunes/217#setting12899"}}}
	title = "Orphan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   e'4. ^"~"    e'8    d'8    b8  \bar "|"   g'8    
fis'8    g'8    a'4.  \bar "|"   b'4.    a'8    b'8    a'8  \bar "|"   g'8    
e'8    fis'8    e'8    d'8    b8  \bar "|"   a4. ^"~"    e'8    d'8    b8  
\bar "|"   g'8    fis'8    g'8    a'8    g'8    a'8  \bar "|"   b'4.    a'8    
b'8    a'8  \bar "|"   g'8    e'8    d'8    e'8    g'8    fis'8  \bar ":|."   
g'8    e'8    d'8    e'4.  \bar "|"   e''8    fis''8    e''8    d''8    b'8    
a'8  \bar "|"   g'8    a'8    b'8    d''4.  \bar "|"   e''8    fis''8    e''8   
 d''8    b'8    a'8  \bar "|"   b'8    e'8    fis'8    e'8    d'8    b8  
\bar "|"   a4. ^"~"    e'8    d'8    b8  \bar "|"   g'8    fis'8    g'8    a'8  
  g'8    a'8  \bar "|"   b'4. ^"~"    a'8    b'8    a'8  \bar "|"   g'8    e'8  
  d'8    e'4.  \bar ":|."   g'8    e'8    d'8    e'8    g'8    fis'8  \bar "||" 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
