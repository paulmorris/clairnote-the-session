\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/240#setting240"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/240#setting240" {"https://thesession.org/tunes/240#setting240"}}}
	title = "Silver Spire, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8    e'8  \bar "|"   d'4    
fis'8    e'8    d'8    fis'8    a'8    cis''8  \bar "|"   d''8    cis''8    
d''8    fis''8    e''8    d''8    a'8    fis'8  \bar "|"   g'8    a'8    b'8    
g'8    fis'8    g'8    a'8    fis'8  \bar "|"   g'8    fis'8    e'8    d'8    
cis'8    e'8    a8    cis'8  \bar "|"     d'4    fis'8    e'8    d'8    fis'8   
 a'8    cis''8  \bar "|"   d''4    d''8    fis''8    e''8    d''8    a'8    
fis'8  \bar "|"   g'4    b'8    g'8    fis'4    a'8    fis'8  \bar "|"   e'8    
a8    cis'8    e'8    d'4  }     \repeat volta 2 {   cis'8    b8  \bar "|"   a8 
   b8    cis'8    d'8    e'8    fis'8    g'8    e'8  \bar "|"   g'8    fis'8    
e'8    fis'8    g'8    a'8    b'8    cis''8  \bar "|"   d''8    cis''8    b'8   
 a'8    b'8    cis''8    d''8    e''8  \bar "|"   fis''4    g''8    fis''8    
e''4    \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"     d''4    fis''8    
d''8    cis''4    e''8    cis''8  \bar "|"   d''8    cis''8    d''8    a'8    
b'8    a'8    fis'8    d'8  \bar "|"   g'4    b'8    g'8    fis'4    a'8    
fis'8  \bar "|"   e'8    a8    cis'8    e'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
