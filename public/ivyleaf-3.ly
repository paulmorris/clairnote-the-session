\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Othannen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1112#setting14362"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1112#setting14362" {"https://thesession.org/tunes/1112#setting14362"}}}
	title = "Ivy Leaf, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   a'4 ^"~"    a'8    e''8    cis''8    a'8    e''8 
   cis''8    \bar "|"   a'8    e''8    cis''8    e''8    d''8    b'8    g'8    
b'8    \bar "|"   a'4    a'8    e''8    cis''8    a'8    e''8    cis''8    
\bar "|"   a'8    g'8    e'8    fis'8    g'8    e'8    d'4    \bar "|"   a'8    
e''8    e''4 ^"~"    cis''8    e''8    e''4 ^"~"    \bar "|"   a''8    e''8    
e''4 ^"~"    d''8    b'8    g'8    b'8    \bar "|"   a'4    a'8    e''8    
cis''8    a'8    e''8    cis''8  \bar "|"   a'8    g'8    e'8    fis'8    g'8   
 a'8    b'16    cis''16    d''8  \bar "|"   e''4    a''8    g''8    e''8    
g''8    fis''8    a''8    \bar "|"   g''4 ^"~"    g''8    e''8    d''8    g'8   
 b'16    cis''16    d''8    \bar "|"   e''4    a''8    g''8    e''8    d''8    
ces''8    a'8    \bar "|"   b'8    g'8    e'8    fis'8    g'8    a'8    b'16    
cis''16    d''8    \bar "|"   e''4    a''8    g''8    e''8    g''8    fis''8    
a''8    \bar "|"   g''4 ^"~"    g''8    e''8    d''8    b'8    g'8    b'8    
\bar "|"   a'4    a'8    b'8    ces''8    b'8    ces''8    d''8    \bar "|"   
e''4    fis''8    a''8    g''8    e''8    d''8    b'8    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
