\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/215#setting12889"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/215#setting12889" {"https://thesession.org/tunes/215#setting12889"}}}
	title = "Mairtin O'Connor's Flying Clog"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g4    g16    g16    g8    d'8    
g8    b8    d'8    \bar "|"   e'8    g8    c'8    e'8    d'8    b8    a8    b8  
  \bar "|"   g4    g16    g16    g8    b8    g8    a8    b8    \bar "|"     c'8 
^"D"     e8 ^"U"     e16 ^"D"   e16    e8    g8    e8    d8    e8    \bar "|"   
g4    g16    g16    g8    d'8    g8    b8    d'8    \bar "|"   e'8    g8    c'8 
   e'8    d'8    b8    a8    b8    \bar "|"     c'8 ^"U"     e8 ^"D"     e16 
^"D"   e16    e8    g8    e8    d8    e8    } \alternative{{   d8    fis8    a8 
   d'8    b8    d'8    a8    b8    } }    \repeat volta 2 {   b8    d'8    d'16 
   d'16    d'8    e'8    d'8    g'8    d'8    \bar "|"   e'8    d'8    g'8    
d'8    e'8    g'8    d'8    c'8    \bar "|"   b8    d'8    d'16    d'16    d'8  
  e'8    d'8    g'8    d'8    \bar "|"   e'8    a'8    a'8    g'8    fis'8    
e'8    d'8    c'8    \bar "|"   b8    c'8    d'8    b8    b8    c'8    c'8    
e8    \bar "|"   e16    e16    e8    c'8    e8    d8    d8    b8    d8    
\bar "|"   c'8    e8    e16    e16    e8    g8    e8    d8    e8    
} \alternative{{   d8    fis8    a8    d'8    b8    g8    g8    a8    } }    
\repeat volta 2 {   g'4    g'16    g'16    g'8    d''8    g'8    b'8    d''8    
\bar "|"   e''8    g'8    c''8    e''8    d''8    b'8    a'8    b'8    \bar "|" 
  g'4    g'16    g'16    g'8    b'8    g'8    a'8    b'8    \bar "|"   c''8    
e'8    e'16    e'16    e'8    g'8    e'8    d'8    e'8    \bar "|"   g'4    
g'16    g'16    g'8    d''8    g'8    b'8    d''8    \bar "|"   e''8    g'8    
c''8    e''8    d''8    b'8    a'8    b'8    \bar "|"   c''8    e'8    e'16    
e'16    e'8    g'8    e'8    d'8    e'8    } \alternative{{   d'8    fis'8    
a'8    d''8    b'8    d''8    a'8    b'8    } }    \repeat volta 2 {   b'8    
d''8    d''16    d''16    d''8    e''8    d''8    g''8    d''8    \bar "|"   
e''8    d''8    g''8    d''8    e''8    g''8    d''8    c''8    \bar "|"   b'8  
  d''8    d''16    d''16    d''8    e''8    d''8    g''8    d''8    \bar "|"   
e''8    a''8    a''8    g''8    fis''8    e''8    d''8    c''8    \bar "|"   
b'8    c''8    d''8    b'8    b'8    c''8    c''8    e'8    \bar "|"   e'16    
e'16    e'8    c''8    e'8    d'8    d'8    b'8    d'8    \bar "|"   c''8    
e'8    e'16    e'16    e'8    g'8    e'8    d'8    e'8    } \alternative{{   
d'8    fis'8    a'8    d''8    b'8    g'8    g'8    a'8    } }    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
