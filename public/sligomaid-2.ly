\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Max Becher"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/399#setting13245"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/399#setting13245" {"https://thesession.org/tunes/399#setting13245"}}}
	title = "Sligo Maid, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   a'4    b'8    a'8    b'8    d''8 
   e''8    fis''8    \bar "|"   g''8    e''8    d''8    b'8    a'8    g'8    
e'8    fis'8    \bar "|"   g'4    b'8    g'8    d'8    g'8    b'8    g'8    
\bar "|"   d'8    e'8    g'8    a'8    b'8    d''8    d''8    b'8    \bar "|"   
a'4    b'8    a'8    b'8    d''8    e''8    fis''8    \bar "|"   g''8    e''8   
 d''8    b'8    a'8    g'8    e'8    g'8    \bar "|"   a'8    b'4.    a'8    
e'8    g'8    e'8    \bar "|"   d'8    e'8    g'8    a'8    b'4    a'8    b'8   
 }   \repeat volta 2 {   e''8    a''8    a''8    g''8    a''4    g''8    a''8   
 \bar "|"   bis''8    a''8    g''8    g''8    fis''8    g''8    e''8    d''8    
g''8    \bar "|"   e''8    g''8    g''8    fis''8    g''4    g''8    e''8    
\bar "|"   d''8    e''8    g''8    a''8    b''8    g''8    a''8    g''8    
\bar "|"   e''8    a''8    a''8    g''8    a''4    g''8    a''8    \bar "|"   
bis''8    a''8    g''8    g''8    fis''8    g''8    e''8    d''8    g''8    
\bar "|"   e''8    g''8    g''4    e''8    d''8    b'8    c''8    \bar "|"   
d''8    b'8    g''8    b'8    b'8    a'8    a'4    }   \repeat volta 2 {   a'4  
  b'8    a'8    b'8    d''8    e''8    g''8    \bar "|"   g''8    e''8    d''8  
  b'8    a'8    g'8    e'8    fis'8    \bar "|"   g'4.    b'8    a'8    e'8    
e'4    \bar "|"   d'8    e'8    g'8    a'8    b'8    d''8    d''8    b'8    
\bar "|"   a'4    b'8    a'8    b'8    d''8    e''8    g''8    \bar "|"   g''8  
  e''8    d''8    b'8    a'8    g'8    e'8    g'8    \bar "|"   a'8    b'4.    
a'8    e'8    g'8    e'8    \bar "|"   d'8    e'8    g'8    a'8    b'8    a'8   
 a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
