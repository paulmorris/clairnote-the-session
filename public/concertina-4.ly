\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Earl Adams"
	crossRefNumber = "3"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/18#setting23829"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/18#setting23829" {"https://thesession.org/tunes/18#setting23829"}}}
	title = "Concertina, The — The Concertina"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major \time 4/4 \key d \major   \repeat volta 2 {   a'4    
fis'8    a'8    b'8    a'8    fis'8    a'8  \bar "|"   a'4    fis'8    a'8    
b'8    a'8    fis'8    a'8  \bar "|"   b'4    cis''8    a'8    b'8    a'8    
cis''8    a'8  \bar "|"   b'8    a'8    cis''8    a'8    b'8    a'8    fis'8    
g'8  \bar "|"     a'4    fis'8    a'8    b'8    a'8    fis'8    a'8  \bar "|"   
a'4    fis'8    a'8    b'8    a'8    fis'8    e'8  \bar "|"   d'4    b'8    
cis''8    \tuplet 3/2 {   d''8    d''8    d''8  }   d''8    b'8  \bar "|"   a'8  
  fis'8    e'8    fis'8    d'2  }     \repeat volta 2 {   a'8    d''8    d''4 
^"~"    a'8    d''8    d''4 ^"~"  \bar "|"   a'8    d''8    d''8    a'8    b'8  
  a'8    fis'8    a'8  \bar "|"   b'4    cis''8    a'8    b'8    a'8    cis''8  
  a'8  \bar "|"   b'8    a'8    cis''8    a'8    b'8    a'8    fis'8    g'8  
\bar "|"     a'8    d''8    d''4 ^"~"    a'8    d''8    d''4 ^"~"  \bar "|"   
a'8    d''8    d''8    cis''8    b'8    a'8    fis'8    e'8  \bar "|"   d'4    
fis'8    a'8    b'8    cis''8    d''8    b'8  \bar "|"   a'8    fis'8    e'8    
fis'8    d'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
