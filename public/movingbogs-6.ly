\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/716#setting28042"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/716#setting28042" {"https://thesession.org/tunes/716#setting28042"}}}
	title = "Moving Bogs, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   a8  \bar "|"   d'4    fis'8    a'8  \grace {    
cis''8  }   d''4    a'8 (   cis''8  -) \bar "|" \grace {    cis''8  }   d''4    
fis''8 (   d''8  -)   fis''8    e''8  \grace {    cis''8  }   d''8    cis''8  
\bar "|" \grace {    cis''8  }   d''4    a'8 (   d''8  -)   b'8 ^\trill   a'8  
\grace {    fis'8  }   g'8    fis'8  \bar "|"   e'4 ^\trill   a'8 (   e'8  -) 
\grace {    g'8  }   fis'8    d'8    d'8  }     a'8  \bar "|" \grace {    
cis''8  }   d''8    e''8    fis''8    g''8  \grace {    fis''8    g''8  }   
a''4    fis''8 (   d''8  -) \bar "|"   g''4    b''8 (   g''8  -) \grace {    
g''8    fis''8  }   e''4  \grace {    d''8  }   cis''8 (   a'8  -) \bar "|" 
\grace {    cis''8  }   d''8    e''8    fis''8    g''8  \grace {    fis''8    
g''8  }   a''4    fis''8 (   d''8  -) \bar "|"   a'8    g''8    fis''8 ^\trill  
 e''8    fis''8    d''8    d''8    a'8  \bar "|"     \grace {    cis''8  }   
d''8    e''8    fis''8    g''8    fis''8    a''8    fis''8    d''8  \bar "|" 
\grace {    fis''8  }   g''4    b''8 (   g''8  -)   e''8    e''8  \grace {    
d''8  }   cis''8    a'8  \bar "|" \grace {    cis''8  }   d'4    a'8 (   d''8  
-)   b'8 ^\trill   a'8  \grace {    fis'8  }   g'8    fis'8  \bar "|"   e'4 
^\trill   a'8 (   e'8  -) \grace {    g'8  }   fis'8    d'8    d'8  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
