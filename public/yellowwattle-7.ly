\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dalta na bPíob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1237#setting29755"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1237#setting29755" {"https://thesession.org/tunes/1237#setting29755"}}}
	title = "Yellow Wattle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \mixolydian   \repeat volta 2 {   d''8    cis''8    a'8    a'4 
   cis''8  \bar "|"   d''8    cis''8    a'8    g'8    e'8    d'8  \bar "|"   
d''8    cis''8    a'8    a'4    d'8  \bar "|"   g'4    g'8    g'8    e'8    d'8 
 \bar "|"     d''8    cis''8    a'8    a'4    cis''8  \bar "|"   d''8    cis''8 
   a'8    g'8    e'8    d'8  \bar "|"   e'4    e'8    c''8    g'8    e'8  
\bar "|"   e'8    d'8    d'8    d'4    a'8  }     d'16    e'16    fis'16    
g'16    a'16    b'16    c''4.  \bar "|"   a'8    d''8    b'8    c''8    a'8    
g'8  \bar "|"   a'8    b'8    cis''8    d''8    e''8    d''8  \bar "|"   d''8   
 fis''8    d''8    a'8    fis'8    d'8  \bar "|"     d'4.    c''4.  \bar "|"   
a'8    d''8    b'8    c''8    a'8    g'8  \bar "|"   e'4    e'8    c''8    g'8  
  e'8  \bar "|"   e'8    d'8    d'8    d'4    a'8  \bar "|"     d'8    fis'16   
 g'16    a'8    c''4.  \bar "|"   a'8    d''8    b'8    c''8    a'8    g'8  
\bar "|"   a'8    b'16    cis''16    d''8    d''8    e''8    d''8  \bar "|"   
d''8    a'8    fis'8    d'8    fis'8    a'8  \bar "|"     d'16    e'16    
fis'16    g'16    a'16    b'16    c''4.  \bar "|"   a'8    d''8    b'8    c''8  
  a'8    g'8  \bar "|"   e'4    e'8    c''8    d''8    e''8  \bar "|"   d''8    
c''8    a'8    g'8    e'8    d'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
