\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Yooval"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/257#setting27612"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/257#setting27612" {"https://thesession.org/tunes/257#setting27612"}}}
	title = "Mother's Delight"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   f'4    f'8    f'8    e'8    c'8    c'8    e'8  
\bar "|"   f'4    f'8    f'8    e'8    c'8    c'8    e'8  \bar "|"   d'8    
d''8    d''8    d''8    d''8    e''8    f''8    e''8  \bar "|"   d''4 ^"~"    
c''8    d''8    a'8    b'8    c''8    a'8  \bar "|"     d''4    d''8    c''8    
a'8    g'8    bes'8    b'!8  \bar "|"   c''8    b'8    c''8    g'8    e'8    
g'8    c'8    e'8  \bar "|"   d'8    d''8    d''8    c''8    a'8    g'8    a'8  
  b'8  \bar "|"   c''8    a'8    g'8    e'8    e'8    d'8    d'8    e'8  
\bar "|"     f'8    e'8    f'8    d'8    g'8    f'8    g'8    e'8  \bar "|"   
a'8    gis'8    a'8    c''8    g'!8    e'8    d'8    e'8  \bar "|"   d'8    
d''8    d''8    d''8    d''8    e''8    f''8    e''8  \bar "|"   d''4 ^"~"    
c''8    d''8    a'8    b'8    c''8    a'8  \bar "|"     d''4    d''8    c''8    
a'8    g'8    bes'8    b'!8  \bar "|"   c''8    b'8    c''8    g'8    e'8    
g'8    c'8    e'8  \bar "|"   d'8    d''8    d''8    c''8    a'8    g'8    a'8  
  b'8  \bar "|"   c''8    a'8    g'8    e'8    e'8    d'8    d'8    e'8  
\bar "|"     \tuplet 3/2 {   f'8    e'8    f'8  }   d''8    a'8    f'8    d'8    
d'8    d'8  \bar "|"   e'8    c''8    c'8    g'8    e'8    c'8    c'8    c'8  
\bar "|"   f'8    d'8    d''8    a'8    f'8    d'8    d'8    d'8  \bar "|"   
e'8    d'8    c'8    e'8    cis'8    d'8    d'8    e'8  \bar "|"     
\tuplet 3/2 {   f'8    e'8    f'8  }   d''8    a'8    f'8    d'8    d'8    d'8  
\bar "|"   e'8    c''8    c'8    g'8    e'8    c'8    c'8    c'8  \bar "|"   
f'4    f'8    f'8    f'8    e'8    d'8    c'8  \bar "|"   a8    f'8    e'8    
f'8    cis'8    d'8    d'8    e'8  \bar "|"     \tuplet 3/2 {   f'8    e'8    
f'8  }   d''8    a'8    f'8    d'8    d'8    d'8  \bar "|" \tuplet 3/2 {   e'8   
 c'8    g'8  }   e''8    g'8    e'8    c'8    c'8    c'8  \bar "|"   f'8    d'8 
   d''8    a'8    f'8    d'8    d'8    d'8  \bar "|"   e'8    d'8    c'8    e'8 
   cis'8    d'8    d'8    e'8  \bar "|"     f'8    d'8    d''8    d'8    e''8   
 d'8    d''8    d'8  \bar "|"   c'8    c'8    c''8    c'8    d''8    c'8    g'8 
   c'8  \bar "|"   f'4    f'8    f'8    f'8    e'8    d'8    c'8  \bar "|"   a8 
   f'8    e'8    f'8    cis'8    d'8    d'8    e'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
