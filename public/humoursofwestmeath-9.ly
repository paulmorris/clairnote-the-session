\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ian Varley"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/428#setting29316"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/428#setting29316" {"https://thesession.org/tunes/428#setting29316"}}}
	title = "Humours Of Westmeath, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   d''4.    d''8    a'8    g'8    
fis'8    e'8    d'8    \bar "|"   d''4. ^"~"    d''8    a'8    fis'8    g'4. 
^"~"    \bar "|"   a'8    b'8    cis''8    d''8    a'8    g'8    fis'8    g'8   
 a'8    \bar "|"   c''8    b'8    c''8    e'4    fis'8    g'8    fis'8    e'8   
 }     d'4    g''8    fis''8    d''8    fis''8    e''8    cis''8    a'8    
\bar "|"   d''4    g''8    fis''8    d''8    fis''8    g''4.    \bar "|"   d''4 
   g''8    fis''8    d''8    fis''8    e''8    a'8    b'8    \bar "|"   c''8    
b'8    c''8    e'4    fis'8    g'8    fis'8    e'8    \bar "|"     d'4    g''8  
  fis''8    d''8    fis''8    e''8    cis''8    a'8    \bar "|"   d''4    g''8  
  fis''8    d''8    fis''8    g''4.    \bar "|"   a''8    fis''8    d''8    
g''8    e''8    cis''8    d''8    a'8    b'8    \bar "|"   c''8    b'8    c''8  
  e'4    fis'8    g'8    fis'8    e'8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
