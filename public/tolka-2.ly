\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1251#setting14561"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1251#setting14561" {"https://thesession.org/tunes/1251#setting14561"}}}
	title = "Tolka, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   e''8.    fis''16    g''8    
\bar "|"   d''8    e''8.    fis''16    g''8    \bar "|"   fis''8    d''8    
g''8.    e''16  \bar "|"   d''8    c''8    b'8    d''8 ( \bar "|"     d''16  -) 
  c''16    b'8    c''8    g''8  \bar "|"   d''8    e''8    c''8    a'8    
\bar "|"   b'16    c''16    b'16    a'16    g'8    d''8  \bar "|"   b'8    a'8  
  a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
