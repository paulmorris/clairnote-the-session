\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mark de Jong"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/957#setting957"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/957#setting957" {"https://thesession.org/tunes/957#setting957"}}}
	title = "Fanny Power"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   d'8  \bar "|"   g'4 ^"G"   d'8    
  g'8. ^"Em"   a'16    b'8  \bar "|"   c''4 ^"C"   b'8      a'4 ^"D"   g'8  
\bar "|"   fis'8. ^"C"   g'16    e'8      d'8. ^"D"   e'16    d'8  \bar "|"   
fis'4 ^"Bm"   g'8      a'8. ^"D"   b'16    c''8  \bar "|"       b'8. ^"G"   
a'16    g'8      b'8. ^"Em"   c''16    d''8  \bar "|"   e''4 ^"C"   a'8      
a'4 ^"D"   g'8  \bar "|"   fis'8. ^"C"   g'16    e'8      d'8. ^"D"   g'16    
fis'8  \bar "|"   g'4. ^"G"   g'4    }     \repeat volta 2 {   d''8  \bar "|"   
d''8 ^"G"   b'16    c''16    d''8    d''8    b'16    c''16    d''8  \bar "|"   
g'8. ^"Em"   a'16    g'8    g'8    b'8    d''8  \bar "|"   e''8 ^"C"   c''16    
d''16    e''8    e''8    c''16    d''16    e''8  \bar "|"   a'8. ^"D"   b'16    
a'8    a'8    b'8    c''8  \bar "|"       b'8. ^"G"   c''16    d''8      e''8. 
^"C"   fis''16    g''8  \bar "|"   fis''8. ^"D"   g''16    a''8    d''4    c''8 
 \bar "|"   b'8. ^"G"   a'16    g'8      a'16 ^"D"   b'16    c''8    fis'8  
\bar "|"   g'4. ^"G"   g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
