\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/466#setting19392"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/466#setting19392" {"https://thesession.org/tunes/466#setting19392"}}}
	title = "Maids Of Ardagh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   \repeat volta 2 {   fis'16    g'16    a'8    b'8    
a'8    \bar "|"   d''8    e''8    fis''4    \bar "|"   fis''8    e''16    
fis''16    g''8    cis''8    \bar "|"   e''8    d''8    b'8    a'8    \bar "|"  
 fis'16    g'16    a'8    b'8    a'8    \bar "|"   d''8    e''8    fis''4    
\bar "|"   fis''8    e''16    fis''16    g''8    cis''8    \bar "|"   e''8    
d''8    d''4    }   \repeat volta 2 {   e''8    fis''16    e''16    cis''8    
e''8    \bar "|"   fis''8    e''8    a''4    \bar "|"   e''8    fis''16    
e''16    cis''8    e''8    \bar "|"   fis''8    e''8    cis''16    b'16    a'8  
  \bar "|"   e''8    fis''16    e''16    cis''8    e''8    \bar "|"   fis''8    
e''8    a''4    \bar "|"   e''8    fis''16    e''16    d''8    b'8    \bar "|"  
 b'8    a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
