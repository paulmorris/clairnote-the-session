\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/852#setting14021"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/852#setting14021" {"https://thesession.org/tunes/852#setting14021"}}}
	title = "Leppadumdowledum"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \mixolydian   a'8    b'8    c''8    c''8    b'8    a'8    g'8  
  a'8    b'8  \bar "|"   b'8    a'8    g'8    c''8    g'8    e'8    g'8    e'8  
  d'8  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'4. ^"~"  \bar "|"   
d'4. ^"~"    g'8    fis'8    d'8    d'8    e'16    fis'16    g'8  \bar "|"   
a'8    b'8    c''8    c''8    d''8    e''8    d''8    c''8    a'8  \bar "|"   
b'8    a'8    g'8    d''8    b'8    g'8    c''8    g'8    e'8  \bar "|"   d'4. 
^"~"    g'8    fis'8    d'8    d'4. ^"~"  \bar "|"   d'4. ^"~"    g'8    fis'8  
  d'8    d'8    e'16    fis'16    g'8  \bar ":|."   d'4. ^"~"    g'8    fis'8   
 d'8    d'8    a'8    b'8  \bar "||"   \bar ".|:"   c''8    d''8    e''8    d''8 
   g''8    fis''8    e''8    a'8    c''8  \bar "|"   b'8    d''8    g'8    c''8 
   g'8    e'8    g'8    e'8    d'8  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8 
   d'4. ^"~"  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'8    a'8    b'8  
\bar "|"   c''8    d''8    e''8    d''8    g''8    fis''8    e''4    d''8  
\bar "|"   g''8    e''8    c''8    d''8    b'8    g'8    c''8    g'8    e'8  
\bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'4. ^"~"  \bar "|"   d'4. ^"~"  
  g'8    fis'8    d'8    d'8    a'8    b'8  \bar ":|."   d'4. ^"~"    g'8    
fis'8    d'8    d'8    c''8    d''8  \bar "||"   \bar ".|:"   e''8    fis''8    
g''8    e''8    a''8    fis''8    d''8    g''8    fis''8  \bar "|"   e''8    
a'8    c''8    b'8    d''8    g'8    c''8    g'8    e'8  \bar "|"   d'4. ^"~"   
 g'8    fis'8    d'8    d'4. ^"~"  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8  
  c''4    d''8  \bar "|"   e''4    d''8    g''8    fis''8    e''8    fis''8    
a''8    fis''8  \bar "|"   d''8    e''8    fis''8    g''8    d''8    b'8    
c''8    g'8    e'8  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'4. ^"~"  
\bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'8    c''8    d''8  \bar ":|."  
 d'4. ^"~"    g'8    fis'8    d'8    d''4    c''8  \bar "||"   \bar ".|:"   b'8  
  d''8    g''8    d''8    b'8    d''8    e''8    d''8    c''8  \bar "|"   b'8   
 d''8    g''8    d''8    b'8    c''8    d''8    b'8    g'8  \bar "|"   c''4    
b'8    a'4    g'8    a'8    d''8    b'8  \bar "|"   c''4    b'8    a'4    g'8   
 a'8    d''8    c''8  \bar "|"   b'8    d''8    g''8    d''8    b'8    d''8    
e''8    d''8    c''8  \bar "|"   b'8    d''8    g''8    a''8    fis''8    g''8  
  d''8    b'8    g'8  \bar "|"   c''4    b'8    a'4    g'8    a'8    d''8    
b'8  \bar "|"   c''4    b'8    a'4    g'8    a'8    d''8    c''8  \bar ":|."   
c''4    b'8    a'4    g'8    a'8    d''8    e''8  \bar "||"   \bar ".|:"   
fis''8    g''8    a''8    g''8    fis''8    e''8    d''8    a'8    b'8  
\bar "|"   c''8    d''8    e''8    d''8    b'8    g'8    c''8    g'8    e'8  
\bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'4. ^"~"  \bar "|"   c''4    
b'8    a'4    g'8    a'8    d''8    e''8  \bar "|"   fis''8    g''8    a''8    
g''8    fis''8    g''8    a''8    fis''8    d''8  \bar "|"   g''8    e''8    
c''8    d''8    b'8    g'8    c''8    g'8    e'8  \bar "|"   d'4. ^"~"    g'8   
 fis'8    d'8    d'4. ^"~"  \bar "|"   d'4. ^"~"    g'8    fis'8    d'8    d'8  
  d''8    e''8  \bar ":|."   d'4. ^"~"    g'8    fis'8    d'8    d'8    e'16    
fis'16    g'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
