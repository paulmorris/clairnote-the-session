\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1212#setting14508"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1212#setting14508" {"https://thesession.org/tunes/1212#setting14508"}}}
	title = "Croen A Ddafad Felan"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key e \minor   e'8    fis'8    g'8    a'8  \bar "|"   b'4    b'4  
\bar "|"   a'8    g'8    a'8    fis'8  \bar "|"   b'4    b'4  \bar "|"   e'8    
fis'8    g'8    a'8  \bar "|"   b'4    b'4  \bar "|"   a'8    g'8    a'8    
fis'8  \bar "|"   b'2  }   \repeat volta 2 {   a'8    g'8    fis'4  \bar "|"   
g'8    fis'8    e'4  \bar "|"   d'8    e'8    fis'8    g'8  \bar "|"   e'4    
e'4  \bar "|"   a'8    g'8    fis'4  \bar "|"   g'8    fis'8    e'4  \bar "|"   
d'8    e'8    fis'8    g'8  \bar "|"   e'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
