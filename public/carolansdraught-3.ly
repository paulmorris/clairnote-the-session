\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1421#setting20802"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1421#setting20802" {"https://thesession.org/tunes/1421#setting20802"}}}
	title = "Carolan's Draught"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g'16    a'16    b'16    c''16    d''16    e''16    
fis''16    d''16    g''8    fis''16    e''16    d''8    d''8    \bar "|"   e''8 
   e'8    d''8    d'8    c''16    b'16    a'16    g'16    fis'16    g'16    
a'16    fis'16    \bar "|"   g'16    a'16    b'16    c''16    d''16    e''16    
fis''16    d''16    g''8    fis''16    e''16    d''8    d''8    \bar "|"   
fis''8    e''16    d''16    a'8    cis''8    d''2    }     b''16    a''16    
g''16    fis''16    g''16    a''16    b''16    g''16    fis''16    g''16    
a''16    fis''16    dis''8    cis''16    b'16    \bar "|"   e''8    fis''16    
g''16    a''16    fis''16    g''16    e''16    dis''16    e''16    fis''16    
dis''16    b'8    g''16    a''16    \bar "|"   b''8    b'8    a''8    a'8    
g''8    g'8    fis''8    fis'8    \bar "|"   e'8    e''16    fis''16    g''16   
 e''16    fis''16    d''16    e''4    fis''4    \bar "|"     g''16    fis''16   
 e''16    d''16    c''16    b'16    a'16    g'16    e''8    d''8    d''8    
c''16    b'16    \bar "|"   c''8    b'8    b'8    a'16    g'16    e''8    d''8  
  d''8    c''16    b'16    \bar "|"   c''8    b'16    c''16    a'16    b'16    
g'16    a'16    fis'16    g'16    a'16    fis'16    d'8    d'8    \bar "|"   
g'16    a'16    b'16    c''16    d''16    e''16    fis''16    d''16    g''8    
fis''16    e''16    d''8    d''8    \bar "|"     g'16    a'16    b'16    c''16  
  d''16    e''16    fis''16    d''16    g''8    fis''16    e''16    d''8    
d''8    \bar "|"   e''16    fis''16    g''16    e''16    c''16    d''16    
e''16    c''16    a'16    b'16    c''16    a'16    fis'16    g'16    a'16    
fis'16    \bar "|"   d'16    e'16    fis'16    g'16    a'16    b'16    c''16    
a'16    b'16    g'16    fis'16    g'16    g''8    fis''16    e''16    \bar "|"  
 d''16    b'16    c''16    a'16    b'16    g'16    a'16    fis'16    g'2    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
