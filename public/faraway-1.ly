\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Domi Charly"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/187#setting187"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/187#setting187" {"https://thesession.org/tunes/187#setting187"}}}
	title = "Far Away"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key b \minor   r2   fis'8    a'8    \bar "|"   b'4    b'8    a'8    
b'8    d''8    \bar "|"   cis''8    a'8    fis'4    fis'8    a'8    \bar "|"   
b'4    b'8    a'8    b'16.    cis''16    e''16.    \bar "|"     cis''2    d''8  
  cis''8    \bar "|"   b'4    b'8    a'8    b'16.    cis''16    d''16.    
\bar "|"   cis''8    b'8    a'4    d''8    e''8    \bar "|"   fis''8    e''8    
d''8    cis''8    b'8    a'8    \bar "|"     b'4.    a'8    fis'8    a'8    
\bar "|"   b'4    b'8    a'8    b'8    d''8    \bar "|"   cis''8    a'8    
fis'4    fis'8    a'8    \bar "|"   b'4    b'8    a'8    b'16.    cis''16    
e''16.    \bar "|"     cis''2    d''8    cis''8    \bar "|"   b'4    b'8    a'8 
   b'16.    cis''16    d''16.    \bar "|"   cis''8    b'8    a'4    d''8    
e''8    \bar "|"   fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   
  b'4.    cis''8    d''8    e''8    \bar "|"   fis''4    fis''8    e''8    
fis''16.    g''16    a''16.    \bar "|"   e''8    cis''8    a'4    d''8    e''8 
   \bar "|"   fis''4    fis''8    e''8    fis''16.    g''16    a''16.    
\bar "|"     e''2    d''8    e''8    \bar "|"   fis''8    d''8    b'8    e''8   
 cis''8    a'8    \bar "|"   d''8    b'8    g'8    d''8    b'8    g'8    
\bar "|"   fis'4    b'4    b'8    a'8    \bar "|"     b'4.    cis''8    d''8    
e''8    \bar "|"   fis''4    fis''8    e''8    fis''16.    g''16    a''16.    
\bar "|"   e''8    cis''8    a'4    d''8    e''8    \bar "|"   fis''4    fis''8 
   e''8    fis''16.    g''16    a''16.    \bar "|"     e''2    d''8    e''8    
\bar "|"   fis''8    d''8    b'8    e''8    cis''8    a'8    \bar "|"   d''8    
b'8    g'8    d''8    b'8    g'8    \bar "|"   fis'4    b'4    b'8    a'8    
\bar "|"     b'4.    a'8    fis'8    a'8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
