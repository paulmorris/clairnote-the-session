\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Edgar Bolton"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/841#setting14005"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/841#setting14005" {"https://thesession.org/tunes/841#setting14005"}}}
	title = "Poll Ha'Penny"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d''8.    b'16    g'8.    b'16    
a'4    a'4  \grace {    a'8    b'8    d''8  }   \bar "|"   e''8.    cis''16    
a'8.    fis'16    g'4    g'4  \grace {    a'8    b'8    d''8  }   \bar "|"   
e''8.    d''16    cis''8.    a'16    d''8.    cis''16    a'8.    g'16    
\bar "|"   a'4    a'8.    b'16    a'2  \grace {    a'8    b'8    d''8  }   
\bar "|"   fis''4.    g''8    e''4.    cis''8    \bar "|"   d''4    a'4  
\grace {    a'8    b'8    d''8  }   e''8.    fis''16    g''8.    e''16    
\bar "|"   a''8.    g''16    e''8.    d''16    cis''8.    a'16    d''8.    
cis''16    \bar "|"   a'4    a'8.    b'16    a'2    }   \repeat volta 2 {   
g''4    g''8.    fis''16    g''8.    e''16    d''8.    b'16    \bar "|"   
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8.    fis''16    g''8.    e''16    
d''8.    fis''16    \bar "|"   e''4    a''4    a''4.    g''8    \bar "|"   e''4 
   a''4    a''4.    g''8    \bar "|"   fis''4.    g''8    e''4.    cis''8    
\bar "|"   d''4    a'4  \grace {    a'8    b'8    d''8  }   e''8.    fis''16    
g''8.    e''16    \bar "|"   a''8.    g''16    e''8.    d''16    cis''8.    
a'16    d''8.    cis''16    \bar "|"   a'4    a'8.    b'16    a'2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
