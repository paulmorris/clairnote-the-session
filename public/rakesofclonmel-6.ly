\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "banjouke"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1130#setting21430"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1130#setting21430" {"https://thesession.org/tunes/1130#setting21430"}}}
	title = "Rakes Of Clonmel, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \minor   a'16    b'16  \repeat volta 2 {   c''8 ^"Am"   b'8    
a'8      g'8 ^"G"   e'8    d'8  \bar "|"   e'8 ^"Am"   a'8    a'8    a'4    b'8 
 \bar "|"   c''8 ^"C"   c''8    c''8    e''8    d''8    c''8  \bar "|"   b'8 
^"G"   g'8    g'8    g'4    e''16    f''16  \bar "|"       g''8 ^"G"   g''8    
g''8      e''8 ^"C"   f''8    g''8  \bar "|"   d''8 ^"G"   b'8    g'8    b'8    
c''8    d''8  \bar "|"   e''8 ^"Am"   c''8    a'8      g'8 ^"G"   e'8    d'8  
} \alternative{{     e'8 ^"Am"   a'8    a'8    a'4    a'16    b'16  } {     e'8 
^"Am"   a'8    a'8    a'4    a''8  \bar "|"       a''8 ^"Am"   e''8    a''8    
a''8    e''8    d''8  \bar "|"   c''8 ^"Am"   a'8    a'8    a'4    e''16    
f''16  \bar "|"   g''8 ^"G"   g''8    g''8    g''8    d''8    c''8  \bar "|"   
b'8 ^"G"   g'8    g'8    g'4    f''8  \bar "|"       g''8 ^"C"   e''8    a''8   
 g''8    f''8    e''8  \bar "|"   d''8 ^"G"   b'8    g'8    b'8    c''8    d''8 
 \bar "|"   e''8 ^"Am"   c''8    a'8      g'8 ^"G"   e'8    d'8  \bar "|"   e'8 
^"Am"   a'8    a'8    a'4    a''8  \bar "|"       a''8 ^"Am"   e''8    a''8    
a''8    e''8    d''8  \bar "|"   c''8 ^"Am"   a'8    a'8    a'4    e''16    
f''16  \bar "|"   g''8 ^"G"   g''8    g''8    g''8    d''8    c''8  \bar "|"   
b'8 ^"G"   g'8    g'8    g'4    b'8  \bar "|"       c''8 ^"C"   b'8    c''8    
d''8    cis''8    d''8  \bar "|"   e''8 ^"C"   d''8    c''8    b'8    c''8    
d''8  \bar "|"   e''8 ^"Am"   c''8    a'8      g'8 ^"G"   e'8    d'8  \bar "|"  
 e'8 ^"Am"   a'8    a'8    a'4    b'8  \bar "|"   a'8 ^"Am"   a''8    a''8    
a''8    a''8    a''8  \bar "|"       b''8 ^"Em"   g''8    e''8    e''4    f''8  
\bar "|"   g''8 ^"G"   g''8    g''8      b''8 ^"Em"   g''8    e''8  \bar "|"   
d''8 ^"G"   b'8    g'8    g'4    b'8  \bar "|"   c''8 ^"C"   b'8    c''8      
d''8 ^"G"   d''8    d''8  \bar "|"       e''8 ^"C"   f''8    e''8      b'8 ^"G" 
  c''8    d''8  \bar "|"   e''8 ^"Am"   c''8    a'8      g'8 ^"G"   e'8    d'8  
\bar "|"   e'8 ^"Am"   a'8    a'8    a'4    a'16    b'16  \bar "|"       c''8 
^"Am"   b'8    a'8      g'8 ^"G"   e'8    d'8  \bar "|"   e'8 ^"Am"   a'8    
a'8    a'4    b'8  \bar "|"   c''8 ^"C"   c''8    c''8    e''8    d''8    c''8  
\bar "|"   b'8 ^"G"   g'8    g'8    g'4    e''16    f''16  \bar "|"       g''8 
^"G"   g''8    g''8      e''8 ^"C"   f''8    g''8  \bar "|"   d''8 ^"G"   b'8   
 g'8    b'8    c''8    d''8  \bar "|"   e''8 ^"Am"   c''8    a'8      g'8 ^"G"  
 e'8    d'8  \bar "|"   e'8 ^"Am"   a'8    a'8    a'4.  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
