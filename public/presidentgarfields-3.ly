\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/419#setting13276"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/419#setting13276" {"https://thesession.org/tunes/419#setting13276"}}}
	title = "President Garfield's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \mixolydian   \repeat volta 2 {   a'8    e'8    cis'8    e'8   
 a'8    e'8    cis'8    e'8    \bar "|"   a'8    gis'8    a'8    b'8    cis''8  
  ces''8    ces''8    dis''8    \bar "|"   e''8    b'8    gis'8    b'8    e''8  
  b'8    gis'8    b'8    \bar "|"   e''8    dis''8    e''8    fis''8    e''8    
d''8    cis''8    b'8    \bar "|"   a'8    e'8    cis'8    e'8    a'8    e'8    
cis'8    e'8    \bar "|"   a'8    gis'8    a'8    b'8    cis''8    ces''8    
ces''8    dis''8    \bar "|"   e''8    fis''8    gis''8    fis''8    e''8    
d''8    cis''8    b'8    } \alternative{{   a'16    b'16    a'8    gis'8    b'8 
   a'8    d''8    cis''8    b'8    } }    \repeat volta 2 {   fis''8    d''8    
a'8    fis'8    d'8    fis'8    a'8    fis''8    \bar "|"   e''8    cis''8    
a'8    e'8    cis'8    e'8    a'8    e''8    \bar "|"   dis''8    e''8    
fis''8    e''8    dis''8    e''8    fis''8    e''8    \bar "|"   cis''8    e''8 
   fis''8    e''8    cis''8    e''8    a''8    gis''8    \bar "|"   fis''8    
d''8    a'8    fis'8    d'8    fis'8    a'8    fis''8    \bar "|"   e''8    
cis''8    a'8    e'8    cis'8    e'8    a'8    e''8    \bar "|"   dis''!8    
e''8    gis''8    fis''8    e''8    d''8    cis''8    b'8    } \alternative{{   
a'16    b'16    a'8    gis'8    b'8    a'4    a''8    gis''8    } }    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
