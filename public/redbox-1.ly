\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kphaup"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1386#setting1386"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1386#setting1386" {"https://thesession.org/tunes/1386#setting1386"}}}
	title = "Red Box, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \mixolydian   a''8    fis''8  \grace {    a''8  }   g''8    
e''8  \grace {    a''8  }   fis''8    d''8    d''4 ^"~"  \bar "|"   c''8    a'8 
 \grace {    c''8  }   b'8    g'8    a'8    d'8    d'8    e'8  \bar "|" 
\grace {    a'8  }   e'8    d'8    e'8    g'8    a'4. ^"~"    b'8  \bar "|"   
c''8    a'8    \tuplet 3/2 {   b'8    c''8    d''8  }   e''8    fis''8    g''4 
^"~"  \bar "|"     a''8    r8 g''8    e''8  \grace {    a''8  }   fis''8    
d''8    d''4 ^"~"  \bar "|"   c''8    a'8  \grace {    c''8  }   b'8    g'8    
a'8    d'8    d'8    e'8  \bar "|" \grace {    a'8  }   e'8    d'8    e'8    
g'8    a'4. ^"~"    b'8  \bar "|"   c''8    a'8    g'8    e'8  \grace {    a'8  
}   e'8    d'8    d'4  }     \repeat volta 2 {   c''4.    e''8  \grace {    
a''8  }   g''8    e''8    cis''8    e''8  \bar "|"   d''8    cis''8    d''8    
fis''8  \grace {    c'''8  }   a''8    fis''8    d''8    e''8  \bar "|"   
fis''8    g''8    e''8    g''8  \grace {    a''8  }   fis''8    e''8    d''8    
fis''8  \bar "|"   e''4. ^"~"    fis''8    e''8    cis''8    a'8    b'8  
\bar "|"     c''4.    e''8  \grace {    a''8  }   g''8    e''8    cis''8    
e''8  \bar "|"   d''8    cis''8    d''8    fis''8  \grace {    c'''8  }   a''8  
  fis''8    d''8    e''8  \bar "|"   fis''8    g''8    e''8    g''8  \grace {   
 a''8  }   fis''8    e''8    d''8    b'8  \bar "|"   c''8    a'8    g'8    e'8  
\grace {    a'8  }   e'8    d'8    d'4  }         \repeat volta 2 {   a''8 
^"variations"   fis''8  \grace {    a''8  }   g''8    e''8  \grace {    a''8  } 
  fis''8    d''8    d''4 ^"~"  \bar "|"   c''8    a'8  \grace {    c''8  }   
b'8    g'8    a'8    d'8    d'4 ^"~"  \bar "|" \grace {    a'8  }   fis'8    
d'8    fis'8    g'8    a'4. ^"~"    b'8  \bar "|"   c''8    b'8    a'8    b'8   
 c''8    d''8    e''8    g''8  \bar "|"     a''8    fis''8  \grace {    a''8  } 
  g''8    e''8  \grace {    a''8  }   fis''8    d''8    d''4 ^"~"  \bar "|"   
c''8    a'8  \grace {    c''8  }   b'8    g'8    a'8    d'8    d'4 ^"~"  
\bar "|" \grace {    a'8  }   fis'8    d'8    fis'8    g'8    a'4. ^"~"    b'8  
\bar "|"   c''8    a'8    g'8    e'8  \grace {    a'8  }   e'8    d'8    d'4  } 
    \repeat volta 2 {   c''4    e''8    c''8  \grace {    a''8  }   g''8    
e''8    cis''8    e''8  \bar "|"   d''8    cis''8    d''8    fis''8  \grace {   
 c'''8  }   a''8    fis''8    d''8    fis''8  \bar "|"   g''4 ^"~"  \grace {    
c'''8  }   b''8    g''8    fis''8    g''8  \grace {    c'''8  }   a''8    
fis''8  \bar "|"   e''8    d''8    e''8    fis''8    e''8    c''!8    a'8    
b'8  \bar "|"     c''8    b'8    c''8    e''8  \grace {    a''8  }   g''8    
e''8    cis''8    e''8  \bar "|"   d''8    cis''8    d''8    fis''8  \grace {   
 c'''8  }   a''8    fis''8    d''8    fis''8  \bar "|"   g''4 ^"~"    a''8    
g''8  \grace {    a''8  }   fis''8    d''8    d''8    b'8  \bar "|"   c''8    
a'8    g'8    e'8  \grace {    a'8  }   e'8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
