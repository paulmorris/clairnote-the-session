\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/279#setting13026"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/279#setting13026" {"https://thesession.org/tunes/279#setting13026"}}}
	title = "Humours Of Ballymanus, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   a'8    b'8    a'8    a'4    fis'8 
   a'4    fis'8    \bar "|"   a'8    b'8    a'8    a'4    fis'8    b'8    
cis''8    d''8    \bar "|"   a'8    b'8    a'8    a'4    fis'8    b'8    a'8    
fis'8    \bar "|"   e'4    b'8    b'4    a'8    b'8    cis''8    d''8    }   
\repeat volta 2 {   d''4    e''8    fis''8    e''8    d''8    cis''8    b'8    
a'8    \bar "|"   d''4    e''8    fis''8    e''8    fis''8    a''4.    \bar "|" 
  d''4    e''8    fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   
b'4    b'8    b'4    a'8    b'8    cis''8    d''8    \bar "|"   d'8    cis''8   
 d''8    fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   d''8    
cis''8    d''8    e''8    d''8    e''8    fis''8.    e''16    fis''8    
\bar "|"   a''8    g''8    fis''8    e''8    fis''8    d''8    cis''8    b'8    
a'8    \bar "|"   b'4    b'8    b'4    a'8    b'8    cis''8    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
