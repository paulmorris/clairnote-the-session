\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Nell"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1332#setting14673"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1332#setting14673" {"https://thesession.org/tunes/1332#setting14673"}}}
	title = "Glenties, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   \bar "|"   a'8    b'8    d''8    e''8    fis''8    
g''8  \bar "|"   a''4    fis''4    a''4  \bar "|"   g''4    e''4    g''4  
\bar "|"   fis''8    g''8    fis''8    e''8    d''8    b'8  \bar "|"   a'8    
b'8    d''8    e''8    fis''8    g''8  \bar "|"   a''4    fis''4    a''4  
\bar "|"   g''4    fis''4    g''4  \bar "|"   fis''2.  \bar "|"   a'8    b'8    
d''8    e''8    fis''8    g''8  \bar "|" \tuplet 3/2 {   a''8    b''8    a''8  } 
  fis''4    a''4  \bar "|" \tuplet 3/2 {   g''8    a''8    g''8  }   e''4    
g''4  \bar "|"   fis''8    g''8    fis''8    e''8    d''8    b'8  \bar "|"   
a'8    b'8    d''8    e''8    fis''8    g''8  \bar "|"   a''4    fis''4    a''4 
 \bar "|"   g''4    e''4    c''4  \bar "|"   d''2.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
