\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "cat"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/697#setting697"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/697#setting697" {"https://thesession.org/tunes/697#setting697"}}}
	title = "Peat-Fire Flame"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor     \bar "||"   d''8 ^"Part A"   f''8    c''8    d''16  
  c''16    a'8    g'8    a'4    \bar "|"   a'1    \bar "||"       \bar "||"   
d'8. ^"Part B"   e'16    f'8    e'8    d'8    c'8    a'4    \bar "|"   a'8    
g'8    g'8    f'16    g'16    a'8    g'8    g'8    a'8    \bar "|"   d'8.    
e'16    f'8    e'8    d'8    c'8    a'8    a'16    g'16    \bar "|"   f'8    
d'8    e'8    c'8    d'4    d'4    \bar "||"       \bar "||"   f'8. ^"Part C"   
g'16    a'8    f''16    e''16    d''8    c''4    a'8    \bar "|"   a'8    g'8   
 g'8    f'16    g'16    a'8    g'8    g'8    a'8    \bar "|"   f'8.    g'16    
a'8    f''16    e''16    d''8    c''8    a'8.    g'16    \bar "|"   f'8    d'8  
  e'8    c'8    d'4    d'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
