\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1211#setting20922"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1211#setting20922" {"https://thesession.org/tunes/1211#setting20922"}}}
	title = "Berwick Billy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key a \major   \repeat volta 2 {   e''4    a'4    a'4    e''4    
fis''8    e''8    d''8    cis''8    \bar "|"   e''4    a'4    a'4    e''4    
cis''4    a''4    \bar "|"   e''4    a'4    a'4    e''4    fis''8    e''8    
d''8    cis''8    \bar "|"   d''4    b'4    b'4    cis''4    d''4    fis''4    
}     \repeat volta 2 {   a''4.    gis''8    a''4    a'4    cis''8    d''8    
e''8    cis''8    \bar "|"   a''4.    gis''8    a''4    a'4    cis''4    e''4   
 \bar "|"   a''4.    gis''8    a''4    a'4    cis''8    d''8    e''8    cis''8  
  \bar "|"   fis''4    a''4    a''4    b'4    d''4    fis''4    }     
\repeat volta 2 {   e''4    e''4    fis''8    e''8    d''8    cis''8    fis''8  
  e''8    d''8    cis''8    \bar "|"   e''4    e''4    fis''8    e''8    d''8   
 cis''8    e''4    a''4    \bar "|"   e''4    e''4    fis''8    e''8    d''8    
cis''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''4    b'4    b'4    
cis''4    d''4    fis''4    }     \repeat volta 2 {   a''4.    gis''8    a''4   
 a'4    cis''8    d''8    e''8    cis''8    \bar "|"   a''4.    gis''8    a''4  
  a'4    cis''4    e''4    \bar "|"   a''4.    gis''8    a''4    a''4    a''8   
 gis''8    fis''8    e''8    \bar "|"   fis''4    a''4    a''4    b'4    d''4   
 fis''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
