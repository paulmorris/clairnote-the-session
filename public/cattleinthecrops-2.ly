\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1206#setting14497"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1206#setting14497" {"https://thesession.org/tunes/1206#setting14497"}}}
	title = "Cattle In The Crops"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   a'8    fis'8    a'8    fis''8    e''8    d''8  
\bar "|"   b'8    g'8    b'8    g''4    b'8  \bar "|"   a'8    fis'8    a'8    
fis''8    e''8    d''8  \bar "|"   b'8    cis''8    b'8    b'4    b'8  \bar "|" 
  a'8    fis'8    a'8    fis''8    e''8    d''8  \bar "|"   b'8    g'8    b'8   
 g''4    g''8  \bar "|"   fis''16    g''16    a''8    fis''8    g''8    e''8    
cis''8  \bar "|"   d''8    e''8    d''8    d''4.  }   \repeat volta 2 {   a''4  
  a''8  \grace {    b''8  }   a''8    fis''8    d''8  \bar "|"   g''8    a''8   
 b''8    a''4    fis''8  \bar "|"   fis''8    g''8    a''8    a''8    g''8    
fis''8  \bar "|"   g''8    fis''8    g''8    e''8    fis''8    g''8  \bar "|"   
a''8    b''8    a''8    a''8    fis''8    d''8  \bar "|"   g''8    a''8    b''8 
   a''4    g''8  \bar "|"   fis''16    g''16    a''8    fis''8    g''8    e''8  
  cis''8  \bar "|"   d''8    e''8    d''8    d''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
