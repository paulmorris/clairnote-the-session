\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1182#setting14454"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1182#setting14454" {"https://thesession.org/tunes/1182#setting14454"}}}
	title = "Batchelder's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   d''4  \repeat volta 2 {   g''4    b''8    g''8    
fis''8    g''8    a''8    fis''8  \bar "|"   g''4    d''4   ~    d''4    c''!8  
  d''8  \bar "|"   e''8    d''8    c''8    b'8    c''8    b'8    a'8    g'8  
} \alternative{{   fis'8    g'8    a'8    b'8    c''8    d''8    e''8    fis''8 
 } {   fis'8    g'8    a'8    fis'8    g'2  } }    \repeat volta 2 {   b'4    
d''8    b'8    d''8    b'8    d''8    b'8  \bar "|"   c''4    e''8    c''8    
e''8    c''8    e''8    c''8  } \alternative{{   b'4    d''8    b'8    d''8    
b'8    d''8    b'8  \bar "|"   c''8    b'8    a'8    g'8    fis'8    g'8    a'8 
   c''8    } {   b'8    d''8    g''8    b''8    c'''8    a''8    fis''8    a''8 
   \bar "|"   g''4    b''4    g''2  } }    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
