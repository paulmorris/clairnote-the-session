\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "talltorpedo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/357#setting18513"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/357#setting18513" {"https://thesession.org/tunes/357#setting18513"}}}
	title = "Denis Murphy's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key c \major   \repeat volta 2 {   c''4    c''8    a'8    \bar "|"   
g'8    e'8    c''4  \bar "|"   e'8    g'8    c''8    a'8    \bar "|"   g'8    
e'8    d'8    c'8  \bar "|"     c''4    c''8    a'8    \bar "|"   g'8    e'8    
c''4    \bar "|"   e'8    g'8    d'8    e'8    \bar "|"   d'8    c'8    c'4  }  
   \repeat volta 2 {   e'8    g'8    g'8    e'8  \bar "|"   f'8    a'8    a'8   
 f'8    \bar "|"   e'8    g'8    g'8.    e'16  \bar "|"   f'16    g'16    f'16  
  e'16    d'8    c'8  \bar "|"     e'8    g'8    g'8    e'8  \bar "|"   f'8    
a'8    a'8.    f'16    \bar "|"   e'8    g'8    d'8    e'8  \bar "|"   d'8    
c'8    c'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
