\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/842#setting14009"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/842#setting14009" {"https://thesession.org/tunes/842#setting14009"}}}
	title = "Hills Of Glenorchy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   \repeat volta 2 {   d''8    \bar "|"   b'4    e'8    
e'8    fis'8    e'8    \bar "|"   b'8    a'8    b'8    d''4    b'8    \bar "|"  
 a'8    fis'8    d'8    d'8    e'8    d'8    \bar "|"   a'8    fis'8    a'8    
d''4    d''8    \bar "|"     b'4    e'8    e'8    fis'8    e'8    \bar "|"   
b'8    a'8    b'8    d''4    e''8    \bar "|"   d''8    b'8    d''8    a'8    
fis'8    d'8    \bar "|"   e'8    fis'8    e'8    e'4    }     
\repeat volta 2 {   d''8    \bar "|"   e''8    fis''8    e''8    e''8    d''8   
 b'8    \bar "|"   e''8    fis''8    g''8    g''8    fis''8    e''8    \bar "|" 
  d''8    e''8    d''8    d''8    a'8    b'8    \bar "|"   d''8    e''8    
fis''8    fis''8    e''8    d''8    \bar "|"     g''8    b''8    g''8    fis''8 
   a''8    fis''8    \bar "|"   e''8    g''8    e''8    b'8    d''8    b'8    
\bar "|"   d''8    b'8    d''8    a'8    fis'8    d'8    \bar "|"   e'8    
fis'8    e'8    e'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
