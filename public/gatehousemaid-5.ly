\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ian Varley"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/941#setting28968"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/941#setting28968" {"https://thesession.org/tunes/941#setting28968"}}}
	title = "Gatehouse Maid, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   a'4    b'16    c''16    d''8    e''8    
d''8    b'8    a'8  \bar "|"   g'4. ^"~"    a'8    g'8    e'8    d'8    b'8  
\bar "|"   a'4    b'16    c''16    d''8    e''8    d''8    b'16    c''16    
d''8  \bar "|"   e''8    g''8    d''8    b'8  \grace {    c''8  }   b'8    a'8  
  b'8    g'8  \bar ":|."   e''8    fis''8    d''8    b'8  \grace {    c''8  }   
b'8    a'8    a'4  \bar "||"     \bar "|"   a''4 ^"~"    e''8    a''8  
\grace {    b''8  }   a''8    g''8    e''8    fis''8  \bar "|"   g''4 ^"~"    
d''8    g''8    g''4 ^"~"    d''8    g''8  \bar "|"   a''4 ^"~"    e''8    a''8 
 \grace {    b''8  }   a''8    g''8    a''8    b''8  \bar "|"   g''8    e''8    
d''8    b'8  \grace {    c''8  }   b'8    a'8    a'4  \bar "|"     \bar "|"   
a''4. ^"~"    b''8    a''8    g''8    e''8    fis''8  \bar "|"   g''4. ^"~"    
a''8    g''4    g''8    e''8  \bar "|"   d''8    e''8    g''8    a''8    b''16  
  a''16    g''8    a''8    b''8  \bar "|"   g''8    e''8    d''8    b'8  
\grace {    c''8  }   b'8    a'8    a'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
