\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/317#setting317"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/317#setting317" {"https://thesession.org/tunes/317#setting317"}}}
	title = "Paddy Whack"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   g'8    a'8    b'8    c''4    e''8  \bar "|"   d''8    
b'8    g'8    a'8    fis'8    d'8  \bar "|"   g'8    b'8    d''8    e''8    
fis''8    g''8  \bar "|"   fis''8    d''8    cis''8    d''8    e''8    fis''8  
\bar "|"   g''4. ^"~"    a''8    fis''8    d''8  \bar "|"   c''8    d''8    
e''8    d''8    b'8    g'8  \bar "|"   g'8    a'8    b'8    c''8    e''8    
d''8  \bar "|"   b'8    g'8    g'8    g'4    d'8  }   \repeat volta 2 {   g'8   
 b'8    d''8    g''4. ^"~"  \bar "|"   e''8    d''8    c''8    b'8    c''8    
a'8  \bar "|"   g'8    b'8    d''8    e''8    fis''8    g''8  \bar "|"   fis''8 
   d''8    cis''8    d''8    e''8    fis''8  \bar "|"   g''4. ^"~"    a''8    
fis''8    d''8  \bar "|"   c''8    d''8    e''8    d''8    b'8    g'8  \bar "|" 
  g'8    a'8    b'8    c''8    e''8    d''8  \bar "|"   b'8    g'8    g'8    
g'4    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
