\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/55#setting24970"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/55#setting24970" {"https://thesession.org/tunes/55#setting24970"}}}
	title = "Kesh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {     g'8 ^"G"   g'16    g'16    g'8  
  g'8    a'8    b'8  \bar "|"   a'8 ^"D"   a'16    a'16    a'8    a'8    b'8    
d''8  \bar "|"   e''8 ^"G"   d''16    d''16    d''8    g''8    d''8    d''8  
\bar "|"   e''8    d''8    b'8      d''8 ^"D"   b'8    a'8  \bar "|"       g'8 
^"G"   g'16    g'16    g'8    g'8    a'8    b'8  \bar "|"   a'8 ^"D"   a'16    
a'16    a'8    a'8    b'8    d''8  \bar "|"   e''8 ^"G"   d''16    d''16    
d''8    g''8    d''8    b'8  \bar "||"   a'8 ^"D"   g'8    fis'8    g'4 ^"G"   
a'8  }     \repeat volta 2 {   b'8 ^"G"   a'16    b'16    b'8    d''8    b'8    
d''8  \bar "|"   e''8 ^"C"   g''8    e''8      d''8 ^"G"   b'8    a'8  \bar "|" 
  b'8 ^"G"   a'16    b'16    b'8    d''8    b'8    g'8  \bar "|"   a'8 ^"D"   
b'16    a'16    a'8    a'4    a'8  \bar "|"       b'8 ^"G"   a'16    b'16    
b'8    d''8    b'8    d''8  \bar "|"   e''8 ^"C"   g''8    e''8      d''8 ^"G"  
 b'8    a'8  \bar "|"   g''8    g''16    fis''16    g''8      a''8 ^"D"   g''8  
  a''8  \bar "|"   b''8 ^"G"   g''8    fis''8    g''4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
