\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kenny"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/66#setting12515"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/66#setting12515" {"https://thesession.org/tunes/66#setting12515"}}}
	title = "Mulqueen's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   b'4    a'8    cis''8    b'8    g'8    g'4  
  \bar "|"   b'8    g'8    d''8    g'8    b'8    g'8    g'4    \bar "|"   b'4   
 a'8    cis''8    b'8    g'8    g'4    \bar "|"   b'8    d''8    e''8    fis''8 
   g''8    e''8    d''8    cis''8    \bar "|"   b'4    a'8    cis''8    b'8    
g'8    g'4    \bar "|"   b'8    g'8    d''8    g'8    b'8    g'8    g'4    
\bar "|"   b'4    a'8    cis''8    b'8    g'8    g'4    \bar "|"   b'8    d''8  
  e''8    fis''8    g''4    fis''8    g''8    \bar "|"   a''4    a''8    g''8   
 a''4    a''8    g''8    \bar "|"   fis''8    g''8    a''8    b''8    cis'''4   
 b''8    cis'''8    \bar "|"   a''4    a''8    g''8    a''8    b''8    a''8    
g''8    \bar "|"   fis''8    d''8    e''8    fis''8    g''4    fis''8    g''8   
 \bar "|"   a''4    a''8    g''8    a''4    a''8    g''8    \bar "|"   fis''8   
 g''8    a''8    b''8    cis'''4    b''8    cis'''8    \bar "|"   d'''8    b''8 
   b''4    cis'''8    a''8    a''4    \bar "|"   g''8    b''8    a''8    fis''8 
   g''8    e''8    d''8    cis''8    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
