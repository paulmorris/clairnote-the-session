\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ObieWhistler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/857#setting21199"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/857#setting21199" {"https://thesession.org/tunes/857#setting21199"}}}
	title = "Spootiskerry"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'4    d'8    e'8    g'8    d'8    e'8    g'8  
\bar "|"   d'8    e'8    g'8    a'8    b'4    a'8    b'8  \bar "|"   g'4    d'8 
   e'8    g'8    a'8    b'8    d''8  \bar "|"   e''8    g''8    e''8    d''8    
b'4    a'8    b'8  \bar "|"     g'4    d'8    e'8    g'8    d'8    e'8    g'8  
\bar "|"   d'8    e'8    g'8    a'8    b'4    a'8    b'8  \bar "|"   g''4    
e''8    d''8    e''8    d''8    b'8    a'8  \bar "|"   b'4    g'4    g'4    d'8 
   e'8  \bar ":|."   b'4    g'4    g'4    e''8    fis''8  \bar "||"     
\bar ".|:"   g''4    e''8    d''8    e''8    d''8    b'8    a'8  \bar "|"   b'8  
  d''8    b'8    g'8    e'4    d'8    e'8  \bar "|"   g'8    a'8    b'8    d''8 
   e''8    g''8    e''8    d''8  \bar "|"   b'4    a'4    a'4    e''8    fis''8 
 \bar "|"     g''4    e''8    d''8    e''8    d''8    b'8    a'8  \bar "|"   
b'8    d''8    b'8    g'8    e'4    d'8    e'8  \bar "|"   g'8    a'8    b'8    
d''8    e''8    g''8    e''8    d''8  \bar "|"   b'4    g'4    g'4    e''8    
fis''8  \bar "|"     g''4    e''8    d''8    e''8    d''8    b'8    a'8  
\bar "|"   b'8    d''8    b'8    g'8    e'4    d'8    e'8  \bar "|"   g'8    
a'8    b'8    d''8    e''8    g''8    e''8    d''8  \bar "|"   b'4    a'4    
a'4    d'8    e'8  \bar "|"     g'4    d'8    e'8    g'8    d'8    e'8    g'8  
\bar "|"   d'8    e'8    g'8    a'8    b'4    a'8    b'8  \bar "|"   g''4    
e''8    d''8    e''8    d''8    b'8    a'8  \bar "|"   b'4    g'4    g'2  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
