\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bayram"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1122#setting1122"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1122#setting1122" {"https://thesession.org/tunes/1122#setting1122"}}}
	title = "Talisker On Ba Bridge"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   \repeat volta 2 {   b'8    g'8    fis'8    b'8    
bes'8    cis''8    b'!8    g'8    \bar "|"   fis'8    e'8    ees'8    b8    
ees'!8    e'!8    fis'8    g'8    \bar "|"   cis''8    d''8    cis''8    b'8    
bes'8    g'8    fis'8    fis'8    \bar "|"   bes'8    b'!8    cis''8    d''8    
fis''8    fis'8    fis''4    \bar "|"     b'8    g'8    fis'8    b'8    
\tuplet 3/2 {   b'8    b'8    b'8  }   b'8    g'8    \bar "|"   fis'8    e'8    
ees'8    b8    ees'!8    e'!8    fis'8    g'8    \bar "|"   cis''8    d''8    
cis''8    b'8    bes'8    g'8    fis'8    fis'8    } \alternative{{   cis''8    
b'8    bes'8    g'8    fis'4    fis'8    fis'8    } {   cis''8    b'8    bes'8  
  g'8    fis'4    fis'8    fis''8  } }      \repeat volta 2 {   |
 e''8    b'8    \tuplet 3/2 {   b'8    b'8    b'8  }   g''8    b'8    fis''8    
b'8    \bar "|"   e''8    b'8    \tuplet 3/2 {   b'8    b'8    b'8  }   g''8    
b'8    fis''8    b'8    \bar "|"   e''8    b'8    \tuplet 3/2 {   b'8    b'8    
b'8  }   fis''8    b'8    e''8    b'8    \bar "|"   d''8    a'8    \tuplet 3/2 { 
  a'8    a'8    a'8  }   fis''8    a'8    e''8    a'8    \bar "|"     d''8    
a'8    \tuplet 3/2 {   a'8    a'8    a'8  }   fis''8    a'8    d''8    a'8    
\bar "|"   e''8    b'8    \tuplet 3/2 {   b'8    b'8    b'8  }   g''8    b'8    
fis''8    b'8    \bar "|"   e''8    b'8    \tuplet 3/2 {   b'8    b'8    b'8  }  
 a'4    r4   } \alternative{{   a'8    fis'8    a'8    b'8    a'8    d''8    
a'8    fis''8  } {   a'8    b'8    c''8    cis''!8    ees''8    e''!4.    
\bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
