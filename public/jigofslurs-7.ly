\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tijn Berends"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/35#setting29819"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/35#setting29819" {"https://thesession.org/tunes/35#setting29819"}}}
	title = "Jig Of Slurs, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   a'8    d''8    d''8    cis''8    
d''8    d''8    \bar "|"   b'8    d''8    d''8    a'8    d''8    d''8    
\bar "|"   b'8    d''8    d''8    a'8    d''8    d''8    \bar "|"   b'4    e''8 
   e''8    d''8    b'8    \bar "|"     \bar "|"   a'8    d''8    d''8    cis''8 
   d''8    d''8    \bar "|"   b'8    d''8    d''8    a'8    d''8    d''8    
\bar "|"   b'8    d''8    d''8    cis''8    d''8    e''8    \bar "|"   fis''8   
 d''8    cis''8    d''4    b'8    }     \repeat volta 2 {   a'4    fis''8    
fis''8    e''8    fis''8    \bar "|"   a''4    fis''8    fis''8    e''8    d''8 
   \bar "|"   b'4    e''8    e''8    d''8    e''8    \bar "|"   fis''8    e''8  
  d''8    e''8    d''8    b'8    \bar "|"     \bar "|"   a'4    fis''8    
fis''8    e''8    fis''8    \bar "|"   a''4    fis''8    fis''8    e''8    d''8 
   \bar "|"   b'8    cis''8    d''8    cis''8    d''8    e''8    \bar "|"   
fis''8    d''8    cis''8    d''4    b'8  }   \key g \major   \repeat volta 2 {  
 g'4    g''8    g''8    fis''8    g''8    \bar "|"   a''8    g''8    fis''8    
g''8    d''8    b'8    \bar "|"   g'4    g''8    g''8    fis''8    g''8    
\bar "|"   a''8    g''8    fis''8    g''4. ^"~"    \bar "|"     \bar "|"   g'4  
  g''8    g''8    fis''8    g''8    \bar "|"   a''8    g''8    fis''8    g''8   
 e''8    d''8    \bar "|"   b'4    e''8    e''8    fis''8    g''8    \bar "|"   
fis''8    e''8    d''8    e''8    d''8    b'8    }     \repeat volta 2 {   g'8  
  b'8    b'8    b'8    d''8    d''8    \bar "|"   d''8    e''8    e''8    e''8  
  d''8    b'8    \bar "|"   g'8    b'8    b'8    b'8    d''8    d''8    
\bar "|"   d''8    e''8    e''8    e''8    fis''8    g''8    \bar "|"     
\bar "|"   g'8    b'8    b'8    b'8    d''8    d''8    \bar "|"   d''8    e''8  
  e''8    e''8    d''8    b'8    \bar "|"   b'4    e''8    e''8    fis''8    
g''8    fis''8    e''8    d''8    e''8    d''8    b'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
