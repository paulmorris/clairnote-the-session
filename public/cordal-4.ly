\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/864#setting25869"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/864#setting25869" {"https://thesession.org/tunes/864#setting25869"}}}
	title = "Cordal, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {   b'8    a'8    fis'8    e'4.    
\bar "|"   fis'8    e'8    fis'8    d'8    fis'8    a'8    \bar "|"   b'8    
a'8    fis'8    d'8    fis'8    a'8    \bar "|"   b'4    a'8    b'8    c''8    
d''8    \bar "|"     b'8    a'8    fis'8    e'4.    \bar "|"   fis'8    e'8    
fis'8    d'8    fis'8    a'8    \bar "|"   d''4    fis''8    e''8    d''8    
c''8    \bar "|"   b'4    a'8    b'8    c''8    d''8    }     \repeat volta 2 { 
  |
 d''4    e''8    g''16    fis''16    e''8    d''8    \bar "|"   c''4    d''8    
e''8    c''8    a'8    \bar "|"   d''4    e''8    g''16    fis''16    e''8    
d''8    \bar "|"   fis''4    e''8    fis''8    g''8    a''8    \bar "|"     
d''4    e''8    g''16    fis''16    e''8    d''8    \bar "|"   c''8    fis'16   
 g'16    c''8    e''8    c''8    a'8    \bar "|"   d''8    fis''8    d''8    
c''8    e''8    c''8    \bar "|"   b'4    a'8    b'8    c''8    d''8    }     
\repeat volta 2 {   |
 b'8    a'8    fis'8    e'4.    \bar "|"   fis'8    e'8    fis'8    d'8    
fis'16    g'16    a'8    \bar "|"   b'8    a'8    fis'8    d'8    fis'8    a'8  
  \bar "|"   b'4    a'8    b'8    c''8    d''8    \bar "|"     b'8    a'8    
fis'8    e'4.    \bar "|"   fis'8    e'8    fis'8    d'8    fis'16    g'16    
a'8    \bar "|"   d''4    fis''8    e''8    d''8    c''8    \bar "|"   b'4    
a'8    b'8    c''8    d''8    }     \repeat volta 2 {   |
 d''4.    g''16    fis''16    e''8    d''8    \bar "|"   c''4.    e''8    c''8  
  a'8    \bar "|"   d''4.    g''16    fis''16    e''8    d''8    \bar "|"   
fis''4    e''8    fis''8    g''8    a''8    \bar "|"     d''4    e''8    g''16  
  fis''16    e''8    d''8    \bar "|"   c''8    fis'16    g'16    c''8    e''8  
  c''8    a'8    \bar "|"   d''8    fis''16    g''16    d''8    c''8    fis''16 
   g''16    c''8    \bar "|"   b'4    a'8    b'8    c''8    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
