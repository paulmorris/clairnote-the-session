\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Knolle"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/16#setting21146"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/16#setting21146" {"https://thesession.org/tunes/16#setting21146"}}}
	title = "Clumsy Lover, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   cis''8 ^"A"   e''8    e''8    
fis''8    e''8    cis''8    b'8    a'8  \bar "|"   d''8 ^"D"   fis''8    fis''8 
   fis''8    g''8    fis''8    e''8    d''8  \bar "|"   cis''8 ^"A"   e''8    
e''8    fis''8    e''8    cis''8    b'8    a'8  \bar "|"   d''8 ^"E"   b'8    
b'8    a'8    b'4    d''4  \bar "|"       cis''8 ^"A"   e''8    e''8    fis''8  
  e''8    cis''8    b'8    a'8  \bar "|"   d''8 ^"D"   fis''8    fis''8    
fis''8    g''8    fis''8    e''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8 
   cis''8    d''4    gis'8    b'8  \bar "|"   b'8    a'8    a'8    b'8      a'2 
^"A" }     \repeat volta 2 {   cis''8 ^"A"   e''8    e''8    a''8    a''8    
e''8    e''8    cis''8  \bar "|"   d''8 ^"D"   fis''8    fis''8    a''8    a''8 
   fis''8    fis''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8    a''8    
a''8    e''8    e''8    cis''8  \bar "|"   d''8 ^"E"   b'8    b'8    a'8    b'4 
   d''4  \bar "|"       cis''8 ^"A"   e''8    e''8    a''8    a''8    e''8    
e''8    cis''8  \bar "|"   d''8 ^"D"   fis''8    fis''8    a''8    a''8    
fis''8    fis''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8    cis''8    
d''4    gis'8    b'8  \bar "|"   b'8    a'8    a'8    b'8      a'2 ^"A" }     
\repeat volta 2 {   cis''8 ^"A"   e''8    e''8    cis''8    e''8    cis''8    
e''8    cis''8  \bar "|"   d''8 ^"D"   fis''8    fis''8    fis''8    g''8    
fis''8    e''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8    cis''8    e''8 
   cis''8    e''8    cis''8  \bar "|"   d''8 ^"E"   b'8    b'8    a'8    b'4    
d''4  \bar "|"       cis''8 ^"A"   e''8    e''8    cis''8    e''8    cis''8    
e''8    cis''8  \bar "|"   d''8 ^"D"   fis''8    fis''8    fis''8    g''8    
fis''8    e''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8    cis''8    d''4 
   gis'8    b'8  \bar "|"   b'8    a'8    a'8    b'8      a'2 ^"A" }     
\repeat volta 2 {   cis''8 ^"A"   a''8    a''8    cis''8    a''8    cis''8    
a''8    cis''8  \bar "|"   d''8 ^"D"   a''8    a''8    d''8    a''8    d''8    
a''8    d''8  \bar "|"   cis''8 ^"A"   a''8    a''8    cis''8    a''8    cis''8 
   a''8    cis''8  \bar "|"   b'8 ^"E"   e''8    cis''8    e''8    d''8    e''8 
   b'8    e''8  \bar "|"       cis''8 ^"A"   a''8    a''8    cis''8    a''8    
cis''8    a''8    cis''8  \bar "|"   d''8 ^"D"   a''8    a''8    d''8    a''8   
 d''8    a''8    d''8  \bar "|"   cis''8 ^"A"   e''8    e''8    cis''8    d''4  
  gis'8    b'8  \bar "|"   b'8    a'8    a'8    b'8      a'2 ^"A" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
