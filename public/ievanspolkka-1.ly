\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Redbird"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/418#setting418"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/418#setting418" {"https://thesession.org/tunes/418#setting418"}}}
	title = "Ievan's Polkka"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key b \minor   b'8.    b'16    b'8    cis''8  \bar "|"   d''8    b'8 
   b'8    d''8  \bar "|"   cis''8    a'8    a'8    cis''8  \bar "|"   d''16    
cis''16    b'16    a'16    b'8    fis'8  \bar "|"   b'8.    b'16    b'8    
cis''8  \bar "|"   d''8    b'8    b'4  \bar "|"   e''8    fis''16    e''16    
d''8    cis''8  \bar "|"   b'4    b'4  }     \repeat volta 2 {   fis''8.    
d''16    d''8    fis''8  \bar "|"   e''8    cis''8    cis''8    d''8  \bar "|"  
 e''8    fis''16    e''16    d''8    cis''8  \bar "|"   b'8    cis''8    d''8   
 e''8  \bar "|"   fis''8.    d''16    d''8    fis''8  \bar "|"   e''8    cis''8 
   cis''8    d''8  \bar "|"   e''8    fis''16    e''16    d''8    cis''8  
\bar "|"   b'4    b'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
