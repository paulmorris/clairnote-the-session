\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1276#setting1276"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1276#setting1276" {"https://thesession.org/tunes/1276#setting1276"}}}
	title = "Irish, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   d'8    g'8  \bar "|"   b'4    b'8    a'8    g'8    
b'8  \bar "|"   d''4    d''8    b'8    a'8    b'8  \bar "|"   c''4    c''8    
e''8    d''8    c''8  \bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }   g''4    
d'8    g'8  \bar "|"     b'4    b'8    a'8    g'8    b'8  \bar "|"   d''4    
d''8    b'8    a'8    b'8  \bar "|"   c''4    c''8    a'8    fis'8    a'8  
\bar "|"   g'2  \bar ":|."   g'4.    g''8    g''8    fis''8  \bar "||"     
\bar ".|:"   e''4    c''8    e''8    c''8    e''8  \bar "|"   d''4    d''8    
b'8    a'8    b'8  \bar "|"   c''4    c''8    a'8    fis'8    a'8  \bar "|"   
b'4    b'8    g''8    g''8    fis''8  \bar "|"     e''4    c''8    e''8    c''8 
   e''8  \bar "|"   d''4    d''8    b'8    a'8    b'8  \bar "|"   c''4    c''8  
  a'8    fis'8    a'8  \bar "|"   g'4.    g''8    g''8    fis''8  \bar ":|."   
g'2  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
