\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/706#setting23560"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/706#setting23560" {"https://thesession.org/tunes/706#setting23560"}}}
	title = "Mairi's Wedding"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   a'8.    gis'16    a'8    b'8    
\bar "|"   d''8    e''8    fis''4    \bar "|"   e''8    d''8    b'8    d''8    
\bar "|"   fis''8    e''8    fis''16    a''8.    \bar "|"     a'8.    a'16    
a'8    b'8    \bar "|"   d''8    e''8    fis''8    g''16    fis''16    \bar "|" 
  e''8    d''8    b'8    g'8    \bar "|"   a'4    a'4    \bar "|"     a''8.    
gis''16    a''8    b''8    \bar "|"   a''8    g''8    fis''4    \bar "|"   e''8 
   d''8    b'8    d''8    \bar "|"   fis''8    e''8    fis''16    a''8.    
\bar "|"     a''8.    a''16    a''8    b''8    \bar "|"   a''8    g''8    
fis''4    \bar "|"   e''8    d''8    b'8    g'8    \bar "|"   a'4    a'4    }   
  a''8.    gis''16    a''8    b''8    \bar "|"   a''8    g''8    fis''4    
\bar "|"   e''8    d''8    b'8    d''8    \bar "|"   fis''8    e''8    fis''16  
  a''8.    \bar "|"     a'8.    fis'16    a'8    b'8    \bar "|"   d''8    e''8 
   fis''8    g''16    fis''16    \bar "|"   e''8.    d''16    b'16    a'16    
g'8    \bar "|"   a'4   ~    a'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
