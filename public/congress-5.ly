\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/208#setting12876"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/208#setting12876" {"https://thesession.org/tunes/208#setting12876"}}}
	title = "Congress, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   d''8  \bar "|"   e''8    a'8    
a'8    a'8    b'8    d''8  \bar "|"   e''8    a''8    fis''8    g''8    e''8    
d''8  \bar "|"   e''8    a'8    a'8    e''8    a'8    a'8  \bar "|"   b'8    
a'8    g'8    a'8    b'8    d''8  \bar "|"   e''8    a'8    a'8    a'8    b'8   
 d''8  \bar "|"   e''8    a''8    fis''8    g''8    e''8    d''8  \bar "|"   
c''4    d''8    e''16    fis''16    g''8    d''8  \bar "|"   b'8    a'8    g'8  
  a'4  }   \repeat volta 2 {   d''8  \bar "|"   e''8    a''8    a''8    a''8    
g''8    a''8  \bar "|"   e''8    a''8    g''8    e''8    g''8    d''8  \bar "|" 
  e''16    fis''16    g''8    d''8    e''8    g''8    d''8  \bar "|"   e''8    
a''8    fis''8    g''8    e''8    d''8  \bar "|"   e''8    a''8    a''8    a''8 
   g''8    a''8  \bar "|"   e''8    a''8    fis''8    g''8    e''8    d''8  
\bar "|"   c''4    d''8    e''16    fis''16    g''8    d''8  \bar "|"   b'8    
a'8    g'8    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
