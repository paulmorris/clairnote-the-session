\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "meri-lawes"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/248#setting12958"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/248#setting12958" {"https://thesession.org/tunes/248#setting12958"}}}
	title = "Tam Lin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \minor   \bar "|"   bes8    d'8    f'8    d'8    bes8    d'8   
 f'8    d'8  \bar "|"   c'8    e'8    g'8    e'8    c'8    e'8    g'8    e'8  
\bar "|"   f'8    a'8    g'8    e'8    f'8    e'8    d'8    c'8  \bar "|"   a8  
  d'8    f'8    d'8    a8    d'8    f'8    d'8  \bar "|"   bes8    d'8    f'8   
 d'8    bes8    d'8    f'8    d'8  \bar "|"   c'8    e'8    g'8    e'8    c'8   
 e'8    g'8    e'8  \bar "|"   f'8    d'8    e'8    c'8    d'4.    a'8  }   
\repeat volta 2 {   d''8    cis''8    d''8    a'8    f'8    d'8    d'4  
\bar "|"   d''8    cis''8    d''8    a'8    f'8    d'8    d'4  \bar "|"   c''8  
  e'8    e'8    e'8    c''8    e'8    d''8    e'8  \bar "|"   c''8    e'8    
e'8    e'8    a'8    b'8    c''8    a'8  \bar "|"   d''8    cis''8    d''8    
a'8    f'8    d'8    d'4  \bar "|"   d''8    cis''8    d''8    a'8    f'8    
d'8    d'4  \bar "|"   bes8    d'8    f'8    d'8    c'8    e'8    g'8    e'8  
\bar "|"   f'8    d'8    e'8    c'8    d'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
