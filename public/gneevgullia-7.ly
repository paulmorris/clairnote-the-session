\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Magpiekate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/876#setting22166"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/876#setting22166" {"https://thesession.org/tunes/876#setting22166"}}}
	title = "Gneevgullia"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \dorian   e'4. ^"~"    fis'8    g'4    e'8    fis'8    
\bar "|"   g'8    b'8    a'8    fis'8    g'8    fis'8    d''8    fis'8    
\bar "|"   e'4. ^"~"    fis'8    g'8    a'8    b'8    cis''8    \bar "|"   d''8 
   a'8    a'8    fis'8    a'8    fis'8    e'4    }     e''4. ^"~"    d''8    
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8    \bar "|"   g''8    
e''8    a''8    fis''8    g''8    fis''8    e''8    d''8    \bar "|"   e''4. 
^"~"    d''8    \tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8    
\bar "|"   \tuplet 3/2 {   g''8    fis''8    e''8  }   d''8    fis''8    e''4.   
 d''8    \bar "|"     e''4. ^"~"    d''8    \tuplet 3/2 {   b'8    cis''8    
d''8  }   e''8    fis''8    \bar "|"   g''8    e''8    a''8    fis''8    g''8   
 fis''8    e''8    fis''8    \bar "|"   g''4. ^"~"    e''8    fis''4. ^"~"    
d''8    \bar "|"   d''4. ^"~"    b'8    a'4. ^"~"    fis'8    \bar "|"     
\repeat volta 2 {   g'8    b'8    d''8    g''8    e''8    g''8    e''8    d''8  
  \bar "|"   a'8    d'4. ^"~"    fis'2    \bar "|"   b'8    e'4. ^"~"    g'4.   
 b'8    \bar "|"   d''8    a'8    a'8    fis'8    a'8    fis'8    e'8    fis'8  
  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
