\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/62#setting25964"
	arranger = "Setting 12"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/62#setting25964" {"https://thesession.org/tunes/62#setting25964"}}}
	title = "Lark In The Morning, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   a'8    fis'16    g'16    a'8    
a'8    fis'16    g'16    a'8  \bar "|"   b'8    fis'16    g'16    b'8    b'8    
d''8    b'8  \bar "|"   a'8    fis'16    g'16    a'8    d'8    fis'8    a'8  
\bar "|"   g''16    fis''16    e''8    d''8    b'8    d''8    b'8  \bar "|"     
a'8    fis'16    g'16    a'8    d'8    fis'8    a'8  \bar "|"   b'4.    b'8    
a'8    b'8  \bar "|"   d''8    e''16    fis''16    g''8    a''8    fis''8    
e''8  \bar "|"   fis''8    d''8    b'8    b'8    a'8    b'8  }     
\repeat volta 2 {   d''8    e''8    fis''8    a''4.  \bar "|"   b''8    a''8    
fis''8    a''8    fis''8    e''8  \bar "|"   d''8    e''16    fis''16    g''8   
 a''8    fis''8    e''8  \bar "|"   fis''8    d''8    b'8    b'8    d''8    b'8 
 \bar "|"     d''8    e''16    fis''16    g''8    a''4.  \bar "|"   b''8    
a''8    fis''8    a''4    fis''8  \bar "|"   g''4.    fis''4.  \bar "|"   e''8  
  d''8    b'8    b'8    a'8    b'8  }     \repeat volta 2 {   d''4    fis''8    
fis''8    e''8    fis''8  \bar "|"   fis''8    e''8    fis''8    fis''8    e''8 
   fis''8  \bar "|"   d''4    fis''8    fis''8    e''8    fis''8  \bar "|"   
e''8    d''8    b'8    b'8    a'8    b'8  \bar "|"     d''4    fis''8    fis''8 
   e''8    fis''8  \bar "|"   fis''8    e''8    fis''8    d''8    e''8    
fis''8  \bar "|"   g''4.    fis''4.  \bar "|"   e''8    d''8    b'8    b'8    
d''8    b'8  }     \repeat volta 2 {   a'8    d''8    d''8    fis''8    d''8    
d''8  \bar "|"   e''8    d''8    d''8    fis''8    d''8    b'8  \bar "|"   a'8  
  d''8    d''8    fis''8    a''8    fis''8  \bar "|"   e''8    d''8    b'8    
b'8    a'8    b'8  \bar "|"     a'8    d''8    d''8    fis''8    d''8    d''8  
\bar "|"   e''8    d''8    b'8    d''8    e''8    fis''8  \bar "|"   g''4.    
fis''4.  \bar "|"   e''8    d''8    b'8    b'8    d''8    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
