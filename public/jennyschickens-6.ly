\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "aidriano"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/756#setting27147"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/756#setting27147" {"https://thesession.org/tunes/756#setting27147"}}}
	title = "Jenny's Chickens"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   \repeat volta 2 {   fis''4 ^\trill   b'8    b''8    
fis''8    b'8    d''8    e''8  \bar "|"   fis''4 ^\trill   b'8    b''8    e''8  
  a'8    cis''8    e''8  \bar "|"   fis''4 ^\trill   b'8    b''8    fis''8    
b'8    d''8    e''8  \bar "|"   fis''16 (   e''16    fis''16    g''16    a''8  
-)   fis''8    e''8    a'8    cis''8    e''8  }     \repeat volta 2 {   fis''8  
  b'8    d''8    b'8    fis''8    b'8    d''8    e''8  \bar "|"   fis''8    b'8 
   d''8    b'8    e''8    a'8    cis''8    e''8  \bar "|"   fis''8    b'8    
d''8    b'8    fis''8    b'8    d''8    e''8  \bar "|"   fis''16 (   e''16    
fis''16    g''16    a''8  -)   fis''8    e''8    a'8    cis''8    e''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
