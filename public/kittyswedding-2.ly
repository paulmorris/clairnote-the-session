\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/869#setting14038"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/869#setting14038" {"https://thesession.org/tunes/869#setting14038"}}}
	title = "Kitty's Wedding"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   \tuplet 3/2 {   g''8    fis''8    
e''8  }   \bar "|"   d''4    b'8.    d''16    a'4    fis'8.    a'16    \bar "|" 
  b'8.    a'16    fis'8.    a'16    d'4    e'8.    d'16    \bar "|"   
\tuplet 3/2 {   b'8    cis''8    d''8  }   a'8.    b'16    d'8.    fis'16    
b'8.    fis'16    \bar "|"   a'8.    fis'16    d'8.    fis'16    e'4    fis''8. 
   e''16    \bar "|"     \tuplet 3/2 {   d''8    e''8    d''8  }   b'8.    d''16 
   a'4    \tuplet 3/2 {   fis'8    g'8    a'8  }   \bar "|"   b'8.    a'16    
fis'8.    a'16    d'4    e'8.    d'16    \bar "|"   b'8.    d''16    a'8.    
b'16    d'8.    fis'16    b'8.    fis'16    \bar "|"   a'8.    fis'16    e'8.   
 fis'16    d'4    }     \repeat volta 2 {   fis''8.    g''16    \bar "|"   
a''8.    fis''16    e''8.    d''16    b''8.    a''16    fis''8.    d''16    
\bar "|"   a'8.    d''16    fis''8.    d''16    e''8.    d''16    \tuplet 3/2 {  
 b'8    cis''8    d''8  }   \bar "|"   d'8.    fis'16    a'8.    d''16    
fis'8.    a'16    d''8.    e''16    \bar "|"   \tuplet 3/2 {   fis''8    g''8    
fis''8  }   \tuplet 3/2 {   d''8    e''8    fis''8  }   e''4    fis''8.    g''16 
   \bar "|"     a''4    \tuplet 3/2 {   fis''8    e''8    d''8  }   b''4    
a''8.    fis''16    \bar "|"   a'8.    d''16    fis''8.    d''16    e''8.    
d''16    b'8.    d''16    \bar "|"   d'8.    fis'16    a'8.    d''16    fis'8.  
  a'16    d''8.    fis''16    \bar "|"   e''8.    a'16    b'8.    cis''16    
d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
