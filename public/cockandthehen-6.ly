\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/93#setting12645"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/93#setting12645" {"https://thesession.org/tunes/93#setting12645"}}}
	title = "Cock And The Hen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key b \minor   fis'4. ^"~"    fis'8    e'8    fis'8    b'8    cis''8 
   a'8  \bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    a'8    fis'8    e'8  
\bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    b'8    cis''8    d''8  
\bar "|"   e''8    cis''16    b'16    a'8    b'8    a'8    fis'8    a'8    
fis'8    e'8  \bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    b'8    cis''8  
  a'8  \bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    a'8    fis'8    e'8  
\bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    b'8    cis''8    d''8  
\bar "|"   e''8    cis''8    a'8    b'8    a'8    fis'8    a'8    fis'8    e'8  
\bar "|"   e''8    cis''8    a'8    a'8    b'8    cis''8    d''4    fis''8    
\bar "|"   e''8    cis''8    a'8    a'8    b'8    cis''8    b'8    a'8    fis'8 
   \bar "|"   e''8    cis''8    a'8    a'8    b'8    cis''8    d''8    cis''8   
 b'8    \bar "|"   cis''8    b'8    a'8    b'8    a'8    fis'8    a'8    fis'8  
  e'8    \bar "|"   e''8    cis''8    a'8    a'8    b'8    cis''8    d''8    
e''16    fis''16    g''8    \bar "|"   a''8    e''8    cis''8    a'8    b'8    
cis''8    b'8    a'8    fis'8    \bar "|"   e''8    cis''8    a'8    a'8    b'8 
   cis''8    d''8    cis''8    b'8    \bar "|"   cis''8    b'8    a'8    b'8    
a'8    fis'8    a'8    fis'8    e'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
