\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/93#setting12642"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/93#setting12642" {"https://thesession.org/tunes/93#setting12642"}}}
	title = "Cock And The Hen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key b \minor   fis'4. ^"~"    fis'4. ^"~"    b'8    cis''8    a'8  
\bar "|"   fis'4. ^"~"    fis'8    e'8    fis'8    a'8    fis'8    e'8  
\bar "|"   fis'4. ^"~"    fis'4. ^"~"    b'8    cis''8    d''8  \bar "|"   e''8 
   cis''8    a'8    a'8    b'8    cis''8    b'8    a'8    fis'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
