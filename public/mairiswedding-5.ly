\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mix O'Lydian"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/706#setting26510"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/706#setting26510" {"https://thesession.org/tunes/706#setting26510"}}}
	title = "Mairi's Wedding"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   d'8.    d'16    d'8    e'8    
\bar "|"   g'8    a'8    b'4    \bar "|"   a'8    g'8    e'8    g'8    \bar "|" 
  b'8    a'8    b'16    d''8.    \bar "|"     d'8.    d'16    d'8    e'8    
\bar "|"   g'8    a'8    b'4    \bar "|"   a'8    g'8    e'8    c'8    \bar "|" 
  d'4    d'4    }     \repeat volta 2 {   d''8.    d''16    d''8    e''8    
\bar "|"   d''8    c''8    b'4    \bar "|"   a'8    g'8    e'8    g'8    
\bar "|"   b'8    a'8    b'16    d''8.    \bar "|"     d''8.    d''16    d''8   
 e''8    \bar "|"   d''8    c''8    b'4    \bar "|"   a'8    g'8    e'8    c'8  
  \bar "|"   d'4    d'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
