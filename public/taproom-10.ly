\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "FiddleFancy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/711#setting13780"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/711#setting13780" {"https://thesession.org/tunes/711#setting13780"}}}
	title = "Tap Room, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key c \major   a4    a4 ^"~"    e'8    a8    b8    a8  \bar "|"   a4 
   e'8    a8    b8    a8    g8    b8  \bar "|"   a4    a4 ^"~"    e'8    a8    
b8    a8  \bar "|"   e'4    d'8    f'8    e'8    a8    a4  \bar "|"   a4    a4 
^"~"    e'8    a8    b8    a8  \bar "|"   a4    e'8    a8    b8    a8    g8    
b8  \bar "|"   a4    a4 ^"~"    e'8    a8    b8    a8  \bar "|"   e'4  
\grace {    f'8  }   d'8    f'8    e'8    a8    a4  \bar "|"   a'4    b'8    
a'8    e''8    a'8    b'8    a'8    \bar "|"   g'8    fis'8    g'8    b'8    
d''4    b'8    g'8  \bar "|"   a'4    b'8    a'8    e''8    a'8    b'8    a'8   
 \bar "|"   g'8    fis'8    g'8    e'8    d'8    a8    a4  \bar "|"   a'4    
b'8    a'8    e''8    a'8    b'8    a'8    \bar "|"   g'8    fis'8    g'8    
b'8    d''4    b'8    g'8  \bar "|"   a'8    b'8  \grace {    d''8  }   b'8    
a'8    g'8    fis'8    g'8    e'8  \bar "|"   d'4    b8    d'8    e'8    a8    
a8    g8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
