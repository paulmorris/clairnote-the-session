\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "shanachie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/911#setting911"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/911#setting911" {"https://thesession.org/tunes/911#setting911"}}}
	title = "Lake House"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian     a'8. ^"Am"   b'16    c''8    a'8  \bar "|"   g'8   
 a'8      e'8 ^"Em"   g'8  \bar "|"   a'8. ^"Am"   b'16    c''8    a'8  
\bar "|"   d''8 ^"G"   b'16    d''16      e''4 ^"Em" \bar "|"       a'8. ^"Am"  
 b'16    c''8    a'8  \bar "|"   g'8    a'8      e'8 ^"Em"   g'8  \bar "|"   
a'8 ^"Am"   b'16    c''16    b'8    a'8  \bar "|"   g'8. ^"G"   a'16    b'8    
g'8  \bar ":|."   g'8. ^"G"   d'16      e'8 ^"D"   fis'8  \bar "||"     
\bar ".|:"   g'8. ^"G"   a'16    b'8    g'8  \bar "|"   e'8 ^"C"   g'8      d'8 
^"D"   e'8  \bar "|"   g'8. ^"G"   a'16    b'8    g'8  \bar "|"   d''8 ^"Em"   
g'8      e''8 ^"C"   d''8  \bar "|"       g'8. ^"G"   a'16    b'8    g'8  
\bar "|"   e'8 ^"C"   g'8      d'8 ^"D"   e'8  \bar "|"   g'8 ^"G"   a'16    
c''16    b'8    g'8  \bar "|"   a'8. ^"Am"   c''16    b'8    a'8  \bar ":|."   
a'8 ^"Am"   e''16    d''16      c''8 ^"Em"   b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
