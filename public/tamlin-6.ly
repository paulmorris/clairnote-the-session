\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "davydd"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/248#setting12960"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/248#setting12960" {"https://thesession.org/tunes/248#setting12960"}}}
	title = "Tam Lin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   d'8    e'8  \repeat volta 2 {   f'8    e'8    d'8    
c'8    d'8    e'8    f'8    d'8  \bar "|"   bes4    d'8    bes8    f'8    bes8  
  d'8    bes8  \bar "|"   c'4    e'8    c'8    g'8    c'8    e'8    c'8  
\bar "|"   f'8    a'8    f'8    e'8    f'8    d'8    d'8    e'8  \bar "|"   
\bar "|"   f'8    e'8    d'8    c'8    d'8    e'8    f'8    d'8  \bar "|"   
bes4    d'8    bes8    f'8    bes8    d'8    bes8  \bar "|"   c'4    e'8    c'8 
   g'8    c'8    e'8    c'8  \bar "|"   f'8    a'8    f'8    e'8    f'8    d'8  
  d'4  }   \repeat volta 2 {   d''8    a'8    a'16    a'16    a'8    f'8    a'8 
   d'8    a'8  \bar "|"   d''8    a'8    a'16    a'16    a'8    f'8    a'8    
d'8    a'8  \bar "|"   c''8    g'8    g'16    g'16    g'8    c''8    g'8    
c''8    d''8  \bar "|"   e''8    d''8    e''8    g''8    f''8    e''8    d''8   
 cis''8  \bar "|"   \bar "|"   d''8    a'8    a'16    a'16    a'8    f'8    a'8 
   d'8    a'8  \bar "|"   d''8    a'8    a'16    a'16    a'8    f'8    a'8    
d'8    a8  \bar "|"   bes8    a8    bes8    c'8    d'8    c'8    d'8    e'8  
\bar "|"   f'8    d'8    e'8    c'8    d'2  }   \repeat volta 2 {   f''8    
d''8    d''16    d''16    d''8    a'8    g'8    f'8    e'8  \bar "|"   d'8    
e'8    f'8    g'8    a'8    b'8    c''8    d''8  \bar "|"   e''8    c''8    
c''16    c''16    c''8    e''8    c''8    g''8    e''8  \bar "|"   e''8    c''8 
   c''16    c''16    c''8    a''8    g''8    f''8    e''8  \bar "|"   \bar "|"  
 f''8    d''8    d''16    d''16    d''8    a'8    d''8    f''8    a''8  
\bar "|"   f''8    d''8    a'8    g'8    f'8    e'8    d'8    a8  \bar "|"   
bes8    a8    bes8    c'8    d'8    c'8    d'8    e'8  \bar "|"   f'8    d'8    
e'8    c'8    d'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
