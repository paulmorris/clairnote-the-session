\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/315#setting315"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/315#setting315" {"https://thesession.org/tunes/315#setting315"}}}
	title = "John Burke's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   \repeat volta 2 {   d'8    e'8    g'8    a'8    b'8  
  d''8    g''8    a''8  \bar "|"   b''8    a''8    g''8    d''8    b'8    g'8   
 d'8    e'8  \bar "|"   f'4    c'!8    f'8    a8    f'8    c'!8    f'8  
\bar "|"   f'8    g'8    a'8    g'8    fis'8    d'8    c'8    e'8  \bar "|"     
d'8    e'8    g'8    a'8    b'8    d''8    g''8    a''8  \bar "|"   b''8    
a''8    g''8    d''8    b'8    g'8    d'8    e'8  \bar "|"   f'4    a'8    f'8  
  c''!8    f'8    a'8    f'8  \bar "|"   fis'8    g'8    a'8    fis'!8    g'4   
 g'8    e'8  }     a'8  \repeat volta 2 {   d''8    a'8    a'8    a'8    e''8   
 a'8    a'8    g'8  \bar "|"   fis'8    d'8    fis'!8    a'8    d''8    c''8    
a'8    b'8  \bar "|"   c''4    b'4    c''8    a'8    g'8    f'8  \bar "|"   e'8 
   c'8    e'8    g'8    c''4    b'8    c''8  \bar "|"     d''8    a'8    a'8    
a'8    e''8    a'8    a'8    g'8  \bar "|"   fis'8 ^"14"   d'8    fis'!8    a'8 
   d''8    c''8    a'8    b'8  \bar "|"   c''4    b'4    c''8    d''8    e''8   
 d''8  \bar "|"   cis''8    a'8    g'8    e'8    d'4   ~    d'8    a'8  }     
\bar "|"   fis'8 ^"Alternate measure 14"   a'8    d''8    e''8    f''4    e''8  
  d''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
