\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "OsvaldoLaviosa"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/197#setting12857"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/197#setting12857" {"https://thesession.org/tunes/197#setting12857"}}}
	title = "Star Of Munster, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \minor   c''8    b'8    a'8    c''16    b'16    a'8    g'8  
\bar "|"   a'8    e'8    f'8    g'8    e'8    d'8  \bar "|"   e'8    a'8    a'8 
   a'8    b'8    d''8  \bar "|"   e''8    a''8    a''8    g''8    e''8    d''8  
\bar "|"   c''8    b'8    a'8    c''16    b'16    a'8    g'8  \bar "|"   a'8    
e'8    f'8    g'8    e'8    d'8  \bar "|"   e'8    a'8    a'8    a'8    b'8    
d''8  \bar "|"   e''8    d''8    b'8    a'8    a'8    a'8  }   e''8    a''8    
a''8    a''8    g''8    e''8  \bar "|"   a''16    a''16    a''8    a''8    a''8 
   g''8    e''8  \bar "|"   g''16    g''16    g''8    g''8    g''8    e''8    
d''8  \bar "|"   b''8    a''8    g''8    a''16    g''16    e''8    d''8  
\bar "|"   e''8    a''8    a''8    a''8    g''8    e''8  \bar "|"   a''16    
a''16    a''8    a''8    a''8    g''8    e''8  \bar "|"   g''16    g''16    
g''8    g''8    g''16    a''16    a''8    a''8  \bar "|"   a''8    b''8    a''8 
   g''8    e''8    d''8  \bar "|"   e''8    a''8    a''8    a''8    e''8    
e''8  \bar "|"   c'''8    b''8    a''8    a''8    e''8    e''8  \bar "|"   
g''16    g''16    g''8    g''8    g''8    e''8    d''8  \bar "|"   b''8    a''8 
   g''8  \grace {    a''8  }   g''8    e''8    d''8  \bar "|"   e''8    a''8    
a''8    a''8    e''8    e''8  \bar "|"   c'''8    b''8    a''8    a''8    e''8  
  e''8  \bar "|"   g''16    g''16    g''8    g''8    g''16    a''16    a''8    
a''8  \bar "|"   a''8    b''8    a''8    g''8    e''8    d''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
