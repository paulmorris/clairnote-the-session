\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "geoffwright"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1240#setting1240"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1240#setting1240" {"https://thesession.org/tunes/1240#setting1240"}}}
	title = "Dog Leap Stairs"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \major   \repeat volta 2 {   g''4    d''2    b'4    e''2  
\bar "|"   c''8    d''8    e''8    c''8    a'4    a''8    g''8    fis''8    
g''8    a''8    fis''8  \bar "|"     g''4    d''2    b'4    e''2  \bar "|"   
c''8    d''8    e''8    g''8    a''4    g''8    fis''8    g''2  }     
\repeat volta 2 {   b'4    d''2    b'4    c''2  \bar "|"   a'4    a''2    g''4  
  fis''8    g''8    a''8    fis''8  \bar "|"     b'4    d''2    b'4    c''2  
\bar "|"   a'4    a''4    fis''8    g''8    a''8    fis''8    g''2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
