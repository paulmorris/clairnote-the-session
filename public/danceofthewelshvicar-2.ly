\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/914#setting14101"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/914#setting14101" {"https://thesession.org/tunes/914#setting14101"}}}
	title = "Dance Of The Welsh Vicar"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key c \major   a''4    a'8    a'4    g''8    a'4    f''8  \bar "|"   
e''4.   ~    e''4    a'8    a'4    a'8  \bar "|"   b'4    c''8    d''4    b'8   
 g'4    a'8  \bar "|"   b'4    g'8    e'4    f'8    g'4    e'8  \bar "|"   a'4  
  a''8    a'4    g''8    a'4    f''8  \bar "|"   e''4.   ~    e''4    a'8    
a'4    a'8  \bar "|"   b'4    c''8    d''4    b'8    g'4    a'8  \bar "|"   b'8 
   cis''8    d''8    cis''4    b'8    a'4    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
