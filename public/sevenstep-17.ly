\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1307#setting14632"
	arranger = "Setting 17"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1307#setting14632" {"https://thesession.org/tunes/1307#setting14632"}}}
	title = "Seven Step, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'16    \bar "|"   d''4    d''4    d''4    d''4    
\bar "|"   cis''8.    d''16    e''8.    cis''16    a'4    a'4    \bar "|"   
e''4    e''4    e''4    e''4    \bar "|"   d''8.    e''16    fis''8.    d''16   
 a'4   ~    a'8.    \bar "||"     fis''16    \bar "|"   g''8.    fis''16    
g''8.    e''16    cis''4    g''4    \bar "|"   fis''8.    f''16    fis''!8.    
d''16    a'4    fis''4    \bar "|"   e''8.    dis''16    e''8.    cis''16    
a'8.    cis''16    e''8.    fis''16    \bar "|"   e''4    d''4    d''4    
\tuplet 3/2 {   d''8    e''8    fis''8  }   \bar "|"     g''4    g''8.    e''16  
  cis''4    a''8.    g''16    \bar "|"   fis''4    fis''8.    d''16    a'4    
fis''8.    d''16    \bar "|"   e''4    e''8.    cis''16    a'8.    cis''16    
e''8.    cis''16    \bar "|"   d''4    fis''4    d''4   ~    d''8.    \bar "|." 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
