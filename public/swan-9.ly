\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "David Levine"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1036#setting25945"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1036#setting25945" {"https://thesession.org/tunes/1036#setting25945"}}}
	title = "Swan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   \tuplet 3/2 {   e'8    fis'8    gis'8  }   \bar "|"   
a'4    \tuplet 3/2 {   cis''8    b'8    a'8  }   e'8    a'8    cis''8    e''8    
\bar "|"   a''4    gis''8    a''8    \tuplet 3/2 {   fis''8    gis''8    a''8  } 
  e''8    cis''8    \bar "|"   d''4    fis''8    d''8    cis''4    e''8    
cis''8    \bar "|"   b'8    e'8    gis'8    b'8    e''8    d''8    cis''8    
b'8    \bar "|"     a'4    \tuplet 3/2 {   cis''8    b'8    a'8  }   e'8    a'8  
  cis''8    e''8    \bar "|"   a''4    gis''8    a''8    \tuplet 3/2 {   fis''8  
  gis''8    a''8  }   e''8    cis''8    \bar "|"   d''4    fis''8    d''8    
cis''4    e''8    cis''8    \bar "|"   b'8    e'8    gis'8    b'8    a'4    }   
  \tuplet 3/2 {   e''8    fis''8    gis''8  }   \bar "|"   a''8    cis''8    
e''8    g''8    fis''8    a'8    d''8    fis''8    \bar "|" \grace {    gis''8  
}   fis''8    e''8    cis''8    a'8    b'8    e'8    gis'8    b'8    \bar "|"   
a'8    cis''8    e''8    cis''8    d''8    fis''8    b''8    a''8    \bar "|"   
\tuplet 3/2 {   gis''8    a''8    b''8  }   fis''8    b''8    e''8    b''8    
gis''8    b''8    \bar "|"     \tuplet 3/2 {   a''8    b''8    a''8  }   e''8    
g''8    fis''8    a'8    d''8    fis''8    \bar "|"   \tuplet 3/2 {   e''8    
fis''8    e''8  }   cis''8    a'8    b'8    e'8    gis'8    b'8    \bar "|"   
a'8    cis''8    e''8    cis''8    d''8    fis''8    b''8    a''8    \bar "|"   
\tuplet 3/2 {   gis''8    fis''8    e''8  }   \tuplet 3/2 {   d''8    cis''8    
b'8  }   a'4    \bar ":|."   gis''8    e''8    cis'''8    b''8    a''4    r4   
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
