\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1404#setting1404"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1404#setting1404" {"https://thesession.org/tunes/1404#setting1404"}}}
	title = "Willie Jamieson's Delight"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   b'8  \repeat volta 2 {   cis''8    a'8    fis'8    
e'8    cis'8    e'8  \bar "|"   a8    cis'8    e'8    a'8    cis''8    e''8  
\bar "|"   d''8    b'8    a'8    gis'8    a'8    b'8  \bar "|"   gis''8    
fis''8    e''8    d''8    cis''8    b'8  \bar "|"     cis''8    a'8    fis'8    
e'8    cis'8    e'8  \bar "|"   a8    cis'8    e'8    a'8    cis''8    e''8  
\bar "|"   d''8    b'8    b''8    gis''8    e''8    d''8  \bar "|"   cis''8    
a'8    gis'8    a'4    b'8  }     cis''8    fis''8    fis''8    fis''8    e''8  
  d''8  \bar "|"   cis''8    b'8    a'8    fis'8    e'8    d'8  \bar "|"   
cis'8    fis'8    fis'8    a'8    gis'8    fis'8  \bar "|"   gis'8    cis''8    
cis''8    d''8    cis''8    b'8  \bar "|"     cis''8    fis''8    fis''8    
fis''8    e''8    d''8  \bar "|"   cis''8    b'8    a'8    fis'8    e'8    d'8  
\bar "|"   cis'8    fis'8    fis'8    gis'8    a'8    b'8  \bar "|"   a'8    
fis'8    fis'8    fis'4    b'8  \bar "||"     cis''8    fis''8    fis''8    
a''8    fis''8    fis''8  \bar "|"   cis''8    b'8    a'8    fis'8    e'8    
d'8  \bar "|"   cis'8    fis'8    fis'8    a'8    gis'8    fis'8  \bar "|"   
gis'8    cis''8    cis''8    d''8    cis''8    b'8  \bar "|"     cis''8    
fis''8    fis''8    fis''8    e''8    d''8  \bar "|"   cis''8    b'8    a'8    
fis'8    e'8    d'8  \bar "|"   cis'8    fis'8    fis'8    gis'8    a'8    b'8  
\bar "|"   a'8    fis'8    fis'8    fis'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
