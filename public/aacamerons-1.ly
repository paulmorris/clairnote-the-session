\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/1275#setting1275"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1275#setting1275" {"https://thesession.org/tunes/1275#setting1275"}}}
	title = "A.A. Cameron's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   e''16    a'8.    a'4    b'8.    
g'16    d''8.    b'16  \bar "|"   e''16    a'8.    a'4    d''8.    g''16    
\tuplet 3/2 {   fis''8    e''8    d''8  } \bar "|"   e''16    a'8.    a'4    
b'8.    g'16    d''8.    b'16  \bar "|"   b'16    g'8.    g'8.    b'16    d''8. 
   g''16    \tuplet 3/2 {   fis''8    e''8    d''8  } }     \repeat volta 2 {   
b'16    e''8.    e''8.    fis''16    g''8.    e''16    a''8.    fis''16  
\bar "|"   b'16    e''8.    e''8.    fis''16    g''8.    e''16    \tuplet 3/2 {  
 fis''8    e''8    d''8  } \bar "|"   b'16    e''8.    e''8.    fis''16    
g''8.    e''16    a''8.    fis''16  \bar "|"   d''16    b'8.    g'8.    b'16    
d''8.    g''16    \tuplet 3/2 {   fis''8    e''8    d''8  } }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
