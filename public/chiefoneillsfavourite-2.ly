\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/13#setting12379"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/13#setting12379" {"https://thesession.org/tunes/13#setting12379"}}}
	title = "Chief O'Neill's Favourite"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   d''8    e''8    \bar "|"   fis''4. ^"~"    g''8    
a''8    a''8    g''8    e''8    \bar "|"   fis''8    d''8    e''8    fis''8    
d''8    cis''8    a'8    g'8    \bar "|"   fis'8    d'8    a'8    d'8    
\tuplet 3/2 {   e'8    fis'8    g'8  }   a'8    b'8  \bar "|"   c''8    a'8    
d''8    cis''!8    a'4    d''8    e''8    \bar "|"   fis''4. ^"~"    g''8    
a''4    g''8    e''8    \bar "|"   fis''8    d''8    e''8    fis''8    d''8    
cis''8    a'8    g'8    \bar "|"   fis'8    d'8    a'8    fis'8    g'8    b'8   
 a'8    g'8    \bar "|"   fis'4    d'4    d'4    }   \repeat volta 2 {   r8 e'8 
   \bar "|"   f'8    a'8    d'8    e'8    f'!8    g'8    a'8    b'8    \bar "|" 
  c''4    d''8    b'8    c''8    a'8    g'8    b'8    \bar "|"   a'4    d''8    
e''8    fis''8    fis''8    e''8    d''8    \bar "|"   cis''8    a'8    d''8    
cis''8    a'4    d''8    e''8    \bar "|"   fis''4. ^"~"    g''8    a''8    
a''8    g''8    e''8  \bar "|"   fis''8    d''8    e''8    fis''8    d''8    
cis''8    a'8    g'8    \bar "|"   fis'8    d'8    a'8    fis'8    \tuplet 3/2 { 
  g'8    a'8    b'8  }   a'8    g'8    \bar "|"   fis'4    d'4    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
