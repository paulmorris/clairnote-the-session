\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/280#setting13029"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/280#setting13029" {"https://thesession.org/tunes/280#setting13029"}}}
	title = "Seanamhac Tube Station"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \dorian   g'8    f'8    d'8    d'4    f'8  \bar "|"   g'8    
a'8    c''8    d''4. ^"~"  \bar "|"   d''8    c''8    a'8    c''8    a'8    f'8 
 \bar "|"   g'8    f'8    d'8    c'8    d'8    f'8  \bar "|"   g'4. ^"~"    g'8 
   f'8    d'8  \bar "|"   g'8    a'8    c''8    d''8    c''8    e''8  \bar "|"  
 d''8    c''8    e''8    d''8    c''8    a'8  \bar "|"   g'4. ^"~"    g'4    
a'8  \bar "|"   g'8    f'8    d'8    d'4    f'8  \bar "|"   g'8    a'8    c''8  
<<   d''4.    d'4.   >> \bar "|"   d''8    c''8    a'8    a'8    bes'8    c''8  
\bar "|"   a'8    f'8    d'8    c'8    d'8    f'8  \bar "|"   g'4. ^"~"    g'8  
  f'8    d'8  \bar "|"   g'8    a'8    c''8    d''8    c''8    e''8  \bar "|"   
d''8    c''8    e''8    d''8    c''8    a'8  \bar "|"   g'4. ^"~"    g'8    a'8 
   c''8  \bar "||"   d''8    e''8    d''8    d''8    c''8    a'8  \bar "|"   
c''4. ^"~"    a'4    c''8  \bar "|"   d''8    c''8    a'8    c''8    a'8    f'8 
 \bar "|"   g'8    f'8    d'8    c'4    a'8  \bar "|" <<   d''4.    d'4.   >>   
d''8    c''8    a'8  \bar "|"   c''4. ^"~"    g''8    f''8    d''8  \bar "|"   
d''8    c''8    a'8    c''8    a'8    f'8  \bar "|"   g'4. ^"~"    g'8    a'8   
 c''8  \bar "|"   d''8    e''8    d''8    d''8    c''8    a'8  \bar "|"   c''4. 
^"~"    a'4    c''8  \bar "|"   d''8    c''8    a'8    c''8    a'8    f'8  
\bar "|"   g'8    f'8    d'8    c'4.  \bar "|"   g'8    f'8    d'8    g'8    
f'8    d'8  \bar "|"   g'8    a'8    c''8    d''4    e''8  \bar "|"   d''8    
c''8    a'8    c''8    a'8    f'8  \bar "|"   g'4. ^"~"    g'4    a'8  
\bar "||"   d''4.    g''4.  \bar "|"   f''4    g''8    a''4    f''8  \bar "|"   
g''8    f''8    d''8    c''4. ^"~"  \bar "|"   d''8    c''8    a'8    g'8    
a'8    c''8  \bar "|"   g''4. ^"~"    g''8    f''8    d''8  \bar "|"   f''4. 
^"~"    f''8    g''8    a''8  \bar "|"   g''8    f''8    d''8    c''8    a'8    
f'8  \bar "|"   g'4.    g'8    a'8    c''8  \bar "|"   d''4.    g''4.  \bar "|" 
  f''4    g''8    a''4    f''8  \bar "|"   g''8    f''8    d''8    c''4. ^"~"  
\bar "|"   d''8    c''8    a'8    g'8    f'8    d'8  \bar "|"   g'8    f'8    
d'8    g'8    f'8    d'8  \bar "|"   g'8    a'8    c''8    d''8    c''8    e''8 
 \bar "|"   d''8    c''8    e''8    d''8    c''8    a'8  \bar "|"   g'4    a'8  
  bes'8  \grace {    c''8  }   bes'8    a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
