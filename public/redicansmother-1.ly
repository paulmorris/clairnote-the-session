\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jomac"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/378#setting378"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/378#setting378" {"https://thesession.org/tunes/378#setting378"}}}
	title = "Redican's Mother"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   b8    d'8    e'8    b8    d'8    e'8    d'8    b8    
g8    \bar "|"   b8    d'8    e'8    b8    d'8    e'8    g'4. ^"~"    \bar "|"  
 b8    d'8    e'8    b8    d'8    e'8    d'8    b8    g8    \bar "|"   b8    
d'8    g'8    a'8    g'8    fis'8    e'4 ^"~"    d'8    \bar "|"     b8    d'8  
  e'8    b8    d'8    e'8    d'8    b8    g8    \bar "|"   b8    d'8    e'8    
b8    d'8    e'8    g'4. ^"~"    \bar "|"   b8    d'8    e'8    b8    d'8    
e'8    d'8    b8    g8    \bar "|"   b8    d'8    g'8    a'8    g'8    fis'8    
e'4 ^"~"    c''8    \bar "|"     b'4. ^"~"    a'4    a'8    g'4    c''8    
\bar "|"   b'4. ^"~"    b'4    b'8    c''4 ^"~"    c''8    \bar "|"   b'4. 
^"~"    a'4    a'8    g'8    e'8    d'8    \bar "|"   e'8    d'8    b8    g8    
d''8    d'8    e''4    d''8    \bar "|"     b'4. ^"~"    a'4    a'8    g'4    
c''8    \bar "|"   b'4. ^"~"    b'4    b'8    c''4 ^"~"    c''8    \bar "|"   
d''8    b'8    g'8    c''8    b'8    a'8    g'8    e'8    d'8    \bar "|"   e'8 
   d'8    b8    g8    b8    d'8    e'4 ^"~"    d'8  \bar "|"     fis'8    a'8   
 b'8    fis'8    a'8    b'8    a'8    fis'8    d'8    \bar "|"   fis'8    a'8   
 b'8    fis'8    a'8    b'8    d''4. ^"~"    \bar "|"   fis'8    a'8    b'8    
fis'8    a'8    b'8    a'8    fis'8    d'8    \bar "|"   fis'8    a'8    d''8   
 e''8    d''8    cis''8    b'4 ^"~"    a'8    \bar "|"     fis'8    a'8    b'8  
  fis'8    a'8    b'8    a'8    fis'8    d'8    \bar "|"   fis'8    a'8    b'8  
  fis'8    a'8    b'8    d''4. ^"~"    \bar "|"   fis'8    a'8    b'8    fis'8  
  a'8    b'8    a'8    fis'8    d'8    \bar "|"   fis'8    a'8    d''8    e''8  
  d''8    cis''8    b'4 ^"~"    g''8    \bar "|"     fis''4. ^"~"    e''4    
e''8    d''4    g''8    \bar "|"   fis''4. ^"~"    fis''4    fis''8    g''4. 
^"~"    \bar "|"   fis''4. ^"~"    e''4    e''8    d''8    b'8    a'8    
\bar "|"   b'8    a'8    fis'8    d'8    d''8    d'8    b''4    a''8    
\bar "|"     fis''4. ^"~"    e''4    e''8    d''4    g''8    \bar "|"   fis''4. 
^"~"    fis''4    fis''8    g''4    g''8    \bar "|"   a''8    fis''8    d''8   
 g''8    fis''8    e''8    d''8    b'8    a'8    \bar "|"   b'8    a'8    fis'8 
   d'8    fis'8    a'8    b'4 ^"~"    a'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
