\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ramiro"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/987#setting14188"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/987#setting14188" {"https://thesession.org/tunes/987#setting14188"}}}
	title = "Leslie's March"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   d''4.    d''4.    \bar "|"   d''8    cis''8    b'8    
a'8    b'8    cis''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    fis'8 
   \bar "|"   fis'16    g'16    a'8    fis'8    e'4    d'8    \bar "|"   e''4.  
  e''4.    \bar "|"   fis''8    e''8    d''8    e''8    a''8    g''8    
\bar "|"   fis''8    e''8    d''8    d''8    a'8    b'8    \bar "|"   d''8.    
e''16    fis''16    g''16    e''4    d''8    } \repeat volta 2 {   fis''8    
d''8    d''8    e''8    a'8    a'8    \bar "|"   fis''8    e''8    d''8    e''8 
   a'8    a'8    \bar "|"   g''8    e''8    e''8    fis''8    b'8    b'8    
\bar "|"   g''8    a''16    g''16    fis''16    e''16    fis''8    b'8    b'8   
 \bar "|"   fis''8    d''8    d''8    e''8    a'8    a'8    \bar "|"   fis''8   
 g''16    fis''16    e''16    d''16    e''8    b''8    g''8    \bar "|"   
fis''8    e''8    d''8    d''8    a'8    b'8    \bar "|"   d''8.    e''16    
fis''16    g''16    e''4    d''8    } \repeat volta 2 {   d''8    fis''8    
a''8    d''8    fis''8    a''8    \bar "|"   d''8    fis''8    a''8    a''8    
g''8    fis''8    \bar "|"   e''8    g''8    b''8    e''8    g''8    b''8    
\bar "|"   e''8    g''8    b''8    b''8    g''8    e''8    \bar "|"   d''8    
fis''8    a''8    d''8    fis''8    a''8    \bar "|"   d''8    fis''8    a''8   
 a''4    g''8    \bar "|"   fis''8    e''8    d''8    d''8    a'8    b'8    
\bar "|"   d''8.    e''16    fis''16    g''16    e''4    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
