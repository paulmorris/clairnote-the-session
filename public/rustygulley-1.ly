\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "geoffwright"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1208#setting1208"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1208#setting1208" {"https://thesession.org/tunes/1208#setting1208"}}}
	title = "Rusty Gulley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \major   \repeat volta 2 {   g'16    a'16    b'16    c''16    
d''8    g'8    b'8    g'8  \bar "|"   fis'8    a'8    a'8    c''8    b'8    a'8 
 \bar "|"   g'16    a'16    b'16    c''16    d''8    g'8    b'8    g'8  
\bar "|"   d'8    g'8    g'8    b'8    a'8    g'8  }     \repeat volta 2 {   
g''4    fis''4    e''8    g''8  \bar "|"   fis''8    d''8    d''8    fis''8    
e''8    d''8  \bar "|"   c''8    e''8    b'8    d''8    a'8    c''8  \bar "|"   
b'8    g'8    g'8    b'8    a'8    g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
