\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/969#setting14166"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/969#setting14166" {"https://thesession.org/tunes/969#setting14166"}}}
	title = "Buckley's Fancy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'4    b'8    g'8    d''8    g'8    b'8    g'8  
\bar "|"   c''4.    d''8    e''8    fis''8    g''8    r8 \bar "|"   g'4    b'8  
  g'8    d''8    g'8    b'8    g'8  \bar "|"   a'8    b'8    a'8    g'8    
fis'8    a'8    \tuplet 3/2 {   d'8    e'8    fis'8  } \bar "|"   g'4    b'8    
g'8    d''8    g'8    b'8    g'8  \bar "|"   c''4.    d''8    e''8    fis''8    
g''8    a''8  \bar "|"   b''8    g''8    g''8    a''8    g''8    e''8    d''8   
 b'8  \bar "|"   g'8    b'8    a'8    fis'8    g'8    b'8    d''8    b'8  
\bar ":|."   g'8    b'8    a'8    fis'8    g'8    a'8    b'8    d''8  \bar "||" 
  \bar ".|:"   g''4. ^"~"    a''8    g''8    e''8    d''8    e''8  \bar "|"   
g''4    b''8    g''8    a''8    g''8    e''8    g''8  \bar "|"   a''4. ^"~"    
b''8    a''8    g''8    e''8    g''8  \bar "|"   a''8    g''8    b''8    g''8   
 a''8    g''8    e''8    fis''8  \bar "|"   g''4. ^"~"    a''8    g''8    e''8  
  d''8    e''8  \bar "|"   g''4    b''8    g''8    a''8    g''8    e''8    
fis''8  \bar "|"   g''4. ^"~"    a''8    g''8    e''8    d''8    b'8  \bar "|"  
 g'8    b'8    a'8    fis'8    g'8    a'8    b'8    d''8  \bar ":|."   g'8    
b'8    a'8    fis'8    g'8    b'8    d''8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
