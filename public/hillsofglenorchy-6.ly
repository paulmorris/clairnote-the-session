\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/842#setting14011"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/842#setting14011" {"https://thesession.org/tunes/842#setting14011"}}}
	title = "Hills Of Glenorchy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   d''16.    cis''32    \bar "|"   b'8.    e'16    e'8  
  e'8.    fis'16    e'8    \bar "|"   b'8.    a'16    b'8    d''4    cis''32    
b'16.    \bar "|"   a'8.    fis'16    e'8    d'8.    e'16    d'8    \bar "|"   
fis'8    d'8    fis'8    a'4    d''32    cis''16.    \bar "|"     b'8.    e'16  
  e'8    e'8.    fis'16    e'8    \bar "|"   b'8    a'8    b'8    e''4    d''32 
   cis''16.    \bar "|"   d''8.    cis''16    b'8    a'16    fis'8.    a'8    
\bar "|"   b'8    e'8    e'8    e'4    \bar "||"     b'16    d''16    \bar "|"  
 e''8.    fis''16    e''8    b'8    cis''8    d''8    \bar "|"   e''8.    
fis''16    e''8    b'4    cis''8    \bar "|"   d''8.    cis''16    b'8    a'8   
 b'8    cis''8    \bar "|"   d''8.    cis''16    b'8    a'4    d''8    \bar "|" 
    e''8.    fis''16    e''8    b'8    cis''8    d''8    \bar "|"   e''8.    
fis''16    e''8    b'4    cis''16.    cis''32    \bar "|"   d''8.    cis''16    
b'8    a'16    fis'8.    a'8    \bar "|"   b'8    e'8    e'8    e'4    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
