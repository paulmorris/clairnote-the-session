\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/239#setting22990"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/239#setting22990" {"https://thesession.org/tunes/239#setting22990"}}}
	title = "Ballydesmond, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   b'16    \bar "|"   c''4    b'4   
 \bar "|"   a'4    g'8.    a'16    \bar "|"   b'8    d''8    e''8    d''8    
\bar "|"   g''4    e''8    d''8    \bar "|"     e''8    a''8    g''8.    e''16  
  \bar "|"   d''8    b'8    g'8.    b'16    \bar "|"   c''8    e''8    d''8    
b'8    \bar "|"   a'4    a'8.    }     \repeat volta 2 {   d''16    \bar "|"   
e''8    a''8    a''4    \bar "|"   d''8    g''8    g''4    \bar "|"   e''8    
a''8    a''8    fis''8    \bar "|"   g''4    e''8    d''8    \bar "|"     e''8  
  a''8    g''8.    e''16    \bar "|"   d''8    b'8    g'4    \bar "|"   c''8    
e''8    d''8    b'8    \bar "|"   a'4   ~    a'8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
