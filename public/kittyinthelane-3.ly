\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bleedin' Heart"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/569#setting24292"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/569#setting24292" {"https://thesession.org/tunes/569#setting24292"}}}
	title = "Kitty In The Lane"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   \repeat volta 2 {   d''8    c''8    a'8    b'8   
 c''4    b'8    a'8    \bar "|"   g'8    e'8    e'4    c''4    a'16    b'16    
c''8    \bar "|"   d''8    c''8    a'8    b'8    c''4    b'8    a'8    \bar "|" 
  g'8    e'8    c''8    e'8    e'8    d'8    d'4    \bar "|"     d''8    c''8   
 a'8    b'8    c''4    b'8    a'8    \bar "|"   g'8    e'8    e'4    c''8    
d''8    e''8    d''8    \bar "|"   d''8    c''8    a'8    b'8    c''8    d''8   
 e''8    d''8    \bar "|"   c''8    a'8    g'8    e'8    e'8    d'8    d'4    } 
    \repeat volta 2 {   a'8    d''8    d''8    e''8    fis''8    d''8    e''8   
 d''8    \bar "|"   c''8    d''8    e''8    fis''8    g''4    e''8    cis''8    
\bar "|"   a'8    d''8    d''8    e''8    fis''8    d''8    e''8    d''8    
\bar "|"   e''8    a''8    a''8    g''8    e''8    d''8    d''8    cis''8    
\bar "|"     a'8    d''8    d''8    e''8    fis''8    d''8    e''8    d''8    
\bar "|"   c''8    d''8    e''8    fis''8    g''4    e''16    fis''16    g''8   
 \bar "|"   a''8    fis''8    g''8    e''8    fis''8    d''8    cis''8    d''8  
  \bar "|"   e''8    a''8    a''8    g''8    e''8    d''8    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
