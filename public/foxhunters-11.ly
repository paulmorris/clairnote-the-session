\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "mrkelahan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/482#setting13391"
	arranger = "Setting 11"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/482#setting13391" {"https://thesession.org/tunes/482#setting13391"}}}
	title = "Foxhunter's, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \mixolydian   \bar "|"   d''16    c''16    \repeat volta 2 {   
b'4    b'8    b'8    g'8    b'8    c''4    a'8    \bar "|"   b'4    b'8    b'8  
  g'8    b'8    a'4    g'8    \bar "|"   b'4    b'8    b'8    g'8    b'8    
c''4    a'8    \bar "|"   d''4    g'8    g'8    a'8    b'8    a'4    g'8    }   
  \repeat volta 2 {   a'4    e''8    e''8    d''8    c''8    b'8    c''8    
d''8    \bar "|"   e''4    a'8    a'8    b'8    c''8    b'8    a'8    g'8    
\bar "|"   g'4    g''8    g''8    fis''8    e''8    d''8    b'8    d''8    
\bar "|"   g''4    g'8    g'8    a'8    b'8    a'4    g'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
