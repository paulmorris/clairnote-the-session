\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "AOG"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/668#setting668"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/668#setting668" {"https://thesession.org/tunes/668#setting668"}}}
	title = "Off To Alaska"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   \repeat volta 2 {   d''8.    e''16    fis''8    d''8  
\bar "|"   e''8    fis''8    g''8    e''8  \bar "|"   d''8.    e''16    fis''8  
  d''8  \bar "|"   e''8    cis''8    a'4  \bar "|"   d''8.    e''16    fis''8   
 d''8  \bar "|"   e''8    fis''8    g''8    e''8  \bar "|"   a''8.    b''16    
a''8    g''8  \bar "|"   e''8    cis''8    d''4    }     \repeat volta 2 {   
a''8.    b''16    a''8    fis''8  \bar "|"   e''8    cis''8    e''8    fis''8  
\bar "|"   a''8.    b''16    a''8    fis''8  \bar "|"   e''8    cis''8    e''4  
\bar "|"   a''8.    b''16    a''8    fis''8  \bar "|"   e''8    cis''8    b'8   
 a'8  \bar "|"   d''8    e''16    fis''16    g''8    fis''8  \bar "|"   e''8    
cis''8    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
