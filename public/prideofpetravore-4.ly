\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/82#setting30084"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/82#setting30084" {"https://thesession.org/tunes/82#setting30084"}}}
	title = "Pride Of Petravore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 {   e'4.    fis'8    g'4.    a'8  
\bar "|"   b'8.    c''16    b'8.    a'16    g'16    e'8.    e'4  \bar "|"   
d'4.    e'8    fis'4.    g'8  \bar "|"   a'8.    b'16    a'8.    g'16    fis'16 
   d'8.    d'4  \bar "|"     e'4.    fis'8    g'4.    a'8  \bar "|"   b'8.    
c''16    b'8.    a'16    g'16    e'8.    e'4  \bar "|"   b'4    b'8    c''8    
b'8.    a'16    g'8.    fis'16  \bar "|"   e'4    e'4    e'4    e'4  }     
\repeat volta 2 {   |
 e''4    e''4    e''4    e''4  \bar "|"   d''8.    b'16    g'8.    a'16    b'4  
  b'4  \bar "|"   c''8    fis'8    fis'4    fis'4    fis'4  \bar "|"   a'8    
e'8    e'4    e'4    e'4  \bar "|"     e''4    e''8    fis''8    g''4.    
fis''16    e''16  \bar "|"   d''8.    b'16    g'8.    a'16    b'4    g'8    
fis'8  \bar "|"   e'8.    b'16    b'8.    c''16    b'8.    a'16    g'8.    
fis'16  \bar "|"   e'4    e'4    e'4    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
