\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Manu Novo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1440#setting14822"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1440#setting14822" {"https://thesession.org/tunes/1440#setting14822"}}}
	title = "Sporting Days Of Easter, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   d''4    c''8    a'8    g'8    e'8    e'8    e'8  
\bar "|" \tuplet 3/2 {   a'8    c''8    a'8  }   g'8    e'8    \tuplet 3/2 {   
a'8    c''8    a'8  }   d'4  \bar "|"   d''4    c''8    a'8    g'8    e'8    
e'8    e'8  \bar "|" \tuplet 3/2 {   a'8    c''8    a'8  }   g'8    e'8    d'8   
 e'8    g'8    a'8  \bar "|"   d''4    c''8    a'8    g'8    e'8    e'8    e'8  
\bar "|" \tuplet 3/2 {   a'8    c''8    a'8  }   g'8    e'8    \tuplet 3/2 {   
a'8    c''8    a'8  }   d'4  \bar "|"   d''4    c''8    a'8    g'8    e'8    
e'8    e'8  \bar "|" \tuplet 3/2 {   a'8    c''8    a'8  }   g'8    e'8    d'2  
\bar "|"   c''4.    d''8    e''8    g''8    g''4 ^"~"  \bar "|"   g''8    a''8  
  a''8    g''8    e''8    g''8    g''8    e''8  \bar "|"   c''4.    d''8    
e''8    g''8    g''4 ^"~"  \bar "|"   a''4    g''8    e''8    d''4.    e''8  
\bar "|"   c''4.    d''8    e''8    g''8    g''4 ^"~"  \bar "|"   g''8    a''8  
  a''8    g''8    e''8    g''8    g''8    e''8  \bar "|"   fis''8    g''8    
e''8    d''8    c''8    a'8    g'4  \bar "|"   a'8    d''8    d''8    cis''8    
d''8    e''8    fis''8    e''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
