\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1232#setting23663"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1232#setting23663" {"https://thesession.org/tunes/1232#setting23663"}}}
	title = "Hundred Pipers"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   a'16    b'16    \bar "|"   cis''4    e'8    e'8.    
fis'16    e'8    \bar "|"   fis'4    a'8    a'4    fis''8    \bar "|"   e''4    
cis''8    cis''8.    b'16    a'8    \bar "|"   b'4    b'8    b'4    a'16    
b'16    \bar "|"     cis''4    e'8    e'8.    fis'16    e'8    \bar "|"   fis'4 
   a'8    a'4    fis''8    \bar "|"   e''4    cis''8    b'8.    cis''16    b'8  
  \bar "|"   a'4    a'8    a'4    \bar "||"     cis''16    d''16    \bar "|"   
e''4    e''8    e''8.    cis''16    e''8    \bar "|"   fis''4    a''8    a''4   
 fis''8    \bar "|"   e''4    cis''8    cis''8.    b'16    a'8    \bar "|"   
b'4    b'8    b'8    a'16    b'8    \bar "|"     cis''4    e'8    e'8.    
fis'16    e'8    \bar "|"   fis'4    a'8    a'4    fis''8    \bar "|"   e''4    
cis''8    b'8.    cis''16    b'8    \bar "|"   a'4    a'8    a'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
