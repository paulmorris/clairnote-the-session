\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/332#setting13115"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/332#setting13115" {"https://thesession.org/tunes/332#setting13115"}}}
	title = "White Petticoat, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   \repeat volta 2 {   b'8    \bar "|"   b'8    e''8    
d''8    c''8    b'8    a'8    \bar "|"   g'8    fis'8    e'8    b8    e'8    
g'8    \bar "|"   fis'8    b'8    b'8  \grace {    d''16  }   c''8    b'8    
b'8    \bar "|"   e''8    b'8    b'8  \grace {    d''16  }   c''8    b'8    a'8 
   \bar "|"     b'8    e''8    d''8  \grace {    d''16  }   c''8    b'8    a'8  
  \bar "|"   g'8    fis'8    e'8    b8    e'8    g'8    \bar "|"   fis'8    b'8 
   b'8  \grace {    d''16  }   c''8    b'8    a'8    \bar "|"   g'8    e'8    
e'8    e'4    }     \repeat volta 2 {   g''8    \bar "|" \grace {    a''16  }   
g''8    e''8    c''8    g'8    c''8    e''8    \bar "|"   g''8    a''8    
fis''8  \grace {    a''16  }   g''8    e''8    c''8    \bar "|" \grace {    
d''16  }   c''8    b'8    a'8    e'8    a'8    c''8    \bar "|"   b'8    e''8   
 dis''8    e''8    b'8    e'8    \bar "|"     fis'8    b'8    b'8    g'8    b'8 
   b'8    \bar "|"   fis'8    b'8    a'8    g'8    fis'8    e'8    \bar "|"   
b8    e'8    g'8    c''8    b'8    a'8    \bar "|"   g'8    e'8    e'8    e'4   
 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
