\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "walterbracht"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1497#setting14886"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1497#setting14886" {"https://thesession.org/tunes/1497#setting14886"}}}
	title = "Donegal Lass, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian <<   \repeat volta 2 {   a'8    cis''8    d''4    
e''4    a''4    e''4    d''4  \bar "|"   cis''4.    d''8    b'4    a'2    fis'4 
 \bar "|"   g'4    b'8    cis''8    d''4    g'4    b'8    cis''8    d''4  
\bar "|"   fis'4    a'8    cis''8    d''4    fis'2.  \bar "|"   a'8    cis''8   
 d''4    e''4    a''4    e''4    d''4  \bar "|"   cis''4.    d''8    b'4    
a'2.  \bar "|"   a''4    r4 e''8    d''8    cis''4    d''4    a'4  \bar "|"   
b'4    a'4    g'4    a'2.  }  >> <<   \repeat volta 2 {   g'4    b'8    cis''8  
  d''4    g'4    b'8    cis''8    d''4  \bar "|"   fis'4    a'8    cis''8    
d''4    fis'2.  \bar "|"   e''2    e''4    e''4    cis''4    a'4  \bar "|"   
e''2    cis''4    d''4    cis''4    a'4  \bar "|"   g'4    b'8    cis''8    
d''4    g'4    b'8    cis''8    d''4  \bar "|"   fis'4    a'8    cis''8    d''4 
   fis''2    a''4  \bar "|"   a''4    r4 e''8    d''8    cis''4    d''4    a'4  
\bar "|"   b'4    a'4    g'4    a'2.  }  >>   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
