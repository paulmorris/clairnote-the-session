\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/412#setting13267"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/412#setting13267" {"https://thesession.org/tunes/412#setting13267"}}}
	title = "Cliffs, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key f \major   c''8    a'8  \bar "|"   f'8    a'8    c''8    a'8    
g'8    bes'8    d''8    bes'8  \bar "|"   a'8    c''8    f''8    g''8    a''8   
 f''8    c''8    a'8  \bar "|"   bes'8    d''8    g''8    bes''8    a'8    c''8 
   f''8    a''8  \bar "|" \tuplet 3/2 {   g''8    a''8    g''8  }   \tuplet 3/2 { 
  f''8    e''8    d''8  }   \tuplet 3/2 {   c''8    d''8    c''8  }   
\tuplet 3/2 {   bes'8    a'8    g'8  } \bar "|"   f'8    a'8    c''8    a'8    
g'8    bes'8    d''8    bes'8  \bar "|"   a'8    c''8    f''8    g''8    a''8   
 f''8    c''8    a'8  \bar "|"   bes'8    d''8    g''8    f''8    e''8    c''8  
  d''8    e''8  \bar "|"   f''4    a''4    f''4  }   \repeat volta 2 {   e''8   
 f''8  \bar "|"   g''8    c''8    \tuplet 3/2 {   c''8    c''8    c''8  }   a''8 
   c''8    \tuplet 3/2 {   c''8    c''8    c''8  } \bar "|"   bes''8    c''8    
a''8    c''8    g''8    c''8    \tuplet 3/2 {   c''8    c''8    c''8  } \bar "|" 
  bes''8    c''8    a''8    c''8    g''8    f''8    e''8    f''8  \bar "|" 
\tuplet 3/2 {   g''8    a''8    g''8  }   \tuplet 3/2 {   f''8    e''8    d''8  } 
  \tuplet 3/2 {   c''8    d''8    c''8  }   \tuplet 3/2 {   bes'8    a'8    g'8  
} \bar "|"   f'8    a'8    c''8    a'8    g'8    bes'8    d''8    bes'8  
\bar "|"   a'8    c''8    f''8    g''8    a''8    f''8    c''8    a'8  \bar "|" 
  bes'8    d''8    g''8    f''8    e''8    c''8    d''8    e''8  \bar "|"   
f''4    a''4    f''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
