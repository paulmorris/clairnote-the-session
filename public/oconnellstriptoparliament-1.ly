\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1443#setting1443"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1443#setting1443" {"https://thesession.org/tunes/1443#setting1443"}}}
	title = "O'Connell's Trip To Parliament"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   d''8    cis''8    a'8    fis'8    g'4    fis'8    g'8 
 \bar "|"   a'8    d''8    d''4 ^"~"    fis''8    d''8    e''8    d''8  
\bar "|"   d''8    cis''8    a'8    fis'8    g'4    fis'8    g'8  \bar "|"   
a'8    fis'8    g'8    e'8    fis'8    d'8    d'4  }     \repeat volta 2 {   
fis''4.    a''8    g''8    fis''8    e''8    d''8  \bar "|" \tuplet 3/2 {   b'8  
  cis''8    d''8  }   e''8    fis''8    g''4    fis''8    e''8  \bar "|"   d''8 
   e''8    fis''8    g''8    a''4    g''8    e''8  \bar "|"   a''4    g''8    
e''8    fis''8    d''8    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
