\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/29#setting22080"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/29#setting22080" {"https://thesession.org/tunes/29#setting22080"}}}
	title = "Dusty Windowsills, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {   a'4 ^"Am"   b'8    c''8    b'8   
 a'8  \bar "|"   e''8 ^"Am"   a'8    b'8    c''8    b'8    a'8  \bar "|"   g'8 
^"G"   a'8    g'8    fis'8    g'8    g'8  \bar "|"   e'8 ^"G"   g'8    g'8    
e'8    fis'8    g'8  \bar "|"       a'4 ^"Am"   b'8    c''8    b'8    a'8  
\bar "|"   e''4 ^"Am"   d''8    e''8    fis''8    g''8  \bar "|"   a''8 ^"G"   
g''8    e''8    d''8    b'8    g'8  \bar "|"   a'8 ^"G"   b'8    a'8      a'4. 
^"Am" }     \repeat volta 2 {   a''4. ^"Am"   a''8    g''8    e''8  \bar "|"   
d''8 ^"G"   b'8    d''8    g''4.  \bar "|"   g''8 ^"G"   a''8    g''8      g''8 
^"C"   fis''8    e''8  \bar "|"   d''8 ^"G"   b'8    a'8    g'8    a'8    g'8  
\bar "|"       e'8 ^"C"   g'8    g'8      d'8 ^"G"   g'8    g'8  \bar "|"   e'8 
^"C"   fis'8    g'8      a'8 ^"Am"   b'8    c''8  \bar "|"   b'8 ^"G"   e''8    
e''8    d''8    b'8    g'8  \bar "|"   a'8 ^"G"   b'8    a'8      a'4. ^"Am" }  
   \repeat volta 2 {   a'4 ^"Am"   a'8    g''8    a'8    fis''8  \bar "|"   a'4 
^"Am"   a'8      g''8 ^"Em"   a'8    fis''8  \bar "|"   g'4 ^"G"   g'8    e''8  
  g'8    d''8  \bar "|"   g'4 ^"G"   g'8      e''8 ^"Em"   d''8    b'8  
\bar "|"       a'4 ^"Am"   a'8    g''8    a'8    fis''8  \bar "|"   a'4 ^"Am"   
d''8      e''8 ^"Em"   fis''8    g''8  \bar "|"   a''8 ^"Am"   g''8    e''8     
 d''8 ^"G"   b'8    g'8  \bar "|"   a'8 ^"Am"   b'8    a'8    a'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
