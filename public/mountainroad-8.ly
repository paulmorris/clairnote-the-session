\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "mrkelahan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/68#setting24724"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/68#setting24724" {"https://thesession.org/tunes/68#setting24724"}}}
	title = "Mountain Road, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \bar "|"   a'8    b'8    \bar "||"   cis''4    e''8   
 cis''8    fis''8    cis''8    e''8    cis''8    \bar "|"   cis''4 ^"~"    e''8 
   cis''8    b'8    cis''8    a'8    b'8    \bar "|"   cis''4    e''8    cis''8 
   fis''8    cis''8    e''8    cis''8    \bar "|"   d''4. ^"~"    cis''8    b'8 
   a'8    fis'8    a'8    \bar "|"     cis''4    e''8    cis''8    fis''8    
cis''8    e''8    cis''8    \bar "|"   cis''4 ^"~"    e''8    cis''8    b'8    
cis''8    a'8    b'8    \bar "|"   cis''4.    e''8    fis''8    e''8    cis''8  
  e''8    \bar "|"   d''8    e''8    fis''8    a''8    cis''8    a''8    a''4   
 \bar "||"     \bar "||"   a''16    a''16    a''8    a''8    fis''8    e''8    
a'8    cis''8    e''8    \bar "|"   a''4. ^"~"    b'8    cis''8    d''8    
cis''8    b''8    \bar "|"   a''8    b''8    cis''8    a''8    e''8    cis''8   
 a'8    cis''8    \bar "|"   \tuplet 3/2 {   d''8    cis''8    b'8  }   
\tuplet 3/2 {   cis''8    b'8    a'8  }   b'8    fis'8    fis'8    a'8    
\bar "|"     a''4. ^"~"    fis''8    e''8    a'8    cis''8    e''8    \bar "|"  
 a''8    gis''8    a''8    b'8    cis''8    d''8    cis''8    b''8    \bar "|"  
 a''8    b''8    cis''8    a''8    e''8    cis''8    a'8    cis''8    \bar "|"  
 \tuplet 3/2 {   d''8    cis''8    b'8  }   \tuplet 3/2 {   cis''8    b'8    a'8  
}   b'8    fis'8    fis'8    a'8    \bar "||"     \bar "||"   a'8    cis''8    
e''8    cis''8    fis''8    cis''8    e''8    cis''8    \bar "|"   a'8    
cis''8    e''8    cis''8    b'8    e'8    e'4    \bar "|"   a'8    cis''8    
cis''8    e''8    d''8    cis''8    a'8    d''8    \bar "|"   d''8    e''8    
fis''8    a''8    cis''8    a''8    a''8    \bar "|"     b''8    \bar "|" 
\grace {    b''8  }   a''8    gis''8    fis''8    e''8    fis''8    e''8    
cis''8    fis''8    \bar "|"   e''8    cis''8    b'8    d''8    cis''8    a'8   
 a'8    b'8    \bar "|"   cis''4    e''8    cis''8    fis''8    cis''8    e''8  
  cis''8    \bar "|"   e'8    fis'8    a'8    b'8    cis''8    a'8    a'8    
gis'8    \bar "||"     \bar "||"   a'4. ^"~"    b'8    cis''8    a'8    b'8    
a'8    \bar "|"   a'4 ^"~"    a'8    b'8    cis''8    fis'8    fis'8    gis'8   
 \bar "|"   a'8    b'8    cis''8    a'8    b'8    gis'8    a'8    fis'8    
\bar "|"   e''8    d''8    cis''8    a'8    b'8    fis'8    fis'8    a'8    
\bar "|"     a'4. ^"~"    b'8    cis''8    a'8    b'8    a'8    \bar "|"   a'4 
^"~"    a'8    b'8    cis''8    fis'8    fis'8    cis''8    \bar "|"   
\tuplet 3/2 {   d''8    e''8    d''8  }   \tuplet 3/2 {   cis''8    d''8    
cis''8  }   \tuplet 3/2 {   b'8    cis''8    b'8  }   a'8    fis'8    \bar "|"   
e'8    fis'8    a'8    b'8    cis''8    a'8    a'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
