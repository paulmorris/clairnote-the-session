\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Avery"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/314#setting13084"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/314#setting13084" {"https://thesession.org/tunes/314#setting13084"}}}
	title = "Poor But Happy At 53"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \mixolydian   d'4.    b'8    a'8    g'8    a'8    b'8  
\bar "|"   d''8    g'8    b'8    g'8    a'8    g'8    e'8    g'8  \bar "|"   
d'8    e'8    g'8    b'8    a'8    g'8    a'8    b'8  \bar "|"   c''8    a'8    
a'4 ^"~"    b'8    g'8    g'4 ^"~"  }   d''4    d''8    e''8    d''2  \bar "|"  
 d''8    b'8    d''8    e''8    g''8    e''8    d''8    b'8  \bar "|"   c''8    
a'8    b'8    g'8    a'8    g'8    e'8    g'8  \bar "|" \tuplet 3/2 {   b'8    
c''8    d''8  }   e''8    d''8    b'8    a'8    g'8    e''8  \bar ":|." 
\tuplet 3/2 {   b'8    c''8    d''8  }   e''8    d''8    b'8    a'8    g'8    
e'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
