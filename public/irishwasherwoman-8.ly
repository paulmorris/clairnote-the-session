\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/92#setting12638"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/92#setting12638" {"https://thesession.org/tunes/92#setting12638"}}}
	title = "Irish Washerwoman, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   e'16    fis'16    g'8    a'8    
\bar "|"   b'4    g'8    d'4    g'8    \bar "|"   b'8    ais'8    b'8    d''8   
 c''8    b'8    \bar "|"   c''4    a'8    e'4    a'8    \bar "|"   c''8    a'16 
   b'16    c''8    e''8    d''8    c''8    \bar "|"     b'8    g'8    g'8    
d'8    g'8    g'8    \bar "|"   b'8    g'16    a'16    b'8    d''8    c''8    
b'8    \bar "|"   c''8.    b'16    c''8    a'8    d''8    c''8    \bar "|"   
b'8    g'8    g'8    }     \repeat volta 2 {   g'8    b'16    c''16    d''8    
\bar "|"   g''8    fis''8    g''8    g'8    b'16    c''16    d''8    \bar "|"   
g''8    fis''8    g''8    b''8    g''8    e''8    \bar "|"   fis''8    eis''8   
 fis''8    fis'8    a'8    d''8    \bar "|"   fis''8    e''8    fis''8    a''8  
  fis''8    d''8    \bar "|"     e''8    g''8    g''8    d''4    g''8    
\bar "|"   c''8    g''8    g''8    b'8    ais'8    b'8    \bar "|"   c''8    
b'8    c''8    a'8    d''8    c''8    \bar "|"   b'8    g'8    g'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
