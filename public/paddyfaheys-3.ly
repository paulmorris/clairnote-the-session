\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Reverend"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1402#setting14772"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1402#setting14772" {"https://thesession.org/tunes/1402#setting14772"}}}
	title = "Paddy Fahey's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key c \major   \repeat volta 2 {   c''4    e''8    g''8    f''8    
d''8    b'8    d''8  \bar "|"   c''8    b'8    g'8    f'8    d'8    e'8    f'8  
  d'8  \bar "|"   g'8    c''8    c''4 ^"~"    g''8    c''8    e''8    g''8  
\bar "|"   f''8    d''8    c''8    b'8    g'8    c''8    b'8    d''8  \bar "|"  
 c''4    d''8    e''8    f''8    d''8    b'8    d''8  \bar "|"   c''8    b'8    
g'8    a'8    bes'4    bes'8    c''8  \bar "|"   d''8    g''8  \tuplet 3/2 {   
g''8    g''8    g''8  }   d''8    e''8    f''8    d''8  \bar "|"   g''8    f''8 
   d''8    c''8    b'8    g'8    a'8    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
