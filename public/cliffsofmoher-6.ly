\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fiddle and pick"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/12#setting23642"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/12#setting23642" {"https://thesession.org/tunes/12#setting23642"}}}
	title = "Cliffs Of Moher, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key a \dorian   e''8    a''8    a''8    b''8    a''8    g''8  
\bar "|"   e''8    a''8    fis''8    g''8    e''8    d''8  \bar "|"   c''4    
a'8    b'8    a'8    g'8  \bar "|"   e'8    fis'8    g'8    a'8    b'8    d''8  
\bar "|"     e''8    a''8    a''8    b''8    a''8    g''8  \bar "|"   e''8    
a''8    fis''8    g''8    e''8    d''8  \bar "|"   c''4    a'8    b'8    a'8    
g'8  \bar "|"   e'8    fis'8    g'8    a'4.  }     \repeat volta 2 {   e''8    
fis''8    e''8    d''8    b'8    a'8  \bar "|"   e''8    fis''8    e''8    d''8 
   b'8    a'8  \bar "|"   g'8    a'8    b'8    d''8    b'8    a'8  \bar "|"   
g'8    a'8    b'8    d''8    b'8    d''8  \bar "|"     } \alternative{{   e''8  
  fis''8    e''8    d''8    b'8    a'8  \bar "|"   e''8    fis''8    e''8    
d''8    b'8    a'8  \bar "|"   g'8    a'8    b'8    d''8    b'8    g'8  
\bar "|"   e'8    fis'8    g'8    a'4.  } }      \bar "|"   e''8    fis''8    
e''8    d''8    e''8    e''8  \bar "|"   c''8    e''8    e''8    b'8    e''8    
e''8  \bar "|"   e'8    fis'8    g'8    b'8    a'8    g'8  \bar "|"   e'8    
d'8    b8    a4.  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
