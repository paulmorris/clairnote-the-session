\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/660#setting22845"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/660#setting22845" {"https://thesession.org/tunes/660#setting22845"}}}
	title = "Burning Of The Piper's Hut, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 {   e'8    fis'8    g'8    a'8    b'2 
 \bar "|"   b'8    a'8    b'8    d''8    b'4    a'8    g'8  \bar "|"   fis'8    
d'8    d'4    a'8    d'8    d'4  \bar "|"   fis'8    g'8    a'8    b'8    a'4   
 g'8    fis'8  \bar "|"     e'8    fis'8    g'8    a'8    b'4.    b'8  \bar "|" 
  b'8    a'8    b'8    d''8    b'4    a'8    g'8  \bar "|"   fis'8    d'8    
d'4    b'4    a'8    g'8  \bar "|"   fis'4    e'4    e'4.    d'8  }     
\repeat volta 2 {   e'8    fis'8    g'8    a'8    b'8    e'8    e'4  \bar "|"   
d''8    e'8    e'4    b'4    a'8    g'8  \bar "|"   fis'8    d'8    d'4    a'8  
  d'8    d'4  \bar "|"   fis'8    g'8    a'8    b'8    a'4    g'8    fis'8  
\bar "|"     e'8    fis'8    g'8    a'8    b'8    e'8    e'4  \bar "|"   d''8   
 e'8    e'4    b'4    a'8    g'8  \bar "|"   fis'8    d'8    d'4    b'4    a'8  
  g'8  \bar "|"   fis'4    e'4    e'4.    d'8  }     \repeat volta 2 {   e'8    
b'8    b'4    d''8    b'8    b'4  \bar "|"   e'8    b'8    b'4    d''8    b'8   
 a'8    g'8  \bar "|"   fis'8    d'8    d'4    a'8    d'8    d'4  \bar "|"   
fis'8    a'8    a'4    d''8    b'8    a'8    fis'8  \bar "|"     e'8    b'8    
b'4    d''8    b'8    b'4  \bar "|"   e'8    b'8    b'4    d''4    a'8    g'8  
\bar "|"   fis'8    d'8    d'4    b'4    a'8    g'8  \bar "|"   fis'8    e'8    
e'4    e'4    b'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
