\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SPeak"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1086#setting1086"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1086#setting1086" {"https://thesession.org/tunes/1086#setting1086"}}}
	title = "Hughie Shortie's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major     g'4 ^"G"   b'8    g'8      c''8 ^"D"   a'8    fis'8 
   a'8  \bar "|"     g'8 ^"G"   a'8    b'8    d''8    g''4    fis''8    g''8  
\bar "|"     e''8 ^"C"   c''8    a'8    c''8      d''8 ^"G"   b'8    g'8    b'8 
 \bar "|"     c''8 ^"Am"   a'8    a'8    b'8      a'8 ^"D"   fis'8    d'4  
\bar "|"       d'8 ^"G"   g'8    b'8    g'8      c''8 ^"D"   a'8    fis'8    
a'8  \bar "|"     g'8 ^"G"   a'8    b'8    d''8    g''4    fis''8    g''8  
\bar "|"     e''8 ^"C"   c''8    a'8    c''8      d''8 ^"G"   b'8    g'8    b'8 
 \bar "|"     c''8 ^"D"   a'8    fis'8    a'8      g'2 ^"G"   }       g''4 ^"G" 
  d''8    g''8    b''8    g''8    a''8    g''8  \bar "|"     e''8 ^"C"   c''8   
 c''16    c''16    c''8      e''8 ^"Am"   fis''8    g''8    e''8  \bar "|"     
d''8 ^"G"   b'8    b'16    b'16    b'8      d''8 ^"Em"   b'8    g'8    b'8  
\bar "|"     c''8 ^"C"   a'8    a'8    b'8      a'8 ^"D"   fis'8    d'4  
\bar "|"       g''4 ^"G"   d''8    g''8    b''8    g''8    a''8    g''8  
\bar "|"     e''8 ^"C"   c''8    c''16    c''16    c''8    e''8    fis''8    
g''8    e''8  \bar "|"     d''8 ^"G"   b'8    b'16    b'16    b'8      d''8 
^"C"   b'8    g'8    b'8  \bar "|"     c''8 ^"D"   a'8    g'8    fis'8      g'2 
^"G" \bar "|"       g''4 ^"G"   d''8    g''8    b''8    g''8    a''8    g''8  
\bar "|"     e''8 ^"C"   c''8    c''16    c''16    c''8      e''8 ^"Am"   
fis''8    g''8    e''8  \bar "|"     d''8 ^"G"   b'8    b'16    b'16    b'8     
 d''8 ^"Em"   b'8    g'8    b'8  \bar "|"     c''8 ^"C"   a'8    a'8    b'8     
 a'8 ^"D"   fis'8    d'4  \bar "|"       g8 ^"C"   c'8    e'8    c'8      b8 
^"G"   d'8    d''8    b'8  \bar "|"     c''8 ^"D"   d''8    e''8    fis''8      
g''4 ^"G"   fis''8    g''8  \bar "|"     e''8 ^"C"   c''8    a'8    c''8      
d''8 ^"G"   b'8    g'8    b'8  \bar "|"     c''8 ^"D"   a'8    fis'8    a'8     
 g'2 ^"G" \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
