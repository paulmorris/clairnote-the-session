\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "The Whistler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/814#setting814"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/814#setting814" {"https://thesession.org/tunes/814#setting814"}}}
	title = "Amhrán Na Leabhar"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key e \dorian   \repeat volta 2 {   b'4  \bar "|"   e'2    fis'4  
\bar "|"   g'2    a'4  \bar "|"   b'8    e''4.   ~    e''4   ~    \bar "|"   
e''2    fis''4  \bar "|"   e''2    d''4  \bar "|"   b'2    a'4  \bar "|"   b'8  
  cis''8    d''2   ~    \bar "|"   d''2    e''4  \bar "|"     e'2    fis'4  
\bar "|"   g'2    fis'4  \bar "|"   g'8    a'8    b'2   ~    \bar "|"   b'2    
a'4  \bar "|"   fis'2    e'4   ~    \bar "|"   e'2    d'4  \bar "|"   e'2.   ~  
  \bar "|"   e'2  }     \repeat volta 2 {   d''4  \bar "|"   e''4    e''2  
\bar "|"   e''2    d''4  \bar "|"   e''8    fis''8    g''2   ~    \bar "|"   
g''2    fis''4  \bar "|"   e''2    d''4  \bar "|"   b'2    a'4  \bar "|"   b'2. 
  ~    \bar "|"   b'2    cis''4  \bar "|"     d''2.  \bar "|"   d''2    cis''4  
\bar "|"   d''4    e''2   ~    \bar "|"   e''2    d''4  \bar "|"   b'2    a'4  
\bar "|"   g'2    a'4  \bar "|"   b'2.   ~    \bar "|"   b'2    b'4  \bar "|"   
  e'2    fis'4  \bar "|"   g'2    a'4  \bar "|"   b'4    e''2   ~    \bar "|"   
e''2    fis''4  \bar "|"   e''2    d''4  \bar "|"   b'2    a'4  \bar "|"   b'8  
  cis''8    d''2   ~    \bar "|"   d''2    e''4  \bar "|"     e'2    fis'4  
\bar "|"   g'2    fis'4  \bar "|"   g'8    a'8    b'2   ~    \bar "|"   b'2    
a'4  \bar "|"   fis'4    e'2   ~    \bar "|"   e'2    d'4  \bar "|"   e'2.   ~  
  \bar "|"   e'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
