\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceili"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/552#setting20739"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/552#setting20739" {"https://thesession.org/tunes/552#setting20739"}}}
	title = "Albert House"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a'8    b'8  \repeat volta 2 {   cis''4    e''8    
cis''8    b'8    d''8    \tuplet 3/2 {   d''8    cis''8    b'8  } \bar "|"   a'8 
   e''8    cis''8    a'8    b'8    cis''8    a'8    fis'8  \bar "|"   e'4    
a'8    b'8    \tuplet 3/2 {   cis''8    b'8    a'8  }   e''8    cis''8  \bar "|" 
  b'8    a'8    fis'8    a'8    b'4    a'8    b'8    \bar "|"     cis''4    
e''8    cis''8    b'8    d''8    \tuplet 3/2 {   d''8    cis''8    b'8  } 
\bar "|"   a'8    e''8    cis''8    a'8    b'8    cis''8    a'8    fis'8  
\bar "|"   e'4    a'8    b'8    \tuplet 3/2 {   cis''8    b'8    a'8  }   e''8   
 cis''8  } \alternative{{   b'8    a'8    fis'8    e'8    a'4    a'8    b'8    
} {   b'8    a'8    fis'8    e'8    a'4    a'8    cis''8  \bar "||"     
\bar "||"   e''4    e''8    fis''8    e''8    cis''8    b'8    a'8    \bar "|"  
 \tuplet 3/2 {   a''8    a''8    a''8  }   gis''8    a''8    fis''8    d''8    
a''8    fis''8    \bar "|"   e''4    e''8    fis''8    e''8    cis''8    a'8    
cis''8    \bar "|"   e''8    cis''8    a'8    cis''8    b'4    cis''8    d''8   
 \bar "|"     e''4    e''8    fis''8    e''8    cis''8    b'8    a'8    
\bar "|"   \tuplet 3/2 {   a''8    a''8    a''8  }   gis''8    a''8    fis''8    
d''8    a''8    fis''8  \bar "|"   e''8    cis''8    a''8    e''8    fis''8    
d''8    a''8    fis''8  \bar "|"   e''8    cis''8    d''8    b'8    a'4    a'8  
  cis''8    \bar "|"     e''4    e''8    fis''8    e''8    cis''8    b'8    a'8 
   \bar "|"   \tuplet 3/2 {   a''8    a''8    a''8  }   gis''8    a''8    fis''8 
   d''8    a''8    fis''8    \bar "|"   e''4    e''8    fis''8    e''8    
cis''8    a'8    cis''8    \bar "|"   e''8    cis''8    a'8    cis''8    b'4    
cis''8    d''8    \bar "|"     \bar "|"   e''8    a''8    a''8    b''8    a''8  
  fis''8    d''8    fis''8  \bar "|"   e''8    cis''8    \tuplet 3/2 {   cis''8  
  cis''8    b'8  }   cis''8    a'8    fis'8    a'8  \bar "|" \tuplet 3/2 {   e'8 
   e'8    e'8  }   a'8    b'8    cis''8    a'8    e''8    cis''8  \bar "|"   
b'8    a'8    fis'8    e'8    a'4    r8 \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
