\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1454#setting14845"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1454#setting14845" {"https://thesession.org/tunes/1454#setting14845"}}}
	title = "Wallop The Potlid"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   fis'4    d'8    d'4. ^"~"  \bar "|"   e'4    d'8    
d'4. ^"~"  \bar "|"   fis'4    d'8    d'4. ^"~"  \bar "|"   d'8    fis'8    a'8 
   b'8    g'8    e'8    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
