\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/442#setting13306"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/442#setting13306" {"https://thesession.org/tunes/442#setting13306"}}}
	title = "Flowers Of Red Hill, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   e''8    a'8    a'4 ^"~"    g''4    fis''8    g''8  
\bar "|"   e''8    a'8    a'4 ^"~"    g'8    a'8    b'8    d''8  \bar "|"   
e''8    a'8    a'4 ^"~"    e''4.    fis''8  \bar "|"   g''8    e''8    d''8    
b'8    g'4    b'8    d''8  }   e''8    a''8    a''4 ^"~"    b''8    a''8    
a''4 ^"~"  \bar "|"   e''8    a''8    a''4 ^"~"    e''8    g''8    d''8    g''8 
 \bar "|"   e''8    a''8    a''4 ^"~"    b''8    a''8    a''4 ^"~"  \bar "|"   
g''8    e''8    d''8    e''8    g''4    fis''8    g''8  \bar "|"   e''8    a''8 
   a''4 ^"~"    b''8    a''8    a''4 ^"~"  \bar "|"   e''8    a''8    a''4 
^"~"    g''4.    a''8  \bar "|"   b''8    g''8    g''4 ^"~"    a''8    fis''8   
 fis''4 ^"~"  \bar "|"   e''4.    fis''8    g''8    e''8    d''8    g''8  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
