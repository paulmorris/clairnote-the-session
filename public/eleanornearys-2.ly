\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "donnchad"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/742#setting13829"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/742#setting13829" {"https://thesession.org/tunes/742#setting13829"}}}
	title = "Eleanor Neary's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   a'8    b'8    \bar "|"   c''8    b'8    c''8    d''8  
  c''8    b'8    a'8    g'8    \bar "|"   fis'8    c'8    fis'8    g'8    a'8   
 b'8    a'8    b'8    \bar "|"   c''8    b'8    \tuplet 3/2 {   a'8    b'8    
c''8  }   fis''8    e''8    c''8    a'8    \bar "|"   \tuplet 3/2 {   b'8    
c''8    b'8  }   a'8    fis'8    e'4    a'8    b'8    \bar "|"   c''4. ^"~"    
d''8    c''8    b'8    a'8    g'8    \bar "|"   fis'8    c'8    fis'8    g'8    
a'8    a''8    g''8    fis''8    \bar "|"   e''8    c''8    \tuplet 3/2 {   d''8 
   c''8    b'8  }   c''8    a'8    b'8    g'8    \bar "|"   a'8    b'8    
\tuplet 3/2 {   g'8    a'8    b'8  }   a'8    e'8    \bar ":|."   a'8    b'8    
\tuplet 3/2 {   g'8    a'8    b'8  }   a'4    e'8    d'8    \bar "|"   
\tuplet 3/2 {   c'8    b8    a8  }   c'8    e'8    fis'8    d'8    fis'8    a'8  
  \bar "|"   \tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    g'8    a'8    g'8   
 a'8    b'8    \bar "|"   c''8    b'8    a'8    c''8    d''4. ^"~"    c''8    
\bar "|"   b'8    c''8    a'8    fis'8    e'8    c'8    d'8    b8    \bar "|"   
c'8    a8    c'8    e'8    fis'8    d'8    fis'8    a'8    \bar "|"   
\tuplet 3/2 {   g'8    a'8    b'8  }   \tuplet 3/2 {   e'8    fis'8    g'8  }   
a'8    a''8    g''8    fis''8    \bar "|"   e''8    c''8    d''8    b'8    c''8 
   a'8    b'8    g'8    \bar "|"   a'4 ^"~"    g'8    b'8    a'4    e'8    d'8  
  \bar ":|."   a'4 ^"~"    \tuplet 3/2 {   g'8    a'8    b'8  }   a'4  \bar "|"  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
