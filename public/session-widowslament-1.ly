\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Aidan Crossey"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1419#setting1419"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1419#setting1419" {"https://thesession.org/tunes/1419#setting1419"}}}
	title = "Session-Widow's Lament, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \bar "|"   d'8  \bar "|"   g'8    fis'8    g'8    a'4 
   g'8  \bar "|"   fis'4    g'8    a'4    c''8  \bar "|"   b'8    a'8    g'8    
a'4    fis'8  \bar "|"   d'8    g'8    fis'8    g'4    d'8  \bar "|"     g'8    
fis'8    g'8    a'4    g'8  \bar "|"   fis'4    g'8    a'4    c''8  \bar "|"   
b'8    a'8    g'8    a'4    fis'8  \bar "|"   d'8    g'8    fis'8    g'4    b'8 
 \bar "|"     d''8    c''8    d''8    e''4    d''8  \bar "|"   c''4    b'8    
a'8    b'8    c''8  \bar "|"   d''8    c''8    b'8    c''8    b'8    a'8  
\bar "|"   b'8    a'8    g'8    d'8    e'8    fis'8  \bar "|"     g'8    fis'8  
  g'8    a'4    g'8  \bar "|"   fis'4    g'8    a'4    c''8  \bar "|"   b'8    
a'8    g'8    a'4    fis'8  \bar "|"   d'4    g'8    g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
