\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "The Archivist"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/59#setting28218"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/59#setting28218" {"https://thesession.org/tunes/59#setting28218"}}}
	title = "Lady Anne Montgomery"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   g'8  \bar "|"   fis'8    a'8    d'8    a'8    fis'8   
 a'8    d'8    a'8  \bar "|"   fis'8    a'8    d''8    a'8    b'8    a'8    b'8 
   d''8  \bar "|"   fis'8    a'8    d'8    fis'8    e'8    d'8    e'8    fis'8  
\bar "|"   d'8    b8    a8    b8    d'8    d'8    d'8    d'8  \bar "||"     
e''8  \bar "|"   fis''8    e''8    fis''8    g''8    fis''8    e''8    d''8    
e''8  \bar "|"   fis''8    e''8    d''8    b'8    a'8    b'8    d''8    e''8  
\bar "|"   fis''8    e''8    fis''8    g''8    fis''8    e''8    d''8    e''8  
\bar "|"   fis''8    g''8    a''8    fis''8    d''8    d''8    d''8    d''8    
e''8  \bar "|"     fis''8    e''8    fis''8    g''8    fis''8    e''8    d''8   
 e''8  \bar "|"   fis''8    e''8    d''8    b'8    a'8    b'8    d''8    e''8  
\bar "|"   fis''8    a''8    e''8    fis''8    d''8    b'8    a'8    g''8  
\bar "|"   fis''8    a''8    fis''8    e''8    d''8    d''8    d''8    d''8  
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
