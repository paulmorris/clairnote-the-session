\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/748#setting13841"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/748#setting13841" {"https://thesession.org/tunes/748#setting13841"}}}
	title = "Flax In Bloom, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   e''8    fis''8    a''8    fis''8    d''8    fis''8    
a''8    fis''8  \bar "|"   g''8    a''8    b''8    g''8    e''8    cis''8    
a'8    b''8  \bar "|"   a''8    fis''8    fis''8    a''8    d''8    e''8    
d''8    b'8  \bar "|"   a'4    g''8    e''8    fis''8    d''8    d''4 ^"~"  
\bar "|"   fis''8    a''8    a''8    fis''8    d''8    fis''8    a''8    fis''8 
 \bar "|"   g''8    a''8    b''8    g''8    e''8    fis''8    d''8    b'8  
\bar "|"   a'8    fis'8    fis'8    g'8    a'4    d'''8    b''8  \bar "|"   
a''8    fis''8    e''8    a''8    fis''8    d''8    d''4 ^"~"  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
