\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/411#setting23300"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/411#setting23300" {"https://thesession.org/tunes/411#setting23300"}}}
	title = "Heather Breeze, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {     d'8 ^"G drone"   g'8    b'8    
g'8    d''8    g'8    b'8    g'8    \bar "|"   d'8 ^"F\# drone"   fis'8    
\tuplet 3/2 {   fis'8    fis'8    fis'8  }   d''8    fis'8    a'8    fis'8    
\bar "|"   d'8 ^"G drone"   g'8    b'8    g'8    d''8    g'8    b'8    g'8    
} \alternative{{   b'8 ^"D drone"   d''8    c''8    a'8    b'16    c''16    
d''8    a'8    fis'8    } {     b'8 ^"D drone"   d''8    c''8    a'8    b'8    
g'8    b'8    d''8    \bar "|"     \bar "|"   g''4    a''16    g''16    fis''8  
  g''8    b''8    a''8    g''8    \bar "|"   fis''8    d''8    \tuplet 3/2 {   
d''8    d''8    d''8  }   fis''8    g''8    a''8    fis''8    \bar "|"   d''8   
 g''8    g''4 ^"~"    g''8    b''8    a''8    g''8    \bar "|"   fis''8    d''8 
   c''8    a'8    b'8    g'8    b'8    d''8    \bar "|"     \bar "|"   g''4  
\grace {    a''8  }   g''8    fis''8    g''8    b''8    a''8    g''8    
\bar "|"   fis''8    d''8    \tuplet 3/2 {   d''8    d''8    d''8  }   fis''8    
g''8    a''8    fis''8    \bar "|"   g''8    b''8    a''8    g''8    fis''8    
a''8    g''8    fis''8    \bar "|"   e''8    d''8    cis''8    a'8    b'16    
cis''16    d''8    a'8    fis'8    \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
