\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/534#setting13474"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/534#setting13474" {"https://thesession.org/tunes/534#setting13474"}}}
	title = "Glenside, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key e \dorian   \repeat volta 2 {   e'8    a'8    \bar "|"   b'8.    
cis''16    b'8    a'8    \bar "|"   fis'8    a'8    d'8    a'8    \bar "|"   
b'8.    cis''16    b'8    a'8    \bar "|"   b'8    e''8    e''8    d''8    
\bar "|"     b'4    cis''16    b'16    a'8    \bar "|"   fis'16    g'16    a'8  
  d'8.    e'16    \bar "|"   fis'8    d'8    a'8    fis'8    \bar "|"   e'4  
 ~    }     \repeat volta 2 {   e'4    \bar "|"   b'8    e''8   ~    e''8    
d''16    e''16    \bar "|"   fis''8    e''8    d''8    a'8    \bar "|"   b'8    
e''8    e''8    d''16    e''16    \bar "|"   fis''16    g''16    fis''16    
e''16    d''4    \bar "|"     b'8    e''8    e''8    d''16    e''16    \bar "|" 
  fis''8    e''8    d''8    b'8    \bar "|"   a'8    fis'8    d''16    a'16    
fis'8    \bar "|"   e'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
