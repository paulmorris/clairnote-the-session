\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Pippa"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/52#setting12489"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/52#setting12489" {"https://thesession.org/tunes/52#setting12489"}}}
	title = "Kid On The Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key e \minor   b8    e'8    e'8    e'8    d'8    e'8    g'4    e'8   
 \bar "|"   b8    e'8    e'8    e'8    d'8    e'8    d'8    b8    a8    
\bar "|"   b8    e'8    b8    e'8    d'8    e'8    g'4    a'8    \bar "|"   b'8 
   a'8    g'8    fis'8    a'8    g'8    fis'8    e'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
