\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "The Merry Highlander"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/53#setting12490"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/53#setting12490" {"https://thesession.org/tunes/53#setting12490"}}}
	title = "O'Keefe's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key e \dorian   \repeat volta 2 {   e'4    b'8    b'4    a'8    
fis'8    e'8    fis'8    a'4    b'8    \bar "|"   e'4    b'8    b'4    a'8    
fis'4    d'8    d'8    e'8    fis'8    \bar "|"     e'4    b'8    b'4    a'8    
fis'8    e'8    fis'8    a'4    b'8    \bar "|"   fis'8    e'8    fis'8    b'4  
  fis'8    fis'4    e'8    e'4    d'8    }     \repeat volta 2 {   b'4    e''8  
  e''4    fis''8    e''4    d''8    b'4    a'8    \bar "|"   b'4    e''8    
e''4    fis''8    e''4    d''8    b'4    cis''8    \bar "|"     d''4    e''8    
d''8    cis''8    b'8    a'8    b'8    a'8    fis'8    e'8    d'8    \bar "|"   
fis'8    e'8    fis'8    b'4    fis'8    fis'4    e'8    e'4    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
