\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1484#setting14871"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1484#setting14871" {"https://thesession.org/tunes/1484#setting14871"}}}
	title = "Tail Toddle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   d''16    d''16    d''8    fis''8    d''8      g''4 
^"tr"   d''8    b'8  \bar "|"   d''16    d''16    d''8    fis''8    d''8      
e''4 ^"tr"   c''8    a'8  \bar "|"   d''16    d''16    d''8    fis''8    d''8   
 g''8    e''8    fis''8    d''8  \bar "|"   e''16    fis''16    g''8    fis''16 
   g''16    a''8      e''4 ^"tr"   c''8    a'8  }   \repeat volta 2 {   g''4 
^"tr"   d''8    b'8    b'8    g''8    e''8    c''8  \bar "|"   g''4 ^"tr"   
d''8    b'8    a'8    e''8    c''8    a'8  \bar "|"   g''4 ^"tr"   d''8    b'8  
  b'8    g''8    d''8    b'8  \bar "|"   e''16    fis''16    g''8    fis''16    
g''16    a''8      e''4 ^"tr"   c''8    a'8  }   \repeat volta 2 {   d''16    
d''16    d''8    fis''8    d''8    g''8    e''8    fis''8    b'8  \bar "|"   
d''16    d''16    d''8    fis''8    b'8    e''8    a'8    c''8    a'8  \bar "|" 
  d''16    d''16    d''8    fis''8    d''8    g''8    e''8    fis''8    d''8  
\bar "|"   e''16    fis''16    g''16    e''16    fis''16    g''16    a''16    
fis''16      e''4 ^"tr"   c''8    a'8  }   \repeat volta 2 {   g''4    b'8    
g''8    b'8    g''8    d''8    b'8  \bar "|"   g''4    b'8    g''8    b'8    
g''8    c''8    a'8  \bar "|"   g''4    b'8    g''8    a''16    g''16    
fis''16    e''16    fis''8    d''8  \bar "|"   e''16    fis''16    g''16    
e''16    fis''16    g''16    a''16    fis''16      e''4 ^"tr"   e''8    a'8  }  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
