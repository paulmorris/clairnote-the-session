\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/277#setting277"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/277#setting277" {"https://thesession.org/tunes/277#setting277"}}}
	title = "Britches, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   d'8  \repeat volta 2 {   g'8    a'8    b'8    g'8    
a'8    g'8    b'8    a'8  \bar "|"   g'8    a'8    b'8    g'8    a'8    g'8    
e'8    d'8  \bar "|"   g'8    a'8    b'8    c''8    d''8    e''8    d''8    b'8 
 \bar "|"   g'8    a'8    g'8    e'8    e'8    d'8    d'4  }     \bar "|"   
d''8    e''8    d''8    b'8    a'8    g'8    a'8    b'8  \bar "|"   d''8    
e''8    d''8    b'8    a'8    g'8    e'4  \bar "|"   d''8    e''8    d''8    
b'8    a'8    g'8    a'8    b'8  \bar "|"   g'8    a'8    g'8    e'8    e'8    
d'8    d'4  \bar "|"     \bar "|"   d''8    e''8    d''8    b'8    a'8    g'8   
 a'8    b'8  \bar "|"   d''8    e''8    d''8    b'8    a'8    g'8    e'4  
\bar "|"   d''8    e''8    g''8    e''8    d''8    e''8    b'8    a'8  \bar "|" 
  g'8    a'8    g'8    e'8    e'8    d'8    d'4  \bar "|"     \repeat volta 2 { 
  g''8    a''8    b''8    g''8    a''8    g''8    b''8    a''8  \bar "|"   g''8 
   a''8    b''8    g''8    a''8    g''8    e''4  \bar "|"   g''8    b''8    
e''8    g''8    d''8    e''8    b'8    a'8  \bar "|"   g'8    a'8    g'8    e'8 
   e'8    d'8    d'4  }     \bar "|"   e''8    d''8    b'8    d''8    e''8    
d''8    b'8    d''8  \bar "|"   e''8    d''8    b'8    g'8    a'8    g'8    e'4 
 \bar "|"   e''8    d''8    b'8    d''8    e''8    d''8    b'8    a'8  \bar "|" 
  g'8    a'8    g'8    e'8    e'8    d'8    d'4  \bar "|"     \bar "|"   e''8   
 d''8    b'8    d''8    e''8    d''8    b'8    d''8  \bar "|"   e''8    d''8    
b'8    g'8    a'8    g'8    e'4  \bar "|"   g''8    b''8    e''8    g''8    
d''8    e''8    b'8    a'8  \bar "|"   g'8    a'8    g'8    e'8    e'8    d'8   
 d'4  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
