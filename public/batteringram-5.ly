\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "DerryMusicMan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/382#setting13209"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/382#setting13209" {"https://thesession.org/tunes/382#setting13209"}}}
	title = "Battering Ram, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'4    g'8    a'8    g'8    a'8    \bar "|"   b'8    
d''8    d''8    b'8    a'8    g'8    \bar "|"   a'8    g'8    a'8    b'8    a'8 
   b'8    \bar "|"   g'8    e'8    d'8    g'8    a'8    b'8      
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
