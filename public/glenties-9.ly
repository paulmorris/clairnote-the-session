\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1332#setting14676"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1332#setting14676" {"https://thesession.org/tunes/1332#setting14676"}}}
	title = "Glenties, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   \repeat volta 2 {   g'8    a'8    \bar "|"   b'4    
b'8    a'8    g'8    e'8    \bar "|"   d'8    b8    d'4    b8    d'8    
\bar "|"   e'4    e'4    c'8    e'8    \bar "|"   d'8    b8    d'4    g'8    
a'8    \bar "|"     b'4    b'8    a'8    g'8    e'8    \bar "|"   d'8    b8    
d'4    b8    d'8    \bar "|"   e'4    fis'4    d'4    \bar "|"   g'4    g'4    
}     \repeat volta 2 {   g'8    a'8    \bar "|"   b'4    b'4    d''8    b'8    
\bar "|"   c''8    b'8    c''4    d'8    fis'8    \bar "|"   a'4    a'4    d''8 
   c''8    \bar "|"   b'8    a'8    b'4    g'8    a'8    \bar "|"     b'4    
b'4    d''8    b'8    \bar "|"   c''8    b'8    c''4    d'8    fis'8    
\bar "|"   a'4    a'8    c''8    b'8    a'8    \bar "|"   g'4    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
