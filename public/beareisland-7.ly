\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "vanMeerdervoort"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/696#setting29168"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/696#setting29168" {"https://thesession.org/tunes/696#setting29168"}}}
	title = "Beare Island"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   e'8    b'8    gis'8    b'8    
a'8    gis'8    e'8    cis''8  \bar "|"   d''8    b'8    cis''8    a'8    
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    a'8  \bar "|"   d''4    
fis''8    d''8    a'8    d'8    fis'8    a'8  \bar "|"     \bar "|" 
\tuplet 3/2 {   g'8    fis'8    e'8  }   fis'8    d'8    e'8    d'8    b8    a8  
\bar "|"   b8    e'8    e'8    d'8    e'8    fis'8    g'8    a'8  \bar "|"   
b'4    b'8    a'8    \tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8  
\bar "|"     \bar "|"   g''8    fis''8    e''8    d''8    b'8    d''8    a'8    
g'8  \bar "|"   fis'8    a'8    d'8    fis'8    e'2    }     \repeat volta 2 {  
 b'8    e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   g''8    fis''8    e''8 
   d''8  \bar "|" \tuplet 3/2 {   b'8    cis''8    d''8  }   a'8    g'8    fis'8 
   a'8    d'8    fis'8  \bar "|"   b'8    e''8    \tuplet 3/2 {   e''8    e''8   
 e''8  }   g''8    fis''8    e''8    d''8  \bar "|"     b'8    e''8    dis''8   
 fis''8    e''4    e''8    fis''8  \bar "|"   e''8    d''8    b'8    cis''8    
d''8    b'8    \bar "|"   cis''8    d''8    b'8    cis''8    d''8    b'8    a'8 
   g'8    fis'8    a'8  \bar "|"     g'8    r8 \tuplet 3/2 {   g'8    g'8    g'8 
 }   a'8    g'8    fis'8    a'8  \bar "|"   g'8    e'8    fis'8    d'8    e'2  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
