\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "turophile"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/482#setting15800"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/482#setting15800" {"https://thesession.org/tunes/482#setting15800"}}}
	title = "Foxhunter's, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   fis'4.    fis'8.    d'8.    g'8.    e'8.  \bar "|"   
fis'4.    fis'8.    d'8.    e'8.    d'8.  \bar "|"   fis'4. ^"~"    fis'4.    
g'8.    b'8.  \bar "|"   a'8.    fis'8.    d'8.    b'4.    b'8.    a'8.    g'8. 
   a'8.  \bar "|"   b'8.    e'8.    e'8.    d'8.    g'8.    a'8.  \bar "|"   
b'4.    b'8.    a'8.    g'8.    b'8.  \bar "|"   a'8.    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
