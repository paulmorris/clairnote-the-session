\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1280#setting1280"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1280#setting1280" {"https://thesession.org/tunes/1280#setting1280"}}}
	title = "Quay Pit, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   fis'8    b'8    b'4 ^"~"    fis'8    b'8    dis'8    
b'8  \bar "|"   cis''8    a'8    fis'8    a'8    e'8    a'8    \tuplet 3/2 {   
cis''8    b'8    a'8  } \bar "|"   fis'8    b'8    b'4 ^"~"    g'8    b'8    
b'4 ^"~"  \bar "|"   cis''8    d''!8    e''8    cis''8    d''8    b'8    cis''8 
   a'8  \bar "|"     fis'8    b'8    b'4 ^"~"    fis''8    b'8    b'4 ^"~"  
\bar "|"   cis''8    a'8    fis'8    a'8    e'8    a'8    \tuplet 3/2 {   cis''8 
   b'8    a'8  } \bar "|"   d''8    a'8    a'4 ^"~"    e''8    a'8    fis''8    
a'8  \bar "|"   fis'8    b'8    b'8    a'8    b'4.    a'8  \bar "|"     fis'8   
 b'8    b'4 ^"~"    fis''8    b'8    dis''8    fis''8  \bar "|"   cis''8    a'8 
   fis'8    a'8    e'8    fis'8    a'8    cis''8  \bar "|"   b'4. ^"~"    a'8   
 fis'8    d''!8    d''4 ^"~"  \bar "|"   cis''8    a'8    e''8    cis''8    
d''8    cis''8    d''8    e''8  \bar "|"     fis''8    b'8    b'4 ^"~"    a'8   
 b'8    d'8    b'8  \bar "|"   cis''8    a'8    fis'8    a'8    e'8    a'8    
\tuplet 3/2 {   cis''8    b'8    a'8  } \bar "|"   d''8    a'8    a'4 ^"~"    
e''8    a'8    fis''8    a'8  \bar "|"   fis'8    b'8    b'8    a'8    b'4.    
cis''8  \bar "||"     d''4    \tuplet 3/2 {   e''8    fis''8    g''8  }   a''8   
 b''8    b''8    a''8  \bar "|"   g''8    e''8    e''8    d''8    e''8    a'8   
 cis''8    e''8  \bar "|"   d''4    \tuplet 3/2 {   e''8    fis''8    g''8  }   
a''8    d''8    d''4 ^"~"  \bar "|"   b''8    d''8    cis'''8    d''8    g''8   
 e''8    cis''8    e''8  \bar "|"     d''4    \tuplet 3/2 {   e''8    fis''8    
g''8  }   a''8    b''8    b''8    a''8  \bar "|"   g''8    e''8    e''8    d''8 
   e''8    a'8    cis''8    a'8  \bar "|"   d''8    a'8    a'4 ^"~"    e''8    
a'8    fis''8    a'8  \bar "|"   fis'8    b'8    b'8    a'8    b'4.    cis''8  
\bar "|"     d''4    \tuplet 3/2 {   e''8    fis''8    g''8  }   a''8    b''8    
b''8    a''8  \bar "|"   g''8    e''8    e''8    d''8    e''8    a'8    cis''8  
  e''8  \bar "|"   d''4    b'8    d''8    a'8    d''8    d''4 ^"~"  \bar "|"   
b'8    d''8    cis''8    d''8    e''8    d''8    cis''8    e''8  \bar "|"     
d''4    \tuplet 3/2 {   e''8    fis''8    g''8  }   a''8    b''8    b''8    a''8 
 \bar "|"   g''8    e''8    e''8    d''8    e''8    a'8    cis''8    a'8  
\bar "|"   d''8    a'8    a'4 ^"~"    e''8    a'8    fis''8    a'8  \bar "|"   
fis'8    b'8    b'8    a'8    b'4.    fis''8  \bar "||"     \repeat volta 2 {   
g''8    e''8    e''4 ^"~"    g''8    e''8    e''4 ^"~"  \bar "|"   fis''8    
d''8    d''4 ^"~"    b'8    d''8    fis'8    d''8  \bar "|"   e'8    e''8    
e''8    b'8    d''8    cis''8    b'8    cis''8  \bar "|"   d'8    d''8    d''8  
  a'8    \tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8  \bar "|"     
g''8    e''8    e''4 ^"~"    g'8    e''8    b'8    e''8  \bar "|"   fis''8    
d''8    d''4 ^"~"    fis'8    a'8    b'8    a'8  \bar "|"   d''8    a'8    a'4 
^"~"    e''8    a'8    fis''8    a'8  \bar "|"   fis'8    b'8    b'8    a'8    
b'4.    fis''8  }     \repeat volta 2 {   g''8    a''8    b''8    g''8    a''8  
  g''8    fis''8    d''8  \bar "|"   cis''8    fis'8    fis'4 ^"~"    fis''8    
fis'8    cis''8    fis'8  \bar "|"   fis'4 ^"~"    fis''8    fis'8    e''8    
b'8    e'8    cis''8  \bar "|"   d'8    d''8    d''8    a'8    \tuplet 3/2 {   
b'8    cis''8    d''8  }   e''8    fis''8  \bar "|"     g''8    a''8    b''8    
g''8    a''8    g''8    fis''8    d''8  \bar "|"   cis''8    fis'8    fis'4 
^"~"    fis''8    a'8    b'8    a'8  \bar "|"   d''8    a'8    a'4 ^"~"    e''8 
   a'8    fis''8    a'8  } \alternative{{   fis'8    b'8    b'8    a'8    b'4.  
  fis''8  } {   fis'8    b'8    b'8    a'8    b'4.    a'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
