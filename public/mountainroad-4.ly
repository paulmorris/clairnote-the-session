\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Earl Adams"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/68#setting12524"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/68#setting12524" {"https://thesession.org/tunes/68#setting12524"}}}
	title = "Mountain Road, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   fis'4 ^\upbow(   a'8  -)   fis'8 ^\downbow(   b'8    
fis'8  -)   a'8    fis'8 ^\downbow   \bar "|"   \tuplet 3/2 {   fis'8    fis'8   
 fis'8 ^\upbow( }   a'8  -)   fis'8 ^\downbow(   e'8    fis'8  -)   d'8    e'8  
  \bar "|"   fis'4 ^\upbow(   a'8  -)   fis'8 ^\downbow(   b'8    fis'8  -)   
a'8    fis'8 ^\downbow(   \bar "|"   g'4  -)   fis'8 ^\upbow   g'8 ^\downbow(   
e'8    d'8  -)   b8    d'8    \bar "|"   fis'4 ^\upbow(   a'8  -)   fis'8 
^\downbow(   b'8    fis'8  -)   a'8    fis'8 ^\downbow   \bar "|"   
\tuplet 3/2 {   fis'8    fis'8    fis'8 ^\upbow( }   a'8  -)   fis'8 ^\downbow(  
 e'8    fis'8  -)   d'8    e'8    \bar "|"   fis'8 ^\upbow   a'8 ^\downbow   
\tuplet 3/2 {   a'8 ^\upbow   a'8    a'8  }   b'8 ^\downbow(   a'8  -)   fis'8   
 b'8    \bar "|"   a'8 ^\upbow(   b'8    d''8  -)   e''8    fis''8    d''8    
d''4    \bar "||"   d''8 ^\upbow(   cis''8    d''8  -)   b'8 ^\downbow(   a'8   
 d'8  -)   fis'8    a'8    \bar "|"   d''8 ^\upbow(   cis''8    d''8  -)   e''8 
^\downbow(   fis''8    g''8  -)   fis''8    e''8    \bar "|"   d''8 ^\upbow(   
cis''8    d''8  -)   b'8 ^\downbow(   a'8    d'8  -)   fis'8    a'8    \bar "|" 
  g'4 ^\upbow   fis'8    g'8    e'8    d'8    b8    d'8    \bar "|"   d''8 
^\upbow(   cis''8    d''8  -)   b'8 ^\downbow(   a'8    d'8  -)   fis'8    a'8  
  \bar "|"   d''8 ^\upbow(   cis''8    d''8  -)   e''8 ^\downbow(   fis''8    
g''8  -)   fis''8    e''8    \bar "|"   d''8    e''8    fis''8    d''8    a'8   
 fis'8    d'8    fis'8 ^\downbow(   \bar "|"   g'4  -)   fis'8 ^\upbow   g'8 
^\downbow(   e'8    d'8  -)   b8    d'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
