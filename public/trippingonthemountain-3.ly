\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fynnjamin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/564#setting13537"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/564#setting13537" {"https://thesession.org/tunes/564#setting13537"}}}
	title = "Tripping On The Mountain"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   \repeat volta 2 {   b'16    cis''16  \bar "|"   d''8  
  d'8    fis'8    d'8    d''8    d'8 (   d'8  -)   b'16    cis''16  \bar "|"   
d''8    d'8    fis'8    d'8    b'8    e'8 (   e'8  -)   b'16    cis''16  
\bar "|"   d''8    d'8    fis'8    d'8    d''8    g''8    fis''8    e''8  
\bar "|"   d''16 (   cis''16    b'16    a'16  -)   b'8    cis''8    d''8    d'8 
   d'8  }   \repeat volta 2 {   fis''16    g''16  \bar "|"   a''8    fis''8    
g''8    e''8    fis''8    d''8 (   d''8  -)   fis''16    g''16  \bar "|"   a''8 
   fis''8    g''8    fis''8    e''8    e'8 (   e'8  -)   fis''16    g''16  
\bar "|"   a''8    fis''8    g''8    e''8    fis''8    d''8    e''8    cis''8  
\bar "|"   d''16 (   cis''16    b'16    a'16  -)   b'8    cis''8    d''8    d'8 
   d'8 ^"~"  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
