\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/217#setting12897"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/217#setting12897" {"https://thesession.org/tunes/217#setting12897"}}}
	title = "Orphan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \minor   d'8  \bar "|"   g'4. ^"~"    g'8    f'8    d'8  
\bar "|"   bes'8    a'8    bes'8    c''8    bes'8    c''8  \bar "|"   d''4    
d''8    c''8    d''8    c''8  \bar "|"   bes'8    a'8    bes'8    g'8    f'8    
d'8  \bar "|"   c'4    g'8    g'8    f'8    d'8  \bar "|"   bes'4. ^"~"    c''8 
   bes'8    c''8  \bar "|"   d''8    c''8    d''8    c''8    d''8    c''8  
\bar "|"   bes'8    g'8    f'8    g'4    d'8  \bar ":|."   bes'8    g'8    f'8  
  g'4.  \bar "||"   g''4. ^"~"    f''8    d''8    c''8  \bar "|"   bes'8    
c''8    d''8    f''8    d''8    f''8    g''8    f''8    d''8    c''8    bes'8   
 c''8  \bar "|"   d''4. ^"~"    c''8    bes'8    a'8  \bar "|"   bes'4    a'8   
 g'8    f'8    d'8  \bar "|"   bes'4. ^"~"    c''8    bes'8    c''8  \bar "|"   
d''8    ees''8    d''8    c''8    d''8    c''8  \bar "|"   bes'8    g'8    f'8  
  g'4    f''8  \bar ":|."   bes'8    g'8    f'8    g'4    d'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
