\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "MrSporre"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/357#setting6931"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/357#setting6931" {"https://thesession.org/tunes/357#setting6931"}}}
	title = "Denis Murphy's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   fis''16    g''16    fis''16    e''16    d''16    
e''16    d''16    cis''16  \bar "|"   a'8    d'8    fis'8    a'8  \bar "|"   
g'8    e'8    e''8.    d''16  \bar "|"   cis''8    b'8    b'8    a'8  \bar "|"  
   fis''16    g''16    fis''16    e''16    d''16    e''16    d''16    cis''16  
\bar "|"   a'8    fis'8    d'8    fis'8  \bar "|"   g'8    e'8    e''8.    
fis''16  \bar "|"   e''8    d''8    d''8.    e''16  \bar ":|."   e''8    d''8   
 d''8.    b'16  \bar "||"     \bar ".|:"   a'8    fis''8    fis''16    e''16    
fis''8  \bar "|"   a'8    g''8    g''16    fis''16    g''8  \bar "|"   a'8    
fis''8    fis''16    e''16    fis''8  \bar "|"   e''16    fis''16    e''16    
d''16    b'8    a'8  \bar "|"     a'8    fis''8    fis''16    e''16    fis''8  
\bar "|"   a'8    g''8    g''16    fis''16    g''16    a''16  \bar "|"   b''8   
 a''8    g''8.    cis''16  \bar "|"   e''8    d''8    d''8.    b'16  \bar ":|." 
  e''8    d''8    d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
