\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/241#setting241"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/241#setting241" {"https://thesession.org/tunes/241#setting241"}}}
	title = "Old Gray Goose, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \minor   e'16 (   fis'16  -) \bar "|"   g'8    b'8    g'8    
fis'8    a'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4    fis'8  \bar "|"   
d'8    fis'8    a'8    d''8    a'8    g'8  \bar "|"   fis'8    d'8    d'8    
d'8    e'8    fis'8  \bar "|"     g'8 ^\tenuto   fis'8    g'8    a'8    g'8    
a'8  \bar "|"   b'8    a'8    b'8    g''8    fis''8    g''8  \bar "|"   e''8    
d''8    b'8    \grace {    c''8 ( }   b'8  -)   a'8    fis'8    g'8    e'8    
e'8    e'4  }     \repeat volta 2 {   fis'8  \bar "|"   g'4    g'8    d''8    
b'8    g'8  \bar "|"   g'8    a'8    b'16 (   c''16  -)   d''8    b'8    g'8  
\bar "|"   a'4    a'8    e''8    c''8    a'8  \bar "|"   a'8    b'8 (   c''16   
 d''16  -)   e''8    c''8    a'8  \bar "|"     g'8    a'8    g'8    d''8    b'8 
   g'8  \bar "|"   e''8    g''8    fis''8    \grace {    a''8 ( }   g''8  -)   
fis''8    g''8  \bar "|"   e''8    d''8    b'8    b'8 ^\tenuto   a'8    fis'8  
\bar "|"   g'8    e'8    e'8    e'4  }     \repeat volta 2 {   d''8  \bar "|"   
e''8    fis''8    e''8    e''8    d''8    b'8  \bar "|"   d''8    e''8    
fis''8    g''4    e''8  \bar "|"   fis''8    d''8    d''8    a''8    d''8    
d''8  \bar "|"   fis''8    a''8    g''8    fis''8    e''8    d''8  \bar "|"     
g''8    fis''8    g''8    fis''8    e''8    fis''8  \bar "|"   d''8    e''8    
fis''8    \grace {    a''8 ( }   g''8  -)   fis''8    g''8  \bar "|"   e''8    
d''8    b'8    b'8 ^\tenuto   a'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4 
 }     \repeat volta 2 {   c''8  \bar "|"   b'8    g'8    g'8    a'8    fis'8   
 fis'8  \bar "|"   g'8    e'8    e'8    e'4    c''8  \bar "|"   b'8    g'8    
g'8    d''8    a'8    g'8  \bar "|"   fis'8    d'8    d'8    d'4    c''8  
\bar "|"     b'8    g'8    g'8    a'8    g'8    fis'8  \bar "|"   g'8    e'8    
e'8    \grace {    a''8 ( }   g''8  -)   fis''8    g''8  \bar "|"   e''8    
d''8    b'8    b'8 ^\tenuto   a'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4 
 }     \repeat volta 2 {   fis''8  \bar "|"   g''8    b''8    g''8    fis''8    
a''8    fis''8  \bar "|"   g''8    e''8    e''8    e''4    fis''8  \bar "|"   
g''8    b''8    g''8    fis''8    a''8    g''8  \bar "|"   fis''8    d''8    
d''8    d''8    e''8    fis''8  \bar "|"     g''8    b''8    g''8    fis''8    
a''8    fis''8  \bar "|"   e''8    g''8    fis''8    \grace {    a''8 ( }   
g''8  -)   fis''8    g''8  \bar "|"   e''8    d''8    b'8    b'8 ^\tenuto   a'8 
   fis'8  \bar "|"   g'8    e'8    e'8    e'4  }     \repeat volta 2 {   fis''8 
 \bar "|"   g''8    d''8    c''8    b'8    c''8    a'8  \bar "|"   g'8    e'8   
 e'8    e'4    fis''8  \bar "|"   g''8    d''8    b'8    d''8    a'8    g'8  
\bar "|"   fis'8    d'8    d'8    d'8    e'8    fis'8  \bar "|"     g'4.    
\grace {    b'8 ( }   a'8  -)   g'8    a'8  \bar "|"   b'8    c''8    b''8    
\grace {    a''8 ( }   g''8  -)   fis''8    g''8  \bar "|"   e''8    d''8    
b'8    b'8 ^\tenuto   a'8    fis'8  \bar "|"   e'8    g'8    g'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
