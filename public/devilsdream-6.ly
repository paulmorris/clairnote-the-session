\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/259#setting24295"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/259#setting24295" {"https://thesession.org/tunes/259#setting24295"}}}
	title = "Devil's Dream, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   e''4    \bar "|"   a''4    e''8    gis''8    a''4    
e''8    gis''8    \bar "|"   a''4    e''8    a''8    fis''8    e''8    d''8    
cis''8    \bar "|"   d''8    fis''8    b'8    fis''8    d''8    fis''8    b'8   
 fis''8    \bar "|"   d''8    fis''8    b''8    a''8    gis''8    e''8    
fis''8    gis''8    \bar "|"   a''4    e''8    gis''8    a''4    e''8    gis''8 
   \bar "|"     a''4    e''8    a''8    fis''8    e''8    d''8    cis''8    
\bar "|"   d''8    e''8    fis''8    e''8    d''8    cis''8    b'8    a'8    
\bar "|"   e'4    gis'4    a'4    \bar "||"   e''8    d''8    \bar "|"   cis''8 
   e''8    a'8    e''8    cis''8    e''8    a'8    e''8    \bar "|"   cis''8    
e''8    a'8    a''8    fis''8    e''8    d''8    cis''8    \bar "|"     d''8    
fis''8    b'8    fis''8    d''8    fis''8    b'8    fis''8    \bar "|"   d''8   
 fis''8    b'8    b''8    gis''8    fis''8    e''8    d''8    \bar "|"   cis''8 
   e''8    a'8    e''8    cis''8    e''8    a'8    e''8    \bar "|"   cis''8    
e''8    a'8    a''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''8    
e''8    fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   e'4    
gis'4    a'4    \bar "||"     e''8    gis''8    \bar "|"   a''4    e''8    
gis''8    a''4    e''8    gis''8    \bar "|"   gis''8    a''8    e''8    a''8   
 d'''8    cis'''8    b''8    a''8    \bar "|"   gis''!8    b''8    fis''8    
b''8    a''8    gis''8    fis''8    b''8    \bar "|"   ais''8    b''8    fis''8 
   b''8    d'''8    cis'''8    b''8    ais''8    \bar "|"   e''8    a''8    
gis''8    a''8    cis''8    a''8    gis''8    a''8    \bar "|"     a'8    a''8  
  gis''8    a''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''8    e''8 
   fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   e'4    gis'4    
a'4    \bar "||"   e''8    d''8    \bar "|"   cis''8    e''8    a'8    e''8    
cis''8    e''8    a'8    e''8    \bar "|"   a''8    gis''8    a''8    gis''8    
fis''8    e''8    d''8    cis''8    \bar "|"     d''8    fis''8    b'8    
fis''8    d''8    fis''8    b'8    fis''8    \bar "|"   b''8    ais''8    b''8  
  a''!8    gis''8    fis''8    e''8    d''8    \bar "|"   cis''8    e''8    a'8 
   e''8    cis''8    e''8    a'8    gis''8    \bar "|"   a''8    gis''8    a''8 
   gis''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''8    e''8    
fis''8    e''8    d''8    cis''8    b'8    a'8    \bar "|"   e'4    gis'4    
a'4    \bar "||"     e'4  \bar "|"   \tuplet 3/2 {   a'8    cis''8    e''8  }   
\tuplet 3/2 {   e''8    cis''8    a'8  }   \tuplet 3/2 {   a'8    cis''8    e''8  
}   \tuplet 3/2 {   e''8    cis''8    a'8  }   \bar "|"   \tuplet 3/2 {   a'8    
cis''8    e''8  }   \tuplet 3/2 {   e''8    cis''8    a'8  }   \tuplet 3/2 {   
a'8    cis''8    e''8  }   \tuplet 3/2 {   e''8    cis''8    a'8  }   \bar "|"   
\tuplet 3/2 {   gis'8    b'8    e''8  }   \tuplet 3/2 {   e''8    b'8    gis'8  } 
  \tuplet 3/2 {   gis'8    b'8    e''8  }   \tuplet 3/2 {   e''8    b'8    gis'8  
}   \bar "|"     \tuplet 3/2 {   gis'8    b'8    e''8  }   \tuplet 3/2 {   e''8   
 b'8    gis'8  }   \tuplet 3/2 {   gis'8    b'8    e''8  }   \tuplet 3/2 {   e''8 
   b'8    gis'8  }   \bar "|"   a'8    cis''8    e'8    cis''8    a'8    cis''8 
   e''8    cis''8    \bar "|"   a'8    cis''8    e''8    cis''8    fis''8    
e''8    d''8    cis''8    \bar "|"     fis''8    gis''8    a''8    fis''8    
e''8    cis''8    b'8    a'8    \bar "|"   e'4    gis'4    a'4    \bar "||" 
\tuplet 3/2 {   e''8    fis''8    gis''8  }   \bar "|"     a''8 ^"Tempo agitato" 
  e''8    cis''8    e''8    fis''8    e''8    cis''8    e''8    \bar "|"   a''8 
   e''8    cis''8    e''8    fis''8    e''8    cis''8    e''8    \bar "|"   
b''8    fis''8    d''8    fis''8    gis''8    fis''8    d''8    fis''8    
\bar "|"     b''8    fis''8    d''8    fis''8    gis''8    fis''8    d''8    
fis''8    \bar "|"   a''8    e''8    cis''8    e''8    fis''8    e''8    cis''8 
   e''8    \bar "|"   a''8    gis''8    a''8    gis''8    fis''8    e''8    
d''8    cis''8    \bar "|"   d''8    e''8    fis''8    gis''8    a''8    fis''8 
   e''8    d''8    \bar "|" <<   cis''4    e'4   >> <<   b'4    d'4   >> <<   
a'4    cis'4   >>   \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
