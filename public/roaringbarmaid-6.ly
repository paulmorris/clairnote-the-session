\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bea Kelly"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/91#setting28867"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/91#setting28867" {"https://thesession.org/tunes/91#setting28867"}}}
	title = "Roaring Barmaid, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   g'4    g'8    e'8    g'8    d'8    \bar "|"   g'8    
fis'8    g'8    e'8    g'8    d'8    \bar "|"   g'8    d'8    d'8    b'8    d'8 
   d'8    \bar "|"   d''8    d'8    d'8    b'4    a'8    \bar "|"   g'4    g'8  
  e'8    g'8    d'8  \bar "|"   g'8    fis'8    g'8    e'8    g'8    d'8  
\bar "|"   g'8    a'8    b'8    d''8    e''8    g''8  \bar "|"   d''8    e''8   
 g''8    e''8    d''8    b'8  }     \repeat volta 2 {   d'8    e'8    g'8    
b'4    b'8  \bar "|"   b'8    a'8    b'8    d'8    e'8    g'8  \bar "|"   b'4   
 b'8    b'8    a'8    g'8  \bar "|"   a'8    g'8    e'8    d'8    e'8    g'8  
\bar "|"   b'4    b'8    b'8    a'8    b'8  \bar "|"   a'8    g'8    g'8    b'8 
   g'8    g'8  \bar "|"   a'8    g'8    g'8    e'8    fis'8    g'8  
} \alternative{{   e'8    fis'8    g'8    e'8    d'8    b8  } {   g4. (   g4.  
-) \bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
