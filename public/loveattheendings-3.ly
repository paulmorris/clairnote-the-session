\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "malcombpiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/990#setting14195"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/990#setting14195" {"https://thesession.org/tunes/990#setting14195"}}}
	title = "Love At The Endings"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \repeat volta 2 {   cis''8  \bar "|"   a'8    
b'8    cis''8    e''8    fis''4    fis''8    a''8  \bar "|"   e''8    cis''8    
cis''16    cis''16    cis''8    e''8    cis''8    b'8    cis''8  \bar "|"   a'8 
   b'8    cis''8    e''8    fis''4    e''8    cis''8  \bar "|"   b'16    b'16   
 b'8    cis''8    a'8    b'4. ^"~"    cis''8  \bar "|"   a'8    b'8    cis''8   
 e''8    fis''4    a''8    fis''8  \bar "|"   e''8    cis''8    cis''16    
cis''16    cis''8    e''4. ^"~"    fis''8  \bar "|"   a''4. ^"~"    fis''8    
e''8    fis''8    a''8    fis''8  \bar "|"   e''8    cis''8    b'8    cis''8    
a'4.  }   \repeat volta 2 {   b'8  \bar "|"   cis''4. ^"~"    b'8    a'8    b'8 
   cis''8    e''8  \bar "|"   e''8    cis''8    fis''8    cis''8    e''8    
cis''8    a'8    b'8  \bar "|"   cis''4. ^"~"    b'8    a'8    fis''8    fis''8 
   cis''8  \bar "|"   e''8    cis''8    a'8    cis''8    b'4    a'8    b'8  
\bar "|"   cis''4. ^"~"    b'8    a'8    fis''8    fis''8    cis''8  \bar "|"   
e''8    cis''8    cis''16    cis''16    cis''8    b'4    a'8    b'8  \bar "|"   
cis''8    e''8    fis''8    g''8    a''4. ^"~"    fis''8  \bar "|"   e''8    
cis''8    b'8    cis''8    a'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
