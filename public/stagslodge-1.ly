\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "shanachie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/903#setting903"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/903#setting903" {"https://thesession.org/tunes/903#setting903"}}}
	title = "Stag's Lodge"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key e \minor     e''8 ^"Em"   b'16    b'16    g'8    b'8  \bar "|"   
fis'8    b'8    e'8    b'8  \bar "|"   c''8. ^"Am"   b'16    a'8    g'8  
\bar "|"   fis'16 ^"D"   e'16    d'8    e'8    fis'8  \bar "|"       e''8 ^"Em" 
  b'16    b'16    g'8    b'8  \bar "|"   fis'8    b'8    e'8    b'8  \bar "|"   
c''16 ^"C"   b'16    a'8      d''8 ^"D"   c''8  \bar "|"   b'8 ^"G"   g'8    
g'4  \bar ":|."   b'8 ^"Em"   e''8    e''4  \bar "||"       b'8. ^"Em"   e''16  
  e''8    g''8  \bar "|"   e''16    fis''16    g''8    fis''8    d''8  \bar "|" 
  c''8 ^"C"   g'8    g'8    c''8  \bar "|"   d''8 ^"D"   a'8    a'8    c''8  
\bar "|"       b'8. ^"Em"   e''16    e''8    g''8  \bar "|"   e''16    fis''16  
  g''8    fis''8    d''8  \bar "|"   c''16 ^"Am"   d''16    e''8    g''8    
e''8  \bar "|"   d''16 ^"Bm"   e''16    fis''8    a''8    fis''8  \bar "|"      
 b''8. ^"Em"   b'16    b'8    g''8  \bar "|"   e''16    fis''16    g''8    
fis''8    d''8  \bar "|"   c''8 ^"C"   g'8    g'8    c''8  \bar "|"   d''8 ^"D" 
  a'8    a'8    c''8  \bar "|"       b'8. ^"Em"   e''16    e''8    g''8  
\bar "|"   e''16    fis''16    g''8    fis''8    d''8  \bar "|"   c''8 ^"Am"   
e''8    a''8    g''8  \bar "|"   fis''16 ^"Bm"   e''16    d''8    e''8    
fis''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
