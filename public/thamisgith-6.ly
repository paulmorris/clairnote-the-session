\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/647#setting28129"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/647#setting28129" {"https://thesession.org/tunes/647#setting28129"}}}
	title = "Tha Mi Sgith"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   b'16    \bar "|"   a'16    a'8.  
\grace {    fis''8  }   a''4    g''16    e''8.  \grace {    fis''8  }   g''4    
\bar "|" \grace {    fis''8  }   e''8.    d''16      b'8. (^\trill   a'16  -) 
\grace {    fis'8  }   g'8.    a'16    b'8.    g'16    \bar "|"     a'16    
a'8.  \grace {    fis''8  }   a''4    g''16    e''8.  \grace {    fis''8  }   
g''4    \bar "|"   e''16    d''8.    b'8.    g''16  \grace {    cis''8  }   
b'16    a'8.    a'8.    }     \repeat volta 2 {   g''16    \bar "|"   e''8.    
d''16      b'8. (^\trill   a'16  -) \grace {    fis'8  }   g'8.    a'16    b'8. 
   g'16    \bar "|"   e''8.    d''16      b'8. (^\trill   a'16  -)   b'4 
^\trill   b'8.    g''16    \bar "|"     e''8.    d''16      b'8. (^\trill   
a'16  -) \grace {    fis'8  }   g'8.    a'16    b'8.    d''16    \bar "|"   
e''8.    d''16  \grace {    fis''8  }   g''8.    b'16  \grace {    c''8    b'8  
}   a'4    a'8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
