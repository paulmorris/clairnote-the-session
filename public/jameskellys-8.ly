\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Abram "
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1492#setting26842"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1492#setting26842" {"https://thesession.org/tunes/1492#setting26842"}}}
	title = "James Kelly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key d \major   fis'4    a'8    a'4    b'8    a'4.  \bar "|"   fis'4  
  a'8    a'4    a'8    b'8    cis''8    d''8  \bar "|"   fis'4    a'8    a'4    
b'8    a'4.  \bar "|"   e''4    fis''8    e''4    d''8    b'4    a'8  }     
fis''4    a''8    a''4    fis''8    d''4.  \bar "|"   e''4    fis''8    e''4    
d''8    b'4    a'8  \bar "|"   fis''4    a''8    a''4    fis''8    d''4.  
\bar "|"   e''16    fis''16    g''4    e''8    fis''4    d''8    b'4  \bar "|"  
   fis''4    a''8    a''4    fis''8    d''4.  \bar "|"   e''4    fis''8    e''4 
   d''8    b'4    a'8  \bar "|"   fis''4    d''8    e''4    cis''8    d''4.  
\bar "|"   b'4    c''8    b'4    a'8    fis'8    e'8    d'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
