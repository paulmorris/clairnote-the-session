\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/302#setting25202"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/302#setting25202" {"https://thesession.org/tunes/302#setting25202"}}}
	title = "Maudabawn Chapel"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'4 ^"1"   fis'8    g'8    e'8    
b8    d'8    b8  \bar "|"   g8    a8    b8    d'8    e'8    b8    d'8    b8  
\bar "|"   g8    a8    b8    d'8    g'8    a'8    b'8    d''8  \bar "|"   g''8  
  a''8    b''8    g''8    e''8    a''8    a''8    fis''8  \bar "|"     
} \alternative{{   g''8    a''8    b''8    g''8    e''8    fis''8    g''8    
e''8  \bar "|"   d''8    fis''8    e''8    d''8    b'8    c''8    d''4  
\bar "|" \tuplet 3/2 {   e''8    fis''8    g''8  }   fis''8    a''8    g''8    
b''8    e''8    c''8  \bar "|"   d''8    b'8    a'8    fis'8    g'8    e'8    
e'8    fis'8  } }      \bar "|"   g''4. ^"~"    a''8    \tuplet 3/2 {   b''8    
a''8    g''8  }   \tuplet 3/2 {   a''8    g''8    fis''8  }   \bar "|"   g''8    
fis''8    e''8    d''8    e''8    a''8    g''8    e''8  \bar "|"   d''8    
fis''8    e''8    c''!8    b'8    d''8    c''8    b'8  \bar "|"   a'8    c''!8  
  b'8    a'8    g'8    e'8    e'8    d'8  \bar "|."     \bar ".|:" \tuplet 3/2 {  
 e'8    e'8    e'8  }   b'8    e'8    e''8    e'8    b'8    e'8  \bar "|" 
\tuplet 3/2 {   e'8 ^"2"   e'8    e'8  }   b'8    e'8    a'8    d'8    fis'8    
d'8  \bar "|" \tuplet 3/2 {   e'8    e'8    e'8  }   b'8    e'8    e''8    e'8   
 b'8    e'8  \bar "|" \tuplet 3/2 {   a'8    b'8    a'8  }   fis'8    d'8    a8  
  d'8    fis'8    d'8  \bar "|"     \tuplet 3/2 {   e'8    e'8    e'8  }   b'8   
 e'8    e''8    e'8    b'8    e'8  \bar "|"   b'8    a'8    gis'8    a'8    b'8 
   c''8    d''8    e''8  \bar "|"   fis''8 ^"3"   g''8    g''8    fis''8    
g''8    fis''8    e''8    c''8  \bar "|"   d''8    b'8    a'8    fis'8    g'8   
 e'8    e'8    d'8  \bar ":|."   d''8    b'8    a'8    fis'8    g'8    e'8    
e'8    fis'8  \bar "||"     \tuplet 3/2 {   g'8 ^"1"   fis'8    e'8  }   fis'8   
 d'8    e'8    b8    d'8    b8    \bar "|"   g'8 ^"2"   a'8    b'8    g'8    
a'8    d'8    fis'8    d'8    \bar "|"   <<   d''4 ^"3"   fis''4   >> fis''8    
g''8    g''8    fis''8    e''8    c''8    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
