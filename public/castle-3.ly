\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeff Finkelstein"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/273#setting13015"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/273#setting13015" {"https://thesession.org/tunes/273#setting13015"}}}
	title = "Castle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key c \major   f''8    e''8    d''8    e''8    d''8    c''8  
\bar "|"   d''8    c''8    a'8    g'8    e'8    c'8  \bar "|"   d'8    e'8    
d'8    a'8    c''8    a'8  \bar "|"   g'8    a'8    c''8    d''8    c''8    a'8 
 \bar "|"   f''8    e''8    d''8    e''8    d''8    c''8  \bar "|"   d''8    
c''8    a'8    g'8    e'8    c'8  \bar "|"   d'8    e'8    d'8    a'8    c''8   
 a'8  \bar "|"   g'8    e'8    c'8    d'4.  \bar ":|."   g'8    e'8    c'8    
d'4    f'8  \bar "||"   f'8    e'8    d'8    d''4. ^"~"  \bar "|"   e''8    
d''8    c''8    a'8    g'8    e'8  \bar "|"   c'8    e'8    g'8    c''8    b'8  
  c''8  \bar "|"   c'8    e'8    g'8    f'8    e'8    d'8  \bar "|"   f'4. 
^"~"    g'4. ^"~"  \bar "|"   a'8    g'8    a'8    c''8    d''8    e''8  
\bar "|"   d''8    c''8    a'8    g'8    e'8    c'8  \bar "|"   a8    b8    c'8 
   d'4.  \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
