\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Caoimghgin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/649#setting649"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/649#setting649" {"https://thesession.org/tunes/649#setting649"}}}
	title = "Humours Of Lissadell, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   g''8    fis''8    \repeat volta 2 {   e''8    b'8    
b'4 ^"~"    e''8    b'8    d''8    b'8    \bar "|"   a'8    fis'8    fis'4 
^"~"    e'8    d'8    b8    a8    \bar "|"   b8    e'8    e'4 ^"~"    b8    e'8 
   g'8    e'8    \bar "|"   fis'8    b'8    b'4 ^"~"    fis'8    b'8    d''8    
fis''8    \bar "|"     \bar "|"   e''8    b'8    b'4 ^"~"    e''8    b'8    
d''8    b'8    \bar "|"   a'8    fis'8    fis'4 ^"~"    e'8    d'8    b8    a8  
  \bar "|"   b8    e'8  \grace {    g'8  }   e'8    d'8    e'8    fis'8    g'8  
  a'8    \bar "|"   \tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    d''8    
e''4    g''8    fis''8    }     \repeat volta 2 {   e''8    b'8    b'4 ^"~"    
a'4    fis'8    a'8    \bar "|"   d''4    d''8    fis''8    e''8    d''8    
e''8    fis''8    \bar "|"   d''8    fis''8    fis''4 ^"~"    d''8    fis''8    
b''8    fis''8    \bar "|"   a''8    fis''8    d''8    fis''8    e''8    d''8   
 b'8    cis''8    \bar "|"     \bar "|"   d''4    fis''8    d''8    b'8    
cis''8    d''8    b'8    \bar "|"   a'8    fis'8    fis'4 ^"~"    a'8    b'8    
d''8    e''8    \bar "|"   fis''4 ^"~"    e''8    fis''8    d''8    fis''8    
b''8    fis''8    \bar "|"   a''8    fis''8    d''8    fis''8    e''4    g''8   
 fis''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
