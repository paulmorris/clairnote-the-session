\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/726#setting28870"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/726#setting28870" {"https://thesession.org/tunes/726#setting28870"}}}
	title = "Ashplant, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   a'8    b'8    e'4    b'8    a'8    g'8    a'8    
\bar "|"   b'8    e'8    e'4 ^"~"    d'8    e'8    g'8    e'8    \bar "|"   a'8 
   b'8    b'8    a'8    b'8    d''8    e''8    d''8    \bar "|"   g''8    e''8  
  d''8    b'8    a'4    g'8    a'8    \bar "|"     a'8    b'8    e''4    b'8    
a'8    g'8    a'8    \bar "|"   b'8    d'8    e'8    g'8    d'8    e'8    g'8   
 e'8    \bar "|"   a'8    b'8    b'8    a'8    b'8    d''8    e''8    d''8    
\bar "|"   g''8    e''8    d''8    b'8    a'4    g'8    a'8    \bar "|"     b'4 
^"~"    e''8    b'8    fis''8    g''8    e''4    \bar "|"   b'4    e''8    d''8 
   b'8    a'8    g'8    a'8    \bar "|"   b'4 ^"~"    d''8    b'8    g''4. 
^"~"    a''8    \bar "|"   g''8    e''8    d''8    b'8    \tuplet 3/2 {   a'8    
bes'8    a'8  }   g'8    a'8    \bar "|"     a'8    b'8    e''8    b'8    
fis''8    g''8    e''4    \bar "|"   b'4 ^"~"    e''8    d''8    b'8    a'8    
g'8    a'8    \bar "|"   b'16    cis''16    d''8    e''8    fis''8    g''4    
a''8    g''8  \bar "|"   g''8    e''8    d''8    b'8    a'4    g'8    a'8    
\bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
