\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "aidriano"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/93#setting29566"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/93#setting29566" {"https://thesession.org/tunes/93#setting29566"}}}
	title = "Cock And The Hen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \minor   \repeat volta 2 {   g''8    e''8    c''8    c''8    
d''8    e''8    f''4.  \bar "|"   g''8    e''8    c''8    c''8    d''8    e''8  
  f''8    d''8    b'8  \bar "|"   g''8    e''8    c''8    c''8    d''8    e''8  
  f''4.  \bar "|"   e''4    c''8    e''4    a'8    c''8    a'8    g'8  }     
\repeat volta 2 {   |
 a'8    c''8    a'8    a'8    g'8    a'8    f''4.  \bar "|"   a'8    c''8    
a'8    a'8    g'8    a'8    c''8    a'8    g'8  \bar "|"   a'8    c''8    a'8   
 a'8    g'8    a'8    f''4.  \bar "|"   e''4    c''8    e''4    a'8    c''8    
a'8    g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
