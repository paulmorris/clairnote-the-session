\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/957#setting23668"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/957#setting23668" {"https://thesession.org/tunes/957#setting23668"}}}
	title = "Fanny Power"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key f \major     f'8 ^"Grazioso"   \bar "|"   f'4    c'8    f'8    
g'8    a'8    \bar "|"   bes'4    a'8    g'4    f'8    \bar "|"   e'4    d'8    
c'8    e'8    c'8    \bar "|"   e'4    f'8    g'4    bes'8    \bar "|"     a'8  
  g'8    f'8    a'8    bes'8    c''8    \bar "|"   d''4    g'8    g'4    f'8    
\bar "|"   e'4    d'8    c'8    f'8    e'8    \bar "|"   f'4.    f'4    
\bar "||"     a'8    \bar "|"   c''8    a'16    bes'16    c''8    c''8    a'16  
  bes'16    c''8    \bar "|"   f'8    a'8    f'8    f'8    a'8    f'8    
\bar "|"   d''8    bes'16    c''16    d''8    d''8    bes'16    c''16    d''8   
 \bar "|"   g'8    bes'8    g'8    g'8    bes'8    g'8    \bar "|"     a'8    
bes'8    c''8    d''8    e''8    f''8    \bar "|"   e''8    f''8    g''8    
c''8    d''8    c''8    \bar "|"   a'4    f'8    g'8    a'8    g'8    \bar "|"  
 f'4.    f'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
