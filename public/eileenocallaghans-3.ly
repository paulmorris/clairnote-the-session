\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1219#setting14520"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1219#setting14520" {"https://thesession.org/tunes/1219#setting14520"}}}
	title = "Eileen O'Callaghan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   b''8.    g''16    \tuplet 3/2 {   g''8    g''8    g''8 
 }   d''8.    b'16    \tuplet 3/2 {   b'8    b'8    b'8  }   \bar "|"   
\tuplet 3/2 {   g''8    fis''8    e''8  }   d''8.    b'16    \tuplet 3/2 {   c''8 
   b'8    a'8  }   \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
