\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1492#setting14881"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1492#setting14881" {"https://thesession.org/tunes/1492#setting14881"}}}
	title = "James Kelly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   fis'8    a'8    a'8    b'8    a'4 
   \bar "|"   fis'8    a'8    a'8    a'8    b'16    cis''16    d''8    \bar "|" 
  fis'8    a'8    a'8    b'8    a'4    \bar "|"   e''8    fis''8    e''8    
d''8    b'8    a'8    }   \repeat volta 2 {   fis''8    a''8    a''8    fis''8  
  d''4    \bar "|"   e''8    fis''8    e''8    d''8    b'8    a'8    \bar "|"   
fis''8    d''8    e''8    cis''8    d''8    a'8    \bar "|"   b'8    c''8    
b'8    a'8    fis'8    e'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
