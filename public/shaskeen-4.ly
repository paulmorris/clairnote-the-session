\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/615#setting22713"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/615#setting22713" {"https://thesession.org/tunes/615#setting22713"}}}
	title = "Shaskeen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   d'8    g'8    g'4 ^"~"    a'8    b'8  
\grace {    d''8  }   c''8    a'8  \bar "|"   d''8    fis''8    e''8    d''8    
c''8    a'8    a'4 ^"~"  \bar "|" \tuplet 3/2 {   b'8 -.   c''8 -.   b'8 -. }   
b'8    g'8    \tuplet 3/2 {   a'8    c''8 -.   a'8 -. }   a'8    fis'8  \bar "|" 
  g'8    b'8    a'8    fis'8    g'8    fis'8    e'8    fis'8  \bar "|"     d'8  
  g'8    g'4 ^"~"    a'8    b'8    c''8    a'8  \bar "|"   d''8    fis''8    
e''8    d''8    c''8    a'8    d''8    e''8  \bar "|" \tuplet 3/2 {   fis''8 -.  
 g''8 -.   a''8 -. }   e''8    g''8    fis''8    d''8    c''8    a'8  \bar "|"  
 a'8    g'8    fis'8    a'8    g'4. ^"~"    b'8  \bar "|"     \bar "|"   d'8    
g'8    g'4 ^"~"  \grace {    d''8  }   a'4 ^"~"    \tuplet 3/2 {   a'8 -.   b'8 
-.   c''8 -. } \bar "|"   d''8    r8 e''8    d''8    c''8    a'8    a'4 ^"~"  
\bar "|"   b'4. ^"~"    d''8    \tuplet 3/2 {   a'8    c''8 -.   a'8 -. }   a'8  
  fis'8  \bar "|"   g'8    b'8    a'8    fis'8    g'8    fis'8    e'8    fis'8  
\bar "|"     d'8    g'8    g'4 ^"~"    a'8    b'8    c''8    a'8  \bar "|"   
d''8    fis''8    e''8    d''8    c''8    a'8    d''8    e''8  \bar "|" 
\tuplet 3/2 {   fis''8 -.   g''8 -.   fis''8 -. }   e''8    g''8    fis''8    
d''8    c''8    a'8  \bar "|"   a'8    g'8    fis'8    a'8    g'4 ^"~"    r8 
b'8  \bar "||"     \bar "|"   d''8    g''8  \grace {    a''8  }   g''8    
fis''8  \grace {    a''8  }   g''4  \tuplet 3/2 {   e''8 -.   fis''8 -.   g''8 
-. } \bar "|"   a''8    b''8    a''8    g''8    fis''8    d''8    c''8    a'8  
\bar "|"   \tuplet 3/2 {   b'8 ^"~" -.   c''8 -.   b'8 -. }   b'8    g'8    
\tuplet 3/2 {   a'8    c''8 -.   a'8 -. }   a'8    fis'8  \bar "|"   g'8    b'8  
  a'8    fis'8    g'8    fis'8    e'8    d'8  \bar "|"     d''8    g''8  
\grace {    a''8  }   g''8    fis''8  \grace {    a''8  }   g''4  \tuplet 3/2 {  
 e''8 -.   fis''8 -.   g''8 -. } \bar "|"   a''8    b''8    a''8    g''8    
fis''8    d''8    d''4 -. \bar "|" \tuplet 3/2 {   fis''8 -.   g''8 -.   fis''8 
-. }   e''8    g''8    fis''8    d''8    c''8    a'8  \bar "|"   a'8    g'8    
fis'8    a'8    g'4    r8 b'8  \bar "|"     \bar "|"   d''8    g''8  \grace {   
 a''8  }   g''8    fis''8  \grace {    a''8  }   g''4  \tuplet 3/2 {   e''8 -.   
fis''8 -.   g''8 -. } \bar "|"   a''8    b''8    a''8    g''8    fis''8    d''8 
   c''8    a'8  \bar "|"   b'4. ^"~"    g'8    a'4. ^"~"    fis'8  \bar "|"   
g'8    b'8    a'8    fis'8    g'8    fis'8    e'8    d'8  \bar "|"     b'8    
g'8    g'4 ^"~"    a'8    b'8    c''8    a'8  \bar "|"   d''8    fis''8    e''8 
   d''8    c''8    a'8    d''8    e''8  \bar "|" \tuplet 3/2 {   fis''8 -.   
g''8 -.   fis''8 -. }   e''8    g''8    fis''8    d''8    c''8    a'8  \bar "|" 
  a'8    g'8    fis'8    a'8    g'4 ^"~"    r8 b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
