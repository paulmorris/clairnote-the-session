\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/854#setting14023"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/854#setting14023" {"https://thesession.org/tunes/854#setting14023"}}}
	title = "Sport Of The Chase, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   g'4    g'8    b'4    g'8    b'8   
 d''8    b'8    \bar "|"   g'4    g'8    b'4    g'8    b'8    d''8    b'8    
\bar "|"   d'4    d'8    fis'4    d'8    fis'8    a'8    fis'8    \bar "|"   
d'4    d'8    fis'4    d'8    fis'16    g'16    a'8    fis'8    }   g'4    g'8  
  g''4    d''8    b'8    d''8    b'8    \bar "|"   g'4    g'8    g''4    d''8   
 b'16    c''16    d''8    b'8    \bar "|"   c''4    c''8    e''4    c''8    
e''8    g''8    e''8    \bar "|"   d''4    d''8    fis''4    d''8    fis''16    
g''16    a''8    fis''8    \bar "|"   g''8    d''8    g''8    g''8    d''8    
g''8    g''8    d''8    b'8    \bar "|"   g''8    d''8    g''8    g''8    d''8  
  g''8    g''8    d''8    g'8    \bar "|"   e''8    c''8    e''8    e''8    
c''8    e''8    e''8    c''8    a'8    \bar "|"   fis''8    d''8    fis''8    
fis''8    d''16    e''16    fis''8    fis''8    g''8    a''8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
