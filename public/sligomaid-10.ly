\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/399#setting29714"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/399#setting29714" {"https://thesession.org/tunes/399#setting29714"}}}
	title = "Sligo Maid, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {     a'4 ^"Am"   b'8    a'8    b'16 
   c''16    d''8    e''8    fis''8    \bar "|"   g''8    e''8    d''8    b'8    
a'8    g'8    e'8    fis'8    \bar "|"   g'4 ^"G"   b'8    g'8    d''8    g'8   
 b'8    g'8    \bar "|"   d'8    e'8    g'8    a'8    b'8    a'8    d''8    b'8 
   \bar "|"       a'4 ^"Am"   b'8    a'8    b'16    c''16    d''8    e''8    
fis''8    \bar "|"   g''8    e''8    d''8    b'8    a'8    g'8    e'8    g'8    
\bar "|"     b'4. ^"G"   g'8    a'4    g'8    e'8    \bar "|"   d'8    e'8    
g'8    a'8      b'8 ^"Am"   a'8    a'4  }     \repeat volta 2 {     |
 e''8 ^"Am"   a''8    a''16    a''16    g''8    a''4    g''8    a''8    
\bar "|"   b''8    g''8    a''8    fis''8    g''8    fis''8    e''8    d''8    
\bar "|"   e''8 ^"G"   g''8    g''16    g''16    fis''8    g''4    g''8    e''8 
   \bar "|"   d''8    e''8    g''8    a''8    b''8    g''8    a''8    g''8    
\bar "|"       e''8 ^"Am"   a''8    a''16    a''16    g''8    a''4    g''8    
a''8    \bar "|"   b''8    g''8    a''8    fis''8    g''8    fis''8    e''8    
d''8    \bar "|"   e''8 ^"G"   g''8    g''16    g''16    g''8    e''8    d''8   
 b'8    a'8    \bar "|"   d''8    b'8    g''8    b'8      b'8 ^"Am"   a'8    
a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
