\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/169#setting22389"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/169#setting22389" {"https://thesession.org/tunes/169#setting22389"}}}
	title = "Gusty's Frolics"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   d'8  \tuplet 3/2 {   d'8    d'8    
d'8  }   d'8  \tuplet 3/2 {   d'8    d'8    d'8  }   fis'4 ^"~"  \grace {    b'8 
 }   a'8    \bar "|"   d'8  \tuplet 3/2 {   d'8    d'8    d'8  }   fis'8    a'8  
  fis'8    g'8    fis'8    e'8  \bar "|"   d'8  \tuplet 3/2 {   d'8    d'8    
d'8  }   d'8  \tuplet 3/2 {   d'8    d'8    d'8  }   fis'4 ^"~"  \grace {    b'8 
 }   a'8    \bar "|"   g'8    fis'8    g'8    e'8  \tuplet 3/2 {   cis''8 -.   
a'8 -.   e'8  }   g'8    fis'8    e'8    }     \repeat volta 2 {   fis'8    d'8 
   fis'8    a'8    b'8    fis'8    a'4    g'8    \bar "|"   fis'8    d'8    
fis'8    a'8    b'8    fis'8    g'8    fis'8    e'8    \bar "|"   fis'8    d'8  
  fis'8    a'8    b'8    fis'8    a'4    fis''8    \bar "|"   g''8    fis''8    
g''8    e''8    cis''8    e''8    g'8    fis'8    e'8    }     
\repeat volta 2 {   d''8  \tuplet 3/2 {   g'8 -.   fis'8 -.   d''8 -. }   d''8  
\tuplet 3/2 {   g'8 -.   fis'8 -.   d''8 -. }   fis''4 ^"~"    a''8    \bar "|"  
 d''8  \tuplet 3/2 {   g'8 -.   fis'8 -.   d''8 -. }   \tuplet 3/2 {   fis''8 -.  
 cis''8 -.   a'8 -. }   fis''8    g''8    fis''8    e''8    \bar "|"   d''8  
\tuplet 3/2 {   g'8 -.   fis'8 -.   d''8 -. }   d''8  \tuplet 3/2 {   g'8 -.   
fis'8 -.   d''8 -. }   fis''4 ^"~"    a''8    \bar "|"   g''8    fis''8    g''8 
   \tuplet 3/2 {   fis''8 -.   cis''8 -.   a'8 -. }   fis''8    g''8    fis''8   
 e''8    }     \repeat volta 2 {   d''8  \tuplet 3/2 {   g'8 -.   fis'8 -.   
d''8 -. }   cis''8  \tuplet 3/2 {   g'8 -.   fis'8 -.   cis''8 -. }   d''4    
b'8    \bar "|"   a'8    fis'8    a'8    d'8  \tuplet 3/2 {   d'8    d'8    d'8  
}   d'8  \tuplet 3/2 {   d'8    d'8    d'8  }   \bar "|"   b'8    d'8    d'8    
cis''8    d'8    d'8    b'8    r8 fis''8  \bar "|"   g''8    fis''8    g''8    
\tuplet 3/2 {   e''8 -.   cis''8 -.   a'8 -. }   fis''8    g''8    fis''8    
e''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
