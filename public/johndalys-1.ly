\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Enob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/524#setting524"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/524#setting524" {"https://thesession.org/tunes/524#setting524"}}}
	title = "John Daly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \tuplet 3/2 {   d''8    e''8    fis''8  } \bar "|" 
\tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 {   a''8    g''8    
fis''8  }   g''8.    d''16    b'8.    a'16    \bar "|"   g'8.    fis'16    g'8. 
   b'16    d''8.    b'16    g'8.    fis'16    \bar "|"   e'8.    c'16    e'8.   
 g'16    c''8.    b'16    a'8.    g'16    \bar "|"     \bar "|"   fis'8.    
d''16    cis''8.    e''16    d''8.    g''16    fis''8.    a''16    \bar "|" 
\tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 {   a''8    g''8    
fis''8  }   g''8.    d''16    b'8.    a'16    \bar "|"   g'8.    fis'16    g'8. 
   b'16    d''8.    b'16    \tuplet 3/2 {   g'8    fis'8    f'8  }   \bar "|"    
 \bar "|"   e'8.    g16    c'8.    e'16    d'8.    fis'16    a'8.    c''16    
\bar "|"   b'8.    g'16    g'8.    fis'16    g'4    \tuplet 3/2 {   d''8    e''8 
   fis''8  }   \bar "|" \tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 { 
  a''8    g''8    fis''8  }   g''8.    d''16    b'8.    a'16    \bar "|"     
\bar "|"   g'8.    fis'16    g'8.    b'16    d''8.    b'16    g'8.    fis'16    
\bar "|"   e'8.    c''16    \tuplet 3/2 {   c''8    c''8    c''8  }   d'8.    
b'16    \tuplet 3/2 {   b'8    b'8    b'8  }   \bar "|" \tuplet 3/2 {   a'8    
b'8    d''8  }   cis''8.    e''16    d''8.    g''16    fis''8    a''8  \bar "|" 
    \bar "|" \tuplet 3/2 {   g''8    b''8    a''8  }   \tuplet 3/2 {   g''8    
fis''8    e''8  }   d''8.    c''16    b'8.    a'16    \bar "|"   g'8.    fis'16 
   g'8.    b'16    d''8.    b'16    g'8.    fis'16    \bar "|"   e'8.    g16    
c'8.    e'16    d'8.    fis'16    a'8.    c''16    \bar "|"     \bar "|"   b'8. 
   g'16    g'8.    fis'16    g'8.    e''16    \tuplet 3/2 {   fis''8    g''8    
a''8  } \bar "|"   b''8.    fis''16    d''8.    b'16    fis'8.    g'16    g'8.  
  fis'16    \bar "|"   e'8.    g'16    c''8.    e''16    d''8.    b'16    g'8.  
  fis'16    \bar "|"     e'8.    c'16    e'8.    g'16    c''8.    b'16    a'8.  
  g'16    \bar "|"   fis'8.    d''16    cis''8.    e''16    d''8.    e''16    
\tuplet 3/2 {   fis''8    g''8    a''8  }   \bar "|"   b''8.    fis''16    d''8. 
   b'16    fis'8.    g'16    g'8.    fis'16    \bar "|"     e'8.    g'16    
c''8.    e''16    d''8.    b'16    \tuplet 3/2 {   g'8    fis'8    f'8  }   
\bar "|"   e'8.    g16    c'8.    e'16    d'8.    fis'16    a'8.    c''16    
\bar "|"   b'8.    g'16    g'8.    fis'16    g'8.    cis''16    d''8.    e''16  
  \bar "|"     fis''4    \tuplet 3/2 {   b'8    b'8    b'8  }   bes'8.    b'!16  
  \tuplet 3/2 {   e''8    fis''8    g''8  } \bar "|"   fis''8.    b'16    g''8.  
  fis''16    e''8.    d''16    cis''8.    b'16    \bar "|"   b'8.    a'16    
gis'8.    a'16    b'8.    a'16    \tuplet 3/2 {   cis''8    d''8    e''8  }   
\bar "|"     a''8.    gis''16    a''8.    b''16    a''8.    g''!16    fis''8.   
 a''16  \bar "|" \tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 {   a''8 
   g''8    fis''8  }   g''8.    d''16    b'8.    a'16    \bar "|"   g'8.    
fis'16    g'8.    b'16    d''8.    b'16    \tuplet 3/2 {   g'8    fis'8    f'8  
}   \bar "|"     e'8.    g16    c'8.    e'16    d'8.    fis'16    a'8.    c''16 
   \bar "|"   b'8.    g'16    g'8.    fis'16    g'2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
