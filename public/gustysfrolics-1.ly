\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JD"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/169#setting169"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/169#setting169" {"https://thesession.org/tunes/169#setting169"}}}
	title = "Gusty's Frolics"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   a8    d'8    d'8    d'8    cis'8  
  d'8    fis'4. ^"~"  \bar "|"   a8    d'8    d'8    d'8    fis'8    a'8    g'8 
   e'8    cis'8  \bar "|"   a8    d'8    d'8    a8    d'8    d'8    fis'8    
e'8    fis'8  \bar "|"   g'4. ^"~"    e'4    fis'8    g'8    fis'8    e'8  }    
 \bar "|"   fis'8    e'8    d'8    a'8    b'8    gis'8    a'4    g'!8  \bar "|" 
  fis'8    d'8    d'8    a'8    b'8    cis''8    d''8    a'8    g'8  \bar "|"   
fis'8    e'8    d'8    a'8    b'8    gis'8    a'8    g'!8    fis'8  \bar "|"   
g'8    b'8    g'8    e'4    fis'8    g'8    fis'8    e'8  \bar "|"     \bar "|" 
  fis'8    e'8    d'8    a'8    b'8    gis'8    a'4    g'!8  \bar "|"   fis'8   
 d'8    d'8    a'8    b'8    cis''8    d''8    a'8    g'8  \bar "|"   fis'8    
e'8    d'8    d''4. ^"~"    a'8    fis'8    d'8  \bar "|"   g'8    b'8    g'8   
 e'4    fis'8    g'8    fis'8    e'8  \bar "|"     \repeat volta 2 {   a'8    
d''8    d''8    d''8    e''8    d''8    fis''4. ^"~"  \bar "|"   a'8    d''8    
d''8    d''8    fis''8    a''8    g''8    e''8    cis''8  \bar "|"   a'8    
d''8    d''8    d''8    e''8    d''8    fis''4. ^"~"  \bar "|"   g''4. ^"~"    
e''8    cis''8    a'8    g''8    fis''8    e''8  }     \repeat volta 2 {   d''8 
   fis''8    d''8    cis''8    e''8    cis''8    d''4    b'8  \bar "|"   a'8    
fis'8    d'8    d'8    e'16    fis'16    g'8    a'8    fis'8    d'8  \bar "|"   
g'8    b'8    d''8    d''8    c''8    b'8    c''4 ^"~"    a'8  \bar "|"   g'8   
 e'8    c'8    c'8    e'8    c'8    g'8    e'8    c'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
