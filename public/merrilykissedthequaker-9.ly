\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/70#setting24550"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/70#setting24550" {"https://thesession.org/tunes/70#setting24550"}}}
	title = "Merrily Kissed The Quaker"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key g \major \time 6/8   \repeat volta 2 {   d''8. (   e''16    
fis''8  -)   a'4    fis''8    \bar "|"   g''4    fis''8  \grace {    fis''16  } 
  e''4    d''8    \bar "|"   d''8. (   e''16    fis''8  -)   a'4    a'8    
\bar "|"   b'4. (   d''4  -)   a'8    \bar "|"     d''8.    e''16    fis''8    
a'4    fis''8    \bar "|"   g''4    fis''8  \grace {    fis''16  }   e''4    
d''8    \bar "|"   d''8.    e''16    fis''8    a'4    a'8    \bar "|"   b'4.    
d''4    r8   }     d''4 (   fis''8  -)   a''4 (   fis''8  -)   \bar "|"   b''4  
  g''8    a''4.    \bar "|"   d''4    fis''8  \grace {    fis''16  }   a''4    
g''8    \bar "|"   fis''4.    a''4    r8   \bar "|"     b''4    g''8    e''8    
fis''8    g''8    \bar "|"   a''4    fis''8    d''4    d''8    \bar "|"   d''8. 
   e''16    fis''8    a'4    a'8    \bar "|"   b'4.    d''4    r8   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
