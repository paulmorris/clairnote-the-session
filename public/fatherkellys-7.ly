\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/791#setting23637"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/791#setting23637" {"https://thesession.org/tunes/791#setting23637"}}}
	title = "Father Kelly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {     g'8 ^"G"   a'8  \bar "|"     
b'4 ^"G"   g'8    b'8    a'8    g'8    e'8    g'8    \bar "|"   d'8 ^"G"   g'8  
  g'8    fis'8    g'4    a'8    b'8    \bar "|"     cis''4 ^"C"   a'8    b'8    
cis''8    b'8    a'8    g'8    \bar "|"   fis'8    a'8    a'8    g'8      fis'8 
^"D"   d'8    g'8    a'8    \bar "|"       b'4 ^"G"   g'8    b'8    a'8    g'8  
  e'8    g'8    \bar "|"     d'8 ^"G"   g'8    g'8    fis'8    g'4  
\tuplet 3/2 {   a'8 -.   b'8 -.   cis''8 -. }   \bar "|"     d''4 ^"C"   b'8    
d''8    g''8    d''8    b'8    d''8    \bar "|"     cis''8 ^"D"   a'8    fis'8  
  a'8      g'4 ^"G"   }     \repeat volta 2 {     b'8 ^"G"   cis''8    \bar "|" 
    d''4 ^"G"   \tuplet 3/2 {   b'8 -.   cis''8 -.   d''8 -. }   g''8    d''8    
b'8    d''8    \bar "|"   d''4    \tuplet 3/2 {   b'8 -.   cis''8 -.   d''8 -. } 
  g''8    d''8    b'8    d''8    \bar "|"     e''4 ^"C"   cis''8    e''8    
a''8    g''8    fis''8    e''8    \bar "|"     d''8 ^"D"   e''8    fis''8    
g''8    a''8    g''8    fis''8    e''8    \bar "|"       d''4 ^"G"   
\tuplet 3/2 {   b'8 -.   cis''8 -.   d''8 -. }   g''8    d''8    b'8    d''8    
\bar "|"     d''4 ^"G"   \tuplet 3/2 {   b'8 -.   cis''8 -.   d''8 -. }   g''8   
 d''8    b'8    d''8    \bar "|"     cis''8 ^"C"   b'8    a'8    cis''8    b'8  
  a'8    g'8    b'8    \bar "|"   a'8 ^"D"   g'8    fis'8    a'8      g'4 ^"G"  
 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
