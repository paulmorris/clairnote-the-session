\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bledsoeo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/725#setting13797"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/725#setting13797" {"https://thesession.org/tunes/725#setting13797"}}}
	title = "Eddie Kelly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \mixolydian   \bar "|"   fis'16    \repeat volta 2 {   g'8    
e'8    fis'8    g'4    a'8  \bar "|"   b'8    e''8    e''8    e''8    d''8    
c''8  \bar "|"   b'8    a'8    b'8    g'8    a'8    b'8  \bar "|"   a'8    
fis'8    d'8    a'8    fis'8    d'8  \bar "|"   \bar "|"   g'8    e'8    fis'8  
  g'4    a'8  \bar "|"   b'8    e''8    e''8    e''8    d''8    c''8  \bar "|"  
 b'8    a'8    b'8    g'8    a'8    b'8  } \alternative{{   a'8    fis'8    d'8 
   e'4.  } {   a'8    fis'8    d'8    e'8.    e''16    fis''8    } }    
\repeat volta 2 {   g''8    fis''8    e''8    fis''8    e''8    d''8  \bar "|"  
 b'8    e''8    e''8    b'8    e''8    e''8  \bar "|"   g''8    fis''8    e''8  
  fis''8    e''8    d''8  \bar "|"   b'8    cis''8    d''8    e''4    fis''8  
\bar "|"   } \alternative{{   g''8    fis''8    e''8    fis''8    e''8    d''8  
\bar "|"   b'8    e''8    e''8    b'8    e''8    e''8  \bar "|"   b'8    a'8    
b'8    g'8    a'8    b'8    \bar "|"   a'8    fis'8    d'8    e'8    e''8    
fis''8  } }    \bar "|"   g''8    fis''8    g''8    a''8    g''8    a''8  
\bar "|"   b''8    g''8    e''8    e''8    d''8    b'8  \bar "|"   b'8    a'8   
 b'8    g'8    a'8    b'8    \bar "|"   a'8    fis'8    d'8    e'4.  \bar ":|." 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
