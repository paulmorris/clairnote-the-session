\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/779#setting13906"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/779#setting13906" {"https://thesession.org/tunes/779#setting13906"}}}
	title = "Chorus, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   a'8    g'8  \bar "|"   fis'4    d'8    fis'8    a'8   
 b'8    a'8    g'8  \bar "|"   fis'8    a'8    d'8    fis'8    a'4    d''4  
\bar "|"   d'4.    fis'8    a'8    b'8    a'8    fis'8  \bar "|"   g'8    fis'8 
   e'8    fis'8    g'4  }   |
 b'8    c''8  \bar "|"   d''8    b'8    c''8    a'8    b'8    g'8    fis'8    
g'8  \bar "|"   a'8    d''8    cis''8    d''8    a'4    b'8    c''!8  \bar "|"  
 d''8    b'8    c''8    a'8    b'8    g'8    fis'8    g'8  \bar "|"   a'8    
c''8    b'8    a'8    g'4    b'8    c''8  \bar "|"   d''8    b'8    c''8    a'8 
   b'8    g'8    fis'8    g'8  \bar "|"   a'8    b'8    c''8    d''8    e''8    
fis''8    g''8    e''8  \bar "|"   d''8    b'8    c''8    a'8    b'8    g'8    
fis'8    g'8  \bar "|"   a'8    c''8    b'8    a'8    g'4  \bar "||"   
\repeat volta 2 {   a''8    g''8  \bar "|"   fis''8    d''8    d''8    d''8    
fis''8    d''8    d''8    d''8  \bar "|"   fis''8    d''8    fis''8    g''8    
a''8    b''8    a''8    g''8  \bar "|"   fis''8    d''8    d''8    d''8    
fis''8    d''8    d''8    d''8  \bar "|"   c''8    d''8    e''8    fis''8    
g''4  }   b'8    c''8  \bar "|"   d''8    b'8    c''8    a'8    b'8    g'8    
fis'8    g'8  \bar "|"   a'8    d''8    cis''8    d''8    a'4    b'8    c''!8  
\bar "|"   d''8    b'8    c''8    a'8    b'8    g'8    fis'8    g'8  \bar "|"   
a'8    c''8    b'8    a'8    g'4    b'8    c''8  \bar "|"   d''4    
\tuplet 3/2 {   c''8    d''8    c''8  }   b'8    g'8    fis'8    g'8  \bar "|"   
a'8    b'8    c''8    d''8    e''8    fis''8    g''8    e''8  \bar "|"   d''8   
 b'8    c''8    a'8    b'8    g'8    fis'8    g'8  \bar "|"   a'8    c''8    
b'8    a'8    g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
