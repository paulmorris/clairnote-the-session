\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/380#setting13204"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/380#setting13204" {"https://thesession.org/tunes/380#setting13204"}}}
	title = "Jenny Dang The Weaver"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d''8    a'8    a'16    a'16    
a'8    a'8    fis'8    a'8    b'8    \bar "|"   d''8    a'8    a'16    a'16    
a'8    fis''4    e''8    fis''8    \bar "|"   d''8    b'8    b'16    b'16    
b'8    b'8    a'8    b'8    d''8    \bar "|"   a'8    b'8    d''8    e''8    
fis''8    a''8    e''8    fis''8  }     \repeat volta 2 {   d''4    fis''8    
d''8    e''8    fis''8    g''8    e''8    \bar "|"   d''8    e''8    fis''8    
d''8    e''16    e''16    e''8    d''8    b'8    \bar "|"   a'4    fis''8    
d''8    e''8    fis''8    g''8    e''8    } \alternative{{   a''8    a'8    
a'16    a'16    a'8    fis''4    e''8    fis''8    } {   a''8    b''8    a''8   
 g''8    fis''8    d''8    d''4    \bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
