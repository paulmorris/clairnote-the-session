\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jomac"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/377#setting377"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/377#setting377" {"https://thesession.org/tunes/377#setting377"}}}
	title = "Ellen O'Grady"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \mixolydian   e''8    a''8    a''8    e''8    fis''8    g''8   
 fis''8    e''8    d''8    \bar "|"   e''8    a''8    a''8    e''8    fis''8    
g''8    fis''4. ^"~"    \bar "|"   e''8    a''8    a''8    e''8    fis''8    
g''8    fis''8    e''8    d''8    \bar "|"   g''4. ^"~"    e''8    cis''8    
a'8    b'8    cis''8    d''8    \bar "|"     e''8    a''8    a''8    e''8    
fis''8    g''8    fis''8    e''8    d''8    \bar "|"   e''8    a''8    a''8    
e''8    fis''8    g''8    fis''4. ^"~"    \bar "|"   e''8    a''8    a''8    
e''8    fis''8    g''8    fis''8    e''8    d''8    \bar "|"   g''4. ^"~"    
e''8    cis''8    a'8    b'8    cis''8    d''8    \bar "|"     e''4. ^"~"    
b'8    cis''8    d''8    cis''8    b'8    a'8    \bar "|"   e''4. ^"~"    b'8   
 cis''8    d''8    cis''4. ^"~"    \bar "|"   e''4. ^"~"    b'8    cis''8    
d''8    cis''8    b'8    a'8    \bar "|"   a''8    g''8    fis''8    e''8    
cis''8    a'8    b'8    cis''8    d''8    \bar "|"     e''4. ^"~"    b'8    
cis''8    d''8    cis''8    b'8    a'8    \bar "|"   e''4. ^"~"    b'8    
cis''8    d''8    cis''4. ^"~"    \bar "|"   e''8    fis''8    g''8    a''8    
g''8    fis''8    e''8    d''8    cis''8    \bar "|"   fis''4. ^"~"    b'4    
cis''8    d''8    cis''8    b'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
