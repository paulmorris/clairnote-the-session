\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/652#setting13684"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/652#setting13684" {"https://thesession.org/tunes/652#setting13684"}}}
	title = "Plains Of Boyle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   fis'8 (   g'8  -) \bar "|"   a'8 ^\upbow   fis'8 (   
d'8    e'8  -)   fis'8    e'8    d'8    fis'8 ^\downbow( \bar "|" \tuplet 3/2 {  
 a'8    b'8    a'8  -) }   g'8    b'8    a'8    fis'8    d'8    e''8 ( \bar "|" 
\grace {    a''8  }   fis''8    e''8  -)   d''8    fis''8    e''8    d''8    
cis''8    e''8 ( \bar "|"   d''8    b'8  -)   a'8    b'8 (   c''8    a'8  -)   
g'8    b'8  \bar "|"   a'8    fis'8    d'8    e'8 ^\downbow( \grace {    g'8  } 
  fis'8    e'8  -)   d'8    fis'8 ( \bar "|" \tuplet 3/2 {   a'8    b'8    a'8  
-) }   g'8    b'8    a'8    fis'8    d'8    e''8 ( \bar "|" \grace {    a''8  } 
  fis''8    e''8  -)   d''8    b'8    a'8    b'8    a'8    g'8  \bar "|"   
fis'4    d'4    d'4  }   \repeat volta 2 {   fis''8 ^\downbow(   g''8  \bar "|" 
  a''8    fis''8  -)   d''8 ^\upbow   fis''8 ( \grace {    a''8  }   g''4  -)   
fis''8    e''8  \bar "|"   d''8    fis''8    e''8    d''8 (   b'8    a'8  -)   
fis'8    g'8 ^\downbow( \bar "|" \tuplet 3/2 {   a'8    b'8    a'8  -) }   fis'8 
   a'8    b'8    a'8    fis'8    b'8 ( \bar "|" \tuplet 3/2 {   a'8    b'8    
a'8  -) }   fis'8    a'8 ( \grace {    d''8  }   b'8    a'8  -)   fis'8    
fis''8 ( \bar "|"   a''8    fis''8  -)   d''8    fis''8    \tuplet 3/2 {   g''8  
  g''8    g''8 ^\upbow( }   fis''8  -)   e''8  \bar "|"   d''8    fis''8    
e''8    d''8 ^\downbow(   b'8    a'8  -)   fis'8    g'8 ( \bar "|"   a'4  -)   
a'8    fis'8    g'8    b'8    a'8    g'8 ( \bar "|"   fis'4  -)   d'4    d'4  } 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
