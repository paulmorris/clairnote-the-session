\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/851#setting26692"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/851#setting26692" {"https://thesession.org/tunes/851#setting26692"}}}
	title = "Return From Fingal"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   e''4    c''8    b'8    a'8    
b'8    c''8    d''8  \bar "|"   e''4    c''8    b'8    a'4    d''4  \bar "|"   
d''8    c''8    b'8    d''8    g''4    d''4  \bar "|"   e''4    d''4    e''8    
fis''8    g''4  \bar "|"     e''4    c''8    b'8    a'8    b'8    c''8    d''8  
\bar "|"   e''4    c''8    b'8    a'4    d''4  \bar "|"   d''8    c''8    b'8   
 d''8    g''4    d''8    c''8  \bar "|"   b'4    a'4    a'2  }     
\repeat volta 2 {   b''4    a''4    a''4    g''8    fis''8  \bar "|"   e''4    
d''8    e''8    g''4    d''4  \bar "|"   e''4    d''4    g''4    d''4  \bar "|" 
  e''4    d''4    e''8    fis''8    g''8    a''8  \bar "|"     b''4    a''4    
a''4    g''8    fis''8  \bar "|"   e''4    d''8    e''8    g''4    d''4  
\bar "|"   d''8    c''8    b'8    d''8    g''4    d''8    c''8  \bar "|"   b'4  
  a'4    a'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
