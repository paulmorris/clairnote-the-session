\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/858#setting14025"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/858#setting14025" {"https://thesession.org/tunes/858#setting14025"}}}
	title = "Captain O'Kane"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \minor \time 3/4   \repeat volta 2 {   g'8    f'8    \bar "|"  
 d'4    bes'4    a'4    \bar "|"   bes'2    a'8    g'8    \bar "|"   a'8    
bes'8    c''8    bes'8    a'8    g'8    \bar "|"   f'4    g'4    a'4    
\bar "|"     bes'4    d''4    bes'4    \bar "|"   c''4    bes'8    a'8    g'8   
 f'8    \bar "|"   d'4    g'4    g'4    \bar "|"   g'2    }     
\repeat volta 2 {   g'8    a'8    \bar "|"   bes'4    d''4    d''4    \bar "|"  
 d''2    c''8    bes'8    \bar "|"   a'4    c''4    c''4    \bar "|"   c''2    
f''4    \bar "|"     d''4.    g''8    fis''4    \bar "|"   g''4.    a''8    
bes''4    \bar "|"   d''4    g''4    fis''4    \bar "|"   g''2    g''8    a''8  
  \bar "|"     bes''4    a''4    g''4    \bar "|"   f''4.    ees''8    d''4    
\bar "|"   d''8    c''8    bes'8    a'8    f''4    \bar "|"   f'4    g'4    a'4 
   \bar "|"     bes'4    d''4    bes'4    \bar "|"   d''8    c''8    bes'8    
a'8    g'8    f'8    \bar "|"   d'4    g'4    g'4    \bar "|"   g'2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
