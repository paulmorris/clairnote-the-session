\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thady Quill"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1117#setting28561"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1117#setting28561" {"https://thesession.org/tunes/1117#setting28561"}}}
	title = "Spanish Lady"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \major   b'4  \repeat volta 2 {   a'8    a'8    a'8    b'8  
\bar "|"   d''8    d''8    d''8    e''8  \bar "|"   fis''16    fis''16    
fis''16    fis''16    fis''8.    e''16  \bar "|"   d''8    b'8    b'8    
fis''16    e''16  \bar "|"   a'8    a'8    a'8    b'8  \bar "|"   d''8    d''8  
  d''8    e''8  \bar "|"   fis''8    a''8    a''8.    fis''16  \bar "|"   e''8  
  d''8    d''4  }     e''8  \repeat volta 2 {   fis''8    a''8    a''8.    
fis''16  \bar "|"   e''8    d''8    d''8.    e''16  \bar "|"   fis''8    a''8   
 a''8.    fis''16  \bar "|"   e''8    d''8    e''4  \bar "|"   fis''8    a''8   
 a''8.    fis''16  \bar "|"   e''8    d''8    d''8.    e''16  \bar "|"   
fis''16    gis''16    fis''16    e''16    fis''8.    e''16  \bar "|"   d''8    
b'8    b'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
