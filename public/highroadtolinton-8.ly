\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Anthony Picard"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1118#setting28045"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1118#setting28045" {"https://thesession.org/tunes/1118#setting28045"}}}
	title = "High Road To Linton, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   c''4    c''8    a'8    d''8    
e''8    d''4  \bar "|"   e''8    d''8    c''8    d''8    e''4    e''4  \bar "|" 
  c''4    c''8    a'8    d''8    e''8    d''4  \bar "|"   e''8    d''8    c''8  
  a'8    g'4    g'4  }     \bar "|"   g''4    g''8    e''8    a''4    a''8    
d''8  \bar "|"   e''8    d''8    c''8    d''8    e''4    e''4  \bar "|"   g''4  
  g''8    e''8    a''4    a''8    d''8  \bar "|"   e''8    d''8    c''8    a'8  
  g'4    g'4  \bar "|"     \bar "|"   g''4    g''8    e''8    a''4    a''8    
d''8  \bar "|"   e''8    d''8    c''8    d''8    e''4    e''4  \bar "|"   c''4  
  c''8    a'8    d''8    e''8    d''4  \bar "|"   e''8    d''8    c''8    a'8   
 g'4    g'4  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
