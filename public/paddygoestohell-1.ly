\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JeffK627"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/339#setting339"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/339#setting339" {"https://thesession.org/tunes/339#setting339"}}}
	title = "Paddy Goes To Hell"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key c \major   b'4    cis''8    d''4    e''8    f''4    b''8    
f''8    a''8    g''8    \bar "|"   a''8    g''8    f''8    e''8    d''8    
cis''8    e''8    d''8    cis''!8    b'4    b''8    \bar "|"     f''4    b''8   
 f''4    cis''8    e''8    d''8    cis''!8    b'8    a'8    g'8    \bar "|"   
f'8    b'8    a'8    f'8    g'8    a'8    d''8    b'8    a'8    b'4.    }     
\repeat volta 2 {   |
 d''4    f''8    cis''8    e''8    cis''!8    d''4    f''8    a''8    g''8    
f''8    \bar "|"   e''8    a''8    g''8    f''8    e''8    d''8    cis''8    
b'8    a'8    a'8    b'8    cis''!8    \bar "|"     d''4    f''8    cis''8    
e''8    cis''!8    d''4    f''8    a''8    g''8    f''8    \bar "|"   g''8    
f''8    e''8    f''8    e''8    d''8    e''8    d''8    cis''8    d''4    
cis''!8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
