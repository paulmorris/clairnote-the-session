\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "seara"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/394#setting394"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/394#setting394" {"https://thesession.org/tunes/394#setting394"}}}
	title = "Rosie's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8 ^"Bm"   b'8    b'8    a'8   
 b'4 ^"~"    d''8    a'8  \bar "|"   fis'8 ^"F^m"   a'8    a'4 ^"~"    a'8    
b'8    fis'8    e'8  \bar "|"   fis'8 ^"G"   b'8    b'8    a'8    b'4 ^"~"    
a'8    b'8  \bar "|"   d''8 ^"D"   e''8    fis''8    d''8      e''4. ^"A"^"~"   
 g''8  \bar "|"       fis''8 ^"Bm"   b'8    b'8    a'8    b'4 ^"~"    d''8    
a'8  \bar "|"   fis'8 ^"F^m"   a'8    a'8    fis'8    a'8    b'8    fis'8    
e'8  \bar "|"   fis'8 ^"Bm"   a'8    b'4 ^"~"      d''8 ^"Em"   b'8    fis''8   
 d''8  \bar "|"   cis''8 ^"A"   a'8    e''8    cis''8      d''8 ^"Bm"   b'8    
b'8    a'8  \bar "|"       fis'2 ^"Bm"^"~"    b'8    cis''8    d''8    a'8  
\bar "|"   fis'4. ^"F^m"^"~"    b'8    a'8    fis'8    e'8    d'8  \bar "|"   
fis'8 ^"G"   b'8    b'8    a'8    b'4 ^"~"    a'8    b'8  \bar "|"   d''8 ^"D"  
 e''8    fis''8    d''8      e''8 ^"A"   a''8    b''8    a''8  \bar "|"       
fis''8 ^"Bm"   b'8    b'8    a'8    b'4 ^"~"    d''8    b'8  \bar "|"   fis'8 
^"F^m"   a'8    a'8    fis'8    a'8    b'8    fis'8    e'8  \bar "|"   fis'8 
^"Bm"   b'8    b'4 ^"~"      d''8 ^"Em"   b'8    fis''8    d''8  \bar "|"   
cis''8 ^"A"   a'8    e''8    cis''8      d''8 ^"Bm"   b'8    b'8    a'8  
\bar "|"     fis''8    b'8    b'8    b'8      g''2 ^"Em7"  ~    \bar "|"   g''8 
   fis''8    fis''8    e''8    e''8    d''8    cis''8    b'8  \bar "|"   a'4 
^"A"^"~"    a'4 ^"~"    e'8    a'8    cis''8    e''8  \bar "|"   a''8 ^"F^m"   
e''8    fis''8    a''8    fis''8    e''8    cis''8    e''8  \bar "|"       
fis''8 ^"Bm"   b'8    b'4 ^"~"      g''8 ^"Em7"   fis''8    e''8    g''8  
\bar "|"   fis''8 ^"Bm"   b'8    b'4 ^"~"      d''8 ^"D"   cis''8    b'8    
cis''8  \bar "|"   a'8 ^"A"   b'8    cis''8    a'8    e'8    a'8    cis''8    
e''8  \bar "|"   a''8 ^"F^m"   e''8    cis''8    e''8      fis''8 ^"Bm"   b'8   
 b'4 ^"~"  \bar "|"       fis''8 ^"Bm"   b'8    b'4 ^"~"      g''8 ^"A"   e''8  
  a''8    g''8  \bar "|"   fis''8 ^"D"   b'8    b'4 ^"~"      d''8 ^"G"   b'8   
 e''8    cis''8  \bar "|"   a'4 ^"A"^"~"    a'4 ^"~"    e'8    a'8    cis''8    
e''8  \bar "|"   a''8 ^"F^m"   e''8    g''8    a''8    fis''8    e''8    cis''8 
   e''8  \bar "|"       fis''8 ^"G"   b'8    b'4 ^"~"    b''8    a''8    fis''8 
   e''8  \bar "|"   fis''8 ^"Gmaug7"   b'8    b'4 ^"~"    d''8    cis''8    b'8 
   cis''8  \bar "|"   a''2 ^"F^m7sus4"   a''8    fis''8    e''8    cis''8  
\bar "|"   d''4 ^"F^m7"^"~"    cis''16    b'16    a'8    b'8    a'8    fis'8    
e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
