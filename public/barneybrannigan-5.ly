\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1429#setting22877"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1429#setting22877" {"https://thesession.org/tunes/1429#setting22877"}}}
	title = "Barney Brannigan"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   fis'4    a'8    a'4. ^"~"    a'4. 
^"~"    \bar "|"   fis'4    a'8    a'4. ^"~"    b'8    cis''8    d''8    
\bar "|"   fis'4    a'8    a'4. ^"~"    a'4. ^"~"    \bar "|"   e''4    e''8    
e''4    d''8    cis''8    b'8    a'8    }     \repeat volta 2 {   fis''4    a'8 
   fis''4    a'8    fis''8    e''8    d''8    \bar "|"   fis''4    a'8    
fis''4    a''8    g''8    fis''8    e''8    \bar "|"   fis''4    a'8    fis''4  
  a'8    fis''8    e''8    d''8    \bar "|"   b'4    e''8    e''4    d''8    
cis''8    b'8    a'8    }     \repeat volta 2 {   fis''4    a''8    e''4    
fis''8    d''4    r8   \bar "|"   g''4    fis''8    e''4    d''8    cis''8    
b'8    a'8    \bar "|"   fis''4    a''8    e''4    fis''8    d''4    r8   
\bar "|"   b'4. ^"~"    a'4    g'8    fis'8    e'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
