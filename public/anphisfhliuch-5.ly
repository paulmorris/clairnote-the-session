\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Cú Chulainn1"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/879#setting23295"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/879#setting23295" {"https://thesession.org/tunes/879#setting23295"}}}
	title = "An Phis Fhliuch"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   d'4. ^"~"    d'4. ^"~"    c''4.  
\bar "|"   c''4    b'8    c''4    a'8    g'8    e'8    d'8  \bar "|"   d'4. 
^"~"    d'8    fis'8    a'8    d''4    a'8  \bar "|"   d''8    fis''8    e''8   
 d''8    c''8    a'8    g'8    e'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
