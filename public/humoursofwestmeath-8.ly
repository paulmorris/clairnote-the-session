\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/428#setting27602"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/428#setting27602" {"https://thesession.org/tunes/428#setting27602"}}}
	title = "Humours Of Westmeath, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   d''8.    d'16    d'8    fis'8    d'8    fis'8    a'8  
  fis'8    a'8    \bar "|"   d''8.    d'16    d'8    fis'8    d'8    fis''8    
g''8    fis''8    e''8    \bar "|"   d''8.    d'16    d'8    fis'8    d'8    
fis'8    a'8    g'8    fis'8    \bar "|"   g'16 (   a'16  -)   b'8    g'8 -.   
e'8    cis'8    e'8    g'8    fis'8    e'8    \bar "||"     d''4    g''8    
fis''8    d''8    fis''8    e''8    cis''8    a'8    \bar "|"   d''4    g''8    
fis''16    g''16    a''8    fis''8    g''8    fis''8    e''8    \bar "|"   d''4 
   g''8    fis''8    d''8    fis''8    e''8    cis''8    e''8    \bar "|"   
c''8    b'8    c''8    e'4    fis'8    g'8    fis'8    e'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
