\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1307#setting26939"
	arranger = "Setting 26"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1307#setting26939" {"https://thesession.org/tunes/1307#setting26939"}}}
	title = "Seven Step, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'4    g'4    g'4    g'4    \bar "|"   g'8.    a'16   
 b'8.    g'16    e'4    d'4    \bar "|"   b'4    b'4    b'4    b'4    \bar "|"  
 b'8.    c''16    d''8.    b'16    a'4    g'4    \bar "||"     b'8.    c''16    
d''8.    b'16    a'4    g'4    \bar "|"   g'8.    a'16    b'8.    g'16    e'4   
 dis'4    \bar "|"   d'8.    e'16    g'8.    a'16    b'8.    d''16    d''8.    
b'16    \bar "|"   a'4    g'4    g'2    \bar "|"     b'4    d''8.    b'16    
a'4    g'4    \bar "|"   g'4    b'8    g'8    e'4    dis'4    \bar "|"   d'4    
e'8.    g'16    b'4    d''8.    b'16    \bar "|"   a'4    g'4    g'2    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
