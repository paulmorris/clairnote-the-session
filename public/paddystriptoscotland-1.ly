\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "paul95"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1303#setting1303"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1303#setting1303" {"https://thesession.org/tunes/1303#setting1303"}}}
	title = "Paddy's Trip To Scotland"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''8    a'8    a'4 ^"~"    b'8    a'8    g'8    fis'8 
 \bar "|"   g'8    a'8    b'8    d''8    cis''8    a'8    b'8    cis''8  
\bar "|"   d''4    e''8    d''8    cis''8    a'8    b'8    cis''8  \bar "|"   
d''8    fis''8    e''8    d''8    cis''8    a'8    b'8    cis''8  \bar "|"     
d''8    a'8    a'4 ^"~"    b'8    a'8    g'8    fis'8  \bar "|"   g'8    a'8    
b'8    d''8    cis''8    a'8    b'8    cis''8  \bar "|"   d''4    e''8    d''8  
  cis''8    a'8    b'8    cis''8  \bar "|"   d''8    fis''8    e''8    cis''8   
 d''4    d''8    cis''8  \bar ":|."   d''8    fis''8    e''8    cis''8    d''4  
  d''8    b'8  \bar "||"     \bar ".|:"   a'8    d''8    fis''8    d''8    a'8   
 d''8    fis''8    d''8  \bar "|"   g''4. ^"~"    fis''8    e''8    d''8    
cis''8    b'8  \bar "|"   a'8    cis''8    e''8    cis''8    a'8    cis''8    
e''8    cis''8  \bar "|"   fis''4. ^"~"    e''8    d''8    cis''8    b'8    
cis''8  \bar "|"     a'8    d''8    fis''8    d''8    a'8    d''8    fis''8    
d''8  \bar "|"   g''4. ^"~"    fis''8    e''8    fis''8    g''8    e''8  
\bar "|"   a''8    a'8    a'4 ^"~"    b'8    a'8    b'8    cis''8  \bar "|"   
d''8    fis''8    e''8    cis''8    d''4    d''8    b'8  \bar ":|."   d''8    
fis''8    e''8    cis''8    d''8    a'8    b'8    cis''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
