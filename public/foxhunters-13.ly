\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/482#setting15802"
	arranger = "Setting 13"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/482#setting15802" {"https://thesession.org/tunes/482#setting15802"}}}
	title = "Foxhunter's, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key d \major   b'4.    b'4    a'8    g'4    a'8  \bar "|"   b'4    
e'8    e'4    d'8    g'4    a'8  \bar "|"   b'8    e''8    d''8    b'4    a'8   
 fis'4    a'8  \bar "|"   d''4    d'8    d'4    fis'8    e'4    d'8  }   
\repeat volta 2 {   fis''4.    fis''4 ^"~"    a''8    g''4    e''8  \bar "|"   
fis''4. ^"~"    fis''4    d''8    e''4    fis''8  \bar "|"   d''4    fis''8    
fis''4    a''8    g''4    b''8  \bar "|"   a''4    fis''8    d''4    fis''8    
e''4    d''8  }   \repeat volta 2 {   fis'4.    fis'4 ^"~"    a'8    g'4    e'8 
 \bar "|"   fis'4. ^"~"    fis'4    d'8    e'4    d'8  \bar "|"   d'4    fis'8  
  a'4    fis'8    g'4    b'8  \bar "|"   a'4    fis'8    d'4    fis'8    e'4    
d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
