\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1292#setting28156"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1292#setting28156" {"https://thesession.org/tunes/1292#setting28156"}}}
	title = "Hector The Hero"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \major   a4    \bar "|"   cis'4. ^"A"   b8    a4    \bar "|"   
  fis'4 ^"D"   e'4    cis'4    \bar "|"     e'2. ^"A"   \bar "|"   e'2    a4    
\bar "|"       cis'4. ^"A"   b8    a4    \bar "|"     fis'4 ^"D"   e'4    cis'4 
   \bar "|"     b2. ^"E"   \bar "|"   b2    a4    \bar "|"       cis'4. ^"A"   
b8    a4    \bar "|"     fis'4 ^"D"   e'4    cis'4    \bar "|"     e'2 ^"A"   
a4    \bar "|"     a'2 ^"D"   fis'4    \bar "|"       e'4 ^"A"   a4    cis'4    
\bar "|"     b2 ^"E"   a4    \bar "|"     a2. ^"A"   \bar "|"   a2    \bar "||" 
    a'4    \bar "|"   cis''4. ^"A"   b'8    a'4    \bar "|"     fis''4 ^"D"   
e''4    cis''4    \bar "|"     e''2. ^"A"   \bar "|"   e''2    a'4    \bar "|"  
     cis''4. ^"A"   b'8    a'4    \bar "|"     fis''4 ^"D"   e''4    cis''4    
\bar "|"     b'2. ^"E"   \bar "|"   b'2    a'4    \bar "|"       cis''4. ^"A"   
b'8    a'4    \bar "|"     fis''4 ^"D"   e''4    cis''4    \bar "|"     e''2 
^"A"   a'4    \bar "|"   a''2    fis''4    \bar "|"       e''4 ^"A"   a'4    
cis''4    \bar "|"     b'2 ^"E"   a'4    \bar "|"     a'2. ^"A"   \bar "|"   
a'2    \bar "||"     cis''4    \bar "|"   fis''4 ^"F\#m"   gis''4    fis''4    
\bar "|"     a''4 ^"D"   gis''4    fis''4    \bar "|"     e''2. ^"A"   \bar "|" 
  e''2    cis''4    \bar "|"       fis''4 ^"F\#m"   e''4    cis''4    \bar "|"  
   e''4 ^"A"   a'4    cis''4    \bar "|"     b'2. ^"E"   \bar "|"   b'2    
cis''4    \bar "|"       fis''4 ^"F\#m"   gis''4    fis''4    \bar "|"     a''4 
^"D"   gis''4    fis''4    \bar "|"     e''4 ^"A"   a'4    b'4    \bar "|"     
cis''4. ^"D"   a''8    fis''4    \bar "|"       e''4 ^"A"   a'4    cis''4    
\bar "|"     b'2 ^"E"   a'4    \bar "|"     a'2. ^"A"   \bar "|"   a'2    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
