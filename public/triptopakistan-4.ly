\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/112#setting20981"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/112#setting20981" {"https://thesession.org/tunes/112#setting20981"}}}
	title = "Trip To Pakistan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \repeat volta 2 {   e'8    gis'8    b'8    e'8    
gis'!4.    b'8  \bar "|"   a'4.    b'8    a'8    gis'8    f'8    gis'!8  
\bar "|"   e'8    gis'8    b'8    e'8    gis'!4.    b'8  \bar "|"   a'8    
gis'8    f'8    gis'!8    e'2  }     \repeat volta 2 {   e'8    gis'8    b'8    
gis'!8    c''4.    a'8  \bar "|"   b'4.    d''8    a'8    gis'8    f'8    
gis'!8  \bar "|"   e'8    gis'8    b'8    gis'!8    c''4.    a'8  \bar "|"   
b'8    a'8    gis'8    b'8    a'2  }     \repeat volta 2 {   f'8    gis'8    
b'8    f'8    gis'!8    b'8    gis'!8    f'8  \bar "|"   e'8    f'8    gis'8    
e'8    f'8    gis'!8    f'8    e'8  \bar "|"   d'8    f'8    a'8    d'8    f'8  
  a'8    d'8    f'8  \bar "|"   a'8    gis'8    f'8    gis'!8    e'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
