\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/371#setting371"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/371#setting371" {"https://thesession.org/tunes/371#setting371"}}}
	title = "Bang Your Frog On The Sofa"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   \repeat volta 2 {   d'8    f'8    a'8    d''8    c''4 
   g'8    bes'8  \bar "|"   a'8    f'8    d'8    f'8    g'8    e'8    c'8    
e'8  \bar "|"   d'8    c'8    d'8    e'8    f'8    g'8    a'8    c''8  \bar "|" 
  d''8    c''8    a'8    f'8    g'4    f'8    g'8  \bar "|"     a'8    d''8    
d''8    e''8    f''8    e''8    d''8    c''8  \bar "|"   d''8    c''8    a'8    
f'8    g'8    e'8    c'8    e'8  \bar "|"   d'8    f'8    e'8    g'8    f'8    
a'8    g'8    bes'8  } \alternative{{   a'8    g'8    e'8    f'8    d'4    a8   
 c'8  } {   a'8    d''8    cis''8    e''8    d''8    e''8    f''8    g''8  
\bar "||"     a''4    a''8    f''8    d''8    f''8    a''8    f''8  \bar "|"   
d''8    f''8    a''8    f''8    g''8    f''8    e''8    d''8  \bar "|"   c''8   
 e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   g''8    e''8    c''8    d''8  
\bar "|"   e''8    d''8    e''8    f''8    g''8    e''8    f''8    g''8  
\bar "|"     a''4    a''8    f''8    d''8    f''8    a''8    f''8  \bar "|"   
d''8    e''8    f''8    g''8    a''8    f''8    d''8    e''8  \bar "|"   f''8   
 e''8    d''8    c''8    a'8    bes'8    g'8    bes'8  \bar "|"   a'8    d''8   
 cis''8    e''8    d''8    e''8    f''8    g''8  \bar "|"     a''4    a''8    
f''8    d''8    f''8    a''8    f''8  \bar "|"   d''8    f''8    a''8    f''8   
 g''8    f''8    e''8    d''8  \bar "|"   c''8    e''8    \tuplet 3/2 {   e''8   
 e''8    e''8  }   g''8    e''8    c''8    d''8  \bar "|"   e''8    d''8    
e''8    f''8    g''8    e''8    f''8    g''8  \bar "|"     a''8    f''8    
\tuplet 3/2 {   f''8    f''8    f''8  }   g''8    e''8    \tuplet 3/2 {   e''8    
e''8    e''8  } \bar "|"   f''8    e''8    d''8    f''8    e''8    d''8    c''8 
   e''8  \bar "|"   f''8    e''8    d''8    c''8    a'8    g'8    f'8    a'8  
\bar "|"   g'8    e'8    c'8    e'8    d'4    a8    c'8  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
