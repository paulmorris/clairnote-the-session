\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "An Draighean"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/45#setting27221"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/45#setting27221" {"https://thesession.org/tunes/45#setting27221"}}}
	title = "Humours Of Glendart, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   a'4.  \bar "|" \grace {    b'8    cis''8  }   b'8    
a'8    fis'8  \grace {    b'8  }   a'8    d'8  \grace {    a'8  }   d'8    
\bar "|" \grace {    a'8  }   fis'8    e'8  \grace {    a'8  }   d'8    a'8    
d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    cis''8  }   b'8    a'8    
fis'8    a'8    d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    a'8  }   
fis'8    e'8  \grace {    a'8  }   d'8    e'8    fis'8    a'8    \bar "|"     
\grace {    b'8    cis''8  }   b'8    a'8    fis'8    a'8    d'8  \grace {    
a'8  }   d'8    \bar "|" \grace {    a'8  }   fis'8    e'8  \grace {    a'8  }  
 d'8    a'8    d'8  \grace {    a'8  }   d'8    \bar "|"   d''8 -.   fis''8 -.  
 e''8    d''8    cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    
fis'8    e'8  \grace {    a'8  }   e'8    fis'8    a'8    \bar "|"     
\grace {    cis''8  }   b'8    a'8    fis'8    a'8  \grace {    b'8  }   a'8    
d'8    \bar "|" \grace {    a'8  }   fis'8    e'8    d'8  \grace {    b'8  }   
a'8    d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    cis''8  }   b'8    
a'8    fis'8    a'8    d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    
a'8  }   fis'8    e'8    d'8    e'8    fis'8    a'8    \bar "|"     \grace {    
b'8    cis''8  }   b'8    a'8    fis'8    a'8    d'8  \grace {    a'8  }   d'8  
  \bar "|" \grace {    a'8  }   fis'8    e'8  \grace {    a'8  }   d'8    a'8   
 d'8  \grace {    a'8  }   d'8    \bar "|"   d''8 -.   fis''8 -.   e''8    d''8 
   cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    fis'8    e'8  
\grace {    a'8  }   e'8    fis'8    a'8    \bar "|"     \repeat volta 2 {   
d''8    fis''8    e''8    d''8    cis''8    b'8    \bar "|" \grace {    a'8    
b'8  }   a'8    fis'8    a'8  \grace {    b'8  }   a'8    fis'8    a'8    
\bar "|"   d''8    fis''8    e''8    d''8    fis''8    d''8    \bar "|"   e''8  
  d''8    e''8    fis''8    d''8    b'8    \bar "|"     d''8    fis''8    e''8  
  d''8    cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    fis'8    
a'8  \grace {    b'8  }   a'8    b'8    d''8    \bar "|"   e''8    fis''8    
g''8  \grace {    a''8    g''8  }   fis''8    d''8    b'8    \bar "|" \grace {  
  a'8    b'8  }   a'8    fis'8    e'8  \grace {    a'8  }   e'8    fis'8    a'8 
 }     \grace {    b'8    cis''8  }   b'8    a'8    fis'8    a'8  \grace {    
b'8  }   a'8    d'8    \bar "|" \grace {    a'8  }   fis'8    e'8    d'8    a'8 
   d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    cis''8  }   b'8    a'8 
   fis'8    a'8    d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    a'8  } 
  fis'8    e'8    d'8    e'8    fis'8    a'8    \bar "|"     \grace {    b'8    
cis''8  }   b'8    a'8    fis'8    a'8    d'8  \grace {    a'8  }   d'8    
\bar "|" \grace {    a'8  }   fis'8    e'8  \grace {    a'8  }   d'8    a'8    
d'8  \grace {    a'8  }   d'8    \bar "|"   d''8 -.   fis''8 -.   e''8    d''8  
  cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    fis'8    e'8  
\grace {    a'8  }   e'8    fis'8    a'8    \bar "|"     \grace {    b'8    
cis''8  }   b'8    a'8    fis'8    a'8    d'8  \grace {    a'8  }   d'8    
\bar "|" \grace {    a'8  }   fis'8    d'8  \grace {    a'8  }   d'8    a'8    
d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    cis''8  }   b'8    a'8    
fis'8    a'8    d'8  \grace {    a'8  }   d'8    \bar "|" \grace {    a'8  }   
fis'8    e'8    d'8    e'8    fis'8    a'8    \bar "|"     \grace {    b'8    
cis''8  }   b'8    a'8    fis'8    a'8  \grace {    b'8  }   a'8    d'8    
\bar "|" \grace {    a'8  }   fis'8    e'8  \grace {    a'8  }   d'8    a'8    
d'8  \grace {    a'8  }   d'8    \bar "|"   d''8 -.   fis''8 -.   e''8    d''8  
  cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    fis'8    e'8  
\grace {    a'8  }   e'8    fis'8    a'8    \bar "|"     \repeat volta 2 {   
d''8    fis''8    e''8    d''8    cis''8    b'8    \bar "|" \grace {    a'8    
b'8  }   a'8    fis'8    a'8  \grace {    b'8  }   a'8    fis'8    a'8    
\bar "|"   d''8    fis''8    e''8    d''8    fis''8    d''8    \bar "|"   e''8  
  d''8    e''8    fis''8    d''8    b'8    \bar "|"     d''8    fis''8    e''8  
  d''8    cis''8    b'8    \bar "|" \grace {    a'8    b'8  }   a'8    fis'8    
a'8  \grace {    b'8  }   a'8    b'8    d''8    \bar "|"   e''8    fis''8    
g''8  \grace {    a''8    g''8  }   fis''8    d''8    b'8    \bar "|" \grace {  
  a'8    b'8  }   a'8    fis'8    e'8  \grace {    a'8  }   e'8    fis'8    a'8 
 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
