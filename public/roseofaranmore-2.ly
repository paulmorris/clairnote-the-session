\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1129#setting14390"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1129#setting14390" {"https://thesession.org/tunes/1129#setting14390"}}}
	title = "Rose Of Aranmore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   \repeat volta 2 {   a'8    fis'8    \bar "|"   d'2    
fis'4    \bar "|"   a'4    fis'4    d'4    \bar "|"   g'2    b'4    \bar "|"   
d''4.    cis''8    b'4    \bar "|"   a'2    b'4    \bar "|"   a'4    fis'4    
d'4    \bar "|"   e'2.     ~    \bar "|"   e'2    a'8    fis'8    \bar "|"     
d'2    d'8    fis'8    \bar "|"   a'4    \tuplet 3/2 {   fis'8    g'8    fis'8  
}   d'4    \bar "|"   g'2    b'4    \bar "|"   d''4    cis''4    b'4    
\bar "|"   a'2    fis'8    a'8    \bar "|"   g'4    fis'4   ~    fis'8    e'8   
 \bar "|"   d'2.     ~    \bar "|"   d'2    e'8    fis'8    \bar "||"     g'2   
 b'4    \bar "|"   d''4    cis''4    b'4    \bar "|"   a'2    b'4    \bar "|"   
a'4    fis'4   ~    fis'8    d'8    \bar "|"   g'2    b'4    \bar "|"   a'4    
\tuplet 3/2 {   fis'8    g'8    fis'8  }   d'4    \bar "|"   e'2.     ~    
\bar "|"   e'2    a'8    fis'8    \bar "|"     d'2   ~    d'8    fis'8    
\bar "|"   a'4    fis'4   ~    fis'8    d'8    \bar "|"   g'4.    d'8    g'8    
b'8    \bar "|"   d''4    cis''4   ~    cis''8    b'8    \bar "|"   a'2   ~    
a'8    fis'8    \bar "|"   g'4    fis'4    e'4    \bar "|"   d'2.     ~    
\bar "|"   d'2    \bar "|."   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
