\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "milesnagopaleen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1449#setting14837"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1449#setting14837" {"https://thesession.org/tunes/1449#setting14837"}}}
	title = "Redican's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   e''8  \bar "|"   d''8    g''8    g''4    g''8    e''8 
   d''8    g''8  \bar "|"   e''8    a''8    a''8    g''8    a''8    g''8    
e''8    g''8  \bar "|"   fis''16    e''16    d''8    a''8    d''8    b''8    
d''8    a''8    d''8  \bar "|"   fis''16    e''16    d''8    a''8    d''8    
g''8    fis''8    e''8    d''8  \bar "|"   b'8    g'8    g'4    e'8    g'8    
d'8    fis''8  \bar "|"   g''4    fis''8    g''8    e''8    d''8    b'8    a'8  
\bar "|"   g'4    b'8    g'8    d'8    g'8    b'8    g'8  \bar "|"   a'8    g'8 
   fis'8    a'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
