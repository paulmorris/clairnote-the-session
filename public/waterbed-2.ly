\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Phantom Button"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/328#setting13105"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/328#setting13105" {"https://thesession.org/tunes/328#setting13105"}}}
	title = "Waterbed, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   \bar "|"   e'8    a'8    a'8    e'8    b'8    c''8   
 a'8    g'8    \bar "|"   e'8    g'8    d'8    g'8    e'8    d'8    e'8    g'8  
  \bar "|"   a'8    e'8    g'8    a'8    c''8    a'8    a'4 ^"~"    \bar "|"   
c''8    d''8    e''8    c''8    d''8    c''8    a'8    g'8    \bar "|"   
\bar "|"   e'8    a'8    a'8    e'8    b'8    c''8    a'8    g'8    \bar "|"   
e'8    g'8    d'8    g'8    e'4    b'16    c''16    d''8    \bar "|"   e''8    
c''8    d''8    c''8    a'8    e'8    g'8    a'8    \bar "|"   c''8    a'8    
g'8    a'8    e'8    a'8    a'4 ^"~"    }   \repeat volta 2 {   e''4 ^"~"    
d''8    e''8    g''8    e''8    e''4 ^"~"    \bar "|"   b'16    c''16    d''8   
 e''8    g''8    d''8    c''8    a'4    \bar "|"   e''4 ^"~"    d''8    e''8    
g''8    a''8    a''4 ^"~"    \bar "|"   g''8    e''8    d''8    c''8    a'8    
e'8    g'8    a'8    \bar "|"   \bar "|"   e''4 ^"~"    d''8    e''8    g''8    
e''8    e''4 ^"~"    \bar "|"   b'16    c''16    d''8    e''8    g''8    d''8   
 e''8    g''8    a''8    \bar "|"   g''8    a''8    e''8    g''8    d''8    e'8 
   g'8    a'8    \bar "|"   c''8    a'8    g'8    a'8    e'8    a'8    a'4 
^"~"    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
