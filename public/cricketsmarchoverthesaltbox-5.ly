\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "wheresrhys"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/983#setting27124"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/983#setting27124" {"https://thesession.org/tunes/983#setting27124"}}}
	title = "Cricket's March Over The Salt Box, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'8 (   \bar "|"   a'4  -)   fis'8    fis'8.    e'16  
  e''8 (   \bar "|"   d''4  -)   b'8    b'8.    a'16    g''8 (   \bar "|"   
fis''4  -)   b'8    b'8.    a'16    b''8 (   \bar "|"   a''4  -)   fis''8    
fis''8.    e''16    fis''8    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
