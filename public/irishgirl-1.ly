\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Kane"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/844#setting844"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/844#setting844" {"https://thesession.org/tunes/844#setting844"}}}
	title = "Irish Girl, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'4 ^"~"    fis'8    e'8    d'8 
   e'8    fis'8    d'8    \bar "|"   e'8    g8    g8    g8    e'8    g8    
\tuplet 3/2 {   g8    g8    g8  }   \bar "|"   fis'4    fis'8    e'8    d'8    
e'8    fis'8    a'8    \bar "|"   \tuplet 3/2 {   b'8    cis''8    d''8  }   
e''8    cis''8    d''8    b'8    a'8    g'8    \bar "|"     fis'4 ^"~"    fis'8 
   e'8    d'8    e'8    fis'8    d'8    \bar "|"   e'8    g8    g8    g8    e'8 
   g'8    g'4 ^"~"    \bar "|"   fis'4    fis'8    e'8    d'8    e'8    fis'8   
 a'8    \bar "|"   cis''8    d''8    e''8    cis''8    d''4.    e''8    }     
\repeat volta 2 {   fis''8    d''8    d''4 ^"~"    d''8    fis''8    a''8    
fis''8    \bar "|"   e''8    d''8    cis''8    d''8    e''8    fis''8    g''8   
 e''8    \bar "|"   fis''8    d''8    d''8    cis''8    d''8    fis''8    a''8  
  fis''8    \bar "|"   g''8    fis''8    e''8    g''8    fis''8    d''8    
\tuplet 3/2 {   d''8    d''8    d''8  }   \bar "|"     fis''8    d''8    d''8    
cis''8    d''8    fis''8    a''4 ^"~"    \bar "|"   e''8    d''8    cis''8    
d''8    e''8    fis''8    g''8    e''8    \bar "|"   d''8    fis''8    a''8    
fis''8    g''4 ^"~"    a''8    g''8    \bar "|"   fis''8    a''8    e''8    
g''8    fis''8    d''8    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
