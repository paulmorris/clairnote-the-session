\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SPeak"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1083#setting1083"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1083#setting1083" {"https://thesession.org/tunes/1083#setting1083"}}}
	title = "Petronella"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'4 ^"D"   a'8    fis'8      
e'4 ^"A7"   a'8    fis'8  \bar "|"   d'4 ^"D"   d'4    d'4    fis'8    a'8  
\bar "|"   d''4 ^"G"   cis''8    d''8      e''4 ^"E7"   d''4  \bar "|"   cis''8 
^"A7"   d''8    e''8    cis''8    a'4    d''8    a'8    \bar "|"       fis'4 
^"D"   a'8    fis'8      e'4 ^"A7"   a'8    fis'8  \bar "|"   d'4 ^"D"   d'4    
d'4    fis'8    a'8  \bar "|"   d''4 ^"G"   cis''8    d''8      e''4 ^"A7"   
cis''4  \bar "|"   d''2 ^"D"   d''4    d''8    a'8  }     \repeat volta 2 {   
fis'4 ^"D"   fis''8    d''8    a'4    a''8    fis''8  \bar "|"   g''4 ^"Em"   
g''8    fis''8    e''8    d''8    cis''8    b'8  \bar "|"   a'4 ^"A7"   e''8    
cis''8    a'4    g''8    e''8  \bar "|"   fis''4 ^"D"   fis''8    d''8      a'8 
^"A7"   d''8    a'8    fis'8    \bar "|"       d'4 ^"D"   fis''8    d''8    a'4 
   a''8    fis''8  \bar "|"   g''4 ^"Em"   g''8    fis''8    e''8    d''8    
cis''8    b'8  \bar "|"   a'4 ^"A7"   e''8    cis''8    a'8    g''8    e''8    
cis''8  \bar "|"   d''2 ^"D"   d''4    d''8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
