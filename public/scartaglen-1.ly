\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/900#setting900"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/900#setting900" {"https://thesession.org/tunes/900#setting900"}}}
	title = "Scartaglen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   d'8    fis'8    e'8    fis'8    d'4    d''8    b'8    
\bar "|"   cis''8    a'8    a'8    g'8    e'8    fis'8    fis'8    e'8    
\bar "|"   d'8    fis'8    e'8    fis'8    d'4    d''8    b'8    \bar "|"   
cis''8    a'8    g'8    e'8    e'8    d'8    d'4      \bar "|"   d'8    fis'8   
 e'8    fis'8    d'4    d''8    b'8    \bar "|"   cis''8    a'8    a'8    g'8   
 e'8    fis'8    g'8    e'8    \bar "|"   a'8    d''8    d''8    cis''8    d''8 
   fis''8    e''8    d''8    \bar "|"   cis''8    a'8    g'8    e'8    e'8    
d'8    d'4    }     \repeat volta 2 {   fis''4.    e''8    fis''8    a''8    
g''8    e''8    \bar "|"   fis''4    g''8    e''8    a''8    fis''8    d''8    
e''8    \bar "|"   e''8    fis''8    fis''8    e''8    fis''8    a''8    g''8   
 e''8    \bar "|"     fis''8    d''8    e''8    cis''8    a'8    d''8    d''4   
 \bar "|"   fis''4    fis''8    e''8    fis''8    a''8    g''8    e''8    
\bar "|"   fis''8    d''8    g''8    e''8    a''8    fis''8    g''8    e''8    
\bar "|"     fis''8    a''8    g''8    e''8    d''8    fis''8    e''8    d''8   
 } \alternative{{   d''8    cis''8    a'8    g'8    e'8    d'8    d'4    } {   
d''8    cis''8    a'8    g'8    e'8    fis'8    fis'8    e'8    \bar "||"     
d'2 ^"last time"   r2   \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
