\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "pchaffee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/248#setting12954"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/248#setting12954" {"https://thesession.org/tunes/248#setting12954"}}}
	title = "Tam Lin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   a8    d'8    d'16    d'16    d'8    a8    d'8    f'8  
  d'8  \bar "|"   bes4    d'8    bes8    f'8    bes8    d'8    bes8  \bar "|"   
c'8    e'8    e'4 ^"~"    c'8    e'8    g'8    e'8  \bar "|"   f'8    c''8    
a'8    f'8    e'4 ^"~"    d'8    c'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
