\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/281#setting25866"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/281#setting25866" {"https://thesession.org/tunes/281#setting25866"}}}
	title = "Master Crowley's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   cis''8    d''8  \bar "|"   e''8    a''8    gis''8    
a''8    fis''8    gis''8    e''8    gis''8    \bar "|"   fis''8    \tuplet 3/2 { 
  d''8    d''8    d''8  }   d''8    fis''8    d''8    a''8    d''8    \bar "|"  
 e''8    a''8    gis''8    a''8    fis''8    gis''8    e''4    \bar "|"   
cis''8    a'8    fis'8    d'8    e'4    e'4    \bar "|"     e''8    a''8    
gis''8    a''8    fis''8    gis''8    e''8    gis''8    \bar "|"   fis''8    
\tuplet 3/2 {   d''8    d''8    d''8  }   d''8    fis''8    d''8    a''8    d''8 
   \bar "|"   fis'8    a'8    b'8    cis''8    \tuplet 3/2 {   d''8    d''8    
d''8  }   d''4    \bar "|"   cis''8    a'8    fis'8    d'8    e'4    e'4    
\bar "|"     \repeat volta 2 {   e''8    \tuplet 3/2 {   e''8    e''8    e''8  } 
  e''8    cis''8    e''8    b'8    e''8    \bar "|"   e''8    \tuplet 3/2 {   
e''8    e''8    e''8  }   e''8    gis''8    e''8    a''8    e''8    \bar "|"   
e''8    \tuplet 3/2 {   e''8    e''8    e''8  }   e''8    cis''8    e''8    b'8  
  e''8    \bar "|"   gis''8    b''8    a''8    fis''8    gis''8    e''8    e''4 
   }   \key d \major   \repeat volta 2 {   a8    d'8    fis'8    d'8    a8    
d'8    fis'8    d'8    \bar "|"   e'8    d'8    e'8    fis'8    g'8    a'8    
b'4    \bar "|"   a8    d'8    fis'8    d'8    a8    d'8    fis'8    d'8    
} \alternative{{   e'8    d'8    cis'8    e'8    d'4    d'4    } {   e'8    d'8 
   cis'8    e'8    d'4    \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
