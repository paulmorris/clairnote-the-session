\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/248#setting248"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/248#setting248" {"https://thesession.org/tunes/248#setting248"}}}
	title = "Tam Lin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \minor   a4    d'8    a8    f'8    a8    d'8    a8  \bar "|"   
bes4    d'8    bes8    f'8    bes8    d'8    bes8  \bar "|"   c'4    e'8    c'8 
   g'8    c'8    e'8    c'8  \bar "|"   f'8    e'8    d'8    c'8    a8    d'8   
 d'8    c'8  \bar "|"     a4    d'8    a8    f'8    a8    d'8    a8  \bar "|"   
bes4    d'8    bes8    f'8    bes8    d'8    bes8  \bar "|"   c'4    e'8    c'8 
   g'8    c'8    e'8    c'8  \bar "|"   f'8    e'8    d'8    c'8    a8    d'8   
 d'4  }     \repeat volta 2 {   d''8    a'8    a'4 ^"~"    f'8    a'8    d'8    
a'8  \bar "|"   d''8    a'8    a'4 ^"~"    f'8    a'8    d'8    a'8  \bar "|"   
c''8    g'8    g'4 ^"~"    e'8    g'8    g'4 ^"~"  \bar "|"   c''8    g'8    
g'4 ^"~"    c''8    d''8    e''8    c''8  \bar "|"     d''8    a'8    a'4 ^"~"  
  f'8    a'8    d'8    a'8  \bar "|"   d''8    a'8    a'4 ^"~"    f'8    a'8    
d'8    a8  \bar "|"   bes4. ^"~"    a8    bes8    c'8    d'8    e'8  \bar "|"   
f'8    d'8    e'8    c'8    a8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
