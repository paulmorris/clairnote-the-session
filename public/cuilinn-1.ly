\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/469#setting469"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/469#setting469" {"https://thesession.org/tunes/469#setting469"}}}
	title = "Cuilinn"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'16    cis''16    d''8    b'8    b'8    a'8    fis'8 
   \bar "|"   a'8    d''8    d''8    d''8    fis'8    a'8    \bar "|"   b'16    
cis''16    d''8    b'8    b'8    a'8    fis'8    \bar "|"   a'8    fis'8    e'8 
   e'8    fis'8    a'8    \bar "|"     b'16    cis''16    d''8    b'8    b'8    
a'8    fis'8    \bar "|"   a'8    d''8    d''8    d''8    a'8    fis'8    
\bar "|"   g'4. ^"~"    fis'8    a'8    fis'8    \bar "|"   e'8    fis'8    e'8 
   d'8    fis'8    a'8    \bar ":|."   e'8    fis'8    e'8    d'4    r8   
\bar "||"     d'8    fis'8    a'8    d''4    e''8    \bar "|"   fis''8    d''8  
  fis''8    e''8    cis''8    a'8    \bar "|"   g''8    e''8    g''8    fis''8  
  d''8    b'8    \bar "|"   a'8    fis'8    a'8    b'4.    \bar "|"     a'8    
d''8    fis''8    a''4. ^"~"    \bar "|"   b''8    a''8    fis''8    a''4    
fis''8    \bar "|"   g''8    fis''8    e''8    fis''8    d''8    b'8    
\bar "|"   a'8    fis'8    a'8    b'4    g''8    \bar "|"     fis''8    d''8    
d''8    d''4    e''8    \bar "|"   fis''8    d''8    fis''8    e''8    cis''8   
 a'8    \bar "|"   g''4. ^"~"    fis''8    d''8    b'8    \bar "|"   a'8    
fis'8    a'8    b'4.    \bar "|"     a'8    d''8    fis''8    a''4. ^"~"    
\bar "|"   b''8    a''8    fis''8    a''4    fis''8    \bar "|"   g''8    
fis''8    e''8    fis''8    d''8    b'8    \bar "|"   a'8    fis'8    a'8    
b'4    r8   \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
