\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Christopher Reynolds"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/540#setting540"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/540#setting540" {"https://thesession.org/tunes/540#setting540"}}}
	title = "Iníon Ní Scannláin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   g'4.    a'8    a'8    g'8  \bar "|"   b'8    g'8    
e'8   ~    e'4 ^"~"    fis'8  \bar "|"   g'4    g'8    a'8    a'8    g'8  
\bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }   d''8    g'8    b'8    d''8  
\bar "|"     e''4.    g''8    fis''8    d''8  \bar "|"   d''8    e''8    e''8   
 d''8    \tuplet 3/2 {   b'8    a'8    g'8  } \bar "|"   a'4.    g'8    a'8    
b'8  \bar "|"   b'8    d''8    d''8    e''8    b'8    a'8  \bar ":|."   b'8    
d''8    d''8    g'8    \tuplet 3/2 {   b'8    c''8    d''8  } \bar "||"     
e''4.    a''8    a''8    b''8  \bar "|"   g''4    g''8    fis''8    g''8    
e''8  \bar "|"   d''4.    e''8    fis''8    g''8  \bar "|"   b'8    a'8    g'4  
  \tuplet 3/2 {   b'8    c''8    d''8  } \bar "|"     e''4.    a''8    a''8    
b''8  \bar "|"   g''4    g''8    d''8    e''16    fis''16    g''16    a''16  
\bar "|"   b''2.  \bar "|"   b''8    d''8    a''8    d''8    g''8    d''8  
\bar "|"     e''4.    g''8    fis''8    d''8  \bar "|"   e''4    e''8    d''8   
 \tuplet 3/2 {   b'8    a'8    g'8  } \bar "|"   a'4    a'8    g'8    a'8    b'8 
 \bar "|"   b'8    d''8    d''8    e''8    b'8    g'8  \bar "|"     a'4    a'8  
  b'8    a'8    g'8  \bar "|"   d'8    e'8    e'8    d'8    \tuplet 3/2 {   e'8  
  fis'8    g'8  } \bar "|"   a'4    a'8    g'8    a'8    b'8  \bar "|"   b'8    
d''8    d''8    e''8    b'8    a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
