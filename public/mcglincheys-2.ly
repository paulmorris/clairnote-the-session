\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/554#setting13518"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/554#setting13518" {"https://thesession.org/tunes/554#setting13518"}}}
	title = "McGlinchey's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   c''8    bes'8    a'8    c''8    bes'8    g'8    g'8  
  fis'8  \bar "|"   g'4    bes8    d'8    g'8    a'8    bes'8    g'8  \bar "|"  
 g'4 ^"~"    a'8    f'8    f'4 ^"~"    bes'8    c''8  \bar "|"   d''8    g''8   
 g''8    f''8    g''4    a''8    g''8  \bar "|"   f''8    d''8    d''4 ^"~"    
g''8    f''8    d''8    c''8  \bar "|"   bes'4. ^"~"    c''8    a'4.    c''8  
\bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   c''8    a'8    bes'8    g'8  
  a'8    c''8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8  
  g'4.    c''8  \bar "|"   c''8    bes'8    a'8    c''8    bes'8    g'8    d'8  
  bes8  \bar "|"   g4.    r8   c''8    bes'8    a'8    c''8  \bar "|"   bes'8   
 g'8    g'4 ^"~"    f'4 ^"~"    bes'8    c''8  \bar "|"   d''8    g''8    g''8  
  f''8    g''4    a''8    g''8  \bar "|"   f''8    d''8    d''4 ^"~"    g''8    
f''8    d''8    c''8  \bar "|"   bes'4. ^"~"    c''8    a'4.    c''8  \bar "|" 
\tuplet 3/2 {   bes'8    c''8    d''8  }   c''8    a'8    bes'8    g'8    a'8    
c''8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8    g'4    
bes'8    c''8  \bar "||"   d''8    bes'8    c''8    a'8    bes'8    d''8    
c''8    a'8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8    
g'4    a'8    g'8  \bar "|"   f'4. ^"~"    g'8    f'8    c'8    c'4 ^"~"  
\bar "|"   f'4. ^"~"    g'8    a'8    c''8    bes'8    a'8  \bar "|"   g'8    
a'8    bes'8    c''8    d''8    c''8    c''4 ^"~"  \bar "|"   d''8    bes'8    
a'8    g'8    a'8    f'8    f'4 ^"~"  \bar "|"   g'4. ^"~"    a'8    g'8    a'8 
   bes'8    c''8  \bar "|"   d''8    g''8    fis''8    a''8    g''4    bes'8    
c''8  \bar "|"   \tuplet 3/2 {   d''8    c''8    bes'8  }   c''8    a'8    bes'8 
   d''8    c''8    a'8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8  
  fis'8    g'4    a'8    g'8  \bar "|"   f'4. ^"~"    g'8    f'8    c'8    c'4 
^"~"  \bar "|"   f'4. ^"~"    g'8    a'8    c''8    bes'8    a'8  \bar "|"   
g'8    a'8    bes'8    c''8    d''8    c''8    c''4 ^"~"  \bar "|"   d''8    
bes'8    a'8    g'8    a'8    f'8    f'4 ^"~"  \bar "|"   g'4. ^"~"    a'8    
g'8    a'8    bes'8    c''8  \bar "|"   d''8    g''8    fis''8    a''8    g''4. 
   bes'8  \bar "||"   d''8    g''8    g''4 ^"~"    d''8    g''8    fis''8    
g''8  \bar "|"   d''8    g''8    g''4 ^"~"    d''4    c''8    bes'8  \bar "|"   
c''8    f''8    f''4 ^"~"    c''8    f''8    e''8    f''8  \bar "|"   g''8    
f''8    c''8    a'8    f'4    bes'8    c''8  \bar "|"   d''4. ^"~"    bes'8    
c''4. ^"~"    a'8  \bar "|"   bes'8    a'8    bes'8    g'8    a'8    f'8    d'8 
   c'8  \bar "|"   bes8    d'8    g'8    a'8    \tuplet 3/2 {   bes'8    c''8    
d''8  }   c''8    a'8  \bar "|"   bes'8    d''8    a'8    fis'8    g'4.    
bes'8  \bar "|"   d''8    g''8    g''4 ^"~"    a''8    g''8    fis''8    g''8  
\bar "|"   d''8    g''8    g''4 ^"~"    d''4    c''8    bes'8  \bar "|"   c''8  
  f''8    f''4 ^"~"    c''8    f''8    e''8    f''8  \bar "|"   g''8    f''8    
c''8    a'8    f'4    bes'8    c''8  \bar "|"   d''4. ^"~"    bes'8    c''4. 
^"~"    a'8  \bar "|"   bes'8    a'8    bes'8    g'8    a'8    f'8    d'8    
c'8  \bar "|"   bes8    d'8    g'8    a'8    \tuplet 3/2 {   bes'8    c''8    
d''8  }   c''8    a'8  \bar "|"   bes'8    d''8    a'8    fis'8    g'4.    r8 
\bar "||"   c''8    bes'8    a'8    c''8    bes'8    g'8    g'8    fis'8  
\bar "|"   g'8    g8    bes8    d'8    g'8    a'8    bes'8    g'8  \bar "|"   
g'4 ^"~"    a'8    f'8    f'4 ^"~"    bes'8    c''8  \bar "|"   d''8    g''8    
g''8    f''8    g''4    a''8    g''8  \bar "|"   f''8    d''8    d''4 ^"~"    
g''8    f''8    d''8    c''8  \bar "|"   bes'4. ^"~"    c''8    a'4.    c''8  
\bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   c''8    a'8    bes'8    g'8  
  a'8    c''8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8  
  g'4.    c''8  \bar "|"   c''8    bes'8    a'8    c''8    bes'8    g'8    d'8  
  bes8  \bar "|"   g4.    r8   c''8    bes'8    a'8    c''8  \bar "|"   bes'8   
 g'8    g'4 ^"~"    f'4 ^"~"    bes'8    c''8  \bar "|"   d''8    g''8    g''8  
  f''8    g''4    a''8    g''8  \bar "|"   f''8    d''8    d''4 ^"~"    g''8    
f''8    d''8    c''8  \bar "|"   bes'4. ^"~"    c''8    a'4.    c''8  \bar "|" 
\tuplet 3/2 {   bes'8    c''8    d''8  }   c''8    a'8    bes'8    g'8    a'8    
c''8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8    g'4    
bes'8    c''8  \bar "||"   d''8    bes'8    c''8    a'8    bes'8    d''8    
c''8    a'8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }   a'8    fis'8    
g'4    a'8    g'8  \bar "|"   f'4. ^"~"    g'8    f'8    c'8    c'4 ^"~"  
\bar "|"   f'4. ^"~"    g'8    a'8    c''8    bes'8    a'8  \bar "|"   g'8    
a'8    bes'8    c''8    d''8    c''8    c''4 ^"~"  \bar "|"   d''8    bes'8    
a'8    g'8    a'8    f'8    f'4 ^"~"  \bar "|"   g'4. ^"~"    a'8    g'8    a'8 
   bes'8    c''8  \bar "|"   d''8    g''8    \tuplet 3/2 {   fis''8    g''8    
a''8  }   g''4    bes'8    c''8  \bar "|"   d''8    bes'8    c''8    a'8    
bes'8    d''8    c''8    a'8  \bar "|" \tuplet 3/2 {   bes'8    c''8    d''8  }  
 a'8    fis'8    g'4    a'8    g'8  \bar "|"   f'4. ^"~"    g'8    f'8    c'8   
 c'4 ^"~"  \bar "|"   f'4. ^"~"    g'8    a'8    c''8    bes'8    a'8  \bar "|" 
  g'8    a'8    bes'8    c''8    d''8    c''8    c''4 ^"~"  \bar "|"   d''8    
bes'8    a'8    g'8    a'8    f'8    f'4 ^"~"  \bar "|"   g'4. ^"~"    a'8    
g'8    a'8    bes'8    c''8  \bar "|"   d''8    g''8    fis''8    a''8    g''4  
  bes'8    c''8  \bar "||"   d''8    g''8    g''4 ^"~"    g''8    fis''8    
fis''8    g''8  \bar "|"   d''8    g''8    g''4 ^"~"    d''4    c''8    bes'8  
\bar "|"   c''8    f''8    f''4 ^"~"    c''8    f''8    e''8    f''8  \bar "|"  
 g''8    f''8    c''8    a'8    f'4    bes'8    c''8  \bar "|"   d''4. ^"~"    
bes'8    c''4. ^"~"    a'8  \bar "|"   bes'8    a'8    bes'8    g'8    a'8    
f'8    d'8    c'8  \bar "|"   bes8    d'8    g'8    a'8    \tuplet 3/2 {   bes'8 
   c''8    d''8  }   c''8    a'8  \bar "|"   bes'8    d''8    a'8    fis'8    
g'4    bes'8    c''8  \bar "|"   d''8    g''8    g''4 ^"~"    a''8    g''8    
fis''8    g''8  \bar "|"   d''8    g''8    g''4 ^"~"    d''4    c''8    bes'8  
\bar "|"   c''8    f''8    f''4 ^"~"    c''8    f''8    e''8    f''8  \bar "|"  
 g''8    f''8    c''8    a'8    f'4    bes'8    c''8  \bar "|"   d''4. ^"~"    
bes'8    c''4. ^"~"    a'8  \bar "|"   bes'8    a'8    bes'8    g'8    a'8    
f'8    d'8    c'8  \bar "|"   bes8    d'8    g'8    a'8    \tuplet 3/2 {   bes'8 
   c''8    d''8  }   c''8    a'8  \bar "|"   d''8    r4 fis'8    g'2  \bar "||" 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
