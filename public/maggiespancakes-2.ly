\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1316#setting14652"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1316#setting14652" {"https://thesession.org/tunes/1316#setting14652"}}}
	title = "Maggie's Pancakes"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key b \minor   fis''8    b'8    b'4 ^"~"    fis''8    g''8    fis''8 
   e''8  \bar "|"   d''8    b'8    a'8    b'8    g'8    b'8    d''8    b'8  
\bar "|"   cis''8    a'8    a'8    d''8    a'8    a'8    e''8    a'8  \bar "|"  
 a'8    a''8    e''8    cis''8    a'8    cis''8    e''8    g''8  \bar "|"   
fis''8    b'8    b'4 ^"~"    fis''8    g''8    fis''8    e''8  \bar "|"   d''8  
  b'8    a'8    b'8    g'8    b'8    d''8    b'8  \bar "|"   cis''8    a'8    
b'8    cis''8    d''8    e''8    fis''8    gis''8  \bar "|"   a''8    fis''8    
e''8    cis''8    b'4.    e''8  }   \repeat volta 2 {   fis''4    d''8    b'8   
 g'8    b'8    d''8    b'8  \bar "|"   cis''8    a''8    e''8    cis''8    d''8 
   b'8    b'4 ^"~"  \bar "|"   fis''4    d''8    b'8    g'8    b'8    d''8    
b'8  \bar "|"   cis''8    a''8    e''8    cis''8    b'8    cis''8    d''8    
e''8  \bar "|"   fis''8    d''8    d''4 ^"~"    fis''8    g''8    fis''8    
e''8  \bar "|"   d''8    b'8    a'8    b'8    g'8    b'8    d''8    b'8  
\bar "|"   cis''8    a'8    b'8    cis''8    d''8    e''8    fis''8    gis''8  
\bar "|"   a''8    fis''8    e''8    cis''8    b'4.    e''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
