\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/714#setting714"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/714#setting714" {"https://thesession.org/tunes/714#setting714"}}}
	title = "Dr. MacPhail's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \mixolydian   cis''4    e''8    a'8    a'4    fis''8    e''8   
 \bar "|"   fis''8    a''8    e''8    fis''8    a''8    e''8    fis''8    e''8  
  \bar "|"   cis''4    e''8    fis''8    a'8    cis''8    fis''8    e''8    
\bar "|"   fis''8    a''8    e''8    fis''8    a''8    a'8    cis''8    b'8    
}     cis''8.    e''16    e''8    a''8    cis''8    e''8    a''8    fis''8    
\bar "|"   e''8    cis''8    cis''8    e''8    fis''16    a''8.    fis''8    
e''8    \bar "|"   cis''8    e''8    e''8    a''8    cis''8    e''8    a''8    
e''8    \bar "|"   fis''8    e''8    e''8    fis''8    a''8    a'8    cis''8    
b'8    \bar "|"     cis''8    e''8    e''8    a''8    cis''8    e''8    a''8    
fis''8    \bar "|"   e''8    cis''8    cis''8    e''8    a''4    fis''8    e''8 
   \bar "|"   cis''4    e''8    a'8    a'8    cis''8    fis''8    e''8    
\bar "|"   fis''16    a''8.    e''8    fis''8    \bar "|"   a''8    a'8    
cis''8    b'8    \bar "|"     \repeat volta 2 {   a'8    e''8    e''8    cis''8 
   e''8    fis''8    cis''8    e''8    \bar "|"   b'8    fis''8    fis''8    
e''8    fis''16    a''8.    fis''8    e''8    \bar "|"   a'8    e''8    e''8    
cis''8    e''8    fis''8    cis''8    e''8    \bar "|"   fis''16    g''16    
a''8    e''8    fis''8    a''8    a'8    cis''8    b'8    }     
\repeat volta 2 {   cis''4    e''8    a''8    d''8    fis''8    cis''8    e''8  
  \bar "|"   d''4    fis''16    a''8.    e''16    a''8.    fis''8    e''8    
} \alternative{{   cis''4    e''8    a''8    d''8    fis''8    cis''8    e''8   
 \bar "|"     fis''16    a''8.    e''8    fis''8    a''8    a'8    cis''8    
b'8    } {   cis''4    e''8    a'8    a'4    fis''8    e''8    \bar "|"   
fis''16    a''8.    e''8    fis''8    a''8    a'8    cis''8    b'8    \bar "||" 
  }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
