\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mix O'Lydian"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/666#setting26148"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/666#setting26148" {"https://thesession.org/tunes/666#setting26148"}}}
	title = "Alexander's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   \tuplet 3/2 {   a'8    b'8    
cis''8  }   \bar "|"   d''8.    a'16    fis'8.    a'16    d'8.    fis'16    
a'8.    d''16    \bar "|"   fis''8.    d''16    cis''8.    d''16    fis'8.    
a'16    d''8.    fis''16    \bar "|"   \tuplet 3/2 {   g''8    g''8    g''8  }   
g''8.    e''16    \tuplet 3/2 {   fis''8    fis''8    fis''8  }   fis''8.    
d''16    \bar "|"   \tuplet 3/2 {   e''8    fis''8    e''8  }   \tuplet 3/2 {   
d''8    cis''8    b'8  }   a'4    \tuplet 3/2 {   a'8    b'8    cis''8  }   
\bar "|"     d''8.    a'16    fis'8.    a'16    d'8.    fis'16    a'8.    d''16 
   \bar "|"   fis''8.    d''16    cis''8.    d''16    fis'8.    a'16    d''8.   
 fis''16    \bar "|"   \tuplet 3/2 {   g''8    g''8    g''8  }   g''8.    e''16  
  \tuplet 3/2 {   fis''8    fis''8    fis''8  }   fis''8.    d''16    \bar "|"   
\tuplet 3/2 {   e''8    fis''8    e''8  }   d''8.    cis''16    d''4    }     
\repeat volta 2 {   a'8.    g'16    \bar "|"   fis'8.    a'16    d''8.    a'16  
  fis'8.    a'16    d''8.    a'16    \bar "|"   g'8.    b'16    d''8.    b'16   
 g'8.    b'16    d''8.    b'16    \bar "|"   a'8.    cis''16    e''8.    
cis''16    a'8.    cis''16    e''8.    cis''16    \bar "|"   d''8.    fis''16   
 a''8.    fis''16    \tuplet 3/2 {   g''8    fis''8    e''8  }   cis''8.    a'16 
   \bar "|"     fis'8.    a'16    d''8.    a'16    fis'8.    a'16    d''8.    
a'16    \bar "|"   g'8.    b'16    d''8.    b'16    g'8.    b'16    d''8.    
b'16    \bar "|"   a'8.    cis''16    e''8.    fis''16    \tuplet 3/2 {   g''8   
 fis''8    e''8  }   cis''8.    d''16    \bar "|"   \tuplet 3/2 {   e''8    
fis''8    e''8  }   d''8.    cis''16    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
