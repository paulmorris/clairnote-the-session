\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Daver"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/753#setting753"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/753#setting753" {"https://thesession.org/tunes/753#setting753"}}}
	title = "March Hare, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \minor   \repeat volta 2 { \tuplet 3/2 {   d'8    ees'8    
fis'8  } \bar "|"   g'4    d''4    d''4    c''8    d''8  \bar "|"   bes'4    
a'8    g'8    f'8    g'8    a'8    f'8  \bar "|"   g'8    f'8    g'8    a'8    
bes'8    a'8    bes'8    c''8  \bar "|"   d''8    ees''8    f''8    ees''8    
d''8    c''8    \tuplet 3/2 {   bes'8    a'8    g'8  } \bar "|"     d'4    g'4   
 g'4    g'8    a'8  \bar "|"   bes'4    a'8    g'8    f'8    g'8    
\tuplet 3/2 {   a'8    g'8    f'8  } \bar "|"   g'8    a'8    \tuplet 3/2 {   
bes'8    a'8    g'8  }   f'8    g'8    \tuplet 3/2 {   a'8    g'8    f'8  } 
\bar "|"   g'4    g'4    g'2  }     \repeat volta 2 {   bes'4    bes'8    c''8  
  d''4    d''8    ees''8  \bar "|"   f''4    f''8    ees''8    d''8    c''8    
\tuplet 3/2 {   bes'8    a'8    g'8  } \bar "|"   f'4    f'8    g'8    a'4    
a'8    bes'8  \bar "|"   c''4    c''8    bes'8    a'8    g'8    f'4  \bar "|"   
  g'4    g'8    a'8    bes'4    bes'8    c''8  \bar "|"   d''4    d''8    c''8  
  bes'8    g'8    a'4  \bar "|"   bes'8    c''8    \tuplet 3/2 {   d''8    c''8  
  bes'8  }   a'8    bes'8    \tuplet 3/2 {   c''8    bes'8    a'8  } \bar "|"   
g'4    g'4    g'2  }     \repeat volta 2 {   c''4    c''4    c''4    bes'8    
g'8  \bar "|"   f'4    f'8    g'8    f'8    ees'8    c'4  \bar "|"   c''4    
c''4    c''4    c''8    d''8  \bar "|"   ees''4    d''8    c''8    bes'8    
c''8    \tuplet 3/2 {   d''8    c''8    bes'8  } \bar "|"     c''4    c''4    
c''4    bes'8    g'8  \bar "|"   f'4    f'8    g'8    f'8    ees'8    c'8    
d'8  \bar "|"   ees'8    f'8    \tuplet 3/2 {   g'8    f'8    ees'8  }   d'8    
ees'8    \tuplet 3/2 {   f'8    ees'8    d'8  } \bar "|"   c'4    c'4    c'2  }  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
