\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bsykes62"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/640#setting640"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/640#setting640" {"https://thesession.org/tunes/640#setting640"}}}
	title = "Loftus Jones"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   a''2    gis''4    fis''4  
\bar "|"   e''4    d''4    cis''8    d''8    e''4  \bar "|"   cis''8    d''8    
b'8    a'8    a'4    r4   \bar "|"   cis''8    d''8    cis''8    b'8    a'4    
b'4  \bar "|"     cis''8    d''8    e''4    d''8    e''8    fis''4  \bar "|"   
e''4    d''8    cis''8    d''8    cis''8    b'8    a'8  \bar "|"   gis'8    a'8 
   gis'8    fis'8    e'4    r4 \bar "|"   a'8    b'8    a'8    gis'8    fis'4   
 d''4  \bar "|"     e''8    d''8    e''8    cis''8    d''8    cis''8    d''8    
b'8  \bar "|"   a'4    e'4    fis'4    gis'4  \bar "|"   a'4    fis'4    gis'4  
  a'4  \bar "|"   b'4    gis'4    a'4    b'4  \bar "|"   cis''4    a'4    b'4   
 cis''4  \bar "|"     d''2    e''4    r4 \bar "|"   d''8    cis''8    b'8    
a'8    gis'8    a'8    b'4  \bar "|"   a'4    e'8    d'8    cis'4    e'4  
\bar "|"   a'2    a'4    r4   }     \repeat volta 2 {   e''4    gis'4    a'4    
fis'4  \bar "|"   gis'4    e'4    fis'4    gis'4  \bar "|"   a'4    b'4    
gis'4    cis''4  \bar "|"   fis'2    gis'2  \bar "|"     cis''4    cis''4    
b'2  \bar "|"   a'8    b'8    cis''8    d''8    cis''4    b'4  \bar "|"   a'8   
 b'8    cis''8    d''8    e''8    d''8    cis''8    b'8  \bar "|"   a'4    
gis'4    fis'2  \bar "|"     cis'8    fis'8    a'8    fis'8    cis'8    fis'8   
 a'8    fis'8  \bar "|"   e'8    gis'8    e'8    gis'8    e'8    gis'8    e'8   
 gis'8  \bar "|"   fis'8    a'8    fis'8    a'8    fis'8    a'8    fis'8    a'8 
 \bar "|"   gis'8    b'8    gis'8    b'8    gis'8    b'8    gis'8    b'8  
\bar "|"     a'8    cis''8    a'8    cis''8    a'8    cis''8    a'8    cis''8  
\bar "|"   a'8    cis''8    a'8    cis''8    a'8    cis''8    a'8    cis''8  
\bar "|"   fis''8    gis''8    a''4    gis''2  \bar "|"   fis''2    fis''4    
gis''4  \bar "|"     a''8    a'8    a''8    a'8    gis''8    a'8    gis''8    
a'8  \bar "|"   fis''8    a'8    fis''8    a'8    e''8    a'8    e''8    a'8  
\bar "|"   d''8    b'8    d''8    b'8    cis''8    a'8    cis''8    a'8  
\bar "|"   b'8    a'8    gis'8    fis'8    e'8    fis'8    gis'4  \bar "|"     
a'4    e'8    d'8    cis'4    e'4  \bar "|"   b'8    a'8    gis'8    fis'8    
e'4    e''4  \bar "|"   d''8    cis''8    b'8    a'8    gis'8    a'8    b'8    
gis'8  \bar "|"   a'2    a'4    r4   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
