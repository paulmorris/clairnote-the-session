\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1244#setting1244"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1244#setting1244" {"https://thesession.org/tunes/1244#setting1244"}}}
	title = "Condon's Frolics"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \bar "|"   e''8    a'8    b'8    c''4    d''8  
\bar "|"   e''8    d''8    c''8    b'8    a'8    g'8  \bar "|"   e''8    a'8    
b'8    c''4    d''8  \bar "|"   e''4    d''8    e''8    a''8    g''8  \bar "|"  
   e''8    a'8    b'8    c''4    d''8  \bar "|"   e''8    d''8    c''8    b'4. 
^"~"  \bar "|"   g'8    b'8    d''8    g''8    d''8    b'8  \bar "|"   a'4. 
^"~"    a'8    b'8    d''8  \bar ":|."   a'4. ^"~"    a'4    d''8  \bar "||"    
 \bar ".|:"   e''8    a''8    a''8    e''8    fis''8    g''8  \bar "|"   d''8    
e''8    c''8    b'8    a'8    g'8  \bar "|"   c''8    b'8    c''8    d''8    
c''8    d''8  \bar "|"   e''4    d''8    e''8    fis''8    g''8  \bar "|"     
e''8    a''8    a''8    e''8    fis''8    g''8  \bar "|"   d''8    e''8    c''8 
   b'8    a'8    b'8  \bar "|"   g'8    b'8    d''8    g''8    d''8    b'8  
\bar "|"   a'4. ^"~"    a'4    d''8  \bar ":|."   a'4. ^"~"    a'8    b'8    
d''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
