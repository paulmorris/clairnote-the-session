\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "PipersWineFiddler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/120#setting12725"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/120#setting12725" {"https://thesession.org/tunes/120#setting12725"}}}
	title = "Maids Of Mitchelstown, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   \repeat volta 2 {   f'4.    d'8    e'2  \bar "|"   
d'8    f'8    e'8    d'8    c'8    a8    a4  \bar "|"   f'4.    d'8    e'4    
e'8    f'8  \bar "|"   g'8    a'8    g'8    e'8    e'8    d'8    d'4  }   
\repeat volta 2 {   c'4    a'8    g'8    g'2  \bar "|"   a'8    g'8    fis'8    
a'8    g'2  \bar "|"   e'8    c'8    d'8    a8    c'4.    c'8  \bar "|"   bes8  
  c'8    d'8    e'8    a8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
