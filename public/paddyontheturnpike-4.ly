\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/338#setting22051"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/338#setting22051" {"https://thesession.org/tunes/338#setting22051"}}}
	title = "Paddy On The Turnpike"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \dorian   f'8  \bar "|"   d'8 ^"Gm"   g'8    g'8    fis'8    
g'4    g'8    a'8  \bar "|"   b'8 ^"G"   g'8    d''8    g'8    e''8    g'8    
d''8    g'8  \bar "|"   a'8 ^"F"   f'8  \grace {    g'8  }   f'8    e'8    f'4  
  f'8    g'8  \bar "|"   a'8 ^"F"   f'8    c''8    f'8    d''8    f'8    c''8   
 f'8  \bar "|"       d'8 ^"Gm"   g'8    g'8    fis'8    g'4    g'8    a'8  
\bar "|"   bes'8 ^"Gm"   a'8    g'8    a'8    bes'8    c''8    d''8    e''8  
\bar "|"   f''8 ^"Dm"   g''8    f''8    d''8      c''8 ^"F"   a'8    f'8    a'8 
 \bar "|"   bes'8 ^"Gm"   g'8    a'8 ^"D"   fis'8      g'4. ^"Gm" }     
\repeat volta 2 {   c''8  \bar "|"   d''8 ^"Gm"   g''8    g''8    fis''8    
g''4    g''8    a''8  \bar "|"   bes''8 ^"Gm"   g''8    d''8    g''8    bes''8  
  g''8    a''8 ^"Dm"   f''8  \bar "|"   c''8 ^"F"   f''8    f''8    g''8    
f''4    c''8    f''8  \bar "|"   a''8 ^"F"   f''8    c''8    f''8    a''8    
f''8    g''8    f''8  \bar "|"       d''8 ^"Gm"   g''8    g''8    fis''8    
g''4    g''8    a''8  \bar "|"   bes''8 ^"Gm"   a''8    g''8    f''8    d''8    
c''8    d''8    e''8  \bar "|"   f''8 ^"Dm"   g''8    f''8    d''8      c''8 
^"F"   a'8    f'8    a'8  \bar "|"   bes'8 ^"Gm"   g'8    a'8 ^"D"   fis'8      
g'4. ^"Gm" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
