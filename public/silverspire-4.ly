\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "kiwifolk"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/240#setting23292"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/240#setting23292" {"https://thesession.org/tunes/240#setting23292"}}}
	title = "Silver Spire, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d'4    fis'8    d'8    a8    d'8    fis'8    a'8  
\bar "|"   d''8    cis''8    d''8    e''8    fis''8    d''8    a'8    fis'8  
\bar "|"   g'4    b'8    g'8    fis'8    a'8    d'8    fis'8  \bar "|" 
\tuplet 3/2 {   g'8    fis'8    e'8  }   fis'8    d'8    cis'8    e'8    a8    
cis'8  \bar "|"     \bar "|"   d'4    fis'8    d'8    a8    d'8    fis'8    a'8 
 \bar "||"   d''8    cis''8    d''8    e''8    fis''8    d''8    a'8    fis'8  
\bar "|"   g'8    a'8    b'8    g'8    fis'8    a'8    d'8    fis'8  \bar "|"   
e'8    a8    cis'8    e'8    d'4    \tuplet 3/2 {   a8    b8    cis'8  } 
\bar ":|."   e'8    a8    cis'8    e'8    d'4    cis'8    b8  \bar "||"     
\bar ".|:"   a8    b8    cis'8    d'8    e'8    fis'8    g'8    e'8  \bar "|"   
fis'8    d'8    e'8    fis'8    g'8    a'8    b'8    cis''8  \bar "|"   d''8    
b'8    b'8    a'8    b'8    cis''8    d''8    e''8  \bar "|"   fis''8    d''8   
 g''8    fis''8    e''8    a'8    cis''8    e''8  \bar "|"     \bar "|"   d''4  
  fis''8    d''8    cis''8    e''8    a'8    cis''8  \bar "|"   d''8    cis''8  
  d''8    b'8    a'8    fis'8    d'8    fis'8  \bar "|"   g'8    a'8    b'8    
g'8    fis'8    a'8    d'8    fis'8  \bar "|"   e'8    a8    cis'8    e'8    
d'4    cis'8    b8  \bar ":|."   e'8    a8    cis'8    e'8    d'4    
\tuplet 3/2 {   a8    b8    cis'8  } \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
