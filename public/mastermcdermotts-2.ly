\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "PJ Mediterranean"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/682#setting13730"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/682#setting13730" {"https://thesession.org/tunes/682#setting13730"}}}
	title = "Master McDermott's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d'8    fis'8    fis'4 ^"~"    d'8    fis'8    a'8    
b'8  \bar "|"   d''8    fis''8    fis''8    e''8    d''4    b'8    d''8  
\bar "|"   a'8    fis'8    fis'4 ^"~"    d'8    fis'8    fis'4 ^"~"  \bar "|"   
b'4    a'8    fis'8    e'8    a'8    fis'8    e'8  \bar "|"   d'8    fis'8    
fis'4 ^"~"    d'8    fis'8    a'8    b'8  \bar "|"   d''8    fis''8    fis''8   
 e''8    d''4    b'8    d''8  \bar "|"   a'8    fis'8    fis'4 ^"~"    b'4    
a'8    fis'8  \bar "|"   e'8    a'8    fis'8    e'8    d'2  \bar ":|."   e'8    
a'8    fis'8    e'8    d'4    a'8    b'8  \bar "||"   d''8    fis''8    fis''4 
^"~"    d''4    a''8    fis''8  \bar "|"   g''4. ^"~"    b''8    a''8    fis''8 
   fis''8    e''8  \bar "|"   d''4    b'8    a'8    fis'8    a'8    d''8    
e''8  \bar "|"   fis''4. ^"~"    a''8    e''8    fis''8    d''4  \bar "|"   
fis''8    a''8    a''8    fis''8    g''4    g''8    b''8  \bar "|"   a''8    
fis''8    fis''8    e''8    d''4    b'8    d''8  \bar "|"   a'8    fis'8    
fis'4 ^"~"    b'8    fis'8    a'8    fis'8  \bar "|"   e'8    a'8    fis'8    
e'8    d'4    a'8    b'8  \bar ":|."   e'8    a'8    fis'8    e'8    d'2  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
