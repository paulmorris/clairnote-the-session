\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/787#setting13921"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/787#setting13921" {"https://thesession.org/tunes/787#setting13921"}}}
	title = "Sleepy Maggie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   \repeat volta 2 {   e''4    a'8    a''8    e''8    
d''8    bes'16    d''8.    \bar "|"   e''4    g''4    bes'16    g'8.    bes'16  
  d''8.    \bar "|"   e''4    a'8    a''8    e''8    d''8    bes'16    d''8.    
\bar "|"   e''8.    f''16    g''8.    e''16    d''16    g'8.    bes'16    d''8. 
   }   \repeat volta 2 {   e''8    a'8    a'4    e''8    a'8    bes'16    d''8. 
   \bar "|"   e''8    a'8    a'4    bes'16    g'8.    bes'16    d''8.    
\bar "|"   e''8    a'8    a'4    e''8    a'8    bes'16    d''8.    \bar "|"   
e''8.    f''16    g''8.    e''16    d''16    g'8.    bes'16    d''8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
