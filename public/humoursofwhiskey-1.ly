\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1023#setting1023"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1023#setting1023" {"https://thesession.org/tunes/1023#setting1023"}}}
	title = "Humours Of Whiskey, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \minor   b'4    a'8    b'8    e'8    e'8    b'8    e'8    e'8  
\bar "|"   b'4    a'8    b'8    g'8    e'8    fis'8    e'8    d'8  \bar "|"   
b'4    a'8    b'8    e'8    e'8    b'8    e'8    e'8  \bar "|"   d'8    fis'8   
 g'8    a'4    g'8    fis'8    e'8    d'8  \bar "|"     b'4    a'8    b'8    
e''8    e''8    b'4. ^"~"  \bar "|"   b'4    a'8    b'8    e''8    g''8    
fis''8    e''8    d''8  \bar "|"   b'4    a'8    b'8    e'8    e'8    b'8    
e'8    e'8  \bar "|"   d'8    fis'8    g'8    a'4    g'8    fis'8    e'8    d'8 
 \repeat volta 2 {     g'4    a'8    b'4    a'8    b'8    c''8    a'8  \bar "|" 
  g'4. ^"~"    b'4    d''8    e''8    a'8    a'8  \bar "|"   g'4    a'8    b'4  
  a'8    b'4    d''8  \bar "|"   e''8    a'8    a'8    a'4    g'8    fis'8    
e'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
