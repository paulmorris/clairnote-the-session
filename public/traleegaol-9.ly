\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1113#setting22746"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1113#setting22746" {"https://thesession.org/tunes/1113#setting22746"}}}
	title = "Tralee Gaol"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   e''8    a'8    a'8    b'16    
d''16    \bar "|"   e''8.    d''16    b'8    d''8    \bar "|"   e''8    a'8    
a'8    b'16    a'16    \bar "|"   g'8.    a'16    b'8    d''8    \bar "|"     
e''8    a'8    a'8    b'16    d''16    \bar "|"   e''8.    d''16    b'8    d''8 
   \bar "|"   e''8    d''8    g''8    b'8    \bar "|"   a'4    a'4    }     
\repeat volta 2 {   e''8    a''8    a''8    g''16    a''16    \bar "|"   b''8.  
  a''16    g''8    e''8    \bar "|"   d''8    b'8    g''8    b'8    \bar "|"   
d''16    c''16    b'16    a'16    g'8    e'8    \bar "|"     e''8    a''8    
a''8    g''16    a''16    \bar "|"   b''8.    a''16    g''8    e''8    \bar "|" 
  e''8    d''8    g''8    b'8    \bar "|"   a'4    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
