\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Frank_Flute"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1194#setting14479"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1194#setting14479" {"https://thesession.org/tunes/1194#setting14479"}}}
	title = "Lads Of Alnwick, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/2 \key g \major   g''2    d''4    e''8    fis''8    g''8    fis''8    
e''8    d''8  \bar "|"   b'4    g'4    g''4    g'4    b'8    c''8    d''8    
b'8  \bar "|"   g''2    d''4    e''8    fis''8    g''8    fis''8    e''8    
d''8  \bar "|"   c''4    a'4    e''4    a'4    c''4    e''4  
} \repeat volta 2 {   g'8    a'8    b'8    c''8    d''4    b'4    d''4    b'4  
\bar "|"   g'8    a'8    b'8    c''8    d''4    b'4    d''4    g''4  \bar "|"   
g'8    a'8    b'8    c''8    d''4    b'4    d''4    b'4  \bar "|"   a'8    b'8  
  c''8    d''8    e''4    a'4    c''4    e''4  } \repeat volta 2 {   d''4    
g''4    b'8    c''8    d''8    b'8    g''4    b'4  \bar "|"   d''4    g''4    
b'8    c''8    d''8    b'8    c''4    e''4  \bar "|"   d''4    g''4    b'8    
c''8    d''8    b'8    g''4    b'4  \bar "|"   a'8    b'8    c''8    d''8    
e''4    a'4    c''4    e''4  } \repeat volta 2 {   g'8    a'8    b'8    c''8    
d''8    e''8    d''8    c''8    b'8    c''8    d''8    b'8  \bar "|"   d''8    
e''8    d''8    b'8    b'8    c''8    d''8    b'8    d''4    g''4  \bar "|"   
g'8    a'8    b'8    c''8    d''8    e''8    d''8    c''8    b'8    c''8    
d''8    b'8  \bar "|"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
