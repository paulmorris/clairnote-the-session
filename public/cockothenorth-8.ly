\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1218#setting24582"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1218#setting24582" {"https://thesession.org/tunes/1218#setting24582"}}}
	title = "Cock O' The North"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   e'8    \bar "|"   fis'4    fis'8  
  fis'8    e'8    d'8    \bar "|"   fis'4    a'8    b'4    a'8    \bar "|"   
fis'4    fis'8    fis'8    e'8    d'8    \bar "|"   e'4.    e'8    d'8    e'8   
 \bar "|"     fis'4.    fis'8    e'8    d'8    \bar "|"   fis'4    a'8    b'4   
 a'8    \bar "|"   fis'8    g'8    fis'8    e'8    fis'8    e'8    \bar "|"   
d'4.    d'4    }     \repeat volta 2 {   a'8    \bar "|"   d''4    a'8    b'4   
 a'8    \bar "|"   d''4    a'8    b'4    a'8  \bar "|"   fis'8    g'8    fis'8  
  fis'8    e'8    d'8    \bar "|"   e'4.    e'8    fis'8    a'8    \bar "|"     
d''4    a'8    b'4    a'8    \bar "|"   d''4    a'8    b'4    a'8    \bar "|"   
fis'8    g'8    fis'8    e'8    fis'8    e'8    \bar "|"   d'4.    d'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
