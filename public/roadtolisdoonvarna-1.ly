\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kerri Coombs"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/250#setting250"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/250#setting250" {"https://thesession.org/tunes/250#setting250"}}}
	title = "Road To Lisdoonvarna, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key e \dorian   e'4    b'8    b'4    a'8    b'4    cis''8    d''4   
 a'8  \bar "|"   fis'4    a'8    a'8    b'8    a'8    d'4    e'8    fis'8    
e'8    d'8  \bar "|"   e'4    b'8    b'4    a'8    b'4    cis''8    d''4.  
\bar "|"   cis''8    d''8    cis''8    b'4    a'8    b'4    e'8    e'4.  }     
\repeat volta 2 {   e''4    fis''8    g''8    fis''8    e''8    d''4    b'8    
b'8    cis''8    d''8  \bar "|"   cis''4    a'8    a'8    b'8    cis''8    d''4 
   b'8    b'4.  \bar "|"   e''4    fis''8    g''8    fis''8    e''8    d''4    
b'8    b'8    cis''8    d''8  \bar "|"   cis''8    d''8    cis''8    b'4    a'8 
   b'4    e'8    e'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
