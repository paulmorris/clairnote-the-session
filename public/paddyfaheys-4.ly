\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1402#setting26255"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1402#setting26255" {"https://thesession.org/tunes/1402#setting26255"}}}
	title = "Paddy Fahey's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d''8    cis''8    a'8    g'8    
e'8    fis'8    g'8    e'8  \bar "|"   a'4    e'8    a'8    cis''8    d''8    
e''8    cis''8  \bar "|"   d''8    cis''8    a'8    b'8    cis''8    d''8    
e''8    fis''8  \bar "|"   g''4    fis''8    e''8    d''8    fis''8    e''8    
cis''8  \bar "|"     d''8    cis''8    a'8    g'8    e'8    fis'8    g'8    e'8 
 \bar "|"   a'4    e'8    a'8    cis''8    d''8    e''8    cis''8  \bar "|"   
d''8    cis''8    a'8    b'8    cis''8    d''8    e''8    d''8  \bar "|"   
cis''8    a'8    g'8    e'8    fis'8    d'8    d'8    cis''8  }     
\repeat volta 2 {   |
 d''4    a''8    fis''8    g''8    e''8    d''8    e''8  \bar "|"   d''8    
cis''8    a'8    g'8    e'8    fis'8    g'8    e'8  \bar "|"   a'8    d''8    
d''4    a''8    d''8    fis''8    a''8  \bar "|"   g''8    e''8    d''8    
cis''8    a'8    d''8    d''4  \bar "|"     fis'8    d''8    e''8    fis''8    
g''8    fis''8    e''8    d''8  \bar "|"   c''8    e''8    a'8    b'8    c''4.  
  d''8  \bar "|"   e''8    a''8    a''8    g''8    e''8    fis''8    g''8    
b''8  \bar "|"   a''8    gis''8    e''8    d''8    c''8    a'8    \tuplet 3/2 {  
 c''8    d''8    e''8  } }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
