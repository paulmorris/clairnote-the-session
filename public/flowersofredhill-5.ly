\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mr G. Cunningham"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/442#setting28524"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/442#setting28524" {"https://thesession.org/tunes/442#setting28524"}}}
	title = "Flowers Of Red Hill, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \mixolydian   e''8    a'8    \tuplet 3/2 {   a'8    a'8    a'8  
}   g''8    a'8    fis''8    a'8  \bar "|"   e''8    a'8    \tuplet 3/2 {   a'8  
  a'8    a'8  }   \tuplet 3/2 {   e''8    fis''8    e''8  }   d''8    fis''8  
\bar "|"   e''8    a'8    \tuplet 3/2 {   a'8    a'8    a'8  }   e''8    a'8    
a''8    fis''8  \bar "|"   g''8    e''8    d''8    b'8    g'8    a'8    b'8    
d''8  }     \repeat volta 2 {   e''8    a''8    \tuplet 3/2 {   a''8    a''8    
a''8  }   b''8    a''8    a''8    g''8  } \alternative{{   e''8    a''8    a''8 
   g''8    e''8    d''8    b'8    d''8  \bar "|"   e''8    a''8    \tuplet 3/2 { 
  a''8    a''8    a''8  }   b''8    a''8    a''8    g''8  \bar "|"   e''8    
d''8    e''8    g''8    g''8    e''8    d''8    g''8  } }    a''8    g''8    
e''8    d''8  \bar "|"   e''8    d''8    e''8    g''8    g''8    e''8    d''8   
 g''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
