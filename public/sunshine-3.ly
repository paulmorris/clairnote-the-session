\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Pierre LAÏLY"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1362#setting14715"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1362#setting14715" {"https://thesession.org/tunes/1362#setting14715"}}}
	title = "Sunshine, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   cis''8    d''8  \bar "|" \tuplet 3/2 {   e''8    a''8  
  e''8  } \tuplet 3/2 {   cis''8    e''8    cis''8  }   \tuplet 3/2 {   a'8    
cis''8    a'8  }   e'8    d'8  \bar "|"   cis'8    e'8    a'8    cis''8    e''8 
   cis''8    a'8    cis''8  \bar "|" \tuplet 3/2 {   d''8    e''8    d''8  } 
\tuplet 3/2 {   cis''8    d''8    cis''8  }   \tuplet 3/2 {   b'8    cis''8    
b'8  } \tuplet 3/2 {   a'8    b'8    a'8  } \bar "|"   gis'8    e''8    fis'8    
e''8    e'4    cis''8    d''8  \bar "|"   \tuplet 3/2 {   e''8    a''8    e''8  
} \tuplet 3/2 {   cis''8    e''8    cis''8  }   \tuplet 3/2 {   a'8    cis''8    
a'8  }   e'8    d'8  \bar "|"   cis'8    e'8    a'8    cis''8    d''8    e''8   
 fis''8    gis''8  \bar "|" \tuplet 3/2 {   a''8    gis''8    fis''8  } 
\tuplet 3/2 {   e''8    d''8    cis''8  }   \tuplet 3/2 {   fis''8    e''8    
d''8  } \tuplet 3/2 {   cis''8    b'8    a'8  } \bar "|" \tuplet 3/2 {   gis'8    
a'8    b'8  } \tuplet 3/2 {   e'8    fis'8    gis'8  }   a'4  }   
\repeat volta 2 {   b'8    cis''8  \bar "|"   d''8    b'8    gis'8    b'8    
e'8    gis'8    b'8    cis''8  \bar "|"   d''8    b'8    gis'8    b'8    d''4   
 fis''8    a''8  \bar "|"   e''8    cis''8  \tuplet 3/2 {   a'8    cis''8    a'8 
 }   e'8    a'8  \tuplet 3/2 {   cis''8    d''8    dis''8  } \bar "|"   e''8    
cis''8  \tuplet 3/2 {   a'8    cis''8    e''8  }   a''4    gis''8    a''8  
\bar "|"   fis''8    d''8    b'8    d''8    \tuplet 3/2 {   fis''8    e''8    
d''8  } \tuplet 3/2 {   cis''8    b'8    a'8  } \bar "|" \tuplet 3/2 {   gis'8    
a'8    b'8  } \tuplet 3/2 {   a'8    gis'8    fis'8  }   e'4    cis''8    d''8  
\bar "|"   e''8    a''8    fis''8    d''8    cis''8    a'8    b'8    gis'8  
\bar "|"   a'8    a''8    e''8    cis''8    a'4  }   cis''8    d''8  \bar "|" 
\tuplet 3/2 {   e''8    a''8    e''8  } \tuplet 3/2 {   cis''8    e''8    cis''8  
}   \tuplet 3/2 {   a'8    cis''8    a'8  }   e'8    d'8  \bar "|"   cis'8    
e'8    a'8    cis''8    e''8    cis''8    a'8    cis''8  \bar "|" \tuplet 3/2 {  
 d''8    b''8    b''8  } \tuplet 3/2 {   cis''8    a''8    a''8  }   
\tuplet 3/2 {   b'8    gis''8    gis''8  } \tuplet 3/2 {   a'8    fis''8    
fis''8  } \bar "|" \tuplet 3/2 {   gis'8    e''8    e''8  } \tuplet 3/2 {   fis'8 
   dis''8    dis''8  }   e''4    cis''8    d''!8  \bar "|"   \tuplet 3/2 {   
e''8    a''8    e''8  } \tuplet 3/2 {   cis''8    e''8    cis''8  }   
\tuplet 3/2 {   a'8    cis''8    a'8  }   e'8    d'8  \bar "|"   cis'8    e'8    
a'8    cis''8    d''8    e''8    fis''8    gis''8  \bar "|" \tuplet 3/2 {   a''8 
   gis''8    fis''8  } \tuplet 3/2 {   e''8    d''8    cis''8  }   \tuplet 3/2 {  
 fis''8    e''8    d''8  } \tuplet 3/2 {   cis''8    b'8    a'8  } \bar "|" 
\tuplet 3/2 {   gis'8    a'8    b'8  } \tuplet 3/2 {   e'8    fis'8    gis'8  }   
a'4  \bar "||"   b'8    cis''8  \bar "|"   d''8    b'8    gis'8    b'8    e'8   
 gis'8    b'8    cis''8  \bar "|"   d''8    b'8    gis'8    b'8    d''4    
fis''8    a''8  \bar "|"   e''8    cis''8  \tuplet 3/2 {   a'8    cis''8    a'8  
}   e'8    a'8  \tuplet 3/2 {   cis''8    d''8    dis''8  } \bar "|"   e''8    
cis''8  \tuplet 3/2 {   a'8    cis''8    e''8  }   a''4    gis''4  \bar "|"   
\tuplet 3/2 {   fis''8    gis''8    fis''8  } \tuplet 3/2 {   e''8    fis''8    
e''8  }   \tuplet 3/2 {   d''8    e''8    d''8  } \tuplet 3/2 {   cis''8    d''8  
  cis''8  } \bar "|"   b'8    cis''8    d''8    e''8    fis''8    e''8    
fis''8    gis''8  \bar "|" \tuplet 3/2 {   a''8    gis''8    fis''8  } 
\tuplet 3/2 {   e''8    d''8    cis''8  }   \tuplet 3/2 {   fis''8    e''8    
d''8  } \tuplet 3/2 {   cis''8    b'8    a'8  } \bar "|" \tuplet 3/2 {   gis'8    
a'8    b'8  } \tuplet 3/2 {   e'8    fis'8    gis'8  }   a'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
