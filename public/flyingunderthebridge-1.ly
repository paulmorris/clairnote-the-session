\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "weemanwatson"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1457#setting1457"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1457#setting1457" {"https://thesession.org/tunes/1457#setting1457"}}}
	title = "Flying Under The Bridge"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \major   \repeat volta 2 {   gis''4.    fis''8    e''8    
fis''8  \bar "|"   gis''8    e''8    cis''8    b'8    gis'8    b'8  \bar "|"   
gis''4.    fis''8    e''8    fis''8  \bar "|"   gis''8    e''8    cis''8    b'8 
   gis'8    e'8  \bar "|"     e'4    gis''8    fis''8    e''8    fis''8  
\bar "|"   gis''8    e''8    cis''8    b'8    gis'8    b'8  \bar "|"   gis''8   
 e''8    e''8    fis''8    e''8    e''8  \bar "|"   dis''8    e''8    fis''8    
e''4.  }     \repeat volta 2 {   gis''8    e''8    e''8    cis''8    e''8    
b'8  \bar "|"   gis''8    e''8    a''8    e''8    b''8    e''8  \bar "|"   
gis''8    e''8    e''8    cis''8    e''8    b'8  \bar "|"   fis''8    e''8    
dis''8    e''8    fis''8    e''8  \bar "|"     gis''8    e''8    e''8    cis''8 
   e''8    b'8  \bar "|"   gis''8    e''8    a''8    e''8    b''8    e''8  
\bar "|"   gis''8    e''8    e''8    fis''8    e''8    e''8  \bar "|"   dis''8  
  e''8    fis''8    e''4.  }     \repeat volta 2 {   gis''4.    fis''4.  
\bar "|"   gis''8    e''8    cis''8    b'8    cis''8    e''8  \bar "|"   
gis''4.    fis''4.  \bar "|"   gis''8    a''8    b''8    a''8    gis''8    
fis''8  \bar "|"     gis''4.    fis''4.  \bar "|"   gis''8    e''8    cis''8    
b'8    cis''8    e''8  \bar "|"   gis''8    e''8    e''8    fis''8    e''8    
e''8  \bar "|"   dis''8    e''8    fis''8    e''4.  }     \repeat volta 2 {   
gis''8    e''8    cis''8    e''8    b'8    e''8  \bar "|"   gis''8    e''8    
a''8    e''8    b''8    e''8  \bar "|"   gis''8    e''8    cis''8    e''8    
b'8    e''8  \bar "|"   fis''8    e''8    dis''8    e''8    fis''8    e''8  
\bar "|"     gis''8    e''8    cis''8    e''8    b'8    e''8  \bar "|"   gis''8 
   e''8    a''8    e''8    b''8    e''8  \bar "|"   gis''8    e''8    e''8    
fis''8    e''8    e''8  \bar "|"   dis''8    e''8    fis''8    e''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
