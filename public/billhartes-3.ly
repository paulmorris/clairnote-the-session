\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Elda Rose"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1281#setting14592"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1281#setting14592" {"https://thesession.org/tunes/1281#setting14592"}}}
	title = "Bill Harte's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   \repeat volta 2 {   d'8    e'8    g'8    a'8    
b'8    g'8    g'4  \bar "|"   c''8    g'8    b'8    g'8    a'8    a'8    g'8    
e'8  \bar "|"   d'8    e'8    g'8    a'8    b'8    d''8    e''8    d''8  
\bar "|"   b'8    a'8    a'8    g'8    a'4    g'8    e'8  \bar "|"   d'8    e'8 
   g'8    a'8    b'8    g'8    g'16    g'16    g'8  \bar "|"   c''8    g'8    
b'8    g'8    a'4    g'8    e'8  \bar "|"   d'8    e'8    g'8    a'8    b'8    
d''8    e''8    d''8  \bar "|"   b'8    a'8    a'8    g'8    a'8    b'8    c''8 
   d''8  \bar "||"   \bar "||"   e''8    a'8    a'8    a'8    c''8    d''8    
e''8    g''8  \bar "|"   d''8    g'8    g'8    fis'8    g'8    a'8    b'8    
d''8  \bar "|"   e''8    a'8    a'16    a'16    a'8    c''8    d''8    e''8    
g''8  \bar "|"   d''8    b'8    g'8    b'8    a'4.    a'8    e''8    a'8    a'8 
   a'8    c''8    d''8    e''8    g''8  \bar "|"   d''8    g'8    g'8    fis'8  
  g'8    a'8    b'8    d''8  \bar "|"   e''4.    d''8    e''4.    fis''8  
\bar "|"   g''8    e''8    d''8    b'8    a'4    g'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
