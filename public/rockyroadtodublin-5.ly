\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "turophile"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/593#setting13601"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/593#setting13601" {"https://thesession.org/tunes/593#setting13601"}}}
	title = "Rocky Road To Dublin, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key a \dorian   \tuplet 3/2 {   e''8    fis''8    e''8  }   d''8    
b'8    a'8    g'8    \bar "|"   e'8    a'8    a'8    b'8    c''8    d''8    
\bar "|"   \tuplet 3/2 {   e''8    fis''8    e''8  }   d''8    b'8    a'8    
c''8    \bar "|"   b'8    g'8    g'8    g'8    b'8    d''8    \bar "|"   
\tuplet 3/2 {   e''8    fis''8    e''8  }   d''8    b'8    a'8    g'8    
\bar "|"   e'8    a'8    a'8    b'8    c''8    d''8    \bar "|"   \tuplet 3/2 {  
 e''8    fis''8    g''8  }   fis''8    a''8    g''8    e''8    \bar "|"   d''8  
  b'8    g'8    a'8    b'8    d''8    }   \repeat volta 2 {   e''8    a''8    
a''8    fis''8    g''8    g''8    \bar "|"   e''8    a''8    a''8    a'8    b'8 
   d''8    \bar "|"   e''8    a''8    a''8    fis''8    g''8    e''8    
\bar "|"   d''8    b'8    g'8    a'8    b'8    d''8    \bar "|"   e''8    a''8  
  a''8    fis''8    g''8    g''8    \bar "|"   e''8    a''8    a''8    fis''8   
 g''8    a''8    \bar "|"   b''8    b''8    a''8    fis''8    g''8    e''8    
\bar "|"   d''8    b'8    g'8    a'8    b'8    d''8    }   \repeat volta 2 { 
\tuplet 3/2 {   e''8    fis''8    e''8  }   d''8    b'8    c''8    a'8    
\bar "|"   e''8    a'8    c''8    a'8    b'8    d''8    \bar "|"   \tuplet 3/2 { 
  e''8    fis''8    e''8  }   d''8    b'8    c''8    a'8    \bar "|"   b'8    
g'8    g'8    g'8    b'8    d''8    \bar "|"   \tuplet 3/2 {   e''8    fis''8    
e''8  }   d''8    b'8    c''8    a'8    \bar "|"   e''8    a'8    c''8    a'8   
 b'8    d''8    \bar "|"   \tuplet 3/2 {   e''8    fis''8    g''8  }   fis''8    
a''8    g''8    e''8    \bar "|"   d''8    b'8    g'8    a'8    b'8    d''8    
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
