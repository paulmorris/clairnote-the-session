\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/363#setting13164"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/363#setting13164" {"https://thesession.org/tunes/363#setting13164"}}}
	title = "Drunken Landlady, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   b'8    e'8    e'4    b'8    a'8    fis'8    a'8    
\bar "|"   b'4    e'8    d''8    b'8    a'8    fis'8    b'8    \bar "|"   a'8   
 b'8    a'8    fis'8    d'4    e'8    d'8    \bar "|"   fis'8    a'8    a'8    
d''8    b'8    a'8    fis'8    a'8    \bar "|"     
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
