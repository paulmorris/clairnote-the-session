\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/373#setting13191"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/373#setting13191" {"https://thesession.org/tunes/373#setting13191"}}}
	title = "Connie The Soldier"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   e'8    a'8    a'8    a'8    b'8    d''8  \bar "|"   
cis''8    a'8    g'8    e'8    fis'8    d'8  \bar "|"   e'8    fis'8    g'8    
e'8    fis'8    g'8  \bar "|"   e'8    a'8    fis'8    g'8    e'8    d'8  
\bar "|"   e'8    a'8    a'8    a'8    b'8    d''8  \bar "|"   cis''8    a'8    
g'8    e'8    a'8    d''8  \bar "|"   cis''4. ^"~"    a'8    b'8    g'8  
\bar "|"   fis'8    d'8    d'8    d'4    g'8  \bar ":|."   fis'8    d'8    d'8  
  d'4    e''8  \bar "||"   \bar ".|:"   fis''8    d''8    d''8    d''8    cis''8 
   d''8  \bar "|"   fis''8    d''8    d''8    d''4    e''8  \bar "|"   fis''8   
 d''8    d''8    e''4    d''8  \bar "|"   cis''8    a'8    a'8    a'4    e''8  
\bar "|"   fis''8    g''8    a''8    e''8    fis''8    g''8  \bar "|"   fis''8  
  e''8    d''8    d''8    e''8    d''8  \bar "|"   cis''8    b'8    cis''8    
a'8    b'8    g'8  \bar "|"   fis'8    d'8    d'8    d'4    e''8  \bar ":|."   
fis'8    d'8    d'8    d'4    g'8  \bar "||"   e'8    a'8    a'8    a'8    b'16 
   cis''16    d''8  \bar "|"   cis''8    a'8    g'8    e'4    d'8  \bar "|"   
e'8    fis'8    g'8    e'8    fis'8    g'8  \bar "|"   e'8    a'8    fis'8    
g'8    e'8    d'8  \bar "|"   e'8    a'8    a'8    a'8    b'16    cis''16    
d''8  \bar "|"   cis''8    a'8    g'8    e'8    cis''8    d''8  \bar "|"   
cis''8    b'8    cis''8    a'8    b'8    g'8  \bar "|"   fis'8    d'8    d'8    
d'4    e'8  \bar ":|."   fis'8    d'8    d'8    d'4    e''8  \bar "||"   
\bar ".|:"   fis''8    d''8    d''8    d''8    cis''8    d''8  \bar "|"   fis''8 
   e''8    d''8    d''4    e''8  \bar "|"   fis''4    d''8    e''8    fis''8    
d''8  \bar "|"   cis''8    a'8    a'8    a'4.  \bar "|"   fis''8    g''8    
a''8    e''8    fis''8    g''8  \bar "|"   fis''8    e''8    d''8    fis''8    
e''8    d''8  \bar "|"   cis''8    b'8    cis''8    a'8    b'8    g'8  \bar "|" 
  fis'8    d'8    d'8    d'4    e''8  \bar ":|."   fis'8    d'8    d'8    d'4   
 e'8  \bar "||"   fis'4    g'8    a'4    d''8  \bar "|"   d''8    cis''8    
d''8    cis''8    a'8    g'8  \bar "|"   e'8    fis'8    g'8    e'8    fis'8    
g'8  \bar "|"   a'4. ^"~"    g'8    fis'8    d'8  \bar "|"   fis'4    g'8    
a'8    d''8    d''8  \bar "|"   d''8    cis''8    d''8    cis''8    a'8    g'8  
\bar "|"   e'8    fis'8    g'8    a'4    g'8  \bar "|"   fis'8    d'8    d'8    
d'4    e'8  \bar ":|."   fis'8    d'8    d'8    d'8    a''8    g''8  \bar "||"  
 \bar ".|:"   fis''4    d''8    e''8    d''8    cis''8  \bar "|"   a'8    d''8   
 d''8    d''4    e''8  \bar "|"   fis''8    d''8    d''8    e''4    d''8  
\bar "|"   cis''8    a'8    a'8    a'8    fis''8    g''8  \bar "|"   fis''8    
d''8    d''8    e''8    d''8    cis''8  \bar "|"   d''8    e''8    d''8    
cis''8    a'8    g'8  \bar "|"   e'8    fis'8    g'8    a'4    g'8  \bar "|"   
fis'8    d'8    d'8    d'8    a''8    g''8  \bar ":|."   fis'8    d'8    d'8    
d'4    e'8  \bar "||"   e'8    fis'8    g'8    a'4. ^"~"  \bar "|"   d''8    
e''8    d''8    c''8    a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    g'8  
  g'8  \bar "|"   e'8    a'8    fis'8    g'8    e'8    d'8  \bar "|"   e'8    
fis'8    g'8    a'8    g'8    a'8  \bar "|"   d''8    e''8    d''8    c''8    
a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    a'8    g'8  \bar "|"   e'8   
 d'8    d'8    d'4    d'8  \bar ":|."   e'8    d'8    d'8    d'4    e''8  
\bar "||"   \bar ".|:"   fis''8    d''8    d''8    e''8    d''8    cis''8  
\bar "|"   e''8    d''8    cis''8    d''4    e''8  \bar "|"   fis''8    d''8    
d''8    c''8    a'8    a'8  \bar "|"   b'8    a'8    g'8    a'4    g''8  
\bar "|"   fis''8    d''8    d''8    e''8    d''8    cis''8  \bar "|"   d''8    
e''8    d''8    c''8    a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    a'8  
  g'8  \bar "|"   e'8    d'8    d'8    d'4    e''8  \bar ":|."   e'8    d'8    
d'8    d'4    d'8  \bar "||"   d'8    e'8    g'8    a'4    a'8  \bar "|"   a'8  
  d''8    b'8    cis''8    a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    
g'8    g'8  \bar "|"   e'8    cis''8    a'8    g'8    e'8    a'8  \bar "|"   
d'8    e'16    fis'16    g'8    a'4. ^"~"  \bar "|"   a'8    d''8    b'8    
cis''8    a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    a'8    g'8  
\bar "|"   e'4 ^"~"    d'8    d'4    a8  \bar ":|."   e'4 ^"~"    d'8    d'8    
d''8    e''8  \bar "||"   \bar ".|:"   fis''8    d''8    d''8    e''8    d''8    
e''8  \bar "|"   fis''8    d''8    cis''!8    d''4    e''8  \bar "|"   fis''8   
 e''8    d''8    cis''4. ^"~"  \bar "|"   b'8    a'8    g'8    a'4    g''8  
\bar "|"   fis''8    d''8    d''8    e''8    d''8    cis''!8  \bar "|"   d''8   
 e''8    d''8    cis''8    a'8    g'8  \bar "|"   e'8    g'8    g'8    e'8    
a'8    g'8  \bar "|"   e'4 ^"~"    d'8    d'8    d''8    e''8  \bar ":|."   e'4 
^"~"    d'8    d'4    a8  \bar "||"   d'8    e'8    g'8    a'8    b'8    a'8  
\bar "|"   a'8    d''8    b'8    c''8    a'8    g'8  \bar "|"   e'4 ^"~"    g'8 
   e'8    g'8    g'8  \bar "|"   e'8    g'8    e'8    g'8    e'8    d'8  
\bar "|"   d'8    e'8    g'8    a'8    b'8    a'8  \bar "|"   a'8    d''8    
b'8    c''8    a'8    g'8  \bar "|"   e'4 ^"~"    g'8    e'8    a'8    g'8  
\bar "|"   e'8    d'8    cis'8    d'4    a'8  \bar ":|."   e'8    d'8    cis'8  
  d'4    e''8  \bar "||"   \bar ".|:"   fis''4 ^"~"    d''8    cis''8    d''8    
e''8  \bar "|"   fis''8    d''8    d''8    d''4    e''8  \bar "|"   fis''8    
a''8    d''8    e''4    d''8  \bar "|"   cis''8    a'8    a'8    a'8    a''8    
g''8  \bar "|"   fis''4 ^"~"    d''8    e''8    d''8    cis''8  \bar "|"   a'8  
  d''8    b'8    c''8    a'8    g'8  \bar "|"   e'4 ^"~"    g'8    e'8    a'8   
 g'8  \bar "|"   e'8    d'8    cis'8    d'4    e''8  \bar ":|."   e'8    d'8    
cis'8    d'4    a'8  \bar "||"   a'4. ^"~"    a'4    d''8  \bar "|"   cis''8    
a'8    g'8    e'8    d'8    d'8  \bar "|"   d'16    e'16    fis'8    g'8    
d'16    e'16    fis'8    g'8  \bar "|"   a'8    g'8    e'8    cis''8    a'8    
g'8  \bar "|"   a'4. ^"~"    d'4    d''8  \bar "|"   cis''8    a'8    g'8    
e'8    d'8    d'8  \bar "|"   d'16    e'16    fis'8    g'8    a'8    g'8    e'8 
 \bar "|"   e'4 ^"~"    d'8    d'8    e'16    fis'16    g'8  \bar ":|."   e'4 
^"~"    d'8    d'4    e''8  \bar "||"   \bar ".|:"   fis''8    e''8    d''8    
e''8    d''8    cis''!8  \bar "|"   e''8    d''8    d''8    d''4    e''8  
\bar "|"   fis''8    d''8    d''8    e''4    d''8  \bar "|"   cis''8    a'8    
g'8    a'4    e''8  \bar "|"   fis''8    d''8    d''8    fis''8    e''8    d''8 
 \bar "|"   cis''8    a'8    b'8    cis''4    d''8  \bar "|"   cis''8    a'8    
g'8    e'8    a'8    g'8  \bar "|"   e'8    d'8    d'8    d'4    e''8  
\bar ":|."   e'8    d'8    d'8    d'8    e'16    fis'16    g'8  \bar "||"   
a'4. ^"~"    a'4    d''8  \bar "|"   cis''8    a'8    g'8    e'4    d'8  
\bar "|"   d'8    g'8    g'8    d'8    e'16    fis'16    g'8  \bar "|"   a'8    
g'8    e'8    cis''8    a'8    g'8  \bar "|"   a'4 ^"~"    d'8    a'4    d''8  
\bar "|"   cis''8    a'8    g'8    e'4    d'8  \bar "|"   d'8    e'16    fis'16 
   g'8    a'8    g'8    e'8  \bar "|"   e'8    d'8    d'8    d'8    e'8    g'8  
\bar ":|."   e'8    d'8    d'8    d'4    e''8  \bar "||"   \bar ".|:"   fis''8   
 e''8    d''8    e''8    d''8    cis''!8  \bar "|"   e''8    d''8    d''8    
d''4    e''8  \bar "|"   fis''8    e''8    d''8    e''4 ^"~"    d''8  \bar "|"  
 cis''!8    a'8    g'8    a'4    d''8  \bar "|"   fis''8    e''8    d''8    
e''4 ^"~"    d''8  \bar "|"   cis''!8    a'8    b'8    c''4    d''8  \bar "|"   
cis''8    a'8    g'8    e'8    a'8    g'8  \bar "|"   e'8    d'8    d'8    d'4  
  e''8  \bar ":|."   e'4 ^"~"    d'8    d'8    e'16    fis'16    g'8  \bar "||" 
  a'8    b'8    a'8    a'8    d''8    b'8  \bar "|"   c''8    b'8    c''8    
e'4    d'8  \bar "|"   e'8    g'8    g'8    e'8    a'8    a'8  \bar "|"   e'8   
 d'8    e'8    g'8    e'8    d'8  \bar "|"   e'8    fis'8    g'8    a'4    b'8  
\bar "|"   c''8    b'8    c''8    g''4    e''8  \bar "|"   fis''8    e''8    
d''8    cis''8    a'8    g'8  \bar "|"   fis'8    g'8    e'8    d'8    fis'8    
g'8  \bar "|"   a'4.    b'8    a'8    b'8  \bar "|"   c''8    b'8    c''8    
e'4    d'8  \bar "|"   e'8    g'8    g'8    e'8    a'8    a'8  \bar "|"   e'8   
 d'8    e'8    g'8    e'8    d'8  \bar "|"   e'8    fis'8    g'8    a'4    b'8  
\bar "|"   c''8    b'8    c''8    g''4    e''8  \bar "|"   fis''8    e''8    
d''8    cis''8    a'8    g'8    fis'8    g'8    e'8    d'4    e''8  \bar "||"   
fis''8    e''8    d''8    e''8    d''8    cis''8  \bar "|"   e''8    d''8    
d''8    d''4    e''8  \bar "|"   fis''8    e''8    d''8    e''4 ^"~"    d''8  
\bar "|"   cis''8    a'8    a'8    a'4    e''8  \bar "|"   fis''8    e''8    
d''8    e''4 ^"~"    d''8  \bar "|"   cis''8    a'8    b'8    c''4    d''8  
\bar "|"   c''8    a'8    g'8    e'8    a'8    g'8  \bar "|"   e'8    d'8    
d'8    d'4    e''8  \bar "|"   fis''8    e''8    d''8    e''8    d''8    cis''8 
 \bar "|"   e''8    d''8    d''8    d''4    e''8  \bar "|"   fis''8    e''8    
d''8    e''4 ^"~"    d''8  \bar "|"   cis''8    a'8    a'8    a'4    d''8  
\bar "|"   e''8    fis''8    g''8    e''8    fis''8    g''8  \bar "|"   b''8    
g''8    e''8    d''4    cis''8  \bar "|"   a'8    b'8    g'8    fis'8    g'8    
e'8  \bar "|"   fis'8    d'8    d'8    d'8    e'16    fis'16    g'8  \bar "||"  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
