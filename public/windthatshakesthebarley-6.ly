\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tall, Dark, and Mysterious"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/116#setting12711"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/116#setting12711" {"https://thesession.org/tunes/116#setting12711"}}}
	title = "Wind That Shakes The Barley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   a'4    a'8    b'8    a'8    fis'8    e'8   
 d'8  \bar "|"   b'4    b'8    a'8    b'8    cis''8    d''8    b'8  \bar "|"   
a'4    a'8    b'8    a'8    fis'8    e'8    d'8  \bar "|"   g''8    fis''8    
e''8    d''8    b'8    cis''8    d''8    b'8  \bar "|"   a'4    a'8    b'8    
a'8    fis'8    e'8    d'8  \bar "|"   b'4    b'8    a'8    b'8    cis''8    
d''8    b'8  \bar "|"   a'4    a'8    b'8    a'8    fis'8    e'8    d'8  
\bar "|"   g''8    fis''8    e''8    d''8    b'8    cis''8    d''8    e''8  
\bar "|"   \bar "|"   fis''4    fis''8    d''8    g''4    g''8    e''8  
\bar "|"   fis''4    fis''8    d''8    b'8    cis''8    d''8    e''8  \bar "|"  
 fis''8    a''8    fis''8    d''8    g''8    b''8    g''8    e''8  \bar "|"   
a''8    fis''8    e''8    d''8    b'8    cis''8    d''8    e''8  \bar "|"   
fis''4.    d''8    g''4.    e''8  \bar "|"   fis''4    fis''8    d''8    b'8    
cis''8    d''8    e''8  \bar "|"   fis''2    g''2  \bar "|"   a''4    fis''4    
e''4    d''4    \bar "|"   a'4    a'8    a'4    fis'8    e'8    d'8  \bar "|"   
b'4    b'8    b'4    cis''8    d''8    b'8  \bar "|"   a'4    a'8    b'8    a'8 
   fis'8    e'8    d'8  \bar "|"   g''8    fis''8    e''4    b'8    cis''8    
d''8    b'8  \bar "|"   a'2 (   a'8  -)   fis'8    e'8    d'8  \bar "|"   b'2 ( 
  b'8  -)   cis''8    d''8    b'8  \bar "|"   a'4    a'8    b'8    a'8    fis'8 
   e'8    d'8  \bar "|"   b'4    cis''4    d''4    e''4  \bar "|"   \bar "|"   
fis''4    fis''8    d''8    g''4    g''8    e''8  \bar "|"   fis''2    b'8    
cis''8    d''8    e''8  \bar "|"   fis''4    fis''8    d''8    g''4    fis''8   
 g''8  \bar "|"   a''8    fis''8    e''4    b'8    cis''8    d''8    e''8  
\bar "|"   fis''4    fis''8    d''8    g''4    g''8    e''8  \bar "|"   fis''4  
  fis''8    d''8    b'8    cis''8    d''8    e''8  \bar "|"   fis''4    a''8    
e''8    g''4    b''8    e''8  \bar "|"   a''8    fis''8    e''8    d''8    b'8  
  cis''8    d''8    b'8    \bar "|"   a'2 (   a'8  -)   fis'8    e'4  \bar "|"  
 b'2 (   b'8  -)   cis''8    d''4  \bar "|"   a'4    a'8    a'4    fis'8    e'4 
 \bar "|"   g''4    e''4    b'8    cis''8    d''8    b'8  \bar "|"   a'4    a'8 
   b'8    a'8    fis'8    e'8    d'8  \bar "|"   b'4    b'8    a'8    b'8    
cis''8    d''8    b'8  \bar "|"   a'4    a'4    a'8    fis'8    e'8    d'8  
\bar "|"   g''4    fis''4    d''4    e''4  \bar "|"   \bar "|"   fis''4    
fis''8    d''8    g''4    g''8    e''8  \bar "|"   fis''4    fis''8    d''8    
b'8    cis''8    d''8    e''8  \bar "|"   fis''8    e''8    fis''4    g''2  
\bar "|"   a''8    fis''8    e''8    d''8    b'8    cis''8    d''8    e''8  
\bar "|"   fis''4    fis''8    d''8    g''4    g''8    e''8  \bar "|"   fis''4  
  fis''8    d''8    b'8    cis''8    d''8    e''8  \bar "|"   fis''8    a''8    
fis''8    d''8    g''8    b''8    g''8    e''8  \bar "|"   a''8    fis''8    
e''8    d''8    b'8    cis''8    d''8    cis''8  \bar "|"   d''1  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
