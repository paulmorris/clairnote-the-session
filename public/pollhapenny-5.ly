\version "2.7.40"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/841#setting21049"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/841#setting21049" {"https://thesession.org/tunes/841#setting21049"}}}
	title = "Poll Ha'Penny"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major \key d \major   \repeat volta 2 {   d''8    a'8    
\bar "|"   \times 2/3 {   b'8 -.   cis''8 -.   b'8 -. } \grace {    cis''8  }   
b'8    g'8    a'8    r8 \times 2/3 {   a'8 -.   b'8 -.   cis''8 -. }   \bar "|" 
  d''8 -.   cis''8 -. \grace {    b'8  }   a'8    fis'8    g'4 ^"~"    fis'8    
a'8    \bar "|"   a'8    fis'8    d'8    e'8    fis'8    a'8    g'8    e'8    
\bar "|"   d'8    e'8    fis'8    g'8    a'4    d''8    a'8      \times 2/3 {   
b'8 -.   cis''8 -.   b'8 -. } \grace {    cis''8  }   b'8    g'8    a'8    r8 
\times 2/3 {   a'8 -.   b'8 -.   cis''8 -. } \bar "|"   d''8 -.   cis''8 -. 
\grace {    b'8  }   a'8    fis'8    g'4 ^"~"    fis'8    a'8    \bar "|"   a'8 
   fis'8    d'8    e'8    fis'8    a'8    g'8    e'8  \bar "|"   d'4    
\times 2/3 {   d'8    d'8    d'8  }   d'4    }     \repeat volta 2 {   fis'8 (  
 g'8  -)   \bar "|"   a'8    d''8 -.   d''8 -.   e''8    fis''4 ^"~"    e''8 -. 
  fis''8 -.   \bar "|"   d''8 -.   cis''8 -. \grace {    b'8  }   a'8    g'8    
a'4 ^"~"    fis'8 (   g'8  -)   \bar "|"   a'8    d''8 -.   d''8 -.   e''8    
fis''4 ^"~"    e''8 -.   fis''8 -.   \bar "|"   g''8    fis''8    e''8    
fis''8    d''4    fis''8    g''8    \bar "|"     a''8    fis''8 -.   
\times 2/3 {   g''8 -.   fis''8 -.   e''8 -. }   fis''8    d''8    e''8    
cis''8    \bar "|"   d''8 -.   cis''8 -. \grace {    b'8  }   a'8    g'8    a'4 
^"~"    fis'8    g'8    \bar "|"   a'8    fis'8    d'8    e'8    fis'8    a'8   
 g'8    e'8  \bar "|"   d'4    \times 2/3 {   d'8    d'8    d'8  }   d'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
