\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dogbox"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/743#setting13831"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/743#setting13831" {"https://thesession.org/tunes/743#setting13831"}}}
	title = "Swallow's Nest, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   \tuplet 3/2 {   d'8    e'8    fis'8  } \bar "|"   g'4. 
   a'8    g'8    e'8    d'8    b8  \bar "|"   d'8    e'8    g'8    a'8    b'8   
 e''8    d''8    b'8  \bar "|"   a'8    e'8    \tuplet 3/2 {   e'8    e'8    e'8 
 }   a'8    b'8    c''8    d''8  \bar "|"   e''8    a''8    b''8    a''8    
g''8    e''8    d''8    b'8  \bar "|"   d'8    g'8    \tuplet 3/2 {   a'8    g'8 
   fis'8  }   g'8    e'8    d'8    b8  \bar "|"   d'8    e'8    g'8    a'8    
b'8    a'8    b'8    c''8  \bar "|"   d''8    g'8    \tuplet 3/2 {   g'8    g'8  
  g'8  }   fis'8    d'8    a'8    fis'8  \bar "|"   d'8    fis'8    a'8    
fis'8    g'4    \tuplet 3/2 {   d'8    e'8    fis'8  } \bar "|"   g'8    a'8    
b'8    a'8    g'8    e'8    d'8    b8  \bar "|"   d'8    e'8    g'8    a'8    
b'8    e''8    d''8    b'8  \bar "|"   a'8    e'8    \tuplet 3/2 {   e'8    e'8  
  e'8  }   a'8    b'8    c''8    d''8  \bar "|"   e''8    g''8    \tuplet 3/2 {  
 a''8    g''8    fis''8  }   g''8    e''8    d''8    b'8  \bar "|"   g'8    
d''8    b'8    a'8    g'8    e'8    d'8    b8  \bar "|"   d'8    e'8    g'8    
a'8    b'8    g'8    \tuplet 3/2 {   a'8    b'8    c''8  } \bar "|"   d''8    
g'8    \tuplet 3/2 {   g'8    g'8    g'8  }   fis'8    d'8    a'8    fis'8  
\bar "|"   d'8    fis'8    a'8    fis'8    g'4  \bar "||"   b'8    c''8  
\bar "|"   d''8    g''8    a''8    g''8    e''8    d''8    b'8    a'8  \bar "|" 
  g'8    b'8    d'8    e'8    g'8    a'8    b'8    d''8  \bar "|"   e''8    
a''8    gis''8    b''8    a''8    gis''8    e''8    d''8  \bar "|"   b'8    a'8 
   b'8    d''8    e''8    d''8    \tuplet 3/2 {   a'8    b'8    c''8  } \bar "|" 
  e''8    g''8    \tuplet 3/2 {   g''8    g''8    g''8  }   a''8    g''8    b''8 
   g''8  \bar "|"   a''8    g''8    e''8    d''8    e''8    a''8    g''8    
e''8  \bar "|"   d''8    g'8    \tuplet 3/2 {   g'8    g'8    g'8  }   fis'8    
d'8    a'8    fis'8  \bar "|"   d'8    fis'8    a'8    fis'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
