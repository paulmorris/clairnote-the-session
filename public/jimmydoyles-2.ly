\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SebastianM"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/542#setting23424"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/542#setting23424" {"https://thesession.org/tunes/542#setting23424"}}}
	title = "Jimmy Doyle's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \major   cis''8    e''8    e''8    fis''8    \bar "|"   e''8   
 cis''8    cis''8    b'16    cis''16    \bar "|"   e''8    cis''8    b'8    
cis''8    \bar "|"   e''8    cis''8    a'8.    b'16    \bar "|"     cis''8    
e''8    e''8    fis''8    \bar "|"   e''8    cis''8    cis''8    b'16    
cis''16    \bar "|"   e''8    a'8    cis''8    b'8    \bar "|"   b'8    a'8    
a'8.    b'16    \bar ":|."   b'8    a'8    a'4    \bar "||"     a'8    a''8    
gis''8    a''8    \bar "|"   gis''8    fis''8    fis''4    \bar "|"   fis''8    
e''8    dis''8    e''8    \bar "|"   fis''8    e''8    cis''8    b'8    
\bar "|"     a'8    a''8    gis''8    a''8    \bar "|"   gis''8    fis''8    
fis''4    \bar "|"   e''8    cis''8    cis''8    b'8    \bar "|"   b'8    a'8   
 a'4    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
