\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1048#setting14275"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1048#setting14275" {"https://thesession.org/tunes/1048#setting14275"}}}
	title = "Cape Breton Fiddlers' Society's Welcome To The Shetland Islands, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   fis''8    gis''8  \bar "|"   a''8    e''8  
\tuplet 3/2 {   e''8    e''8    e''8  }   e''8    fis''8    e''8    d''8  
\bar "|"   cis''8    e''8  \tuplet 3/2 {   e''8    e''8    e''8  }   fis''8    
e''8    cis''8    e''8  \bar "|"   b''8    fis''8    fis''8    e''8    fis''8   
 gis''8    fis''8    e''8  \bar "|"   d''8    e''8    fis''8    d''8    b'4    
e''8    d''8  \bar "|"   cis''8    d''8    b'8    cis''8    a'8    b'8    gis'8 
   a'8  \bar "|"   fis'8    gis'8    e'8    fis'8    d'8    e'8    cis'8    d'8 
 \bar "|"   b'8    cis'8    d'8    e'8    fis'8    a'8    b'8    cis''8  
\bar "|"   d''8    e''8    fis''8    gis''8    a''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
