\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "O'Bryan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/475#setting25983"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/475#setting25983" {"https://thesession.org/tunes/475#setting25983"}}}
	title = "King Of The Fairies"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \dorian   e'8.    d'16    e'8.    fis'16    g'8.    fis'16    
g'8.    a'16    \bar "|"   b'4    b'4    g'4   ~    g'8.    a'16    \bar "|"   
b'4    e'4    e'8.    fis'16    \tuplet 3/2 {   g'8    fis'8    e'8  }   
\bar "|"   fis'8.    g'16    fis'8.    e'16    d'4    b'4    \bar "|"     e'8.  
  d'16    e'8.    fis'16    g'8.    fis'16    g'8.    a'16    \bar "|"   b'8.   
 a'16    g'8.    b'16    d''4   ~    d''8.    cis''16    \bar "|"   b'4    e'4  
  g'8.    fis'16    e'8.    d'16    \bar "|"   e'2    b'2    }     \bar "||"   
e''4    e''4    b'8.    d''16    e''8.    fis''16    \bar "|"   g''8.    a''16  
  g''8.    fis''16    e''4   ~    e''8.    fis''16    \bar "|"   e''4    b'4    
b'8.    a'16    b'8.    cis''16    \bar "|"   d''8.    e''16    d''8.    
cis''16    b'8.    cis''16    \tuplet 3/2 {   d''8    cis''8    b'8  }   
\bar "|"     e''4    e''4    b'8.    d''16    e''8.    fis''16    \bar "|"   
g''8.    a''16    g''8.    fis''16    e''8.    fis''16    e''8.    d''16    
\bar "|"   b'8.    d''16    e''8.    g''16    fis''8.    e''16    \tuplet 3/2 {  
 d''8    e''8    fis''8  }   \bar "|"   e''2.    r8. fis''16    \bar "|"     
g''4   ~    g''8.    e''16    fis''4   ~    fis''8.    d''16    \bar "|"   
e''8.    d''16    b'8.    cis''16    d''4   ~    d''8.    e''16    \bar "|"   
d''8.    b'16    a'8.    fis'16    g'8.    a'16    b'8.    cis''16    \bar "|"  
 d''8.    b'16    a'8.    fis'16    g'8.    fis'16    e'8.    d'16    \bar "|"  
   b'8.    e'16    e'8.    d'16    e'8.    fis'16    g'8.    a'16    \bar "|"   
b'4    e''4    e''8.    d''16    e''8.    fis''16    \bar "|"   e''4    b'4    
b'8.    a'16    g'8.    fis'16    \bar "|"   e'1    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
