\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/746#setting13834"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/746#setting13834" {"https://thesession.org/tunes/746#setting13834"}}}
	title = "Miller Of Droghan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   gis'4    b'8    gis'8    d'8    
gis'8    b'8    gis'8  \bar "|"   e'8    gis'8    e''8    fis''8    gis''8    
d''8    b'8    a'8  \bar "|"   gis'4    b'8    gis'8    d'8    gis'8    b'8    
a'8  \bar "|"   gis'8    e'8    gis'8    a'8    b'8    d''8    b'8    a'8  
\bar "|"   gis'4    b'8    gis'8    d'8    gis'8    b'8    gis'8  \bar "|"   
e'8    gis'8    e''8    fis''8    gis''8    d''8    b'8    a'8  \bar "|"   
fis''8    a''8    e''8    fis''8    d''4    e''8    d''8  } \alternative{{   
b'8    a'8    gis'8    a'8    b'4    b'8    a'8  } {   b'8    a'8    gis'8    
a'8    b'8    gis'8    \tuplet 3/2 {   b'8    cis''8    d''8  } \bar "|"   
\bar "|"   e''8    e'8    e'4    d'8    e'8    b'8    e'8  \bar "|"   d'8    
e'8    gis'8    a'8    b'4    b'8    a'8  \bar "|"   gis'8    e'8    e'4    d'8 
   e'8    b'8    e'8  \bar "|"   d'8    e'8    gis'8    a'8    b'8    gis'8  
\tuplet 3/2 {   b'8    cis''8    d''8  } \bar "|"   e''8    e'8    e'4    d'8    
e'8    b'8    e'8  \bar "|"   d'8    e'8    gis'8    a'8    b'4    b'8    a'8  
\bar "|"   gis'8    e'8    e'4    d'8    e'8    b'8    e'8  \bar "|"   d'8    
e'8    gis'8    a'8    b'4    b'8    a'8  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
