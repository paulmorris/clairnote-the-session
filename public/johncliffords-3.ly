\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "arpadoro"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/916#setting14103"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/916#setting14103" {"https://thesession.org/tunes/916#setting14103"}}}
	title = "John Clifford's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   \repeat volta 2 {   a'4    a'8    b'16    cis''16  
\bar "|"   d''16    cis''16    b'16    a'16    fis'8    d'8  \bar "|"   fis'8   
 a'8    a'16    fis'16    d'16    e'16  \bar "|"   fis'8    e'8    e'8    
fis'16    g'16  \bar "|"   \bar "|"   a'4    a'8    b'16    cis''16  \bar "|"   
d''16    cis''16    b'16    a'16    fis'8    d'8  \bar "|"   fis'8    a'8    
d''8.    e''16  \bar "|"   fis''8    e''8    d''4  }   \repeat volta 2 {   
fis'8    a'8    d''8.    e''16  \bar "|"   fis''8    e''8    e''8.    d''16  
\bar "|"   cis''8    a'8    cis''8    e''8  \bar "|"   d''8    cis''8    b'8    
a'8  \bar "|"   \bar "|"   fis'8    a'8    d''8.    e''16  \bar "|"   fis''8    
e''8    e''8.    d''16  \bar "|"   cis''8    a'8    cis''8    e''8  \bar "|"   
d''4    d''8.    g'16  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
