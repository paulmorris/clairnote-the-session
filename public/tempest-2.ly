\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fynnjamin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1072#setting14300"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1072#setting14300" {"https://thesession.org/tunes/1072#setting14300"}}}
	title = "Tempest, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   d''16    c''16  \bar "|"   b'4    b'8    b'8    g''8  
  fis''8  \bar "|" \grace {    fis''8  }   e''4    e''8    e''8    fis''8    
g''8  \bar "|"   d''8    e''8    d''8    d''8    c''8    b'8  \bar "|"   b'8    
a'8    a'8    a'4    d''16    c''16  \bar "|"   b'4    b'8    b'8    g''8    
fis''8  \bar "|"   e''4    e''8    e''8    fis''8    g''8  \bar "|"   d''8    
e''8    d''8    c''8    b'8    a'8  \bar "|"   g'4    g'8    g'4  }   g''16    
a''16  \bar "|"   b''4    g''8    a''4    fis''8  \bar "|"   g''8    fis''8    
e''8    d''8    c''8    b'8  \bar "|"   c''8    d''8    e''8    d''16    g''8.  
  b'8  \bar "|"   b'8    a'8    a'8    a'4    g''16    a''16  \bar "|"   b''4   
 g''8    a''4    fis''8  \bar "|"   g''8    fis''8    e''8    a''8    g''8    
fis''8  \bar "|"   b''8    a''8    g''8    fis''8    g''8    e''8  \bar "|"   
d''8    g''8    fis''8    e''8    d''8    c''8  \bar "|"   b'4    b'8    b'8    
g''8    fis''8  \bar "|" \grace {    fis''8  }   e''4    e''8    e''8    fis''8 
   g''8  \bar "|"   d''8    e''8    d''8    d''8    c''8    b'8  \bar "|"   b'8 
   a'8    a'8    a'4    d''16    c''16  \bar "|"   b'4    b'8    b'8    g''8    
fis''8  \bar "|"   e''4    e''8    e''8    fis''8    g''8  \bar "|"   d''8    
e''8    d''8    c''8    b'8    a'8  \bar "|"   g'4    g'8    g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
