\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/685#setting13732"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/685#setting13732" {"https://thesession.org/tunes/685#setting13732"}}}
	title = "King's Fancy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   d''16    e''16  \bar "|"   fis''8 
   e''8    d''8    d''8    cis''8    d''8  \bar "|"   b'8    d''8    g''8    
a'8    d''8    fis''8  \bar "|"   g'8    b'8    e''8    fis'8    a'8    d''8  
\bar "|"   e''8    cis''8    a'8    b''8    a''8    g''8  \bar "|"   fis''8    
e''8    d''8    d''8    cis''8    d''8  \bar "|"   b'8    d''8    g''8    a'8   
 d''8    fis''8  \bar "|"   g'8    b'8    e''8    fis'8    a'8    d''8  
\bar "|"   e''8    cis''8    a'8    d''4  }   \repeat volta 2 {   d''16    
e''16  \bar "|"   fis''8    d''8    b'8    b'8    ais'8    b'8  \bar "|"   
fis'8    b'8    d''8    fis''8    d''8    b'8  \bar "|"   cis''8    ais'8    
fis'8    e''8    cis''8    ais'8  \bar "|"   fis''8    g''8    fis''8    e''8   
 d''8    cis''8  \bar "|"   fis''8    d''8    b'8    b'8    ais'8    b'8  
\bar "|"   fis'8    b'8    d''8    fis''8    d''8    b'8  \bar "|"   cis''8    
ais'8    fis'8    fis''8    g''8    fis''8  \bar "|"   e''8    d''8    cis''8   
 b'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
