\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Yooval"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/884#setting29687"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/884#setting29687" {"https://thesession.org/tunes/884#setting29687"}}}
	title = "Mountain Lark, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   fis'8    e'8    e'4 ^"~"    fis'8    e'8    d'8    
e'8  \bar "|"   fis'8    e'8    e'4 ^"~"    fis'8    b'8    b'8    a'8  
\bar "|"   fis'8    e'8    e'4 ^"~"    fis'8    e'8    d'8    b8  \bar "|"   
d'4. ^"~"    e'8    fis'8    b'8    b'8    a'8  \bar "|"     fis'8    e'8    
e'4 ^"~"    fis'8    e'8    d'8    e'8  \bar "|"   fis'8    e'8    e'4 ^"~"    
fis'8    b'8    b'8    a'8  \bar "|"   fis'8    e'8    e'4 ^"~"    fis'8    e'8 
   d'8    b8  \bar "|"   d'4. ^"~"    e'8    fis'8    a'8    a'4 ^"~"  
\bar "||"     b'8    e''8    e''4 ^"~"    fis''8    e''8    e''8    d''8  
\bar "|"   b'8    e''8    e''8    cis''8    d''8    b'8    cis''8    a'8  
\bar "|"   b'8    e''8    e''4 ^"~"    fis''8    d''8    e''8    d''8  \bar "|" 
  b'4. ^"~"    cis''8    d''8    cis''8    b'8    a'8  \bar "|"     b'8    e''8 
   e''4 ^"~"    fis''8    e''8    e''8    d''8  \bar "|"   e''8    g''8    
fis''8    e''8    d''4.    e''8  \bar "|"   fis''8    d''8    e''8    cis''8    
d''8    cis''8    b'8    a'8  \bar "|"   b'4. ^"~"    cis''8    d''8    cis''8  
  b'8    a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
