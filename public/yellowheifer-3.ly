\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/344#setting13140"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/344#setting13140" {"https://thesession.org/tunes/344#setting13140"}}}
	title = "Yellow Heifer, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \mixolydian   d'8    g'8    g'8    fis'8    g'4    a'8    c''8 
 \bar "|"   b'8    g'8    g'4 ^"~"    e''8    g'8    d''8    g'8  \bar "|"   
d'8    f'8    f'8    e'8    f'4. ^"~"    bes'8  \bar "|"   a'8    f'8    c''8   
 f'8    d''8    f'8    c''8    f'8  \bar "|"   d'8    g'8    g'8    fis'8    
g'4    a'8    c''8  \bar "|"   b'8    g'8    g'8    a'8    b'8    c''8    d''8  
  e''8  \bar "|"   f''8    a''8    g''8    e''8    f''8    d''8    c''8    a'8  
\bar "|"   b'8    d''8    a'8    fis'8    d'8    g'8    g'8    c'8  \bar ":|."  
 b'8    d''8    a'8    fis'8    d'8    g'8    g'8    c''8  \bar "||"   
\bar ".|:"   d''8    g''8    g''8    fis''8    g''4. ^"~"    a''8  \bar "|"   
b''8    g''8    g''4 ^"~"    b''8    g''8    a''8    f''8  \bar "|"   d''4. 
^"~"    e''8    f''8    e''8    f''8    g''8  \bar "|"   a''8    f''8    f''4 
^"~"    g''8    f''8    d''8    c''8  \bar "|"   d''8    g''8    g''8    fis''8 
   g''4. ^"~"    a''8  \bar "|"   b''8    g''8    g''4 ^"~"    g''8    f''8    
d''8    e''8  \bar "|"   f''8    a''8    g''8    e''8    f''8    d''8    c''8   
 a'8  \bar "|"   b'8    d''8    a'8    fis'8    d'8    g'8    g'8    c''8  
\bar ":|."   b'8    d''8    a'8    fis'8    d'8    g'8    g'8    a'8  \bar "||" 
  \bar ".|:"   b'8    g'8    g'4 ^"~"    b'8    g'8    d''8    g'8  \bar "|"   
b'8    g'8    g'4 ^"~"    \tuplet 3/2 {   b'8    c''8    d''8  }   g''8    d''8  
\bar "|"   a'8    f'8    f'4 ^"~"    a'8    f'8    c''8    f'8  \bar "|"   a'8  
  f'8    f'4 ^"~"    a'8    b'8    c''8    a'8    \bar "|"   b'8    g'8    g'4 
^"~"    b'8    g'8    d''8    g'8  \bar "|"   b'8    g'8    g'8    a'8    b'8   
 c''8    d''8    e''8  \bar "|"   f''8    a''8    g''8    e''8    f''8    d''8  
  c''8    a'8  \bar "|"   b'8    d''8    a'8    fis'8    d'8    g'8    g'8    
a'8  \bar ":|."   b'8    d''8    a'8    fis'8    d'8    g'8    g'8    c'8  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
