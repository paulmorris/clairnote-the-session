\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Damien Rogeau"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/442#setting29016"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/442#setting29016" {"https://thesession.org/tunes/442#setting29016"}}}
	title = "Flowers Of Red Hill, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {     e''8 ^"a"   a'4. ^"~"    g''8  
  a'8    fis''8    a'8    \bar "|"   e''8    a'4. ^"~"      e''8 ^"G"   a'8    
d''8    a'8    \bar "|"     e''8 ^"a"   a'4. ^"~"    e''8    d''8    e''8    
a''8    \bar "|"     g''8 ^"G"   e''8    d''8    b'8    g'8    a'8    b'8    
d''8    }     \bar "|"     e''8 ^"a"   a''8    a''8    g''8    b''4    a''8    
g''8    \bar "|"   e''8    a''8    a''8    g''8    e''8    g''8    d''8    g''8 
   \bar "|"   e''8    a''8    a''8    g''8    b''4    a''8    g''8    \bar "|"  
 e''4    d''8    e''8      g''8 ^"G"   d''8    b'8    d''8    \bar "|"       
e''8 ^"a"   a''8    a''8    g''8    b''4    a''8    g''8    \bar "|"   e''8    
d''8    e''8    fis''8      g''4. ^"G"^"~"    a''8    \bar "|"     b''8 ^"a"   
g''8    a''8    fis''8      g''4 ^"G"   e''8    d''8    \bar "|"     e''8 
^"D/F\#"   a''8    a''8    g''8      e''8 ^"G"   d''8    b'8    d''8    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
