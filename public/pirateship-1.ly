\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Kane"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/895#setting895"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/895#setting895" {"https://thesession.org/tunes/895#setting895"}}}
	title = "Pirate Ship, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g'4 ^"~"    g'8    c''8    b'8    d''8    g''8    
b''8    \bar "|"   a''8    g''8    fis''8    a''8    g''8    fis''8    g''8    
c''8    \bar "|"   b'8    g'8    b'8    d''8    c''8    b'8    a'8    b'8    
\bar "|"   c''8    e''8    d''8    c''8    b'8    g'8    g'4 ^"~"    }     b''8 
   a''8    g''8    b''8    a''8    d''8    fis''8    a''8    \bar "|"   g''8    
fis''8    g''8    b''8    a''8    g''8    fis''8    a''8    \bar "|"   b''8    
a''8    g''8    fis''8    g''8    d''8    c''8    b'8    \bar "|"   e''8    
c''8    d''8    c''8    b'8    g'8    g'4    \bar "|"     b''8    g''8    
fis''8    g''8    d''8    g''8    fis''8    a''8    \bar "|"   g''8    fis''8   
 g''8    b''8    a''8    g''8    fis''8    a''8    \bar "|"   b''8    b'8    
a''8    b'8    g''8    b'8    fis''8    b'8    \bar "|"   c''8    e''8    d''8  
  c''8    b'8    g'8    g'4 ^"~"    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
