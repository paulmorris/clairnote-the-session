\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/185#setting22218"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/185#setting22218" {"https://thesession.org/tunes/185#setting22218"}}}
	title = "Dinny Delaney's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key e \mixolydian   b'4    e'8    e'4.    b'4    a'8    fis'8    
gis'8    a'8  \bar "|"   b'4    e'8    e'4.    a'4    b'8    a'8    fis'8    
e'8  \bar "|"   b'4    e'8    e'4.    d''4    e''8    fis''4    e''8  \bar "|"  
 e''8    d''8    b'8    b'8    a'8    fis'8    a'4    b'8    a'8    fis'8    
e'8  }     \repeat volta 2 {   e''8    d''8    b'8    b'8    a'8    fis'8    
d''4.    b'8    cis''8    d''8  \bar "|"   e''8    d''8    b'8    b'8    a'8    
fis'8    a'4    b'8    a'8    fis'8    e'8  \bar "|"   e''8    d''8    b'8    
b'8    a'8    fis'8    d''4    e''8    fis''4    e''8  \bar "|"   e''8    d''8  
  b'8    b'8    a'8    fis'8    a'4    b'8    a'8    fis'8    e'8  }     
\repeat volta 2 {   b'4    e'8    e'4.    b'4    cis''8    a'4    cis''8  
\bar "|"   b'4    cis''8    a'4    b'8    fis'4.    a'8    fis'8    e'8  
\bar "|"   b'4    e'8    e'4.    d''4    e''8    fis''4    e''8  \bar "|"   
e''8    d''8    b'8    b'8    a'8    fis'8    a'4    b'8    a'8    fis'8    e'8 
 }     \repeat volta 2 {   e''8    d''8    b'8    b'8    a'8    fis'8    d''4.  
  b'8    cis''8    d''8  \bar "|"   e''8    d''8    b'8    b'8    a'8    fis'8  
  a'4    b'8    a'8    fis'8    e'8  \bar "|"   e''8    d''8    b'8    b'8    
a'8    fis'8    d''4    e''8    fis''4    e''8  \bar "|"   e''8    d''8    b'8  
  b'8    a'8    fis'8    a'4    b'8    a'8    fis'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
