\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "BenH"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/957#setting25632"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/957#setting25632" {"https://thesession.org/tunes/957#setting25632"}}}
	title = "Fanny Power"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   d'8    \bar "|"     g'4 ^"G"   
d'8    g'8.    a'16    b'8    \bar "|"     c''4 ^"Am"   b'8    a'4    g'8    
\bar "|"     fis'8. ^"D"   g'16    e'8    d'8    e'8    d'8    \bar "|"     
fis'4 ^"D7"   g'8    a'8.    b'16    c''8    \bar "|"       b'8. ^"G"   a'16    
g'8    b'8    c''8    d''8    \bar "|"     e''4 ^"C"   a'8    a'4    g'8    
\bar "|"     fis'8. ^"D"   g'16    e'8    d'8    g'8    fis'8    \bar "|"     
g'4 ^"G"   g'8    g'4    }     \repeat volta 2 {   d''8    \bar "|"     d''8 
^"G"   b'16    c''16    d''8      d''8 ^"(Bm/F\#)"   b'16    c''16    d''8    
\bar "|"     g'8. ^"Em"   a'16    g'8    g'8    b'8    d''8    \bar "|"     
e''8 ^"C"   c''16    d''16    e''8      e''8 ^"(Am)"   c''16    d''16    e''8   
 \bar "|"     a'8. ^"D"   b'16    a'8    a'8    b'8    c''8  \bar "|"       
b'8. ^"G"   c''16    d''8      e''8 ^"Em"   fis''8    g''8    \bar "|"     
fis''8 ^"D (Am)"   g''8    a''8      d''4 ^"(D)"   c''8    \bar "|"     b'8. 
^"G (G/D)"   a'16    g'8      a'16 ^"D"   b'16    c''8    fis'8    \bar "|"     
g'4 ^"G"   g'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}

\markup \column {
  \line { "(Music part A:)" }
  \line { "When all but dreaming was Fanny Power," }
   \line { "A light came streaming from out her bower." }
   \line { "A heavy thought at her door delayed." }
   \line { "A heavy hand on the latch was laid." }
   \line { "(Music part A:)" }
   \line { "\"Now, who dare venture at this dark hour" }
   \line { "Unbid to enter my maiden bower?\"" }
   \line { "\"Oh, Fanny, open the door to me" }
   \line { "And your true lover you'll surely see.\"" }
   \line { "(Music part B:)" }
   \line { "\"My own true lover so tall and brave," }
   \line { "He lives in next isle o'er the angry wave.\"" }
   \line { "\"Your true love's body lies on the pier." }
   \line { "His faithful spirit is with you here.\"" }
   \line { "(Music part B:)" }
   \line { "\"Oh, his look was cheerful and his voice was gay," }
   \line { "Your face is fearful and your speech is gray;" }
   \line { "And sad and tearful your eye of blue." }
   \line { "Ah, but Patrick, Patrick, alas, 'tis you!\"" }
   \line { "(Music part A:)" }
   \line { "The dawn was breaking. She heard below" }
   \line { "The two cocks shaking their wings to crow." }
   \line { "\"Oh, hush you! hush you! both red and gray," }
   \line { "Or will you hurry my love away?\"" }
   \line { "(Music part B:)" }
   \line { "\"Oh, hush your crowin', both gray and red," }
   \line { "Or he will be goin' to join the dead;" }
   \line { "And cease you calling his ghost to the mold;" }
   \line { "And I'll come crowning your wings with gold.\"" }
   \line { "(Music part A:)" }
   \line { "When all but dreaming was Fanny Power," }
   \line { "A light came streaming beneath her bower;" }
   \line { "And on the morrow, when they awoke," }
   \line { "They knew that sorrow her heart had broke." }
   \line { "(Music part B)
" }
 
}
