\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1208#setting20970"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1208#setting20970" {"https://thesession.org/tunes/1208#setting20970"}}}
	title = "Rusty Gulley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \mixolydian   \repeat volta 2 {   g'8    a'8    b'8    c''8    
d''4    g'4    d''4    g'4    \bar "|"   f'4    a'4   ~    a'4    c''4    b'4   
 a'4    \bar "|"   g'8    a'8    b'8    c''8    d''4    g'4    b'4    g'4    
\bar "|"   d'4    g'4   ~    g'4    b'4    a'4    g'4    }     
\repeat volta 2 {   g''2    f''2    e''2    \bar "|"   f''4    d''4   ~    d''4 
   f''4    e''4    d''4    \bar "|"   c''2    b'2    a'2    \bar "|"   b'4    
g'4   ~    g'4    b'4    a'4    g'4    }     \repeat volta 2 {   g'8    a'8    
b'8    c''8    d''4    g'4    b'4    g'4    \bar "|"   f'4    a'4    a'4    
c''4    b'4    a'4    \bar "|"   g'8    a'8    b'8    c''8    d''4    g'4    
b'4    g'4    \bar "|"   d'4    g'4    g'4    b'4    a'4    g'4    }     
\repeat volta 2 {   g''2    f''2    e''4    g''4    \bar "|"   f''4    d''4    
d''4    f''4    e''4    d''4    \bar "|"   c''4    g''4    b'4    g''4    a'4   
 g''4    \bar "|"   b'4    g'4    g'4    b'4    a'4    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
