\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/298#setting13051"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/298#setting13051" {"https://thesession.org/tunes/298#setting13051"}}}
	title = "Ballydesmond, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \mixolydian   \repeat volta 2 {   a'8.    b'16    a'8    g'8   
 \bar "|"   e'8    fis'8    g'8    e'8    \bar "|"   a'8.    b'16    a'8    g'8 
   \bar "|"   a'8    d''8    d''16    e''16    d''16    c''16    \bar "|"   
a'8.    b'16    a'8    g'8    \bar "|"   e'8    fis'8    g'4    \bar "|"   a'8  
  b'8    c''8    e'8    \bar "|"   e'8    d'8    d'4    }   a'8    d''8    d''4 
   \bar "|"   e''8    d''8    c''4    \bar "|"   e''8    d''8    c''8    d''8   
 \bar "|"   e''8    a''8    a''16    b''16    a''16    g''16    \bar "|"   e''8 
   d''8    c''4    \bar "|"   e''8    d''8    c''4    \bar "|"   a'8.    b'16   
 c''8    d''8    \bar "|"   e''8    fis''8    g''8.    fis''16    \bar "|"   
e''8    d''8    c''8    d''8    \bar "|"   e''8    a'8    c''8    d''8    
\bar "|"   e''8.    d''16    c''8    d''8    \bar "|"   e''8    a''8    a''16   
 b''16    a''16    g''16    \bar "|"   e''16    fis''16    g''8    d''8.    
b'16    \bar "|"   c''8    e''8    d''8.    b'16    \bar "|"   a'8    b'8    
c''8    e'8    \bar "|"   e'8    d'8    d'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
