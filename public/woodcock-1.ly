\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Donough"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/580#setting580"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/580#setting580" {"https://thesession.org/tunes/580#setting580"}}}
	title = "Woodcock, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   b'8  \repeat volta 2 {   d''8    b'8    g'8    c''8   
 g'8    e'8  \bar "|"   d'8    b'8    d'8    g'4. ^"~"  \bar "|"   b'4. ^"~"    
d''8    c''8    b'8  \bar "|"   d''8    c''8    b'8    a'8    b'8    c''8  
\bar "|"     d''8    b'8    g'8    c''8    g'8    e'8  \bar "|"   d'8    b'8    
d'8    g'4. ^"~"  \bar "|"   b'4. ^"~"    d''8    e''8    d''8  
} \alternative{{   a'8    b'8    a'8    g'4. ^"~"  } {   a'8    b'8    a'8    
g'4    b'8  \bar "||"     d''8    b'8    g'8    e''8    c''8    a'8  \bar "|"   
fis''4. ^"~"    g''4    d''8  \bar "|"   e''16    fis''16    g''8    e''8    
d''8    b'8    g'8  \bar "|"   d''8    c''8    b'8    a'8    b'8    c''8  
\bar "|"     d''8    b'8    g'8    e''8    c''8    a'8  \bar "|"   fis''4. 
^"~"    g''4    d''8  \bar "|"   e''16    fis''16    g''8    e''8    d''8    
b'8    g'8  \bar "|"   a'8    b'8    a'8    g'8    b'8    c''8  \bar "|"     
d''8    b'8    g'8    e''8    c''8    a'8  \bar "|"   fis''4. ^"~"    g''4    
g''8  \bar "|"   e''16    fis''16    g''8    e''8    d''8    b'8    g'8  
\bar "|"   d''8    c''8    b'8    a'8    b'8    c''8  \bar "|"     d''8    b'8  
  g'8    c''8    g'8    e'8  \bar "|"   d'8    b'8    d'8    g'4. ^"~"  
\bar "|"   b'4. ^"~"    d''8    e''8    d''8  \bar "|"   a'8    b'8    a'8    
g'4. ^"~"  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
