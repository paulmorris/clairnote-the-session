\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/50#setting22152"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/50#setting22152" {"https://thesession.org/tunes/50#setting22152"}}}
	title = "Jackie Coleman's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   b'8  \bar "|"   a'8 ^"D"   fis'8  
  fis'16    fis'16    fis'8      e'8 ^"Em"   fis'8    d'8 ^"D"   e'8  \bar "|"  
 fis'4 ^"D"   a'8    fis'8    b'8    fis'8    a'8    fis'8  \bar "|"   e'4 
^"Em"   b'8    e'8    d''8    e'8    b'8    e'8  \bar "|"   a'8 ^"D"   b'8    
d''8    e''8    \grace {    g''8 ^"Bm" }   fis''8    e''8    d''8    b'8  
\bar "|"       a'8 ^"D"   fis'8    fis'16    fis'16    fis'8      e'8 ^"Em"   
fis'8    d'8 ^"D"   e'8  \bar "|"   fis'4 ^"D"   a'8    fis'8    b'8    fis'8   
 a'8    fis'8  \bar "|"   e'4 ^"Em"   b'8    e'8    d''8    e'8    b'8    e'8  
\bar "|"   a'8 ^"D"   b'8    d''8    e''8    d''4    d''8  }     
\repeat volta 2 {   g''8  \bar "|"   fis''8 ^"D"   d''8  \grace {    e''8  }   
d''8    cis''8    d''4    d''8    g''8  \bar "|"   fis''8 ^"D"   d''8  
\grace {    e''8  }   d''8    cis''8    d''8    fis''8    a''8    fis''8  
\bar "|"   e''8 ^"A"   a'8    a'16    a'16    a'8    e''8    a'8    fis''8    
a'8  \bar "|"   e''8 ^"A"   a'8    a'16    a'16    a'8      e''8 ^"Em"   d''8   
 e''8    g''8  \bar "|"       fis''8 ^"D"   d''8  \grace {    e''8  }   d''8    
cis''8    d''4    d''8    g''8  \bar "|"   fis''8 ^"D"   d''8  \grace {    e''8 
 }   d''8    cis''8    d''8    fis''8    a''8    fis''8  \bar "|"   g''4 ^"G"   
g''8    fis''8    g''8    b''8    a''8    g''8  \bar "|"   fis''8 ^"D"   d''8   
 e''8 ^"A"   cis''8      d''4 ^"D"   d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
