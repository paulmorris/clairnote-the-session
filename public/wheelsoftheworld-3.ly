\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ger the Rigger"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1010#setting14227"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1010#setting14227" {"https://thesession.org/tunes/1010#setting14227"}}}
	title = "Wheels Of The World, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   b'8  \bar "|"   a'8    d'8    e'8    d'8    
fis'8    a'8    g'8    fis'8  \bar "|"   e'8    c'4. ^"~"    g8    c'8    e'8   
 g'8  \bar "|"   a'8    d'4. ^"~"    fis'8    a'8    d''8    e''8  \bar "|"   
f''8    e''8    d''8    c''8    a'8    d''8    d''8    c''8  \bar "|"   a'8    
d'8    e'8    d'8    fis'8    a'8    g'8    fis'8  \bar "|"   e'8    c'4. ^"~"  
  g8    c'8    e'8    g'8  \bar "|"   a'4    d''8    b'8    c''8    b'8    a'8  
  g'8  \bar "|"   e'8    d'8    d'8    e'8  \grace {    g'8  }   e'8    d'8    
d''8  \bar ":|."   e'8    d'8    d'8    e'8  \grace {    g'8  }   e'8    d'8    
a'8    b'8  \bar "||"   c''4.    e''8    g''8    e''8    c''8    a'8  \bar "|"  
 d''4    a''8    d''8    b''8    d''8    a''8    d''8  \bar "|"   c''4. ^"~"    
d''8    e''8    d''8    cis''8    d''8  \bar "|"   e''8    a''4. ^"~"    e''8   
 d''8    a'8    b'8  \bar "|"   c''4.    e''8    g''8    e''8    c''8    a'8  
\bar "|"   d''4    a''8    d''8    b''8    d''8    a''8    d''8  \bar "|"   
a''8    b''8    a''8    g''8    e''8    fis''8    g''8    e''8  \bar "|"   
\tuplet 3/2 {   fis''8    g''8    a''8  }   g''8    e''8    d''4    a'8    b'8  
\bar ":|."   fis''4. ^"~"    e''8    d''4.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
