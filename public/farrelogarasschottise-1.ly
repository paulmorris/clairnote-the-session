\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1021#setting1021"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1021#setting1021" {"https://thesession.org/tunes/1021#setting1021"}}}
	title = "Farrel O'Gara's Schottise"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   a'8    fis'8  \bar "|"   e'4    
a'8    cis''8  \grace {    b'8    cis''8  }   b'4    \tuplet 3/2 {   a''8    
gis''8    fis''8  } \bar "|" \tuplet 3/2 {   e''8    d''8    cis''8  }   
\tuplet 3/2 {   d''8    cis''8    b'8  }   cis''8    a'8    fis'8    a'8  
\bar "|"   e'4    a'8    cis''8  \grace {    b'8    cis''8  }   b'4    a''8    
fis''8  \bar "|"   e''8    cis''8    b'8    d''8    cis''8    a'8    fis'8    
a'8  \bar "|"   <<   a'4    e'4   >>   a'8    cis''8  \grace {    b'8    cis''8 
 }   b'4    \tuplet 3/2 {   a''8    gis''8    fis''8  } \bar "|" \tuplet 3/2 {   
e''8    d''8    cis''8  }   \tuplet 3/2 {   d''8    cis''8    b'8  }   cis''8    
a'8    fis'8    a'8  \bar "|" <<   a'4    e'4   >>   a'8    cis''8  \grace {    
b'8    cis''8  }   b'4    a''8    fis''8  \bar "|"   e''8    cis''8    b'8    
cis''8    a'4  }     \repeat volta 2 {   e''4  \bar "|"   a''8    e''8    
fis''8    a''8    e''8    a''8    cis''8    e''8  \bar "|"   d''8    b'8    
cis''8    a'8    b'8    fis'8    fis'4  \bar "|"   a''8    e''8    fis''8    
a''8    e''8    a''8    cis'''8    a''8  \bar "|"   b''8    a''8    b''8    
cis'''8    a''4    a''8    b''8  \bar "|"     cis'''8    a''8    fis''8    a''8 
 \grace {    e''8    fis''8  }   e''4    cis''8    e''8  \bar "|"   d''8    b'8 
   cis''8    a'8    b'8    a'8    fis'8    a'8  \bar "|" <<   a'4    e'4   >>   
a'8    cis''8  \grace {    b'8    cis''8  }   b'4    a''8    fis''8  \bar "|"   
e''8    cis''8    b'8    cis''8    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
