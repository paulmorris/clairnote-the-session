\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/838#setting13997"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/838#setting13997" {"https://thesession.org/tunes/838#setting13997"}}}
	title = "Andy De Jarlis'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major \time 12/8   \repeat volta 2 {   a'8    \bar "|"   d'8  
  fis'8    a'8    d'8    fis'8    a'8    d'8    fis'8    a'8    b'4    a'8    
\bar "|"   d'8    fis'8    a'8    d'8    fis'8    a'8    a8    cis'8    e'8    
g'8    fis'8    e'8    \bar "|"     d'8    fis'8    a'8    d'8    fis'8    a'8  
  d'8    fis'8    a'8    b'4    g''8    \bar "|"   fis''4    e''8    a'8    b'8 
   cis''8    d''4.    d''4    }     \repeat volta 2 {   g''8    \bar "|"   
fis''8    a''8    fis''8    d''8    e''8    fis''8    a''4    g''8    b'4.    
\bar "|"   e''4    fis''8    e''4    d''8    cis''8    e''8    cis''8    a'4    
g''8    \bar "|"     fis''8    a''8    fis''8    d''8    e''8    fis''8    a''4 
   g''8    b'4    g''8    \bar "|"   fis''4    e''8    a'8    b'8    cis''8    
d''4.    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
