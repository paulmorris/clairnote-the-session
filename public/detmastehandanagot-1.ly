\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1168#setting1168"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1168#setting1168" {"https://thesession.org/tunes/1168#setting1168"}}}
	title = "Det Måste Hända Något"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/4 \key b \minor     d''8 ^"Bm"(   cis''8    b'8  -)   d''8 (   cis''8   
 b'8  -) \bar "|"   d''8 (   cis''8    b'8  -)   d''8 (   cis''8    b'8  -) 
\bar "|"   g'2. ^"G"( \bar "|"   g'4  -)   b'4    g'4  \bar "|"   fis'4. ^"D"   
g'8    fis'4  \bar "|"   e'4 ^"A"   d'4.    e'8    \bar "|"   fis'2. ^"F\#"( 
\bar "|"   fis'2.  -) \bar "|"       d''8 ^"Bm"(   cis''8    b'8  -)   d''8 (   
cis''8    b'8  -) \bar "|"   d''8 (   cis''8    b'8  -)   d''8 (   cis''8    
b'8  -) \bar "|"   g'2. ^"G"( \bar "|"   g'4  -)   b'4    g'4  \bar "|"   fis'4 
^"D"   a'4.    fis'8  \bar "|"   g'4 ^"G"   b'4    g'4  \bar "|"   fis'4 ^"D"   
a'4.    fis'8  \bar "|"       g'4 ^"G"   b'4    g'4  \bar "|"   fis'8 ^"D"   
d''8      g'8 ^"G"   e''8      e''8 ^"D"   d''8  \bar "|"   e'8 ^"Em"   d''8    
  d''4 ^"Bm"     cis''4 ^"F\#" \bar "|"   b'2. ^"Bm"( \bar "|"   b'2.  -) }     
  fis''4. ^"Bm"   e''8    d''4  \bar "|"   fis''4    e''4    d''4  \bar "|"   
e''4 ^"A"   cis''4.    d''8  \bar "|"   e''2    fis''4  \bar "|"   e''4.    
d''8    cis''4 ( \bar "|"   cis''4 ^"G" -)   d''4    cis''4 ^"F\#" \bar "|"   
b'2. ^"Bm"( \bar "|"   b'4  -)   a''4    g''4  \bar "|"       fis''4. ^"Bm"   
e''8    d''8    e''8  \bar "|"   fis''4    e''4    d''4  \bar "|"   e''4 ^"A"   
cis''4.    d''8  \bar "|"   e''2    fis''4  \bar "|"   e''4.    d''8    cis''4 
( \bar "|"   cis''4 ^"G" -)   d''4    cis''4 ^"F\#" \bar "|"   b'2. ^"Bm"( 
\bar "|"   b'4  -)   d''4    e''4  \bar ":|."   b'2.  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
