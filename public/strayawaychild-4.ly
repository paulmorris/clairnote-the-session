\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "BenH"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/134#setting25594"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/134#setting25594" {"https://thesession.org/tunes/134#setting25594"}}}
	title = "Strayaway Child, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \minor   a'8  \bar "|"   b'8 ^"Em"   e'8    e'8    g'8    e'8  
  e'8  \bar "|"   b8    e'8    e'8    g'4    a'8  \bar "|"   b'8    e'8    e'8  
  a'4    g'8  \bar "|"   fis'8 ^"D"   d'8    fis'8    a'8    d''8    c''8  
\bar "|"       b'8 ^"Em"   e'8    e'8    g'8    e'8    e'8  \bar "|"   b8    
e'8    e'8    g'4    a'8  \bar "|"   b'8 ^"Bm"   d''8    b'8    b'4    a'8  
\bar "|"   g'8 ^"Em"   e'8    e'8    e'4  }     a'8  \bar "|"   b'8 ^"Em"   
e''8    e''8    b'8    d''8    b'8  \bar "|"   d''8    b'8    g'8    a'8    g'8 
   a'8  \bar "|"   b'8    e''8    e''8    b'8    d''8    b'8  \bar "|"   d''8 
^"D"   b'8    g'8    a'4    a'8  \bar "|"       b'8 ^"Em"   e''8    e''8    b'8 
   d''8    b'8  \bar "|"   d''8    b'8    g'8    a'8    g'8    a'8  \bar "|"   
b'8 ^"Bm"   d''8    b'8    b'4    a'8  \bar "|"   g'8 ^"Em"   e'8    e'8    e'4 
   a'8  \bar "|"       b'8 ^"Em"   e''8    e''8    b'8    d''8    b'8  \bar "|" 
  d''8    b'8    g'8    a'8    g'8    a'8  \bar "|"   b'8    e''8    e''8    
b'8    d''8    b'8  \bar "|"   d''8 ^"D"   b'8    g'8    a'4    fis''8  
\bar "|"       g''8 ^"CMaj7"   fis''8    e''8    d''8    c''8    b'8  \bar "|"  
 a'8    g'8    a'8    b'8    g'8    e'8  \bar "|"   a'8 ^"Bm"   g'8    e'8    
d'8    b8    d'8  \bar "|"   e'4. ^"Em"^"~"    e'4  \bar "||"     
\repeat volta 2 {   d'8  \bar "|"   e'8 ^"Am"   a'8    g'8    e'8    d'8    e'8 
 \bar "|"   g'4. ^"Em"^"~"    b'8    a'8    g'8  \bar "|"   a'8 ^"Bm"   g'8    
e'8    d'8    b8    d'8  \bar "|"   e'8 ^"Em"   fis'8    e'8    e'4    d'8  
\bar "|"       e'8 ^"Am"   a'8    g'8    e'8    d'8    e'8  \bar "|"   g'4. 
^"Em"^"~"    b'8    a'8    g'8  \bar "|"   a'8 ^"Bm"   g'8    e'8    d'8    b8  
  d'8  \bar "|"   e'4. ^"Em"^"~"    e'4  }     \repeat volta 2 {   b'8  
\bar "|"   e''8 ^"Em"   fis''8    e''8    d''8    e''8    d''8  \bar "|"   c''8 
   b'8    a'8    b'8    g'8    e'8  \bar "|"   g'8    a'8    b'8    d''8    b'8 
   b'8  \bar "|"   a'8 ^"D"   g'8    e'8    d'4    c''8  \bar "|"       b'8 
^"CMaj7"   a'8    g'8    e'8    d'8    e'8  \bar "|"   g'8    a'8    b'8    
c''8    b'8    a'8  \bar "|"   b'8 ^"Bm"   a'8    g'8    e'8    a'8    g'8  
\bar "|"   e'4. ^"Em"^"~"    e'4  }     \repeat volta 2 {   a'8  \bar "|"   b'8 
^"Em"   e''8    e''8    e''8    fis''8    g''8  \bar "|"   fis''8 ^"D"   d''8   
 fis''8    e''8    d''8    a'8  \bar "|"   b'8 ^"Em"   e''8    e''8    e''8    
fis''8    g''8  \bar "|"   fis''8 ^"D"   d''8    fis''8    e''4    fis''8  
\bar "|"       g''8 ^"CMaj7"   fis''8    e''8    d''8    c''8    b'8  \bar "|"  
 a'8    g'8    a'8    b'8    g'8    e'8  \bar "|"   a'8 ^"Bm"   g'8    e'8    
d'8    b8    d'8  \bar "|"   e'4. ^"Em"^"~"    e'4  }     \repeat volta 2 {   
b'8  \bar "|"   e''8 ^"Em"   d''8    b'8    e''8    d''8    b'8  \bar "|"   a'8 
^"D"   g'8    a'8    b'8    g'8    e'8  \bar "|"   e''8 ^"Em"   d''8    b'8    
e''8    d''8    b'8  \bar "|"   a'8 ^"D"   g'8    a'8    b'4.  \bar "|"       
e''8 ^"Em"   d''8    b'8    g''8    fis''8    e''8  \bar "|"   d''8 ^"D"   c''8 
   b'8    a'8    g'8    a'8  \bar "|"   b'8 ^"Bm"   a'8    g'8    e'8    a'8    
g'8  \bar "|"   e'4. ^"Em"^"~"    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
