\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jimmydearing"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/795#setting26200"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/795#setting26200" {"https://thesession.org/tunes/795#setting26200"}}}
	title = "Praties They Grow Small, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   fis'4    a'4  \bar "|"   b'4.    cis''8    b'4    a'4 
 \bar "|"   fis'2    d''4    d''4  \bar "|"   d''2    cis''4    b'4  \bar "|"   
cis''2 (   cis''2    \bar "|"   cis''2  -)   d''4    cis''4    \bar "|"     
b'4.    cis''8    b'4    a'4  \bar "|"   fis'2    fis'4    a'4  \bar "|"   b'2 
(   b'2    \bar "|"   b'2  -)   \repeat volta 2 {   b'4    cis''4    \bar "|"   
  d''4.    a'8    d''4    e''4  \bar "|"   fis''2    g''4    fis''4  \bar "|"   
e''4.    cis''8    a'4    cis''4    \bar "|"   e''2    d''4    cis''4  \bar "|" 
    \bar "|"   b'4.    cis''8    b'4    a'4  \bar "|"   fis'2    d''4    d''4  
\bar "|"   d''2    e''4    d''4  \bar "|"   cis''2 (   cis''2    \bar "|"   
cis''2  -)   d''4    cis''4    \bar "|"     b'4.    cis''8    b'4    a'4  
\bar "|"   fis'4    e'4    fis'4    a'4  \bar "|"   b'2 (   b'2    \bar "|"   
b'2  -)   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}

\markup \column {
  \line { "with slow march feel" }

}
