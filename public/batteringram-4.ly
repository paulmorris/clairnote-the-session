\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "kjay_bc_box"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/382#setting13208"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/382#setting13208" {"https://thesession.org/tunes/382#setting13208"}}}
	title = "Battering Ram, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   b'8  \bar "|"   d''8    b'8    a'8    b'8    d''8    
a'8  \bar "|"   g'4. ^"~"    g'4    b'8  \bar "|"   d''8    b'8    a'8    b'8   
 d''8    a'8  \bar "|"   g'4. ^"~"    e''8    fis''8    g''8  \bar "|"   d''8   
 b'8    a'8    a'8    b'8    a'8  \bar "|"   g'8    e'8    d'8    g'4. ^"~"  
\bar "|"   a'8    g'8    e'8    a'4    b'8  \bar "|"   g'8    e'8    d'8    d'4 
   b'8  \bar "|"   d''8    b'8    a'8    b'8    d''8    a'8  \bar "|"   g'4. 
^"~"    g'4    b'8  \bar "|"   d''8    b'8    a'8    b'8    d''8    a'8  
\bar "|"   g'8    b'16    c''16    d''8    e''8    fis''8    g''8  \bar "|"   
d''8    b'8    a'8    a'8    b'8    a'8  \bar "|"   g'8    e'8    d'8    g'4. 
^"~"  \bar "|"   a'8    g'8    e'8    a'4    b'8  \bar "|"   g'8    e'8    d'8  
  d'4.  \bar "|"   d''8    e''8    g''8    a''4    b''8  \bar "|"   g''8    
e''8    d''8    e''8    d''8    b'8  \bar "|"   d''8    e''8    g''8    a''4    
b''8  \bar "|"   g''8    a''8    g''8    g''4    e''8  \bar "|"   d''8    e''8  
  g''8    a''4    b''8  \bar "|"   g''8    e''8    d''8    e''8    d''8    b'8  
\bar "|"   d''8    b'8    g'8    a'4    b'8  \bar "|"   g'8    e'8    d'8    
d'4.  \bar "|"   d''8    e''8    g''8    a''4    b''8  \bar "|"   g''8    e''8  
  d''8    e''8    d''8    b'8  \bar "|"   d''8    e''8    g''8    a''4    b''8  
\bar "|"   g''8    a''8    g''8    g''4    e''8  \bar "|"   d''8    e''8    
g''8    a''4    b''8  \bar "|"   g''8    e''8    d''8    e''8    d''8    b'8  
\bar "|"   d''8    b'8    g'8    a'4    b'8  \bar "|"   g'8    e'8    d'8    
d'4  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
