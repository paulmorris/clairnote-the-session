\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JeffK627"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/647#setting647"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/647#setting647" {"https://thesession.org/tunes/647#setting647"}}}
	title = "Tha Mi Sgith"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \repeat volta 2 {   a'8    a'8    a''4    \bar "|"   
g''16    e''8.    g''4    \bar "|"   e''8.    d''16    b'8    a'8    \bar "|"   
g'8    a'8    b'16    g'8.    \bar "|"     a'8    a'8    a''4    \bar "|"   
g''16    e''8.    g''4    \bar "|"   e''8.    d''16    b'8    e''8    \bar "|"  
 a'4    a'4    }     \repeat volta 2 {   e''8.    d''16    b'8    a'8    
\bar "|"   g'8    a'8    b'16    g'8.    \bar "|"   e''8.    d''16    b'8    
a'8    \bar "|"   b'16    d''8.    d''4    \bar "|"     e''8.    d''16    b'8   
 a'8    \bar "|"   g'8    a'8    b'16    g'8.    \bar "|"   e''8    d''8    
b'16    e''8.    \bar "|"   a'4    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
