\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/883#setting14066"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/883#setting14066" {"https://thesession.org/tunes/883#setting14066"}}}
	title = "Laurel Tree, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   g''8    \bar "|"   e''8    a'8   
 a'16    a'16    a'8    e''4    d''8    c''8    \bar "|"   b'8    g'8    g'16   
 g'16    g'8    b'8    e'8    e'8    a''8    \bar "|"   e''8    a'8    a'16    
a'16    a'8    e''4    d''8    c''8    \bar "|"   b'8    g'8    a'8    g'8    
g''8    e''8    e''8    }     
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
