\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "daveb"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1037#setting1037"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1037#setting1037" {"https://thesession.org/tunes/1037#setting1037"}}}
	title = "Shepherd's Wife, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key e \minor   \repeat volta 2 {   d'4    \bar "|"   g'2    a'4    
\bar "|"   b'2    c''4    \bar "|"   d''2    g''4    \bar "|"   g''4    fis''4  
  d''4    \bar "|"     e''4.    fis''8    g''4    \bar "|"   d''4.    c''8    
b'4    \bar "|"   a'4.    b'8    g'4    \bar "|"   fis'4.    e'8    d'4    
\bar "|"     g'2    a'4    \bar "|"   b'2    c''4    \bar "|"   d''2    g''4    
\bar "|"   g''4    fis''4    d''4    \bar "|"     e''4.    fis''8    g''4    
\bar "|"   a''4.    g''8    fis''4    \bar "|"   g''2.    \bar "|"   g''2    }  
   \repeat volta 2 {   g''8    a''8    \bar "|"   b''2    b''4    \bar "|"   
a''4.    g''8    fis''4    \bar "|"   g''4.    a''8    g''4    \bar "|"   
fis''4.    e''8    d''4    \bar "|"     e''4.    fis''8    g''4    \bar "|"   
d''4.    c''8    b'4    \bar "|"   a'4.    b'8    g'4    \bar "|"   fis'4.    
e'8    d'4    \bar "|"     b''2    b''4    \bar "|"   a''4.    g''8    fis''4   
 \bar "|"   g''4.    a''8    g''4    \bar "|"   fis''4.    e''8    d''4    
\bar "|"     e''4.    fis''8    g''4    \bar "|"   a''4.    g''8    fis''4    
\bar "|"   g''2.    \bar "|"   g''2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
