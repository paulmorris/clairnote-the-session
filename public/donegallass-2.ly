\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fynnjamin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1497#setting14885"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1497#setting14885" {"https://thesession.org/tunes/1497#setting14885"}}}
	title = "Donegal Lass, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian   \repeat volta 2 {   a'8    cis''16    d''16    
e''8    a''8    e''8    d''8  \bar "|"   cis''8    d''8    b'8    a'4 ^"~"    
a'8  \bar "|"   g'8    b'8    d''8    g'4 ^"~"    g'8  \bar "|"   fis'8    a'8  
  d''8    fis''4. ^"~"  \bar "|"   a'8    cis''16    d''16    e''8    a''8    
e''8    d''8  \bar "|"   cis''8    d''8    b'8    a'4    a''8  \bar "|"   a''8  
  e''8    d''8    cis''8    d''8    b'8  \bar "|"   a'8    b'8    g'8    a'4 
^"~"    a'8  }   \repeat volta 2 {   g'8    b'8    d''8    g'4 ^"~"    g'8  
\bar "|"   fis'8    a'8    d''8    fis''4 ^"~"    fis''8  \bar "|"   e''4 ^"~"  
  e''8    e''8    cis''8    a'8  \bar "|"   cis''16    d''16    e''8    cis''8  
  d''8    b'8    a'8  \bar "|"   g'8    b'8    d''8    g'4 ^"~"    g'8  
\bar "|"   fis'8    a'8    d''8    fis''4 ^"~"    fis''8  \bar "|"   a''8    
e''8    d''8    cis''8    d''8    b'8  \bar "|"   a'8    b'8    g'8    a'4 
^"~"    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
