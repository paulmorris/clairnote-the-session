\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "The Whistler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/795#setting795"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/795#setting795" {"https://thesession.org/tunes/795#setting795"}}}
	title = "Praties They Grow Small, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   e'4    fis'4  \bar "|"   g'4    fis'4    e'4    dis'4 
 \bar "|"   e'2    e'4    fis'4  \bar "|"   e'2    e'4    fis'4  \bar "|"     
e'2    e'4    fis'4  \bar "|"   g'4    fis'4    g'4    a'4  \bar "|"   b'2    
a'4    b'4  \bar "|"   c''4    b'4    a'4    g'4  \bar "|"     fis'2    e'4    
fis'4  \bar "|"   g'4    fis'4    e'4    dis'4  \bar "|"   e'2    e'4    fis'4  
\bar "|"   e'2    e'4    fis'4  \bar "|"   e'2  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
