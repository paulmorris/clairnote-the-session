\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Shan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1208#setting22651"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1208#setting22651" {"https://thesession.org/tunes/1208#setting22651"}}}
	title = "Rusty Gulley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key b \minor   b'8    cis''8    d''8    e''8    fis''4    b'4    
d''4    b'4    \bar "|"   b'8    cis''8    e''4    e''4    d''4    cis''4    
fis'4    \bar "|"   b'8    cis''8    d''8    e''8    fis''4    b'4    d''4    
b'4    \bar "|"   b'8    cis''8    g''4    fis''4    d''4    e''8    e''8    
d''8    cis''8    \bar "|"     b'8    cis''8    d''8    e''8    fis''4    b'4   
 d''4    b'4    \bar "|"   b'8    cis''8    e''4    e''4    d''4    cis''4    
fis'4    \bar "|"   b'8    cis''8    d''8    e''8    fis''4    b'4    d''4    
b'4    \bar "|"   b'8    cis''8    g''4    fis''4    d''4    e''2    \bar "|"   
  b''2    a''2    g''4.    b''8    \bar "|"   a''4    e''2    g''4    fis''4    
e''4    \bar "|"   g''4.    a''8    fis''2    e''4    d''8    e''8    \bar "|"  
 fis''4    b'4    b'4    d''4    cis''4    b'4    \bar "|"     b''2    a''2    
g''4.    b''8    \bar "|"   a''4    e''2    g''4    fis''4    e''4    \bar "|"  
 g''4.    a''8    fis''2    e''4    d''8    e''8    \bar "|"   fis''4    b'4    
b'4    d''4    cis''2    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
