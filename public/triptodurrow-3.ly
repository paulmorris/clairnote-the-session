\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tøm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/891#setting25330"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/891#setting25330" {"https://thesession.org/tunes/891#setting25330"}}}
	title = "Trip To Durrow, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \major   e'4    e'8    gis'8    b'8    e'8    gis'8    b'8  
\bar "|"   e''8    gis''8    fis''8    e''8    cis''4.    dis''8  \bar "|"   
e''8    cis''8    cis''8    b'8    e''8    cis''8    cis''8    b'8  \bar "|"   
gis'8    b'8    e'8    fis'8    gis'8    fis'8    fis'4  \bar "|"     e'4    
e'8    gis'8    b'8    e'8    gis'8    b'8  \bar "|"   e''8    gis''8    fis''8 
   e''8    cis''4.    dis''8  \bar "|"   e''8    cis''8    cis''8    b'8    
gis'8    b'8    e''8    cis''8  \bar "|"   b'8    gis'8    fis'8    a'8    
gis'8    e'8    e'4  }     \repeat volta 2 {   e''8    dis''8    e''8    fis''8 
   gis''8    fis''8    gis''8    a''8  \bar "|"   b''8    gis''8    e''8    
gis''8    a''8    gis''8    fis''8    e''8  \bar "|" \tuplet 3/2 {   cis''8    
dis''8    e''8  }   fis''8    gis''8    a''8    fis''8    cis'''8    fis''8  
\bar "|"   a''8    fis''8    cis'''8    fis''8    a''8    gis''8    fis''8    
gis''8  \bar "|"     e''4    e''8    fis''8    gis''8    fis''8    gis''8    
a''8  \bar "|"   b''8    gis''8    e''8    gis''8    a''8    gis''8    fis''8   
 e''8  \bar "|" \tuplet 3/2 {   cis''8    dis''8    e''8  }   fis''8    gis''8   
 a''8    cis'''8    b''8    a''8  \bar "|"   gis''8    e''8    fis''8    dis''8 
   e''4    e''8    fis''8  \bar "|"     gis''8    e''8    fis''8    dis''8    
e''4    e''8    fis''8  \bar "|"   gis''8    e''8    fis''8    e''8    cis''4.  
  dis''8  \bar "|"   e''8    cis''8    cis''8    b'8    e''8    cis''8    
cis''8    b'8  \bar "|"   gis'8    b'8    e'8    fis'8    gis'8    fis'8    
fis'4  \bar "|"     e'4    e'8    gis'8    b'8    e'8    gis'8    b'8  \bar "|" 
  e''8    gis''8    fis''8    e''8    cis''4.    dis''8  \bar "|"   e''8    
cis''8    cis''8    b'8    gis'8    b'8    e''8    cis''8  \bar "|"   b'8    
gis'8    fis'8    a'8    gis'8    e'8    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
