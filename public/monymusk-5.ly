\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1387#setting14752"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1387#setting14752" {"https://thesession.org/tunes/1387#setting14752"}}}
	title = "Monymusk"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a''8    fis''8  \repeat volta 2 {   e''8    a'8    
cis''8    fis''8    e''8    a'8    cis''8    fis''8  \bar "|"   e''8    a'8    
cis''8    a'8    b'8    cis''8    d''8    fis''8  \bar "|"   e''16    a'8.    
cis''8    fis''8    e''8    a'8    cis''8    e''8  } \alternative{{   fis''8    
d''8    b'8    d''8    cis''8    a'8    \tuplet 3/2 {   a''8    gis''8    fis''8 
 } } {   fis''8    d''8    b'8    d''8    cis''8    a'8    a'4  \bar "||"   
a''8    e''8    cis''8    e''8    fis''8    e''8    cis''8    e''8  \bar "|"   
a''8    e''8    cis''8    e''8    fis''8    b'8    b'4  \bar "|"   a''8    e''8 
   cis''8    e''8    fis''8    e''8    cis''8    e''8  \bar "|"   fis''8    
d''8    b'8    d''8    cis''8    a'8    a'8    gis''8  \bar "|"   a''8    e''8  
  cis''8    e''8    fis''8    e''8    cis''8    e''8  \bar "|"   a''8    e''8   
 cis''8    e''8    fis''8    b'8    b'8    gis''8  \bar "|" \tuplet 3/2 {   a''8 
   b''8    a''8  }   \tuplet 3/2 {   gis''8    a''8    gis''8  }   \tuplet 3/2 {  
 fis''8    gis''8    fis''8  }   e''8    cis''8  \bar "|"   d''8    e''8    
fis''8    gis''8    a''4    \tuplet 3/2 {   a''8    gis''8    fis''8  } 
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
