\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "pchaffee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/553#setting13514"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/553#setting13514" {"https://thesession.org/tunes/553#setting13514"}}}
	title = "Drunken Sailor's, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   d'8    g'8    a'8    d''8    d'8    g'8    a'8    
d''8  \bar "|"   d'8    f'8    a'8    c''8    d'8    f'8    a'8    c''8  
\bar "|"   d'8    g'8    a'8    d''8    d'8    g'8    a'8    d''8  \bar "|"   
c''8    bes'8    a'8    g'8    e'16    f'16    g'8    a'8    g'8    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
