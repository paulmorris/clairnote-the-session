\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/989#setting989"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/989#setting989" {"https://thesession.org/tunes/989#setting989"}}}
	title = "Cloch Na Ceithre Mhile"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   a4    e'8    a8    a4 ^"~"    e'8    a8  \bar "|"   
a4 ^"~"    g'8    e'8    d'8    b8    a8    b8  \bar "|"   g4  \tuplet 3/2 {   
b8    a8    g8  }   d'8    g8    b8    a8  \bar "|"   g8    b8    b4 ^"~"    g8 
   b8    d'8    b8  \bar "|"     a4    e'8    a8    a4 ^"~"    a'8    b'8  
\bar "|"   b'16    b'16    a'8    b'8    g'8    a'8    g'8    a'8    b'8  
\bar "|"   b'16    b'16    a'8    b'8    g'8    a'8    g'8    e'8    a'8  
\bar "|"   g'8    e'8    d'8    b8    g8    a8    a8    g8  }     
\repeat volta 2 { \tuplet 3/2 {   a8    d'8    a'8  }   b'8    a'8    e''8    
a'8    b'8    a'8  \bar "|"   a8    e'8    e'4 ^"~"    d'4    b8    g8  
\bar "|" \grace {    b8  }   a8    g8    d'8    g8    e'8    g8    d'4  
\bar "|" \tuplet 3/2 {   d''8    e''8    d''8  }   \tuplet 3/2 {   b'8    d''8    
b'8  }   \tuplet 3/2 {   g'8    a'8    g'8  }   d'8    a8  \bar "|"     a8    
e'8    a'8    a'8    a'8    b'8  \tuplet 3/2 {   c''8    d''8    e''8  } 
\bar "|"   a'8    g'8    e'8    d'8    e'4 ^"~"    a'8    b'8  \bar "|"   b'16  
  b'16    a'8    b'8    g'8    a'8    g'8    e'8    a'8  \bar "|"   g'8    e'8  
  d'8    b8    g8    a8  \grace {    b8  }   a8    g8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
