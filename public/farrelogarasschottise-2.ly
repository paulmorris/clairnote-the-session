\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1021#setting14244"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1021#setting14244" {"https://thesession.org/tunes/1021#setting14244"}}}
	title = "Farrel O'Gara's Schottise"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   d'4    g'8.    b'16    a'4    \tuplet 3/2 {   g''8    
fis''8    e''8  }   \bar "|"   \tuplet 3/2 {   d''8    c''8    b'8  }   
\tuplet 3/2 {   c''8    b'8    a'8  }   b'8.    g'16    e'8.    g'16    \bar "|" 
  d'4    g'8.    b'16    \tuplet 3/2 {   a'8    b'8    a'8  }   g''8.    e''16   
 \bar "|"   d''8.    b'16    a'8.    c''16    b'8.    g'16    e'8.    g'16    
\bar ":|."   d''8.    b'16    a'8.    b'16    g'4.    d''8    \bar "||"   g''8. 
   d''16    e''8.    g''16    d''8.    g''16    b'8.    d''16    \bar "|"   
c''8.    a'16    b'8.    g'16    a'8.    e'16    \tuplet 3/2 {   e'8    e'8    
e'8  }   \bar "|"   g''8.    d''16    e''8.    g''16    d''8.    g''16    b''8. 
   g''16    \bar "|"   a''8.    gis''16    a''8.    b''16    g''!4.    a''8    
\bar "|"   b''8.    g''16    \tuplet 3/2 {   e''8    fis''8    g''8  }   d''4    
\tuplet 3/2 {   b'8    c''8    d''8  }   \bar "|"   c''4    \tuplet 3/2 {   b'8   
 a'8    g'8  }   a'8.    g'16    e'8.    g'16    \bar "|"   d'4    g'8.    b'16 
   a'4    g''8.    e''16    \bar "|"   d''8.    b'16    \tuplet 3/2 {   a'8    
b'8    c''8  }   b'8.    g'16    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
