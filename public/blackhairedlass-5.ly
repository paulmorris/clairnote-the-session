\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "thooom_sb"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/237#setting24997"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/237#setting24997" {"https://thesession.org/tunes/237#setting24997"}}}
	title = "Blackhaired Lass, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \bar "|"   cis''4    a'8    b'8    cis''8    
d''8    e''8    cis''8  \bar "|"   d''4    g''8    d''8    b'8    g'8    b'8    
d''8  \bar "|"   cis''8    b'8    a'8    b'8    cis''8    d''8    e''8    
cis''8  \bar "|"   d''8    fis''8    e''8    cis''8    a'4.    b'8  \bar "|"    
 \bar "|"   cis''4    a'8    b'8    cis''8    d''8    e''8    cis''8  \bar "|"  
 d''4    g''8    d''8    b'8    g'8    b'8    d''8  \bar "|"   cis''8    b'8    
a'8    b'8    cis''8    d''8    e''8    cis''8  \bar "|"   d''8    fis''8    
e''8    cis''8    a'4.    b'8  \bar "|"     cis''8    d''8    e''8    fis''8    
g''4    e''8    g''8  \bar "|"   fis''4    d''8    fis''8    g''8    fis''8    
e''8    d''8  \bar "|"   cis''8    d''8    e''8    fis''8    g''8    e''8    
a''8    g''8  \bar "|"   fis''8    d''8    e''8    cis''8    a'4.    b'8  
\bar "|"     cis''8    d''8    e''8    fis''8    g''4    e''8    g''8  \bar "|" 
  fis''4    d''8    fis''8    g''4    fis''8    g''8  \bar "|"   a''8    fis''8 
   g''8    e''8    fis''8    d''8    e''8    cis''8  \bar "|"   d''8    fis''8  
  e''8    cis''8    a'4.    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
