\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "janglecrow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/201#setting23116"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/201#setting23116" {"https://thesession.org/tunes/201#setting23116"}}}
	title = "Johnny Doherty's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   fis'4    fis'8    e'8    fis'8    g'8    a'8    g'8  
\bar "|"   a'8    d''8  \grace {    e''8  }   d''8    cis''8    d''4    e''8    
a''8  \bar "|"   fis''8    d''8    e''8    d''8    cis''8    a'8    b'8    d''8 
 \bar "|"   c''8    a'8    g'8    e'8    d'16    d'16    d'8    d'8    e'8  
\bar "|"     fis'16    fis'16    fis'8  \grace {    g'8  }   fis'8    e'8    
fis'8    g'8    a'8    g'8  \bar "|"   a'8    d''8  \grace {    e''8  }   d''8  
  cis''8    d''4    e''8    g''8  \bar "|"   fis''8    d''8    e''8    d''8    
cis''8    a'8    b'8    d''8  \bar "|"   c''8    a'8    g'8    e'8    fis'8    
d'8    d'8    g''8  \bar "||"     \grace {    fis''8    g''8  }   fis''4    
e''8    d''8    cis''8    a'8    a'8    g''8  \bar "|"   fis''8    d''8    e''8 
   cis''8    d''8    fis''8    a''8    g''8  \bar "|" \grace {    fis''8    
g''8  }   fis''4    e''8    d''8    cis''8    a'8    b'8    d''8  \bar "|"   
c''8    a'8    g'8    e'8    d'16    d'16    d'8    d'8    g''8  \bar "|"     
\grace {    fis''8    g''8  }   fis''4    e''8    d''8    cis''8    a'8    a'8  
  g''8  \bar "|"   fis''8    d''8    e''8    cis''8    d''8    fis''8    e''8   
 g''8  \bar "|" \grace {    fis''8    g''8  }   fis''4    e''8    d''8    
cis''8    a'8    b'8    d''8  \bar "|"   c''8    a'8    g'8    e'8    fis'8    
d'8    d'8    e'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
