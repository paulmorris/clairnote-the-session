\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeffery"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/736#setting13818"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/736#setting13818" {"https://thesession.org/tunes/736#setting13818"}}}
	title = "Jump At The Sun"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \minor   d'8.    f'16    a'8    gis'4    a'8  \bar "|"   d'8.  
  f'16    a'8    gis'4    a'8  \bar "|"   d''8    a'8    a'8    d''8    a'8    
a'8  \bar "|"   bes'4 ^"~"    g'8    f'8    g'8    e'8  \bar "|"   d'8.    f'16 
   a'8    gis'4    a'8  \bar "|"   d'8.    f'16    a'8    gis'4    a'8  
\bar "|"   d''8    a'8    a'8    bes'8    a'8    g'8  \bar "|"   f'8    g'8    
e'8    d'4    a'8  }   \repeat volta 2 {   d''4    d''4    e''8    f''8  
\bar "|"   e''4.    g''8    f''8    e''8  \bar "|"   d''4    d''4    e''8    
f''8  \bar "|"   e''4.    a'8    bes'8    a'8  \bar "|"   d''8    a'8    a'8    
d''8    e''8    f''8  \bar "|"   e''4.    g''8    f''8    e''8  \bar "|"   d''8 
   a'8    a'8    bes'8    a'8    g'8  \bar "|"   f'8    g'8    e'8    d'4    
a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
