\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bsykes62"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/598#setting598"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/598#setting598" {"https://thesession.org/tunes/598#setting598"}}}
	title = "Countess Cathleen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \minor   d'8  \repeat volta 2 {   e'8    b'8    b'8    b'8    
a'8    g'8    a'8    d'8    d'8  \bar "|"   fis'8    g'8    d''8    d''8    
c''8    b'8    a'8    g'8    d''8  \bar "|"   fis''8    g''8    fis''8    d''8  
  c''8    b'8    a'8    b'8    c''8  } \alternative{{   d''8    d'8    c''8    
d'8    b'8    d'8    a'8    g'8    fis'8  } {   d''8    g'8    e''8    g'8    
d''8    c''8    fis''8    g''8    a''8  \bar "||"     b''4    b''8    b''8    
a''8    g''8    fis''8    g''8    a''8  \bar "|"   b''4    d''8    c''8    d''8 
   g''8    d''8    g''8    a''8  \bar "|"   b''4    b''8    b''8    a''8    
g''8    fis''8    g''8    a''8  \bar "|"   b''4    d''8    c''8    d''8    g''8 
   d''8    g''8    b''8  \bar "|"     d'''4    d'''8    c'''8    b''8    a''8   
 b''8    a''8    g''8  \bar "|"   a''8    fis''8    d''8    g''8    e''8    
c''8    fis''8    b'8    d''8  \bar "|"   e''8    c''8    d''8    b'8    c''8   
 a'8    b'8    g'8    a'8  \bar "|"   fis'16    g'16    d''8    g'8    a'8    
g'8    fis'8    g'4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
