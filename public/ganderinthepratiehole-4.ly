\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/401#setting26368"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/401#setting26368" {"https://thesession.org/tunes/401#setting26368"}}}
	title = "Gander In The Pratie Hole, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   a'8  \repeat volta 2 { \tuplet 3/2 {   fis'8 -.   g'8 
-.   a'8 -. }   d'8    \tuplet 3/2 {   fis'8 -.   g'8 -.   a'8 -. }   d'8  
\bar "|"   g'4. ^"~"    e'8    fis'8    g'8  \bar "|" \tuplet 3/2 {   fis'8 -.   
g'8 -.   a'8 -. }   d'8    \tuplet 3/2 {   fis'8 -.   g'8 -.   a'8 -. }   d'8  
\bar "|"   g'8    e'8    d'8  \grace {    g'8  }   \tuplet 3/2 {   d'8    d'8    
d'8  }   a'8  \bar "|"     fis'8    a'8    d'8    fis'8    a'8    d'8  \bar "|" 
  g'8    fis'8    g'8    e'8    fis'8    g'8  \bar "|" \tuplet 3/2 {   g''8 -.   
fis''8 -.   e''8 -. }   d''8    e''8    cis''8    a'8  \bar "|"   g'8    e'8    
d'8  \grace {    g'8  }   d'4    a'8  }     a'8    d''8    d''8  \grace {    
e''8  }   d''8    e''8    d''8  \bar "|"   cis''!8    a'8    b'8    cis''8    
a'8    g'8  \bar "|"   a'8    d''8    d''8  \grace {    e''8  }   d''8    e''8  
  d''8  \bar "|"   cis''!8    a'8    b'8    cis''4.  \bar "|"     a'8    d''8   
 d''8  \grace {    e''8  }   d''8    e''8    d''8  \bar "|"   cis''!8    a'8    
b'8    cis''8    d''8    e''8  \bar "|" \tuplet 3/2 {   g''8 -.   fis''8 -.   
e''8 -. }   d''8    e''8    cis''8    a'8  \bar "|"   g'8    e'8    d'8  
\grace {    g'8  }   d'4    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
