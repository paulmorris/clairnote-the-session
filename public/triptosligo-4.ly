\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/397#setting13239"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/397#setting13239" {"https://thesession.org/tunes/397#setting13239"}}}
	title = "Trip To Sligo, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   \repeat volta 2 {   b8    \bar "|" <<   b4    e'4   
>>   c''8    b'8    g'8    e'8    \bar "|"   d'4    d''8    a'8    fis'8    d'8 
   \bar "|" <<   b4    e'4   >>   c''8    b'8    a'8    fis'8    \bar "|"   g'8 
   fis'8    e'8    e''4    e''16 (   fis''16    \bar "|"     g''8  -)   fis''8  
  e''8    d''8    c''8    b'8    \bar "|"   a'16 (   b'16    a'8  -)   g'8 -.   
fis'8    e'8    d'8    \bar "|"   e'8    fis'8    g'8    a'8    b'8    c''8    
\bar "|"   b'8    g'8    fis'8    e'4    }     \repeat volta 2 {   b'8    
\bar "|"   e''8    b'8    e''8    g''16 (   b''8.  -)   e''8 -.   \bar "|"   
d''8    a'8    d''8    fis''16 (   a''8.  -)   fis''8 -.   \bar "|"   e''8    
b'8    e''8    g''16 (   b''8.  -)   e''8 -.   \bar "|"   fis''16 (   a''8.  -) 
  fis''8 -.   g''16 (   b''8.  -)   e''8 -.   \bar "|"     fis''16 (   a''8.  
-)   fis''8 -.   g''8    fis''8    e''8    \bar "|"   d''8    \grace {    e''16 
(   d''16  }   c''8  -)   d''8    a'8    fis'8    d'8    \bar "|"   e'8    
fis'8    g'8    a'8    b'8    c''8    \bar "|"   b'8    g'8    fis'8    e'4    
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
