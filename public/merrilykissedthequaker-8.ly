\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/70#setting24522"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/70#setting24522" {"https://thesession.org/tunes/70#setting24522"}}}
	title = "Merrily Kissed The Quaker"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key g \major   \repeat volta 2 {   d'8    \bar "|"   g'8    a'8    
b'8    d'4.    b'8    c''8    a'8    b'8    g'8    e'8    \bar "|"   g'8    a'8 
   b'8    d'8    e'8    g'8    a'8    b'8    g'8    a'8    g'8    e'8    
\bar "|"     g'8    a'8    b'8    d'4    b'8    c''4    a'8    b'8    g'8    
e'8    \bar "|"   g'8    a'8    b'8    d'4    e'8    g'4.    g'4    }     
\repeat volta 2 {   a'8    \bar "|"   b'8    g'8    g'8    a'8    g'8    g'8    
b'8    g'8    g'8    a'8    g'8    e'8    \bar "|"   g'8    a'8    b'8    d'8   
 e'8    g'8    a'8    b'8    a'8    a'8    g'8    a'8    \bar "|"     b'8    
g'8    g'8    a'8    g'8    g'8    b'8    g'8    g'8    a'8    g'8    e'8    
\bar "|"   g'8    a'8    b'8    d'4    e'8    g'4.    g'4    }     
\repeat volta 2 {   b'16    d''16    \bar "|"   g''8    fis''8    g''8    a''8  
  g''8    a''8    b''8    g''8    e''8    d''8    b'8    d''8    \bar "|"   
g''8    fis''8    g''8    g''8    a''8    b''8    a''4.    a''8    g''8    
fis''8    \bar "|"     g''8    a''8    g''8    fis''8    g''8    fis''8    e''8 
   g''8    e''8    d''8    b'8    a'8    \bar "|"   g'8    a'8    b'8    d'4    
e'8    g'4.    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
