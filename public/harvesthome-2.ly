\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/49#setting12479"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/49#setting12479" {"https://thesession.org/tunes/49#setting12479"}}}
	title = "Harvest Home, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   cis''8 ^\downbow   d''8 ^\upbow   \bar "|"   d'4 
^\downbow   fis'8    a'8    d'8 (   a'8  -)   fis'8    a'8    \bar "|"   d''8 
^\downbow   e''8 (   fis''8  -)   cis''8    d''8 (   cis''8  -)   b'8    a'8    
\bar "|"   e''16 ^\downbow(   fis''16    g''8  -)   fis''8    a''8    g''8 (   
a''8  -)   fis''8    g''8    \bar "|"   e''8 ^\downbow   d''8 (   cis''8  -)   
b'8    a'8 (   g'8  -)   fis'8    e'8    \bar "|"   d'4 ^\downbow   fis'8    
a'8    d'8 (   a'8  -)   fis'8    a'8    \bar "|"   d''8    e''8 (   fis''8  -) 
  cis''8    d''8 (   cis''8  -)   b'8    a'8    \bar "|"   e''16 (   fis''16    
g''8  -)   fis''8    a''8    g''8 (   e''8  -)   cis''8    a'8    \bar "|"   
d''8    g''8 (   fis''8  -)   e''8    d''4    a'8    fis'8    \bar "|"   d'4 
^\downbow   fis'8    a'8    d'8 (   a'8  -)   fis'8    a'8    \bar "|"   d''8   
 e''8 (   fis''8  -)   cis''8    d''8 (   cis''8  -)   b'8    a'8    \bar "|"   
e''16 (   fis''16    g''8  -)   fis''8    a''8    g''8 (   a''8  -)   fis''8    
g''8    \bar "|"   e''16 ^\downbow   fis''16 ^\upbow   e''8 ^\downbow   d''8    
b'8    a'8 (   g'8  -)   fis'8    e'8    \bar "|"   d'4 ^\downbow   fis'8    
a'8    d'8 (   a'8  -)   fis'8    a'8    \bar "|"   d''8    e''8 (   fis''8  -) 
  cis''8    d''8 (   cis''8  -)   b'8    a'8    \bar "|"   e''16 ^\downbow   
fis''16 ^\upbow   g''8 ^\downbow   fis''8    a''8    g''8 (   a''8  -)   fis''8 
   g''8    \bar "|"   d''8    g''8 (   fis''8  -)   e''8    d''4    cis''8    
d''8    \bar "|"   e''8 ^\downbow(   a'8  -)   a'16    a'16    a'8    fis''8 (  
 a'8  -)   a'16    a'16    a'8 (   \bar "|"   g''8    a'8  -)   fis''8    a'8   
 e''8    a'8    a'16    a'16    a'8    \bar "|"   e''16 ^\downbow(   fis''16    
g''8  -)   fis''8    a''8    g''8    g''8 (   fis''8  -)   g''8    \bar "|"   
e''8    d''8    cis''8    b'8    a'8    g'8 (   fis'8  -)   e'8  \bar "|"   d'4 
^\downbow   fis'8    a'8    d'8 (   a'8  -)   fis'8    a'8    \bar "|"   d''8   
 e''8 (   fis''8  -)   cis''8    d''8 (   cis''8  -)   b'8    a'8    \bar "|"   
e''16 (   fis''16    g''8  -)   fis''8    a''8    g''8 (   e''8  -)   cis''8    
a'8    \bar "|"   d''8    g''8 (   fis''8  -)   e''8    d''4    cis''8    d''8  
  \bar "|"   e''8 ^\downbow(   a'8  -)   a'16    a'16    a'8    fis''8 (   a'8  
-)   a'16    a'16    a'8 (   \bar "|"   g''8    a'8  -)   fis''8    a'8    e''8 
   a'8    a'16    a'16    a'8    \bar "|"   e''16 ^\downbow(   fis''16    g''8  
-)   fis''8    a''8    g''8    a''8 (   fis''8  -)   g''8    \bar "|"   e''16   
 fis''16    e''8    d''16    cis''16    b'8    a'16    g'16    a'8    fis'16    
g'16    e'8    \bar "|"   d'4 ^\downbow   fis'8    a'8    d'8 (   a'8  -)   
fis'8    a'8    \bar "|"   d''8    e''8 (   fis''8  -)   cis''8    d''8 (   
cis''8  -)   b'8    a'8    \bar "|"   e''16 (   fis''16    g''8  -)   fis''8    
a''8    g''8 (   e''8  -)   cis''8    a'8    \bar "|"   d''8    g''8 (   fis''8 
 -)   e''8    d''4    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
