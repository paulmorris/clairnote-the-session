\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/84#setting22215"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/84#setting22215" {"https://thesession.org/tunes/84#setting22215"}}}
	title = "Rakes Of Kildare, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \dorian   \repeat volta 2 {   r8   \bar "|"   a'4. ^"~"    b'8 
   a'8    g'8    \bar "|"   b'8    c''8    d''8    e''4. ^"~"    \bar "|"   a'8 
   e''8    a'8    e''4. ^"~"    \bar "|"   d''4    g'8    b'8    a'8    g'8    
\bar "|"     a'4. ^"~"    b'8    a'8    g'8    \bar "|"   b'8    c''8    d''8   
 e''4    a''8    \bar "|"   a''8    g''8    e''8    d''8    b'8    e''8    
\bar "|"   a'8    b'8    a'8    a'4    }     \repeat volta 2 {   a''8    
\bar "|"   a''4. ^"~"    a''8    e''8    a''8    \bar "|"   a''8    e''8    
a''8    b''4    a''8    \bar "|"   g''4. ^"~"    a''4. ^"~"    \bar "|"   g''4. 
^"~"    d''8    e''8    g''8    \bar "|"     a''4    a''8    a''8    g''8    
e''8    \bar "|"   a''8    e''8    a''8    b''4. ^"~"    \bar "|"   g''4. ^"~"  
  e''8    d''8    b'8    \bar "|"   a'8    b'8    a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
