\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/314#setting28304"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/314#setting28304" {"https://thesession.org/tunes/314#setting28304"}}}
	title = "Poor But Happy At 53"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 { <<   b16    d'16   >>   \bar "|"   
e'8.    fis'16    g'8.    a'16    b'8.    e'16    dis'8.    e'16    \bar "|"   
\tuplet 3/2 {   b'8    c''8    b'8  }   a'8.    c''16    b'16    e'8.    
\tuplet 3/2 {   g'8    fis'8    e'8  }   \bar "|"   d'8.    e'16    fis'8.    
g'16    \tuplet 3/2 {   a'8    b'8    a'8  }   d'8.    fis'16    \bar "|"   
\tuplet 3/2 {   a'8    b'8    a'8  }   gis'8.    b'16    a'16    g'!8.    fis'8. 
   d'16    \bar "|"     e'4    \tuplet 3/2 {   fis'8    g'8    a'8  }   b'8.    
e''16    e''8.    dis''16    \bar "|"   e''8.    d''16    b'8.    ais'16    
\tuplet 3/2 {   b'8    c''8    b'8  }   g'8.    a'!16    \bar "|"   b'16    e'8. 
   g'8.    e'16    a'16    d'8.    fis'8.    d'16    \bar "|"   g'8.    e'16    
d'16    fis'8.    e'4   ~    e'8.    }     \repeat volta 2 {   e''16    
\bar "|"   e''8.    d''16    b'8.    ais'16    \tuplet 3/2 {   b'8    c''8    
b'8  }   e''8.    fis''16    \bar "|"   g''8.    fis''16    e''8.    d''16    
b'8.    a'16    \tuplet 3/2 {   b'8    c''8    d''8  }   \bar "|"   e''8.    
dis''16    e''8.    fis''16    g''16    b''8.    a''8.    fis''16    \bar "|"   
g''16    fis''8.    e''8.    dis''16    e''4   ~    e''8.    fis''16    
\bar "|"     e''8.    b'16    b'8.    ais'16    b'8.    e'16    \tuplet 3/2 {   
fis'8    g'8    a'!8  }   \bar "|"   b'8.    g'16    a'8.    fis'16    g'16    
fis'8.    e'8.    g'16    \bar "|"   b'4    g'8.    b'16    \tuplet 3/2 {   a'8  
  b'8    a'8  }   fis'8.    a'16    \bar "|"   g'16    e'8.    fis'16    d'8.   
 e'4   ~    e'8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
