\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1215#setting1215"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1215#setting1215" {"https://thesession.org/tunes/1215#setting1215"}}}
	title = "Rock Valley"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key c \major   c'4    e'8    g'8    f'8    e'8  \bar "|"   d'4    
e'8    f'4    a'8  \bar "|"   g'8    a'8    g'8    g'8    b'8    d''8  \bar "|" 
  c''8    b'8    c''8    a'8    g'8    e'8  \bar "|"     c'4    e'8    g'8    
f'8    e'8  \bar "|"   d'4    e'8    f'4    a'8  \bar "|"   g'8    a'8    g'8   
 g'8    b'8    d''8  \bar "|"   c''8    d''8    c''8    c''4.  }     
\repeat volta 2 {   g''8    a''8    g''8    g''8    a''8    g''8  \bar "|"   
e''4    c''8    c''8    b'8    a'8  \bar "|"   g'4    c''8    b'4    c''8  
\bar "|"   e''4    d''8    d''8    cis''8    d''8  \bar "|"     g''8    a''8    
g''8    g''8    a''8    g''8  \bar "|"   e''4    c''8    c''8    b'8    a'8  
\bar "|"   g'4    c''8    b'16    c''16    d''8    b'8  \bar "|"   c''8    d''8 
   c''8    c''4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
