\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/32#setting12425"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/32#setting12425" {"https://thesession.org/tunes/32#setting12425"}}}
	title = "Friendly Visit, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   \bar "|"   a''4    e''8    c''8    a'8    c''8    
e''8    a''8    \bar "|"   fis''4    d''8    b'8    fis'4.    fis''8    
\bar "|"   g''4    fis''8    e''8    dis''8    e''8    fis''8    a''8    
\bar "|"   g''8    e''8    fis''8    dis''8    e''8    d''!8    c''8    b'8    
\bar "|"   \tuplet 3/2 {   a'8    b'8    a'8  }   e'8    a'8    c''8    a'8    
c''8    e''8    \bar "|"   d''8    c''8    b'8    c''8    d''8    e''8    
fis''8    g''8    \bar "|"   a''8    e''8    c''8    a'8    e'8    g'8    b'8   
 d''8    \bar "|"   c''4    a'4    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
