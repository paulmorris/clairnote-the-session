\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1081#setting22004"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1081#setting22004" {"https://thesession.org/tunes/1081#setting22004"}}}
	title = "Saint Lawrence"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   fis'16    g'16    \bar "|"   a'8  
  b'8    a'8    fis'8    g'8    a'8    \bar "|"   fis''4    d''8    a'4.    
\bar "|"   cis''8    b'8    a'8    g'8    fis'8    g'8    \bar "|"   b'4    a'8 
   fis'4    g'8    \bar "|"     a'8    a'16    a'16    a'8    fis'8    g'8    
a'8    \bar "|"   fis''4    d''8    a'8    b'16    cis''16    d''8    \bar "|"  
 cis''8    b'8    cis''8    a'8    b'8    cis''8    \bar "|"   d''4.   ~    
d''4    }     \repeat volta 2 {   fis''16    g''16    \bar "|"   a''4    g''8   
 b'8    ais'8    b'8    \bar "|"   g''4    fis''8    a'8    gis'8    a'8    
\bar "|"   cis''8    bis'8    cis''8    a'4    cis''8    \bar "|"   d''8    
cis''8    b'8    a'4    fis''8    \bar "|"     a''4    g''8    b'8    ais'8    
b'8    \bar "|"   g''4    fis''8    a'4.    \bar "|"   cis''8    b'8    cis''8  
  a'8    b'8    cis''8    \bar "|"   d''4.   ~    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
