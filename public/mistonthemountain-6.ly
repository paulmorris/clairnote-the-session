\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "David50"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/470#setting13356"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/470#setting13356" {"https://thesession.org/tunes/470#setting13356"}}}
	title = "Mist On The Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \minor   a'4.    a'4.  \bar "|"   e''8.    a'16    b'8    a'8. 
   g'16    e'8  \bar "|"   g'4.    g'4.  \bar "|"   b'8.    a'16    b'8    a'8. 
   g'16    e'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
