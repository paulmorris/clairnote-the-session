\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Trinil"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1070#setting1070"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1070#setting1070" {"https://thesession.org/tunes/1070#setting1070"}}}
	title = "Monaghan Twig, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \repeat volta 2 {   cis''8    a'8    a'8    b'8  
  cis''8    d''8    e''8    d''8  \bar "|"   cis''8    a'8    a'8    g'8    
e'4. ^"~"    b'8  \bar "|"   cis''8    a'8    a'8    b'8    cis''8    d''8    
e''8    d''8  \bar "|"   b'8    a'8    g'8    b'8    d''4    e''8    d''8  }    
 cis''8    d''8    e''8    fis''8    g''8    fis''8    e''8    d''8  \bar "|" 
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8    e''4. ^"~"    d''8  
\bar "|"   cis''8    d''8    e''8    fis''8    g''4. ^"~"    e''8  \bar "|"   
d''8    b'8    g'8    b'8    d''4    e''8    d''8  \bar "|"     cis''8    d''8  
  e''8    d''8    cis''8    d''8    e''8    d''8  \bar "|"   cis''8    a'8    
a'8    b'8    cis''8    d''8    e''8    fis''8  \bar "|"   g''4. ^"~"    e''8   
 d''8    e''8    e''8    d''8  \bar "|"   b'8    a'8    g'8    b'8    d''4    
e''8    d''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
