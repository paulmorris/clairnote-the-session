\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/741#setting16882"
	arranger = "Setting 13"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/741#setting16882" {"https://thesession.org/tunes/741#setting16882"}}}
	title = "Paddy O'Rafferty"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   a'8  \bar "|"   b'8    g'8    e'8    d'4    e'8  
\bar "|"   g'8    a'8    b'8    c''4    a'8  \bar "|"   b'8    g'8    e'8    
d'4    e'8  \bar "|"   g'4. ^"~"    g'8    e'8    d'8  \bar "|"   b'8    g'8    
e'8    d'4    e'8  \bar "|"   g'8    a'8    b'8    d''4    e''8  \bar "|"   
d''8    b'8    g'8    a'8    g'8    a'8  \bar "|"   a'8    g'8    fis'8    g'4  
}   \repeat volta 2 {   b'8  \bar "|"   d''8    b'8    b'8    d''4    e''8  
\bar "|"   d''8    b'8    b'8    a'4    b'8  \bar "|"   d''8    b'8    b'8    
d''8    b'8    a'8  \bar "|"   g'4. ^"~"    g'8    e'8    d'8  \bar "|"   d''8  
  b'8    b'8    d''4    e''8  \bar "|"   d''8    b'8    d''8    g''4 ^"~"    
e''8  \bar "|"   d''8    b'8    g'8    a'8    g'8    a'8  \bar "|"   a'8    g'8 
   fis'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
