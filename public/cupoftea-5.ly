\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/20#setting23896"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/20#setting23896" {"https://thesession.org/tunes/20#setting23896"}}}
	title = "Cup Of Tea, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   b'8 ^"Em"   a'8    g'8    fis'8  
  g'8    e'8    e'8    fis'8  \bar "|"   g'8 ^"Em"   e'8    b'8    e'8    g'8   
 e'8    e'16    e'16    e'8  \bar "|"   b'8 ^"Em"   a'8    g'8    fis'8    g'8  
  e'8    e'8    fis'8  \bar "|"   fis'8 ^"D"   d'8    a'8    g'8    fis'8    
d'8    d'16    d'16    d'8  \bar "|"       b'8 ^"Em"   a'8    g'8    fis'8    
g'8    e'8    e'8    fis'8  \bar "|"   g'8 ^"Em"   e'8    b'8    e'8    g'8    
e'8    e'8    a'8  \bar "|"   b'8 ^"Em"   a'8    g'8    fis'8    g'8    a'8    
b'8    cis''8  \bar "|"     d''8 ^"D"   b'8    a'8    g'8    fis'8    d'8    
d'4    }     \repeat volta 2 {     d''4 ^"D"   e''16    fis''16    g''8    
fis''8    d''8    e''8    cis''8  \bar "|"   d''4 ^"Bm"   e''8    g''8    
fis''8    b'8    b'16    b'16    b'8  \bar "|"   d''4 ^"D"   e''8    g''8    
fis''8    d''8    e''8    cis''8  \bar "|"   d''8 ^"A"   b'8    a'8    g'8      
fis'8 ^"D"   d'8    d'4  \bar "|"       d''4 ^"D"   e''16    fis''16    g''8    
fis''8    d''8    e''8    cis''8  \bar "|"   d''8 ^"D"   fis''8    a''8    
fis''8      g''4 ^"G"   fis''8    g''8  \bar "|"   a''8 ^"D"   fis''8    g''8   
 e''8    fis''8    d''8    e''8    cis''8  \bar "|"   d''8 ^"D"   b'8    a'8    
g'8    fis'8    d'8    d'4    }     \repeat volta 2 {     fis'8 ^"D"   a'8    
d''8    a'8    fis'8    a'8    b'8    a'8  \bar "|"   fis'8 ^"D"   a'8    d''8  
  a'8      fis'8 ^"A"   e'8    e'16    e'16    e'8  \bar "|"   fis'8 ^"D"   a'8 
   d''8    a'8      b'8 ^"G"   a'8    b'8    cis''8  \bar "|"   d''8 ^"A"   b'8 
   a'8    g'8      fis'8 ^"D"   d'8    d'4  \bar "|"       fis'8 ^"D"   a'8    
d''8    a'8    fis'8    a'8    b'8    a'8  \bar "|"   fis'8 ^"D"   a'8    d''8  
  fis''8      fis''8 ^"A"   e''8    e''16    e''16    e''8  \bar "|"   fis''8 
^"D"   d''8    e''8    cis''8      d''8 ^"G"   b'8    a'8    fis'8  \bar "|"   
d''8 ^"A"   b'8    a'8    g'8      fis'8 ^"D"   d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
