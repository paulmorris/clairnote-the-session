\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ian Varley"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/481#setting29241"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/481#setting29241" {"https://thesession.org/tunes/481#setting29241"}}}
	title = "Bill Sullivan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \major   \repeat volta 2 {   a''4    a''8.    fis''16    
\bar "|"   e''8    cis''8    a''4    \bar "|"   cis''8    e''8    a''8.    
fis''16    \bar "|"   e''8    cis''8    b'8    a'8    \bar "|"   a''4    a''8.  
  fis''16    \bar "|"   e''8    cis''8    a''4    \bar "|"   cis''8    e''8    
b'8    cis''8  \bar "|"   b'8    a'8    a'4    }     \repeat volta 2 {   cis''8 
   e''8    e''8    cis''8    \bar "|"   d''8    fis''8    fis''8    e''8    
\bar "|"   cis''8    e''8    e''8    d''8    \bar "|"   d''8    cis''8    b'8   
 a'8    \bar "|"   cis''8    e''8    e''8    cis''8    \bar "|"   d''8    
fis''8    fis''8    e''8    \bar "|"   cis''8    e''8    b'8    cis''8    
\bar "|"   b'8    a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
