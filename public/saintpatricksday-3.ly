\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "The Merry Highlander"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/385#setting13215"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/385#setting13215" {"https://thesession.org/tunes/385#setting13215"}}}
	title = "Saint Patrick's Day"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   d''8.    e''16    fis''8    g''8  
  fis''8    e''8    \bar "|"   fis''8    e''8    d''8    e''8    d''8    b'8    
\bar "|"   d''8.    e''16    fis''8    g''8    fis''8    e''8    \bar "|"   
fis''8    e''8    d''8    e''4.  \bar "|"   d''8    e''8    fis''8    g''8    
fis''8    e''8    \bar "|"   fis''8    e''8    d''8    e''8    d''8    b'8    
\bar "|"   \bar "|"   a'8    g'8    a'8    b'8    a'8    g'8    \bar "|"   e'4  
  fis'8    g'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
