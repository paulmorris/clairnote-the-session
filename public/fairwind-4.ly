\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "stanton135"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/936#setting21841"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/936#setting21841" {"https://thesession.org/tunes/936#setting21841"}}}
	title = "Fair Wind, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   e'8    \bar "|"   g'4    g'8    
g'8    g'4.    a'8    \bar "|"   b'8    a'8    g'8    a'8    b'8    a'8    a'8  
  b'8    \bar "|"   g'4    g'8    g'8    g'4.    a'8    \bar "|"   b'8    a'8   
 g'8    e'8    e'8    d'8    d'8    }     \repeat volta 2 {   a'8    \bar "|"   
b'8    d''8    d''8    b'8    g'4.    a'8    \bar "|"   b'8    d''8    d''8    
b'8    c''8    a'8    a'4 ^"~"    \bar "|"   b'8    d''8    d''8    b'8    g'4. 
   a'8    \bar "|"   b'8    a'8    g'8    e'8    e'8    d'8    d'8    }     
\repeat volta 2 {   e'8    \bar "|"   g'8    e'8    e'4 ^"~"    c''4.    a'8    
\bar "|"   b'8    d''8    d''8    b'8    a'8    c''8    b'8    a'8    \bar "|"  
 g'8    e'8    e'4 ^"~"    c''4.    a'8    \bar "|"   b'8    a'8    g'8    e'8  
  e'8    d'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
