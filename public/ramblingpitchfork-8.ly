\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zjuul Portside"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/89#setting29372"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/89#setting29372" {"https://thesession.org/tunes/89#setting29372"}}}
	title = "Rambling Pitchfork, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   e'8    \bar "|"   fis'8    d'8    d'8    a'8    d'8   
 d'8  \bar "|"   d''8    cis''8    d''8    a'8    fis'8    d'8  \bar "|"   g'4  
  a'8    b'8    g''8    fis''8  \bar "|"   e''8    fis''8    e''8    d''8    
b'8    a'8  \bar "|"     \bar "|"   fis'8    d'8    d'8    a'8    d'8    d'8  
\bar "|"   d''8    cis''8    d''8    a'8    g'8    fis'8  \bar "|"   g'4    a'8 
   b'8    g'8    b'8  \bar "|"   a'8    fis'8    d'8    d'4    e'8  \bar "|"    
 \bar "|"   fis'8    d'8    d'8    a'8    d'8    d'8  \bar "|"   d''8    cis''8 
   d''8    a'8    fis'8    d'8  \bar "|"   g'4    a'8    b'8    g''8    fis''8  
\bar "|"   e''8    fis''8    e''8    d''8    b'8    a'8  \bar "|"     \bar "|"  
 fis'8    d'8    d'8    a'8    d'8    d'8  \bar "|"   d''8    cis''8    d''8    
a'8    g'8    fis'8  \bar "|"   g'4    a'8    b'8    g'8    b'8  \bar "|"   a'8 
   fis'8    d'8    d'8    fis'8    a'8  \bar "|"     \bar "|"   d''4    e''8    
fis''8    d''8    fis''8  \bar "|"   e''8    cis''8    a'8    a'8    b'8    
cis''8  \bar "|"   d''4    e''8    fis''4    g''8  \bar "|"   a''8    g''8    
fis''8    e''8    d''8    cis''8  \bar "|"     \bar "|"   d''4    e''8    
fis''8    d''8    fis''8  \bar "|"   e''8    cis''8    a'8    a'8    g'8    
fis'8  \bar "|"   g'4    a'8    b'8    g'8    b'8  \bar "|"   a'8    fis'8    
d'8    d'8    fis'8    a'8  \bar "|"     \bar "|"   d''4    e''8    fis''8    
d''8    fis''8  \bar "|"   e''8    cis''8    a'8    a'8    b'8    cis''8  
\bar "|"   d''4    e''8    fis''4    g''8  \bar "|"   a''8    g''8    fis''8    
e''8    d''8    e''8  \bar "|"     \bar "|"   a''8    g''8    fis''8    e''8    
fis''8    e''8  \bar "|"   d''8    b'8    a'8    b'8    a'8    fis'8  \bar "|"  
 g'4    a'8    b'8    g'8    b'8  \bar "|"   a'8    fis'8    d'8    d'4.  
\bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
