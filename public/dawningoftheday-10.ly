\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kilcash"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1441#setting27082"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1441#setting27082" {"https://thesession.org/tunes/1441#setting27082"}}}
	title = "Dawning Of The Day, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'8    a'8    \bar "|"   b'4    
b'4    b'4    a'8    b'8    \bar "|"   d''4.    e''8    fis''8    e''8    d''8  
  b'8    \bar "|"   a'8    g'8    a'8    b'8    g'4    g'8    a'8    \bar "|"   
b'2.    g'8    a'8    \bar "|"     b'4    b'4    b'4    a'8    b'8    \bar "|"  
 d''4.    e''8    fis''8    e''8    d''8    b'8    \bar "|"   a'8    g'8    a'8 
   b'8    g'4    g'8    a'8  \bar "|"   g'2.    b'8    a'8    \bar "|"     b'8  
  d''8    e''8    fis''8    g''4    fis''8    g''8    \bar "|"   e''8    g''8   
 e''8    d''8    b'8    a'8    b'8    d''8    \bar "|"   e''4    e''4    fis''4 
   d''4  \bar "|"   e''4.    fis''8    g''8    fis''8    e''8    d''8    
\bar "|"     b'4    b'4    b'4    a'8    b'8    \bar "|"   d''4.    e''8    
fis''8    e''8    d''8    b'8    \bar "|"   a'8    g'8    a'8    b'8    g'4    
g'8    a'8    \bar "|"   g'2.    \bar "|"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
