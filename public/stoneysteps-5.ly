\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/571#setting13550"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/571#setting13550" {"https://thesession.org/tunes/571#setting13550"}}}
	title = "Stoney Steps"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'4.    b'8    a'8    fis'8    fis'4 ^"~"  \bar "|"   
a'8    fis'8    d''8    fis''8    e''8    fis''8    d''8    a'8  \bar "|"   
fis'8    a'8    a'4 ^"~"    b'8    r8 a'8    fis'8  \bar "|"   e'8    fis'8    
g'8    e'8    fis'8    d'8    d'8    b'8  \bar ":|."   e'8    fis'8    g'8    
e'8    fis'8    d'8    d'8    r8 \bar "||"   fis''8    e''8    d''8    cis''8   
 d''8    e''8    fis''8    g''8  \bar "|"   a''8    r8 b''8    fis''8    a''8   
 fis''8    e''8    g''8  \bar "|"   fis''8    d''8    e''8    d''8    fis''8    
d''8    e''8    d''8  \bar "|" \tuplet 3/2 {   b'8    cis''8    d''8  }   a''8   
 fis''8    e''8    d''8    e''8    g''8  \bar "|"   fis''8    e''8    d''8    
cis''8    d''8    e''8    fis''8    g''8  \bar "|"   a''8    r8 b''8    fis''8  
  a''8    fis''8    e''8    g''8  \bar "|"   fis''4. ^"~"    a''8    e''4. 
^"~"    fis''8  \bar "|"   d''8    fis''8    a''8    fis''8    e''8    fis''8   
 d''8    b'8  \bar "||"   a'4.    b'8    a'8    fis'8    fis'4 ^"~"  \bar "|"   
a'8    b'8    d''8    fis''8    e''4    d''8    a'8  \bar "|"   fis'8    a'8    
a'4 ^"~"    b'8    fis'8    a'8    fis'8  \bar "|"   e'8    fis'8    g'8    e'8 
   fis'8    d'8    d''8    b'8  \bar "|"   a'8    gis'8    a'8    b'8    a'8    
fis'8    fis'4 ^"~"  \bar "|"   a'8    b'8    d''8    fis''8    e''4    d''8    
a'8  \bar "|"   fis'8    a'8    a'4 ^"~"    b'8    fis'8    a'8    fis'8  
\bar "|"   e'8    fis'8    g'8    e'8    fis'8    a'8    d''8    e''8  
\bar "||"   fis''8    d''8    d''4 ^"~"    d''4    fis''8    g''8  \bar "|"   
a''4    b''8    fis''8    a''8    fis''8    e''8    g''8  \bar "|"   fis''8    
d''8    d''4 ^"~"    d''8    b'8    a'8    d''8  \bar "|"   b'8    fis'8    a'8 
   fis'8    e'4.    g''8  \bar "|"   fis''8    d''8    d''4 ^"~"    d''4    
fis''8    g''8  \bar "|"   a''4    b''8    fis''8    a''8    fis''8    e''8    
g''8  \bar "|"   fis''4.    d''8    e''4    d''8    e''8  \bar "|"   fis''8    
d''8    a''8    fis''8    e''8    fis''8    d''8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
