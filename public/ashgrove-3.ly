\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/997#setting14205"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/997#setting14205" {"https://thesession.org/tunes/997#setting14205"}}}
	title = "Ash Grove, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/4 \key g \major   g'4    b'4    d''4    \bar "|"   b'4    g'4    b'4    
\bar "|"   a'4    c''4    a'4    \bar "|"   fis'4    d'4    d'4    \bar "|"   
g'4    b'8    g'8    g'4    \bar "|"   e'4    c'4    e'4    \bar "|"   d'4    
g'4    fis'4    \bar "|"   g'4    r4   }   g'4    b'4    d''4    \bar "|"   
d''4    c''4    b'4    \bar "|"   a'4    c''4    c''4    \bar "|"   c''4    b'4 
   a'4    \bar "|"   g'4    b'4    b'4    \bar "|"   b'4    a'4    g'4    
\bar "|"   fis'4    d''4    cis''4    \bar "|"   d''4    r4   d'4    \bar "|"   
g'4    b'4    d''4    \bar "|"   b'4    g'4    b'4    \bar "|"   a'4    c''4    
a'4    \bar "|"   fis'4    d'4    d'4    \bar "|"   g'4    b'8    g'8    g'4    
\bar "|"   e'4    c'4    e'4    \bar "|"   d'4    g'4    fis'4    \bar "|"   
g'4    r4   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
