\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "irishfiddleCT"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1446#setting14832"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1446#setting14832" {"https://thesession.org/tunes/1446#setting14832"}}}
	title = "Eel In The Sink, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   f''4    b'8    c''8    d''8    e''8    f''8    d''8  
\bar "|"   e''8    a'8    c''8    a'8    e''8    a'8    \tuplet 3/2 {   c''8    
d''8    e''8  } \bar "|"   f''4    b'8    c''8    d''8    e''8    f''8    b''8  
\bar "|"   a''8    f''8    e''8    c''8    c''8    b'8    b'8    a'8  
\bar ":|."   a''8    f''8    e''8    c''8    c''8    b'8    b'4 ^"~"  \bar "||" 
  b''4    f''8    b''8    b''8    a''8    f''8    e''8  \bar "|"   dis''8    
e''8    f''8    g''8    a''4 ^"~"    g''8    a''8  \bar "|"   b''4    f''8    
b''8    b''8    a''8    f''8    e''8  \bar "|"   a''8    f''8    e''8    c''8   
 c''8    b'8    b'8    a'8  \bar "|"   b''4    f''8    b''8    b''8    a''8    
f''8    e''8  \bar "|"   dis''8    e''8    f''8    g''8    a''4 ^"~"    g''8    
a''8  \bar "|"   b''8    g''8    a''8    f''8    g''8    e''8    f''8    g''8  
\bar "|"   a''8    f''8    e''8    c''8    c''8    b'8    b'8    a'8  \bar "||" 
  e''4    a'8    b'8    c''8    d''8    e''8    c''8  \bar "|"   d''8    g'8    
b'8    g'8    d''8    g'8    \tuplet 3/2 {   b'8    c''8    d''8  } \bar "|"   
e''4    a'8    b'8    c''8    d''8    e''8    a''8  \bar "|"   g''8    e''8    
d''8    b'8    b'8    a'8    a'8    g'8  \bar ":|."   g''8    e''8    d''8    
b'8    b'8    a'8    a'4 ^"~"  \bar "||"   a''4    e''8    a''8    a''8    g''8 
   e''8    d''8  \bar "|"   cis''8    d''8    e''8    f''8    g''4 ^"~"    f''8 
   g''8  \bar "|"   a''4    e''8    a''8    a''8    g''8    e''8    d''8  
\bar "|"   g''8    e''8    d''8    b'8    b'8    a'8    a'8    g'8  \bar "|"   
a''4    e''8    a''8    a''8    g''8    e''8    d''8  \bar "|"   cis''8    d''8 
   e''8    f''8    g''4 ^"~"    f''8    g''8  \bar "|"   a''8    f''8    g''8   
 e''8    f''8    d''8    e''8    f''8  \bar "|"   g''8    e''8    d''8    b'8   
 b'8    a'8    a'8    g'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
