\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/279#setting25307"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/279#setting25307" {"https://thesession.org/tunes/279#setting25307"}}}
	title = "Humours Of Ballymanus, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key d \major   a'8    b'8    a'8    a'4    fis'8    a'4    e'8  
\bar "|"   a'8    b'8    a'8    a'4    fis'8    b'8    cis''8    d''8  \bar "|" 
  a'8    b'8    a'8    a'4    fis'8    b'8    a'8    fis'8  \bar "|"   e'4    
b'8    b'4    a'8    b'8    cis''8    d''8  }     d''4    e''8    fis''8    
e''8    d''8    cis''8    b'8    a'8  \bar "|"   d''8    d''8    d''8    fis''8 
   fis''8    fis''8    a''4    fis''8  \bar "|"   d''4    e''8    fis''8    
e''8    d''8    cis''8    b'8    a'8  \bar "|"   b'8    b'8    b'8    b'4    
a'8    b'8    cis''8    d''8  \bar "|"     d''4    e''8    fis''8    e''8    
d''8    cis''8    b'8    a'8  \bar "|"   d''8    d''8    d''8    e''8    e''8   
 e''8    fis''4    g''8  \bar "|"   a''8    g''8    fis''8    e''8    fis''8    
d''8    cis''8    b'8    a'8  \bar "|"   b'8    b'8    b'8    b'4    a'8    b'8 
   cis''8    d''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
