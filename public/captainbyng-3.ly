\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1400#setting14770"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1400#setting14770" {"https://thesession.org/tunes/1400#setting14770"}}}
	title = "Captain Byng"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g''4. ^"~"    d''8    \bar "|"   b'8    g'8    g'8    
a'16    b'16    \bar "|"   \tuplet 3/2 {   c''8    b'8    a'8  }   a''8.    
g''16    \bar "|"   fis''8    d''8    e''8    fis''8    \bar "|"   g''4. ^"~"   
 d''8    \bar "|"   b'8    g'8    g'8    a'16    b'16    \bar "|"   
\tuplet 3/2 {   c''8    b'8    a'8  }   d''8    fis'8    \bar "|"   g'4. ^"~"    
r8   }   \repeat volta 2 {   b'8    g'8    d''8    g'8    \bar "|"   b'8    g'8 
   g'8    a'16    b'16    \bar "|"   c''8    a'8    e''8    a'8    \bar "|"   
c''8    a'8    a'8.    c''16    \bar "|"   b'8    g'8    d''8    g'8    
\bar "|"   b'8    g'8    g'8    a'16    b'16    \bar "|"   \tuplet 3/2 {   c''8  
  b'8    a'8  }   d''8    fis'8    \bar "|"   g'4. ^"~"    r8   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
