\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/187#setting12835"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/187#setting12835" {"https://thesession.org/tunes/187#setting12835"}}}
	title = "Far Away"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   b'4    b'8    fis'8    \tuplet 3/2 {   b'8    cis''8   
 d''8  }   \bar "|"   cis''8    a'8    fis'4    fis'8    a'8    \bar "|"   b'4  
  b'8    fis'8    b'8    e''8    \bar "|"   cis''4.    cis''8    d''8    cis''8 
   \bar "|"   b'4    b'8    g'8    \tuplet 3/2 {   b'8    cis''8    d''8  }   
\bar "|"   cis''8    b'8    a'4    d''8    e''8    \bar "|"   fis''8    e''8    
d''8    cis''8    b'8    a'8    \bar "|"   b'4    b'8    a'8    fis'8    a'8    
\bar ":|."   b'4    b'8    cis''8    d''8    e''8    \bar "||"   \bar ".|:"   
fis''4.    d''8    fis''8    a''8    \bar "|"   e''8    cis''8    a'4    d''8   
 e''8    \bar "|"   fis''4.    d''8    fis''8    a''8    \bar "|"   e''2    
d''8    e''8    \bar "|"   fis''8    d''8    b'8    e''8    cis''8    a'8    
\bar "|"   d''8    b'8    g'8    d''8    b'8    e''8    \bar "|"   fis'4    b'4 
   a'4    \bar "|"   b'4    b'8    cis''8    d''8    e''8    \bar ":|."   b'4   
 b'8    a'8    fis'8    a'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
