\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "DonaldK"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/195#setting12851"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/195#setting12851" {"https://thesession.org/tunes/195#setting12851"}}}
	title = "Flogging, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major <<   b'8    g'8   >> g'8    g'16    g'16    g'8  <<   
b'8    g'8   ~   >> g'8  <<   c''8    g'8   >> g'8  \bar "|" <<   b'8    g'8   
>> g'8    g'16    g'16    g'8    b'8    d''8    g''8    d''8  \bar "|" <<   b'8 
   g'8   >> g'8    g'16    g'16    g'8  <<   b'8    g'8   ~   >> g'8  <<   c''8 
   g'8   >> <<   b'8    g'8   >> \bar "|"   a'8    g'8    fis'8    g'8    a'8   
 b'8    c''8    a'8  }   g''4.    d''8    \tuplet 3/2 {   b'8    c''8    d''8  } 
  d''8    b'8  \bar "|"   g''4.    e''8    fis''8    g''8    a''8    fis''8  
\bar "|"   g''4.    d''8    b'8    c''8    d''8    b'8  \bar "|"   a'8    g'8   
 fis'8    g'8    a'8    b'8    c''8    a'8  \bar "|"   g''4.    d''8    
\tuplet 3/2 {   b'8    c''8    d''8  }   d''8    b'8  \bar "|"   g''4.    e''8   
 fis''8    g''8    a''8    g''8  \bar "|"   b''8    g''8    a''8    fis''8    
g''8    e''8    d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8    a'8    
b'8    c''8    a'8  \bar "|"   \repeat volta 2 { \tuplet 3/2 {   b'8    c''8    
d''8  }   g''8    d''8    \tuplet 3/2 {   b'8    c''8    d''8  }   g''8    d''8  
\bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }   g''8    d''8    b'8    g'8    
g'8    b'8  \bar "|" \tuplet 3/2 {   a'8    b'8    c''8  }   f''8    c''8    
\tuplet 3/2 {   a'8    b'8    c''8  }   f''8    c''8  \bar "|" \tuplet 3/2 {   
a'8    b'8    c''8  }   f''8    c''8    a'8    f'8    f'8    c''8  \bar "|"   
\tuplet 3/2 {   b'8    c''8    d''8  }   g''8    d''8    \tuplet 3/2 {   b'8    
c''8    d''8  }   g''8    d''8  \bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }  
 e''8    fis''8    g''4    g''8    a''8  \bar "|"   b''8    g''8    a''8    
fis''8    g''8    e''8    d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8    
a'8    b'8    c''8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
