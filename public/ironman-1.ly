\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JD"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/172#setting172"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/172#setting172" {"https://thesession.org/tunes/172#setting172"}}}
	title = "Iron Man, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a'4    cis''8.    a'16    b'16    gis'8.    a'8.    
b'16  \bar "|"   cis''4    e''8.    cis''16    d''16    b'8.    cis''8.    
e''16  \bar "|"   fis''4    d''8.    fis''16    e''4    cis''8.    e''16  
\bar "|" \tuplet 3/2 {   d''8    cis''8    b'8  }   \tuplet 3/2 {   cis''8    b'8 
   a'8  }   gis'16    b'8.    b'8.    cis''16  \bar "|"     a'4    cis''8.    
a'16    b'16    gis'8.    a'8.    b'16  \bar "|"   cis''4    e''8.    cis''16   
 d''16    b'8.    cis''8.    e''16  \bar "|"   fis''4    d''8.    fis''16    
e''16    a''8.    cis''8.    a'16  \bar "|"   \tuplet 3/2 {   b'8    cis''8    
d''8  }   \tuplet 3/2 {   d''8    cis''8    b'8  }   cis''16    a'8.    a'8.    
e'16  \bar ":|."   \tuplet 3/2 {   b'8    cis''8    d''8  }   \tuplet 3/2 {   
d''8    cis''8    b'8  }   cis''16    a'8.    a'8.    e''16  \bar "||"     
\bar ".|:"   a''8.    a'16    gis''8.    a'16    fis''8.    a'16    e''8.    
a'16  \bar "|" \tuplet 3/2 {   cis''8    b'8    a'8  }   e''8.    gis''16    
a''8.    e''16    cis''8.    a'16  \bar "|"   d''16    e'8.    cis''8.    e'16  
  b'16    e'8.    cis''8.    e'16  \bar "|"   d''16    cis''16    b'8    
cis''16    b'16    a'8    \tuplet 3/2 {   b'8    cis''8    d''8  }   
\tuplet 3/2 {   e''8    fis''8    gis''8  } \bar "|"     a''8.    a'16    
gis''8.    a'16    fis''8.    a'16    e''8.    a'16  \bar "|"   cis''16    b'16 
   a'8    e''8.    gis''16    a''8.    e''16    cis''8.    a'16  \bar "|"   
d''4    fis''8.    d''16    \tuplet 3/2 {   e''8    a''8    gis''8  }   
\tuplet 3/2 {   fis''8    e''8    d''8  } \bar "|"   \tuplet 3/2 {   cis''8    
b'8    a'8  }   \tuplet 3/2 {   gis'8    a'8    b'8  }   cis''16    a'8.    a'8. 
   e''16  \bar ":|."   \tuplet 3/2 {   cis''8    b'8    a'8  }   \tuplet 3/2 {   
gis'8    a'8    b'8  }   cis''16    a'8.    a'8.    e'16  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
