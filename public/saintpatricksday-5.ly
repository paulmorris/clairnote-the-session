\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "joe fidkid"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/385#setting13217"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/385#setting13217" {"https://thesession.org/tunes/385#setting13217"}}}
	title = "Saint Patrick's Day"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key f \major   c'4  \bar "|"   f'4.    g'8    f'4    f'4    g'4    
a'4    \bar "|"   c''4.    d''8    c''4    c''4    a'4    f'4    \bar "|"   
g'4.    f'8    g'4    a'4    f'4    c'4    \bar "|"   d'4.    e'8    d'4    d'2 
   c'8    c'8  \bar "|"   f'4.    g'8    f'4    f'4    g'4    a'4    \bar "|"   
c''4.    d''8    c''4    c''4    a'4    f'4    \bar "|"   g'4.    f'8    g'4    
a'4    f'4    c'4    \bar "|"   d'2    e'4    f'2    c''4    \bar "|"   c''4    
d''4    e''4    f''2    d''4  \bar "|"   e''2    c''4    d''2    a'4    
\bar "|"   c''4    d''4    e''4    f''4    e''4    d''4    \bar "|"   e''4    
d''4    c''4    d''2    c''8    c''8    \bar "|"   c''4    d''4    e''4    f''2 
   d''4    \bar "|"   e''4    d''4    c''4    d''2    e''8    f''8    \bar "|"  
 f'4.    g'8    f'4    f'4    g'4    a'4    \bar "|"   c''4.    d''8    c''4    
c''4    a'4    f'4    \bar "|"   g'4.    f'8    g'4    a'4    f'4    c'4    
\bar "|"   d'4.    e'8    d'4    d'2    c'4    \bar "|"   f'4.    g'8    f'4    
f'4    g'4    a'4    \bar "|"   c''4.    d''8    c''4    c''4    a'4    f'4    
\bar "|"   g'4.    f'8    g'4    a'4    f'4    c'4    \bar "|" <<   d'2    bes2 
  >> <<   e'4    bes'4   >> <<   f'2    a2   >>   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
