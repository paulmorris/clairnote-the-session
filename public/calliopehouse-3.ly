\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/15#setting12382"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/15#setting12382" {"https://thesession.org/tunes/15#setting12382"}}}
	title = "Calliope House"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major     \bar "|"   a'8 ^"~"    a'16    a'16    a'8    a'4   
 fis'8    \bar "|"   a'4    b'8    d''8    d''16    d''16    e''8    \bar "|"   
fis''8    fis''16    fis''16    fis''8    e''8    e''16    e''16    e''8    
\bar "|"   d''4.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
