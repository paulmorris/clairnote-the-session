\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/557#setting2552"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/557#setting2552" {"https://thesession.org/tunes/557#setting2552"}}}
	title = "Ruined Old Cottage In The Glen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   b'8    a'8    fis'8    e'8    d'4    a'8    d'8  
\bar "|" \tuplet 3/2 {   b'8    cis''8    d''8  }   a'8    d'8    b'8    d''8    
e''8    fis''8  \bar "|"   d''8    b'8    a'8    fis'8    d'8    e'8    fis'8   
 a'8  \bar "|"   b'8    d'8    a'8    fis'8    fis'8    e'8    e'4 ^"~"  }     
b'8    e''8    e''8    d''8    b'8    cis''8    d''8    a'8  \bar "|" 
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8    g''8    fis''8    
e''8    d''8  \bar "|"   b'8    e''8    e''8    d''8    b'4    a'8    fis'8  
\bar "|"   b'8    d'8    a'8    fis'8    fis'8    e'8    e'8    a'8  \bar "|"   
  b'8    e''8    e''8    d''8    b'8    cis''8    d''8    a'8  \bar "|" 
\tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    fis''8    g''4    fis''8    
g''8  \bar "|"   a''8    fis''8    g''8    e''8    fis''8    d''8    e''8    
cis''8  \bar "|"   d''8    b'8    a'8    fis'8    fis'8    e'8    e'8    a'8  
\bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
