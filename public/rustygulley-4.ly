\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1208#setting20969"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1208#setting20969" {"https://thesession.org/tunes/1208#setting20969"}}}
	title = "Rusty Gulley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key b \mixolydian   \repeat volta 2 {   b'8    cis''8    dis''8    
e''8    fis''4    b'4    fis''4    b'4    \bar "|"   a'4    cis''4   ~    
cis''4    e''4    dis''4    cis''4    \bar "|"   b'8    cis''8    dis''8    
e''8    fis''4    b'4    dis''4    b'4    \bar "|"   fis'4    b'4   ~    b'4    
dis''4    cis''4    b'4    }     \repeat volta 2 {   b''2    a''2    gis''2    
\bar "|"   a''4    fis''4   ~    fis''4    a''4    gis''4    fis''4    \bar "|" 
  e''2    dis''2    cis''2    \bar "|"   dis''4    b'4   ~    b'4    dis''4    
cis''4    b'4    }     \repeat volta 2 {   b'8    cis''8    dis''8    e''8    
fis''4    b'4    dis''4    b'4    \bar "|"   a'4    cis''4    cis''4    e''4    
dis''4    cis''4    \bar "|"   b'8    cis''8    dis''8    e''8    fis''4    b'4 
   dis''4    b'4    \bar "|"   fis'4    b'4    b'4    dis''4    cis''4    b'4   
 }     \repeat volta 2 {   b''2    a''2    gis''4    b''4    \bar "|"   a''4    
fis''4    fis''4    a''4    gis''4    fis''4    \bar "|"   e''4    b''4    
dis''4    b''4    cis''4    b''4    \bar "|"   dis''4    b'4    b'4    dis''4   
 cis''4    b'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
