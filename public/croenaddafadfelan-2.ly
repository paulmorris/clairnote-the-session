\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1212#setting14507"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1212#setting14507" {"https://thesession.org/tunes/1212#setting14507"}}}
	title = "Croen A Ddafad Felan"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g'8.    a'16    b'8.    c''16    
d''4    d''4  \bar "|"   c''8.    b'16    c''8.    a'16    d''4    d''4  
\bar "|"   c''8.    b'16    a'8.    a'16    b'8.    a'16    g'8.    g'16  
} \alternative{{   a'8.    g'16    fis'8.    g'16    a'4    d'4  } {   a'8.    
a'16    d''8.    d''16    g'4    g'4  \bar "||"   \bar "|"   g''8.    fis''16   
 e''8.    g''16    fis''4    fis''4  \bar "|"   c''8.    b'16    c''8.    a'16  
  d''2  \bar "|"   e''8.    d''16    c''8.    e''16    d''4    b'4  \bar "|"   
g'8.    a'16    b'8.    g'16    a'2  \bar "|"   g''8.    fis''16    e''8.    
g''16    fis''4    fis''4  \bar "|"   e''8.    d''16    c''8.    e''16    d''2  
\bar "|"   c''8.    b'16    a'8.    c''16    b'8.    a'16    g'8.    b'16  
\bar "|"   a'4    fis'4    g'2  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
