\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "geoffwright"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/603#setting13618"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/603#setting13618" {"https://thesession.org/tunes/603#setting13618"}}}
	title = "Kitchen Girl, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'16    b'16  \repeat volta 2 {   c''8    c''8    b'8 
   b'8  \bar "|"   a'16    b'16    a'16    g'16    e'8    a'16    b'16  
\bar "|"   c''8    c''8    b'8    b'8  \bar "|"   cis''!4    cis''8    a'16    
b'16  \bar "|"   c''8    c''8    b'8    b'8  \bar "|"   a'16    b'16    a'16    
g'16    e'8    e'16    fis'16  \bar "|"   g'8    e'8    d'8    b8  \bar "|"   
a4    a8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
