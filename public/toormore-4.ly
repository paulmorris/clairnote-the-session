\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/110#setting12693"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/110#setting12693" {"https://thesession.org/tunes/110#setting12693"}}}
	title = "Toormore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key a \minor   e'4    a'8    a'8    gis'8    a'8    c''4    d''8    
e''4    cis''8    \bar "|"   d''4    c''8    a'4    gis'8    a'4.    a'4    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
