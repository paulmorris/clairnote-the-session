\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/169#setting27607"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/169#setting27607" {"https://thesession.org/tunes/169#setting27607"}}}
	title = "Gusty's Frolics"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   a8    d'8    d'8    a8    d'8    
d'8    fis'8    e'8    fis'8    \bar "|"   a8    d'8    d'8    d'8    fis'8    
a'8    g'8    e'8    d'8    \bar "|"   a8    d'8    d'8    a8    d'8    d'8    
fis'8    g'8    a'8    \bar "|"   g'8    e'8    cis'8    cis'8    e'8    fis'8  
  g'8    fis'8    e'8    }     \repeat volta 2 {   fis'8    d'8    fis'8    a'8 
   b'8    g'8    a'8    b'8    g'8    \bar "|"   fis'8    d'8    fis'8    a'8   
 b'8    fis'8    g'8    fis'8    e'8    \bar "|"   fis'8    d'8    fis'8    a'8 
   b'8    fis'8    a'8    b'8    fis'8    \bar "|"   g'8    fis'8    g'8    e'8 
   cis'8    e'8    g'8    fis'8    e'8    }     \repeat volta 2 {   a'8    d''8 
   d''8    a'8    d''8    d''8    fis''8    e''8    fis''8    \bar "|"   a'8    
d''8    d''8    d''8    fis''8    a''8    g''8    fis''8    e''8    \bar "|"   
a'8    d''8    d''8    a'8    d''8    d''8    fis''8    e''8    fis''8    
\bar "|"   g''8    fis''8    g''8    e''8    cis''8    e''8    g''8    fis''8   
 e''8    }     \repeat volta 2 {   fis''8    d''8    fis''8    a''8    b''16    
a''16    g''8    a''8    b''16    a''16    g''8    \bar "|"   fis''8    d''8    
fis''8    a''4    fis''8    g''8    fis''8    e''8    \bar "|"   fis''8    d''8 
   fis''8    a''8.    b''16    a''16    g''16    a''8.    b''16    a''16    
g''16    \bar "|"   g''8    b''8    g''8    e''8    cis''8    e''8    g''8    
fis''8    e''8    }     \repeat volta 2 {   a''8    fis''8    g''16    fis''16  
  d''8    fis''8    fis''8    d''8    fis''8    fis''8    \bar "|"   a''8    
fis''8    fis''8    d''8    fis''8    fis''8    g''8    fis''8    e''8    
\bar "|"   a''8    fis''8    fis''8    d''8    fis''8    fis''8    d''8    
fis''8    fis''8    \bar "|"   g''8    fis''8    g''8    e''8    cis''8    e''8 
   g''8    fis''8    e''8    }     \repeat volta 2 {   d''8    fis''8    a''16  
  fis''16    d''8    fis''8    a''16    fis''16    d''4    a'8    \bar "|"   
d''8    fis''8    a''16    fis''16    d''8    fis''8    a''16    fis''16    
g''8    fis''8    e''8    \bar "|"   d''8    fis''8    a''16    fis''16    d''8 
   fis''8    a''16    fis''16    d''8    fis''8    a''16    fis''16    \bar "|" 
  g''8    b''8    g''8    e''8    cis''8    e''8    g''8    fis''8    e''8    } 
    \repeat volta 2 {   d''8    fis''8    e''8    d''8    fis''8    e''8    
d''8    a'8    fis'8    \bar "|"   d'8    fis'8    a'8    d''8    fis''8    
a''8    g''8    fis''8    e''8  \bar "|"   d''8    fis''8    a''16    fis''16   
 d''8    e''8    cis''8    d''8    fis''8    a''8    \bar "|"   g''8    fis''8  
  g''8    e''8    cis''8    e''8    g''8    fis''8    e''8    }     
\repeat volta 2 {   a'8    fis'8    a'8    d''8.    e''16    d''16    cis''16   
 d''4    d'8    \bar "|"   d'8    fis'8    a'8    d''8    cis''8    b'8    a'8  
  g'8    fis'8    \bar "|"   d'8    fis'8    a'8    c''8    d''8    b'8    c''8 
   b'8    a'8    \bar "|"   g'8    e'8    cis'8    cis'8    e'8    fis'8    g'8 
   fis'8    e'8    }     \repeat volta 2 {   a8    d'8    d'8    a8    d'8    
d'8    fis'8    e'8    fis'8    \bar "|"   a8    d'8    d'8    d'8    fis'8    
a'8    g'8    e'8    d'8    \bar "|"   a8    d'8    d'8    a8    d'8    d'8    
fis'4    a'8    \bar "|"   g'8    e'8    cis'8    cis'8.    d'16    e'16    
fis'16    g'8    fis'8    e'8    }     \repeat volta 2 {   a'4    a'8    a'8    
d''8    cis''16    b'16    a'4    g'8    \bar "|"   a'8    fis'8    d'8    d'8  
  e'8    d'8    e'8    fis'8    g'8    \bar "|"   a'8    fis'8    a'8    b'8    
g'8    b'8    c''8    b'8    a'8    \bar "|"   g'8    e'8    cis'8    cis'8    
d'8    e'16    fis'16    g'8    fis'8    e'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
