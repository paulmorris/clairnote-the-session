\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "CreadurMawnOrganig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/425#setting425"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/425#setting425" {"https://thesession.org/tunes/425#setting425"}}}
	title = "Wink And She'll Follow You"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key e \dorian   e'8    e'8    e'8    b'8    a'8    g'8    b'8    
cis''8    d''8    \bar "|"   e'4    b'8    b'8    a'8    fis'8    a'4    fis'8  
  \bar "|"   e'8    e'8    e'8    b'8    a'8    g'8    b'8    cis''8    d''8    
\bar "|"   e''8    d''8    b'8    b'8    a'8    fis'8    a'4    fis'8    }     
\repeat volta 2 {   d''4    fis''8    fis''8    d''8    b'8    b'8    cis''8    
d''8    \bar "|"   cis''4    e''8    e''8    fis''8    e''8    cis''8    b'8    
a'8    } \alternative{{   d''4    fis''8    fis''8    d''8    b'8    b'8    
cis''8    d''8    \bar "|"     e''8    d''8    b'8    b'8    a'8    fis'8    
a'4. ^"~"    } {   d''4    fis''8    a''8    a''8    a''8    fis''8    e''8    
d''8    \bar "|"   cis''8    b'8    a'8    b'8    a'8    fis'8    a'4    fis'8  
  } }    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
