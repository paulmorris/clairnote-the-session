\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/1398#setting14767"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1398#setting14767" {"https://thesession.org/tunes/1398#setting14767"}}}
	title = "Star Above The Garter, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key g \major   \repeat volta 2 {   d''4    e''8    fis''4    d''8   
 \tuplet 3/2 {   g''8    a''8    g''8  }   e''8    \tuplet 3/2 {   d''8    e''8   
 d''8  }   b'8  \bar "|"   g'4    b'8    c''4    b'8  \grace {    d''8  }   b'8 
   a'8    g'8    a'8    b'8    c''8  \bar "|"   d''4    e''8    fis''8    g''8  
  a''8    \tuplet 3/2 {   g''8    a''8    g''8  }   e''8    \tuplet 3/2 {   d''8  
  e''8    d''8  }   b'8  \bar "|"   g'4    b'8    c''4    e'8    d'4.    d'4.  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
