\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Macha"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/907#setting907"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/907#setting907" {"https://thesession.org/tunes/907#setting907"}}}
	title = "Chumha Eoghain Rua Ui Neill"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   e'8.    fis'16    g'4    a'4    b'4    \bar "|"   
e''4.    b'8    d''16    b'16    g'8    a'4    \bar "|"   g'4    g'4    g'2  
\bar "|"     e''8.    fis''16    g''4    fis''4    g''8.    fis''16    \bar "|" 
  e''4.    fis''8    e''16    d''16    b'16    a'16    b'4    \bar "|"   e''4   
 e''8.    fis''16    e''2  \bar "|"     e''8.    fis''16    g''4    fis''4    
g''8.    fis''16    \bar "|"   e''4.    fis''8    e''16    d''16    b'16    
a'16    b'4   ~    \bar "|"   b'8    a'8    g'8.    e'16    d'2  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
