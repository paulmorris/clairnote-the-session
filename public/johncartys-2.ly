\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/853#setting20681"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/853#setting20681" {"https://thesession.org/tunes/853#setting20681"}}}
	title = "John Carty's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   c''4    \tuplet 3/2 {   c''8    b'8    a'8  }   g'4 
^"~"    g'8    b'8  \bar "|"   a'8    e'8    e'4 ^"~"    g'8    e'8    d''8    
b'8  \bar "|"   c''4    \tuplet 3/2 {   c''8    b'8    a'8  }   g''8    a''8    
g''16    a''16    g''16    e''16  \bar "|"   d''8    b'8    a'8    c''8    b'8  
  g'8    g'4 ^"~"  \bar "|"     c''4    \tuplet 3/2 {   c''8    b'8    a'8  }   
g'4 ^"~"    g'8    b'8  \bar "|"   a'8    e'8    e'4 ^"~"    g'8    e'8    d''8 
   b'8  \bar "|"   c''4    \tuplet 3/2 {   c''8    b'8    a'8  }   g''8    a''8  
  g''16    a''16    g''16    e''16  \bar "|"   d''8    b'8    a'8    d''8    
b'8    g'8    g'8    e'8  \bar "||"     d''8    c''8    d''8    e'8    g'4 
^"~"    g'8    a'8  \bar "|"   b'8    a'8    b'8    d''8    e''8    d''8    b'8 
   e''8  \bar "|"   d''4    d''8    e''8    g''8    a''8    g''16    a''16    
g''16    e''16  \bar "|"   d''8    b'8    a'8    d''8    b'8    g'8    g'8    
e'8  \bar "|"     d''8    c''8    d''8    e''8    g''4    g'8    a'8  \bar "|" 
\tuplet 3/2 {   cis''8    b'8    a'8  }   b'8    d''8    e''8    d''8    b'8    
e''8  \bar "|"   d''8    b'8    d''8    e''8    g''8    a''8    g''16    a''16  
  g''16    e''16  \bar "|"   d''8    b'8    a'8    c''8    b'8    g'8    g'4 
^"~"  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
