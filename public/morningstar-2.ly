\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Innocent Bystander"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/828#setting13979"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/828#setting13979" {"https://thesession.org/tunes/828#setting13979"}}}
	title = "Morning Star, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   g'8    a'8  \bar "|"   b'4    b'8    a'8    b'8    
g'8    e'8    fis'8  \bar "|"   g'4    b'8    d''8    e''8    fis''8    g''8    
d''8  \bar "|"   b'4    b'8    a'8    b'8    g'8    e'8    fis'8  \bar "|"   
g'8    b'8    a'8    fis'8    g'4    g'8    a'8  \bar "|"   b'4    b'8    a'8   
 b'8    g'8    e'8    fis'8  \bar "|"   g'8    a'8    b'8    d''8    e''8    
fis''8    g''8    d''8  \bar "|"   b'4    b'8    a'8    b'8    g'8    e'8    
fis'8  \bar "|"   g'8    b'8    a'8    g'8    g'2  }   \repeat volta 2 {   g'8  
  a'8  \bar "|"   b'8    d''8    e''8    fis''8    g''4    e''8    fis''8  
\bar "|"   fis''4    d''8    fis''8    e''8    d''8    b'8    a'8  \bar "|"   
b'8    d''8    e''8    fis''8    g''8    fis''8    g''8    a''8  \bar "|"   
a''8    fis''8    d''8    fis''8    e''4    e''8    d''8  \bar "|"   b'8    
d''8    e''8    fis''8    g''4    e''8    g''8  \bar "|"   fis''4    e''8    
fis''8    e''8    d''8    b'8    a'8  \bar "|"   b'8    d''8    d''8  \grace {  
  e''8  }   cis''8    d''8    e''8    e''8    fis''8  \bar "|"   g''8  
\grace {    e''8  }   fis''8    a''8    fis''8    e''2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
