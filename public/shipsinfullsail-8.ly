\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ellen V"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/261#setting23977"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/261#setting23977" {"https://thesession.org/tunes/261#setting23977"}}}
	title = "Ships In Full Sail, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   fis'8    \bar "|"   e'8    a'8    a'8    cis''8    
a'8    a'8  \bar "|"   e''8    a'8    a'8    cis''8    a'8    a'8  \bar "|"   
fis''8    gis''8    a''8    e''8    cis''8    a'8  \bar "|"   b'8    d''8    
cis''8    b'8    a'8    fis'8  \bar "|"   e'8    a'8    a'8    cis''8    a'8    
a'8  \bar "|"   e''8    a'8    a'8    cis''8    a'8    a'8  \bar "|"   fis''8   
 a''8    fis''8    e''8    cis''8    a'8    \bar "|"   d''8    b'8    gis'8    
a'4    \bar ":|."   d''8    b'8    gis'8    a'4    fis''8  \bar "|"     
\bar ".|:"   e''8    a''8    a''8    cis'''8    a''8    a''8    \bar "|"   b''8  
  a''8    b''8    cis'''8    a''8    fis''8    \bar "|"   e''8    a''8    a''8  
  cis'''8    a''8    a''8    \bar "|"   b''4.    b''8    a''8    fis''8    
\bar "|"   e''8    a''8    a''8    cis'''8    a''8    a''8    \bar "|"   b''8   
 a''8    b''8    cis'''8    a''8    e''8    \bar "|"   fis''8    gis''8    a''8 
   e''8    cis''8    a'8    \bar "|"   d''8    b'8    gis'8    a'4    fis''8    
\bar ":|."   d''8    b'8    gis'8    a'8    cis''8    d''8  \bar "|"     
\bar ".|:"   e''4    fis''8    e''8    cis''8    a'8    \bar "|"   fis''8    
e''8    fis''8    a''4    fis''8    \bar "|"   e''8    a''8    fis''8    e''8   
 cis''8    a'8    \bar "|"   b'4.    b'8    cis''8    d''8    \bar "|"   e''4   
 fis''8    e''8    cis''8    a'8    \bar "|"   fis''8    e''8    fis''8    a''4 
   fis''8    \bar "|"   e''8    cis''8    a'8    b'8    a'8    b'8    \bar "|"  
 cis''8    a'8    a'8    a'4    fis''8    \bar ":|."   cis''8    a'8    a'8    
a'4.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
