\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Steve Owen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1098#setting29732"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1098#setting29732" {"https://thesession.org/tunes/1098#setting29732"}}}
	title = "Haunted House, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key a \major   a'4    a'8    b'8    a'8    b'8  \bar "|"   cis''8    
a'8    fis'8    fis'8    e'8    fis'8  \bar "|"   a'8    cis''8    e''8    
fis''8    a''8    e''8  \bar "|"   fis''8    a''8    e''8    fis''8    e''8    
cis''8  \bar "|"   a'4    a'8    b'8    a'8    b'8  \bar "|"   cis''8    a'8    
fis'8    fis'8    e'8    fis'8  \bar "|"   a'8    cis''8    e''8    fis''8    
a''8    fis''8  \bar "|"   e''8    cis''8    b'8    a'4    e'8  }     
\repeat volta 2 {   a'8    cis''8    e''8    fis''8    a''8    e''8  \bar "|"   
fis''8    a''8    e''8    fis''8    e''8    cis''8  \bar "|"   a'8    cis''8    
d''8    e''4. ^"~"  \bar "|"   e''8    fis''8    e''8    e''8    cis''8    b'8  
} \alternative{{   a'8    cis''8    e''8    fis''8    a''8    e''8  \bar "|"   
fis''8    a''8    e''8    fis''8    e''8    cis''8  \bar "|"     a'8    cis''8  
  e''8    fis''8    a''8    fis''8  \bar "|"   e''8    cis''8    b'8    a'4    
e'8  } {   a'4    a'8    b'8    a'8    b'8  \bar "|"   cis''8    a'8    fis'8   
 fis'8    e'8    fis'8  \bar "|"   a'8    cis''8    e''8    fis''8    a''8    
fis''8  \bar "|"   e''8    cis''8    b'8    a'4    e'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
