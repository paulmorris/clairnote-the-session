\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1305#setting1305"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1305#setting1305" {"https://thesession.org/tunes/1305#setting1305"}}}
	title = "Bush On The Hill, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   \repeat volta 2 {   a4. ^"~"    cis'8    e'8    a'8   
 \bar "|"   a'8    e'8    a'8    cis''8    e''8    a''8    \bar "|"   fis''8    
e''8    d''8    cis''8    d''8    b'8    \bar "|"   cis''8    a'8    fis'8    
fis'8    e'8    fis'8    \bar "|"     a4. ^"~"    cis'8    e'8    a'8    
\bar "|"   a'8    e'8    a'8    cis''8    e''8    a''8    \bar "|"   fis''8    
e''8    d''8    cis''8    d''8    b'8    \bar "|"   cis''8    a'8    gis'8    
a'4    r8   }     \repeat volta 2 {   a'8    cis''8    e''8    a''8    gis''8   
 a''8    \bar "|"   fis''8    a''8    fis''8    e''8    cis''8    a'8    
\bar "|"   d''8    fis''8    d''8    cis''8    e''8    cis''8    \bar "|"   b'8 
   a'8    b'8    cis''8    a'8    fis'8    \bar "|"     a'8    cis''8    e''8   
 a''8    gis''8    a''8    \bar "|"   fis''8    a''8    fis''8    e''8    
cis''8    a'8    \bar "|"   d''8    fis''8    d''8    cis''8    e''8    cis''8  
  \bar "|"   b'8    a'8    gis'8    a'4    r8   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
