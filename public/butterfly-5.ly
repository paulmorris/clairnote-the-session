\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/10#setting21788"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/10#setting21788" {"https://thesession.org/tunes/10#setting21788"}}}
	title = "Butterfly, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   b'4 ^"Em"   e'8    g'4    e'8     
 fis'4. ^"D" \bar "|"   b'4 ^"Em"   e'8    g'4    e'8      fis'8 ^"D"   e'8    
d'8  \bar "|"   b'4 ^"Em"   e'8    g'4    e'8      fis'4. ^"D" \bar "|"   b'4 
^"G"   d''8    d''4    b'8      a'8 ^"D"   fis'8    d'8  }     
\repeat volta 2 {   b'4 ^"Em"   d''8    e''4    fis''8    g''4.  \bar "|"   b'4 
   d''8    g''4    e''8      d''8 ^"D"   b'8    a'8  \bar "|"   b'4 ^"Em"   
d''8    e''4    fis''8    g''4    a''8  \bar "|"   b''4 ^"G"   a''8    g''4    
e''8      d''8 ^"D"   b'8    a'8  }     \repeat volta 2 {   b'4. ^"Em"   b'4    
a'8    g'4    a'8  \bar "|"   b'4.    b'8    a'8    b'8      d''8 ^"D"   b'8    
a'8  \bar "|"   b'4. ^"Em"   b'4    a'8    g'4    a'8  \bar "|"   b'4 ^"G"   
d''8    g''4    e''8      d''8 ^"D"   b'8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
