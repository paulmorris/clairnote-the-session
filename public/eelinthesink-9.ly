\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1446#setting28567"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1446#setting28567" {"https://thesession.org/tunes/1446#setting28567"}}}
	title = "Eel In The Sink, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \major   \bar "|"   cis''4    fis'8    gis'8    a'8    b'8    
cis''8    a'8  \bar "|"   b'4    gis'8    e'8    b'8    e'8    \tuplet 3/2 {   
gis'8    a'8    b'8  } \bar "|"   cis''8    fis'8    fis'8    gis'8    a'8    
b'8    cis''8    dis''8  \bar "|"   e''8    cis''8    b'8    gis'8    cis''8    
fis'8    fis'4  \bar "|"     cis''4    fis'8    gis'8    a'8    b'8    cis''8   
 a'8  \bar "|"   b'4    gis'8    e'8    b'8    e'8    \tuplet 3/2 {   gis'8    
a'8    b'8  } \bar "|"   cis''8    fis'8    fis'8    gis'8    a'8    b'8    
cis''8    dis''8  \bar "|"   e''8    cis''8    b'8    gis'8    cis''8    fis'8  
  fis'4  \bar "||"     \bar "|"   fis''4    cis''8    fis''8    fis''8    e''8  
  cis''8    b'8  \bar "|" \tuplet 3/2 {   gis'8    a'8    b'8  }   cis''8    
dis''8    e''4.    e''8  \bar "|"   fis''4    cis''8    fis''8    fis''8    
e''8    fis''8    gis''8  \bar "|"   e''8    cis''8    b'8    gis'8    cis''8   
 fis'8    fis'4  \bar "|"     fis''4    cis''8    fis''8    fis''8    e''8    
cis''8    b'8  \bar "|" \tuplet 3/2 {   gis'8    a'8    b'8  }   cis''8    
dis''8    e''4.    e''8  \bar "|"   fis''8    dis''8    e''8    cis''8    
dis''8    b'8    cis''8    dis''8  \bar "|" \tuplet 3/2 {   e''8    dis''8    
cis''8  }   b'8    gis'8    gis'8    fis'8    fis'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
