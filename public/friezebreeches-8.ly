\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/34#setting12440"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/34#setting12440" {"https://thesession.org/tunes/34#setting12440"}}}
	title = "Frieze Breeches, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   a'4    b'8    cis''4    b'8    \bar "|"   cis''4    
d''8    cis''8    a'8    g'8    \bar "|"   a'4 ^"~"    d''8    d''8    cis''8   
 d''8    \bar "|"   \tuplet 3/2 {   g''8 -.   fis''8 -.   e''8 -. }   d''8 -.   
cis''8    a'8    g'8    \bar "|"   a'4 ^"~"    b'8    cis''8    a'8    g'8    
\bar "|"   d''8    cis''8    b'8  \grace {    d''16  }   cis''8    a'8    g'8   
 \bar "|"   fis'8    a'8 -.   a'8 -. \grace {    a'16  }   g'8    e'8  
\grace {    b'16  }   a'8    \bar "|"   d'4. ^"~"  \grace {    a'16  }   d'4.   
 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
