\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tim Gillespie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/55#setting22733"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/55#setting22733" {"https://thesession.org/tunes/55#setting22733"}}}
	title = "Kesh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   a8    \repeat volta 2 {   d'4. ^"D"   d'8    e'8    
fis'8    \bar "|"     e'4. ^"A"   e'8    fis'8    a'8    \bar "|"     b'8 ^"D"  
 a'8    a'8      d''8 ^"G"   a'8    a'8    \bar "|"     b'8 ^"D"   a'8    fis'8 
     a'8 ^"A"   fis'8    e'8    \bar "|"       d'4. ^"D"   d'8    e'8    fis'8  
  \bar "|"     e'4. ^"A"   e'8    fis'8    a'8    \bar "|"     b'8 ^"D"   a'8   
 a'8      d''8 ^"G"   a'8    fis'8    \bar "|"     e'8 ^"A"   d'8    cis'8      
d'4. ^"D"   }     \repeat volta 2 {   \grace {    e'8 ^"D" }   fis'4.    a'8    
fis'8    a'8    \bar "|"     b'8 ^"G"   d''8    b'8    b'8    a'8    fis'8    
\bar "|"   \grace {    e'8 ^"D" }   fis'4.    a'8    fis'8    d'8    \bar "|"   
  e'8 ^"A"   fis'8    e'8    e'8    d'8    e'8    \bar "|"     \grace {    e'8 
^"D" }   fis'4.    a'8    fis'8    a'8    \bar "|"     b'8 ^"G"   d''8    b'8   
   b'4 ^"D"   b'8    \bar "|"     d''8 ^"D"   cis''8    d''8      e''8 ^"G"   
d''8    e''8    \bar "|"     fis''8 ^"A"   d''8    cis''8      d''4. ^"D"   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
