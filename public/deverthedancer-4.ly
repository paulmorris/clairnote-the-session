\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bogman"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/148#setting12772"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/148#setting12772" {"https://thesession.org/tunes/148#setting12772"}}}
	title = "Dever The Dancer"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key e \minor   \repeat volta 2 {   b'8    e'8    e'8    b'8    e'8   
 fis'8    g'4    a'8    \bar "|"   b'8    e'8    e'8    b'8    a'8    g'8    
fis'8    g'8    a'8    \bar "|"   b'8    e'8    e'8    b'8    e'8    fis'8    
g'4    a'8    \bar "|"   b'8    d''8    d''8    a'8    b'8    g'8    fis'8    
e'8    d'8    }   d''8    e''8    d''8    c''8    a'8    fis'8    g'4    a'8    
\bar "|"   b'8    d''8    d''8    d''8    e''8    fis''8    g''8    fis''8    
e''8    \bar "|"   d''8    e''8    g''8    c''16    b'16    a'8    fis'8    g'4 
   a'8    \bar "|"   b'8    d''8    d''8    a'8    b'8    g'8    fis'8    e'8   
 d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
