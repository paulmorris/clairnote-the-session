\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jon Kiparsky"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/885#setting885"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/885#setting885" {"https://thesession.org/tunes/885#setting885"}}}
	title = "Wizard's Walk, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   e''4    d''8    c''8    b'8    a'8    g'8    fis'8  
\bar "|"   g'8    a'8    fis'8    g'8    e'4    b'4  \bar "|"   e''4    e''8    
fis''8    g''8    fis''8    e''8    fis''8  \bar "|"   g''8    a''8    fis''8   
 g''8    e''2  \bar "|"     a''4    g''8    fis''8    e''8    d''8    c''8    
b'8  \bar "|"   c''8    d''8    b'8    c''8    a'4    e'4  \bar "|"   a'4    
a'8    b'8    c''8    b'8    a'8    b'8  \bar "|"   c''8    d''8    b'8    c''8 
   a'2  \bar "||"     b'8    b'8  <<   b'8    fis''8   >> b'8    b'8  <<   b'8  
  fis''8   >> b'8    b'8  \bar "|" <<   c''8    g''8   >> c''8    c''8  <<   
c''8    g''8   >>   c''8    c''8  <<   c''8    g''8   >> c''8  \bar "|"   b'8   
 b'8  <<   b'8    fis''8   >> b'8    b'8  <<   b'8    fis''8   >> b'8    b'8  
\bar "|" <<   c''8    g''8   >> c''8    c''8  <<   c''8    g''8   >>   c''8    
c''8  <<   c''8    g''8   >> c''8  \bar "|"   <<   b'8    fis''8   >> b'8    
b'8    b'8  <<   c''8    g''8   >> c''8    c''8    c''8  \bar "|" <<   b'8    
fis''8   >> b'8    b'8    b'8  <<   c''8    g''8   >> c''8    c''8    c''8  
\bar "|" <<   b'8    fis''8   >> b'8    cis''8    cis''8    dis''8    dis''8    
e''8    e''8  \bar "|"   fis''8    fis''8    g''8    g''8    a''8    a''8    
b''8    b''8  \bar "|"     \repeat volta 2 {   e''8    g''8    b''8    e''8    
b''8    g''8    e''8    g''8  \bar "|"   dis''8    fis''8    b''8    fis''8    
b''8    fis''8    dis''8    fis''8  \bar "|"   d''!8    fis''8    a''8    b''8  
  a''8    fis''8    d''8    b'8  \bar "|"   cis''8    e''8    a''8    e''8    
a''8    e''8    cis''8    a'8  \bar "|"     c''!8    e''8    g''8    e''8    
c''8    e''8    g''8    e''8  \bar "|"   b'8    e''8    g''8    e''8    b'8    
e''8    g''8    e''8  \bar "|"   bes'8    e''8    g''8    e''8    bes'8    e''8 
   g''8    e''8  \bar "|"   b'!8    e''8    g''8    e''8    fis''8    e''8    
dis''8    e''8  }     \repeat volta 2 {   |
 e''4 ^"Coda"   d''8    c''8    b'8    a'8    g'8    fis'8    g'4    e''4    
fis'4    dis''4  \bar "|"   e''2    b'4    g'4  \bar "|"   e'2    e''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
