\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/910#setting14099"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/910#setting14099" {"https://thesession.org/tunes/910#setting14099"}}}
	title = "Windbroke"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   b'8    e'8    e'16    e'16    e'8    g'8    a'8    
b'8    e'16 (   e'16  \bar "|"   e'8  -)   g'8    a'8    d''8    b'4    a'8    
g'8  \bar "|"   b'8    e'8    e'16    e'16    e'8    g'8    a'8    b'8    c''8 
( \bar "|"   c''8  -)   b'8    a'8    g'8    b'4    a'8    g'8  \bar "|"     
b'8    e'8    e'4 ^"~"    g'8    a'8    b'8      e'8 (^"~"  \bar "|"   e'8  -)  
 g'8    a'8    d''8    b'4    a'8    b'8  \bar "|"   e''8    d''8    d''8    
e''8    b'4.    g'8  \bar "|"   a'4.    g'8    fis'8    g'8    a'8    c''8  
\bar ":|."   a'4    a'8    g'8    fis'8    g'8    a'4  \bar "||"     b'8    
e''8    g''8    fis''8    e''8    fis''8    d''8    e''8 -. \bar "|"   r8 e''8  
  d''8    fis''8    e''8    fis''8    e''8    d''8  \bar "|"   b'8    e''8    
g''8    fis''8    e''8    fis''8    g''8    a''8 -. \bar "|"   r8 a''4    g''8  
  fis''8    e''8    d''8    e''8  \bar "|"     b'8    e''8    g''8    fis''8    
e''8    fis''8    d''8    e''8 -. \bar "|"   r8 e''8    d''8    fis''8    e''8  
  fis''8    e''8    b'8  \bar "|"   c''4.    a'8    b'4.    g'8  \bar "|"   a'4 
   a'8    g'8    fis'8    g'8    a'4  \bar ":|."   c''1   ~    \bar "|"   c''8  
  d''8    \tuplet 3/2 {   c''8    b'8    a'8  }   fis'8    g'8    a'8    c''8  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
