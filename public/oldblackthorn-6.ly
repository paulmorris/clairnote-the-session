\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tijn Berends"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/720#setting30088"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/720#setting30088" {"https://thesession.org/tunes/720#setting30088"}}}
	title = "Old Blackthorn, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \major   \bar "||"   e''4  \bar "|"   f''8    a'8    a'8    
d''8    c''8    a'8    g'8    a'8  \bar "|"   f'8    f''8    f''8    e''8    
d''8    c''8    d''8    e''8  \bar "|"   f''8    a'8    a'8    d''8    c''8    
a'8    f'8    a'8  \bar "|"   g'8    f'8    e'8    g'8    f'8    bes''8    a''8 
   g''8  \bar "|"     \bar "|"   f''8    a'8    a'8    d''8    c''8    a'8    
g'8    a'8  \bar "|"   f'8    f''8    f''8    e''8    d''8    c''8    d''8    
e''8  \bar "|"   f''8    a'8    a'8    d''8    c''8    a'8    f'8    a'8  
\bar "|"   g'8    f'8    e'8    g'8    f'4    f''4 ^"~"  \bar "||"     \bar "|" 
  f''4    a''16    g''16    f''8    g''8    bes''8    a''8    f''8  \bar "|"   
c''4    e''16    d''16    c''8    g''8    c''8    e''8    c''8  \bar "|"   f''8 
   g''8    a''8    bes''8    c'''4    c'''8    d'''8  \bar "|"   c'''8    a''8  
  g''8    bes''8    a''8    f''8    f''4 ^"~"  \bar "|"     \bar "|"   f''4    
a''16    g''16    f''8    g''8    bes''8    a''8    f''8  \bar "|"   c''4    
e''16    d''16    c''8    g''8    c''8    e''8    c''8  \bar "|"   f''8    g''8 
   a''8    bes''8    c'''4    c'''8    d'''8  \bar "|"   c'''8    a''8    g''8  
  a''8    f''2  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
