\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1424#setting24747"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1424#setting24747" {"https://thesession.org/tunes/1424#setting24747"}}}
	title = "Long Note, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   d'4.    a'8    a'8    g'8    e'8    fis'8  
\bar "|"   g'8    e'8    e'4 ^"~"    c''8    e'8    d''8    a'8  \bar "|"   
d'4.    a'8    a'8    g'8    e'8    g'8  \bar "|"   a'4    g'8    a'8    e'8    
a'8    g'8    e'8  \bar ":|."   a'4    g'8    a'8    e'8    a'8    d'8    a'8  
\bar "||"     d''4.    b'8    c''8    a'8    d''8    b'8  \bar "|"   c''8    
a'8    d''8    b'8    c''8    a'8    g'4  \bar "|"   d''4.    b'8    c''8    
a'8    \tuplet 3/2 {   b'8    c''8    d''8  } \bar "|"   e''4    a''8    g''8    
e''4 ^"~"    d''4  \bar "|"     d''4.    b'8    c''8    a'8    d''8    b'8  
\bar "|"   c''8    a'8    d''8    b'8    c''8    a'8    g'8    e'8  \bar "|"   
d'4    \tuplet 3/2 {   e'8    fis'8    g'8  }   a'4    d''8    c''8  \bar "|"   
a'4 ^"~"    g'8    a'8    e'8    a'8    d'4  \bar "||"     a''4 ^"~"    a''8    
b''8    a''8    fis''8    d''4  \bar "|"   g''4 ^"~"    g''8    a''8    g''8    
e''8    cis''8    e''8  \bar "|"   a''8    gis''8    a''8    b''8    a''8    
fis''8    d''8    fis''8  \bar "|"   e''8    a''8    a''8    g''8    e''4 ^"~"  
  d''4  \bar "|"     a''4 ^"~"    a''8    b''8    a''8    fis''8    d''4  
\bar "|" \tuplet 3/2 {   g''8    fis''8    e''8  }   \tuplet 3/2 {   fis''8    
e''8    d''8  }   e''8    d''8    cis''8    b'8  \bar "|"   a'8    b'8    c''8  
  d''8    e''4    d''8    cis''8  \bar "|"   a'4 ^"~"    g'8    a'8    e'8    
a'8    g'8    e'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
