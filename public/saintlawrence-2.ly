\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1081#setting14314"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1081#setting14314" {"https://thesession.org/tunes/1081#setting14314"}}}
	title = "Saint Lawrence"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   b'8    \bar "|"   a'8    b'8    a'8    fis'8    a'8   
 d''8    \bar "|"   fis''4    d''8    a'4    d''8    \bar "|"   cis''4    b'8   
 g'4    b'8    \bar "|"   b'4    a'8    fis'4    g'8    \bar "|"     a'8    b'8 
   a'8    fis'8.    a'16    d''8    \bar "|"   fis''4    d''8    a'4    d''8    
\bar "|"   cis''8    d''8    e''8    a'8    b'8    cis''8    \bar "|"   d''4.   
 d''4    b'8    \bar "|"     a'8    b'8    a'8    fis'8    a'8    d''8    
\bar "|"   fis''4    d''8    a'4    d''8    \bar "|"   cis''4    b'8    g'4    
b'8    \bar "|"   b'8    cis''8    b'8    fis'4    g'8    \bar "|"     a'8.    
b'16    a'8    fis'8    a'8    d''8    \bar "|"   fis''4    d''8    a'4    d''8 
   \bar "|"   cis''4    e''8    b'4    cis''8    \bar "|"   d''4.    d''4    
\bar "||"     \repeat volta 2 {   fis''8    \bar "|"   a''4    g''8    b'4.    
\bar "|"   g''4    fis''8    a'4.    \bar "|"   d''4    cis''8    cis''8    b'8 
   cis''8    \bar "|"   e''8    d''8    b'8    a'4    fis''8    \bar "|"     
a''4    g''8    b'8    ais'8    b'8    \bar "|"   g''4    fis''8    a'4.    
\bar "|"   d''4    cis''8    cis''8    b'8    cis''8    \bar "|"   e''8    d''8 
   d''8    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
