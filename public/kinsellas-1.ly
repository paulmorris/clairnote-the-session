\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Paul-Kin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/956#setting956"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/956#setting956" {"https://thesession.org/tunes/956#setting956"}}}
	title = "Kinsella's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   d'8    f'8    f'4 ^"~"    a'8    f'8    e'8    f'8    
\bar "|"   bes8    d'8    f'8    d'8    a8    d'8    f'8    d'8    \bar "|"   
d'8    f'8    f'4 ^"~"    a'8    f'8    e'8    f'8    \bar "|"   g'8    e'8    
e'4 ^"~"    f'8    d'8    d'4 ^"~"    \bar "|"     d'8    f'8    f'4 ^"~"    
a'8    f'8    e'8    f'8    \bar "|"   bes8    d'8    d'4 ^"~"    a8    d'8    
d'4 ^"~"    \bar "|"   d'8    f'8    a'8    f'8    g'8    a'8    bes'8    g'8   
 \bar "|"   a'8    d''8    d''4 ^"~"    e''8    d''8    cis''8    d''8    
\bar "||"   \key d \major <<   d'4    a'4   >> fis'8    a'8    d''8    fis''8   
 e''8    d''8    \bar "|"   d'8    fis'8    fis'4 ^"~"    e'8    fis'8    g'8   
 e'8    \bar "|" <<   d'4    a'4   >> fis'8    a'8    d''8    fis''8    a''8    
fis''8    \bar "|"   g''8    e''8    cis''8    e''8    d''8    b'8    a'8    
fis'8    \bar "|"     d'4    fis'8    a'8    d''8    fis''8    e''8    d''8    
\bar "|"   d'8    fis'8    fis'4 ^"~"    g'8    e'8    cis'8    e'8    \bar "|" 
  d'8    fis'8    e'8    fis'8    d'4    d''8    b'8    \bar "|"   a'8    fis'8 
   fis'4 ^"~"    e'8    fis'8    g'8    e'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
