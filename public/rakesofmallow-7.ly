\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/85#setting12596"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/85#setting12596" {"https://thesession.org/tunes/85#setting12596"}}}
	title = "Rakes Of Mallow, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g'8    b'8    g'8    b'8    
\bar "|"   g'8    b'8    c''16    b'16    a'16    g'16    \bar "|"   fis'8    
a'8    fis'8    a'8    \bar "|"   fis'8    a'8    d''16    c''16    b'16    
a'16    \bar "|"   g'8    b'8    g'8    b'8    \bar "|"   g'8    b'8    d''4    
\bar "|"   c''16    b'16    a'16    g'16    fis'16    g'16    a'16    c''16    
\bar "|"   b'8    g'8    g'4    }   \repeat volta 2 {   g''8    fis''16    
e''16    d''8    c''8    \bar "|"   b'16    a'16    b'16    c''16    d''4    
\bar "|"   g''8    fis''16    e''16    d''8    c''8    \bar "|"   b'16    a'16  
  b'16    c''16    a'4    \bar "|"   g''8    fis''16    e''16    d''8    c''8   
 \bar "|"   b'16    a'16    b'16    c''16    d''4    \bar "|"   c''16    b'16   
 a'16    g'16    fis'16    g'16    a'16    c''16    \bar "|"   b'8    g'8    
g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
