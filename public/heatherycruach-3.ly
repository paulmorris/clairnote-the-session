\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Donough"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1265#setting14579"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1265#setting14579" {"https://thesession.org/tunes/1265#setting14579"}}}
	title = "Heathery Cruach, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'16    b'16    cis''8  \repeat volta 2 {   d''4    
a'8    g'8    fis'8    d'8    a'8    g'8  \bar "|"   fis'8    d'8    a'8    
fis'8    e'8    a'8    b'8    cis''8  \bar "|"   d''4    a'8    g'8    fis'8    
d'8    a'8    g'8  \bar "|"   fis'8    g'8    a'8    fis'8    d'8    g''8    
fis''8    e''8  \bar "|"   d''4    a'8    g'8    fis'8    d'8    d'8    e'8  
\bar "|"   fis'8    g'8    a'8    fis'8    e'4    fis'8    g'8  \bar "|"   a'8  
  b'8    a'8    g'8    fis'8    d'8    d'8    e'8  } \alternative{{   fis'8    
g'8    a'8    fis'8    d'4  } }    fis'8    g'8    a'8    fis'8    d'8    
fis''8    e''8    d''8  \bar "||"   \repeat volta 2 {   cis''8    a'8    a'4    
cis''8    a'8    g'8    a'8  \bar "|"   cis''8    b'8    cis''8    d''8    e''8 
   fis''8    e''8    d''8  \bar "|"   cis''8    a'8    a'4    cis''8    a'8    
g'8    e'8  \bar "|"   fis'8    a'8    g'8    e'8    d'8    fis''8    e''8    
d''8  \bar "|"   cis''8    a'8    a'4    cis''8    a'8    g'8    a'8  \bar "|"  
 cis''8    b'8    cis''8    d''8    e''4    e''4 ^"~"  \bar "|"   fis''4    
e''8    d''8    cis''8    a'8    a'8    g'8  } \alternative{{   e'8    fis'8    
g'8    e'8    a'8    fis''8    e''8    d''8  } }    e'8    fis'8    g'8    e'8  
  d'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
