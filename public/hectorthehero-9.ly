\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "paul95"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1292#setting27742"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1292#setting27742" {"https://thesession.org/tunes/1292#setting27742"}}}
	title = "Hector The Hero"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \major   a8  \bar "|"   cis'8. ^"A"   b16    a8      fis'8 
^"D"   e'8    cis'8  \bar "|"   e'4. ^"A"   e'4    a8  \bar "|"   cis'8. ^"A"   
b16    a8      fis'8 ^"D"   e'8    cis'8  \bar "|"   b4. ^"E"   b4    a8  
\bar "|"       cis'8. ^"A"   b16    a8      fis'8 ^"D"   e'8    cis'8  \bar "|" 
  e'4 ^"A"   a8      a'4 ^"D"   fis'8  \bar "|"   e'8 ^"A"   a8    cis'8      
b4 ^"E"   a8  \bar "|"   a4. ^"A"   a4    \bar "||"     a'8  \bar "|"   cis''8. 
^"A"   b'16    a'8      fis''8 ^"D"   e''8    cis''8  \bar "|"   e''4. ^"A"   
e''4    a'8  \bar "|"   cis''8. ^"A"   b'16    a'8      fis''8 ^"D"   e''8    
cis''8  \bar "|"   b'4. ^"E"   b'4    a'8  \bar "|"       cis''8. ^"A"   b'16   
 a'8      fis''8 ^"D"   e''8    cis''8  \bar "|"   e''4 ^"A"   a'8    a''4    
fis''8  \bar "|"   e''8 ^"A"   a'8    cis''8      b'4 ^"E"   a'8  \bar "|"   
a'4. ^"A"   a'4  \bar "||"     cis''8  \bar "|"   fis''8 ^"F\#m"   gis''8    
fis''8      a''8 ^"D"   gis''8    fis''8  \bar "|"   e''4. ^"A"   e''4    
cis''8  \bar "|"   fis''8 ^"F\#m"   e''8    cis''8      e''8 ^"A"   a'8    
cis''8  \bar "|"   b'4. ^"E"   b'4    cis''8  \bar "|"       fis''8 ^"F\#m"   
gis''8    fis''8      a''8 ^"D"   gis''8    fis''8  \bar "|"   e''8 ^"A"   a'8  
  b'8      cis''16 ^"D"   a''8.    fis''8  \bar "|"   e''8 ^"A"   a'8    cis''8 
     b'4 ^"E"   a'8  \bar "|"   a'4. ^"A"   a'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
