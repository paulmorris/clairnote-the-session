\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "troisrive"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/272#setting13013"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/272#setting13013" {"https://thesession.org/tunes/272#setting13013"}}}
	title = "Rickett's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \major   f''8.    c''16   ~    c''8    f''16    e''16  
\bar "|"   d''8    g''16    f''16    e''8    d''16    c''16    \bar "|"   d''8. 
   c''16   ~    c''8    bes'16    a'16  \bar "|"   g'8    c''16    bes'16    
a'8    g'16    f'16  \bar "|"   f''8.    c''16   ~    c''8    f''16    e''16  
\bar "|"   d''8    g''16    f''16    e''8    d''16    c''16    \bar "|"   d''8  
  c''8   ~    c''8    b'8    \bar "|"   c''4    r8   a'8  \bar "|"   bes'16    
c''16    d''16    e''16    f''8.    f''16    \bar "|"   g''16    a''16    
bes''16    a''16    g''8.    f''16    \bar "|"   g''8    f''8    f''8    e''8   
 \bar "|"   f''4    r8   a'8    \bar "|"   bes'16    c''16    d''16    e''16    
f''8.    f''16    \bar "|"   g''16    a''16    bes''16    a''16    g''8.    
f''16    \bar "|"   g''8    f''8    f''8    e''8    \bar "|"   f''4.    r8   
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
