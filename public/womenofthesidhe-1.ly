\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "RTP"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1350#setting1350"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1350#setting1350" {"https://thesession.org/tunes/1350#setting1350"}}}
	title = "Women Of The Sidhe"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   e''8    g''8    fis''8    e''8    
d''8    b'8    \bar "|"   e''8    b''8    b''8    e''8    a''8    a''8    
\bar "|"   e''8    g''8    fis''8    e''8    d''8    e''8    \bar "|"   g''8    
fis''8    e''8    d''8    e''8    b'8    \bar "|"     e''8    g''8    fis''8    
e''8    d''8    b'8    \bar "|"   e''8    b''8    b''8    e''8    a''8    a''8  
  \bar "|"   e''8    g''8    fis''8    e''8    d''8    e''8    \bar "|"   g''8  
  fis''8    e''8    e''4    b'8    }     \repeat volta 2 {   c''8    d''8    
a''8    c''8    g''8    c''8    \bar "|"   e''8    d''8    c''8    b'8    c''8  
  a'8    \bar "|"   c''8    g'8    c''8    a'8    c''8    d''8    \bar "|"   
e''8    d''8    c''8    a'8    b'8    c''8    \bar "|"     c''8    d''8    a''8 
   c''8    g''8    c''8    \bar "|"   e''8    d''8    c''8    b'8    c''8    
a'8    \bar "|"   c''8    g'8    c''8    a'8    b'8    c''8    } \alternative{{ 
  d''8    e''8    c''8    d''4    b'8    } {   d''8    e''8    b'8    d''8    
r4   \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
