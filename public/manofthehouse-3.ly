\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/222#setting12906"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/222#setting12906" {"https://thesession.org/tunes/222#setting12906"}}}
	title = "Man Of The House, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   \repeat volta 2 {   d'4    a'8    d'8    \tuplet 3/2 { 
  e'8    f'8    g'8  }   a'8    f'8    \bar "|"   \tuplet 3/2 {   d'8    d'8    
d'8  }   a'8    f'8    g'8    e'8    c'8    e'8    \bar "|" <<   d'4    d'4   
>>   a'8    d'8    \tuplet 3/2 {   e'8    f'8    g'8  }   a'8    d''8    
} \alternative{{   c''8  \grace {    d''8    c''8  }   a'8    g'8    bes'8    
a'8    g'8    f'8    e'8    } {   c''8  \grace {    d''8    c''8  }   a'8    
g'8    bes'8    a'8    d'8    f'8    a'8    \bar "||"   \bar "|"   d''8    e''8 
   f''8    d''8    e''8    f''8    g''8    e''8    \bar "|"   f''8    e''8    
d''8    c''8    d''8    c''8    a'8    c''8    \bar "|"   d''4. ^"~"    e''8    
f''4. ^"~"    a''8    \bar "|"   g''8    e''8    c''8    e''8    f''8    d''8   
 d''8    e''8    \bar "|"   \bar "|"   f''8    c''8    a'8    c''8    f''8    
g''8    a''8    f''8    \bar "|"   g''8    f''8    a''8    f''8    g''8    f''8 
   d''8    f''8    \bar "|"   a''8    f''8    g''8    f''8    d''8    f''8    
c''8    e''8    \bar "|"   d''8    a'8    a'8    g'8    \tuplet 3/2 {   e'8    
f'8    g'8  }   a'8    f'8    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
