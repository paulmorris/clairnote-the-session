\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/970#setting20984"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/970#setting20984" {"https://thesession.org/tunes/970#setting20984"}}}
	title = "Connaught Heifers, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   a'8    g'8  \repeat volta 2 {   fis'4. ^"~"    
g'8    a'4 ^"~"    b'8    g'8  \bar "|" \tuplet 3/2 {   a'8 -.   b'8 -.   a'8 -. 
}   b'8    g'8    a'8    g'8    fis'8    d'8  \bar "|"   fis'4. ^"~"    g'8  
\grace {    b'8  }   a'8    g'8  \grace {    b'8  }   g'8    fis'8  \bar "|"   
d'8    g'8    g'8    g'8    c''8    g'8    a'8    g'8  \bar "|"     fis'4. 
^"~"    g'8    a'4 ^"~"    b'8    g'8  \bar "|" \tuplet 3/2 {   a'8 -.   b'8 -.  
 a'8 -. }   b'8    g'8    a'8    g'8    fis'8    d'8  \bar "|"   fis'4. ^"~"    
g'8  \grace {    b'8  }   a'8    g'8  \grace {    b'8  }   g'8    fis'8  
\bar "|"   d'8    g'8    g'8    b'8    c''8    a'8    a'8    g''8  \bar "||"    
 \bar "|"   fis''8    d''8    e''8    fis''8    d''4    c''8    a'8  \bar "|"   
a'8    a'8    b'8    g'8    a'8    b'8    d''8    e''8  \bar "|"   fis''8    
d''8    \tuplet 3/2 {   g''8 -.   fis''8 -.   e''8 -. }   d''4    c''8    a'8  
\bar "|"   b'8    g'8    g'4 ^"~"    d''8    g'8    g'8    g'8  \bar "|"     
\bar "|"   fis''8    d''8    e''8    c''8    d''4    c''8    a'8  \bar "|"   
a'8 -.   a'8 -.   b'8    g'8    a'8    b'8    d''8    g''8  \bar "|"   fis''4. 
^"~"    d''8    e''8    d''8    c''8    a'8  \bar "|"   b'8    g'8    g'4 ^"~"  
  a'8    g'8    fis'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
