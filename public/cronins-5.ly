\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Shortall"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/478#setting29744"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/478#setting29744" {"https://thesession.org/tunes/478#setting29744"}}}
	title = "Cronin's — Cronin's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major \time 4/4 \key g \major   b'8    a'8  \bar "|"   g'8    
a'8    b'8    d''8    d''8    b'8    d''8    e''8  \bar "|"   g''8    a''8    
g''8    e''8    d''8    e''8    g''8    a''8  \bar "|"   b''8    a''8    g''8   
 e''8    d''8    b'8    g'8    a'8  \bar "|"   b'8    a'8    a'8    g'8    a'8  
  c''8    b'8    a'8  \bar "|"     g'8    a'8    b'8    d''8    d''8    b'8    
d''8    e''8  \bar "|"   g''8    a''8    g''8    e''8    d''8    e''8    g''8   
 a''8  \bar "|"   b''8    a''8    g''8    e''8    d''8    b'8    a'8    b'8  
\bar "|"   g'4    g'8    fis'8    g'4  }     \repeat volta 2 {   fis''8    g''8 
 \bar "|"   a''8    d''8    d''8    cis''8    d''4    g''8    a''8  \bar "|"   
b''8    e''8    e''8    d''8    e''4    g''8    a''8  \bar "|"   b''8    g''8   
 a''8    fis''8    \tuplet 3/2 {   g''8    fis''8    e''8  }   d''8    b'8  
\bar "|"   b'8    a'8    a'8    g'8    a'8    c''8    b'8    a'8  \bar "|"     
g'8    a'8    b'8    d''8    d''8    b'8    d''8    e''8  \bar "|"   g''8    
a''8    g''8    e''8    d''8    e''8    g''8    a''8  \bar "|"   b''8    a''8   
 g''8    e''8    d''8    b'8    a'8    b'8  \bar "|"   g'4    g'8    fis'8    
g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
