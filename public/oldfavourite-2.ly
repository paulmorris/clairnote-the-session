\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Donald Potter"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/56#setting25038"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/56#setting25038" {"https://thesession.org/tunes/56#setting25038"}}}
	title = "Old Favourite, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major     b'4. ^"G"   b'8    a'8    b'8  \bar "|"   d''8    
b'8    a'8    g'4    b'8  \bar "|"   d''8    e''8    d''8    d''4    b'8  
\bar "|"   d''8    e''8    d''8      b'4 ^"D"   a'8  \bar "|"       b'4. ^"G"   
b'8    a'8    b'8  \bar "|"   d''8    b'8    a'8    g'4    b'8  \bar "|"   d''8 
   e''8    d''8      c''8 ^"D"   b'8    a'8  \bar "|"     g'4. ^"G"   g'4.    } 
    \repeat volta 2 {   g''4. ^"G"     fis''4. ^"D" \bar "|"     e''8 ^"C"   
fis''8    e''8      d''4 ^"G"   b'8  \bar "|"   d''8    e''8    d''8    d''4    
b'8  \bar "|"   d''8    e''8    d''8    b'8    d''8    fis''8  \bar "|"     
g''4. ^"G"     fis''4. ^"D" \bar "|"       e''8 ^"C"   fis''8    e''8      d''4 
^"G"   b'8  } \alternative{{     d''8 ^"D"   g''8    e''8    d''8    b'8    a'8 
 \bar "|"     g'4. ^"G"   g'4.    } {     d''8 ^"G"   g''8    e''8    d''8    
g''8    e''8  \bar "|"   d''8    g''8    e''8      d''8 ^"D"   b'8    a'8    
\bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
