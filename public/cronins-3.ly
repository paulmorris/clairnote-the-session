\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/478#setting25360"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/478#setting25360" {"https://thesession.org/tunes/478#setting25360"}}}
	title = "Cronin's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8.    e'16    \bar "|"   d'8. 
   e'16    fis'8.    g'16    a'8.    fis'16    \tuplet 3/2 {   a'8    b'8    
cis''8  }   \bar "|"   d''8.    e''16    d''8.    b'16    a'8.    b'16    d''8. 
   e''16    \bar "|"   fis''8.    e''16    d''8.    b'16    a'8.    fis'16    
d'8.    e'16    \bar "|"   fis'8.    e'16    \tuplet 3/2 {   e'8    e'8    dis'8 
 }   e'8.    g'16    fis'8.    e'16    \bar "|"     d'4    d'8.    fis'16    
a'4    a'8.    b'16    \bar "|"   d''4    d''8.    b'16    a'8.    b'16    
d''8.    e''16    \bar "|"   fis''8.    e''16    d''8.    b'16    a'16    
fis'8.    e'8.    g'16    \bar "|"   fis'4    d'4    d'4    }     
\repeat volta 2 {   cis''8.    d''16    \bar "|"   e''8.    cis''16    a'8.    
gis'16    a'4    \tuplet 3/2 {   cis''8    d''8    e''8  }   \bar "|"   fis''8.  
  b'16    b'8.    ais'16    b'4    d''8.    e''16    \bar "|"   fis''8.    
d''16    \tuplet 3/2 {   e''8    d''8    cis''8  }   d''8.    b'16    
\tuplet 3/2 {   a'8    g'8    fis'8  }   \bar "|"   fis'8.    e'16    e'8.    
dis'16    \tuplet 3/2 {   e'8    fis'8    g'8  }   fis'8.    e'16    \bar "|"    
 d'8.    e'16    fis'8.    g'16    a'8.    fis'16    \tuplet 3/2 {   a'8    b'8  
  cis''8  }   \bar "|"   d''8.    e''16    \tuplet 3/2 {   d''8    cis''8    b'8 
 }   a'8.    b'16    d''8.    e''16    \bar "|"   fis''8.    e''16    d''8.    
b'16    a'4    \tuplet 3/2 {   e'8    fis'8    g'8  }   \bar "|"   fis'8.    
d'16    \tuplet 3/2 {   d'8    d'8    d'8  }   d'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
