\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Gra5ity"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1196#setting1196"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1196#setting1196" {"https://thesession.org/tunes/1196#setting1196"}}}
	title = "Sir Roger De Coverly"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   g'8    fis'8    g'8    g'4    
e''8    d''8    b'8    g'8  \bar "|"   e'4    a'8    a'8    b'8    a'8    fis'8 
   e'8    d'8  \bar "|"   g'8    fis'8    g'8    b'8    g'8    b'8    d''8    
e''8    fis''8  \bar "|"   g''8    d''8    b'8    g'8    a'8    g'8    fis'8    
e'8    d'8  }     \repeat volta 2 {   d''8    b'8    d''8    e''8    c''8    
e''8    d''8    b'8    g'8  \bar "|"   e'4    a'8    a'8    b'8    a'8    fis'8 
   e'8    d'8  \bar "|"   d''8    b'8    d''8    e''8    c''8    e''8    d''8   
 e''8    fis''8  \bar "|"   g''8    d''8    b'8    g'8    a'8    g'8    fis'8   
 e'8    d'8  }     \repeat volta 2 {   d''8    c''8    b'8    c''8    b'8    
a'8    b'8    a'8    g'8  \bar "|"   e'4    a'8    a'4    b'8    c''8    b'8    
a'8  \bar "|"   d''8    c''8    b'8    c''8    b'8    a'8    b'8    a'8    g'8  
\bar "|"   d'4    g'8    g'4    a'8    b'8    a'8    g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
