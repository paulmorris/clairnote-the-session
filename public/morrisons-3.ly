\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/71#setting27472"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/71#setting27472" {"https://thesession.org/tunes/71#setting27472"}}}
	title = "Morrison's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   \repeat volta 2 {   e'4 ^"Em"   e'8    b'4    b'8  
\bar "|"   e'4    e'8      a'8 ^"D"   fis'8    d'8  \bar "|"   e'4 ^"Em"   e'8  
  b'4    b'8  \bar "|"   d''8    b'8    a'8      fis'8 ^"D"   e'8    d'8  
\bar "|"   e'4 ^"Em"   e'8    b'4    b'8  \bar "|"   e'4    e'8      a'8 ^"D"   
fis'8    d'8  \bar "|"   g'4 ^"Em"   g'8    fis'4    a'8  \bar "|"   d''8 ^"D"  
 a'8    g'8    fis'8    e'8    d'8  }       b'4 ^"Em"   e''8    fis''4    e''8  
\bar "|"   a''4    e''8    fis''8    e''8    d''8  \bar "|"   b'4    e''8    
fis''4    fis''8  \bar "|"   fis''8 ^"G"   a''8    g''8      fis''8 ^"D"   e''8 
   d''8  \bar "|"   b'4 ^"Em"   e''8    fis''4    e''8  \bar "|"   a''4    e''8 
   fis''8    e''8    d''8  \bar "|"   g''8 ^"G"   fis''8    e''8      d''4 ^"D" 
  a'8  \bar "|"   b'8 ^"C"   a'8    g'8      fis'8 ^"D"   g'8    a'8  \bar "|"  
     b'4 ^"Em"   e''8    fis''4    e''8  \bar "|"   a''4    e''8    fis''8    
e''8    d''8  \bar "|"   b'4    e''8    fis''4    fis''8  \bar "|"   fis''8 
^"D"   a''8    fis''8    d''8    e''8    fis''8  \bar "|"   g''4 ^"G"   g''8    
fis''4    e''8  \bar "|"   d''8 ^"D"   e''8    fis''8      g''4 ^"G"   d''8  
\bar "|"   e''8 ^"Bm"   d''8    cis''8    d''4    a'8  \bar "|"   b'8 ^"C"   
a'8    g'8      fis'8 ^"D"   e'8    d'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
