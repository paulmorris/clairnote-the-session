\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/102#setting102"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/102#setting102" {"https://thesession.org/tunes/102#setting102"}}}
	title = "Colonel McBain"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    e'8    
\tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    e'8  \bar "|" \tuplet 3/2 {   
fis'8    e'8    d'8  }   a'8    d'8    \tuplet 3/2 {   fis'8    e'8    d'8  }   
a'8    d'8  \bar "|" \tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    e'8    
\tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    e'8  \bar "|"   a'8    g'8    
fis'8    g'8    e'4    e'8    fis'8  \bar "|"     \tuplet 3/2 {   g'8    fis'8   
 e'8  }   b'8    e'8    \tuplet 3/2 {   g'8    fis'8    e'8  }   b'8    e'8  
\bar "|"   a'8    b'8    a'8    g'8    fis'8    d'8    d'4  \bar "|"   g'4    
g'8    fis'8    g'8    b'8    d''8    b'8  \bar "|"   a'8    g'8    fis'8    
g'8    e'4    e'8    fis'8  \bar "|"     g'4    g'8    fis'8    g'8    b'8    
d''8    b'8  \bar "|"   a'8    b'8    a'8    g'8    fis'8    a'8    a'4  
\bar "|" \tuplet 3/2 {   b'8    c''8    d''8  }   e''8    d''8    e''8    fis''8 
   g''8    e''8  \bar "|"   fis''8    e''8    d''8    fis''8    e''4    fis''4  
\bar "|"     e''8    fis''8    e''8    d''8    b'4    d''4  \bar "|"   a'8    
b'8    a'8    g'8    fis'8    d'8    d'4  \bar "|"   g'4    g'8    fis'8    g'8 
   b'8    d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8    e'4    e'8    
fis'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
