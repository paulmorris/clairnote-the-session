\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "G.Ryckeboer"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/370#setting29604"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/370#setting29604" {"https://thesession.org/tunes/370#setting29604"}}}
	title = "Jenny's Welcome To Charlie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \mixolydian   d'4.    a'8    a'8    g'8    e'8    fis'8  
\bar "|"   g'8    e'8    e'16    e'16    e'8    c''8    e'8    d''4  \bar "|"   
d'4.    a'8    a'8    g'8    e'8    g'8  \bar "|"   a'16 -.   b'16 -.   a'8    
g'8    a'8    e'8    a'8    g'8    e'8  \bar "|"     d'16    d'16    d'8    d'8 
   a'8    a'8    g'8    e'8    fis'8  \bar "|"   g'16    fis'16    e'8    c''8  
  e'8    d''8    e'8    c''8    e'8  \bar "|"   d'4    d'8    a'8    a'8    g'8 
   e'8    g'8  \bar "|"   a'16 -.   b'16 -.   a'8 -.   g'8    a'8    e'8    a'8 
   d'4  }     d''4.    a'8    g''16 -.   fis''16 -.   a'8    d''8    a'8  
\bar "|"   c''4.    a'8    e''8    a'8    c''8    a'8  \bar "|"   d''4.    a'8  
  g''16 -.   fis''16 -.   a'8    d''8    a'8  \bar "|"   e''8    a''8    a''8   
 g''8    e''16 -.   fis''16 -.   e''8 -.   d''8    a'8  \bar "|"     ees''8    
a'8    d''8    a'8    ees''!8    a'8    d''8    a'8  \bar "|"   c''4.    a'8    
e''8    a'8    c''8    a'8  \bar "|"   g'4    b'16 -.   cis''16 -.   d''8    
e''8    d''8    cis''8    a'8  \bar "|"   g'8    e'8    c''8    e'8    e'8    
d'8    d'4  \bar "||"     fis''8    a''4. ^"~"    a''8    g''8    e''8    
fis''8  \bar "|"   g''4    g''8    a''8    g''8    e''8    c''4  \bar "|"   
e''8    a''4. ^"~"    a''8    g''8    e''8    fis''8  \bar "|"   g''16    
fis''16    e''8    a''8    e''8    e''8    d''8    d''8    fis''8  \bar "|"     
a''4    a''8    b''8    a''8    fis''8    d''8    fis''8  \bar "|"   g''4    
g''8    a''8    g''8    e''8    c''8    a'8  \bar "|"   g'4    b'16 -.   
cis''16 -.   d''8    e''8    d''8    cis''8    a'8  \bar "|"   g'8    e'8    
c''8    e'8    e'8    d'8    d'4  \bar "||"     d''4    g''16 -.   fis''16 -.   
e''8 -.   d''8    cis''8    a'8    b'8  \bar "|"   c''8    a'8    d''8    b'8   
 c''8    a'8    g'4  \bar "|"   a'8    d''8    d''8    cis''8    a'8    d''8    
d''8    fis''8  \bar "|"   e''8    a''8    a''8    g''8    e''16 -.   fis''16 
-.   e''8    d''8    a'8  \bar "|"     d''4.    e''8    d''8    cis''8    a'8   
 b'8  \bar "|"   c''4.    d''8    c''8    a'8    g'8    e'8  \bar "|"   d'4    
fis'16 -.   g'16 -.   a'8    a'8    d''8    d''8    c''8  \bar "|"   a'8    a'8 
   g'8    a'8    e'8    a'8    g'8    e'8  \bar "|"   d'2    r2 \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
