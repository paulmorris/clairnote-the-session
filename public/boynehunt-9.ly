\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jordan Bairn"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/142#setting29097"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/142#setting29097" {"https://thesession.org/tunes/142#setting29097"}}}
	title = "Boyne Hunt, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   b'8 ^\upbow( \bar "|" \grace {    d''8  }   b'8    
a'8  -)   fis'8    a'8    d'4    fis'16 ^\downbow   g'16    a'8 ( \bar "|"   
d'8  -)   a'8 (   fis'8  -)   a'8 (   b'8    e'8  -)   e'8    d''8  \bar "|"   
\grace {    d''8 ^\upbow( }   b'8    a'8  -)   fis'8    a'8 (   d'8    e'8    
fis'8  -)   b'8 ( \bar "|"   a'8    b'8    d''8  -)   e''8 (   fis''8    e''8  
-)   d''8    b'8 ( \bar "|"     \grace {    d''8  }   b'8    a'8  -)   fis'8    
a'8    <<   d'4 (   a'4   >> fis'8  -)   a'8 ( \bar "|"   d''8    a'8  -)   
fis'8    a'8 (   b'8    e'8  -)   e'8    d''8  \bar "|"   b'16    b'16    a'8 ( 
  fis'8  -)   a'8 (   d'8    e'8  -)   fis'8    b'8 ( \bar "|"   a'8    b'8  -) 
  d''8    e''8 (   fis''8    d''8  -)   d''8    e''8  \bar "|"     \bar "|"   
fis''8 ^\downbow   a''8 ( \grace {    b''8  }   a''8    fis''8    a''8  -)   
b''8 (   fis''8  -)   a''8  \bar "|"   g''4. ^\downbow^"~"    a''8 (   b''8    
g''8  -)   e''8    a''8  \bar "|"   fis''8 ^\downbow     a''4. (^"~"    b''8  
-)   fis''8 (   a''8  -)   fis''8  \bar "|"   e''8 ^\downbow   a'8 (   d''8  -) 
  e''8 (   fis''8    d''8  -)   d''8    e''8  \bar "|"     fis''8 ^\upbow   
a''8 ( \grace {    b''8  }   a''8    fis''8  -)     a''4. (^"~"    fis''8  -) 
\bar "|"   g''4. ^\downbow^"~"    a''8 (   b''8    g''8  -)   e''8    a''8  
\bar "|"   e''8 ^\downbow(   fis''8  -)   a''8    fis''8    e''8      b'4. 
(^"~"  \bar "|"   a'8 ^\downbow -)   b'8 (   d''8  -)   e''8 ( \grace {    a''8 
 }   fis''8    e''8  -)   d''8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
