\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/593#setting25897"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/593#setting25897" {"https://thesession.org/tunes/593#setting25897"}}}
	title = "Rocky Road To Dublin, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \dorian   \repeat volta 2 {   e''4.    d''4    b'8    a'4.  
\bar "|"   e'4    a'8    a'4    g'8    b'8    c''8    d''8  \bar "|"   e''4.    
d''4    b'8    a'4    c''8  \bar "|"   b'4    g'8    g'4    a'8    b'8    c''8  
  d''8  \bar "|"     e''4.    d''4    b'8    a'4.  \bar "|"   e'4    a'8    a'4 
   g'8    b'8    c''8    d''8  \bar "|"   e''4.    d''4    b'8    a'4    c''8  
\bar "|"   b'4    g'8    g'4    a'8    b'8    c''8    d''8  \bar "||"     e''4  
  a''8    a''4    fis''8    g''4.  \bar "|"   e''4    a''8    a''4    a'8    
b'8    c''8    d''8  \bar "|"   e''4    a''8    a''4    fis''8    g''4    e''8  
\bar "|"   d''4    b'8    g'4    a'8    b'8    c''8    d''8  \bar "|"     e''4  
  a''8    a''4    fis''8    g''4.  \bar "|"   e''4    a''8    a''4    a'8    
b'8    c''8    d''8  \bar "|"   e''4    a''8    a''4    fis''8    g''4    e''8  
\bar "|"   d''4    b'8    g'4    a'8    b'8    c''8    d''8  \bar "||"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
