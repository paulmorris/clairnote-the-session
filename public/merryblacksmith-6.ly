\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "BenH"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/72#setting25590"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/72#setting25590" {"https://thesession.org/tunes/72#setting25590"}}}
	title = "Merry Blacksmith, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   a'8    \bar "|"     d''4. ^"D"   a'8    b'8    a'8    
fis'8    a'8    \bar "|"   a'8    b'8    d''8    a'8    b'8    a'8    fis'8    
a'8    \bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8    e''8    d''8  
  \bar "|"     b'8 ^"A"   e''8    e''8    d''8    e''8    g''8    fis''8    
e''8    \bar "|"       d''4. ^"D"   a'8    b'8    a'8    fis'8    a'8    
\bar "|"   a'8    b'8    d''8    a'8    b'8    a'8    fis'8    a'8    \bar "|"  
   a'8 ^"G"   b'8    d''8    e''8    fis''8    d''8    e''8    cis''8    
\bar "|"     d''8 ^"A"   a'8    a'8    g'8      fis'8 ^"D"   a'8    d'8  }     
\repeat volta 2 {   fis''8    \bar "|"     a''4 ^"D"   a''8    g''8    fis''8   
 a''8    fis''8    e''8    \bar "|"   d''8    e''8    d''8    a'8    b'8    a'8 
   fis'8    a'8    \bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8    
e''8    d''8    \bar "|"     b'8 ^"A"   e''8    e''8    d''8    e''4    fis''8  
  g''8    \bar "|"       a''8 ^"D"   b''8    a''8    g''8    fis''8    g''8    
fis''8    e''8    \bar "|"   d''8    e''8    d''8    a'8    b'8    a'8    fis'8 
   a'8    \bar "|"     a'8 ^"G"   b'8    d''8    e''8    fis''8    d''8    e''8 
   cis''8    \bar "|"     d''8 ^"A"   a'8    a'8    g'8      fis'8 ^"D"   a'8   
 d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
