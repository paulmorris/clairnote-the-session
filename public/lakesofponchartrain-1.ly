\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bdickler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/896#setting896"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/896#setting896" {"https://thesession.org/tunes/896#setting896"}}}
	title = "Lakes Of Ponchartrain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \major   \repeat volta 2 {   e'4  \bar "|"   a'2    fis''4  
\bar "|"   e''2    fis''8    e''8  \bar "|"   cis''4    b'4    e''4  \bar "|"   
cis''2    b'4  \bar "|"     a'8    fis'4.    gis'4  \bar "|"   a'2    a'4  
\bar "|"   a'2. (   \bar "|"   a'2  -)   e''4  \bar "|"     e''2    cis''4  
\bar "|"   e''4    fis''4    gis''4  \bar "|"   a''2    a''4  \bar "|"   gis''4 
   fis''4    e''4  \bar "|"     cis''2    b'4  \bar "|"   cis''4    d''4    
e''4  \bar "|"   fis''2. (   \bar "|"   fis''2  -)   fis''4  \bar "|"     e''2  
  cis''4  \bar "|"   e''4    fis''4    gis''4  \bar "|"   a''2    a''4  
\bar "|"   gis''4    fis''4    e''4  \bar "|"     cis''2    b'4  \bar "|"   
cis''4    d''4    e''4  \bar "|"   fis''2. (   \bar "|"   fis''2  -)   a'4  
\bar "|"     a'2    fis''4  \bar "|"   e''2    fis''8    e''8  \bar "|"   
cis''4    b'4    e''4  \bar "|"   cis''2    b'4  \bar "|"     a'8    fis'4.    
gis'4  \bar "|"   a'2    a'4  \bar "|"   a'2. (   \bar "|"   a'2  -)   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
