\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Kane"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/694#setting694"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/694#setting694" {"https://thesession.org/tunes/694#setting694"}}}
	title = "Off With You!"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   d''8    e'8    b'8    e'8    
e''8    e'8    b'8    e'8    \bar "|"   d''8    e'8    b'8    e'8    fis'8    
d'8    d'4 ^"~"    \bar "|"   d''8    e'8    b'8    e'8    g'8    b'8    e''8   
 b'8    } \alternative{{   cis''8    a'8    d''8    a'8    fis'8    d'8    d'8  
  cis''8    } {   cis''8    a'8    d''8    a'8    fis'8    d'8    d'4 ^"~"    
\bar "||"     g''8    e''8    b''8    e''8    g''8    e''8    e''8    fis''8    
\bar "|"   g''8    e''8    b''8    e''8    fis''8    d''8    d''8    fis''8    
\bar "|"   g''8    e''8    b''8    e''8    g''8    e''8    b''8    e''8    
\bar "|"   cis''8    a'8    d''8    a'8    fis'8    d'8    d'8    fis''8    
\bar "|"     g''8    e''8    b''8    e''8    g''8    b''8    b''4 ^"~"    
\bar "|"   g''8    e''8    b''8    e''8    fis''8    d''8    d''8    fis''8    
\bar "|"   e''8    b''8    g''8    b''8    fis''8    b''8    e''8    g''8    
\bar "|"   fis''8    cis''8    d''8    a'8    fis'8    d'8    d'8    cis''8    
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
