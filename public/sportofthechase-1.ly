\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mark Cordova"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/854#setting854"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/854#setting854" {"https://thesession.org/tunes/854#setting854"}}}
	title = "Sport Of The Chase, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key a \major   a'4    a'8    cis''4    a'8    cis''8    e''8    
cis''8  \bar "|"   a'4    a'8    cis''4    a'8    cis''8    e''8    cis''8  
\bar "|"   d'4    d'8    fis'4    d'8    fis'8    a'8    fis'8  \bar "|"   e'4  
  e'8    gis'4    e'8    gis'8    b'8    gis'8  }     a'4    a''8    a''4    
e''8    cis''8    e''8    cis''8  \bar "|"   a'4    a''8    a''4    e''8    
cis''8    e''8    cis''8  \bar "|"   d''4    d''8    fis''4    d''8    fis''8   
 a''8    fis''8  \bar "|"   e''4    e''8    gis''4    e''8    gis''8    b''8    
gis''8  \bar "|"     a''8    e''8    cis''8    a''8    e''8    cis''8    a''8   
 e''8    cis''8  \bar "|"   a''8    e''8    cis''8    a''8    e''8    cis''8    
a''8    e''8    cis''8  \bar "|"   fis''8    d''8    b'8    fis''8    d''8    
b'8    fis''8    d''8    b'8  \bar "|"   gis''8    e''8    b'8    gis''8    
e''8    b'8    gis''8    e''8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
