\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/991#setting27861"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/991#setting27861" {"https://thesession.org/tunes/991#setting27861"}}}
	title = "Lord Inchiquin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/4 \key d \major   a'4  \bar "|"   d''4    d''8    e''8    fis''8    
e''8  \bar "|"   d''4    e''8    d''8    cis''8    b'8  \bar "|"   a'4    fis'4 
   a'4  \bar "|"   fis''2    e''4  \bar "|"     d''4    e''8    d''8    cis''8  
  b'8  \bar "|"   a'4    g'4    fis'4  \bar "|"   g'4    b'8    a'8    g'8    
fis'8  \bar "|"   e'2    a''8    g''8  \bar "|"     fis''4.    e''8    d''4  
\bar "|"   d''4    cis''4    b'4  \bar "|"   a'4    fis'4    a'4  \bar "|"   
fis''2    e''4  \bar "|"     d''4    e''8    d''8    cis''8    b'8  \bar "|"   
a'8    d''4.    fis'4  \bar "|"   e'2    d'4  \bar "|"   d'2  }     
\repeat volta 2 {   a'4  \bar "|"   a'8    b'8    cis''8    d''8    e''4  
\bar "|"   e''4    d''4    e''4  \bar "|"   fis''4    d''4    fis''4  \bar "|"  
 e''2    d''4  \bar "|"     d''8    e''8    fis''8    g''8    a''4  \bar "|"   
a''4    b''4    g''4  \bar "|"   fis''4.    d''8    fis''4  \bar "|"   e''2    
d''4  \bar "|"     d''8    e''8    fis''8    g''8    a''4  \bar "|"   a''4    
b''4    g''4  \bar "|"   fis''4.    g''8    e''4  \bar "|"   d''4    e''4    
fis''4  \bar "|"     g''8    fis''8    e''4    e''4  \bar "|"   e''4    fis''4  
  d''4  \bar "|"   cis''4    d''4    b'4  \bar "|"   a'4    a'4    a''8    g''8 
 \bar "|"     fis''4.    e''8    d''4  \bar "|"   d''4    cis''4    b'4  
\bar "|"   a'4    fis'4    a'4  \bar "|"   g''2    fis''8    e''8  \bar "|"     
d''4    e''8    d''8    cis''8    b'8  \bar "|"   a'8    d''4.    fis'4  
\bar "|"   e'2    d'4  \bar "|"   d'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
