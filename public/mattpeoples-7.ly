\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/691#setting27936"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/691#setting27936" {"https://thesession.org/tunes/691#setting27936"}}}
	title = "Matt Peoples'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   ais'8  \repeat volta 2 {   b'4    fis''8    b'8    
d''8    b'8    fis''8    b'8  \bar "|"   cis''4    a''8    cis''8    cis''8    
b'8    a'8    cis''8  \bar "|"   b'4    fis''8    b'8    d''8    b'8    fis''8  
  b'8  \bar "|"   cis''8    b'8    a'8    cis''8    b'4.    ais'8  \bar "|"     
b'4    fis''8    b'8    d''8    b'8    fis''8    b'8  \bar "|"   cis''4    a''8 
   cis''8    cis''8    b'8    a'8    cis''8  \bar "|"   b'4    fis''8    b'8    
d''8    b'8    fis''8    b'8  \bar "|"   cis''8    b'8    a'8    cis''8    b'4. 
 \bar "||"     ais''8  \bar "|"   b''8    fis''8    fis''4    b''8    a''8    
fis''8    g''8  \bar "|"   a''8    cis''8    cis''4    a''4.    ais''8  
\bar "|"   b''8    fis''8    fis''4    b''8    a''8    fis''8    g''8  \bar "|" 
\tuplet 3/2 {   a''8    g''8    fis''8  }   e''8    cis''8    cis''8    b'8    
b'8    ais''8  \bar "|"     b''8    fis''8    fis''4    b''8    a''8    fis''8  
  g''8  \bar "|"   a''8    cis''8    cis''4    a''4.    ais''8  \bar "|"   b''8 
   g''8    a''8    fis''8    g''8    e''8    fis''8    g''8  \bar "|" 
\tuplet 3/2 {   a''8    g''8    fis''8  }   e''8    cis''8    cis''8    b'8    
a'8    cis''8  \bar "||"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
