\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1131#setting28087"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1131#setting28087" {"https://thesession.org/tunes/1131#setting28087"}}}
	title = "Richard Dwyer's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \dorian   \repeat volta 2 {   e'4    b'8    e'8    g'8    a'8  
  b'8    d''8    \bar "|"   a'8    g'8    fis'8    d'8    e'8    b'8    b'4  
\bar "|"   b'8    e'8    g'8    e'8    fis'8    a'8    d''8    cis''8    
\bar "|"   b'8    e''8    e''8    d''8    e''4.    d''8    \bar "|"     
\bar "|"   b'4.    a'8    g'8    e'8    e'4    \bar "|"   fis'8    d''8    a'8  
  g'8    fis'8    d'8    d'4    \bar "|"   e'8    b'8    g'8    b'8    a'8    
b'8    g'8    a'8  \bar "|"   b'8    g'8    e'8    d'8    b'8    e'8    e'4  }  
   \bar "|"   |
 e''4    b'8    e''8    fis''8    e''8    e''4  \bar "|"   d''4    a'8    d''8  
  fis''8    d''8    d''4    \bar "|"   e''8    d''8    b'8    a'8    g'8    a'8 
   b'8    a'8    \bar "|"   g'8    e'8    fis'8    d'8    b'8    e'8    e'4    
\bar "|"     \bar "|"   e''4    b'8    e''8    fis''8    e''8    e''4    
\bar "|"   d''4.    b'8    a'8    fis'8    d'8    fis'8    \bar "|"   e'4    
\tuplet 3/2 {   fis'8    g'8    a'8  }   b'8    e''8    e''8    cis''8    
\bar "|"   d''8    b'8    a'8    fis'8    b'8    e'8    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
