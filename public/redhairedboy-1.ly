\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dave McGrath"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/566#setting566"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/566#setting566" {"https://thesession.org/tunes/566#setting566"}}}
	title = "Red Haired Boy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   \repeat volta 2 {   e'8 ^"A"   a'8    a'8    g'8 
   a'8    b'8    cis''8    d''8  \bar "|"   e''8    fis''8    e''8    cis''8    
  d''4 ^"D"   cis''8    d''8  \bar "|"   e''8 ^"A"   a'8    a'8    a'8    a'8   
 b'8    cis''8    a'8  \bar "|"   b'8 ^"G"   g'!8    e'8    fis'8      g'4 ^"G" 
  fis'8    g'8  \bar "|"       e'8 ^"A"   a'8    a'8    g'8    a'8    b'8    
cis''8    d''8  \bar "|"   e''8    fis''8    e''8    cis''8      d''4 ^"D"   
cis''8    d''8  \bar "|"   e''8 ^"A"   a''8    a''8    a''8    a''8    fis''8   
 e''8    d''8  \bar "|"   cis''8    a'8    b'8 ^"E"   g'8      a'2 ^"A"   }     
\repeat volta 2 {     g''4 ^"G"   g''8    a''8 ^"G"   g''8    fis''8    e''8    
fis''8  \bar "|"   g''8    fis''8    e''8    cis''8      d''4 ^"D"   cis''8    
d''8  \bar "|"   e''8 ^"A"   a'8    a'8    a'8    a'8    b'8    cis''8    a'8  
\bar "|"   b'8 ^"G"   g'!8    e'8    fis'8    g'4    fis'8    g'8  \bar "|"     
  e'8 ^"A"   a'8    a'8    g'8    a'8    b'8    cis''8    d''8  \bar "|"   e''8 
   fis''8    e''8    cis''8      d''4 ^"D"   cis''8    d''8  \bar "|"   e''8 
^"A"   a''8    a''8    a''8    a''8    fis''8    e''8    d''8  \bar "|"   
cis''8    a'8    b'8 ^"E"   g'8      a'2 ^"A"   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
