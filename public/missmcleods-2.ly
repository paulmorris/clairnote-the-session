\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/75#setting12552"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/75#setting12552" {"https://thesession.org/tunes/75#setting12552"}}}
	title = "Miss McLeod's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   a'4.    cis''8    e''4    cis''8  
  e''8  \bar "|"   fis''8    e''8    cis''8    fis''8    e''8    cis''8    b'8  
  cis''8  \bar "|"   a'4.    cis''8    e''4    cis''8    b'8  \bar "|"   a'8    
fis'8    a'8    fis'8    e'8    a'8    cis''8    b'8  \bar "|"   a'4.    cis''8 
   e''4    cis''8    e''8  \bar "|"   fis''8    e''8    cis''8    b'8    a'8    
b'8    cis''8    e''8  \bar "|"   fis''8    e''8    d''8    cis''8    d''8    
e''8    fis''8    a''8  \bar "|"   a''8    fis''8    e''8    cis''8    b'8    
a'8    cis''8    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
