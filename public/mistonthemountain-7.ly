\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dogbox"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/470#setting13357"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/470#setting13357" {"https://thesession.org/tunes/470#setting13357"}}}
	title = "Mist On The Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key b \minor   b'4.    b'4.    \bar "|"   fis''4    fis''8    
fis''8.    e''16    cis''8    \bar "|"   a'4.    a'4.  \bar "|"   cis''8.    
b'16    cis''8    b'8.    a'16    b'8  \bar "|"   d''4.    e''4.  \bar "|"   
fis''8.    g''16    a''8    cis''8.    b'16    a'8  \bar "|"   b'8.    cis''16  
  fis''8    e''8.    d''16    cis''8  \bar "|"   b'4.    b'4.  \bar "||"   
fis''4.    fis''4.  \bar "|"   e''8.    fis''16    a''8    fis''8.    e''16    
cis''8  \bar "|"   a'4    a'8    e''4    cis''8  \bar "|"   fis''4    fis''8    
e''8.    d''16    cis''8  \bar "|"   b'4.    d''4.  \bar "|"   e''8.    fis''16 
   a''8    cis''8.    b'16    a'8  \bar "|"   b'8.    cis''16    fis''8    
e''8.    d''16    cis''8  \bar "|"   b'4.    b'4.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
