\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/563#setting25156"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/563#setting25156" {"https://thesession.org/tunes/563#setting25156"}}}
	title = "Longford Collector, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'16 ^"G"   g'16    g'8    g'8    
a'8    b'8    c''8    d''8    g''8  \bar "|"   e''8 ^"Em"   b'8    b'16    b'16 
   b'8      e''8 ^"D"   b'8    d''8    b'8  \bar "|"   g'16 ^"G"   g'16    g'8  
  g'8    a'8      b'4 ^"Em"   d''8    b'8  \bar "|"   a'8 ^"Am"   c''8    b'8   
 a'8      g'8 ^"D"   e'8    d'8    e'8  \bar "|"       g'16 ^"G"   g'16    g'8  
  g'8    a'8    b'8    c''8    d''8    g''8  \bar "|"   e''8 ^"Em"   b'8    
b'16    b'16    b'8      d''4 ^"D"   e''8    fis''8  \bar "|"   g''16 ^"G"   
g''16    g''8    a''8    fis''8    g''8    e''8    d''8    b'8  \bar "|"   a'8 
^"Am"   c''8    b'8    a'8      g'8 ^"D"   e'8    d'8    e'8    }     
\repeat volta 2 {   g''16 ^"G"   g''16    g''8    g''8    a''8    g''8    e''8  
  d''8    g''8  \bar "|"   e''8 ^"Em"   b'8    b'16    b'16    b'8      d''4 
^"D"   e''8    fis''8  \bar "|"   g''16 ^"G"   g''16    g''8    a''8    fis''8  
  g''8    e''8    d''8    b'8  \bar "|"   a'8 ^"Am"   c''8    b'8    a'8      
g'8 ^"D"   e'8    d'8    e'8  \bar "|"       g''16 ^"G"   g''16    g''8    g''8 
   a''8    g''8    e''8    d''8    g''8  \bar "|"   e''8 ^"Em"   b'8    b'16    
b'16    b'8      d''4 ^"D"   e''8    fis''8  \bar "|"   g''8 ^"G"   b''8    
a''8    fis''8    g''8    e''8    d''8    b'8  \bar "|"   a'8 ^"Am"   c''8    
b'8    a'8      g'8 ^"D"   e'8    d'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
