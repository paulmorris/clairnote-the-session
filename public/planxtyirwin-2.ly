\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/790#setting13930"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/790#setting13930" {"https://thesession.org/tunes/790#setting13930"}}}
	title = "Planxty Irwin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   a'8  \bar "|"   d''4    c''8    b'8.    c''16    d''8 
 \bar "|"   a'4    g'8    fis'8.    e'16    d'8  \bar "|"   g'4    e'8    
fis'8.    g'16    a'8  \bar "|"   c'4 ^"~"    d'8    e'4    c''8  \bar "|"   
d''4    c''8    b'8.    c''16    d''8  \bar "|"   a'4    g'8    fis'8.    e'16  
  d'8  \bar "|"   g'4    e'8    fis'8.    g'16    a'8  \bar "|"   d'4    c'8    
d'4  }   \repeat volta 2 {   a'8  \bar "|"   d''8.    e''16    d''8    d''4    
d''8  \bar "|"   e''4    e''8    e''8    c''8    a'8  \bar "|" <<   d'4    a'4  
  fis''4   >> fis''8    e''4    d''8  \bar "|"   d''8    c''8    b'8    a'8    
b'8    c''8  \bar "|"   d''4    c''8    b'8.    c''16    d''8  \bar "|"   a'4   
 g'8    fis'8.    e'16    d'8  \bar "|"   g'4    e'8    fis'8.    g'16    a'8  
\bar "|"   d'4    c'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
