\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/304#setting23076"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/304#setting23076" {"https://thesession.org/tunes/304#setting23076"}}}
	title = "Whistler Of Rosslea, The — The Whistler Of Rosslea"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \dorian \time 4/4 \key g \dorian   g'8    a'8    bes'8    c''8 
   d''4    c''8    a'8  \bar "|"   d''8    a'8    c''8    a'8    bes'8    g'8   
 g'4 ^"~"  \bar "|"   f'8    g'8    a'8    bes'8    c''4 ^"~"    bes'8    c''8  
\bar "|"   d''8    ees''8    c''8    d''8    bes'8    c''8    a'8    bes'8  
\bar "|"     g'8    a'8    bes'8    g'8    a'8    bes'8    c''8    a'8  
\bar "|"   d''8    e''8    f''8    d''8    g''8    f''8    d''8    c''8  
\bar "|"   bes'8    c''8    c''16    c''16    bes'8    c''8    ees''8    a'8    
c''8  \bar "|"   bes'8    g'8    a'8    f'8    g'2  }     \repeat volta 2 {   
g''4    d''8    g''8    bes'8    g''8    d''8    g''8  \bar "|"   g''4 ^"~"    
a''8    g''8    g''8    f''8    d''8    e''8  \bar "|"   f''4 ^"~"    c''8    
f''8    a'8    f''8    c''8    f''8  \bar "|"   f''8    e''8    d''8    c''8    
bes'8    g'8    f'8    a'8  \bar "|"     g'8    g''8    g''8    f''8    g''8    
f''8    d''8    c''8  \bar "|"   bes'8    c''8    d''8    e''8    f''4.    g''8 
 \bar "|"   g''8    f''8    d''8    c''8    bes'8    c''8    c''16    c''16    
bes'8  \bar "|"   c''8    f'8    a'8    f'8    g'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
