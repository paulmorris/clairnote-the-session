\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "spindizzy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/590#setting13593"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/590#setting13593" {"https://thesession.org/tunes/590#setting13593"}}}
	title = "Cherish The Ladies"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   d''4. ^"~"    fis''8    e''8    
d''8  \bar "|"   a''8    fis''8    d''8    fis''8    e''8    d''8  \bar "|"   
b'4. ^"~"    g''4. ^"~"  \bar "|"   e''8    cis''8    e''8    g''8    fis''8    
e''8  } \alternative{{   d''8    cis''8    d''8    fis''8    e''8    d''8  
\bar "|"   a''8    fis''8    d''8    g''8    b''8    g''8  \bar "|"   fis''8    
d''8    b'8    a'8    g'8    fis'8  \bar "|"   g'8    e'8    fis'8    g'8    
fis'8    e'8  } {   a''4. ^"~"    b''8    g''8    b''8  \bar "|"   a''8    
fis''8    a''8    g''4    e''8  \bar "|"   fis''8    d''8    b'8    a'8    g'8  
  fis'8  \bar "|"   g'8    e'8    fis'8    g'8    fis'8    e'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
