\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Aidan Crossey"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1422#setting1422"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1422#setting1422" {"https://thesession.org/tunes/1422#setting1422"}}}
	title = "Glassdrummond"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   e''8.    cis''16    a'8    g''8   
 fis''8    d''8    a'8    g'8  \bar "|"   fis'8    a'8    d'8.    d'16    d'4   
 a8    b8  \bar "|"   cis'8    d'8    e'8    fis'8    g'8    b'8    a'8    
fis'8  \bar "|"   g'8    fis'8    e'8    d'8    e'4    r4 \bar "|"     e''8.    
cis''16    a'8    g''8    fis''8    d''8    a'8    fis'8  \bar "|"   g'8    b'8 
   e''8    fis''8    g''8    e''8    a''8    g''8  \bar "|"   fis''8    d''8    
a'8    fis'8    e''8    cis''8    a''8    fis''8  \bar "|"   d''4    d''8.    
e''16    d''4    r4 }     \repeat volta 2 {   |
 a''8    fis''8    d'8    a''16    fis''16    g''8    e''8    a'8    g''16    
e''16  \bar "|"   fis''8    d''8    b'8    cis''16    d''16    a'8    g'8    
fis'8    e'8  \bar "|"   d'8    a8    d'8    fis'8    a'8    d''8    fis''8    
a''8  \bar "|"   g''8    fis''8    e''8    d''8    e''4    r4 \bar "|"     a''8 
   fis''8    d'8    a''16    fis''16    g''8    e''8    a'8    g''16    e''16  
\bar "|"   fis''8    d''8    cis''8    d''8    b''8    a''8    g''8    fis''8  
\bar "|"   g''8    e''8    a'8    a''16    fis''16    g''8    e''8    a''8    
fis''8  \bar "|"   d''4    d''8.    e''16    d''4    r4 }     \repeat volta 2 { 
  |
 fis'8    a'8    fis''8    a'16    fis'16    g'8    b'8    g''8    a'16    g'16 
 \bar "|"   fis'8    a'8    d''8    fis''8    b''8    a''8    g''8    fis''8  
\bar "|"   e''8    a'8    a''8    fis''8    g''8    fis''8    e''8    d''8  
\bar "|"   cis''4    e''4    e''4    r8 a''16    fis''16  \bar "|"     e''8.    
cis''16    a'8    g''8    fis''8    d''8    a'8    fis'8  \bar "|"   g'8    b'8 
   e''8    fis''8    g''8    e''8    a''8    g''8  \bar "|"   fis''8    d''8    
a'8    fis''8    e''8    cis''8    a''8    fis''8  \bar "|"   d''4    d''8.    
e''16    d''4    r4 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
