\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/317#setting24514"
	arranger = "Setting 13"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/317#setting24514" {"https://thesession.org/tunes/317#setting24514"}}}
	title = "Paddy Whack"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   g'8 ^\upbow   \bar "|"   g'8    
b'8    d''8    g''8.    fis''16    g''8    \bar "|"   e''8    d''8    c''8    
b'8    a'8    g'8    \bar "|"   g'8    b'8    d''8    e''8    fis''8    g''8    
\bar "|"   fis''8    d''8    d''8    d''8    e''8    fis''8    \bar "|"     
g''8    b''8    g''8    a''8    fis''8    d''8    \bar "|"   c''8    d''8    
e''8    d''8    b'8    g'8    \bar "|"   g'8    a'8    b'8    c''8    a'8    
d''8    \bar "|"   b'8    g'8    g'8    g'4    }     \repeat volta 2 {   d''8 
^\upbow   \bar "|"   b'8    g'8    b'8    c''4 ^\upbow   e''8 ^\upbow   
\bar "|"   d''8    b'8    g'8    a'4 ^\upbow   g'8 ^\upbow   \bar "|"   g'8    
b'8    d''8    e''8    fis''8    g''8    \bar "|"   fis''8    d''8    d''8    
d''8    e''8    fis''8    \bar "|"     g''8    b''8    g''8    a''8    fis''8   
 d''8    \bar "|"   c''8    d''8    e''8    d''8    b'8    g'8    \bar "|"   
g'8    a'8    b'8    c''8    a'8    d''8    \bar "|"   b'8    g'8    g'8    g'4 
   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
