\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bleedin' Heart"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/516#setting13447"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/516#setting13447" {"https://thesession.org/tunes/516#setting13447"}}}
	title = "Limerick Lasses, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d''8    b'8    a'8    fis'8    
d'8    e'8    fis'8    d'8    \bar "|"   g'4. ^"~"    a'8    b'8    e'8    e'4 
^"~"    \bar "|"   d''8    b'8    a'8    fis'8    d'8    e'8    fis'8    d'8    
\bar "|"   g'8    b'8    a'8    g'8    fis'8    d'8    d'4    }   
\repeat volta 2 {   d''8    cis''8    d''8    e''8    fis''4. ^"~"    d''8    
\bar "|"   e''8    fis''8    e''8    d''8    cis''8    a'8    a'4 ^"~"    
\bar "|"   b'16    cis''16    d''8    e''8    fis''8    g''4    a''8    g''8  
\bar "|"   fis''8    a''8    e''8    a''8    fis''8    d''8    d''8    a'8  }   
\repeat volta 2 {   b'8    d''8    a'8    d''8    b'8    d''8    a'8    d''8    
\bar "|"   b'8    d''8    e''8    fis''8    g''8    fis''8    e''8    d''8  
\bar "|"   b'8    d''8    a'8    d''8    b'8    d''8    a''8    g''8  \bar "|"  
 fis''8    a''8    e''8    a''8    fis''8    d''8    d''4 ^"~"  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
