\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1441#setting14823"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1441#setting14823" {"https://thesession.org/tunes/1441#setting14823"}}}
	title = "Dawning Of The Day, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d'8    e'8    \bar "|"   fis'4    fis'4    fis'4    
e'8    fis'8    \bar "|"   a'4    a'4    b'4    a'8    fis'8    \bar "|"   d'4  
  e'4    d'4    d'4    \bar "|"   d'2.    a'4    \bar "|"     b'4.    a'8    
b'4    d''4    \bar "|"   fis'4.    e'8    d'4    fis'4    \bar "|"   a'4    
fis'4    d''4    fis'4    \bar "|"   e'2.    a'4    \bar "|"     b'4.    a'8    
b'4    d''4    \bar "|"   fis'4.    e'8    d'4    fis'4    \bar "|"   a'4    
fis'4    d''4    fis'4    \bar "|"   e'2.    d'8    e'8    \bar "|"     fis'4   
 fis'4    fis'4    e'8    fis'8    \bar "|"   a'4    a'4    b'4    a'8    fis'8 
   \bar "|"   d'4    fis'4    e'4    d'4    \bar "|"   d'2.    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
