\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/761#setting13872"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/761#setting13872" {"https://thesession.org/tunes/761#setting13872"}}}
	title = "Dowd's No. 9"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   d'8    fis'8    a'8    fis'8    g'8    
fis'8    e'8    fis'8  \bar "|"   d'8    e'8    fis'8    a'8    d''8    a'8    
b'8    g'8  \bar "|"   fis'8    g'8    a'8    fis'8    g'8    fis'8    g'8    
b'8  \bar "|"   a'8    g'8    fis'16    e'16    d'8    a8    cis'8    e'8    
cis'8  \bar "|"   \bar "|"   d'8    fis'8    a'8    fis'8    g'8    fis'8    
e'8    fis'8  \bar "|"   d'4.    cis''8    d''8    a'8    b'8    g'8  \bar "|"  
 fis'8    g'8    a'8    fis'8    g'8    fis'8    g'8    b'8  \bar "|"   a'8    
g'8    fis'16    e'16    d'8    a8    cis'8    e'8    cis'8  \bar "|"   
\bar "|"   d'8    fis'4. ^"~"    g'8    fis'8    e'8    fis'8  \bar "|"   d'4.  
  cis''8    d''8    a'8    b'8    g'8  \bar "|"   fis'8    g'8    a'8    fis'8  
  g'8    fis'8    g'8    b'8  \bar "|"   a'8    g'8    fis'16    e'16    d'8    
a8    cis'8    e'8    cis'8  \bar "|"   \bar "|"   d'8    fis'8    a'8    fis'8 
   g'8    fis'8    e'8    fis'8  \bar "|"   d'8    e'8    fis'8    a'8    d''8  
  a'8    b'8    g'8  \bar "|"   fis'8    g'8    a'8    fis'8    g'8    fis'8    
g'8    b'8  \bar "|"   a'8    g'8    fis'8    d'8    e'8    fis'8    d'4    
\bar "||"   \bar "|"   fis''8    d''8    d''16    d''16    d''8    g''4    a''8 
   g''8  \bar "|"   fis''8    d''4. ^"~"    cis''8    d''8    e''8    g''8  
\bar "|"   fis''8    d''8    a'8    d''8    g''8    fis''8    e''8    cis''8  
\bar "|"   d''8    cis''8    d''8    e''8    fis''8    d''8    d''4    \bar "|" 
  \bar "|"   fis''8    g''8    a''8    fis''8    g''8    b''8    e''8    g''8  
\bar "|"   fis''8    e''8    d''8    fis''8    e''8    cis''8    a'8    fis'8  
\bar "|"   g'4    b'16    a'16    g'8    fis'8    a'8    d''8    b'8  \bar "|"  
 a'8    g'8    fis'8    d'8    e'8    fis'8    d'4    \bar "|"   \bar "|"   
fis''8    d''8    d''16    d''16    d''8    g''4    a''8    g''8  \bar "|"   
fis''8    d''4. ^"~"    cis''8    d''8    e''8    g''8  \bar "|"   fis''8    
d''8    a'8    d''8    g''8    fis''8    e''8    cis''8  \bar "|"   d''8    
cis''8    d''8    e''8    fis''8    d''8    d''4    \bar "|"   \bar "|"   
fis''16    fis''16    g''8    a''8    fis''8    g''8    b''8    e''8    g''8  
\bar "|"   fis''8    e''8    d''8    fis''8    e''8    cis''8    a'8    fis'8  
\bar "|"   g'4    b'16    a'16    g'8    fis'8    a'8    d''8    b'8  \bar "|"  
 a'8    g'8    fis'8    d'8    a8    cis'8    e'8    cis'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
