\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "TimBuk2"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/69#setting12525"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/69#setting12525" {"https://thesession.org/tunes/69#setting12525"}}}
	title = "Morning Dew, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   \repeat volta 2 {   d'4.    a'8    r8 g'8    f'8    
e'8    \bar "|"   d'8    e'8    f'8    a'8    g'8    e'8    f'8    c'8    
\bar "|"   d'4.    a'8    r8 g'8    f'8    g'8    \bar "|"   a'8    bes'8    
c''8    a'8    g'8    e'8    f'8    c'8    }   g'8    aes'8    d''8    c''8    
e''8    g''8    d''8    c''16    ees''16    \bar "|"     r8 d''8    r8 c''8    
a'8    g'8    f'8    g'8    \bar "|"   a'8    c''8    d''8    f''8    d''8    
e''8    e''4    \bar "|"   c''4.    a'8    g'8    e'8    f'8    g'8    \bar "|" 
  g'8    aes'8    d''8    c''8    e''8    g''8    d''8    c''16    ees''16    
\bar "|"     r8 d''8    d''8    c''8    r8 g'8    f'8    g'8    \bar "|"   g'64 
   a'8    c''8    d''8    f''8    e''4.    d''8    \bar "|"   r4. a'8    g'8    
e'8    f'8    g'8    \bar "|"   g'32    g'16.    f'8    g'8    f'8    d'8    
e'8    f'8    g'8    \bar "|"   a'4    f'8    a'8    g'8    a'8    b'32    
a'16.    f'8    \bar "|"   g'8    f'8    g'8    f'8    d'8    e'8    f'8    g'8 
   \bar "|"   a'8    f'8    c''8    a'8  <<   g'4    g'4   >>   f'8    g'8    
\bar "|"   g'32    g'16.    f'8    g'8    f'8    d'8    e'8    f'8    g'16    
bes'16    \bar "|"   r4   f'8    a'8    g'8    a'8    a'32    g'16.    f'8    
\bar "|"   g'64    g'8    f'8    g'8    f'8    d'8    e'8    f'8    g'16    
bes'16    \bar "|"   r8 f'8    c''8    a'8  <<   g'4    g'4   >>   f'8    g'8   
 \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
