\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1376#setting14734"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1376#setting14734" {"https://thesession.org/tunes/1376#setting14734"}}}
	title = "If We Hadn't Any Women In The World"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   b'8    c''8  }   d''4.    b'8    g'8    a'8    b'8    
g'8  \bar "|"   e'4    e'4    d'2  \bar "|"   b'16    b'16    c''8    d''8    
b'8    a'8    g'8    a'8    b'8  \bar "|"   a'4.    b'8    a'4    b'8    c''8  
\bar "|"   d''8    e''8    d''8    b'8    g'8    a'8    b'8    g'8  \bar "|"   
e'4    e'4    d'2  \bar "|"   b'16    b'16    c''8    d''8    b'8    a'8    g'8 
   a'8    b'8  \bar "|"   g'4    g'8    fis'8    g'4    b'8    c''8  \bar ":|." 
  g'4    g'8    fis'8    g'4    b'8    d''8  \bar "||"   e''4.    fis''8    
g''4    fis''8    g''8  \bar "|"   e''8    d''8    d''8    cis''8    d''4    
b'8    d''8  \bar "|"   e''8    d''8    b'8    d''8    g''8    d''8    b'8    
g'8  \bar "|" \tuplet 3/2 {   a'8    b'8    a'8  }   gis'8    b'8    a'4    b'8  
  d''8  \bar "|"   e''4.    fis''8    g''4    fis''8    g''8  \bar "|"   e''8   
 d''8    d''8    cis''8    d''4    b'8    d''8  \bar "|"   e''8    d''8    b'16 
   c''16    d''8    g''8    d''8    b'8    g'8  \bar "|" \tuplet 3/2 {   a'8    
b'8    a'8  }   g'8    fis'8    g'4    b'8    d''8  \bar "|"   e''4.    fis''8  
  g''4    fis''8    g''8  \bar "|"   e''8    d''8    d''8    cis''8    d''4    
b'8    d''8  \bar "|"   e''8    d''8    b'8    d''8    g''8    d''8    b'8    
g'8  \bar "|" \tuplet 3/2 {   a'8    b'8    a'8  }   gis'8    b'8    a'4    b'8  
  c''8  \bar "|"   d''4.    b'8    g'8    a'8    b'8    g'8  \bar "|"   e'4    
e'4    d'2  \bar "|"   b'16    b'16    c''8    d''8    b'8    a'8    g'8    a'8 
   b'8  \bar "|"   g'4    g'8    fis'8    g'4    b'8    c''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
