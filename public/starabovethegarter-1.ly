\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/1398#setting1398"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1398#setting1398" {"https://thesession.org/tunes/1398#setting1398"}}}
	title = "Star Above The Garter, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key g \major   d''4    b'8    b'8    a'8    g'8    a'4. ^"~"    a'8 
   b'8    a'8  \bar "|"   g'4    e'8    c''4    b'8    b'8    a'8    g'8    a'8 
   b'8    c''8  \bar "|"     d''4    b'8    b'8    a'8    g'8    a'4. ^"~"    
a'8    b'8    a'8  \bar "|"   g'4    e'8    c''4    e'8    d'4.    d'4.  }     
\repeat volta 2 {   d''4    e''8    fis''4    a''8    g''4    e''8    d''4    
b'8  \bar "|"   g'4    b'8    c''4    b'8    b'8    a'8    g'8    a'8    b'8    
c''8  \bar "|"     d''4    e''8    fis''4    a''8    g''4    e''8    d''4    
b'8  \bar "|"   g'4    b'8    c''4    e'8    d'4.    d'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
