\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Edgar Bolton"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/580#setting13571"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/580#setting13571" {"https://thesession.org/tunes/580#setting13571"}}}
	title = "Woodcock, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {     d''8 ^"G"   b'8    g'8      
c''8 ^"C"   g'8    e'8    \bar "|"     d'8 ^"G"   b'8    d'8    g'4. ^"~"    
\bar "|"     b'8 ^"G"   a'8    g'8    d''8    c''8    b'8    \bar "|"     d''8 
^"G"   c''8    b'8      a'8 ^"D"   b'8    c''8    \bar "|"     d''8 ^"G"   b'8  
  g'8      c''8 ^"C"   g'8    e'8    \bar "|"     d'8 ^"G"   b'8    d'8    g'4. 
^"~"    \bar "|"     b'8 ^"G"   a'8    g'8      d''8 ^"C"   e''8    d''8    
\bar "|"     a'8 ^"D"   b'8    a'8      g'4 ^"G"   d''8  }   \bar "|"     d''8 
^"G"   b'8    g'8      e''8 ^"C"   c''8    a'8    \bar "|"   fis''4. ^"D"^"~"   
   g''4 ^"G"   d''8    \bar "|"     e''16 ^"C"   fis''16    g''8    e''8      
d''8 ^"G"   b'8    g'8    \bar "|"   d''8    c''8    b'8      a'8 ^"D"   b'8    
c''8    \bar "|"   \bar "|"     d''8 ^"G"   b'8    g'8      e''8 ^"C"   c''8    
a'8    \bar "|"   fis''4. ^"D"^"~"      g''4 ^"G"   d''8    \bar "|"   e''16 
^"C"   fis''16    g''8    e''8      d''8 ^"G"   b'8    g'8    \bar "|"     a'8 
^"D"   b'8    a'8      g'4 ^"G"   d''8    \bar "|"   \bar ":|."     d''8 ^"G"   
b'8    g'8      c''8 ^"C"   g'8    e'8    \bar "|"     d'8 ^"G"   b'8    d'8    
g'4. ^"~"    \bar "|"     b'8 ^"G"   a'8    g'8      d''8 ^"C"   e''8    d''8   
 \bar "|"     a'8 ^"D"   b'8    a'8      g'4. ^"G"   \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
