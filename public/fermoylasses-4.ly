\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Manu Novo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/219#setting12902"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/219#setting12902" {"https://thesession.org/tunes/219#setting12902"}}}
	title = "Fermoy Lasses, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \bar "|"   g'8    e'8    e'4 ^"~"    b'8    e'8    
e'4 ^"~"  \bar "|"   g'8    e'8    e'4 ^"~"    b'8    c''8    b'8    a'8  
\bar "|"   g'8    e'8    e'4 ^"~"    b'8    e'8    e'4 ^"~"  \bar "|"   a'8    
fis'8    d'8    fis'8    a'8    c''8    b'8    a'8  \bar "|"   \bar "|"   g'8   
 e'8    e'4 ^"~"    b'8    e'8    e'4 ^"~"  \bar "|"   g'8    e'8    e'4 ^"~"   
 b'8    c''8    b'8    a'8  \bar "|"   g'4. ^"~"    fis'8    g'8    b'8    d''8 
   b'8  \bar "|"   a'8    fis'8    d'8    fis'8    a'8    c''8    b'8    a'8  
\bar "|"   \bar "|"   g'4    b'8    g'8    d''8    g'8    b'8    g'8  \bar "|"  
 g'4 ^"~"    b'8    d''8    e''8    fis''8    g''4  \bar "|"   g'4    b'8    
g'8    d''8    g'8    b'8    g'8  \bar "|"   a'8    fis'8    d'8    fis'8    
a'8    c''8    b'8    a'8  \bar "|"   \bar "|"   g'4    b'8    g'8    d''8    
g'8    b'8    g'8  \bar "|"   g'4 ^"~"    b'8    d''8    e''8    fis''8    g''8 
   fis''8  \bar "|"   g''8    a''8    g''8    e''8    d''8    e''8    d''8    
b'8  \bar "|"   a'8    fis'8    d'8    fis'8    a'8    c''8    b'8    a'8  
\bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
