\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/771#setting24743"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/771#setting24743" {"https://thesession.org/tunes/771#setting24743"}}}
	title = "Carraroe, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   d'8    e'8    d'8    fis'4    a'8 
 \bar "|"   d''8    fis''8    e''8    d''4    a'8  \bar "|"   b'16    cis''16   
 d''8    b'8    a'8    d''8    fis''8  \bar "|"   a''8    fis''8    d''8    
e''4    fis''8  \bar "|"     d'8    e'8    d'8    fis'4    a'8  \bar "|"   d''8 
   fis''8    e''8    d''4    a'8  \bar "|"   b'16    cis''16    d''8    b'8    
a'8    fis'8    a'8  \bar "|"   d''8    a'8    fis'8    e'4    d'8  }     
\repeat volta 2 {   |
 d''8    fis''8    a''8    a''8    fis''8    a''8  \bar "|"   b''8    g''8    
e''8    e''8    d''8    b'8  \bar "|"   d''8    fis''8    a''8    a''8    
fis''8    a''8  \bar "|"   b''8    g''8    e''8    e''8    fis''8    e''8  
\bar "|"     d''8    fis''8    a''8    a''8    fis''8    a''8  \bar "|"   b''8  
  g''8    e''8    e''8    d''8    b'8  \bar "|"   d''4    b'8    a'8    fis'8   
 a'8  } \alternative{{   d''8    a'8    fis'8    e'4    d'8  } {   d''8    a'8  
  fis'8    e'4    fis'8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
