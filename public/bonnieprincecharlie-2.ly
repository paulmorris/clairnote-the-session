\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/892#setting21307"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/892#setting21307" {"https://thesession.org/tunes/892#setting21307"}}}
	title = "Bonnie Prince Charlie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   fis'4    \tuplet 3/2 {   fis'8    e'8    d'8  } 
\bar "|"   d'8    fis'8    a'4  \bar "|"   a'8.    b'16    d''4  \bar "|"   
fis'4    \tuplet 3/2 {   fis'8    e'8    d'8  } \bar "|"   fis'4    e'8.    d'16 
 \bar "|"   e'4    fis'8    a'8  \bar "|"   b'8    a'8    d''8    cis''8  
\bar "|"   b'8    a'8    fis'4  \bar "|"     a'4    \tuplet 3/2 {   b'8    
cis''8    d''8  } \bar "|"   e''4    d''8    e''8  \bar "|"   fis''4    e''8    
d''8  \bar "|"   d''8    a'8    b'8    d''16    b'16  \bar "|"   a'4    fis'8   
 e'8  \bar "|"   d'8    d'8    d''8.    e''16  \bar "|"   fis''8    d''8    
a''8    fis''8  \bar "|"   e''4    d''4  \bar "||"     d''8.    a'16    a'4 -. 
\bar "|"   e''8.    a'16    a'4 -. \bar "|"   fis''4    e''8    d''8  \bar "|"  
 d''8    a'8    b'8    d''16    b'16  \bar "|"   a'4    fis'8    e'8  \bar "|"  
 d'8    d'8    d''8.    e''16  \bar "|"   fis''8    d''8    a''8    fis''8  
\bar "|"   e''4    d''4  \bar "|"     d''8.    a'16    a'4 -. \bar "|"   e''8.  
  fis''16    g''4  \bar "|"   fis''4    e''8    d''8  \bar "|"   d''8    a'8    
b'8    d''16    b'16  \bar "|"   a'4    fis'8    e'8  \bar "|"   d'8    d'8    
d''8.    e''16  \bar "|"   fis''8    d''8    a''8    fis''8  \bar "|"   e''4    
d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
