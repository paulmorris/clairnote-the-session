\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/815#setting22470"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/815#setting22470" {"https://thesession.org/tunes/815#setting22470"}}}
	title = "Rosewood"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   \repeat volta 2 {   e''8    \bar "|"   a''8    gis''8 
   fis''8    e''8    fis''8    gis''8    \bar "|"   a''8    e''8    d''8    
cis''8    b'8    a'8    \bar "|"   a''8    gis''8    a''8    cis''8    d''8    
e''8    \bar "|"   fis''8    b'8    b'8    b'4    e''8    \bar "|"     a''8    
gis''8    fis''8    e''8    fis''8    gis''8    \bar "|"   a''8    e''8    d''8 
   cis''8    b'8    a'8    \bar "|"   d''8    e''8    fis''8    b'8    e''8    
d''8    \bar "|"   cis''8    a'8    a'8    a'4    }     \repeat volta 2 {   
d''8    \bar "|"   cis''8    d''8    e''8    a'8    cis''8    e''8    \bar "|"  
 fis''8    d''8    d''8    d''8    e''8    fis''8    \bar "|"   dis''8    e''8  
  fis''8    b'8    dis''8    fis''8    \bar "|"   gis''8    e''8    e''8    
e''8    fis''8    gis''8    \bar "|"     a''8    gis''8    fis''8    e''8    
d''8    cis''8    \bar "|"   fis''8    e''8    d''8    cis''8    b'8    a'8    
\bar "|"   d''8    e''8    fis''8    b'8    e''8    d''8    \bar "|"   cis''8   
 a'8    a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
