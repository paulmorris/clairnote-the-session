\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "arkle"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/79#setting22307"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/79#setting22307" {"https://thesession.org/tunes/79#setting22307"}}}
	title = "Paddy Ryan's Dream"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   c''8    b'8  \repeat volta 2 {   a'8    e'4.    c''4  
  b'8    g'8  \bar "|"   e'8    b4.    g8    a8    b8    g8  \bar "|"   a8    
e'4.    c'8    d'8    e'8    g'8  \bar "|"   c''8    a'8    b'8    g'8    a'8   
 b'8    c''8    b'8  \bar "|"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
