\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/750#setting13851"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/750#setting13851" {"https://thesession.org/tunes/750#setting13851"}}}
	title = "A Fig For A Kiss"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \dorian   \repeat volta 2 {   g'4    b'8    e'4    b'8    b'8  
  a'8    g'8    \bar "|"   fis'4    a'8    d'4    a'8    a'8    g'8    fis'8    
\bar "|"   g'4    b'8    e'4    b'8    b'8    a'8    g'8    \bar "|"   b'8    
d''8    b'8    a'8    g'8    fis'8    e'4.    }   e''4    d''8    e''4    
fis''8    a''8    fis''8    d''8    \bar "|"   e''4    fis''8    g''4    a''8   
 b''8    a''8    g''8    \bar "|"   e''4    d''8    e''4    fis''8    g''8    
fis''8    e''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    fis'8    
e'4.    \bar "|"   e''4    fis''8    g''4    a''8    b''8    a''8    g''8    
\bar "|"   fis''4    d''8    d''4    fis''8    a''8    g''8    fis''8    
\bar "|"   b''8    a''8    g''8    a''8    g''8    fis''8    g''8    fis''8    
e''8    \bar "|"   d''8    cis''8    b'8    a'8    g'8    fis'8    e'4.    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
