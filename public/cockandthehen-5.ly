\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/93#setting12644"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/93#setting12644" {"https://thesession.org/tunes/93#setting12644"}}}
	title = "Cock And The Hen, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key b \minor   \repeat volta 2 {   b'8    cis''8    b'8    b'8    
a'8    b'8    g''4    a'8    \bar "|"   b'8    cis''8    b'8    b'8    a'8    
b'8    d''8    b'8    a'8    \bar "|"   b'8    cis''8    b'8    b'8    a'8    
b'8    g''4    e''8    \bar "|"   fis''8    e''8    d''8    e''8    d''8    b'8 
   d''8    b'8    a'8    }   a''8    fis''8    d''8    d''8    e''8    fis''8   
 g''4    e''8    \bar "|"   a''8    fis''8    d''8    d''8    e''8    fis''8    
g''8    fis''8    e''8    \bar "|"   a''8    fis''8    d''8    d''8    e''8    
fis''8    g''4    e''8    \bar "|"   fis''8    e''8    d''8    e''8    d''8    
b'8    d''8    b'8    a'8    \bar "|"   a''8    fis''8    d''8    d''8    e''8  
  fis''8    g''4    e''8    \bar "|"   a''8    fis''8    d''8    d''8    e''8   
 fis''8    g''8    fis''8    e''8    \bar "|"   a''4    a''8    a''8    g''8    
fis''8    g''4    e''8    \bar "|"   fis''8    e''8    d''8    e''8    d''8    
b'8    d''8    b'8    a'8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
