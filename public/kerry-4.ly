\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/39#setting12454"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/39#setting12454" {"https://thesession.org/tunes/39#setting12454"}}}
	title = "Kerry, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g'8.    a'16    \bar "|"   b'8    
d'8    e'8    d'8    \bar "|"   b'8    d'8    e'16    fis'16    e'16    d'16    
\bar "|"   g'4    a'8.    b'16    \bar "|"   a'8    g'8    e'8    d'8    
\bar "|"     b'8    d'8    e'8    d'8    \bar "|"   b'16    c''16    b'16    
d'16    e'8    d'16    e'16    \bar "|"   g'4    a'8.    b'16    \bar "|"   a'8 
   g'8    }     \repeat volta 2 {   g'8.    a'16    \bar "|"   b'8    d''8    
b'8    d''16    b'16    \bar "|"   a'8    g'8    e'8    d'8    \bar "|"   g'4   
 a'8.    b'16    \bar "|"   a'8    g'8    e'8    g'16    a'16    \bar "|"     
b'8    d''8    b'8    c''16    b'16    \bar "|"   a'8    g'8    e'4    \bar "|" 
  g'8    d'16    g'16    a'8    b'8    \bar "|"   a'8    g'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
