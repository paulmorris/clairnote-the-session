\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/950#setting14146"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/950#setting14146" {"https://thesession.org/tunes/950#setting14146"}}}
	title = "Fat Cat, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g4    b8    d'8    e'8    d'8    b'8    a'8  \bar "|" 
  g'8    d'8    e'8    g'8    e'8    d'8    b8    d'8  \bar "|"   b'8    a'8    
a'8    g'8    e'4    d'8    b8  \bar "|"   a4    g8    a8    b8    d'8    d'8   
 b8  \bar "|"   g4    b8    d'8    e'8    d'8    b'8    a'8  \bar "|"   g'8    
d'8    e'8    g'8    e'8    d'8    b8    d'8  \bar "|"   b'8    a'8    a'8    
g'8    e'4    d'8    b8  \bar "|"   a8    g8    b8    a8    g4.    a8  }   |
 b4    d'8    b8    g8    b8    d'8    g'8  \bar "|"   b'4    a'8    b'8    g'8 
   e'8    d'4  \bar "|"   g''2. ^"meaow"   e''8    d''8  \bar "|"   b'4    a'8  
  g'8    e'4    g'8    e'8  \bar "|"   d'8    g8    b8    d'8    d'8    e'8    
e'8    g'8  \bar "|"   b'4    a'8    b'8    g'8    e'8    d'4  \bar "|"   g''2. 
^"meaow"   a'8    g'8  \bar "|"   e'4    d'8    e'8    g'2  \bar ":|."   e'4    
d'8    b8    g2  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
