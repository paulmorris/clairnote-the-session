\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "injun bob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1016#setting1016"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1016#setting1016" {"https://thesession.org/tunes/1016#setting1016"}}}
	title = "Josefin's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major     d'8 ^"G"   g'8    a'8  \bar "|"   b'8    d''8    
c''8  \bar "|"   b'8    a'8    g'8  \bar "|"   d'4    e'8  \bar "|"   c'4 ^"C"  
 c'16    c'16  \bar "|"   e'8    g'8    fis'8  \bar "|"   e'4.  \bar "|"   d'4. 
^"D" \bar "|"       d'8 ^"G"   g'8    a'8  \bar "|"   b'8    d''8    c''8  
\bar "|"   b'8    a'8    g'8  \bar "|"   d'4    e'8  \bar "|"   c'4 ^"C"   c'16 
   c'16  \bar "|"   e'8    fis'8    g'8  \bar "|"     a'4. ^"D"( \bar "|"   a'8 
 -)   b'8    c''8  \bar "|"     \repeat volta 2 {   d''8 ^"G"   b'8    d''8  
\bar "|"   g''4    fis''8  \bar "|"   e''4. ^"C" \bar "|"   d''4. ^"G" \bar "|" 
  c''8 ^"C"   e''8    d''8  \bar "|"   c''8    b'8    a'8  \bar "|"   b'8. ^"G" 
  c''16    b'8  \bar "|"   a'8 ^"D"   b'8    c''8  }     \bar "|"   b'8 ^"Em"   
a'8    g'8  \bar "|"   fis'4 ^"D"   g'8  \bar "|"   g'4. ^"C" \bar "|"   c'8    
d'8    c'8  \bar "|"   b8 ^"G"   d'8    g'8  \bar "|"   fis'8 ^"D"   e'8    
fis'8  \bar "|"     g'4. ^"G"( \bar "|"   g'8  -) \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
