\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SebastianM"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/741#setting23430"
	arranger = "Setting 15"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/741#setting23430" {"https://thesession.org/tunes/741#setting23430"}}}
	title = "Paddy O'Rafferty"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   d''8    b'8    b'8    d''8    a'8    a'8    \bar "|"  
 d''8    b'8    b'8    a'8    g'8    a'8    \bar "|"   d''8    b'8    b'8    
a'4    d'8    \bar "|"   g'8    b'8    a'8    g'8    e'8    d'8    \bar "|"     
d''4    b'8    a'8    d'8    d'8    \bar "|"   g'8    a'8    b'8    c''8    
d''8    e''8    \bar "|"   d''8    b'8    g'8    a'8    g'8    e'8    \bar "|"  
 g'8    a'8    g'8    g'8    e'8    d'8    }     b'4.    a'4. ^"~"    \bar "|"  
 b'4    g'8    a'8    g'8    e'8    \bar "|"   b'8    g'8    g'8    a'8    g'8  
  e'8    \bar "|"   g'8    a'8    g'8    g'8    e'8    d'8    \bar "|"     b'8  
  d'8    d'8    a'4. ^"~"    \bar "|"   g'8    a'8    b'8    c''8    d''8    
e''8    \bar "|"   d''8    b'8    g'8    a'8    g'8    e'8    \bar "|"   g'8    
a'8    g'8    g'8    e'8    d'8    \bar "|"     g'8    b'8    d'8    g'8    b'8 
   d'8    \bar "|"   g'8    b'8    d''8    e''8    d''8    c''8    \bar "|"   
b'8    d''8    g'8    b'8    d''8    b'8    \bar "|"   g'8    a'8    g'8    g'8 
   e'8    d'8    \bar "|"     g'8    b'8    d'8    g'8    b'8    d'8    
\bar "|"   g'8    b'8    d''8    g''4. ^"~"    \bar "|"   e''8    d''8    b'8   
 a'4    b'8    \bar "|"   g'8    b'8    a'8    g'8    e'8    d'8    \bar "|"    
 \repeat volta 2 {   b'4    d''8    g''8    d''8    d''8    \bar "|"   e''8    
d''8    d''8    g''8    d''8    d''8    \bar "|"   b'8    d''8    d''8    e''8  
  d''8    b'8    \bar "|"   g'8    a'8    g'8    g'8    e'8    d'8    \bar "|"  
   b'4    d''8    g''8    d''8    d''8    \bar "|"   e''8    d''8    e''8    
g''8    fis''8    g''8    \bar "|"   e''8    d''8    b'8    a'4. ^"~"    
\bar "|"   g'8    b'8    a'8    g'8    e'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
