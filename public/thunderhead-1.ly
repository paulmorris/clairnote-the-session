\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "seara"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/732#setting732"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/732#setting732" {"https://thesession.org/tunes/732#setting732"}}}
	title = "Thunderhead"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key b \minor   \bar "|"   b'4. ^"~"    fis''8    b'8    b'8  
\bar "|"   e''8    b'8    b'8    d''8    b'8    a'8  \bar "|"   b'4. ^"~"    
fis''8    g''8    fis''8  \bar "|"   e''8    d''8    b'8    a'8    b'8    d''8  
\bar "|"     \bar "|"   b'4. ^"~"    fis''8    b'8    b'8  \bar "|"   e''8    
b'8    b'8    d''8    e''8    fis''8  \bar "|"   g''8    fis''8    e''8    
fis''4. ^"~"  \bar "|"   e''8    d''8    b'8    a'8    b'8    d''8  }     
\repeat volta 2 {   a''8    fis''8    d''8    e''8    d''8    b'8  \bar "|"   
a'8    b'8    d''8    cis''4    cis''8  \bar "|"   b'8    cis''8    cis''8    
b'8    cis''8    cis''8  \bar "|"     d''8    cis''8    b'8    cis''4. ^"~"  
\bar "|"   a''8    fis''8    d''8    e''8    d''8    b'8  \bar "|"   a'8    b'8 
   d''8    cis''4    cis''8  \bar "|"     \tuplet 3/2 {   b'8    cis''8    d''8  
}   b'8    cis''4    a''8  } \alternative{{   g''8    e''8    cis''8    d''8    
fis''8    g''8  } {   g''8    e''8    cis''8    d''8    cis''8    c''8  
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
