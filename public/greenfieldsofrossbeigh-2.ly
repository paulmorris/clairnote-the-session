\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "joe fidkid"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/322#setting13091"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/322#setting13091" {"https://thesession.org/tunes/322#setting13091"}}}
	title = "Green Fields Of Rossbeigh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   d''8    cis''8    \repeat volta 2 {   b'8    e'8    
e'4 ^"~"    b'8    a'8    fis'8    a'8    \bar "|"   fis'8    a'8    b'8    
d''8    fis''8    d''8    e''8    cis''8    \bar "|"   b'8    e'8    e'4 ^"~"   
 b'8    a'8    fis'8    a'8    \bar "|"   b'8    fis'8    a'8    fis'8    e'4   
 fis'8    a'8    }   \repeat volta 2 {   b'8    d''8    e''8    fis''8    g''8  
  fis''8    g''8    e''8    \bar "|"   d''8    b'8    b'4 ^"~"    d''8    b'8   
 a'8    c''8    \bar "|"   b'8    d''8    e''8    fis''8    g''8    fis''8    
e''8    b'8    \bar "|"   d''8    b'8    a'8    fis'8    e'4    e''8    fis''8  
  \bar "|"   g''8    fis''8    g''8    b''8    a''8    g''8    fis''8    e''8  
\bar "|"   fis''4 ^"~"    fis''8    e''8    fis''8    e''8    d''8    b'8    
\bar "|"   a'8    b'8    d''8    e''8    fis''8    g''8    fis''8    e''8    
} \alternative{{   d''8    b'8    a'8    fis'8    e'4    fis'8    a'8    } {   
d''8    b'8    a'8    fis'8    e'2    \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
