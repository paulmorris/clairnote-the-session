\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Sol Foster"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1090#setting24796"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1090#setting24796" {"https://thesession.org/tunes/1090#setting24796"}}}
	title = "Cahir's Kitchen"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   e'4    g'8    b'8    e'4    g'8    b'8  \bar "|"   
a'4    fis'8    a'8    d'8    e'8    \tuplet 3/2 {   fis'8    e'8    d'8  } 
\bar "|"   e'4    g'8    b'8    e'4    g'8    a'8  \bar "|"   b'8    bes'8    
bes'8    c''8    bes'8    a'8    g'8    fis'8  \bar "|"     e'4    g'8    b'8   
 e'4    g'8    b'8  \bar "|"   a'4    fis'8    a'8    d'8    e'8    
\tuplet 3/2 {   fis'8    e'8    d'8  } \bar "|" \tuplet 3/2 {   e'8    fis'8    
g'8  }   \tuplet 3/2 {   fis'8    g'8    a'8  }   \tuplet 3/2 {   g'8    a'8    
b'8  }   \tuplet 3/2 {   a'8    b'8    c''8  } \bar "|"   b'8    a'8    fis'8    
g'8    e'2  \bar "||"     e''8    b'8    g''8    fis''8    e''8    b'8    
fis''8    e''8  \bar "|"   d''8    a'8    fis'8    a'8    d'8    e'8    
\tuplet 3/2 {   fis'8    e'8    d'8  } \bar "|"   e''8    b'8    g''8    fis''8  
  e''8    b'8    b'8    a'8  \bar "|"   b'8    bes'8    bes'8    c''8    bes'8  
  a'8    g'8    fis'8  \bar "|"     e''8    b'8    g''8    fis''8    e''8    
b'8    fis''8    e''8  \bar "|"   d''8    a'8    fis'8    a'8    d'8    e'8    
\tuplet 3/2 {   fis'8    e'8    d'8  } \bar "|" \tuplet 3/2 {   e'8    fis'8    
g'8  }   \tuplet 3/2 {   fis'8    g'8    a'8  }   \tuplet 3/2 {   g'8    a'8    
b'8  }   \tuplet 3/2 {   a'8    b'8    c''8  } \bar "|"   b'8    a'8    fis'8    
g'8    e'2  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
