\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/697#setting13761"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/697#setting13761" {"https://thesession.org/tunes/697#setting13761"}}}
	title = "Peat-Fire Flame"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   a''8    c'''8    g''8    a''16    g''16  \bar "|"   
e''8    d''8    e''4  \bar "|"   e''2 ( \bar "|"   e''2  -) \bar "||"   a'8.    
b'16    c''8    b'8  \bar "|"   a'8    g'8    e''4  \bar "|"   e''8    d''8    
d''8    c''16    d''16  \bar "|"   e''8    d''8    d''8    e''8  \bar "|"   a'8 
   a'16    b'16    c''8    b'8  \bar "|"   a'8    g'8    e''8.    d''16  
\bar "|"   c''8    a'8    b'8    g'8  \bar "|"   a'4    a'4  \bar "||"   c''8.  
  d''16    e''8    c'''16    b''16  \bar "|"   a''8    g''4    e''8  \bar "|"   
e''8    d''8    d''8    c''16    d''16  \bar "|"   e''8    d''8    d''8    e''8 
 \bar "|"   c''8.    d''16    e''8    c'''16    b''16  \bar "|"   a''8    g''8  
  e''8.    d''16  \bar "|"   c''8    a'8    b'8    g'8  \bar "|"   a'4    a'4  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
