\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dancarney84"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/983#setting22440"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/983#setting22440" {"https://thesession.org/tunes/983#setting22440"}}}
	title = "Cricket's March Over The Salt Box, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   d'8    e'8    d'8    fis'8    e'8    d'8  \bar "|"   
fis'4    a'8    d''4    a'8  \bar "|" \grace {    a'8  }   b'8    cis''8    
d''8    a'4    fis'8  \bar "|"   e'4    d'8    b'4.  \bar "|"     d'8    e'8    
d'8    fis'8    e'8    d'8  \bar "|"   fis'4    a'8    d''4    a'8  \bar "|" 
\grace {    a'8  }   b'8    cis''8    d''8    a'8    g'8    fis'8  \bar "|"   
e'4  \grace {    fis'8    e'8  }   d'8    d'4.  \bar ":|."   e'4  \grace {    
fis'8    e'8  }   d'8    d'4    b'8  \bar "||"     a'4    fis'8    fis'8    e'8 
   fis'8  \bar "|"   d''4    b'8    b'8    a'8    b'8  \bar "|"   fis''4    b'8 
   b'8    a'8    b'8  \bar "|"   a''4    fis''8    fis''8    e''8    fis''8  
\bar "|"     g''8    fis''8    e''8    fis''8    e''8    d''8  \bar "|"   d''8  
  e''8    a'8    d''4    a'8  \bar "|" \grace {    a'8  }   b'8    cis''8    
d''8    a'4    fis'8  \bar "|"   e'4  \grace {    fis'8    e'8  }   d'8    d'4  
  b'8  \bar ":|."   e'4  \grace {    fis'8    e'8  }   d'8    d'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
