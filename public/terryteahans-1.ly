\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Joerg Froese"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/331#setting331"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/331#setting331" {"https://thesession.org/tunes/331#setting331"}}}
	title = "Terry Teahan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \major   \repeat volta 2 {   e'4    e'8    cis''8    \bar "|"  
 b'8    a'8    fis'8    a'8    \bar "|"   e'4    e'8    cis''8    \bar "|"   
b'8    a'8    a'4    \bar "|"     e'4    e'8    cis''8    \bar "|"   b'8    a'8 
   fis'8    a'8    \bar "|"   e'4    e''8    cis''8    } \alternative{{   b'8   
 a'8    a'4    } {   b'8    a'8    a'8.    b'16    \bar "||"     \bar "|"   
cis''8    e''8    b'8.    cis''16    \bar "|"   b'8    a'8    fis'8    a'8    
\bar "|"   cis''8    e''8    b'8.    cis''16    \bar "|"   b'8    a'8    a'8.   
 b'16    \bar "|"     cis''8    e''8    b'8.    cis''16    \bar "|"   b'8    
a'8    fis'8    a'8    \bar "|"   e'4    e'8.    cis''16    \bar "|"   b'8    
a'8    a'8.    b'16    \bar "|"     \bar "|"   cis''8    e''8    b'8.    
cis''16    \bar "|"   b'8    a'8    fis'8    a'8    \bar "|"   cis''8    e''8   
 b'8    cis''16    e''16    \bar "|"   fis''8    e''8    cis''8    b'8    
\bar "|"     cis''8    e''8    b'8.    cis''16    \bar "|"   b'8    a'8    
fis'8    a'8    \bar "|"   e'8    r8   e''8.    cis''16    \bar "|"   b'8    
a'8    a'4    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
