\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/31#setting12423"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/31#setting12423" {"https://thesession.org/tunes/31#setting12423"}}}
	title = "Franc A'Phoill"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \mixolydian   a'4    fis'8    b'4    fis'8  \bar "|"   a'4    
g'8    fis'8    e'8    d'8  \bar "|"   a'4    fis'8    b'4    fis'8  \bar "|"   
g'8    fis'8    g'8    g'8    fis'8    d'8  \bar "|"   a'4    fis'8    b'4    
fis'8  \bar "|"   a'4    g'8    fis'8    e'8    d'8  \bar "|"   g'8    a'8    
b'8    c''8    a'8    fis'8  \bar "|"   g'8    fis'8    g'8    a'8    fis'8    
d'8  }   \repeat volta 2 {   d''8    fis''8    a''8    a''8    fis''8    d''8  
\bar "|"   d''8    cis''8    d''8    e''8    cis''8    a'8  \bar "|"   g'8    
a'8    b'8    c''!8    a'8    fis'8  \bar "|"   g'8    fis'8    g'8    a'8    
fis'8    d'8  \bar "|"   d''8    fis''8    a''8    a''8    fis''8    a''8  
\bar "|"   g''8    e''8    g''8    fis''8    d''8    a'8  \bar "|"   g'8    a'8 
   b'8    c''8    a'8    fis'8  \bar "|"   g'8    fis'8    g'8    a'8    fis'8  
  d'8  }   \repeat volta 2 {   d''4    e''8    d''4    a'8  \bar "|"   d''4    
e''8    d''8    c''8    a'8  \bar "|"   d''4    e''8    d''8    c''8    a'8  
\bar "|"   g'8    fis'8    g'8    a'8    fis'8    d'8  \bar "|"   d''4    e''8  
  d''4    a'8  \bar "|"   d''4    e''8    d''8    c''8    a'8  \bar "|"   g'8   
 a'8    b'8    c''8    a'8    fis'8  \bar "|"   g'8    fis'8    g'8    a'8    
fis'8    d'8  }   \repeat volta 2 {   d''8    e''8    fis''8    d''8    e''8    
fis''8  \bar "|"   d''8    e''8    fis''8    e''8    cis''8    a'8  \bar "|"   
d''8    e''8    fis''8    d''8    e''8    fis''8  \bar "|"   g'8    fis'8    
g'8    a'8    fis'8    d'8  \bar "|"   d''8    e''8    fis''8    d''8    e''8   
 fis''8  \bar "|"   d''8    e''8    fis''8    e''8    cis''8    a'8  \bar "|"   
g'8    a'8    b'8    c''!8    a'8    fis'8  \bar "|"   g'8    fis'8    g'8    
a'8    fis'8    d'8  }   \repeat volta 2 {   d'8    d''8    d''8    d'8    c''8 
   c''8  \bar "|"   d'8    b'8    b'8    d'8    c''8    c''8  \bar "|"   d'8    
d''8    d''8    d'8    c''8    c''8  \bar "|"   g'8    fis'8    g'8    a'8    
fis'8    d'8  \bar "|"   d'8    d''8    d''8    d'8    c''8    c''8  \bar "|"   
d'8    b'8    b'8    d'8    a'8    a'8  \bar "|"   g'8    a'8    b'8    c''8    
a'8    fis'8  \bar "|"   g'8    fis'8    g'8    a'8    fis'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
