\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Kane"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/393#setting393"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/393#setting393" {"https://thesession.org/tunes/393#setting393"}}}
	title = "Lakes Of Sligo, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \minor   \repeat volta 2 {   f'8    a'8    a'8  \grace {    
bes'8  }   c''8    \bar "|"   d''4    d''8    c''8    \bar "|"   bes'8.    a'16 
   bes'8  \grace {    c''8  }   d''8    \bar "|"   e''4    e''8    f''8    
\bar "|"     f'8    a'8    a'8  \grace {    bes'8  }   c''8    \bar "|"   d''8  
  e''8    f''8.    e''16    \bar "|"   d''8    bes'8    a'8    f''8    \bar "|" 
  e''8  \grace {    f''8  }   e''8    d''4    }     \repeat volta 2 {   f''8    
a''8    d''8.    e''16    \bar "|"   f''8    a''8    a''8  \grace {    g''8  }  
 f''8    \bar "|"   g''8    bes''8    e''8    f''8    \bar "|"   g''8    bes''8 
   bes''8    a''8    \bar "|"     f''8    a''8    e''8  \grace {    f''8  }   
e''8    \bar "|"   d''8    e''8    f''8.    e''16    \bar "|"   d''8    bes''8  
  a''8    f''8    \bar "|"   e''8  \grace {    f''8  }   e''8    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
