\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gfisch"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/470#setting28312"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/470#setting28312" {"https://thesession.org/tunes/470#setting28312"}}}
	title = "Mist On The Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \minor     a'2. ^"Am" \bar "|"   a'2.  \bar "|"   e''4. ^"C"   
e''8    e''4  \bar "|"   e''4 ^"Em"   d''4    b'4  \bar "|"   g'2. ^"G" 
\bar "|"   g'2.  \bar "|"   b'4. ^"Bm"   a'8    b'4  \bar "|"   a'4. ^"Am"   
g'8    a'4  \bar "|"       c''2. ^"Am" \bar "|"   d''2. ^"G" \bar "|"   e''4. 
^"C"   fis''8    g''4  \bar "|"   b'4. ^"Em"   a'8    g'4  \bar "|"   a'4. 
^"Am"   b'8    e''4  \bar "|"   d''4 ^"G"   c''4    b'4  \bar "|"   a'2. ^"Am" 
\bar "|"   a'2.  \bar "||"       e''2. ^"Am" \bar "|"   e''2.  \bar "|"   d''4. 
^"G"   e''8    g''4  \bar "|"   e''4. ^"Am"   d''8    b'4  \bar "|"   g'2 ^"G"  
 g'4  \bar "|"   d''2 ^"Bm"   b'4  \bar "|"   e''2 ^"Am"   e''4  \bar "|"   
d''4. ^"Bm"   c''8    b'4  \bar "|"       a'2. ^"Am" \bar "|"   c''2.  \bar "|" 
  d''4. ^"G"   e''8    g''4  \bar "|"   b'4. ^"Em"   a'8    g'4  \bar "|"   
a'4. ^"Am"   b'8    e''4  \bar "|"   d''4 ^"G"   c''4    b'4  \bar "|"   a'2. 
^"Am" \bar "|"   a'2.  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
