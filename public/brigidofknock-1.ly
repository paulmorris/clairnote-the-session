\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Enob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/523#setting523"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/523#setting523" {"https://thesession.org/tunes/523#setting523"}}}
	title = "Brigid Of Knock"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   \tuplet 3/2 {   d'8    e'8    fis'8  }   \bar "|"   
g'8    fis'8    g'8    a'8    b'8    d''8    g''8    fis''8    \bar "|"   a''8  
  g''8    fis''8    c''8    e''8    d''8    cis''8    d''8    \bar "|"   b'8    
g'8    d'8    g'8    c''8    a'8    fis'8    g'8    \bar "|"   a'8    b'8    
c''8    b'8    a'8    g'8    \tuplet 3/2 {   fis'8    e'8    d'8  }   \bar "|"   
  \bar "|"   g8    b8    d'8    f'8    e'8    c'8    e'8    g'8    \bar "|"   
fis'8    d'8    fis'8    a'8    g'8    b'8    a'8    c''8    \bar "|"   b'8    
g''8    fis''8    d''8    c''8    a'8    fis'8    a'8    \bar "|"   g'4    g'8  
  fis'8    g'4    }     \repeat volta 2 {   fis''8    g''8    \bar "|"   a''8   
 fis''8    d''8    g''8    b''8    g''8    fis''8    a''8    \bar "|"   g''8    
fis''8    g''8    e''8    d''8    c''8    b'8    d''8    \bar "|"   c''8    
e''8    a'8    c''8    b'8    c''8    d''8    a'8    \bar "|"   fis'8    a'8    
c''8    a'8    fis'8    d'8    \tuplet 3/2 {   c'8    b8    a8  }   \bar "|"     
\bar "|"   g8    b8    d'8    f'8    e'8    c'8    e'8    g'8    \bar "|"   
fis'8    d'8    fis'8    a'8    g'8    b'8    a'8    c''8    \bar "|"   b'8    
g''8    fis''8    d''8    c''8    a'8    fis'8    a'8    \bar "|"   g'4    g'8  
  fis'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
