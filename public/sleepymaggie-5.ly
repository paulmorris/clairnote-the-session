\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/787#setting13920"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/787#setting13920" {"https://thesession.org/tunes/787#setting13920"}}}
	title = "Sleepy Maggie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   e''4    c''8    a'8    e''8    a'8    b'16    c''16  
  d''8  \bar "|"   e''4    c''8    a'8    d''8    g'8    b'16    c''16    d''8  
\bar "|"   e''4    c''8    a'8    e''8    a'8    b'16    c''16    d''8  
\bar "|"   e''8    d''8    g''8    e''8    d''8    g'8    b'16    c''16    d''8 
 }   \repeat volta 2 {   e''8    a'8    c''8    a'8    e''8    a'8    b'16    
c''16    d''8  \bar "|"   e''8    a'8    c''8    a'8    d''8    g'8    b'16    
c''16    d''8  \bar "|"   e''8    a'8    c''8    a'8    e''8    a'8    b'16    
c''16    d''8  \bar "|"   e''8    d''8    g''8    e''8    d''8    g'8    b'16   
 c''16    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
