\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Boyen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1356#setting24087"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1356#setting24087" {"https://thesession.org/tunes/1356#setting24087"}}}
	title = "Soldier's Joy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8    g'8  \bar "|"   a'8    
fis'8    d'8    fis'8    a'8    fis'8    d'8    fis'8  \bar "|"   a'4    d''4   
 d''4    cis''8    b'8  \bar "|"   a'8    fis'8    d'8    fis'8    a'8    fis'8 
   d'8    fis'8  \bar "|"   g'4    e'4    e'4    fis'8    g'8  \bar "|"     
\bar "|"   a'8    fis'8    d'8    fis'8    a'8    fis'8    d'8    fis'8  
\bar "|"   a'4    d''4    d''4    d''8    e''8  \bar "|"   fis''8    a''8    
fis''8    d''8    e''8    g''8    e''8    cis''8  \bar "|"   d''4    fis''4    
d''4  }     \repeat volta 2 {   d''8    e''8  \bar "|"   fis''8    d''8    a'8  
  d''8    fis''8    a''8    g''8    fis''8  \bar "|"   e''8    d''8    cis''8   
 d''8    e''8    fis''8    g''8    e''8  \bar "|"   fis''8    d''8    a'8    
d''8    fis''8    g''8    a''8    fis''8  \bar "|"   e''8    d''8    cis''8    
b'8    a'4    d''8    e''8  \bar "|"     \bar "|"   fis''8    d''8    a'8    
d''8    fis''8    a''8    g''8    fis''8  \bar "|"   e''8    d''8    cis''8    
d''8    e''8    fis''8    g''8    e''8  \bar "|"   fis''8    a''8    fis''8    
d''8    e''8    g''8    e''8    cis''8  \bar "|"   d''4    fis''4    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
