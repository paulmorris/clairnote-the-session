\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dogbox"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/19#setting28759"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/19#setting28759" {"https://thesession.org/tunes/19#setting28759"}}}
	title = "Connaughtman's Rambles, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   \repeat volta 2 {   b'8.    e''16    e''8    b'8.    
d''16    d''8  \bar "|"   b'8    e''8    b'8    d''8    b'8    a'8  \bar "|"   
b'8.    e''16    e''8    b'8.    d''16    d''8  \bar "|"   b'8    a'8    b'8    
d''4    a'8  \bar "|"     b'8.    e''16    e''8    b'8.    d''16    d''8  
\bar "|"   b'8    e''8    cis''8    d''8    b'8    a'8  \bar "|"   d''8.    
b'16    a'8    g'8    a'8    b'8  \bar "|"   a'8    g'8    e'8    e'4    d'8  
\bar "||"     b4    d'8    g'8    d'8    d'8  \bar "|"   e'8    d'8    e'8    
g'8    d'8    d'8  \bar "|"   b4    d'8    g'8    a'8    b'8  \bar "|"   a'8    
g'8    e'8    e'4    d'8  \bar "|"     b4    d'8    g'8    d'8    d'8  \bar "|" 
  e'8    d'8    e'8    g'8    d'8    d'8  \bar "|"   b4    d'8    g'8    a'8    
b'8  \bar "|"   a'8    g'8    e'8    e'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
