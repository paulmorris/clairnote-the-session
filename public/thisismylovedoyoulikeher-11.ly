\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/6#setting12359"
	arranger = "Setting 11"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/6#setting12359" {"https://thesession.org/tunes/6#setting12359"}}}
	title = "This Is My Love, Do You Like Her?"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   \repeat volta 2 {   g'4    fis'8    \bar "|"   e'4   
 e'8    e'8    dis'8    e'8    \bar "|"   g'4.    g'8    a'8    b'8    \bar "|" 
  a'4    a'8    a'8    gis'8    a'8    \bar "|"   d''4.    e''4. ^"~"    
\bar "|"     d''8    b'8    a'8    g'4    b'8    \bar "|"   a'8    fis'8    e'8 
   d'4    fis'8    \bar "|"   g'4    e'8    fis'8    e'8    d'8    \bar "|"   
e'4.    }     \repeat volta 2 {   e'8    g'16    a'16    b'8    \bar "|"   e''4 
   e''8    e''8    d''8    b'8    \bar "|"   d''4    d''8    d''8    e''8    
fis''8    \bar "|"   e''4    e''8    e''8    d''8    b'8    \bar "|"   d''4.    
b'4    e''8    \bar "|"     d''8    b'8    g'8    g'8    d''8    b'8    
\bar "|"   a'8    fis'8    e'8    d'8    e'8    fis'8    \bar "|"   g'8    
fis'8    e'8    fis'8    e'8    d'8    \bar "|"   e'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
