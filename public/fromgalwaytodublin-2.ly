\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Earl Adams"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1464#setting14856"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1464#setting14856" {"https://thesession.org/tunes/1464#setting14856"}}}
	title = "From Galway To Dublin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   b8    d'8    \bar "|"   e'2    e'8    d'8    b8    
d'8    \bar "|"   g'8    fis'8    g'8    b'8    a'4    g'8    a'8    \bar "|"   
b'8    d''8    g''8    e''8    d''8    b'8    a'8    fis'8    \bar "|"   g'8    
e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   d'4    b8    d'8    \bar "|"   e'2 
   e'8    d'8    b8    d'8    \bar "|"   g'8    fis'8    g'8    b'8    a'4    
g'8    a'8    \bar "|"   b'8    d''8    g''8    e''8    d''8    b'8    a'8    
fis'8    \bar "|"   g'8    e'8    fis'8    d'8    e'4    b8    d'8    \bar "|"  
 e'2    e'8    d'8    b8    d'8    \bar "|"   g'8    fis'8    g'8    b'8    a'4 
   g'8    a'8    \bar "|"   b'8    d''8    g''8    e''8    d''8    b'8    a'8   
 fis'8    \bar "|"   g'8    e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   d'4    
b8    d'8    \bar "|"   e'2    e'8    d'8    b8    d'8    \bar "|"   g'8    
fis'8    g'8    b'8    a'4    g'8    a'8    \bar "|"   b'8    d''8    g''8    
e''8    d''8    b'8    a'8    fis'8    \bar "|"   g'8    e'8    fis'8    d'8    
e'4    b'8    d''8    \bar "|"   \tuplet 3/2 {   e''8    fis''8    g''8  }   
e''8    d''8    b'8    c''8    b'8    a'8    \bar "|"   g'8    fis'8    g'8    
b'8    d''4    g''8    fis''8    \bar "|"   e''8    g''8    e''8    d''8    b'8 
   c''8    b'8    a'8    \bar "|"   g'8    e'8    \tuplet 3/2 {   e'8    e'8    
e'8  }   d'8    e'8    b8    d'8    \bar "|"   e'4    b8    d'8    e'8    d'8   
 b8    e'8    \bar "|"   d'8    e'8    g'8    b'8    a'4    g'8    a'8    
\bar "|"   b'8    d''8    g''8    e''8    d''8    b'8    a'8    fis'8    
\bar "|"   g'8    e'8    \tuplet 3/2 {   fis'8    e'8    d'8  }   e'4    
\tuplet 3/2 {   b'8    c''8    d''8  }   \bar "|"   e''8    g''8    e''8    d''8 
   b'8    c''8    b'8    a'8    \bar "|"   g'8    fis'8    g'8    b'8    d''4   
 g''8    fis''8    \bar "|"   e''8    g''8    e''8    d''8    b'8    c''8    
b'8    a'8    \bar "|"   g'8    e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   
d'8    e'8    b8    d'8    \bar "|"   e'2    e'8    d'8    b8    d'8    
\bar "|"   g'8    fis'8    g'8    b'8    a'4    g'8    a'8    \bar "|"   b'8    
d''8    g''8    e''8    d''8    b'8    a'8    fis'8    \bar "|"   \tuplet 3/2 {  
 g'8    fis'8    e'8  }   fis'8    d'8    e'4    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
