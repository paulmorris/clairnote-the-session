\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ediot"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/281#setting24950"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/281#setting24950" {"https://thesession.org/tunes/281#setting24950"}}}
	title = "Master Crowley's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   c'8  \repeat volta 2 {   a8    d'8    \tuplet 3/2 {   
d'8    d'8    d'8  }   a8    d'8    f'8    d'8  \bar "|"   e'8    c'8    
\tuplet 3/2 {   c'8    c'8    c'8  }   g8    c'8    e'8    c'8  \bar "|"   a8    
d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8    f'8    d'8  \bar "|"  
   \tuplet 3/2 {   e'8    f'8    g'8  }   c''8    g'8    e'8    d'8    d'8    
c'8  \bar "|"   a8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8    
f'8    d'8  \bar "|"   e'8    c'8    \tuplet 3/2 {   c'8    c'8    c'8  }   g8   
 c'8    e'8    c'8  \bar "|"     a8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  
}   a8    d'8    f'8    d'8  \bar "|" \tuplet 3/2 {   e'8    f'8    g'8  }   
c''8    g'8    e'8    d'8    d'8    c'8  \bar "|"   a4.    c'8    a8    c'8    
f'8    e'8  \bar "|"     e'8    d'8    c'8    d'8    bes4.    g8  \bar "|"   a8 
   d'8    d'8    d'8    d'8    a8    d'8    f'8    d'8  \bar "|"   e'8    g'8   
 c''8    g'8    e'8    d'8    d'8    c'8  \bar "|"     a8    d'8    
\tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8    f'8    d'8  \bar "|"   e'8   
 c'8    \tuplet 3/2 {   c'8    c'8    c'8  }   g8    c'8    e'8    c'8  \bar "|" 
  a8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8    f'8    d'8  
\bar "|"     \tuplet 3/2 {   e'8    f'8    g'8  }   c''8    g'8    e'8    d'8    
d'8    e'8  \bar "||"   d'8    a''8    f''8    a''8    e''8    a''8    d''8    
f''8  \bar "|"   e''8    c''4.    a''8    c''4.  \bar "|"   a'8    a''8    f''8 
   a''8    e''8    a''8    d''8    b'8  \bar "|"     c''8    a'8    g'8    e'8  
  e'8    d'8    cis'8    e'8  \bar "|"   d'8    a''8    f''8    a''8    e''8    
a''8    d''8    f''8  \bar "|"   e''8    c''4.    a''8    c''4.  \bar "|"   d'8 
   e'8    f'8    a'8    g'8    e'4.  \bar "|"   g'8    e'8    c'8    d'8    e'8 
   d'8    cis'8    e'8  \bar "||"     d'8    a''8    f''8    a''8    e''8    
a''8    d''8    f''8  \bar "|"   e''8    c''8    g''8    c''8    a''8    c''8   
 g''8    c''8  \bar "|"   a'8    a''8    f''8    a''8    e''8    a''8    d''8   
 b'8  \bar "|"   c''8    a'8    g'8    e'8    e'8    d'8    cis'8    e'8  
\bar "|"   d'8    a''8    f''8    a''8    e''8    a''8    d''8    f''8  
\bar "|"     e''8    c''4.    a''8    c''4.    \bar "|"   d'8    e'8    f'8    
a'8    g'8    e'4.  \bar "|"   g'8    e'8    c'8    d'8    e'8    d'8    d'8    
g8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
