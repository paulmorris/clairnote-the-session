\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Trinil"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1108#setting1108"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1108#setting1108" {"https://thesession.org/tunes/1108#setting1108"}}}
	title = "Khazi"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \minor   bes'8    a'8    c''8    bes'8    f'8    ees'8  
\bar "|"   d'8    ees'8    f'8    bes'4    c''8   ~    \bar "|"   c''8    d''8  
  bes'8    c''4    bes'8  \bar "|" \tuplet 3/2 {   a'8    bes'8    c''8  }   a'8 
   f'8    g'8    a'8  \bar "|"     bes'4. ^"~"    bes'8    f'8    ees'8  
\bar "|"   d'8    ees'8    f'8    bes'8    d''8    f''8  \bar "|"   g''8    
f''8    d''8    ees''8    d''8    c''8  \bar "|"   c''8    bes'8    a'8    
bes'8    d''8    c''8  \bar ":|."   c''8    bes'8    a'8    bes'8    r4 
\bar "||"     c''4.    c''8    a'8    bes'8  \bar "|"   c''8    d''8    a'8    
c''4    f'8   ~    \bar "|"   f'8    a'8    c''8    f''4    e''8  \bar "|"   
d''8    c''8    a'8    bes'8    a'8    bes'8  \bar "|"     c''4. ^"~"    c''8   
 a'8    bes'8  \bar "|"   c''8    d''8    a'8    c''4. ^"~"  \bar "|"   c''4. 
^"~"    e'8    f'8    g'8  \bar "|"   g'8    f'8    e'8    f'8    a'8    bes'8  
\bar "|"     c''4.    c''8    a'8    bes'8  \bar "|"   c''8    d''8    a'8    
c''4    f'8   ~    \bar "|"   f'8    a'8    c''8    f''4    e''8  \bar "|"   
d''8    c''8    a'8    bes'8    a'8    bes'8  \bar "|"     c''8    f'8    f'8   
 d''8    f'8    f'8  \bar "|"   c''8    d''8    a'8    c''4. ^"~"  \bar "|"   
c''4. ^"~"    e'8    f'8    g'8  \bar "|"   g'8    f'8    e'8    f'8    g'8    
a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
