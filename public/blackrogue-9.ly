\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Eugenie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1076#setting29384"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1076#setting29384" {"https://thesession.org/tunes/1076#setting29384"}}}
	title = "Black Rogue, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian   \bar "|"   cis''8 ^"A"   a'8    a'8      b'8 
^"G"   g'8    g'8    \bar "|"   cis''8 ^"A"   a'8    a'8      a'8 ^""   b'8    
d''8    \bar "|"     cis''8 ^"A"   a'8    a'8      b'8 ^"G"   a'8    g'8    
\bar "|"     a'8 ^"D"   fis'8    d'8    d'4    d''8  \bar "|"       cis''8 ^"A" 
  a'8    a'8      b'8 ^"G"   g'8    g'8    \bar "|"     cis''8 ^"A"   a'8    
a'8    a'4    fis'8    \bar "|"   g'4 ^"G"   a'8    b'8    a'8    g'8    
\bar "|"   a'8 ^"D"   fis'8    d'8    d'4    b'8    \bar ":|."     a'8 ^"D"   
fis'8    d'8    d'4.  \bar "||"     \bar ".|:"   fis''4. ^"D"^"~"      g''4. 
^"C"^"~"    \bar "|"     a''8 ^"D"   fis''8    d''8      cis''8 ^"A"   b'8    
a'8  \bar "|"     fis''4. ^"D"^"~"      g''8 ^"C"   fis''8    g''8    \bar "|"  
   a''8 ^"D"   fis''8    d''8      d''8 ^"A"   fis''8    g''8    \bar "|"       
a''8 ^"D"   g''8    fis''8      g''8 ^"C"   fis''8    e''8    \bar "|"     
fis''8 ^"D"   e''8    d''8      e''4 ^"A"   d''8    \bar "|"     cis''8 ^""   
b'8    a'8      b'8 ^"G"   a'8    g'8    \bar "|"   a'8 ^"D"   fis'8    d'8    
d'4.    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
