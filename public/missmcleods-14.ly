\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/75#setting24548"
	arranger = "Setting 14"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/75#setting24548" {"https://thesession.org/tunes/75#setting24548"}}}
	title = "Miss McLeod's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major \time 2/4   \repeat volta 2 {   d'8    d''8    b'16 (   
a'16    b'16    d''16  -)   \bar "|"   fis'8    fis'8    fis'16 (   g'16    
fis'16    e'16  -)   \bar "|"   d'8    d''8    b'16 (   a'16    b'16    d''16  
-)   \bar "|"   e'8    e'8    e'16    g'16    fis'16    e'16    \bar "|"     
d'8    d''8    b'16    a'16    b'16    d''16    \bar "|"   fis'8    fis'16    
g'16    fis'16    g'16    a'8    \bar "|"   b'8    b'16    cis''16    b'16    
a'16    b'16    cis''16    \bar "|"   d''16    b'16    a'16    fis'16    e'4    
}     \repeat volta 2 {   d'8    fis'16    d'16    a'16    d'16    fis'16    
d'16    \bar "|"   fis'8    fis'16    e'16    fis'16    g'16    fis'16    e'16  
  \bar "|"   d'8    fis'16    d'16    a'16    d'16    fis'16    d'16    
\bar "|"   e'8    e'16    d'16    e'16    g'16    fis'16    e'16    \bar "|"    
 d'8    fis'16    d'16    a'16    d'16    fis'16    d'16    \bar "|"   fis'8    
fis'8    fis'8    g'16    a'16    \bar "|"   b'8    b'16 (   cis''16  -)   b'16 
   a'16    b'16    cis''16    \bar "|"   d''16    b'16    a'16    fis'16    e'4 
   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
