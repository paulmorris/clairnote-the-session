\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1323#setting14664"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1323#setting14664" {"https://thesession.org/tunes/1323#setting14664"}}}
	title = "Tommy Peoples'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   \repeat volta 2 {   fis'8.    g'16    a'8.    fis'16  
  a'4  \bar "|"   d''8.    cis''16    b'8.    ais'16    b'4  \bar "|"   e''8    
d''8    cis''8.    bis'16    cis''4  \bar "|" \tuplet 3/2 {   a'8    b'8    
cis''8  }   d''8.    e''16    fis''4  \bar "|"   fis'8.    g'16    a'4    a'4  
\bar "|"   d''8.    cis''16    b'4    b'4  \bar "|"   e''8.    d''16    cis''4  
  cis''4  \bar "|" \tuplet 3/2 {   a'8    b'8    cis'8  }   d''4    d''4  }   
\repeat volta 2 {   fis''8.    g''16    a''8.    fis''16    d''8.    e''16  
\bar "|"   d''8.    cis''16    b'4    g'4  \bar "|"   e''8.    d''16    cis''4  
  cis''4  \bar "|" \tuplet 3/2 {   a'8    b'8    cis''8  }   d''4    d''4  
\bar "|"   fis''8.    g''16    a''8.    fis''16    d''8.    e''16  \bar "|"   
d''8.    cis''16    b'4    g'4  \bar "|"   e''8.    d''16    cis''8.    bis'16  
  cis''4  \bar "|" \tuplet 3/2 {   a'8    b'8    cis''8  }   d''4    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
