\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/166#setting12802"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/166#setting12802" {"https://thesession.org/tunes/166#setting12802"}}}
	title = "Green Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   b'8  \bar "|"   a'8    fis'8    
fis'8    a'8    fis'8    e'8  \bar "|"   d'8    fis'8    fis'8    a'8    b'8    
e''8  \bar "|"   fis''8    d''8    d''8    e''8    d''8    b'8  \bar "|"   a'8  
  fis''8    d''8    e''8    d''8    b'8  \bar "|"   a'8    fis'8    fis'8    
a'8    fis'8    e'8  \bar "|"   d'8    fis'8    fis'8    a'8    b'8    e''8  
\bar "|"   fis''8    d''8    d''8    e''8    d''8    b'8  \bar "|"   a'8    
fis'8    e'8    d'4  }   \repeat volta 2 {   e''8  \bar "|"   fis''8    a''8    
a''8    b''8    a''8    fis''8  \bar "|"   d''8    fis''8    d''8    e''8    
d''8    e''8  \bar "|"   fis''8    d''8    d''8    e''8    d''8    b'8  
\bar "|"   a'8    fis''8    d''8    e''8    d''8    b'8  \bar "|"   fis''8    
a''8    a''8    b''8    a''8    fis''8  \bar "|"   d''8    fis''8    d''8    
e''8    d''8    e''8  \bar "|"   fis''8    d''8    d''8    e''8    d''8    b'8  
\bar "|"   a'8    fis'8    e'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
