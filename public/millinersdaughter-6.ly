\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Damian"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1409#setting14785"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1409#setting14785" {"https://thesession.org/tunes/1409#setting14785"}}}
	title = "Milliner's Daughter, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   d''8    g''8    g''4 ^"~"    d''8    g''8    g''4 
^"~"  \bar "|"   d''8    fis''8    fis''4 ^"~"    d''8    fis''8    fis''4 
^"~"  \bar "|"   d''8    g''8    g''4 ^"~"    b''8    g''8    a''8    g''8  
\bar "|"   d''8    cis''8    d''8    e''8    fis''8    d''8    c''!8    a'8  
\bar "|"   d''8    g''8    g''4 ^"~"    b''8    g''8    a''8    g''8  \bar "|"  
 d''8    cis''8    d''8    e''8    fis''4. ^"~"  \bar "|"   a''8    fis''8    
g''8    e''8    fis''8    d''8    e''8    cis''8  \bar "|"   d''8    cis''8    
d''8    e''8    fis''8    d''8    c''!8    a'8  \bar "||"   d''8    g''8    
g''4 ^"~"    d''8    g''8    g''4 ^"~"  \bar "|"   d''8    f''8    f''!4 ^"~"   
 d''8    f''!8    f''!4 ^"~"  \bar "|"   d''8    g''8    g''4 ^"~"    b''8    
g''8    a''8    g''8  \bar "|"   d''8    cis''8    d''8    e''8    f''8    d''8 
   cis''8    a'8  \bar "|"   d''8    g''8    g''4 ^"~"    b''8    g''8    a''8  
  g''8  \bar "|"   d''8    cis''8    d''8    e''8    fis''4. ^"~"  \bar "|"   
a''8    fis''8    g''8    e''8    fis''8    d''8    e''8    cis''8  \bar "|"   
d''8    cis''8    d''8    e''8    fis''8    d''8    cis''8    a'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
