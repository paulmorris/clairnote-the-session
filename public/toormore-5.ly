\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/110#setting26399"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/110#setting26399" {"https://thesession.org/tunes/110#setting26399"}}}
	title = "Toormore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key b \minor   fis'4    b'8    b'8    a'8    b'8  \bar "|"   d''4   
 e''8    fis''4    d''8  \bar "|"   e''4.    e''8    fis''8    e''8  \bar "|"   
cis''4    e''8    a'4.  \bar "|"     fis'4    b'8    b'8    a'8    b'8  
\bar "|"   d''4    e''8    fis''4    d''8  \bar "|"   e''4    d''8    b'4    
a'8  \bar "|"   b'4.    b'8    a'8    g'8  \bar ":|."   b'4.    d''4    e''8  
\bar "||"     fis''8    cis''16    a'16    fis''8    e''8    cis''16    a'16    
e''8  \bar "|"   d''8    fis'16    g'16    d''8    cis''8    fis'16    g'16    
cis''8  \bar "|"   b'8    cis''16    a'16    b'8    e''8    d''8    b'8  
\bar "|"   a'8    fis'8    e'8    fis'4    e'8  \bar "|"     fis'4    b'8    
b'8    a'8    b'8  \bar "|"   d''4    e''8    fis''4    d''8  \bar "|"   e''4   
 d''8    b'4    a'8  \bar "|"   b'4.    d''4    e''8  \bar ":|."   b'4.    b'8  
  a'8    g'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
