\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/589#setting23581"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/589#setting23581" {"https://thesession.org/tunes/589#setting23581"}}}
	title = "Julia Delaney's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   \repeat volta 2 {   c''8    \bar "|"     d''8 ^"Dm"  
 c''8    a'8    g'8  \grace {    f'8    g'8  }   f'4    d'8    f'8    \bar "|"  
 \grace {    e'8 ^"C"   f'8  }   e'4    c'8    e'8      f'8 ^"F"   e'8    f'8   
 a'8    \bar "|"     d''8 ^"Dm"   c''8    a'8    g'8  \grace {    f'8    g'8  } 
  f'4    d'8    f'8    \bar "|"     a'8 ^"Dm"   d''8  \grace {    e''8  }   
d''8    cis''8    d''8    e''8    f''8    e''8    \bar "|"       d''8 ^"Dm"   
c''8    a'8    g'8  \grace {    f'8    g'8  }   f'4    d'8    f'8    \bar "|"   
\grace {    e'8 ^"C"   f'8  }   e'4    c'8    e'8      f'8 ^"F"   e'8    f'8    
a'8    \bar "|"     d''8 ^"Dm"   c''8    a'8    g'8  \grace {    f'8    g'8  }  
 f'4    d'8    f'8    \bar "|"     a'8 ^"Dm"   d''8  \grace {    e''8  }   d''8 
   cis''8    d''4    d''8    }     \repeat volta 2 {   e''8    \bar "|"     
f''8 ^"Dm"   e''8    d''8    e''8      f''8 ^"F"   a''8    g''8    f''8    
\bar "|"     e''8 ^"C"   c''8    g''8    c''8    a''8    c''8    g''8    e''8   
 \bar "|"     f''8 ^"Dm"   e''8    d''8    e''8      f''8 ^"F"   a''8    g''8   
 f''8    \bar "|"     e''8 ^"C"   d''8    c''8    g'8      a'8 ^"Dm"   d''8    
d''8    e''8    \bar "|"       f''8 ^"Dm"   e''8    d''8    e''8      f''8 ^"F" 
  a''8    g''8    f''8    \bar "|"     e''8 ^"C"   c''8    g''8    c''8    a''8 
   c''8    g''8    e''8    \bar "|"     f''8 ^"Dm"   e''8    d''8    f''8      
e''8 ^"C"   d''8    c''8    g'8    \bar "|"     a'8 ^"Dm"   d''8  \grace {    
e''8  }   d''8    cis''8    d''4    d''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
