\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/28#setting12417"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/28#setting12417" {"https://thesession.org/tunes/28#setting12417"}}}
	title = "Dusty Miller, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \major   \repeat volta 2 {   b'4    d''8    d''4    a'8    b'4 
   gis'8    \bar "|"   e'4    a'8    a'4    b'8    cis''4    a'8    \bar "|"   
b'4    d''8    d''4    a'8    b'4    gis'8    \bar "|"   d'4    gis'8    gis'4  
  a'8    b'4    gis'8    }   \repeat volta 2 {   b'4    d''8    e''4    fis''8  
  gis''4.    \bar "|"   a''4    a'8    a'4    b'8    cis''4    a'8    \bar "|"  
 b'4    d''8    e''4    fis''8    gis''4    a''8    \bar "|"   gis''4    e''8   
 d''4    cis''8    b'4    gis'8    }   \repeat volta 2 {   b'4.    b'4    a'8   
 gis'4    a'8    \bar "|"   e'4    a'8    a'4    b'8    cis''4    a'8    
\bar "|"   b'4.    b'4    a'8    gis'4    a'8    \bar "|"   d'4    gis'8    
gis'4    a'8    b'4    gis'8    }   \repeat volta 2 {   cis''4    e''8    e''4  
  b'8    cis''8    b'8    a'8    \bar "|"   fis'4    b'8    b'4    cis''8    
d''8    cis''8    b'8    \bar "|"   cis''4    e''8    e''4    b'8    cis''8    
b'8    a'8    \bar "|"   e'4    a'8    a'4    b'8    cis''8    b'8    a'8    }  
 \repeat volta 2 {   cis''4    e''8    fis''4    gis''8    a''8    gis''8    
a''8    \bar "|"   b''4    b'8    b'4    cis''8    d''8    cis''8    b'8    
\bar "|"   cis''4    e''8    fis''4    gis''8    a''4    b''8    \bar "|"   
a''4    fis''8    e''4    d''8    cis''8    b'8    a'8    }   \repeat volta 2 { 
  cis''4.    cis''4    b'8    a'8    gis'8    a'8    \bar "|"   fis'4    b'8    
b'4    cis''8    d''8    cis''8    b'8    \bar "|"   cis''4.    cis''4    b'8   
 a'8    gis'8    a'8    \bar "|"   e'4    a'8    a'4    b'8    cis''8    b'8    
a'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
