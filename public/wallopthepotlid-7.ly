\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1454#setting14848"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1454#setting14848" {"https://thesession.org/tunes/1454#setting14848"}}}
	title = "Wallop The Potlid"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   a'8  \bar "|"   b'8    a'8    b'8 
   c''8    b'8    c''8  \bar "|"   d''8    g''8    g''8    fis''8    d''8    
c''8  \bar "|"   b'8    a'8    b'8    c''8    b'8    c''8  \bar "|"   d''8    
g''8    fis''8    g''4    a''8  \bar "|"   b''8    a''8    g''8    a''8    g''8 
   fis''8  \bar "|"   g''4. ^"~"    fis''8    d''8    c''8  \bar "|"   b'8    
a'8    g'8    a'8    d''8    c''8  \bar "|"   a'8    g'8    fis'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
