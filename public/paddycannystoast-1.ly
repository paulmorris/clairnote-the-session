\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "donnchad"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/675#setting675"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/675#setting675" {"https://thesession.org/tunes/675#setting675"}}}
	title = "Paddy Canny's Toast"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \minor   g'8    f'8    \bar "|"   d'8    g'8    g'4 ^"~"    
d''8    g'8    c''8    g'8    \bar "|"   d''8    g'8    c''8    a'8    g'8    
f'8    d'8    c'8    \bar "|"   d'8    g'8    g'4 ^"~"    d''8    g'8    c''8   
 g'8    \bar "|"   d''8    c''8    a'8    f'8    a'8    g'8    }     g'8    a'8 
   \bar "|"   bes'4. ^"~"    c''8    d''8    f''8    f''4 ^"~"    \bar "|"   
g''4. ^"~"    f''8    f''8    e''8    d''8    c''8    \bar "|"   bes'8    a'8   
 bes'8    c''8    d''8    f''8    f''4 ^"~"    \bar "|"   g''8    f''8    d''8  
  bes'8    c''4    d''8    c''8    \bar "|"     bes'4. ^"~"    c''8    d''8    
f''8    e''8    f''8    \bar "|"   g''4. ^"~"    e''8    f''4    d''8    f''8   
 \bar "|"   g''8    f''8    d''8    c''8    bes'8    d''8    c''8    d''8    
\bar "|"   bes'8    g'8    a'8    f'8    g'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
