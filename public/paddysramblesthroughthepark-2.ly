\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tøm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/512#setting20726"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/512#setting20726" {"https://thesession.org/tunes/512#setting20726"}}}
	title = "Paddy's Rambles Through The Park"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 3/4 \key a \mixolydian   cis''8    d''8  \bar "|"   e''4    e''8    d''8  
  cis''16    e''8.    \bar "|"   d''4    d''8    cis''8    b'8    d''8    
\bar "|"   cis''4    cis''8    b'8    a'8    cis''8  \bar "|"     b'2    e'8    
fis'8  \bar "|"   g'4    b'4    e''8    cis''8  \bar "|"   d''4.    e''8    
fis''8    d''8  \bar "|"   e''4    e'4    gis'4  \bar "|"   a'2  }     a'16    
g'16    fis'16    g'16  \bar "|"   e'4    a'4    a'4  \bar "|"   g'4    b'4    
b'8    a'8  \bar "|"   b'8    g'4.    g'4  \bar "|"     g'2    a'8    g'8  
\bar "|"   e'4    a'4    a'8    b'8  \bar "|"   c''2    b'8    a'8  \bar "|"   
gis'8    e'4.    e'4  \bar "|"   e'2    cis''8    d''8  \bar "|"     e''4    
e''8    d''8    cis''16    e''8.    \bar "|"   d''4    d''8    cis''8    b'8    
d''8    \bar "|"   cis''4    cis''8    b'8    a'8    cis''8  \bar "|"     b'2   
 e'8    fis'8  \bar "|"   g'4    b'4    e''8    cis''8  \bar "|"   d''4.    
e''8    fis''8    d''8  \bar "|"   e''4    e'4    gis'4  \bar "|"   a'2  
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
