\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/560#setting25127"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/560#setting25127" {"https://thesession.org/tunes/560#setting25127"}}}
	title = "Tarbolton, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   e'8 ^"Em"   e''8    e''16    
e''16    d''8    e''4    b'8    a'8  \bar "|"   g'8 ^"Em"   b'8    a'8    g'8   
 fis'8    g'8    fis'8    e'8  \bar "|"   d'8 ^"D"   d''8    d''16    d''16    
cis''8    d''4    a'8    g'8  \bar "|"   fis'8 ^"D"   d'8    a'8    g'8    
fis'8    d'8    d'8    fis'8  \bar "|"       e'8 ^"Em"   b'8    e''16    e''16  
  d''8    e''8    fis''8    g''8    e''8  \bar "|"   fis''8 ^"D"   e''8    d''8 
   fis''8      e''4 ^"Em"   b'8    a'8  \bar "|"   g'8 ^"Em"   b'8    a'8    
g'8      fis'8 ^"D"   g'8    a'8    c''8  \bar "|"   b'8 ^"D"   g'8    a'8    
fis'8      g'8 ^"Em"   e'8    e'4  }     \repeat volta 2 {   g''8 ^"Em"   
fis''8    e''8    fis''8    g''16    g''16    fis''8    b''8    e''8  \bar "|"  
 g''16 ^"Em"   g''16    fis''8    b''8    e''8    g''8    fis''8    e''8    
fis''8  \bar "|"   d''16 ^"D"   d''16    d''8    fis''8    d''8    a'8    d''8  
  fis''8    d''8  \bar "|"   a'8 ^"D"   b'8    a'8    g'8    fis'8    d'8    
d'8    fis'8  \bar "|"       g'16 ^"Em"   g'16    g'8    b'8    g'8      fis'16 
^"D"   fis'16    fis'8    a'8    fis'8  \bar "|"   e'4 ^"Em"   e''8    fis''8   
 g''8    fis''8    e''8    d''8  \bar "|"   b'16 ^"Em"   b'16    b'8    d''8    
b'8    a'8    g'8    fis'8    a'8  \bar "|"   b'8 ^"D"   g'8    a'8    fis'8    
  g'8 ^"Em"   e'8    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
