\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dexy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/455#setting455"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/455#setting455" {"https://thesession.org/tunes/455#setting455"}}}
	title = "Going To The Well For Water"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key d \major   a'4    fis''8    a'4    fis''8    a'4    fis''8    
fis''8    e''8    d''8  \bar "|"   b'8    g''8    g''8    b'8    g''8    g''8   
 b'8    g''8    g''8    g''8    fis''8    e''8  \bar "|"     cis''8    d''8    
cis''8    b'8    cis''8    b'8    a'8    cis''8    e''8    a''4    fis''8  
\bar "|"   g''4    e''8    cis''8    d''8    e''8    d''4.    d''8    cis''8    
b'8  \bar ":|."   g''4    e''8    cis''8    d''8    e''8    d''4.    d''4    
e''8  \bar "||"     \bar ".|:"   fis''4    fis''8    fis''8    e''8    d''8    
e''4    e''8    e''8    d''8    cis''8  \bar "|"   d''4    d''8    d''8    
cis''8    b'8    cis''4    cis''8    cis''8    b'8    a'8  \bar "|"     g'8    
b'8    b'8    g'8    b'8    b'8    fis'8    a'8    a'8    fis'8    a'8    a'8  
\bar "|"   e'8    d'8    e'8    e''4    d''8    cis''8    b'8    cis''8    d''4 
   e''8  \bar ":|."   e'8    d'8    e'8    e''4    d''8    cis''8    b'8    
cis''8    d''4.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
