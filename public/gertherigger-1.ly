\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1367#setting1367"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1367#setting1367" {"https://thesession.org/tunes/1367#setting1367"}}}
	title = "Ger The Rigger"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key a \major   e''8    a'8    e''8    a'8  \bar "|"   e''16    
fis''16    e''16    d''16    cis''8    b'16    a'16  \bar "|"   d''4    d''16   
 e''16    fis''16    gis''16  \bar "|"   a''8    e''8    fis''8    e''8  
\bar "|"     e''8    a'8    e''8    a'8  \bar "|"   e''16    fis''16    e''16   
 d''16    cis''8    b'16    a'16  \bar "|"   d''8    fis''16    d''16    cis''8 
   e''16    cis''16  \bar "|"   b'16    a'16    b'16    cis''16    a'4  }     
\repeat volta 2 {   a''8    e''8    fis''16    e''16    cis''16    e''16  
\bar "|"   a''8    e''8    fis''16    e''16    cis''16    e''16  \bar "|"   
d''4    d''16    e''16    fis''16    gis''16  \bar "|"   a''8    e''8    fis''8 
   e''8  \bar "|"     a''8    e''8    fis''16    e''16    cis''16    e''16  
\bar "|"   a''8    e''8    fis''16    e''16    cis''16    e''16  \bar "|"   
d''8    fis''16    d''16    cis''8    e''16    cis''16  \bar "|"   b'16    a'16 
   b'16    cis''16    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
