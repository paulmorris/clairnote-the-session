\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1484#setting14870"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1484#setting14870" {"https://thesession.org/tunes/1484#setting14870"}}}
	title = "Tail Toddle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'16    g'16    g'8    b'8    g'8 
   c''4    g'8    e'8    \bar "|"   g'16    g'16    g'8    b'8    g'8    a'4    
fis'8    d'8    \bar "|"   g'16    g'16    g'8    b'8    g'8    c''8    a'8    
b'8    g'8    \bar "|"   a'16    b'16    c''8    b'16    c''16    d''8    a'4   
 fis'8    d'8    }   \repeat volta 2 {   c''4    g'8    e'8    e'8    c''8    
g'8    e'8    \bar "|"   c''4    g'8    e'8    d'8    a'8    fis'8    d'8    
\bar "|"   c''4    g'8    e'8    d''16    c''16    b'16    a'16    b'8    g'8   
 \bar "|"   a'16    b'16    c''8    b'16    c''16    d''8    a'4    fis'8    
d'8    }   \repeat volta 2 {   g'16    g'16    g'8    b'8    g'8    c''8    a'8 
   b'8    e'8    \bar "|"   g'16    g'16    g'8    b'8    g'8    a'8    d'8    
fis'8    d'8    \bar "|"   g'16    g'16    g'8    b'8    g'8    c''8    a'8    
b'8    g'8    \bar "|"   a'16    b'16    c''8    b'16    c''16    d''8    c''4  
  a'8    fis'8    }   \repeat volta 2 {   c''4    e'8    c''8    e'8    c''8    
g'8    e'8    \bar "|"   c''4    e'8    c''8    d'8    a'8    fis'8    d'8    
\bar "|"   c''4    e'8    c''8    d''16    c''16    b'16    a'16    b'8    g'8  
  \bar "|"   a'16    b'16    c''8    b'16    c''16    d''8    a'4    fis'8    
d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
