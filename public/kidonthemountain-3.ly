\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/52#setting12487"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/52#setting12487" {"https://thesession.org/tunes/52#setting12487"}}}
	title = "Kid On The Mountain, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key e \minor   \repeat volta 2 {   b8    e'8    e'8    e'4    g'8    
e'4    e'8    \bar "|"   b4    e'8    e'4    g'8    a'8    fis'16    e'16    
d'8    \bar "|"   b8    e'8    e'8    e'8    g'8    g'8    e'4    g'8    
\bar "|"   b'4    g'8    a'4    g'8    fis'8    e'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
