\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "janglecrow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/43#setting24277"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/43#setting24277" {"https://thesession.org/tunes/43#setting24277"}}}
	title = "Geese In The Bog, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \minor   b'8  \repeat volta 2 {   c''8    e'8    e'8    g'8    
e'8    e'8  \bar "|"   c''8    e'8    e'8    g'8    a'8    b'8  \bar "|"   c''8 
   e'8    e'8    g'8    e'8    d'8  \bar "|"   e'8    a'8    a'8    a'4    b'8  
\bar "|"     c''8    e'8    e'8    g'8    e'8    e'8  \bar "|"   c''8    e'8    
e'8    g'8    a'8    b'8  \bar "|"   c''8    b'8    a'8    g'8    e'8    d'8  
\bar "|"   e'8    a'8    a'8    a'4    b'8  }     \repeat volta 2 {   c''8    
d''8    e''8    g''8    e''8    d''8  \bar "|"   e''8    a''8    a''8    g''8   
 e''8    d''8  \bar "|"   c''8    d''8    e''8    g''8    e''8    d''8  
\bar "|"   e''8    a''8    a''8    a''4    b'8  \bar "|"     c''8    d''8    
e''8    g''8    e''8    d''8  \bar "|"   e''8    a''8    a''8    g''8    e''8   
 d''8  \bar "|"   c''8    b'8    a'8    g'8    e'8    d'8  \bar "|"   e'8    
a'8    a'8    a'4    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
