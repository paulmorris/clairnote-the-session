\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1406#setting14777"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1406#setting14777" {"https://thesession.org/tunes/1406#setting14777"}}}
	title = "Jimmy O'Reilly's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   g'8    a'8    g'8    f'8    d'8    f'8    c'8    d'8 
 \bar "|"   f'4    a'8    f'8    c''8    f'8    a'8    f'8  \bar "|"   g'8    
a'8    g'8    f'8    d'8    f'8    c'8    e''8  \bar "|"   f''8    d''8    c''8 
   a'8    a'8    g'8    g'8    f'8  \bar ":|."   f''8    d''8    c''8    a'8    
a'8    g'8    g'8    g''8  \bar "||"   g''8    f''8    d''8    g''8    g''4    
a''8    g''8  \bar "|"   f''8    d''8    \tuplet 3/2 {   b'8    c''8    d''8  }  
 f''8    d''8    c''8    g''8  \bar "|"   g''8    f''8    d''8    g''8    g''4  
  f''8    g''8  \bar "|"   a''8    f''8    f''8    g''8    a''8    f''8    f''4 
^"~"  \bar "|"   g''8    f''8    d''8    g''8    g''8    f''8    d''8    g''8  
\bar "|"   f''8    d''8    c''8    a'8    f'8    g'8    a'8    c''8  \bar "|"   
d''8    g''8    g''4 ^"~"    f''4.    g''8  \bar "|"   f''8    d''8    c''8    
a'8    a'8    g'8    g'8    f'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
