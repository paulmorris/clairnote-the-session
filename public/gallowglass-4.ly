\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "DrSchlock"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1369#setting27744"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1369#setting27744" {"https://thesession.org/tunes/1369#setting27744"}}}
	title = "Gallowglass, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   e'16    d'16    \bar "|"   b8    e'8    e'8    e'8.   
 d'16    e'8    \bar "|"   fis'8    e'8    fis'8    d''4    b'8    \bar "|"   
a'8    fis'8    a'8    d''8    b'16    cis''16    d''16    b'16    \bar "|"   
a'8    fis'8    d'8    d'4    e'16    d'16    \bar "|"     b8    e'8    e'8    
e'8.    d'16    e'8    \bar "|"   fis'8    e'8    fis'8    e''4    d''8    
\bar "|"   b'8.    a'16    g'8    a'8.    g'16    fis'8    \bar "|"   b'8    
g'8    e'8    e'4    }     b'32    cis''32    dis''16    \bar "|"   e''8    b'8 
   e''16    fis''16    g''8.    fis''16    e''8    \bar "|"   d''8    e''8    
fis''8    g''4    fis''16    e''16    \bar "|"   d''8.    cis''16    b'8    a'8 
   fis'8    d''16    b'16    \bar "|"   a'8    fis'8    d'8    d'4    b'32    
cis''32    dis''16    \bar "|"     e''8    b'8    e''16    fis''16    g''8.    
fis''16    e''8    \bar "|"   d''8    e''8    fis''8    g''4    fis''16    
e''16    \bar "|"   d''8.    cis''16    b'8    a'8    fis'8    a'8    \bar "|"  
 b'8    g'8    e'8    e'4    b'32    cis''32    dis''16    \bar "|"     e''8    
b'8    e''16    fis''16    g''8.    fis''16    e''8    \bar "|"   d''8    e''8  
  fis''8    g''4    fis''16    e''16    \bar "|"   d''8.    cis''16    b'8    
a'8    fis'8    d''16    b'16    \bar "|"   a'8    fis'8    d'8    d'4    e'16  
  d'16    \bar "|"     b8    e'8    e'8    e'8.    d'16    e'8    \bar "|"   
fis'8    e'8    fis'8    e''4    d''8    \bar "|"   b'8.    a'16    g'8    a'8. 
   g'16    fis'8    \bar "|"   b'8    g'8    e'8    e'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
