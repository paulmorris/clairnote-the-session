\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/85#setting12592"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/85#setting12592" {"https://thesession.org/tunes/85#setting12592"}}}
	title = "Rakes Of Mallow, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \bar "|"   g'4    b'4    g'4    b'4  \bar "|"   g'4   
 b'4    c''8    b'8    a'8    g'8  \bar "|"   fis'4    a'4    fis'4    a'4  
\bar "|"   fis'4    a'4    b'8    a'8    g'8    fis'8  \bar "|"   g'4    b'4    
g'4    b'4  \bar "|"   g'4    b'4    d''4    d''4  \bar "|"   c''8    b'8    
a'8    g'8    fis'8    g'8    a'8    b'8    \bar "|"   g'2    g'4    r4 
\bar "|"   g''4    fis''8    e''8    d''4    c''4  \bar "|"   b'4    c''4    
d''2    \bar "|"   g''4    fis'8    e'8    d''4    c''4  \bar "|"   b'8    c''8 
   d''4    a'2  \bar "|"   g''4    fis''8    e''8    d''4    c''4  \bar "|"   
b'4    c''4    d''2    \bar "|"   c''8    b'8    a'8    g'8    fis'8    g'8    
a'8    b'8    \bar "|"   g'2    g'4    r4 \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
