\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1153#setting1153"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1153#setting1153" {"https://thesession.org/tunes/1153#setting1153"}}}
	title = "Brisk Young Lads, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \minor   \repeat volta 2 {   a'8    b'8    a'8    c''4    d''8 
 \bar "|"   e''8    d''8    c''8    b'4    a'8  \bar "|"   g'8    a'8    g'8    
b'4    c''8  \bar "|"   d''8    g''8    e''8    d''8    b'8    g'8  \bar "|"    
 a'8    b'8    a'8    c''4    d''8  \bar "|"   e''8    d''8    c''8    b'8    
c''8    d''8  \bar "|"   e''4    a'8    a'8    b'8    gis'8  \bar "|"   a'4.    
a''4    b'8  }     \repeat volta 2 {   c''8.    d''16    e''16    f''16    g''4 
   a''8  \bar "|"   g''8    e''8    c''8    g''8    e''8    c''8  \bar "|"   
g'8.    a'16    b'16    c''16    d''4    e''8  \bar "|"   d''8    b'8    g''8   
 d''8    b'8    g'8  \bar "|"     c''8.    d''16    e''16    f''16    g''4    
a''8  \bar "|"   g''8    e''8    c''8    c''8    e''8    gis''8  \bar "|"   
a''4    a'8    a'8    b'8    gis'8  \bar "|"   a'4.    a''4    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
