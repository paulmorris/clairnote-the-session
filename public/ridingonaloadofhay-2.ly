\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Heiko"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/1239#setting2609"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1239#setting2609" {"https://thesession.org/tunes/1239#setting2609"}}}
	title = "Riding On A Load Of Hay"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key e \dorian   \repeat volta 2 {   e'8.    fis'16    g'8    a'8  
\bar "|"   b'8    e''8    e''8    fis''16    e''16  \bar "|"   d''8    b'8    
a'8    g'8  \bar "|"   fis'8    d'8    d'4  \bar "|"     e'8.    fis'16    g'8  
  a'8  \bar "|"   b'8    e''8    e''8    fis''16    e''16  \bar "|"   g''8    
e''8    a''8    fis''8  } \alternative{{   e''4    e''4  } {   e''4    e''8    
fis''8  \bar "||"     g''4    fis''4  \bar "|"   e''8    b'8    b'8    b'16    
cis''16  \bar "|"   d''8    b'8    a'8    g'8  \bar "|"   fis'8    d'8    d'4  
\bar "|"     g''8    e''8    fis''8    d''8  \bar "|"   e''8    b'8    b'8    
a'8  \bar "|"   b'8    e''8    d''8    fis''8  \bar "|"   e''4    e''8    
fis''8  \bar "|"     g''4    fis''4  \bar "|"   e''8    b'8    b'8    b'16    
cis''16  \bar "|"   d''8    b'8    a'8    g'8  \bar "|"   fis'8    d'8    d'4  
\bar "|"     e'8    fis'8    g'8    a'8  \bar "|"   b'8    e''8    e''8    
fis''16    e''16  \bar "|"   g''8    e''8    a''8    fis''8  \bar "|"   e''4    
e''4  \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
