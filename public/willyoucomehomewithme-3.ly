\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kilcash"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1220#setting14523"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1220#setting14523" {"https://thesession.org/tunes/1220#setting14523"}}}
	title = "Will You Come Home With Me"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   b'8  \bar "|"   d''8    g''8    e''8    fis''8    
d''8    c''8  \bar "|"   b'4    d''8    c''8    a'8    fis'8  \bar "|"   g'4    
g'8    a'8    b'8    a'8  \bar "|"   g'8    fis'8    g'8    a'8    b'8    c''8  
\bar "|"   d''8    g''8    e''8    fis''8    d''8    c''8  \bar "|"   b'4    
d''8    c''8    a'8    fis'8  \bar "|"   g'4    g'8    a'8    b'8    fis'8  
\bar "|"   g'4.    g'4  }   b'8  \repeat volta 2 {   d''4    b'8    c''8    b'8 
   a'8  \bar "|"   d''8    c''8    b'8    c''8    a'8    fis'8  \bar "|"   g'4  
  g'8    a'8    b'8    a'8  \bar "|"   g'8    fis'8    g'8    a'8    b'8    
c''8  \bar "|"   d''4    b'8    c''8    b'8    a'8  \bar "|"   d''8    c''8    
b'8    c''8    a'8    fis'8  \bar "|"   g'4    g'8    a'8    b'8    fis'8  
\bar "|"   g'4.    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
