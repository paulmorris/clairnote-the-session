\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/34#setting26531"
	arranger = "Setting 16"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/34#setting26531" {"https://thesession.org/tunes/34#setting26531"}}}
	title = "Frieze Breeches, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \mixolydian     fis'8 ^"D"   e'8    d'8    e'8    fis'8    g'8 
   \bar "|"   a'8    d''8    d''8    c''8    a'8    g'8    \bar "|"   a'16    
a'16    a'8    a'8      b'8 ^"G"   a'8    g'8    \bar "|"     fis'8 ^"D"   a'8  
  fis'8      g'8 ^"A7"   fis'8    e'8    \bar "|"       fis'8 ^"D"   e'8    d'8 
   e'8    fis'8    g'8    \bar "|"   a'8    d''8    d''8    c''8    a'8    g'8  
  \bar "|"   a'16    a'16    a'8    a'8      g'8 ^"A"   e'8    a'8    \bar "|"  
   d'8 ^"D"   e'8    d'8    d'4.    }     \repeat volta 2 {     d''4 ^"D"   
e''8  \grace {    g''8  }   fis''8    e''8    d''8    \bar "|"     e''16 ^"C"   
e''16    e''8    d''8    c''8    a'8    g'8    \bar "|"     a'16 ^"D"   a'16    
a'8    a'8      b'8 ^"G"   a'8    g'8    \bar "|"     fis'8 ^"D"   a'8    fis'8 
     g'8 ^"A7"   fis'8    e'8    \bar "|"       d''4 ^"D"   e''8  \grace {    
g''8  }   fis''8    e''8    d''8    \bar "|"     e''16 ^"C"   e''16    e''8    
d''8    c''8    a'8    g'8    \bar "|"   a'16 ^"D"   a'16    a'8    a'8      
g'8 ^"A"   e'8    a'8    \bar "|"     d'8 ^"D"   e'8    d'8    d'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
