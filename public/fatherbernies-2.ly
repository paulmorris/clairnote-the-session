\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/995#setting14202"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/995#setting14202" {"https://thesession.org/tunes/995#setting14202"}}}
	title = "Father Bernie's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key c \major   \repeat volta 2 {   c''8    d''8    \bar "|"   e''8   
 c''8    b'8    c''8    e''8    c''8    b'8    c''8    \bar "|"   f''8    c''8  
  a'8    c''8    f''8    c''8    a'8    c''8    \bar "|"   fis''8    c''8    
a'8    c''8    fis''8    d''8    e''8    fis''8    \bar "|"   \tuplet 3/2 {   
g''8    a''8    g''8  }   fis''8    g''8    f''!8    d''8    b'8    d''8    
\bar "|"   e''8    c''8    g'8    e'8    f'4 ^"~"    d'8    c'8    \bar "|"   
b8    d'8    g'8    b'8    c''4 ^"~"    b'8    a'8    \bar "|"   g'8    b'8    
d''8    g''8    a''8    g''8    fis''8    g''8    } \alternative{{   e''8    
c''8    d''8    b'8    c''4    } {   e''8    c''8    g'8    e'8    c'4    } }   
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
