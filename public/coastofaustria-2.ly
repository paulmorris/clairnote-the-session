\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gam"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/692#setting20955"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/692#setting20955" {"https://thesession.org/tunes/692#setting20955"}}}
	title = "Coast Of Austria, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   cis''8    e''8    e''8    e''8    
fis''8    e''8    cis''8    e''8  \bar "|"   e''8    fis''8    fis''8    fis''8 
   e''8    cis''8    b'8    cis''8  \bar "|"   a'8    e''8    e''8    e''8    
fis''8    e''8    cis''8    e''8  \bar "|"   fis''8    a''8    a''8    gis''8   
 a''4    r4 \bar "|"     c'''8    b''8    a''8    c'''!8    b''8    a''8    
fis''4  \bar "|"   e''8    fis''8    fis''8    e''8    fis''8    a''8    a''4  
\bar "|"   e''8    e''8    cis''8    e''8    e''8    a''8    a''8    fis''8  
\bar "|"   e''8    cis''8    b'8    cis''8    a'4.    b'8  }     
\repeat volta 2 {   cis''8    cis''8    cis''8    b'8    a'8    fis'8    fis'8  
  fis'8  \bar "|"   e'8    e'8    e'8    fis'8    a'8    a'8    a'8    b'8  
\bar "|"   cis''8    e''8    e''8    e''8    fis''8    e''8    cis''8    e''8  
\bar "|"   fis''8    a''8    a''8    gis''8    a''4    r4 \bar "|"     c'''8    
b''8    a''8    c'''!8    b''8    a''8    fis''4  \bar "|"   e''8    fis''8    
fis''8    e''8    fis''8    a''8    a''4  \bar "|"   e''8    e''8    cis''8    
e''8    e''8    a''8    a''8    fis''8  \bar "|"   e''8    cis''8    b'8    
cis''8    a'4.    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
