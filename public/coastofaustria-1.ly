\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Brendan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/692#setting692"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/692#setting692" {"https://thesession.org/tunes/692#setting692"}}}
	title = "Coast Of Austria, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   b'8    \repeat volta 2 {   cis''8    e''8    e''4 
^"~"    fis''8    e''8    cis''8    e''8    \bar "|"   fis''4. ^"~"    a''8    
e''8    a'8    cis''8    a'8    \bar "|"   cis''8    e''8    e''4 ^"~"    
fis''8    e''8    cis''8    e''8    \bar "|"   fis''8    a''8    a''8    gis''8 
   a''4    a''8    b''8    \bar "|"       cis'''8 ^"See Var \#1"   b''8    a''8 
   cis'''8    b''8    a''8    fis''8    a''8  \bar "|"   fis''4. ^"~"    e''8   
 fis''4    a''8    fis''8  \bar "|"   e''8 ^"See Var \#2"   a'8    cis''8    
e''8    fis''4. ^"~"    a''8  \bar "|"   e''8    cis''8    b'8    cis''8    a'4 
   a'8    b'8  }     \repeat volta 2 {   cis''4    b'8    cis''8    a'8    
fis'8    fis'4 ^"~"    \bar "|"   e'8    cis''8    e'8    fis'8    a'4    a'8   
 b'8  \bar "|"   cis''8    e''8    e''4 ^"~"    fis''8    e''8    cis''8    
e''8    \bar "|"   fis''8    a''8    a''8    gis''8    a''4    a''8    b''8    
\bar "|"     cis'''8    b''8    a''8    cis'''8    b''8    a''8    fis''8    
a''8  \bar "|"   fis''4. ^"~"    e''8    fis''4    a''8    fis''8  \bar "|"   
e''8    a'8    cis''8    e''8    fis''4. ^"~"    a''8  \bar "|"   e''8    
cis''8    b'8    cis''8    a'4    a'8    b'8  }     \bar "||"   cis'''4 
^"Var \#1"   cis'''4    b''8    a''8    fis''8    a''8    \bar "||"   e''8 
^"Var \#2"   a'8    cis''8    e''8    fis''4    fis''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
