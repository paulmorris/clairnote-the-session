\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/745#setting745"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/745#setting745" {"https://thesession.org/tunes/745#setting745"}}}
	title = "Katty's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   e''8  \bar "|"   fis''8    e''8    d''8    e''8    
d''8    b'8  \bar "|"   a'4. ^"~"    a'8    fis'8    a'8  \bar "|"   b'8    g'8 
   b'8    b'8    d''8    b'8  \bar "|"   a'8    fis'8    a'8    a'8    d''8    
e''8  \bar "|"     fis''8    e''8    d''8    e''8    d''8    b'8  \bar "|"   
a'4. ^"~"    a'8    fis'8    a'8  \bar "|"   b'8    cis''8    d''8    a'8    
d''8    fis''8  \bar "|"   g''8    e''8    cis''8    d''4  }     
\repeat volta 2 {   e''8  \bar "|"   fis''8    d''8    d''8    a''8    d''8    
d''8  \bar "|"   fis''4. ^"~"    fis''8    e''8    d''8  \bar "|"   a'4. ^"~"   
 e''4. ^"~"  \bar "|"   g''8    fis''8    e''8    d''8    b'8    a'8  \bar "|"  
   fis''8    a'8    a'8    a''8    a'8    a'8  \bar "|"   fis''4. ^"~"    
fis''8    e''8    d''8  \bar "|"   b'8    cis''8    d''8    a'8    d''8    
fis''8  \bar "|"   g''8    e''8    cis''8    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
