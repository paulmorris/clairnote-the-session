\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/23#setting12403"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/23#setting12403" {"https://thesession.org/tunes/23#setting12403"}}}
	title = "Dingle Regatta, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key g \major   \repeat volta 2 {   b'8    \bar "|"   d''8    cis''8 
   d''8    e''4    d''8    b'8    ais'8    b'8    d''4    b'8    \bar "|"   a'4 
   a'8    a'8    gis'8    a'8    b'4    a'8    g'!4    b'8    \bar "|"     d''8 
   cis''8    d''8    e''4.    b'8    ais'8    b'8    d''4.    \bar "|"   a'8    
a'16    b'16    c''8    b'4    a'8    g'4.    g'4    }     \repeat volta 2 {   
b'8    \bar "|"   d''4    d''8    d''8    e''8    fis''8    g''4    g''8    
g''8    fis''8    g''8    \bar "|"   a''4    a''8    a''8    gis''8    a''8    
b''4    a''8    g''!8    g''16    fis''16    e''8    \bar "|"     d''4.   ~    
d''8    e''8    fis''8    g''8    a''8    g''8    g''8    fis''8    g''8    
\bar "|"   a''4    g''8    fis''4    e''8    d''8    d''16    e''16    fis''8   
 g''4    }     d''8    \bar "|"   g''8    b''8    g''8    d''8    g''8    d''8  
  b'8    d''8    b'8    g'4.    \bar "|"   fis'8    fis'16    g'16    a'8    
d'8    e'8    fis'8    g'4    b'8    d''8    cis''8    d''8    \bar "|"     
g''4. -.   d''4. -.   b'4. -.   g'4. -.   \bar "|"   fis'8    g'8    a'8    d'4 
   fis'8    g'4.    g'8    b'16    c''16    d''8    \bar "|"     g''16    a''16 
   b''8    g''8    d''4.    b'16    c''16    d''8    b'8    g'4.    \bar "|"   
fis'4    a'8    d'8    c'8    d'8    g'4    b'8    d''8    d''16    e''16    
fis''8    \bar "|"     g''8    b''8    g''8    d''8    c''8    d''8    b'8    
ais'8    b'8    g'8    fis'8    g'8    \bar "|"   fis'8    g'8    a'8    d'8    
e'8    fis'8    g'4.   ~    g'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
