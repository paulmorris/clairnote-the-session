\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "cj"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/905#setting905"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/905#setting905" {"https://thesession.org/tunes/905#setting905"}}}
	title = "Princess Royal, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \minor   g'8    a'8  \repeat volta 2 {     bes'4 ^"Gm"   a'8   
 bes'8    g'4    d''8    c''8  \bar "|"   bes'4 ^"Gm"   a'8    bes'8    g'4    
d''4  \bar "|"   ees''4 ^"Cm"   ees''8    d''8      c''4 ^"F7"   f''8    ees''8 
 \bar "|"   d''8 ^"Bb"   ees''8      c''8 ^"F"   d''8      bes'4 ^"Gm" 
\grace {    c''8  }   bes'8    a'8  \bar "|"       bes'4 ^"Gm"   g''8    bes'8  
    a'4 ^"Dm"   f''8    a'8  \bar "|"   g'8 ^"Eb"   a'8    bes'8    g'8      
d'4 ^"D7"   d''8    c''8  \bar "|"   bes'8 ^"Gm"   a'8    bes'8    g'8      d'4 
^"D7" <<   c'4    fis'4   >> \bar "|" <<   bes2 ^"Gm"   g'2   >> <<   bes4    
g'4   >> }     d''4  \bar "|"   g''4 ^"Gm"   g''8    fis''8    \grace {    a''8 
^"D7" }   g''8    fis''8    g''8    a''8  \bar "|"   bes''4 ^"Bb"   bes'4    
bes'4    bes''8    a''8  \bar "|"   bes''8 ^"Gm"   a''8    g''8    f''8      
ees''8 ^"Cm"   d''8    c''8    bes'8  \bar "|"   a'8 ^"F"   bes'8    c''8    
a'8      f'4 ^"F7"   g'8    a'8  \bar "|"     <<   f'4 ^"Bb"   bes'4   >> a'8   
 bes'8    <<   f'4 ^"F"   c''4   >> bes'8    c''8  \bar "|" <<   f'4 ^"Bb"   
d''4   >> <<   f'4    d''4   >>   <<   g'2 ^"Eb"   bes'2    g''2   >> \bar "|" 
<<   f'4 ^"Bb"   bes'4    f''4   >> <<   f'4    bes'4   >>   <<   f'2 ^"F7"   
a'2    ees''2   >> \bar "|" <<   f'4 ^"Bb"   bes'4    d''4   >>     g'4 ^"Gm"   
<<   ees'2 ^"Adim"   g'2    c''2   >> \bar "|"       bes'8 ^"Gm"   c''8    
bes'8    a'8    g'8    a'8    bes'8    g'8  \bar "|"   fis'8 ^"D"   g'8    a'8  
  fis'8      d'4 ^"D7"   d''8    c''8  \bar "|"   bes'8 ^"Gm"   a'8    bes'8    
g'8      d'4 ^"D7" <<   c'4    fis'4   >> \bar "|" <<   bes2 ^"Gm"   g'2   >> 
<<   bes4    g'4   >> \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
