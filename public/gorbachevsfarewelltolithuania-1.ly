\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kerri Coombs"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/254#setting254"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/254#setting254" {"https://thesession.org/tunes/254#setting254"}}}
	title = "Gorbachev's Farewell To Lithuania"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   des''8  \bar "||"   d''8    d'8    \tuplet 3/2 {   d'8 
   d'8    d'8  }   f'8    e'8    d'8    f'8  \bar "|"   e'4    des'8    e'8    
bes8    e'8    des'8    e'8  \bar "|"   d''8    d'8    \tuplet 3/2 {   d'8    
d'8    d'8  }   f'8    e'8    d'8    f'8  \bar "|"   e'8    g'8    f'8    e'8   
 f'8    d'8    d'8    des''8  \bar "|"     d''8    d'8    \tuplet 3/2 {   d'8    
d'8    d'8  }   f'8    e'8    d'8    f'8  \bar "|"   e'4    des'8    e'8    
bes8    e'8    des'8    e'8  \bar "|"   ees'4    c''8    ees'8    d''8    ees'8 
   c''8    ees'8  \bar "|"   e'4    des'8    e'8    f'8    d'!8    d'8    
des''8  \bar "|"     d''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   f'8   
 e'8    d'8    f'8  \bar "|"   e'4    des'8    e'8    bes8    e'8    des'8    
e'8  \bar "|"   d''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   f'8    e'8 
   d'8    f'8  \bar "|"   e'8    g'8    f'8    e'8    f'8    d'8    d'8    
des''8  \bar "|"     d''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   c''8  
  d'8    bes'8    d'8  \bar "|"   a'8    d'8    \tuplet 3/2 {   d'8    d'8    
d'8  }   e'8    d'8    des'8    d'!8  \bar "|"   ees'4    c''8    ees'8    d''8 
   ees'8    c''8    ees'8  \bar "|"   e'4    des'8    e'8    f'8    d'!8    d'8 
   des''8  \bar "|"     \repeat volta 2 {   d''8    f''8    a''8    f''8    
des''8    f''8    a''8    f''8  \bar "|"   c''8    f''8    a''8    f''8    g''8 
   f''8    e''8    g''8  \bar "|"   f''4    d''8    f''8    des''8    f''8    
c''8    f''8  \bar "|"   d''8    f''8    a''8    f''8    g''4    f''8    g''8  
\bar "|"     a''8    f''8    g''8    e''8    f''8    d''8    e''8    d''8  
\bar "|"   des''8    a''8    b'8    a''8    des''8    a''8    a'8    a''8  
\bar "|"   d''8    a'8    fis'8    a'8    b'4    g'8    b'8  } \alternative{{   
a'8    cis''8    e''8    g''8    fis''8    d''8    d''8    cis''!8  } {   a'8   
 cis''8    e''8    g''8    fis''8    d''8    d''4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
