\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "DonaldK"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/603#setting22373"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/603#setting22373" {"https://thesession.org/tunes/603#setting22373"}}}
	title = "Kitchen Girl, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \mixolydian   a''4    a''8    a''8 (   g''4  -)   e''4   ~    
\bar "|"   e''4    e''8    d''8 (   cis''4  -)   cis''8    d''8  \bar "|"   
e''4.    fis''8 (   g''8    fis''8  -)   g''8    a''8 ( \bar "|"   b''8    a''8 
 -)   g''8    fis''8 (   e''8    fis''8  -)   g''8    e''8  \bar "|"     a''16  
  a''16    a''8    a''4 (   g''4  -)   g''4 ( \bar "|"   e''8  -)   fis''8    
e''8    d''8 (   cis''4  -)   cis''8    d''8 ( \bar "|"   cis''8    b'8  -)   
a'8    cis''8 (   b'8    a'8  -)   g'8    g'8  \bar "|"   a'4.    a'8   ~    
a'4    a'8    a'8  }   \key a \minor   a'8    r8   c''8    r8 b'2 ( \bar "|"   
a'8  -)   b'8    a'8    g'8 (   e'4  -)   a'4   ~    \bar "|"   a'4    b'4 (   
c''4  -)   d''8    d''8  \bar "|"   e''4.    f''8 (   e''8    d''8  -)   c''8   
 b'8 ( \bar "|"     a'4  -)   a'8    a'8 (   b'4  -)   b'8    b'8 ( \bar "|"   
a'8  -)   b'8    a'8    g'8 (   e'4  -)   a'8    b'8 ( \bar "|"   c''8    b'8  
-)   a'8    c''8 (   b'8    a'8  -)   g'8    g'8  \bar "|"   a'4.    a'8   ~    
a'4    e'8    a'8  \bar "|"     a'8 -.   a'8 -.   a'8 -.   c''8 -.   c''8 -.   
b'8 -.   b'8 -.   b'8 -. \bar "|"   a'8 -.   b'8 -.   a'8 -.   g'8 -.   e'8 -.  
 e'8 -.   e'8 -.   a'8 -. \bar "|"   a'8 -.   a'8 -.   b'8 -.   b'8 -.   c''8 
-.   c''8 -.   d''8 -.   d''8 -. \bar "|"   e''8 -.   e''8 -.   e''8 -.   f''8 
-.   e''8 -.   d''8 -.   c''8 -.   b'8 -. \bar "|"     a'8 -.   a'8 -.   a'8 -. 
  a'8 -.   b'8 -.   b'8 -.   b'8 -.   b'8 -. \bar "|"   a'8 -.   b'8 -.   a'8 
-.   g'8 (   e'8  -)   r8 a'8    b'8 ( \bar "|"   c''8    b'8  -)   a'8    c''8 
(   b'8    a'8  -)   g'8    g'8  \bar "|"   a'2    a'4    a'8    a'8  \bar "||" 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
