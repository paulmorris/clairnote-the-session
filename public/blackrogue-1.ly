\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1076#setting1076"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1076#setting1076" {"https://thesession.org/tunes/1076#setting1076"}}}
	title = "Black Rogue, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian   \bar "|"   cis''8    a'8    a'8    b'8    g'8    
g'8    \bar "|"   cis''8    a'8    a'8    a'8    b'8    d''8    \bar "|"   
cis''8    a'8    a'8    b'8    a'8    g'8    \bar "|"   a'8    fis'8    d'8    
d'4    b'8  \bar "|"     cis''8    a'8    a'8    b'8    g'8    g'8    \bar "|"  
 cis''8    a'8    a'8    a'8    fis'8    d'8    \bar "|"   g'4    a'8    b'16 ( 
  cis''16    d''8  -)   b'8    \bar "|"   a'8    fis'8    d'8    d'4    b'8    
\bar ":|."   a'8    fis'8    d'8    d'4.    \bar "||"     \bar ".|:"   fis''4. 
^"~"    g''4. ^"~"    \bar "|"   a''8    fis''8    d''8    cis''8    b'8    a'8 
 \bar "|"   fis''4. ^"~"    g''8    fis''8    g''8    \bar "|"   a''8    fis''8 
   d''8    d''8    fis''8    g''8    \bar "|"     a''8    g''8    fis''8    
g''8    fis''8    e''8    \bar "|"   fis''8    e''8    d''8    e''16    fis''16 
   e''8    d''8    \bar "|"   cis''8    b'8    a'8    b'8    a'8    g'8    
\bar "|"   a'8    fis'8    d'8    d'4.    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
