\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Earl Adams"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/948#setting22047"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/948#setting22047" {"https://thesession.org/tunes/948#setting22047"}}}
	title = "Kitty Lie Over"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   a'8    fis'8    d'8    d'8    fis'8    a'8    
\bar "|"   a'8    d''8    d''8    b'4    a'8  \bar "|"   a'8    b'8    a'8    
fis'4.  \bar "|"   g'8    fis'8    g'8    e'4    b'8  \bar "|"     a'8    fis'8 
   d'8    d'8    fis'8    a'8    \bar "|"   a'8    d''8    d''8    b'4    a'8  
\bar "|"   a'8    b'8    a'8    fis'4    e'8  \bar "|"   e'8    d'8    d'8    
d'4.  \bar "|"     fis''8    d''8    d''8    e''8    d''8    e''8    \bar "|"   
fis''8    d''8    d''8    d''4    g''8  \bar "|"   fis''8    d''8    d''8    
d''8    e''8    fis''8  \bar "|"   g''8    fis''8    g''8    e''4    g''8  
\bar "|"     fis''8    e''8    d''8    b'4.    \bar "|"   a'8    d''8    fis'8  
  g'4.  \bar "|"   a'8    b'8    a'8    fis'4    e'8  \bar "|"   e'8    d'8    
d'8    d'4.  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
