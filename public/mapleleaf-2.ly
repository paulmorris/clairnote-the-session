\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Edgar Bolton"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1050#setting21229"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1050#setting21229" {"https://thesession.org/tunes/1050#setting21229"}}}
	title = "Maple Leaf, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {     e'4 ^"Em"   e'8    fis'8    
g'8    e'8    e'4 ^"~"    \bar "|"     b'4 ^"G"   a'8    b'8      a'8 ^"Am"   
g'8    e'8    d'8    \bar "|"     e'4 ^"Em"   e'8    fis'8    g'8    fis'8    
g'8    a'8  \bar "|"     b'4 ^"G"   d''8    b'8      a'8 ^"Am"   g'8    e'8    
d'8    \bar "|"       e'4 ^"Em"   e'8    fis'8    g'8    e'8    e'4 ^"~"    
\bar "|"     b'4 ^"G"   a'8    b'8      a'8 ^"Am"   g'8    e'8    d'8    
\bar "|"     b'8 ^"Em"   e''8    e''4 ^"~"      b'8 ^"G"   a'8    g'8    b'8    
\bar "|"     c''8 ^"Am"   a'8    b'8    g'8      a'8 ^"C"   g'8    e'8    d'8   
 }     \repeat volta 2 {     b'8 ^"Em"   e''8    e''4 ^"~"    e''8    g''8    
g''4 ^"~"  \bar "|"   b'8 ^"Em"   e''8    e''4 ^"~"    e''8    d''8    b'8    
a'8    \bar "|"   b'8 ^"Em"   e''8    e''4 ^"~"    e''8    g''8    g''4 ^"~"  
\bar "|"     a''4. ^"D"^"~"    e''8      fis''8 ^"Bm"   e''8    d''8    a'8    
\bar "|"       b'8 ^"Em"   e''8    e''4 ^"~"    e''8    g''8    g''4 ^"~"  
\bar "|"   b'8 ^"Em"   e''8    e''4 ^"~"    e''8    d''8    b'8    a'8    
\bar "|"     a''8 ^"Am"   e''8    e''4 ^"~"      fis''8 ^"Bm"   e''8    d''8    
b'8    \bar "|"   c''8 ^"Am"   a'8    b'8    g'8      a'8 ^"C"   g'8    e'8    
d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
