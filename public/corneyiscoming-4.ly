\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "benhockenberry"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/943#setting24209"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/943#setting24209" {"https://thesession.org/tunes/943#setting24209"}}}
	title = "Corney Is Coming"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \minor   d'8    d''8    d''8    cis''8    d''8    e''8    f''8 
   d''8  \bar "|"   c''8    bes'8    a'8    g'8    f'8    g'8    e'8    c'8  
\bar "|"   d'8    d''8    d''8    cis''8    d''8.    e''16    f''8.    g''16  
\bar "|"   e''8    d''8    cis''8    e''8    d''16    d''16    d''8    d''4    
}     d''8    e''8    f''8    g''8    a''8    f''8    d''8    g''8  \bar "|"   
e''8.    d''16    c''8    g''8    e''8    c''8    g''8    e''8  \bar "|"   d''8 
   e''8    f''8    g''8    a''8    f''8    d''8    g''8  \bar "|"   e''8    
c''8    g''8    e''8    d''16    d''16    d''8    d''4  \bar "|"     d''8    
e''8    f''8    g''8    a''8    f''8    d''8    g''8  \bar "|"   e''8.    d''16 
   c''8    g''8    e''8    c''8    g''8    e''8  \bar "|"   d''8    e''8    
f''8    g''8    a''8    f''8    g''8    f''8  \bar "|"   e''8    c''8    g''8   
 e''8    d''16    d''16    d''8    d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
