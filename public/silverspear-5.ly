\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jdogbishop96"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/182#setting20993"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/182#setting20993" {"https://thesession.org/tunes/182#setting20993"}}}
	title = "Silver Spear, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8 ^"D"   a'8  \grace {    b'8 
   cis''8    b'8  }   a'4  \grace {    a'8  }   b'8    a'8    fis'8    a'8  
\bar "|" \grace {    a'8  }   b'8    a'8    fis'8    a'8    e''8    cis''8    
d''8    a'8  \bar "|"   fis'8    a'8  \grace {    b'8    cis''8    b'8  }   a'4 
 \grace {    a'8  }   b'8    a'8    fis'8    a'8  \bar "|" \grace {    cis''8 
^"G" }   d''8    fis''8    e''8    d''8    b'8    cis''8    d''8    a'8  
\bar "|"       fis'8 ^"D"   a'8  \grace {    b'8    cis''8    b'8  }   a'4  
\grace {    a'8  }   b'8    a'8    fis'8    a'8  \bar "|" \grace {    a'8  }   
b'8    a'8    fis'8    a'8    d''4    e''8    fis''8  \bar "|" \grace {    
fis''8 ^"G" }   g''4    fis''8    e''8    fis''4    fis''8    e''8  \bar "|"   
d''8    fis''8    e''8    d''8  \grace {    a'8  }   b'4    a'8  <<   g'8    
d''8   >> }     \repeat volta 2 {   |
 fis''8 ^"D"   a''8    \tuplet 3/2 {   a''8    g''8    fis''8  }   a''8    
fis''8    fis''8    fis''8  \bar "|" \grace {    fis''8 ^"G" }   g''8    e''8   
 fis''8    d''8    e''8    d''8    cis''8    d''8  \bar "|"   fis''8 ^"D"   
a''8    \tuplet 3/2 {   a''8    g''8    fis''8  }   a''4    fis''8    a''8  
\bar "|"   g''8 ^"G"   fis''8    e''8    d''8    \tuplet 3/2 {   b'8    d''8    
b'8  }   a'8    g'8  \bar "|"       fis''8 ^"D"   a''8    a''8  \grace {    
b''8    a''8    g''8    fis''8  }   g''8    a''4    fis''8    a''8  \bar "|"   
g''8 ^"G"   fis''8    e''8    d''8    b'8    d''8    e''8    fis''8  \bar "|" 
\grace {    fis''8  }   g''4    fis''8    e''8    fis''4    fis''8    e''8  
\bar "|"   d''8    fis''8    e''8    d''8  \grace {    a'8  }   b'4    a'8  <<  
 g'8    d''8   >> }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
