\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/710#setting27807"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/710#setting27807" {"https://thesession.org/tunes/710#setting27807"}}}
	title = "Queen Of The Rushes, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \mixolydian   \repeat volta 2 {   a'16    g'16  \bar "|"   
fis'4    d'8    d'4.  \bar "|"   g'4    e'8    e'4.  \bar "|"   fis'4    d'8    
d'4.  \bar "|"   g'8    e'8    d'8    d'8    a'8    g'8  \bar "|"     fis'4    
d'8    fis'4    d'8  \bar "|"   g'4    e'8    e'4.  \bar "|"   fis'8    a'8    
fis'8    g'8    e'8    a'8  \bar "|"   d'4.    d'4  }     \repeat volta 2 {   
|
 c''4    a'8    b'8    c''8    d''8  \bar "|"   e''8    c''8    a'8    a'8    
g'8    e'8  \bar "|"   d''4    e''8    fis''4    d''8  \bar "|"   e''8    c''8  
  a'8    a'8    g'8    e'8  \bar "|"     c''4    a'8    b'8    c''8    d''8  
\bar "|"   e''8    c''8    a'8    a'8    g'8    e'8  \bar "|"   fis'8    a'8    
fis'8    g'8    e'8    a'8  \bar "|"   d'4.    d'4.  }     \repeat volta 2 {   
|
 fis'16    g'16  \bar "|"   a'4.    d''8    a'8    fis'8  \bar "|"   a'4.    
d''8    a'8    fis'8  \bar "|"   g'4.    e'8    g'8    e'8  \bar "|"   g'8    
b'8    g'8    e'8    fis'8    g'8  \bar "|"     a'4.    d''8    a'8    fis'8  
\bar "|"   a'4.    d''8    a'8    g'8  \bar "|"   fis'8    a'8    fis'8    g'8  
  e'8    a'8  \bar "|"   d'4.    d'4    }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
