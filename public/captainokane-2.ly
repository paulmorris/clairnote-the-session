\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/858#setting14024"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/858#setting14024" {"https://thesession.org/tunes/858#setting14024"}}}
	title = "Captain O'Kane"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \minor   \repeat volta 2 {   g'16    f'16    \bar "|"   d'8    
bes'8    a'8    bes'4    a'16    g'16    \bar "|"   a'16    bes'16    c''16    
bes'16    a'16    g'16    f'8    g'8    a'8    \bar "|"   bes'8    d''8    
bes'8    c''8    bes'16    a'16    g'16    f'16    \bar "|"   d'8    g'8    g'8 
   g'4    }     \repeat volta 2 {   g'16    a'16    \bar "|"   bes'8    d''8    
d''8    d''4    c''16    bes'16    \bar "|"   a'8    c''8    c''8    c''4    
f''8    \bar "|"   d''8.    g''16    fis''8    g''8.    a''16    bes''8    
\bar "|"   d''8    g''8    fis''8    g''4    g''16    a''16    \bar "|"     
bes''8    a''8    g''8    f''8.    ees''16    d''8    \bar "|"   d''16    c''16 
   bes'16    a'16    f''8    f'8    g'8    a'8    \bar "|"   bes'8    d''8    
bes'8    d''16    c''16    bes'16    a'16    g'16    f'16    \bar "|"   d'8    
g'8    g'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
