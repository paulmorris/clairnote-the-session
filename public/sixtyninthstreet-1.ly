\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Caoimghgin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/679#setting679"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/679#setting679" {"https://thesession.org/tunes/679#setting679"}}}
	title = "Sixty Ninth Street"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key e \dorian   e'8.    fis'16    g'8    a'8    \bar "|"   b'8    
e''8    b'4  \bar "|"   a'8.    fis'16    d'8    fis'8  \bar "|"   a'8    fis'8 
   d'8    fis'8  \bar "|"     e'8.    fis'16    g'8    a'8    \bar "|"   b'8    
e''8    b'4  \bar "|"   a'8.    fis'16    d'8    fis'8  \bar "|"   e'4    e'4  
\bar "|"     e'8.    fis'16    g'8    a'8    \bar "|"   b'8    e''8    b'4  
\bar "|"   a'8.    fis'16    d'8    fis'8  \bar "|"   a'8    fis'8    d'8    
fis'8  \bar "|"     e'8.    fis'16    g'8    a'8    \bar "|"   b'8    e''8    
b'4  \bar "|"   a'8.    fis'16    d'8    fis'8  \bar "|"   e'4    e'4  \bar "|" 
    e''8    e''8    b'4  \bar "|"   e''8    e''8    b'4  \bar "|"   a'8.    
fis'16    d'8    fis'8  \bar "|"   a'8    fis'8    d'8    fis'8  \bar "|"     
e''8    e''8    b'4  \bar "|"   e''8    e''8    b'4  \bar "|"   a'8.    fis'16  
  d'8    fis'8  \bar "|"   e'4    e'4  \bar "|"     e''8    e''8    b'4  
\bar "|"   e''8    e''8    b'4  \bar "|"   a'8.    fis'16    d'8    fis'8  
\bar "|"   a'8    fis'8    d'8    fis'8  \bar "|"     e'8.    fis'16    g'8    
a'8    \bar "|"   b'8    e''8    b'4  \bar "|"   a'8.    fis'16    d'8    fis'8 
 \bar "|"   e'4    e'4  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
