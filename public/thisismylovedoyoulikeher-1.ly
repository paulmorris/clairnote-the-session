\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/6#setting6"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/6#setting6" {"https://thesession.org/tunes/6#setting6"}}}
	title = "This Is My Love, Do You Like Her?"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   \repeat volta 2 {   e'4 ^"Em"   e'8    e'8    d'8    
b8  \bar "|"   g'4. ^"G"   g'8    a'8    b'8  \bar "|"   a'4 ^"D"   a'8    a'8  
  b'8    a'8  \bar "|"   d''4.    d''4    e''8  \bar "|"     d''4    b'8    g'8 
   a'8    b'8  \bar "|"   a'4    fis'8    d'8    e'8    fis'8  \bar "|"   g'4 
^"G"   e'8      fis'8 ^"D"   e'8    d'8  \bar "|"   e'4. ^"Em"   e'4.  }     
\repeat volta 2 {   e''4 ^"Em"   e''8    e''8    d''8    b'8  \bar "|"   d''4. 
^"D"   d''8    e''8    fis''8  \bar "|"   e''4 ^"Em"   e''8    e''8    d''8    
b'8  \bar "|"   d''4. ^"D"   d''4    e''8  \bar "|"     d''4    b'8    g'8    
a'8    b'8  \bar "|"   a'4    fis'8    d'8    e'8    fis'8  \bar "|"   g'4 ^"G" 
  e'8      fis'8 ^"D"   e'8    d'8  \bar "|"   e'4. ^"Em"   e'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
