\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Gallopede"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/591#setting13595"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/591#setting13595" {"https://thesession.org/tunes/591#setting13595"}}}
	title = "Pinch Of Snuff, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 {   fis''2.    e''4    \bar "|"   
d''2.    b'4    \bar "|"   a'2.    fis'4    \bar "|"   d'4    fis'4    a'4    
fis'4    \bar "|"   d'4    fis'4    a'4    b'4    \bar "|"   c''2.    a'4    
\bar "|"   g'4    e'4    e'2    \bar "|"   c''4    e'4    g'4    e'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
