\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Larke"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1283#setting21344"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1283#setting21344" {"https://thesession.org/tunes/1283#setting21344"}}}
	title = "Flowers Of Spring, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \dorian   d'8    e'8    d'8    d'8    e'8    g'8    \bar "|"   
a'8    g'8    e'8    c'4    e'8    \bar "|"   g'8    e'8    e'8    c''8    e'8  
  e'8    \bar "|"   c'8    e'8    g'8    a'8    b'8    c''8    \bar "|"     d'8 
   e'8    d'8    d'8    e'8    g'8    \bar "|"   a'8    g'8    e'8    c'4    
e'8    \bar "|"   g'8    e'8    e'8    c''8    g'8    e'8    \bar "|"   d'8    
e'8    d'8    a8    b8    c'8    \bar ":|."   d'8    e'8    d'8    d'4.    
\bar "||"     a'4    d''8    d''8    c''8    d''8    \bar "|"   e''8    f''8    
e''8    d''8    c''8    a'8    \bar "|"   \tuplet 3/2 {   a'8    b'8    c''8  }  
 a'8    c'8    e'8    g'8    \bar "|"   a'8    g'8    a'8    c''8    a'8    g'8 
   \bar "|"     a'4    d''8    d''8    c''8    d''8    \bar "|"   e''8    f''8  
  e''8    d''8    c''8    a'8    \bar "|"   \tuplet 3/2 {   a'8    b'8    c''8  
}   a'8    g'8    e'8    c'8    \bar "|"   e'8    d'8    c'8    d'4.    
\bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
