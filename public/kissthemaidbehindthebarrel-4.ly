\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Manu Novo"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/676#setting13725"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/676#setting13725" {"https://thesession.org/tunes/676#setting13725"}}}
	title = "Kiss The Maid Behind The Barrel"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   d'8    g'8    g'4 ^"~"    a'8    
g'8    g'8    fis'8    \bar "|"   d'8    g'8    g'4 ^"~"    c''8    a'8    
fis'8    a'8    \bar "|"   d'8    g'8    g'4 ^"~"    a'8    g'8    fis'8    a'8 
   \bar "|"   d''8    e''8    fis''8    d''8    c''8    a'8    fis'8    a'8    
}   \bar "|"   d''8    g''8    g''4 ^"~"    a''8    g''8    g''8    fis''8    
\bar "|"   d''8    g''8    g''4 ^"~"    a''8    g''8    fis''8    e''8    
\bar "|"   d''8    fis''8    fis''4 ^"~"    a''8    fis''8    fis''4 ^"~"    
\bar "|"   d''8    e''8    fis''8    d''8    c''8    a'8    fis'8    a'8    
\bar "|"   \bar "|"   d''8    g''8    g''4 ^"~"  \bar "|"   a''8    g''8    
fis''8    g''8    \bar "|"   a''8    b''8    a''8    g''8    fis''4. ^"~"    
g''8    \bar "|"   a''8    fis''8    \tuplet 3/2 {   g''8    fis''8    e''8  }   
fis''8    d''8    e''8    cis''8    \bar "|"   d''8    fis''8    e''8    g''8   
 fis''8    d''8    c''8    a'8    \bar "||"   \bar "|"   g'4    d''8    g'8    
a'8    g'8    d''8    g'8    \bar "|"   g'4 ^"~"    d''8    b'8    c''8    a'8  
  fis'8    a'8    \bar "|"   g'4. ^"~"    a'8    b'4. ^"~"    c''8    \bar "|"  
 d''8    e''8    fis''8    d''8    c''8    a'8    fis'8    a'8    \bar "|"   
\bar "|"   g'4    d''8    g'8    a'8    g'8    d''8    g'8    \bar "|"   g'4 
^"~"    d''8    b'8    c''8    a'8    fis'8    a'8    \bar "|"   b'4. ^"~"    
g'8    a'4. ^"~"    b'8    \bar "|"   d''8    e''8    fis''8    d''8    c''8    
a'8    fis'8    a'8    \bar "||"   \repeat volta 2 {   b'4. ^"~"    g'8    a'4. 
^"~"    g'8    \bar "|"   b'4. ^"~"    c''8    d''8    e''8    d''8    c''8    
\bar "|"   b'4. ^"~"    g'8    a'8    g'8    fis'8    a'8    \bar "|"   d''8    
e''8    fis''8    d''8    c''8    a'8    fis'8    a'8    }   \bar "||"   b'8    
d'8    g'8    b'8    a'8    d'8    fis'8    a'8    \bar "|"   b'8    d'8    g'8 
   b'8    d''8    e''8    d''8    c''8    \bar "|"   b'8    d'8    g'8    b'8   
 a'8    g'8    fis'8    a'8    \bar "|"   d''8    e''8    fis''8    d''8    
c''8    a'8    fis'8    a'8    \bar "|"   \bar "|"   b'8    b'8    g'8    a'8   
 a'8    g'8    b'8    b'8    \bar "|"   g'8    a'8    a'8    b'8    d''8    
e''8    d''8    c''8    \bar "|"   b'8    b'8    b'8    g'8    a'8    g'8    
fis'8    a'8    \bar "|"   d''8    e''8    fis''8    d''8    c''8    a'8    
fis'8    a'8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
