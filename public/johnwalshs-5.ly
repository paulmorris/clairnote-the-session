\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thady Quill"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/329#setting28208"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/329#setting28208" {"https://thesession.org/tunes/329#setting28208"}}}
	title = "John Walsh's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g''4    g''8    fis''16    g''16  
  \bar "|"   a''8    g''8    g''8    e''8    \bar "|"   d''8    b'8    b'8    
a'8    \bar "|"   g'8    a'8    b'8    d''8    \bar "|"     \bar "|"   g''4    
g''8    fis''16    g''16    \bar "|"   a''8    g''8    g''8    e''8    \bar "|" 
  d''8    b'8    b'16    d''16    b'16    a'16    \bar "|"   g'4    g'4    }    
 \repeat volta 2 {   g'8.    a'16    b'8    a'8    \bar "|"   g'8    e'8    e'8 
   d'8    \bar "|"   b8    d'8    e'8    d'8    \bar "|"   b8    d'8    e'8    
d'8    \bar "|"     \bar "|"   g'8.    a'16    b'8    a'8    \bar "|"   g'8    
e'8    e'8    d'8    \bar "|"   b8    d'8    e'8    d'8    \bar "|"   g'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
