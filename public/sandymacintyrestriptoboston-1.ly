\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Redbird"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/375#setting375"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/375#setting375" {"https://thesession.org/tunes/375#setting375"}}}
	title = "Sandy MacIntyre's Trip To Boston"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   \repeat volta 2 {   fis'8  \bar "|"   e'4    cis'8    
e'8    a8    e'8    cis'8    e'8    \bar "|"   a'8    cis''8    e''8    fis''8  
  e''8    cis''8    cis''8    e''8    \bar "|"   fis''4    a''8    fis''8    
e''8    cis''8    a'8    cis''8    \bar "|"   d''8    b'8    cis''8    a'8    
b'8    fis'8    fis'8    a'8    \bar "|"     e'4    cis'8    e'8    a8    e'8   
 cis'8    e'8    \bar "|"   a'8    cis''8    e''8    fis''8    e''8    cis''8   
 cis''8    e''8    \bar "|"   fis'4    a''8    fis''8    e''8    cis''8    b'8  
  cis''8    \bar "|"   d''8    fis''8    e''8    cis''8    a'4    a'8    }     
fis''8    \bar "|"   e''8    a'8    cis''4    e''8    cis''8    b'8    cis''8   
 \bar "|"   a'8    b'8    cis''8    e''8    fis''4    fis''8    a''8    
\bar "|"   e''8    a'8    cis''4    e''8    cis''8    b'8    cis''8    \bar "|" 
  d''8    fis''8    e''8    cis''8    b'4    b'8    fis''8    \bar "|"     e''8 
   a'8    cis''4    e''8    cis''8    b'8    cis''8    \bar "|"   a'8    b'8    
cis''8    e''8    fis''4    fis''8    gis''8    \bar "|"   a''8    gis''8    
fis''8    e''8    fis''8    gis''8    a''8    fis''8    \bar "|"   e''8    
cis''8    b'8    cis''8    a'4    a'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
