\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "seara"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/444#setting444"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/444#setting444" {"https://thesession.org/tunes/444#setting444"}}}
	title = "Terry 'Cuz' Teehan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 {   b'4    b'8    a'8    b'4    b'8   
 a'8  \bar "|"   d''8    b'8    a'8    fis'8    d'8    e'8    fis'8    a'8  
\bar "|"   b'4    b'8    a'8    b'4    b'8    a'8  \bar "|"   d''8    b'8    
a'8    fis'8    e'4.    r8 \bar "|"     b'4    b'8    a'8    b'4    b'8    a'8  
\bar "|"   d''8    b'8    a'8    fis'8    d'8    e'8    fis'8    a'8  \bar "|"  
 b'4    b'8    a'8    b'4    b'8    a'8  \bar "|"   d''8    b'8    a'8    fis'8 
   e'4.    r8 \bar "|"     b'8    e''8    e''8    fis''8    g''4    fis''8    
e''8  \bar "|"   d''4    b'8    d''8    a'8    d'8    fis'8    a'8  \bar "|"   
b'8    r8   e''8    fis''8    g''4    fis''8    e''8  \bar "|"   d''8    b'8    
a'8    fis'8    e'4.    r8 \bar "|"     b'8    e''8    e''8    fis''8    g''4   
 fis''8    e''8  \bar "|"   d''4    b'8    d''8    a'8    d'8    fis'8    a'8  
\bar "|"   b'8    e''8    e''8    d''8    b'8    a'8    fis'8    a'8  
} \alternative{{   d'8    e'8    e'8    d'8    e'8    fis'8    g'8    a'8  } {  
 d'8    e'8    e'8    d'8    e'2  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
