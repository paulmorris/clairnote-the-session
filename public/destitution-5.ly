\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/673#setting27951"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/673#setting27951" {"https://thesession.org/tunes/673#setting27951"}}}
	title = "Destitution, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \repeat volta 2 {   a'4    e''8    a'8    g''8    a'8 
   e''8    a'8  \bar "|"   g''8    f''8    e''8    f''8    d''8    f''8    e''8 
   c''8  \bar "|"   a'4    e''8    a'8    g''8    a'8    e''8    a'8  \bar "|"  
 d''8    c''8    b'8    c''8    d''8    f''8    e''8    c''8  \bar "|"     a'4  
  e''8    a'8    g''8    a'8    e''8    a'8  \bar "|"   g''8    f''8    e''8    
f''8    d''8    f''8    e''8    c''8  \bar "|"   a'8    c''8    b'8    d''8    
c''8    e''8    d''8    f''8  \bar "|"   e''8    d''8    b'8    c''8    a'4.    
g'8  }     \repeat volta 2 {   |
 e'8    a'8    a'4    e'8    a'8    c''8    a'8    \bar "|"   e'8    a'8    a'4 
   b'8    c''8    d''4  \bar "|"   e'8    a'8    a'4    e'8    a'8    c''8    
a'8    \bar "|"   e''8    f''8    e''8    d''8    e''8    a'8    a'4  \bar "|"  
   e'8    a'8    a'4    e'8    a'8    c''8    a'8  \bar "|"   e'8    a'8    a'4 
   b'8    c''8    d''4  \bar "|"   e''8    c''8    d''8    b'8    c''8    a'8   
 b'8    g'8    \bar "|"   e''8    f''8    e''8    d''8    e''8    a'8    a'8    
g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
