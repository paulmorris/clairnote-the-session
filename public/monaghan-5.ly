\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Josh Bowser"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/67#setting23930"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/67#setting23930" {"https://thesession.org/tunes/67#setting23930"}}}
	title = "Monaghan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   b'8    g'8    e'8    fis'4 ^"~"    e'8    \bar "|"   
b'8    g'8    e'8    fis'8    g'8    a'8    \bar "|"   b'8    g'8    e'8    
fis'4. ^"~"    \bar "|"   a'8    fis'8    d'8    e'8    fis'8    g'8    
\bar "|"     b'8    g'8    e'8    fis'4. ^"~"    \bar "|"   b'8    g'8    e'8   
 fis'8    g'8    a'8    \bar "|"   d''4    b'8    a'8    b'8    g'8    \bar "|" 
  fis'4. ^"~"    a'8    fis'8    d'8    }     \repeat volta 2 {   e'8    g'8    
b'8    e''8    fis''8    g''8    \bar "|"   fis''8    d''8    fis''8    e''8    
cis''8    a'8    \bar "|"   d'8    fis'8    a'8    d''8    a'8    g'8    
\bar "|"   fis'4. ^"~"    a'8    fis'8    d'8    \bar "|"     e'8    g'8    b'8 
   e''4    g''8    \bar "|"   fis''4. ^"~"    e''8    d''8    cis''8    
\bar "|"   d''8    cis''8    b'8    a'8    b'8    g'8    \bar "|"   fis'4. 
^"~"    a'8    fis'8    d'8    }     \repeat volta 2 {   g''4. ^"~"    e''8    
b'8    e''8    \bar "|"   g''4. ^"~"    b''8    g''8    e''8    \bar "|"   g''4 
   g''8    e''8    b'8    e''8    \bar "|"   fis''4. ^"~"    a''8    fis''8    
d''8    \bar "|"     g''4. ^"~"    e''8    b'8    e''8    \bar "|"   g''8    
e''8    g''8    b''8    g''8    e''8    \bar "|"   d''4    b'8    a'8    b'8    
g'8    \bar "|"   fis'4. ^"~"    a'8    fis'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
