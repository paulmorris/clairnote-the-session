\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1348#setting14695"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1348#setting14695" {"https://thesession.org/tunes/1348#setting14695"}}}
	title = "Boys Of Ballinahinch, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   fis''4    d''8    fis''8    e''8    b'8    b'4 ^"~"   
 \bar "|"   d''4. ^"~"    e''8    fis''8    g''8    a''8    g''8    \bar "|"   
fis''4    d''8    fis''8    e''8    d''8    b'8    e''8    \bar "|"   d''8    
b'8    a'8    fis'8    e'16    fis'16    e'8    d'4    \bar "|"     d''8    
cis''8    d''8    fis''8    e''8    b'8    b'4 ^"~"    \bar "|"   d''4. ^"~"    
e''8    fis''8    g''8    a''8    g''8    \bar "|"   fis''4    d''8    fis''8   
 e''8    d''8    b'8    e''8    \bar "|"   d''8    b'8    a'8    fis'8    e'16  
  fis'16    e'8    d'4    \bar "||"     a''4. ^"~"    b''8    a''8    fis''8    
d''8    fis''8    \bar "|"   a''8    fis''8    d''8    fis''8    a''8    fis''8 
   d''8    g''8    \bar "|"   fis''8    a''8    a''8    gis''8    a''8    
fis''8    d''8    fis''8    \bar "|"   g''8    fis''8    e''8    d''8    b'16   
 cis''16    d''8    e''8    a''8    \bar "|"     fis''8    a''8    a''8    
gis''8    a''8    fis''8    d''8    fis''8    \bar "|"   a''8    b''8    a''8   
 fis''8    g''8    fis''8    g''8    a''8    \bar "|"   a''8    g''8    g''8    
fis''8    g''8    fis''8    e''8    fis''8    \bar "|"   g''4    g''8    fis''8 
   g''8    b''8    a''8    g''8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
