\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/634#setting25698"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/634#setting25698" {"https://thesession.org/tunes/634#setting25698"}}}
	title = "Galway Rambler, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   g'16 ^"G"   g'16    g'8    d''8    g'8     
 e''8 ^"Em"   g'8    d''8    fis'8  \bar "|"   g'16 ^"G"   g'16    g'8    d''8  
  g'8      a'8 ^"D"   g'8    e'8    fis'8    \bar "|"   g'16 ^"G"   g'16    g'8 
   d''8    g'8    e''8    g'8    d''8    e''8  \bar "|"   g''8 ^"C"   e''8    
d''8    b'8      a'8 ^"D"   g'8    e'8    fis'8  \bar "|"       g'16 ^"G"   
g'16    g'8    d''8    g'8      e''8 ^"Em"   g'8    d''8    fis'8  \bar "|"   
g'16 ^"G"   g'16    g'8    d''8    g'8      a'8 ^"D"   g'8    e'8    fis'8  
\bar "|"   g'16 ^"G"   g'16    g'8    d''8    g'8    e''8    g'8    d''8    
e''8  \bar "|"   g''8 ^"C"   e''8    d''8    b'8      a'4. ^"D"   fis''8  
\bar "||"       g''16 ^"G"   g''16    g''8    g''8    b''8      a''8 ^"D"   
g''8    a''8    b''8  \bar "|"   g''8 ^"G"   a''8    b''8    g''8      a''8 
^"D"   g''8    e''8    fis''8  \bar "|"   g''16 ^"G"   g''16    g''8    g''8    
b''8      a''8 ^"D"   g''8    a''8    b''8  \bar "|"   g''8 ^"Em"   e''8    
d''8    b'8      a'4. ^"D"   fis''8  \bar "|"       g''16 ^"G"   g''16    g''8  
  g''8    b''8      a''8 ^"D"   g''8    a''8    b''8  \bar "|"   g''8 ^"G"   
a''8    b''8    g''8      a''4 ^"D"   g''8    a''8  \bar "|"   b''16 ^"G"   
b''16    b''8    a''16    a''16    a''8      g''16 ^"C"   g''16    g''8    e''8 
   d''8  \bar "|"   g''8 ^"G"   e''8    d''8    b'8      a'8 ^"D"   g'8    e'8  
  fis'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
