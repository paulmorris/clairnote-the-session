\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JD"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/610#setting610"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/610#setting610" {"https://thesession.org/tunes/610#setting610"}}}
	title = "Millroad Devil, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   d'4    fis'8    a'8    d''8    cis''8    d''8    
fis''8    \bar "|"   d'4    fis'8.    d'16    e'8.    b16    b8.    e'16    
\bar "|"   d'4    fis'8    a'8    d''8    cis''8    d''8    a'8    \bar "|"   
\tuplet 3/2 {   b'8    cis''8    d''8  }   \tuplet 3/2 {   a'8    b'8    g'8  }   
fis'8    d'8    d'4  }     |
 d''8.    e''16    fis''8.    g''16    a''8.    b''16    \tuplet 3/2 {   a''8    
g''8    fis''8  }   \bar "|"   d''8.    e''16    fis''8.    a''16    g''8.    
a''16    \tuplet 3/2 {   g''8    fis''8    e''8  }   \bar "|"   d''8.    e''16   
 fis''8.    g''16    a''8.    b''16    \tuplet 3/2 {   a''8    g''8    fis''8  } 
  \bar "|"   \tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 {   b''8    
a''8    g''8  }   fis''8.    d''16    d''8.    g''16    \bar "|"     
\tuplet 3/2 {   fis''8    d''8    fis''8  }   \tuplet 3/2 {   a''8    fis''8    
d''8  }   \tuplet 3/2 {   g''8    fis''8    g''8  }   \tuplet 3/2 {   b''8    
a''8    g''8  }   \bar "|"   \tuplet 3/2 {   fis''8    d''8    fis''8  }   
\tuplet 3/2 {   a''8    g''8    fis''8  }   \tuplet 3/2 {   g''8    fis''8    
g''8  }   \tuplet 3/2 {   e''8    a''8    g''8  }   \bar "|"   \tuplet 3/2 {   
fis''8    e''8    d''8  }   \tuplet 3/2 {   b'8    cis''8    d''8  }   
\tuplet 3/2 {   a'8    d''8    a'8  }   \tuplet 3/2 {   b'8    a'8    g'8  }   
\bar "|"   \tuplet 3/2 {   fis'8    g'8    a'8  }   \tuplet 3/2 {   e'8    fis'8  
  g'8  }   fis'8    d'8    d'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
