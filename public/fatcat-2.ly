\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/950#setting14145"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/950#setting14145" {"https://thesession.org/tunes/950#setting14145"}}}
	title = "Fat Cat, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   d'4    fis'8    a'8    b'8    a'8    fis''8    e''8  
\bar "|"   d''8    a'8    b'8    d''8    b'8    a'8    fis'8    a'8  \bar "|"   
fis''8    e''8    e''8    d''8    b'4    a'8    fis'8  \bar "|"   e'4    d'8    
e'8    fis'8    a'8    a'8    fis'8  \bar "|"   d'4    fis'8    a'8    b'8    
a'8    fis''8    e''8  \bar "|"   d''8    a'8    b'8    d''8    b'8    a'8    
fis'8    a'8  \bar "|"   fis''8    e''8    e''8    d''8    b'4    a'8    fis'8  
\bar "|"   e'8    d'8    fis'8    e'8    d'4.    e'8  }   |
 fis'4    a'8    fis'8    d'8    fis'8    a'8    d''8  \bar "|"   fis''4    
e''8    fis''8    d''8    b'8    a'4  \bar "|"   r2. ^"meaow?"   b''8    a''8  
\bar "|"   fis''4    e''8    d''8    b'4    d''8    b'8  \bar "|"   a'8    d'8  
  fis'8    a'8    a'8    b'8    b'8    d''8  \bar "|"   fis''4    e''8    
fis''8    d''8    b'8    a'4  \bar "|"   r2. ^"meaow!"   e''8    d''8  \bar "|" 
  b'4    a'8    b'8    d''2  \bar ":|."   b'4    a'8    fis'8    d'2  \bar "|." 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
