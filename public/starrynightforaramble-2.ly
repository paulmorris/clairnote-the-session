\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mix O'Lydian"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1210#setting26177"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1210#setting26177" {"https://thesession.org/tunes/1210#setting26177"}}}
	title = "Starry Night For A Ramble"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   e''8    \bar "|"   fis''4    d''8 
   a'4    fis'8    \bar "|"   g'4.    b'4.    \bar "|"   a'4    d''8    cis''4  
  d''8    \bar "|"   e''4    e''8    e''8    d''8    e''8    \bar "|"     
fis''4    d''8    a'4    fis'8    \bar "|"   g'4.    b'4.    \bar "|"   a'4    
d''8    cis''8    d''8    e''8    \bar "|"   d''4.    d''4    }     
\repeat volta 2 {   fis''8    \bar "|"   g''4    g''8    g''8    fis''8    e''8 
   \bar "|"   fis''4    fis''8    fis''8    e''8    d''8    \bar "|"   e''4    
e''8    e''8    fis''8    g''8    \bar "|"   a''4    fis''8    a''4    fis''8   
 \bar "|"     g''4    g''8    g''8    fis''8    e''8    \bar "|"   fis''8    
g''8    fis''8    fis''8    e''8    d''8    \bar "|"   e''4    e''8    e''8    
d''8    cis''8    \bar "|"   d''4.    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
