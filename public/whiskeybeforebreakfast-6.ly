\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "paulj504"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/602#setting22315"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/602#setting22315" {"https://thesession.org/tunes/602#setting22315"}}}
	title = "Whiskey Before Breakfast"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   \bar "|"     a8 ^"D"   d'8    fis'8    g'8    a'4    
a'4    \bar "|"   a'8    b'8    a'8    g'8    fis'8    e'8    d'8    fis'8    
\bar "|"     g'4 ^"G"   b'8    g'8      fis'4 ^"D"   a'8    fis'8    \bar "|"   
  e'8 ^"Em"   d'8    e'8    fis'8      e'8 ^"A"   d'8    cis'8    b8    
\bar "|"     \bar "|"     a8 ^"D"   d'8    fis'8    g'8    a'4    a'4    
\bar "|"   a'8    b'8    a'8    g'8    fis'8    e'8    d'8    fis'8    \bar "|" 
    g'4 ^"G"   b'8    g'8      fis'4 ^"D"   a'8    fis'8    \bar "|"     e'8 
^"A"   d'8    e'8    fis'8      d'2 ^"D"   }     \repeat volta 2 {     a'8 ^"D" 
  d''4    a'8    d''4    d''4    \bar "|"   cis''8    d''8    e''8    d''8    
cis''8    b'8    a'4    \bar "|"     b'8 ^"Em"   e''4    b'8    e''4    e''8    
fis''8    \bar "|"     g''8 ^"A"   fis''8    e''8    d''8    cis''8    a'8    
b'8    cis''8    \bar "|"     \bar "|"     d''4 ^"D"   fis''8    d''8      
cis''4 ^"A"   e''8    cis''8    \bar "|"     b'8 ^"G"   a'8    b'8    cis''8    
  b'8 ^"D"   a'8    fis'8    a'8    \bar "|"     g'4 ^"G"   b'8    g'8      
fis'4 ^"D"   a'8    fis'8    \bar "|"     e'8 ^"A"   d'8    e'8    fis'8      
d'2 ^"D"   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
