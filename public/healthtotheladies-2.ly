\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/327#setting13102"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/327#setting13102" {"https://thesession.org/tunes/327#setting13102"}}}
	title = "Health To The Ladies"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \major   \repeat volta 2 {   a''16    fis''16    \bar "|"   
e''8    cis''8    a'8    b'8    a'8    fis'8    \bar "|"   a'8    fis'8    e'8  
  e'8    fis'8    a'8    \bar "|"   b'16    cis''16    d''8    cis''8    b'8    
ais'8    b'8    \bar "|"   cis''8    b'8    ais'8    b'4    a''!16    fis''16   
 \bar "|"     e''8    cis''8    a'8    b'8    a'8    fis'8    \bar "|"   a'8    
fis'8    e'8    e'8    fis'8    a'8    \bar "|"   b'16    cis''16    d''8    
cis''8    b'8    ais'8    b'8    \bar "|"   cis''8    a'8    gis'8    a'4    }  
   \repeat volta 2 {   b'8    \bar "|"   cis''8    e''8    e''8    d''4    
fis''8    \bar "|"   cis''8    e''8    e''8    e''8    cis''8    a'8    
\bar "|"   a'8    cis''16    d''16    e''8    fis''16    gis''16    a''8    
fis''8    \bar "|"   e''8    cis''8    a'8    b'8    ais'8    b'8    \bar "|"   
  cis''4    e''8    d''8    fis''8    fis''8    \bar "|"   cis''4    e''8    
e''8    cis''8    a'8    \bar "|"   b'16    cis''16    d''8    cis''8    b'8    
ais'8    b'8    \bar "|"   cis''8    a'8    gis'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
