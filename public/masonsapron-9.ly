\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/74#setting23630"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/74#setting23630" {"https://thesession.org/tunes/74#setting23630"}}}
	title = "Mason's Apron, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a''8    a'8    a'4 ^"~"    a'8    b'8    a'8    fis'8 
 \bar "|"   e'8    fis'8    a'8    b'8    cis''4    b'8    cis''8  \bar "|"   
d''8    b'8    b'8    a'8    b'8    cis''8    b'8    a'8  \bar "|"   gis'8    
a'8    b'8    cis''8    d''8    e''8    fis''8    gis''8  \bar "|"     a''8    
a'8    a'4 ^"~"    a'8    b'8    a'8    fis'8  \bar "|"   e'8    fis'8    a'8   
 b'8    cis''4    b'8    a'8  \bar "|"   d''4. ^"~"    e''8    fis''8    e''8   
 a''8    fis''8  \bar "|"   e''8    d''8    cis''8    b'8    a'4    a''8    
fis''8  \bar "|"     e''8    cis''8    cis''4 ^"~"    b'8    fis'8    a'8    
fis'8  \bar "|"   e'8    fis'8    a'8    b'8    cis''4    b'8    a'8  \bar "|"  
 fis'8    b'8    b'4 ^"~"    cis''8    b'8    b'4 ^"~"  \bar "|"   cis''8    
e''8    fis''8    gis''8    a''8    b''8    a''8    fis''8  \bar "|"     e''8   
 cis''8    cis''4 ^"~"    b'8    fis'8    a'8    fis'8  \bar "|"   e'8    fis'8 
   a'8    b'8    cis''4    b'8    a'8  \bar "|"   d''4. ^"~"    e''8    fis''8  
  e''8    a''8    fis''8  \bar "|"   e''8    d''8    cis''8    b'8    a'4    
a'8    b'8  \bar "||"     \repeat volta 2 { \tuplet 3/2 {   cis''8    b'8    a'8 
 }   e''8    a'8    fis''8    a'8    e''8    cis''8  \bar "|"   a'8    cis''8   
 e''8    cis''8    fis''8    cis''8    e''8    cis''8  \bar "|"   d''8    
fis''8    fis''4 ^"~"    a''8    fis''8    fis''4 ^"~"  \bar "|"   d''8    
fis''8    fis''4 ^"~"    a''8    fis''8    e''8    cis''8  \bar "|"     a'8    
cis''8    e''8    cis''8    fis''8    cis''8    e''8    cis''8  \bar "|"   a'8  
  cis''8    e''8    cis''8    fis''8    cis''8    e''8    cis''8  \bar "|"   
d''4. ^"~"    e''8    fis''8    e''8    a''8    fis''8  } \alternative{{   e''8 
   d''8    cis''8    b'8    a'4    a'8    b'8  } {   e''8    d''8    cis''8    
b'8    a'4    \tuplet 3/2 {   e''8    fis''8    gis''8  } \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
