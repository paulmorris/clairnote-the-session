\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/603#setting603"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/603#setting603" {"https://thesession.org/tunes/603#setting603"}}}
	title = "Kitchen Girl, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   a''2    g''2    \bar "|"   e''8    fis''8    e''8    
d''8    cis''4    cis''8    d''8    \bar "|"   e''8    cis''8    e''8    fis''8 
   g''8    a''8    b''8    a''8    \bar "|"   g''4    e''4    e''4    e''8    
fis''8    }     |
 a''8    b''8    a''8    fis''8    g''8    a''8    g''8    g''8    \bar "|"   
e''8    fis''8    e''8    d''8    cis''8    d''8    e''8    fis''8    \bar "|"  
 g''4    d''4    e''8    fis''8    e''8    d''8    \bar "|"   cis''4    a'4    
a'4    e''8    g''8    \bar ":|."   cis''4    a'4    a'4    a'4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
