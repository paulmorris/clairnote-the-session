\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/566#setting13544"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/566#setting13544" {"https://thesession.org/tunes/566#setting13544"}}}
	title = "Red Haired Boy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   a8    d'8    d'8    c'8    d'8    e'8    fis'8   
 g'8  \bar "|"   a'8    b'8    a'8    fis'8    g'4    fis'8    g'8  \bar "|"   
a'8    d'8    d'8    c'8    d'8    e'8    e'8    d'8  \bar "|"   e'8    c'8    
a8    b8    c'4    b8    g8  \bar "|"   a8    d'8    d'8    c'8    d'8    e'8   
 fis'8    g'8  \bar "|"   a'8    b'8    a'8    fis'8    g'4    fis'8    g'8  
\bar "|"   a'8    d''8    d''8    c''8    d''8    c''8    a'8    g'8  \bar "|"  
 fis'8    d'8    e'8    fis'8    d'4.    c'8  \bar ":|."   fis'8    e'8    d'8  
  fis'8    d'4.    r8 \bar "||"   \bar ".|:"   c''4.    b'8    c''4    a'8    
b'8  \bar "|"   c''8    b'8    a'8    fis'8    g'4    fis'8    g'8  \bar "|"   
a'8    d'8    d'8    c'8    d'8    e'8    e'8    d'8  \bar "|"   e'8    c'8    
a8    b8    c'4    b8    g'8  \bar "|"   a8    d'8    d'8    c'8    d'8    e'8  
  fis'8    g'8  \bar "|"   a'8    b'8    a'8    fis'8    g'4    fis'8    g'8  
\bar "|"   a'8    d''8    d''8    c''8    d''8    c''8    a'8    g'8  \bar "|"  
 fis'8    d'8    e'8    fis'8    d'4.    r8 \bar ":|."   fis'8    e'8    d'8    
fis'8    d'4.    c'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
