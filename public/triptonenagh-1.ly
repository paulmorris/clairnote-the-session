\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bootboy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/831#setting831"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/831#setting831" {"https://thesession.org/tunes/831#setting831"}}}
	title = "Trip To Nenagh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \mixolydian   d''4    a'8    g'8    fis'8    d'8    d'4 ^"~"  
\bar "|"   a8    d'8    fis'8    a'8    d''8    c''8    a'8    g'8  \bar "|"   
a'8    d''8    d''8    c''8    d''8    f''8    e''8    d''8  \bar "|"   c''4    
g'8    c''8    e'8    c''8    g'8    c''8  \bar "|"     d''8    c''8    a'8    
g'8    fis'8    d'8    d'4 ^"~"  \bar "|"   a8    d'8    fis'8    a'8    d''8   
 c''8    a'8    g'8  \bar "|"   a'8    d''8    d''4 ^"~"    a'8    d'8    fis'8 
   a'8  \bar "|"   g'8    e'8    cis'8    e'8    e'8    d'8    d'4  }     
\repeat volta 2 {   d''4    fis''8    d''8    a'8    d''8    fis''8    d''8  
\bar "|"   g''8    e''8    e''8    d''8    cis''8    d''8    e''8    cis''8  
\bar "|"   d''4    c''!8    a'8    fis'8    d'8    d'8    e'8  \bar "|"   fis'8 
   g'8    a'8    b'8    c''!4. ^"~"    b'8  \bar "|"     a'8    d''8    d''4 
^"~"    a'8    d''8    fis''8    d''8  \bar "|"   cis''8    d''8    e''8    
fis''8    g''8    e''8    fis''8    g''8  \bar "|"   a''4    e''8    d''8    
cis''8    d''8    e''8    cis''8  \bar "|"   d''8    c''!8    a'8    g'8    
fis'8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
