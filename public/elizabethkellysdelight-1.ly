\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/953#setting953"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/953#setting953" {"https://thesession.org/tunes/953#setting953"}}}
	title = "Elizabeth Kelly's Delight"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \dorian   \repeat volta 2 {   a'4.    a'8    b'8    a'8    a'8 
   g'8    e'8  \bar "|"   a'4    e'8    g'4    e'8    d'8    e'8    g'8  
\bar "|"   a'4.    a'8    b'8    a'8    a'8    g'8    e'8  \bar "|"   g'4. 
^"~"    g'4    e'8    d'8    e'8    g'8  }     \repeat volta 2 {   |
 c''4    a'8    b'8    a'8    g'8    a'8    g'8    e'8  \bar "|"   c''8    b'8  
  a'8    b'8    g'8    e'8    d'8    e'8    g'8  \bar "|"   c''4    a'8    b'8  
  a'8    g'8    a'8    g'8    e'8  \bar "|"   g'4. ^"~"    g'4    e'8    d'8    
e'8    g'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
