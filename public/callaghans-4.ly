\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidlfad"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/724#setting21556"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/724#setting21556" {"https://thesession.org/tunes/724#setting21556"}}}
	title = "Callaghan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''8    b'8    \repeat volta 2 {   a'8    fis'8  
\grace {    g'8  }   fis'8    e'8    e'8    fis'8    d'8    e'8    \bar "|"   
fis'4    fis'8    e'8    fis'8    a'8    d''8    b'8    \bar "|"   a'8    fis'8 
   fis'8    e'8    e'8    fis'8    d'8    e'8    \bar "|"   fis'8    b'8    b'4 
^"~"    fis'8    e'8    e'8    fis'8    \bar "|"     \bar "|"   a'8    fis'8    
fis'8    e'8    e'8    fis'8    d'8    e'8    \bar "|"   fis'4    fis'8    e'8  
  fis'8    a'8    d''8    b'8    \bar "|"   a'8    fis'8    fis'8    e'8    e'8 
   fis'8    d'8    e'8    \bar "|"   \tuplet 3/2 {   fis'8    e'8    d'8  }   
e'8    g'8    fis'8    d'8    d'4    }     \repeat volta 2 {   a'4.    b'8    
a'8    fis'8    fis'4 ^"~"    \bar "|"   a'8    b'8    d''8    e''8    fis''8   
 e''8    e''8    g''8    \bar "|"   fis''8    d''8    e''8    d''8    b'8    
d''8    d''4 ^"~"    \bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8    
d''8    b'8    \bar "|"     \bar "|"   a'4.    b'8    a'8    fis'8    fis'4 
^"~"    \bar "|"   a'8    b'8    d''8    e''8    fis''8    e''8    e''8    g''8 
   \bar "|"   fis''8    d''8    e''8    d''8    b'8    d''8    d''4 ^"~"    
\bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8    d''4 ^"~"    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
