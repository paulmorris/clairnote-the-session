\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "hillfolk"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/583#setting583"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/583#setting583" {"https://thesession.org/tunes/583#setting583"}}}
	title = "Rattlin' Bog, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 2/4 \key d \major     fis''4 ^"D"   fis''8.    e''16  \bar "|"   d''8 
^"G"   b'8    b'8.    b'16  \bar "|"   a'8 ^"D"   d''8    d''8    e''8  
\bar "|"   fis''8 ^"A7"   e''8    e''4  \bar "|"       fis''4 ^"D"   fis''8.    
e''16  \bar "|"   d''8 ^"G"   b'8    b'8.    b'16  \bar "|"   a'8 ^"D"   a''8   
 a''8    fis''8  \bar "|"   fis''8 ^"A7"   e''8      d''4 ^"D" }     
\repeat volta 2 {   fis''8    d''8    e''8    d''8  \bar "|"   fis''8    d''8   
 e''8.    d''16  \bar "|"   fis''4    a''8    fis''16    fis''16  \bar "|"   
fis''8 ^"A7"   e''8    e''8    e''16    e''16  \bar "|"       fis''8 ^"D"   
d''16    d''16    e''8    d''16    d''16  \bar "|"   fis''8    a''8    a''8    
fis''8  \bar "|"   fis''8 ^"A7"   e''8      d''4 ^"D" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
