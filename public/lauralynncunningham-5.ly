\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/699#setting27338"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/699#setting27338" {"https://thesession.org/tunes/699#setting27338"}}}
	title = "Laura Lynn Cunningham"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key b \minor   b'4 ^"~"    cis''8    a'8    b'8    fis'8    fis'4 
^"~"  \bar "|"   e'8    fis'8    a'8    b'8    cis''8    e''8    a''8    e''8  
\bar "|"   fis''8    b'8    b'4 ^"~"    fis'8    b'8    b'8    cis''8  \bar "|" 
  a'8    cis''8    e''8    fis''8    cis''4 ^"~"    d''8    cis''8  \bar "|"    
 b'8    b''8    b''8    fis''8    a''4    fis''8    e''8  \bar "|"   cis''8    
fis''8    e''8    cis''8    e''8    cis''8    b'8    cis''8  \bar "|"   b'8    
fis'8    fis'8    b'8    a'8    e'8    cis''8    e''8  \bar "|"   fis''8    
cis''8    e''8    cis''8    b'4.    a'8  }     fis'8    b'8    b'4 ^"~"    a'8  
  cis''8    cis''4 ^"~"  \bar "|"   d''8    fis''8    fis''4 ^"~"    e''8    
a''8    a''8    e''8  \bar "|"   fis''8    b'8    b'4 ^"~"    fis'8    b'8    
b'8    cis''8  \bar "|"   a'8    cis''8    e''8    fis''8    cis''4 ^"~"    b'8 
   a'8  \bar "|"     fis'8    b'8    b'4 ^"~"    a'8    b'8    \tuplet 3/2 {   
cis''8    b'8    a'8  } \bar "|"   d''8    e''8    fis''8    d''8    cis''8    
a'8    a'8    cis''8  \bar "|"   b'8    fis''8    fis''8    cis''8    e''8    
cis''8    a'8    b'8  \bar "|"   cis''8    fis''8    e''8    cis''8    b'4.    
a'8  \bar "|"     fis'8    b'8    b'4 ^"~"    a'8    cis''8    cis''4 ^"~"  
\bar "|"   d''8    fis''8    fis''4 ^"~"    e''8    a''8    a''8    e''8  
\bar "|"   fis''8    b'8    b'4 ^"~"    fis'8    b'8    b'8    cis''8  \bar "|" 
  a'8    cis''8    e''8    fis''8    cis''4 ^"~"    d''8    cis''8  \bar "|"    
 b'8    b''8    b''8    fis''8    a''4    fis''8    e''8  \bar "|"   cis''8    
fis''8    e''8    cis''8    e''8    cis''8    b'8    cis''8  \bar "|"   b'8    
fis'8    fis'8    b'8    a'8    e'8    cis''8    e''8  \bar "|"   fis''8    
cis''8    e''8    cis''8    b'4.    r8 \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
