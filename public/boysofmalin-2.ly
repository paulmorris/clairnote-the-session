\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Paddy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/11#setting12373"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/11#setting12373" {"https://thesession.org/tunes/11#setting12373"}}}
	title = "Boys Of Malin, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   e''8  \bar "|"   a'8    b'8    cis''8    d''8    e''8 
   a'8    cis''8    e''8  \bar "|"   fis''8    d''8    fis''8    a''8    e''8   
 d''8    cis''8    b'8  \bar "|"   a'8    b'8    cis''8    d''8    e''8    a'8  
  cis''8    e''8  \bar "|"   fis''8    d''8    b'8    cis''8    d''4    e''8    
cis''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
