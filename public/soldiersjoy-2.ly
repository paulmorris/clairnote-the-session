\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bob himself"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1356#setting14701"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1356#setting14701" {"https://thesession.org/tunes/1356#setting14701"}}}
	title = "Soldier's Joy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'8    b'8  \repeat volta 2 {   a'4    fis'4    d'4   
 fis'4  \bar "|"   a'4    d''4    d''4    a'8    b'8  \bar "|"   a'4    fis'4   
 d'4    fis'4  \bar "|"   e'4    e'8    fis'8    e'4    a'8    b'8  \bar "|"   
\bar "|"   a'4    fis'4    d'4    fis'4  \bar "|"   a'4    d''4    d''4    d''8 
   e''8  \bar "|"   fis''8    a''8    fis''8    d''8    e''8    g''8    e''8    
cis''8  \bar "|"   d''4    d''4    d''2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
