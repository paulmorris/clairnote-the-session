\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1240#setting14537"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1240#setting14537" {"https://thesession.org/tunes/1240#setting14537"}}}
	title = "Dog Leap Stairs"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \major % Alternative bars 6&7 |FGAF D2dB AGFA|G4 D2 E4 G2|
   \repeat volta 2 {   d''8    c''8    b'8    a'8    g'4    b'8    d''8    c''8 
   b'8    a'8    g'8  \bar "|"   c''4    e'2    a'4    fis'2  \bar "|"   b'8    
a'8    g'8    d'8    b4    d'8    g'8    a'8    b'8    c''8    d''8  \bar "|"   
e''4    e'2    fis'4    g'2  }   \repeat volta 2 {   g''8    a''8    b''8    
g''8    d''4    g''8    b''8    a''4    g''8    fis''8  \bar "|"   e''8    d''8 
   c''8    b'8    a'4    e'8    g'8    fis'2  \bar "|"   g'8    a'8    b'8    
d''8    g''4    b''8    g''8    a''4    g''8    fis''8  \bar "|"   e''8    
fis''8    g''8    a''8    fis''4    e''8    d''8    g''2  }   \repeat volta 2 { 
  d''2    b'4    g'2    e'4  \bar "|"   a'8    b'8    c''8    a'8    e'4    
c''8    b'8    a'8    g'8    fis'8    a'8  \bar "|"   b'2    g'4    e'2    g'4  
\bar "|"   a'8    b'8    c''8    d''8    e''4    c''8    a'8    g'2  }   
\repeat volta 2 {   d'2    g'4    e'2    g'4  \bar "|"   fis'8    g'8    a'8    
fis'8    d'4    d''8    b'8    a'8    g'8    fis'8    e'8  \bar "|"   d'2    
g'4    e'2    g'4  \bar "|"   a'8    g'8    fis'8    a'8    d'4    fis'8    a'8 
   g'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
