\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "cac"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/537#setting26495"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/537#setting26495" {"https://thesession.org/tunes/537#setting26495"}}}
	title = "Up Sligo"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \dorian   e'8    g'8    b'8  \grace {    cis''8  }   b'8    
a'8    g'8    \bar "|"   fis'8    a'8    d''8    a'8    fis'8    d'8    
\bar "|"   e''4. ^"~"    d''8    a'8    d''8    \bar "|"   a'8    g'8    fis'8  
  e'8  \grace {    fis''8  }   e''8    d''8    \bar "|"     e''8    r8   b'8  
\grace {    cis''8  }   b'8    a'8    g'8    \bar "|"   fis'4. ^"~"  \grace {   
 b'8  }   a'8    fis'8    d'8    \bar "|"   e'8    g'8    b'8    e'8    g'8  
\grace {    cis''8  }   b'8    \bar "|"   a'8  \grace {    g'8    a'8  }   g'8  
  fis'8    e'8  \grace {    fis''8  }   e''8    d''8    \bar "||"     e''8    
r8   b'8  \grace {    cis''8  }   b'8    a'8    g'8    \bar "|"   fis'16    
g'16    a'8    d''8    a'8    fis'8    d'8    \bar "|"   e''4. ^"~"    b'8    
g'8  \grace {    cis''8  }   b'8    \bar "|"   a'8    g'8    fis'8    e'8    r8 
  d''8    \bar "|"     e''4. ^"~"  \grace {    cis''8  }   b'8    a'8    g'8    
\bar "|"   fis'8    a'8    d''8    a'8    fis'8    d'8    \bar "|"   e''4. 
^"~"    b'8    r8   b'8    \bar "|"   a'8    g'8    fis'8    e'4.    \bar "||"  
   b'8    e''8    e''8    e''8    fis''8    g''8    \bar "|" \grace {    g''8  
}   fis''8    d''8    b'8    a'8  \grace {    a'8  }   g'8    fis'8    \bar "|" 
  e''8    b'8    e''8  \grace {    fis''8  }   e''8    fis''8    g''8    
\bar "|"   fis''16    g''16    a''8    fis''8    g''8    r8   g''8    \bar "|"  
   b'4. ^"~"    b''8    g''8    e''8    \bar "|"   d''8    a'8    d''8    a'8   
 fis'8    d'8    \bar "|"   e''8    r8 \grace {    e''8    fis''8  }   e''8    
b'8    g'8    b'8    \bar "|"   a'8    g'8    fis'8    e'4.    \bar "||"     
b'8    e''8    e''8    e''8    fis''8    g''8    \bar "|" \grace {    g''8  }   
fis''8    d''8    b'8    a'8    g'8    fis'8    \bar "|"   e''4. ^"~"    e''8   
 r8   g''8    \bar "|"   fis''8    a''8    fis''8  \grace {    a''8  }   g''8   
 fis''8    e''8    \bar "|"     b'4. ^"~"  \grace {    cis''8  }   b'8    g'8   
 e'8    \bar "|"   d''8    r8   d''8    a'8    fis'8    d'8    \bar "|"   e'4. 
^"~"    b'8    g'8    b'8    \bar "|"   a'8    g'8    fis'8    e'4.    
\bar "||"     e'8    g'8    b'8  \grace {    cis''8  }   b'8    a'8    g'8    
\bar "|" \grace {    g'8  }   fis'8    d'8    fis'8  \grace {    b'8  }   a'8   
 fis'8    d'8    \bar "|"   e''4. ^"~"    d''8    a'8    d''8    \bar "|"   a'8 
   g'8    fis'8    e'8    r8   d''8    \bar "|"     e''8    g'8    b'8  
\grace {    cis''8  }   b'8    a'8    g'8    \bar "|"   fis'4. ^"~"    a'8    
r8   d'8    \bar "|"   e'8    g'8    b'8    e'8    g'8    b'8    \bar "|"   a'8 
 \grace {    g'8    a'8  }   g'8    fis'8    e'8  \grace {    fis''8  }   e''8  
  d''8    \bar "||"     e''8    r8   b'8  \grace {    cis''8  }   b'8    a'8    
g'8    \bar "|"   fis'8    a'8    d''8    a'8    fis'8    d'8    \bar "|"   
e'4. ^"~"    b'8    g'8    b'8    \bar "|"   a'8  \grace {    a'8  }   g'8    
fis'8    e'8    r8   d''8    \bar "|"     e''4. ^"~"  \grace {    cis''8  }   
b'8    a'8    g'8    \bar "|"   fis'8    a'8    d''8    a'8    fis'8    d'8    
\bar "|"   e''8    r8 \grace {    e''8    fis''8  }   e''8    b'8    g'8    b'8 
   \bar "|"   a'8    g'8    fis'8    e'4.    \bar "||"     b'8    e''8    e''8  
  e''8    fis''8    g''8    \bar "|"   fis''8    d''8    b'8    a'8    g'8    
fis'8    \bar "|"   e''4. ^"~"    e''4. ^"~"    \bar "|"   g''8    r8   g''8  
\grace {    a''8  }   g''8    fis''8    e''8    \bar "|"     b'4. ^"~"    b''8  
  g''8    e''8    \bar "|"   d''8    a'8    d''8    a'8    fis'8    d'8    
\bar "|"   e''8    r8 \grace {    e''8    fis''8  }   e''8    b'8    g'8    b'8 
   \bar "|"   a'8    g'8    fis'8    e'4.    \bar "||"     b'8    e''8    e''8  
  e''8    fis''8    g''8    \bar "|"   fis''4. ^"~"    a'8    g'8    fis'8    
\bar "|"   e''8    r8   e''8    e''8    fis''8  \grace {    a''8  }   g''8    
\bar "|"   fis''16    g''16    a''8    fis''8  \grace {    a''8  }   g''8    
fis''8    e''8    \bar "|"     b'4. ^"~"    b''8    g''8    e''8    \bar "|"   
d''8    a'8    d''8    fis''8    r8   d''8    \bar "|"   e''4. ^"~"    b'8    
g'8    b'8    \bar "|"   a'8    g'8    fis'8    e'4.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
