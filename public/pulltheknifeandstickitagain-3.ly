\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fiddleK"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/398#setting13241"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/398#setting13241" {"https://thesession.org/tunes/398#setting13241"}}}
	title = "Pull The Knife And Stick It Again"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \minor   b'4.    d''8    c''8    b'8  \bar "|"   a'8    fis'8  
  b'8    a'8    fis'8    a'8  \bar "|"   b'4.    d''8    c''8    b'8  \bar "|"  
 e''8    fis''8    d''8    e''8    d''8    c''8  \bar "|"     b'4.    d''8    
c''8    b'8  \bar "|"   a'8    fis'8    b'8    a'8    b'8    c''8  \bar "|"   
d''8    c''8    b'8    c''8    b'8    a'8  \bar "|"   fis'8    b'8    a'8    
b'4.  }     \repeat volta 2 {   b''8    a''8    fis''8    fis''8    e''8    
c''8  \bar "|"   e''8    fis''8    e''8    c''8    b'8    a'8  \bar "|"   b''8  
  a''8    fis''8    fis''8    e''8    c''8  \bar "|"   e''8    fis''8    g''8   
 a''4.  \bar "|"   b''8    a''8    fis''8    fis''8    e''8    c''8  \bar "|"   
  b'8    a'8    c''8    a'8    b'8    c''8  \bar "|"   d''8    c''8    b'8    
c''8    b'8    a'8  \bar "|"   fis'8    b'8    a'8    b'4    a'8  }     |
 b''8    a''8    fis''8    fis''8    e''8    c''8  \bar "|"   e''8    fis''8    
e''8    c''8    b'8    a'8  \bar "|"   b''8    a''8    fis''8    fis''8    e''8 
   c''8  \bar "|"   e''8    fis''8    g''8    a''4.  \bar "|"     b''8    a''8  
  fis''8    fis''8    e''8    c''8  \bar "|"   b'8    a'8    c''8    a'8    b'8 
   c''8  \bar "|"   d''8    c''8    d''8    e''8    d''8    e''8  \bar "|"   
fis''4.    e''8    d''8    c''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
