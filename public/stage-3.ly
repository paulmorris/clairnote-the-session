\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1249#setting14557"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1249#setting14557" {"https://thesession.org/tunes/1249#setting14557"}}}
	title = "Stage, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   \tuplet 3/2 {   d''8    e''8    
fis''8  }   \bar "|"   g''8.    a''16    g''8.    e''16    d''8.    g''16    
b''8.    d''16    \bar "|"   c''8.    fis''16    a''8.    c''16    b'8.    
d''16    g''8.    d''16    \bar "|"   b'8.    g'16    d''8.    b'16    c''8.    
a'16    a''8.    g''16    \bar "|"   \tuplet 3/2 {   fis''8    g''8    fis''8  } 
  \tuplet 3/2 {   e''8    fis''8    e''8  }   d''8.    e''16    fis''8.    d''16 
   \bar "|"     g''8.    a''16    g''8.    e''16    d''8.    g''16    b''8.    
d''16    \bar "|"   c''8.    fis''16    a''8.    c''16    b'8.    d''16    
g''8.    d''16    \bar "|"   b'8.    g''16    fis''8.    g''16    e''8.    
c''16    a'8.    fis'16    \bar "|"   g'4    g'4    g'4    }     
\repeat volta 2 {   d''8.    c''16    \bar "|"   b'8.    g'16    fis'8.    g'16 
   e'8.    g'16    d'8.    g'16    \bar "|"   b8.    d'16    g'8.    b'16    
c''8.    b'16    a'8.    g'16    \bar "|"   a'8.    gis'16    a'8.    b'16    
c''8.    b'16    a'8.    gis'16    \bar "|"   \tuplet 3/2 {   g'8    b'8    g'8  
}   \tuplet 3/2 {   fis'8    g'8    e'8  }   \tuplet 3/2 {   d'8    e'8    c'8  } 
  \tuplet 3/2 {   b8    c'8    a8  }   \bar "|"     g8.    b16    d'8.    g'16   
 b'8.    d''16    c''8.    b'16    \bar "|"   c''8.    d''16    e''8.    
fis''16    g''8.    fis''16    g''8.    e''16    \bar "|"   d''8.    b''16    
c''8.    a''16    e''8.    c''16    a'8.    fis'16    \bar "|"   g'4    g'4    
g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
