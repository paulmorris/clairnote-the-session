\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "olirish"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/483#setting13393"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/483#setting13393" {"https://thesession.org/tunes/483#setting13393"}}}
	title = "Reel Beatrice"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   d'4 ^"~"    b'8    d'8    fis'4    d'8    fis'8  
\bar "|"   b'8    fis'8    fis'8    e'8    fis'4    b'8    cis''8  \bar "|"   
d''4    b'8    d''8    fis''4 ^"~"    d''8    fis''8  \bar "|"   b''8    fis''8 
   fis''8    e''8    fis''4    b''8    cis'''8  \bar "|"   d'''8    cis'''8    
b''8    a''8    g''8    fis''8    e''8    d''8  \bar "|"   cis''8    e''8    
e''8    d''8    e''4.    e''8  \bar "|"   fis''8    fis''4 ^"~"    g''8    
fis''8    e''8    d''8    cis''8  \bar "|"   d''8    b'8    b'8    a'8    b'4. 
^"~"    cis''8  \bar ":|."   d''8    b'8    b'8    a'8    b'8    cis''8    d''8 
   fis''8  \bar "||"   b''4    g''8    b''8    cis'''8    g''8    g''4 ^"~"  
\bar "|"   g''8    fis''8    eis''8    fis''8    b''4.    g''8  \bar "|"   
fis''4    eis''8    fis''8    g''8    fis''8    fis''8    e''!8  \bar "|"   
d''8    e''8    d''8    cis''8    b'8    cis''8    d''8    fis''8  \bar "|"   
b''4    g''8    b''8    cis'''8    g''8    g''4 ^"~"  \bar "|"   g''8    fis''8 
   eis''8    fis''8    b''4.    g''8  \bar "|"   fis''8    fis''4 ^"~"    g''8  
  fis''8    e''8    d''8    cis''8  \bar "|"   d''8    b'8    b'4 ^"~"    b'8   
 cis''8    d''8    fis''8  \bar ":|."   b'2    a'4    b'8    cis''8  \bar "||"  
 d''8    a'8    a'4 ^"~"    fis'8    a'8    d''8    fis''8  \bar "|"   d''8    
a'8    a'4 ^"~"    fis'8    a'8    d''8    fis''8  \bar "|"   e''8    cis''8  
\grace {    d''8  }   cis''8  \grace {    b'8  }   cis''8    a'8    cis''8    
e''8    cis''8  \bar "|"   d''8    e''8    fis''8    b''8    a''8    g''8    
fis''8    e''8  \bar "|"   d''8    a'8    a'4 ^"~"    fis'8    a'8    d''8    
fis''8  \bar "|"   d''8    a'8    a'4 ^"~"    fis'8    a'8    d''8    fis''8  
\bar "|"   e''8    cis''8  \grace {    d''8  }   cis''8  \grace {    b'8  }   
cis''8    a'8    cis''8    e''8    cis''8  \bar "|"   d''8    cis''8    d''8    
e''8    d''8    a'8    b'8    cis''8  \bar ":|."   d''8    cis''8    d''8    
e''8    d''4    b'8    cis''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
