\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/789#setting13926"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/789#setting13926" {"https://thesession.org/tunes/789#setting13926"}}}
	title = "Primrose Lasses, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   fis''4    fis''8.    e''16    d''4    d''8.    b'16  
\bar "|"   a'8.    b'16    d''8.    e''16    fis''16    b'8.    b'8.    e''16  
\bar "|"   fis''4    fis''8.    e''16    d''4    d''8.    b'16  \bar "|"   a'8. 
   d''16    a''8.    g''16    fis''16    d''8.    d''8.    e''16  \bar ":|."   
a'8.    d''16    a''8.    g''16    fis''16    d''8.    d''8.    b'16  \bar "||" 
  a'8.    d''16    fis''8.    d''16    a''8.    d''16    fis''8.    d''16  
\bar "|"   a'8.    d''16    fis''8.    d''16    e''16    b'8.    b'4  \bar "|"  
 a'8.    d''16    fis''8.    d''16    a''8.    d''16    fis''8.    d''16  
\bar "|"   fis''16    g''16    a''8    e''8.    g''16    fis''16    d''8.    
d''8.    b'16  \bar "|"   a'8.    d''16    fis''8.    d''16    a''8.    d''16   
 fis''8.    d''16  \bar "|"   a'8.    d''16    fis''8.    d''16    e''16    
b'8.    b'8.    e''16  \bar "|"   fis''4    fis''8.    e''16    d''4    d''8.   
 b'16  \bar "|"   a'8.    d''16    a''8.    g''16    fis''16    d''8.    d''8.  
  e''16  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
