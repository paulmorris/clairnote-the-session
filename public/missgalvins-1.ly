\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/624#setting624"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/624#setting624" {"https://thesession.org/tunes/624#setting624"}}}
	title = "Miss Galvin's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''4    cis''8    a'8    g'8    e'8    fis'8    e'8   
 \bar "|"   d'4    fis'8    a'8    d'4    fis'8    a'8    \bar "|"   d''4    
cis''8    a'8    g'8    e'8    fis'8    a'8    \bar "|"   cis''8    d''8    
e''8    fis''8    g''4. ^"~"    b'8    \bar ":|."   \tuplet 3/2 {   b'8    
cis''8    d''8  }   e''8    cis''8    d''4.    r8   \bar "||"     fis''8    
g''8    a''8    fis''8    d''4. ^"~"    e''8    \bar "|"   fis''8    g''8    
a''8    fis''8    g''4. ^"~"    e''8    \bar "|"   fis''8    g''8    a''8    
fis''8    d''4. ^"~"    d''8    \bar "|"   c''8    d''8    e''8    fis''8    
g''4 ^"~"    fis''8    g''8    \bar "|"     fis''8    g''8    a''8    fis''8    
d''4. ^"~"    e''8    \bar "|"   fis''8    g''8    a''8    fis''8    g''4. 
^"~"    e''8    \bar "|"   fis''8    a'8    e''8    a'8    d''8    cis''8    
a'8    g'8    \bar "|"   c''8    d''8    e''8    fis''8    g''4 ^"~"    fis''8  
  g''8    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
