\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1307#setting14628"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1307#setting14628" {"https://thesession.org/tunes/1307#setting14628"}}}
	title = "Seven Step, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major \time 2/4   d'8    g'8    g'8    g'8    \bar "|"   
fis'16    g'16    a'16    fis'16    g'4    \bar "|"   d'8    b'8    b'8    b'8  
  \bar "|"   a'16    b'16    c''16    a'16    b'4    \bar "||"     d''8    d''8 
   e''4    \bar "|"   c''8    c''8    d''4    \bar "|"   b'8    b'8    c''8    
c''8    \bar "|"   a'8    a'8    b'4    \bar "|"     d''8    d''8    e''4    
\bar "|"   c''8    c''8    d''4    \bar "|"   b'8    b'8    c''8    c''8    
\bar "|"   a'8    a'8    g'4    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
