\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1296#setting14604"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1296#setting14604" {"https://thesession.org/tunes/1296#setting14604"}}}
	title = "Old French, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   \tuplet 3/2 {   a'8    b'8    cis''8  }   
\bar "|"   d''4    cis''8.    d''16    b'8.    d''16    a'8.    fis'16    
\bar "|"   d'8.    fis'16    a'8.    d''16    \tuplet 3/2 {   fis''8    g''8    
fis''8  }   e''8.    d''16    \bar "|"   cis''8.    d''16    e''8.    fis''16   
 g''8.    e''16    cis''8.    e''16    \bar "|"   d''8.    fis''16    e''8.    
d''16    \tuplet 3/2 {   b'8    cis''8    b'8  }     
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
