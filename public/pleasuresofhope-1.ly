\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Enob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/521#setting521"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/521#setting521" {"https://thesession.org/tunes/521#setting521"}}}
	title = "Pleasures Of Hope, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"   
d''8.    a'16    fis'8.    a'16    b'8.    g'16    e'8.    g'16  \bar "|" 
\tuplet 3/2 {   fis'8    e'8    d'8  }   fis'8.    a'16    d''8.    cis''16    
b'8.    a'16  \bar "|"   e'8.    e''16    e''8.    fis''16    \tuplet 3/2 {   
g''8    fis''8    e''8  }   fis''8.    d''16  \bar "|"   e''8.    cis''16    
a'8.    b'16    a'8.    g'16    fis'8.    e'16  \bar "|"     \bar "|"   fis'8.  
  a'16    d''8.    d''16    d''8.    a'16    fis'8.    d'16  \bar "|"   e'8.    
e''16    e''8.    fis''16    \tuplet 3/2 {   g''8    fis''8    e''8  }   cis''8. 
   a'16  \bar "|"   d''8.    fis''16    a''8.    fis''16    b''8.    g''16    
e''8.    cis''16  \bar "|"   d''4  \grace {    e''8  }   d''8.    cis''16    
d''4  }     \repeat volta 2 { \tuplet 3/2 {   a'8    b'8    cis''8  } \bar "|"   
d''8.    fis''16    a''8.    fis''16    b''8.    g''16    e''8.    cis''16  
\bar "|"   d''8.    fis''16    a''8.    fis''16    g''8.    e''16    cis''8.    
a'16  \bar "|"   d''8.    fis''16    a''8.    fis''16    \tuplet 3/2 {   g''8    
fis''8    e''8  }   fis''8.    d''16  \bar "|" \tuplet 3/2 {   b'8    b'8    b'8 
 }   b'8.    a'16    b'8.    d''16    a'8.    g'16  \bar "|"     \bar "|"   
fis'8.    a'16    d''8.    d''16    d''8.    a'16    fis'8.    d'16  \bar "|"   
e'8.    e''16    e''8.    fis''16    \tuplet 3/2 {   g''8    fis''8    e''8  }   
cis''8.    a'16  \bar "|"   d''8.    fis''16    a''8.    fis''16    b''8.    
g''16    e''8.    cis''16  \bar "|"   d''4  \grace {    e''8  }   d''8.    
cis''16    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
