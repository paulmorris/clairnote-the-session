\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "billwolfe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/187#setting29219"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/187#setting29219" {"https://thesession.org/tunes/187#setting29219"}}}
	title = "Far Away"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key b \minor   \repeat volta 2 {   fis'8    a'8    \bar "|"     b'4. 
^"Bm"   a'8    b'8    d''8    \bar "|"     cis''8 ^"F\#m"   a'8    fis'4    
fis'8    a'8    \bar "|"     b'4. ^"Bm"   a'8    b'8    e''8    \bar "|"     
cis''4. ^"A"   a'8    d''8    cis''8    \bar "|"       b'4. ^"G"   g'8    b'8   
 d''8    \bar "|"     cis''8 ^"F\#m"   a'8    fis'4    d''8    e''8    \bar "|" 
    fis''8 ^"D"   e''8    d''8      cis''8 ^"A"   b'8    a'8    
} \alternative{{     b'2 ^"Bm"   } {     b'4 ^"Bm"   b'8    cis''8    d''8    
e''8    \bar "|."       fis''4 ^"D"   fis''8    e''8    fis''8    a''8    
\bar "|"     e''8 ^"A"   cis''8    a'4    d''8    e''8    \bar "|"     fis''4. 
^"D"   e''8    fis''8    a''8    \bar "|"     e''2 ^"A"   d''8    e''8    
\bar "|"       fis''4 ^"D"   fis''8    e''8    fis''8    a''8    \bar "|"     
e''8 ^"A"   cis''8    a'4    b'8    cis''8    \bar "|"     d''8 ^"G"   cis''8   
 b'8      cis''8 ^"A"   b'8    a'8    \bar "|"     b'4 ^"Bm"   b'8    cis''8    
d''8    e''8    \bar "|"       fis''4 ^"D"   fis''8    e''8    fis''8    a''8   
 \bar "|"     e''8 ^"A"   cis''8    a'4    d''8    e''8    \bar "|"     fis''4. 
^"D(Bm)"   e''8    fis''8    a''8    \bar "|"     e''2 ^"A"   d''8    e''8    
\bar "|"       fis''8 ^"Bm"   d''8    b'8      e''8 ^"A"   cis''8    a'8    
\bar "|"     d''8 ^"G"   b'8    g'8      cis''8 ^"A"   b'8    a'8    \bar "|"   
  fis'4 ^"Bm"   b'4      b'8 ^"(F\#m)"   a'8    \bar "|"   b'2. ^"Bm"   
\bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
