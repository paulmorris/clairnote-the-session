\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/573#setting13553"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/573#setting13553" {"https://thesession.org/tunes/573#setting13553"}}}
	title = "Cuckoo's Nest, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   b''8.    g''16    \tuplet 3/2 {   g''8    fis''8    
g''8  }   d''8.    g''16    b'8.    g''16    \bar "|"   b''8.    g''16    
\tuplet 3/2 {   g''8    g''8    g''8  }   d''8.    g''16    b'8.    g''16    
\bar "|"   c'''8.    a''16    \tuplet 3/2 {   a''8    b''8    a''8  }   e''8.    
a''16    c''8.    a''16    \bar "|"   c'''8.    a''16    \tuplet 3/2 {   a''8    
a''8    a''8  }   e''8.    a''16    c''8.    a''16    \bar "|"   b''8.    g''16 
   \tuplet 3/2 {   g''8    fis''8    g''8  }   d''8.    g''16    \tuplet 3/2 {   
b'8    c''8    d''8  }   \bar "|"   b''8    g''8    \tuplet 3/2 {   g''8    g''8 
   g''8  }   d''8.    g''16    b'8.    g''16    \bar "|"   \tuplet 3/2 {   a''8  
  b''8    a''8  }   \tuplet 3/2 {   g''8    a''8    g''8  }   \tuplet 3/2 {   
fis''8    g''8    fis''8  }   \tuplet 3/2 {   e''8    fis''8    e''8  }   
\bar "|"   \tuplet 3/2 {   d''8    e''8    d''8  }   cis''8.    d''16    b'8.    
d''16    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
