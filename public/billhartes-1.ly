\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1281#setting1281"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1281#setting1281" {"https://thesession.org/tunes/1281#setting1281"}}}
	title = "Bill Harte's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   \bar "||"   d'8    e'8    g'8    a'8    b'8    
g'8    g'4 ^"~"    \bar "|"   b'8    g'8    d''8    b'8    a'8    b'8    g'8    
e'8    \bar "|"   d'8    e'8    g'8    a'8    b'8    e''8    e''8    d''8    
\bar "|"   b'8    a'8    a'8    b'8    a'4    g'8    e'8    \bar "|"     d'8    
e'8    g'8    a'8    b'8    g'8  \grace {    a'8  }   g'4    \bar "|"   b'8    
g'8    d''8    b'8    a'8    b'8    g'8    e'8    \bar "|"   d'8    e'8    g'8  
  a'8    b'8    e''8    e''8    d''8    \bar "|"   b'8    a'8    a'8    b'8    
a'4    b'8    d''8    \bar "||"     e''8    a'8    a'4 ^"~"    b'8    d''8    
e''8    g''8    \bar "|"   d''8    g'8    g'4 ^"~"    g'8    a'8    b'8    d''8 
   \bar "|"   e''8    a'8    a'4 ^"~"    b'8    d''8    e''8    g''8    
\bar "|"   d''8    b'8    g'8    b'8    a'4    \tuplet 3/2 {   c''8    b'8    
a'8  }   \bar "|"     e''8    a'8    \tuplet 3/2 {   a'8    a'8    a'8  }   b'8  
  d''8    e''8    g''8    \bar "|"   d''8    g'8    \tuplet 3/2 {   g'8    g'8   
 g'8  }   g'8    a'8    b'8    d''8    \bar "|"   e''8    g''8    g''4    e''8  
  d''8    e''8    a''8    \bar "|"   g''8    e''8    d''8    b'8    a'8    b'8  
  g'8    e'8    \bar "||"   % Variation measure 1 or 5:
   \bar "||"   d'4.    a'4    b'8    g'4    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
