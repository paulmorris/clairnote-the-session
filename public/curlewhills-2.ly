\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/670#setting13711"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/670#setting13711" {"https://thesession.org/tunes/670#setting13711"}}}
	title = "Curlew Hills, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'8    a'8    b'8    c''8    d''8    g''8    fis''8   
 a''8  \bar "|"   g''4    b'4    d''4.    b'8  \bar "|"   c''8    b'8    a'8    
b'8    c''4.    c''8  \bar "|"   b'8    a'8    g'8    a'8    b'8    c''8    b'8 
   a'8  \bar "|"   g'8    a'8    b'8    c''8    d''8    g''8    fis''8    a''8  
\bar "|"   g''4    b'4    d''4.    b'8  \bar "|"   c''8    b'8    a'8    b'8    
c''8    a'8    fis'8    a'8  \bar "|"   g'4  \grace {    a'8  }   g'8    fis'8  
  g'4    \tuplet 3/2 {   d'8    e'8    fis'8  } \bar ":|."   g'4  \grace {    
a'8  }   g'8    fis'8    g'8    a'8    b'8    c''8  \bar "|"   \bar ".|:"   d''4 
   b''4    c''4    a''4  \bar "|"   b'4    g''4    g''8    d''8    b'8    g'8  
\bar "|"   fis'8    g'8    a'8    b'8    c''8    a'8    fis'8    a'8  \bar "|"  
 g'8    fis'8    g'8    a'8    b'8    a'8    b'8    c''8  \bar "|"   d''4    
b''4    c''4    a''4  \bar "|"   b'4    g''4    g''8    d''8    b'8    g'8  
\bar "|"   fis'8    g'8    a'8    b'8    c''8    a'8    fis'8    a'8  \bar "|"  
 g'4  \grace {    a'8  }   g'8    fis'8    g'8    a'8    b'8    c''8  
\bar ":|."   g'4    d'4    g2  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
