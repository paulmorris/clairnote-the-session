\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/104#setting104"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/104#setting104" {"https://thesession.org/tunes/104#setting104"}}}
	title = "Snowy Path, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \bar "|"   fis'4 ^"D"   a'8    b'4    fis'8    a'4    
fis'8  \bar "|"   g'4 ^"G"   b'8    d''4    e''8    d''8    b'8    a'8  
\bar "|"   fis'4 ^"D"   a'8    b'4    fis'8    a'4    fis'8  \bar "|"   e'4 
^"Em"   d'8    e'4    fis'8    g'8    fis'8    e'8  \bar "|"     fis'4 ^"D"   
a'8    b'4    fis'8    a'4    fis'8  \bar "|"   g'4 ^"G"   b'8    d''4    e''8  
  d''8    b'8    a'8  \bar "|"   fis'4 ^"D"   a'8    b'4    fis'8    a'4    
fis'8  \bar "|"   e'4 ^"Em"   d'8    e'4    fis'8    g'8    a'8    b'8  
\bar "||"   \bar "|"   cis''4. ^"A"   cis''4    e''8    d''4    cis''8  
\bar "|"   b'4 ^"G"   g'8    b'4    cis''8    d''4    e''8  \bar "|"   fis''4. 
^"Bm"   fis''4    e''8    d''4    b'8  \bar "|"   a'4 ^"D"   g'8    fis'4    
g'8    a'4    b'8  \bar "|"     cis''4. ^"A"   cis''4    e''8    d''4    cis''8 
 \bar "|"   b'4 ^"G"   g'8    b'4    cis''8    d''4    e''8  \bar "|"   d''4 
^"D"   a'8    b'4    fis'8    a'4    fis'8  \bar "|"   e'4 ^"Em"   d'8    e'4   
 fis'8    g'8    fis'8    e'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
