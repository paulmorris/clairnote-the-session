\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1429#setting14807"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1429#setting14807" {"https://thesession.org/tunes/1429#setting14807"}}}
	title = "Barney Brannigan"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   fis'4    a'8    a'8    b'8    a'8 
   a'8    b'8    a'8    \bar "|"   fis'4    a'8    a'8    b'8    cis''8    d''8 
   e''8    fis''8    \bar "|"   fis'4    a'8    a'8    b'8    a'8    a'8    b'8 
   a'8    \bar "|"   b'8    cis''8    d''8    e''8    fis''8    d''8    cis''8  
  b'8    a'8    }   \repeat volta 2 {   fis''4    a''8    fis''4    a''8    
g''8    fis''8    e''8    \bar "|"   fis''4    a''8    fis''4    a''8    g''4   
 e''8    \bar "|"   fis''4    a''8    fis''4    a''8    g''8    fis''8    e''8  
  \bar "|"   cis''8    d''8    e''8    e''8    fis''8    d''8    cis''8    b'8  
  a'8    }   \repeat volta 2 {   fis''4    r8   a''4    r8   d''4    r8   
\bar "|"   g''4    fis''8    e''4    d''8    cis''8    b'8    a'8    \bar "|"   
fis''4    r8   e''4    r8   d''4    r8   \bar "|"   b'4    b'8    a'4    g'8    
fis'8    e'8    d'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
