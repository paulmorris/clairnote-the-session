\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/717#setting717"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/717#setting717" {"https://thesession.org/tunes/717#setting717"}}}
	title = "Sourgrass And Granite"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   fis'8    e'8  \repeat volta 2 {   d'8 ^"D"   a8    
d'8    fis'8    a'8    d''8  \bar "|"   cis''4. ^"A"   d''8    e''8    g''8  
\bar "|"   fis''4 ^"D"   d''4    a'4  \bar "|"   b'4. ^"G"   g'8    d'4  
\bar "|"       b'4 ^"G"   b'8    cis''8    d''4  \bar "|"   fis'4. ^"D"   g'8   
 a'4  } \alternative{{     b'8 ^"Em"   cis''8    d''4    fis'4  \bar "|"   e'4. 
^"A"   g'8    fis'8    e'8  } }      \bar "|"     b'8 ^"Em"   cis''8    d''4    
  e'8 ^"A7"   fis'16    e'16  \bar "|"   d'4. ^"D"   cis''8    d''8    e''8  
\bar "||"       fis''8 ^"Bm"   b'8    b'8    cis''8    d''8    fis''8  \bar "|" 
  e''8 ^"A"   a'8    a'8    b'8    cis''8    e''8  \bar "|"   d''4 ^"G"   
cis''4    b'4  \bar "|"   a'4. ^"D"   fis'8    d'4  \bar "|"       b'4 ^"G"   
b'8    a'8    fis'8    a'8  \bar "|"   d'4 ^"D"   d'8    fis'8    a'8    d''8  
\bar "|"     fis''4 ^"E9"   fis''8    e''8    d''8    fis''8  \bar "|"   e''4. 
^"A"   cis''8      d''8 ^"D"   e''8  \bar ":|."     \bar "|"     fis''4 ^"E9"   
fis''8    e''8      d''8 ^"A7"   cis''8  \bar "|"   d''2. ^"D" \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
