\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/236#setting28422"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/236#setting28422" {"https://thesession.org/tunes/236#setting28422"}}}
	title = "Swinging On The Gate"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   b'8    d''8  \bar "|"   g''8    
e''8    d''8    b'8    g'4    a'8    b'8  \bar "|"   c''8    a'8    b'8    g'8  
  a'8    g'8    e'8    g'8  \bar "|"   d'8    g'8    b'8    d''8    g''4    
fis''8    g''8  \bar "|"   e''8    a''8    a''8    g''8    fis''8    d''8    
e''8    fis''8  \bar "|"     g''8    e''8    d''8    b'8    g'4    a'8    b'8  
\bar "|"   c''8    a'8    b'8    g'8    a'8    g'8    e'8    g'8  \bar "|"   
c''8    a'8    b'8    g'8    a'8    g'8    e'8    g'8  \bar "|"   d'8    g'8    
g'8    fis'8    g'8    a'8    b'8    d''8  }     \repeat volta 2 {   g''4.    
a''8    b''8    g''8    a''8    fis''8  \bar "|"   g''8    fis''8    e''8    
d''8    b'4    a'8    g'8  \bar "|"   e'8    a'8    a'8    b'8    c''8    b'8   
 a'8    g'8  \bar "|" \tuplet 3/2 {   e'8    fis'8    g'8  }   a'8    b'8    
c''8    d''8    e''8    fis''8  \bar "|"     g''4.    a''8    b''8    g''8    
a''8    fis''8  \bar "|"   g''8    fis''8    e''8    d''8    b'4    a'8    b'8  
\bar "|"   c''8    a'8    b'8    g'8    a'8    g'8    e'8    g'8  \bar "|"   
d'8    g'8    g'8    fis'8    g'8    a'8    b'8    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
