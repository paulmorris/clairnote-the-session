\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/806#setting806"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/806#setting806" {"https://thesession.org/tunes/806#setting806"}}}
	title = "Flagstone Of Memories"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   b'4    b'8    a'8    b'8    a'8   
 g'8    a'8    \bar "|"   b'8    d'8    d'4 ^"~"    e'8    g'8    g'8    a'8    
\bar "|"   b'8    d'8    d'4 ^"~"    e'8    g'8    g'8    a'8    \bar "|"   b'8 
   d''8    d''8    b'8    a'4    g'8    a'8    \bar "|"     b'4    b'8    a'8   
 b'8    a'8    g'8    a'8    \bar "|"   b'8    d'8    d'4 ^"~"    e'8    g'8    
g'8    a'8    \bar "|"   b'8    d''8    d''4    e''8    g''8    g''8    e''8    
\bar "|"   d''8    b'8    a'8    b'8    g'4.    b'8    }     \repeat volta 2 {  
 |
 g''4 ^"~"    g''8    e''8    fis''4 ^"~"    fis''8    d''8    \bar "|"   e''4  
  e''8    d''8    b'8    a'8    g'8    a'8    \bar "|"   b'8    d''8    e''8    
fis''8    g''8    fis''8    g''8    d''8    \bar "|"   b'8    d''8    e''8    
fis''8    g''8    d''8    b'8    d''8    \bar "|"     g''4 ^"~"    g''8    e''8 
   fis''4 ^"~"    fis''8    d''8    \bar "|"   e''4    e''8    d''8    b'8    
a'8    g'8    a'8    \bar "|"   b'8    d''8    d''4    e''8    g''8    g''8    
e''8    \bar "|"   d''8    b'8    a'8    b'8    g'4.    r8   }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
