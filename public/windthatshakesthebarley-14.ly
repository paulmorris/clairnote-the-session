\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/116#setting22309"
	arranger = "Setting 14"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/116#setting22309" {"https://thesession.org/tunes/116#setting22309"}}}
	title = "Wind That Shakes The Barley, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   fis'4  \bar "|"   g'4    fis'4    e'4    fis'4  
\bar "|"   g'4    fis'4    e'4    fis'4  \bar "|"   g'4    g'4    g'4    b'4  
\bar "|"   a'4    fis'4    d'4    fis'4  \bar "|"     g'4    fis'4    g'4    
a'4  \bar "|"   b'4    c''4    b'4    a'4  \bar "|"   g'4    e'4    fis'4    
d'4  \bar "|"   e'4    e'4    fis'2  \bar "|"     g'4    fis'4    g'4    a'4  
\bar "|"   b'4    c''4    b'4    a'4  \bar "|"   g'4    fis'4    g'4    a'4  
\bar "|"   b'4    e'4    e'4    fis'4  \bar "|"     g'4    fis'4    g'4    a'4  
\bar "|"   b'4    c''4    b'4    a'4  \bar "|"   g'4    e'4    fis'4    d'4  
\bar "|"   e'4    e'4    e'4  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
