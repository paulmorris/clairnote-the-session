\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "PJ Mediterranean"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/234#setting12934"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/234#setting12934" {"https://thesession.org/tunes/234#setting12934"}}}
	title = "Farrel O'Gara's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   a8 
   d'8    fis'8    a'8    \bar "|"   b'8    fis'8    a'8    fis'8    e'4    
\tuplet 3/2 {   a'8    b'8    cis''8  }   \bar "|"   d''4    cis''8    a'8    
b'8    d''8    a'8    fis'8    \bar "|"   \tuplet 3/2 {   g'8    fis'8    e'8  } 
  fis'8    d'8    e'8    d'8    b8    d'8    \bar "|"   a8    d'8    
\tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8    fis'8    a'8    \bar "|"   
b'8    fis'8    a'8    fis'8    e'8    fis'8    a'8    cis''8    \bar "|"   
cis''8    d''8    cis''8    d''8    b'8    d''8    a'8    fis'8    \bar "|"   
g'8    fis'8    e'8    g'8    fis'8    d'8    b8    d'8    \bar ":|."   g'8    
fis'8    e'8    g'8    fis'8    d'8    d'4    \bar "||"   fis''8    d''8    
\tuplet 3/2 {   d''8    d''8    d''8  }   fis''8    d''8    g''8    d''8    
\bar "|"   fis''8    d''8    \tuplet 3/2 {   d''8    d''8    d''8  }   e''8    
d''8    b'8    cis''8    \bar "|"   d''8    b'8    b'8    a'8    b'8    cis''8  
  d''8    e''8    \bar "|"   fis''8    a''8    g''8    fis''8    e''4.    d''8  
  \bar "|"   fis''8    d''8    \tuplet 3/2 {   d''8    d''8    d''8  }   fis''8  
  d''8    g''8    d''8    \bar "|"   fis''8    d''8    \tuplet 3/2 {   d''8    
d''8    d''8  }   e''8    d''8    b'8    cis''8    \bar "|"   d''8    b'8    
b'8    a'8    b'8    d''8    a'8    fis'8    \bar "|"   g'8    fis'8    e'8    
g'8    fis'8    d'8    d'4    \bar "|"   fis''8    d''8    \tuplet 3/2 {   d''8  
  d''8    d''8  }   fis''8    d''8    g''8    d''8    \bar "|"   fis''8    d''8 
   \tuplet 3/2 {   d''8    d''8    d''8  }   e''8    d''8    b'8    cis''8    
\bar "|"   d''8    b'8    b'8    a'8    b'8    cis''8    d''8    e''8    
\bar "|"   fis''8    a''8    g''8    fis''8    e''8    fis''8    \tuplet 3/2 {   
g''8    fis''8    e''8  }   \bar "|"   a''4.    fis''8    e''8    d''8    b'8   
 cis''8    \bar "|"   d''8    fis''8    e''8    d''8    b'8    a'8    fis'8    
a'8    \bar "|"   a8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   a8    d'8  
  fis'8    a'8    \bar "|"   b'8    fis'8    a'8    fis'8    d'4    d'8    b8   
 \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
