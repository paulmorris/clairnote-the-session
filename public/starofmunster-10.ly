\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Kevin Rietmann"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/197#setting26184"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/197#setting26184" {"https://thesession.org/tunes/197#setting26184"}}}
	title = "Star Of Munster, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   b'8 ^"1"   a'8    g'8    b'8    a'8    g'8 
   fis'8    d'8    \bar "|"   g'8    b'8    a'8    g'8    f'8    d'8    c'8    
a8    \bar "|"   g4    g'8    fis'8    g'8    a'8    b'8    c''8    \bar "|"   
d''8    c''8    d''8    e''8    f''8    d''8    c''8    a'8    \bar "|"     b'4 
   g'8    b'8    a'8    g'8    fis'8    d'8    \bar "|"   g'8    b'8    a'8    
g'8    f'8    d'8    c'8    a8    \bar "|"   g4    g'8    fis'8    g'8    a'8   
 b'8    c''8    \bar "|"   \tuplet 3/2 {   d''8    c''8    b'8  }   c''8    a'8  
  b'8    g'8    g'4    \bar "|"     b'8    g'8    \tuplet 3/2 {   g'8    g'8    
g'8  }   a'8    fis'8    \tuplet 3/2 {   fis'8    fis'8    fis'8  }   \bar "|"   
g'8    b'8    a'8    g'8    f'8    d'8    c'4    \bar "|"   d'8    g'8    g'8   
 fis'8    g'8    a'8    b'8    c''8    \bar "|"   d''8    c''8    d''8    e''8  
  f''8    d''8    c''8    a'8    \bar "|"     b'4    c''8    a'8    b'8    a'8  
  g'8    fis'8    \bar "|"   g'8    b'8    a'8    g'8    f'8    d'8    c'8    
a8    \bar "|"   g4    g'8    fis'8    g'8    a'8    b'8    c''8    \bar "|"   
\tuplet 3/2 {   d''8    c''8    b'8  }   c''8    a'8    b'8    g'8    g'4    
\bar "|"     \repeat volta 2 {   g''8    fis''8    g''8    a''8    g''8    d''8 
   \tuplet 3/2 {   d''8    d''8    d''8  }   \bar "|"   g''8    d''8    a''8    
d''8    g''8    d''8    \tuplet 3/2 {   d''8    d''8    d''8  }   \bar "|"   
f''4    g''8    e''8    f''8    c''8    \tuplet 3/2 {   c''8    c''8    c''8  }  
 \bar "|"   f''8    g''8    a''8    g''8    f''8    d''8    c''8    a'8    
\bar "|"     g''8    fis''8    g''8    a''8    g''8    d''8    \tuplet 3/2 {   
d''8    d''8    d''8  }   \bar "|"   g''8    d''8    a''8    d''8    g''8    
d''8    d''8    e''8    \bar "|"   f''2    g''2    \bar "|"   a''4.    g''8    
f''8    d''8    c''8    a'8    }     \bar "|"   b'8 ^"1"   d''8    c''8    b'8  
  a'8    c''8    b'8    a'8    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
