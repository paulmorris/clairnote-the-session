\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/252#setting12979"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/252#setting12979" {"https://thesession.org/tunes/252#setting12979"}}}
	title = "Follow Me Down To Milltown"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \tuplet 3/2 {   a'8    a'8    a'8  }   \tuplet 3/2 {   
fis'8    fis'8    fis'8  }   \tuplet 3/2 {   a'8    a'8    a'8  }   \tuplet 3/2 { 
  fis'8    fis'8    fis'8  }   \bar "|"   \tuplet 3/2 {   b'8    b'8    b'8  }   
\tuplet 3/2 {   g'8    g'8    g'8  }   \tuplet 3/2 {   b'8    b'8    b'8  }   
\tuplet 3/2 {   g'8    g'8    g'8  }   \bar "|"   \tuplet 3/2 {   a'8    b'8    
a'8  }   fis'4    \tuplet 3/2 {   a'8    b'8    a'8  }   fis'4    \bar "|"   
\tuplet 3/2 {   e'8    e'8    e'8  }   \tuplet 3/2 {   g'8    g'8    g'8  }   
\tuplet 3/2 {   b'8    b'8    b'8  }   \tuplet 3/2 {   g'8    g'8    g'8  }   
\bar "|"     a'8.    d'16    fis'8.    g'16    a'8.    d'16    fis'8.    d''16  
  \bar "|"   b'8.    d'16    g'8.    a'16    b'8.    d''16    cis''8.    b'16   
 \bar "|"   \tuplet 3/2 {   a'8    a'8    a'8  }   \tuplet 3/2 {   d''8    cis''8 
   b'8  }   \tuplet 3/2 {   a'8    a'8    a'8  }   \tuplet 3/2 {   g'8    fis'8   
 e'8  }   \bar "|"   \tuplet 3/2 {   d'8    d'8    d'8  }   \tuplet 3/2 {   cis'8 
   cis'8    cis'8  }   d'4    r4   \bar "||"     a'8.    d''16    fis''8.    
d''16    a'8.    d''16    fis''4    \bar "|"   a'8.    cis''16    e''8.    
cis''16    a'8.    cis''16    e''4    \bar "|"   a'8.    d''16    fis''8.    
d''16    a'8.    d''16    fis''8.    d''16    \bar "|"   e''4    cis''8.    
e''16    d''8.    cis''16    d''8.    b'16    \bar "|"     a'8.    d''16    
fis''4    a''8.    d''16    fis''8.    d''16    \bar "|"   a'8.    cis''16    
e''4    g''8.    cis''16    e''8.    cis''16    \bar "|"   d''4    cis''4    
a'4    g'8.    e'16    \bar "|"   d'4    cis'4    d'2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
