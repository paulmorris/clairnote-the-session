\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/898#setting27416"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/898#setting27416" {"https://thesession.org/tunes/898#setting27416"}}}
	title = "Mourne Mountains, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   b'8    g'8    g'8    a'8    g'4    e'8 (   d'8  -) 
\bar "|"   e'8    a'8    a'8    b'8    a'4    g'8 (   e'8  -) \bar "|"   d'8    
e'8    g'8    a'8    b'8    a'8    b'8    d''8  \bar "|" \tuplet 3/2 {   e''8    
fis''8    g''8  }   d''8 (   b'8  -)   c''8    b'8    a'8    g'8  \bar "|"     
b'8    g'8    g'8    a'8    g'4    e'8 (   d'8  -) \bar "|"   e'8    a'8    a'8 
   b'8    a'4    g'8 (   e'8  -) \bar "|"   d'8    e'8    g'8    a'8    b'8    
a'8    b'8    d''8  \bar "|" \tuplet 3/2 {   e''8    fis''8    g''8  }   d''8    
c''8    b'8    g'8    g'4  \bar "||"     d''8    g''8    g''8    a''8    g''4   
 e''8 (   d''8  -) \bar "|"   e''8    a''8    a''8    b''8    a''4    g''8 (   
e''8  -) \bar "|"   d''8    e''8    g''8    a''8    b''8    a''8    g''8    
fis''8  \bar "|" \tuplet 3/2 {   e''8    fis''8    g''8  }   \tuplet 3/2 {   d''8 
   c''8    b'8  }   c''8    b'8    a'8    c''8  \bar "|"     d''8    g''8    
g''8    a''8    g''4    e''8 (   d''8  -) \bar "|"   e''8    a''8    a''8    
b''8    a''4    g''8 (   e''8  -) \bar "|"   d''8    e''8    g''8    a''8    
b''8    a''8    g''8    fis''8  \bar "|" \tuplet 3/2 {   g''8    fis''8    e''8  
}   d''8    c''8    b'8    g'8    g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
