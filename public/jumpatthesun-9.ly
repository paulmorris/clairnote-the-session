\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/736#setting13817"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/736#setting13817" {"https://thesession.org/tunes/736#setting13817"}}}
	title = "Jump At The Sun"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \minor   \repeat volta 2 {   e'8    \bar "|"   d'8    f'8    
a'8    gis'4    a'8    \bar "|"   d'8    f'8    a'8    gis'4    a'8    \bar "|" 
  d''8    a'8    a'8    d''8    a'8    a'8    \bar "|"   a'8    g'8    f'8    
e'4    f'8    \bar "|"     d'8    f'8    a'8    gis'4    a'8    \bar "|"   d'8  
  f'8    a'8    gis'4    a'8    \bar "|"   d''8    a'8    a'8    bes'8    a'8   
 g'8    \bar "|"   f'8    g'8    e'8    d'4    }     \repeat volta 2 {   a'8    
\bar "|"   d''8    a'8    a'8    f''8    e''8    d''8    \bar "|"   e''8    a'8 
   a'8    g''8    f''8    e''8    \bar "|"   f''8    e''8    d''8    f''8    
e''8    d''8    \bar "|"   e''8    cis''8    a'8    bes'4    a'8    \bar "|"    
 d''8    a'8    a'8    f''8    e''8    d''8    \bar "|"   e''8    a'8    a'8    
g''8    f''8    e''8    \bar "|"   d''8    a'8    a'8    bes'8    a'8    g'8    
\bar "|"   f'8    g'8    e'8    d'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
