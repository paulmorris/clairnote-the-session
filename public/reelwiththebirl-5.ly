\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "hetty"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/189#setting12840"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/189#setting12840" {"https://thesession.org/tunes/189#setting12840"}}}
	title = "Reel With The Birl, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   e'4    b'8    e'8    d''8    e'8    b'8    e'8    
\bar "|"   e'4.    b'8    a'8    fis'8    d'8    fis'8    \bar "|"   e'4    b'8 
   e'8    d''8    e'8    b'8    e'8    \bar "|"   d''8    cis''8    d''8    b'8 
   a'8    fis'8    d'8    fis'8    \bar ":|."   d''8    cis''8    d''8    b'8   
 a'2    \bar "||"   b'8    a'8    b'8    d''8    e''4    d''8    e''8    
\bar "|"   fis''8    d''8    d''8    cis''8    d''4    e''8    fis''8    
\bar "|"   g''2    g''2    \bar "|"   fis''8    a''8    a''8    b''8    a''8    
fis''8    e''8    d''8    \bar "|"   d''8    b''8    b''4    b''8    a''8    
fis''8    e''8    \bar "|"   fis''8    a''8    a''8    b''8    a''8    fis''8   
 e''8    d''8    \bar "|"   b'4.    b'8    b'8    a'8    fis'8    a'8    
\bar "|"   d''8    cis''8    d''8    b'8    a'8    fis'8    d'8    fis'8    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
