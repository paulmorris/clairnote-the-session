\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dan the Man"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/87#setting12614"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/87#setting12614" {"https://thesession.org/tunes/87#setting12614"}}}
	title = "Rolling In The Ryegrass"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   a'8    b'8    a'8    fis'8    d'8    e'8   
 fis'8    a'8  \bar "|"   g'8    a'8    b'8    g'8    d''8    g'8    b'8    g'8 
 \bar "|"   a'8    b'8    a'8    fis'8    d'8    e'8    fis'8    a'8  \bar "|"  
 b'8    a'8    a'8    fis'8    e'8    d'8    e'8    fis'8  \bar "|"   \bar "|"  
 a'8    b'8    a'8    fis'8    d'8    e'8    fis'8    d'8  \bar "|" 
\tuplet 3/2 {   g'8    g'8    g'8  }   b'8    g'8    d'8    g'8    b'8    g'8  
\bar "|"   a'8    b'8    a'8    fis'8    d'8    e'8    fis'8    g'8  \bar "|"   
b'8    g'8    a'8    g'8    fis'8    d'8    d'8    fis'8  \bar "|"   \bar "|"   
a'4.    a'8    d'8    e'8    fis'8    g'8  \bar "|"   g'4    b'8    g'8    d''8 
   g'8    b'8    g'8  \bar "|"   a'4.    fis'8    fis'4.    a'8  \bar "|"   b'8 
   fis'8    a'8    fis'8    e'8    fis'8    d'4  \bar "|"   \bar "|"   a'8    
b'8    a'8    fis'8    d'8    e'8  \tuplet 3/2 {   fis'8    e'8    d'8  } 
\bar "|"   g'8    a'8    b'8    g'8    d''8    b'8    a'8    g'8  \bar "|"   
a'8    b'8    a'8    fis'8    e'8    d'8    d''8    cis''8  \bar "|"   b'8    
a'8    a'8    fis'8    e'8    d'8    d''8    b'8  \bar "|"   \bar "|" <<   a'2  
  d'2   >>   fis''8    fis''8    e''8    fis''8  \bar "|"   fis''8    g''4    
e''8    fis''8    e''8    d''8    b'8  \bar "|" \tuplet 3/2 {   a'8    b'8    
cis''8  }   d''8    e''8    fis''4.    a''8  \bar "|"   a''8    fis''8    d''8  
  fis''8    e''8    d''8    d''8    b'8  \bar "|"   \bar "|"   a'8    b'8    
d''8    e''8    fis''4.    e''8  \bar "|" \tuplet 3/2 {   g''8    g''8    g''8  
}   fis''8    d''8    e''8    d''8    b'8    d''8  \bar "|"   a'8    b'8    
d''8    e''8  <<   d''4.    fis''4.   >> a''8  \bar "|"   a''8    fis''8    
d''8    fis''8    e''8    d''8    b'8    d''8  \bar "|"   \bar "|"   a'8    b'8 
   d''8    e''8    fis''8    d''8  \tuplet 3/2 {   d''8    d''8    d''8  } 
\bar "|"   g''8    d''8    fis''8    d''8    e''8    d''8  \tuplet 3/2 {   d''8  
  d''8    d''8  } \bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8  
\tuplet 3/2 {   d''8    d''8    d''8  } \bar "|"   a''8    fis''8    e''8    
d''8    e''8    d''8  \tuplet 3/2 {   b'8    cis''8    d''8  } \bar "|"   
\bar "|"   a'8    b'8    d''8    e''8    fis''8    e''8    fis''8    a''8  
\bar "|"   g''4    fis''8    g''8    e''8    d''8  \tuplet 3/2 {   b'8    b'8    
b'8  } \bar "|"   a'8    b'8    d''8    e''8    fis''8    e''8    fis''8    
b''8  \bar "|"   a''8    fis''8    d''8    fis''8    \tuplet 3/2 {   e''8    
fis''8    e''8  } <<   d''2    d'2   >> \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
