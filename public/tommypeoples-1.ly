\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "milesnagopaleen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1323#setting1323"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1323#setting1323" {"https://thesession.org/tunes/1323#setting1323"}}}
	title = "Tommy Peoples'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   \repeat volta 2 {   fis'8    g'8  \bar "|"   a'8    
fis'8    a'4    d''8    cis''8  \bar "|"   b'8    g'8    b'4    e''8    fis''8  
\bar "|"   g''8    e''8    cis''8    a'8    b'8    cis''8  \bar "|"   b'8    
a'8    fis'4    fis'8    g'8  \bar "|"     a'8    fis'8    a'4    d''8    
cis''8  \bar "|"   b'8    g'8    b'4    e''8    fis''8  \bar "|"   g''8    e''8 
   cis''8    a'8    b'8    cis''8  \bar "|"   d''2  }     \repeat volta 2 {   
fis''8    g''8  \bar "|"   a''8    fis''8    d''8    a'8    cis''8    d''8  
\bar "|"   cis''4    b'4    e''8    fis''8  \bar "|"   g''8    e''8    cis''8   
 a'8    b'8    cis''8  \bar "|"   b'8    a'8    fis'4    fis''8    g''8  
\bar "|"     a''8    fis''8    d''8    a'8    cis''8    d''8  \bar "|"   cis''4 
   b'4    e''8    fis''8  \bar "|"   g''8    e''8    cis''8    a'8    b'8    
cis''8  \bar "|"   d''2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
