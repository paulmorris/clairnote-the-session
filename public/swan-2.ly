\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Conán McDonnell"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1036#setting14265"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1036#setting14265" {"https://thesession.org/tunes/1036#setting14265"}}}
	title = "Swan, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'4 ^"G"   b'16    a'16    g'8    
d''8 ^"G"   g'8    b'8    d''8  \bar "|"   g''4 ^"G"   fis''8    g''8    e''8 
^"Em"   g''8    d''8    b'8  \bar "|"   c''8 ^"Am"   a'8    e''8    c''8    
b'16 ^"G"   c''16    d''8    g'8    b'8  \bar "|"   a'8 ^"D"   d'8    fis'8    
a'8    d''8 ^"G"   b'8    c''8    a'8  \bar "|"     g'4 ^"G"   b'16    a'16    
g'8    d''8 ^"G"   g'8    b'8    d''8  \bar "|"   g''4 ^"G"   fis''8    g''8    
e''8 ^"Em"   g''8    d''8    b'8  \bar "|"   c''8 ^"Am"   a'8    e''8    c''8   
 b'16 ^"G"   c''16    d''8    g'8    b'8  \bar "|"   a'8 ^"D"   d'8    fis'8    
a'8    g'4. ^"G"   r8 }   \repeat volta 2 {   g''8 ^"G"   g'8    b'8    g''8    
fis''8 ^"Am"   a'8    c''8    e''8  \bar "|"   e''8 ^"Em"   d''8    b'16    
a'16    g'8    a'8 ^"D"   d'8    fis'16    e'16    d'8  \bar "|"   g'8 ^"G"   
b'8    b'16    b'16    b'8    c''8 ^"C"   e''8    a''8    g''8  \bar "|"   
fis''16 ^"Am"   g''16    a''8    e''8    a''8    cis''8 ^"A"   d''8    e''8    
fis''8  \bar "|"     g''8 ^"G"   g'8    b'8    g''8    fis''8 ^"Am"   a'8    
c''8    e''8  \bar "|"   e''8 ^"Em"   d''8    b'16    a'16    g'8    a'8 ^"D"   
d'8    fis'16    e'16    d'8  \bar "|"   g'8 ^"G"   b'8    b'16    b'16    b'8  
  c''8 ^"C"   e''8    a''8    g''8  \bar "|"   fis''16 ^"D"   e''16    d''8    
c''16    b'16    a'8    g'8 ^"G"   a'8    b'8    d''8  }   \bar "|"   g''8 
^"G"^"~"    g''8    g''8    b''8    g''8 ^"G"   d''8    b'8    d''8  \bar "|"   
c''8 ^"C"   b'8    a'8    g'8    fis'8 ^"D"   a'8    d''8    c''8  \bar "|"   
b'8 ^"G"   g'8    g''8    fis''8    e''8 ^"A"   cis''8    a''8    g''8  
\bar "|"   fis''16 ^"Am"   g''16    a''8    e''8    a''8    cis''8 ^"A"   d''8  
  e''8    fis''8  \bar "|"     g''8 ^"G"   g'8    b'8    g''8    fis''8 ^"Am"   
a'8    c''8    e''8  \bar "|"   e''8 ^"Em"   d''8    b'16    a'16    g'8    a'8 
^"D"   d'8    fis'16    e'16    d'8  \bar "|"   g'8 ^"G"   b'8    b'16    b'16  
  b'8    c''8 ^"C"   e''8    a''8    g''8  \bar "|"   fis''16 ^"D"   e''16    
d''8    c''16    b'16    a'8    g'8 ^"G"   a'8    b'8    d''8  \bar "|"   
\bar "|"   g''8 ^"G"^"~"    g''8    g''8    b''8    g''8 ^"G"   d''8    b'8    
d''8  \bar "|"   c''8 ^"C"   b'8    a'8    g'8    fis'8 ^"D"   a'8    d''8    
c''8  \bar "|"   b'8 ^"G"   g'8    g''8    fis''8    e''8 ^"A"   cis''8    a''8 
   g''8  \bar "|"   fis''16 ^"Am"   g''16    a''8    e''8    a''8    cis''8 
^"A"   d''8    e''8    fis''8  \bar "|"     g''8 ^"G"   g'8    b'8    g''8    
fis''8 ^"Am"   a'8    c''8    e''8  \bar "|"   e''8 ^"Em"   d''8    b'16    
a'16    g'8    a'8 ^"D"   d'8    fis'16    e'16    d'8  \bar "|"   g'8 ^"G"   
b'8    b'16    b'16    b'8    c''8 ^"C"   e''8    a''8    g''8  \bar "|"   
fis''16 ^"D"   e''16    d''8    c''16    b'16    a'8    g'4. ^"G"   r8 \bar "|" 
  
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
