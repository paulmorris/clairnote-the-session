\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ebarr"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/75#setting24681"
	arranger = "Setting 16"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/75#setting24681" {"https://thesession.org/tunes/75#setting24681"}}}
	title = "Miss McLeod's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   g'4    b'8    g'8    d'8    g'8   
 b'8    g'8  \bar "|"   b'4. ^"~"    a'8    b'8    c''8    b'8    a'8  \bar "|" 
  g'4    b'8    g'8    d'8    g'8    b'8    g'8  \bar "|"   a'4    a'8    fis'8 
   a'8    c''8    b'8    a'8  \bar "|"     g'4    b'8    g'8    d'8    g'8    
b'8    g'8  \bar "|"   b'4. ^"~"    a'8    b'8    c''8    d''4  \bar "|"   e''8 
   d''8    c''8    b'8    c''8    d''8    e''8    fis''8  \bar "|"   g''4. 
^"~"    d''8    e''8    c''8    a'8    fis'8  }     \repeat volta 2 {   g'4    
g''8    fis''8    e''8    fis''8    g''8    d''8  \bar "|"   b'4. ^"~"    a'8   
 b'8    c''8    b'8    a'8  \bar "|"   g'4    g''8    fis''8    e''8    fis''8  
  g''8    e''8  \bar "|"   a''4    a''8    b''8    a''8    g''8    e''8    
fis''8  \bar "|"     g''4. ^"~"    fis''8    e''8    fis''8    g''8    e''8  
\bar "|"   b'4. ^"~"    a'8    b'8    c''8    d''4  \bar "|"   e''8    d''8    
c''8    b'8    c''8    d''8    e''8    fis''8  \bar "|"   g''4. ^"~"    d''8    
e''8    c''8    a'8    fis'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
