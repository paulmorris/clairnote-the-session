\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/441#setting13304"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/441#setting13304" {"https://thesession.org/tunes/441#setting13304"}}}
	title = "John Ryan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   d''8    d''8    b'8    d''16    b'16    \bar "|"   
a'8    fis'8    a'8    fis'8    \bar "|"   d''8    d''8    b'16    cis''16    
d''16    b'16    \bar "|"   a'8    fis'8    e'8    d'8    \bar "|"     d''8    
d''8    b'16    cis''16    d''16    b'16    \bar "|"   a'8    fis'8    a'8    
d''16    e''16    \bar "|"   fis''8    d''8    e''8    cis''8    \bar "|"   
d''4    d''4    \bar "|"     d''4    b'8    d''16    b'16    \bar "|"   a'8    
fis'8    a'16    b'16    cis''8    \bar "|"   d''4    b'16    cis''16    d''8   
 \bar "|"   a'8    fis'8    e'16    fis'16    e'16    d'16    \bar "|"     d''8 
   d''8    b'4    \bar "|"   a'16    b'16    a'16    fis'16    a'8.    e''16    
\bar "|"   fis''8    d''8    e''8    cis''8    \bar "|"   d''8.    a'16    b'16 
   cis''16    d''16    e''16    \bar "||"     fis''8    d''8   ~    d''8    
e''16    fis''16    \bar "|"   g''16    a''16    g''16    fis''16    e''8    
d''16    e''16    \bar "|"   fis''8    d''8    a''8    d''16    e''16    
\bar "|"   fis''8    d''16    fis''16    a''8    a''16    g''16    \bar "|"     
fis''8    d''8    d''8.    fis''16    \bar "|"   g''8    fis''8    e''4    
\bar "|"   fis''8    d''8    e''16    d''16    cis''8    \bar "|"   d''4    
d''8    e''8    \bar "|"     fis''4    fis''8    d''16    fis''16    \bar "|"   
g''8    fis''8    e''8    d''8    \bar "|"   fis''8    d''8    a'8    d''8    
\bar "|"   fis''8    d''16    fis''16    a''16    b''16    a''16    g''16    
\bar "|"     fis''16    g''16    fis''16    e''16    d''8    e''16    fis''16   
 \bar "|"   g''8.    fis''16    e''8    a''16    g''16    \bar "|"   fis''4    
e''16    d''16    cis''8    \bar "|"   d''2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
