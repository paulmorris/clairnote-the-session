\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Madelyn"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/789#setting28715"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/789#setting28715" {"https://thesession.org/tunes/789#setting28715"}}}
	title = "Primrose Lasses, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   cis''4.    b'8    b'8    a'8    fis'8    a'8  
\bar "|"   e'8    fis'8    a'8    b'8    cis''8    b'8    b'4  \bar "|"   
fis''8    cis''8    cis''16    cis''16    cis''8    b'8    cis''8    a'8    
fis'8  \bar "|"   e'8    fis'8    a'8    b'8    cis''8    a'8    a'8    b'8  }  
   cis''4.    fis''8    fis''8    e''8    a''8    e''8  \bar "|"   fis''8    
e''8    a''8    e''8    fis''8    e''8    d''8    e''8  \bar "|"   b'8    
cis''4    fis''8    fis''8    e''8    a''8    e''8  \bar "|"   fis''8    fis''8 
   b'8    d''8    cis''8    a'8    a'8    b'8  \bar "|"     cis''8    e''8    
e''16    e''16    e''8    fis''8    e''8    a''8    e''8  \bar "|"   fis''4    
fis''8    a''8    fis''8    e''8    cis''8    e''8  \bar "|"   a''8    gis''8   
 fis''8    e''8    fis''4    a''8    fis''8  \bar "|"   e''8    cis''8    b'8   
 d''8    cis''8    a'8    a'8    b'8  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
