\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/250#setting21776"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/250#setting21776" {"https://thesession.org/tunes/250#setting21776"}}}
	title = "Road To Lisdoonvarna, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key e \dorian   d'8  \repeat volta 2 {   e'4 ^"Em"   b'8    b'4    
a'8    b'4    cis''8    d''4.  \bar "|"   fis'4 ^"D"   a'8    a'8    b'8    a'8 
   d'4    e'8    fis'8    e'8    d'8  \bar "|"       e'4 ^"Em"   b'8    b'4    
a'8    b'4    cis''8    d''4.  \bar "|"   cis''8 ^"D"   d''8    cis''8    b'4   
 a'8      b'4 ^"Em"   e'8    e'4.  }     \repeat volta 2 {   e''4 ^"Em"   
fis''8    g''8    fis''8    e''8      d''4 ^"Bm"   b'8    b'8    cis''8    d''8 
 \bar "|"   cis''4 ^"A"   a'8    a'8    b'8    cis''8      d''4 ^"Bm"   b'8    
b'4.  \bar "|"       e''4 ^"Em"   fis''8    g''8    fis''8    e''8      d''4 
^"Bm"   b'8    b'8    cis''8    d''8  \bar "|"   cis''8 ^"A"   d''8    cis''8   
 b'4    a'8      b'4 ^"Em"   e'8    e'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
