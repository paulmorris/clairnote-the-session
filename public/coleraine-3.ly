\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/17#setting22459"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/17#setting22459" {"https://thesession.org/tunes/17#setting22459"}}}
	title = "Coleraine, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key a \minor   \repeat volta 2 {   e'8 ^"Am"   a'8    a'8    a'8    
b'8    c''8  \bar "|"   b'8 ^"G"   e''8    e''8    e''4    d''8  \bar "|"     
c''8 ^"C"   b'8    a'8    a'8    b'8    c''8  \bar "|"     b'8 ^"E"   g'8    
e'8    e'4    d'8  \bar "|"       e'8 ^"Am"   a'8    a'8    a'8    b'8    c''8  
\bar "|"   b'8 ^"G"   e''8    e''8    e''4    d''8  \bar "|"   c''8 ^"Am"   b'8 
   a'8      gis'8 ^"E"   a'8    b'8  \bar "|"   a'4 ^"Am"   a'8    a'4.  }      
 c''4. ^"C"   c''8    b'8    a'8  \bar "|"   b'8 ^"G"   g''8    g''8    g''4    
gis''8  \bar "|"   a''8 ^"Am"   g''8    e''8    d''8    c''8    b'8  \bar "|"   
gis'8 ^"E"   a'8    b'8    e'4.  \bar "|"       a'8 ^"Am"   gis'8    a'8      
b'8 ^"Bm"   a'8    b'8  \bar "|"   c''8 ^"C"   b'8    c''8      e''4 ^"Dm"   
d''8  \bar "|"   c''8 ^"Am"   b'8    a'8      gis'8 ^"E"   a'8    b'8  \bar "|" 
  a'4 ^"Am"   a'8    a'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
