\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "GoldenKeyboard"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/374#setting13195"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/374#setting13195" {"https://thesession.org/tunes/374#setting13195"}}}
	title = "Toss The Fiddles"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"     g''2 ^"G"     g''4 ^"G"   \tuplet 3/2 {   
b'8 ^"D7"   bes'8    a'8  } \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
