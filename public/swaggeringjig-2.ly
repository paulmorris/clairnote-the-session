\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/661#setting13695"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/661#setting13695" {"https://thesession.org/tunes/661#setting13695"}}}
	title = "Swaggering Jig, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key e \minor   c''8  \bar "|"   b'8    a'8    g'8    e'8    g'8    
g'8    e'8    g'8    g'8  \bar "|"   b'8    a'8    g'8    e'8    fis'8    g'8   
 a'4    c''8  \bar "|"   b'8    a'8    g'8    e'8    g'8    g'8    e'8    g'8   
 g'8  \bar "|"   b'8    a'8    g'8    e'8    fis'8    e'8    d'4  }   
\repeat volta 2 {   c''8  \bar "|"   b'8    d''8    d''8    d''8    e''8    
d''8    d''8    b'8    g'8  \bar "|"   c''8    e''8    e''8    e''8    fis''8   
 e''8    e''4    fis''8  \bar "|"   g''8    fis''8    e''8    d''8    c''8    
b'8    a'8    g'8    a'8  \bar "|"   a'8    g'8    a'8    b'8    e'8    e'8    
e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
