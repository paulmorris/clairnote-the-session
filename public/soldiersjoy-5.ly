\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fynnjamin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1356#setting14704"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1356#setting14704" {"https://thesession.org/tunes/1356#setting14704"}}}
	title = "Soldier's Joy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis'8    g'8  \bar "|"   a'8    
fis'8    d'8    fis'8    a'8    fis'8    d'8    fis'8  \bar "|"   d'4    d''2   
 cis''8    b'8  \bar "|"   a'8    fis'8    d'8    fis'8    a'8    fis'8    d'8  
  fis'8  \bar "|"   g'4    e'2    fis'8    g'8  \bar "|"   \bar "|"   a'8    
fis'8    d'8    fis'8    a'8    fis'8    d'8    fis'8  \bar "|"   d'4    d''2   
 fis''8    g''8  \bar "|"   a''8    fis''8    d''8    fis''8    g''8    e''8    
cis''8    e''8  \bar "|"   d''4    d''2  }   \repeat volta 2 {   e''4  \bar "|" 
  fis''8    e''8    fis''8    g''8    fis''8    a''8    g''8    fis''8  
\bar "|"   e''8    d''8    cis''8    d''8    e''8    fis''8    g''8    e''8  
\bar "|"   fis''8    e''8    fis''8    g''8    fis''8    a''8    g''8    fis''8 
 \bar "|"   e''8    d''8    cis''8    b'8    a'4    a'4  \bar "|"   \bar "|"   
fis''8    e''8    fis''8    g''8    fis''8    a''8    g''8    fis''8  \bar "|"  
 e''8    d''8    cis''8    d''8    e''8    fis''8    g''8    e''8  \bar "|"   
a''8    fis''8    d''8    fis''8    g''8    e''8    cis''8    e''8  \bar "|"   
d''4    d''2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
