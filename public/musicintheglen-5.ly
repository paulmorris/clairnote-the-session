\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Lily Manzueta"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/303#setting25660"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/303#setting25660" {"https://thesession.org/tunes/303#setting25660"}}}
	title = "Music In The Glen"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   a'4    cis''8    a'8    fis'8    
a'8    e'8    d'8    \bar "|"   cis'8    e'8    a'8    cis''8    b'8    a'8    
b'8    cis''8    \bar "|"   d''4    fis''8    d''8    cis''8    d''8    e''8    
cis''8    \bar "|"   d''8    b'8    cis''8    a'8    b'8    a'8    fis'8    e'8 
   \bar "|"     a'8    b'8    cis''8    a'8    fis'8    a'8    e'8    d'8    
\bar "|"   cis'8    e'8    a'8    cis''8    b'8    a'8    b'8    cis''8    
\bar "|"   d''8    e''8    fis''8    gis''8    a''4    a''8    fis''8    
\bar "|"   e''8    cis''8    b'8    cis''8    a'4    e'4    }     
\repeat volta 2 {   cis''16    b'16    a'8    e''8    a'8    fis''8    a'8    
e''8    a'8    \bar "|"   cis''8    a'8    e''8    a'8    d''8    b'8    b'8    
b'8    \bar "|"   cis''16    b'16    a'8    cis''8    e''8    fis''8    e''8    
fis''8    gis''8    \bar "|"   a''8    e''8    cis''8    a'8    b'8    a'8    
fis'8    a'8    \bar "|"     cis''8    a'8    cis''8    e''8    fis''8    e''8  
  fis''8    gis''8    \bar "|"   a''4    gis''8    a''8    b''8    gis''8    
e''8    gis''8    \bar "|"   a''4    gis''8    a''8    fis''8    a''8    e''8   
 cis''8    \bar "|"   d''8    b'8    cis''8    a'8    b'8    a'8    e'8    a'8  
  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
