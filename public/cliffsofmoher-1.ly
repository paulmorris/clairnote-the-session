\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1390#setting1390"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1390#setting1390" {"https://thesession.org/tunes/1390#setting1390"}}}
	title = "Cliffs Of Moher, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \dorian   b'8    e''8    e''8    d''8    e''8    e''8  
\bar "|"   b'8    e''8    e''8    d''8    b'8    a'8  \bar "|"   fis'4    d'8   
 a8    d'8    d'8  \bar "|"   fis'8    e'8    d'8    fis'8    g'8    a'8  
\bar "|"     b'8    e''8    e''8    d''8    e''8    e''8  \bar "|"   b'8    
e''8    e''8    d''8    b'8    a'8  \bar "|"   fis'4    d'8    a8    d'8    d'8 
 \bar "|"   fis'8    e'8    d'8    e'4.  }     \repeat volta 2 {   b'4. ^"~"    
b'8    a'8    fis'8  \bar "|"   b'8    a'8    fis'8    a'8    fis'8    e'8  
\bar "|"   fis'4    d'8    a8    d'8    d'8  \bar "|"   fis'8    e'8    d'8    
fis'8    g'8    a'8  \bar "|"     b'4. ^"~"    b'8    a'8    fis'8  \bar "|"   
b'8    d''8    b'8    a'8    fis'8    e'8  \bar "|"   fis'4    d'8    a8    d'8 
   d'8  \bar "|"   fis'8    e'8    d'8    e'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
