\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Nigel Gatherer"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1415#setting21833"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1415#setting21833" {"https://thesession.org/tunes/1415#setting21833"}}}
	title = "Farewell To Glasgow"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key e \minor   d''4    \bar "|"   e''2    b'4    \bar "|"   d''2    
e''8    d''8    \bar "|"   b'4.    a'8    g'4    \bar "|"   d''2    e''8    
d''8    \bar "|"   b'4.    a'8    g'4    \bar "|"   e'2    d'4    \bar "|"   
e'2.    \bar "|"   e'2      d'4    \bar "|"   e'4.    e'8    e'4    \bar "|"   
g'4.    a'8    b'4    \bar "|"   d''4.    e''8    d''4    \bar "|"   b'2    
d''4    \bar "|"   e''2    fis''4    \bar "|"   e''2    d''4    \bar "|"   b'2. 
   \bar "|"   b'2      d'4    \bar "|"   e'4.    e'8    e'4    \bar "|"   g'4.  
  a'8    b'4    \bar "|"   d''4.    e''8    d''4    \bar "|"   b'2    d''4    
\bar "|"   e''2    fis''4    \bar "|"   e''2    d''4    \bar "|"   b'2.    
\bar "|"   b'2      d''4    \bar "|"   e''2    b'4    \bar "|"   d''2    e''8   
 d''8    \bar "|"   b'4.    a'8    g'4    \bar "|"   d''2    e''8    d''8    
\bar "|"   b'4.    a'8    g'4    \bar "|"   e'2    d'4    \bar "|"   e'2.    
\bar "|"   e'2    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
