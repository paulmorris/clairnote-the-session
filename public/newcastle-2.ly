\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/684#setting13731"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/684#setting13731" {"https://thesession.org/tunes/684#setting13731"}}}
	title = "Newcastle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \minor   \repeat volta 2 {   bes'8    a'8  \bar "|"   bes'4    
bes4    \tuplet 3/2 {   d''8    c''8    bes'8  }   \tuplet 3/2 {   a'8    g'8    
f'8  } \bar "|"   ees'4    g''4    g''4    f''8    ees''8  \bar "|"   d''8    
f''8    bes'8    d''8    c''8    ees''8    a'8    c''8  \bar "|"   bes'8    
bes''8    a''8    g''8    f''8    ees''8    d''8    c''8  \bar "|"   bes'4    
bes4    \tuplet 3/2 {   d''8    c''8    bes'8  }   \tuplet 3/2 {   a'8    g'8    
f'8  } \bar "|"   ees'4    g''4    g''4    f''8    ees''8  \bar "|"   d''8    
f''8    bes'8    d''8    c''8    ees''8    a'8    c''8  \bar "|"   bes'8    a'8 
   bes'8    c''8    bes'4  }   \repeat volta 2 {   d'8    c'8  \bar "|"   bes8  
  d'8    f'8    bes'8    d''8    f''8    bes''8    a''8  \bar "|"   g''8    
fis''8    g''8    a''8    g''4    c'8    bes8  \bar "|"   a8    c'8    f'8    
a'8    c''8    f''8    a''8    g''8  \bar "|"   f''8    e''8    f''8    g''8    
f''4    d'8    c'8  \bar "|"   bes8    d'8    f'8    bes'8    d''8    f''8    
bes''8    a''8  \bar "|"   g''8    fis''8    g''8    a''8    g''8    d''8    
ees''8    e''8  \bar "|"   f''8    ees''8    f''8    g''8    f''8    ees''8    
c''8    a'8  \bar "|"   bes'8    a'8    bes'8    c''8    bes'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
