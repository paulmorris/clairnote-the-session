\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "shanachie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/897#setting897"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/897#setting897" {"https://thesession.org/tunes/897#setting897"}}}
	title = "Fern Height"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major     g'8. ^"G"   a'16    b'8    d''8  \bar "|"   c''8 
^"Am"   b'8    a'8    g'8  \bar "|"   fis'16 ^"D"   g'16    fis'16    e'16    
d'8.    e'16  \bar "|"   fis'8    d'8    fis'8    a'8  \bar "|"       g'8. ^"G" 
  a'16    b'8    d''8  \bar "|"   c''8 ^"Am"   b'8    a'8    g'8  \bar "|"   
fis'16 ^"D"   g'16    fis'16    e'16    d'8.    e'16  \bar "|"   fis'8    a'8   
 g'8 ^"G"   d'8  \bar ":|."   fis'8    a'8    g'8 ^"G"   a'8  \bar "||"       
b'8 ^"G"   g''8    g''8.    fis''16  \bar "|"   e''8 ^"Em"   d''8    c''8    
b'8  \bar "|"   c''8 ^"Am"   a''8      a''8. ^"A"   g''16  \bar "|"   fis''16 
^"D"   e''16    d''8    e''8    fis''8  \bar "|"       g''8 ^"G"   b'8      
c''8 ^"C"   e''8  \bar "|"   d''8 ^"G"   b'8      c''8 ^"C"   a'8  \bar "|"   
fis'16 ^"D"   g'16    fis'16    e'16    d'8.    e'16  \bar "|"   fis'8    a'8   
 g'8 ^"G"   a'8  \bar ":|."   fis'8    a'8    g'8 ^"G"   d'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
