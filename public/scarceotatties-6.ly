\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/95#setting27679"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/95#setting27679" {"https://thesession.org/tunes/95#setting27679"}}}
	title = "Scarce O' Tatties"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key a \dorian   a4    e'8    e'8    fis'8    g'8  \bar "|"   e'8    
d'8    b8    d'8    b8    g8  \bar "|"   a4    e'8    e'8    fis'8    g'8  
\bar "|"   e'8    d'8    b8    a8    c'8    e'8  \bar "|"     a'4.    g'4.  
\bar "|"   fis'4.    e'8    b8    g8  \bar "|"   a4    e'8    e'8    fis'8    
g'8  \bar "|"   e'8    d'8    b8    a4.  }     \repeat volta 2 {   a'8    e'8   
 g'8    a'4    fis'8  \bar "|"   g'4    fis'8    fis'8    a4  \bar "|"   a'8    
e'8    g'8    a'4    fis'8  \bar "|"   g'4    fis'8    e'4.  \bar "|"     e'4.  
  a4.  \bar "|"   d'4    fis'8    fis'8    e'4  \bar "|"   a'4    e'8    e'8    
fis'8    g'8  } \alternative{{   e'8    d'8    b8    a'4.  } {   e'8    d'8    
b8    a4.  \bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
