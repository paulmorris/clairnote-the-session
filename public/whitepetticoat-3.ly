\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Caulfield05"
	area = "|: E5 | E5 | B5 C5 | B5 A5 | E5 | E5 | B5 C5 | G5 Em |
B: | C5 | C5 | A5 | Em | B5 C5 | B5 | B5 A5 | G5 Em |
"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/332#setting13114"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/332#setting13114" {"https://thesession.org/tunes/332#setting13114"}}}
	title = "White Petticoat, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
