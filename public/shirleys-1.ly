\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1188#setting1188"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1188#setting1188" {"https://thesession.org/tunes/1188#setting1188"}}}
	title = "Shirley's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   e''8    d''8  \bar "|" 
\tuplet 3/2 {   cis''8    d''8    cis''8  }   b'8    cis''8    a'8    fis'8    
fis'4 ^"~"  \bar "|"   a'8    cis''8    e''8    a''8    e''8    fis''8  
\grace {    g''8  }   fis''8    e''8  \bar "|"   fis''16    a''8.  \grace {    
b''8  }   a''8    fis''8    e''8    cis''8    cis''4 ^"~"  \bar "|"   e''8    
a''8    fis''8    e''8    b'8    cis''8  \grace {    d''8  }   cis''8    b'8  
\bar "|"     e''8    a'8    \tuplet 3/2 {   cis''8    b'8    a'8  }   fis'8    
a'8    e'8    fis'8  \bar "|"   a'8    cis''8    e''8    a''8    e''8    fis''8 
 \grace {    g''8  }   fis''8    e''8  \bar "|"   fis''16    a''8.  \grace {    
b''8  }   a''8    fis''8    e''8    cis''8    a'8    cis''8  \bar "|"   b'8    
e'8    gis'!8    b'8    a'4  }     \repeat volta 2 {   cis''8    d''8  \bar "|" 
  e''4. ^"~"    fis''8    e''8    a'8    cis''8    e''8  \bar "|"   d''8    
fis''8    a''8    fis''8    e''8    cis''8    cis''8    d''8  \bar "|"   e''4. 
^"~"    fis''8    e''8    cis''8  \grace {    d''8  }   cis''8    b'8  \bar "|" 
  a'8    d''8    cis''8    d''8    b'8    e'8    cis''8    d''8  \bar "|"     
e''4. ^"~"    fis''8    e''8    a'8    cis''8    e''8  \bar "|"   d''8    
fis''8    a''8    fis''8    e''8    cis''8    cis''8    e''8  \bar "|" 
\grace {    cis''8  }   d''4    fis''8    d''8    cis''8    d''8    e''8    
cis''8  \bar "|"   b'8    e'8    gis'8    b'8    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
