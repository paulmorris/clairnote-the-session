\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/670#setting26973"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/670#setting26973" {"https://thesession.org/tunes/670#setting26973"}}}
	title = "Curlew Hills, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   d'4    \bar "|"   g'8    a'8    
b'8    c''8    d''8    g''8    fis''8    a''8    \bar "|"   g''4    b'4    d''2 
   \bar "|"   c''8    e''8    a'8    b'8    c''4    c''4    \bar "|"   b'8    
d''8    g'8    a'8    b'8    c''8    b'8    a'8    \bar "|"     g'8    a'8    
b'8    c''8    d''8    g''8    fis''8    a''8    \bar "|"   g''4    b'4    d''2 
   \bar "|"   c''8    e''8    a'8    b'8    c''8    a'8    fis'8    g'8    
\bar "|"   a'4    g'4    g'4    }     \repeat volta 2 {   b'8    c''8    
\bar "|"   d''4    b''4    c''4    a''4    \bar "|"   b'8    g''8    g''8    
fis''8    g''8    d''8    b'8    g'8    \bar "|"   fis'8    g'8    a'8    b'8   
 c''4    b'8    a'8    \bar "|"   g'8    fis'8    g'8    a'8    b'8    a'8    
b'8    c''8    \bar "|"     d''4    b''4    c''4    a''4    \bar "|"   b'8    
g''8    g''8    fis''8    g''8    d''8    b'8    g'8    \bar "|"   fis'8    g'8 
   a'8    b'8    c''8.    a'16    fis'8    g'8    \bar "|"   a'4    g'4    g'4  
  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
