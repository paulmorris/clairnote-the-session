\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1120#setting14384"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1120#setting14384" {"https://thesession.org/tunes/1120#setting14384"}}}
	title = "Tame Her When Da Snaw Comes"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g''8    fis''8    e''8    d''8    e''8    g''8    b'4 
 \bar "|"   a'8    g'8    a'8    b'8    a'8    g'8    a'8    b'8  \bar "|"   
g''8    fis''8    e''8    d''8    e''8    g''8    b'4  \bar "|"   a'8    g'8    
a'8    b'8    g'4    g'4  }   \repeat volta 2 { <<   g'4    g4   >> <<   g'4    
g4   >>   e'8    g'8    d'8    e'8  \bar "|"   g'4    g'4    a'8    c''8    b'8 
   a'8  \bar "|" <<   g'4    g4   >> <<   g'4    g4   >>   e'8    g'8    d'8    
e'8  \bar "|"   g'8    b'8    a'8    fis'8    g'4    g'4  }   \repeat volta 2 { 
  g''8    fis''8    e''8    d''8    e''8    g''8    b'4  \bar "|"   a'8    g'8  
  a'8    b'8    a'8    g'8    a'8    b'8  \bar "|"   g''8    fis''8    e''8    
d''8    e''8    g''8    b'4  \bar "|"   a'8    g'8    a'8    b'8    g'4    g'4  
}   \repeat volta 2 { <<   e'8    g8   >> <<   g'8    g8   >> <<   d'8    g8   
>> <<   g'8    g8   >> <<   e'8    g8   >> <<   g'8    g8   >> <<   d'4    g4   
>> \bar "|" <<   d'8    g8   >> g'8    a'8    b'8    c''8    b'8    a'8    g'8  
\bar "|" <<   e'8    g8   >> <<   g'8    g8   >> <<   d'8    g8   >> <<   g'8   
 g8   >> <<   e'8    g8   >> <<   g'8    g8   >> <<   d'4    g4   >> 
} \alternative{{ <<   g'8    g8   >> b'8    a'8    fis'8    g'4    g'4  } { <<  
 g'8    g8   >> b'8    a'8    fis'8  <<   g'4    g4   >> <<   g'4    g4   >> 
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
