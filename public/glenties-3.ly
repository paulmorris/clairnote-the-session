\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1332#setting14670"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1332#setting14670" {"https://thesession.org/tunes/1332#setting14670"}}}
	title = "Glenties, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   \repeat volta 2 {   g'8.    a'16    b'4    b'8.    
a'16  \bar "|"   b'8.    e'16    d'4    \tuplet 3/2 {   d'8    d'8    d'8  } 
\bar "|"   b8.    d'16    e'4    \tuplet 3/2 {   e'8    e'8    e'8  } \bar "|"   
c'8.    e'16    d'4    \tuplet 3/2 {   d'8    d'8    d'8  } \bar "|"   g'8.    
a'16    b'4    b'8.    a'16  \bar "|"   g'8.    e'16    d'4    \tuplet 3/2 {   
d'8    d'8    d'8  } \bar "|"   b8.    d'16    e'4    a'4  \bar "|"   fis'4    
g'2  }   \repeat volta 2 {   g'8.    a'16    b'4    b'8.    d''16  \bar "|"   
c''8.    b'16    c''4    \tuplet 3/2 {   c''8    c''8    c''8  } \bar "|"   d'8. 
   fis'16    a'4    a'8.    c''16  \bar "|"   b'8.    a'16    b'4    
\tuplet 3/2 {   b'8    b'8    b'8  } \bar "|"   g'8.    a'16    b'4    b'8.    
d''16  \bar "|"   c''8.    b'16    c''4    \tuplet 3/2 {   c''8    c''8    c''8  
} \bar "|"   d'8.    fis'16    a'4    a'8.    c''16  \bar "|"   b'8.    a'16    
g'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
