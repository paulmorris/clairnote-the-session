\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1299#setting25317"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1299#setting25317" {"https://thesession.org/tunes/1299#setting25317"}}}
	title = "Jimmy's Return"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \repeat volta 2 {   e''4    a'8    b'8    c''8    
e''8    d''8    c''8  \bar "|"   b'8    g'8    b'8    d''8    g''8    d''8    
b'8    d''8  \bar "|"   e''4    a'8    b'8    c''8    e''8    d''8    c''8  
\bar "|"   b'8    c''8    d''8    b'8    c''8    a'8    a'4  \bar "|"     e''4  
  a'8    b'8    c''8    e''8    d''8    c''8  \bar "|"   b'8    g'8    b'8    
d''8    g''8    d''8    b'8    g'8  \bar "|"   a'8    b'8    c''8    a'8    b'8 
   c''8    d''8    f''8  \bar "|"   e''8    d''8    c''8    b'8    c''8    a'8  
  a'4  }     \repeat volta 2 {   |
 e''4    a''8    g''8    e''8    g''8    d''8    c''8  \bar "|"   b'8    g'8    
b'8    d''8    g''8    d''8    b'8    d''8  \bar "|"   e''4    a''8    g''8    
e''8    g''8    d''8    c''8  \bar "|"   b'8    c''8    d''8    b'8    c''8    
a'8    a'4  \bar "|"     e''4    a''8    g''8    e''8    g''8    d''8    c''8  
\bar "|"   b'8    g'8    b'8    d''8    g''8    d''8    b'8    g'8  \bar "|"   
a'8    b'8    c''8    a'8    b'8    c''8    d''8    f''8  \bar "|"   e''8    
d''8    c''8    b'8    c''8    a'8    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
