\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Loughcurra"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1148#setting14419"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1148#setting14419" {"https://thesession.org/tunes/1148#setting14419"}}}
	title = "Drag Her Round The Road"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   a'8    b'8  \bar "||"   c''8    fis'8  \grace {    
a'8  }   fis'8    e'8    e'8 (   fis'8  -)   a'8    fis'8  \bar "|" \grace {    
g'8  }   fis'8    e'8    c'8    e'8    e'8    fis'8    a'8    b'8  \bar "|"   
c''8    fis'8  \grace {    a'8  }   fis'8    e'8    fis'4    e'8    fis'8  
\bar "|"   a'8    b'8    c''8    a'8    b'8  \grace {    c''8  }   b'8    a'8   
 b'8  \bar "|"   c''8    fis'8  \grace {    a'8  }   fis'8    e'8    e'8 (   
fis'8  -)   a'8    fis'8  \bar "|" \grace {    g'8  }   fis'8    e'8    c'8    
e'8    e'8    fis'8    a'8    b'8  \bar "|"   c''8    fis'8  \grace {    a'8  } 
  fis'8    e'8    e'8 (   fis'8  -)   e'8 (   fis'8  -) \bar "|"   a'8    b'8   
 c''8    a'8    b'8    a'8    c''8    b'8  \bar "||"   a'4.    a'8    a'8    
b'8    c''8    e''8  \bar "|"   d''8    c''8    b'8    c''8    d''8    e''8    
fis''8    e''8  \bar "|"   c''4. ^"~"    b'8    a'8    c''8    e''8    fis''8  
\bar "|"   e''8    c''8    a'8    c''8    b'8    fis'8    fis'4  \bar "|"   a'8 
   e'8    c''8    e'8    a'8    c''8    e''8    c''8  \bar "|"   d''8    c''8   
 b'8    c''8    d''8    e''8    fis''8    r8 \bar "|"   a''8    fis''8    e''8  
  c''8    d''8    fis''8    a''8    fis''8  \bar "|"   e''8    c''8    a'8    
c''8    b'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
