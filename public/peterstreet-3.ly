\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dogbox"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1171#setting14439"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1171#setting14439" {"https://thesession.org/tunes/1171#setting14439"}}}
	title = "Peter Street"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   g'4    \tuplet 3/2 {   b'8    a'8    g'8  }   d''8    
g'8    \tuplet 3/2 {   b'8    a'8    g'8  } \bar "|"   d''8    g'8    g''8    
g'8    fis''8    g'8    e''8    g'8  \bar "|"   d''8    g'8    \tuplet 3/2 {   
b'8    a'8    g'8  }   d''8    g'8    e''8    g'8  \bar "|"   a'8    d'8    
c''8    d'8    b'8    d'8    a'8    d'8  \bar "|"     g'4    \tuplet 3/2 {   b'8 
   a'8    g'8  }   d''8    g'8    \tuplet 3/2 {   b'8    a'8    g'8  } \bar "|"  
 d''8    g'8    g''8    g'8    fis''8    g'8    e''8    g'8  \bar "|"   d''8    
e''8    d''8    b'8    d''8    g''8    d''8    b'8  \bar "|"   a'8    g'8    
a'8    b'8    g'2  }     \repeat volta 2 {   d'4    \tuplet 3/2 {   fis'8    e'8 
   d'8  }   a'8    d'8    \tuplet 3/2 {   fis'8    e'8    d'8  } \bar "|"   a'8  
  d'8    c''8    d'8    b'8    d'8    a'8    d'8  \bar "|"   g'4    
\tuplet 3/2 {   b'8    a'8    g'8  }   d''8    g'8    \tuplet 3/2 {   b'8    a'8  
  g'8  } \bar "|"   d''8    g'8    g''8    g'8    fis''8    g'8    e''8    g'8  
\bar "|"     d''8    cis''8    d''8    b'8    g''8    fis''8    g''8    e''8  
\bar "|"   d''8    cis''8    d''8    b'8    g''8    fis''8    g''8    e''8  
\bar "|"   d''8    b'8    g''8    fis''8    e''8    d''8    c''8    b'8  
\bar "|"   a'8    g'8    fis'8    e'8    d'8    e'8    fis'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
