\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "bobgreen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/27#setting12413"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/27#setting12413" {"https://thesession.org/tunes/27#setting12413"}}}
	title = "Drowsy Maggie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian     e'4 ^"Em"   b'8    e'8    e'4    b'8    e'8  
\bar "|"   e'4    b'8    e'8      a'8 ^"D"   fis'8    d'8    fis'8  \bar "|"    
 e'4 ^"Em"   b'8    e'8    b'4    b'8    cis''8  \bar "|"   d''8    e''8    
d''8    b'8      a'8 ^"D"   fis'8    d'8    fis'8  \bar "||"     e'8 ^"Em"   
e'8    b'8    e'8    d''8    e'8    b'8    e'8  \bar "|"   e'8    e'8    b'8    
e'8      a'8 ^"D"   fis'8    d'8    fis'8  \bar "|"     e'8 ^"Em"   e'8    b'8  
  e'8    d''8    e'8    b'8    e'8  \bar "|"   d''8    e''8    d''8    b'8      
a'8 ^"D"   fis'8    d'8    fis'8  \bar "||"     e'8 ^"Em"   e'8    b8    d'8    
e'8    b'8    a'8 ^"D"   fis'8    \bar "|"     g'8 ^"Em"   a'8    b'8    e''8   
   d''8 ^"D"   b'8    a'8    fis'8    \bar "|"     e'8 ^"Em"   e'8    b8    d'8 
   e'8    b'8    a'8 ^"D"   fis'8    \bar "|"     g'8 ^"Em"   a'8    g'8    
fis'8    e'8    d'8    b8    d'8    \bar "||"   e'8    e'8    b8    d'8    e'8  
  b'8    a'8 ^"D"   fis'8    \bar "|"     g'8 ^"Em"   a'8    b'8    e''8      
d''8 ^"D"   b'8    a'8    fis'8    \bar "|"     e'8 ^"Em"   e'8    b8    d'8    
e'8    b'8    a'8 ^"D"   fis'8    \bar "|"     g'8 ^"Em"   a'8    g'8    fis'8  
  e'8    d'8    b8    g8    \bar "||"     a4 ^"Am"   e'8    a8    a4    e'8    
a8  \bar "|"   a4    e'8    a8      d'8 ^"G"   b8    g8    b8  \bar "|"     a4 
^"Am"   e'8    a8    e'4    e'8    fis'8  \bar "|"   g'8    a'8    g'8    e'8   
   d'8 ^"G"   b8    g8    b8  \bar "||"     a8 ^"Am"   a8    e'8    a8    g'8   
 a8    e'8    a8  \bar "|"   a8    a8    e'8    a8      d'8 ^"G"   b8    g8    
b8  \bar "|"     a8 ^"Am"   a8    e'8    a8    g'8    a8    e'8    a8  \bar "|" 
  d'8    e'8    d'8    b8      d'8 ^"G"   b8    g8    b8  \bar "||"     a8 
^"Am"   a8    c'8    e'8    a'8    g'8    e'8    c'!8    \bar "|"   a8    a8    
c'8    e'8    a'8    g'8    e'8    c'!8    \bar "|"   a8    a8    c'8    e'8    
a'8    b'8    a'8    e'8    \bar "|"   c''8    d''8    c''!8    a'8      e'8 
^"Em"   d'8    cis'8    b8    \bar "||"     a8 ^"Am"   a8    c'8    e'8    a'8  
  g'8    e'8    c'!8    \bar "|"   a8    a8    c'8    e'8    a'8    g'8    e'8  
  c'!8    \bar "|"   a8    a8    c'8    e'8    a'8    b'8    c''8    d''8    
\bar "|"   e''8    d''8    c''8    e''8      b'8 ^"Em"   a'8    g'8    fis'8    
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
