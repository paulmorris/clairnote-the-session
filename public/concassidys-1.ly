\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/971#setting971"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/971#setting971" {"https://thesession.org/tunes/971#setting971"}}}
	title = "Con Cassidy's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \mixolydian   b'8    g'8    d''8    g'8    b'16    c''16    
d''8    g'8    b'8  \bar "|"   a'8    d'8    d'16    d'16    d'8    e'16    
fis'16    g'8    a'8    c''8  \bar "|"   b'8    g'8    d''8    g'8    b'16    
c''16    d''8    g'8    f''8  \bar "|"   e''8    c''8    e''16    f''16    g''8 
   f''8    e''8    d''8    c''8  }     b'16    c''16    d''8   ~    d''8    
e''8    f''8    e''8    f''8    c''8  \bar "|"   a'8    f'8    a'8    b'8    
c''8    e''8    d''8    c''8  \bar "|"   b'16    c''16    d''8   ~    d''8    
e''8    f''8    e''8    f''8    d''8  \bar "|"   e''16    f''16    g''8    e''8 
   c''8    f''8    e''8    d''8    c''8  \bar "|"     b'16    c''16    d''8  
 ~    d''8    e''8    f''8    e''8    f''8    d''8  \bar "|"   c''8    a'8    
f'8    a'8    c''8    f''8    e''8    f''8  \bar "|"   g''8    f''8    e''8    
g''8    f''8    e''8    d''8    cis''8  \bar "|"   d''8    a''8    f''8    e''8 
   f''8    e''8    d''8    c''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
