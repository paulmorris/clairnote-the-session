\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1129#setting14391"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1129#setting14391" {"https://thesession.org/tunes/1129#setting14391"}}}
	title = "Rose Of Aranmore, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   a'8    fis'8    \bar "|"   d'4.    a8    d'8    fis'8 
   \bar "|"   a'16    b'16    a'8    fis'8    a8    d'8    a8    \bar "|"   
g'4.    d'8    g'8    b'8    \bar "|"   d''16    e''16    d''8    cis''16    
d''16    cis''8    b'16    cis''16    b'8    \bar "|"   a'4.    fis'8    a'8    
b'8    \bar "|"   a'4    fis'8    a'8    d'8    a'8    \bar "|"   e'2   ~    
e'8    fis'8    \bar "|"   e'4.    g'8    fis'8    e'8    \bar "|"     d'4.    
a8    d'8    fis'8    \bar "|"   a'16    b'16    a'8    fis'8    a8    d'8    
a8    \bar "|"   g'4.    d'8    g'8    b'8    \bar "|"   d''16    e''16    d''8 
   cis''16    d''16    cis''8    b'16    cis''16    b'8    \bar "|"   a'16    
b'16    a'8   ~    a'8    d'8    fis'8    a'8    \bar "|"   g'16    a'16    g'8 
   fis'8    a8    cis'8    e'8    \bar "|"   d'2   ~    d'8    d'8    \bar "|"  
 d'4.    e'8    fis'8    a'8    \bar "||"     g'4.    d'8    g'8    b'8    
\bar "|"   d''16    e''16    d''8    cis''16    d''16    cis''8    b'16    
cis''16    b'8    \bar "|"   a'4.    fis'8    a'8    b'8    \bar "|"   a'16    
b'16    a'8    fis'8    a8    d'8    a8    \bar "|"   g'4.    d'8    g'8    b'8 
   \bar "|"   a'4    fis'8    a'8    d'8    a'8    \bar "|"   e'2   ~    e'8    
fis'8    \bar "|"   e'4.    g'8    fis'8    e'8    \bar "|"     d'4.    a8    
d'8    fis'8    \bar "|"   a'16    b'16    a'8    fis'8    a8    d'8    a8    
\bar "|"   g'4.    d'8    g'8    b'8    \bar "|"   d''16    e''16    d''8    
cis''16    d''16    cis''8    b'16    cis''16    b'8    \bar "|"   a'4.    d'8  
  fis'8    a'8    \bar "|"   g'16    a'16    g'8    fis'8    a8    cis'8    e'8 
   \bar "|"   d'2   ~    d'8    e'8    \bar "|"   d'4.    e'8    fis'8    e'8   
 \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
