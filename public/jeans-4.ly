\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tijn Berends"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/255#setting30130"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/255#setting30130" {"https://thesession.org/tunes/255#setting30130"}}}
	title = "Jean's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   g'8    d'8    d'4 ^"~"    a'8    d'8    
d'4 ^"~"    \bar "|"   b'8    d'8    d'4 ^"~"    g'8    d'8    d'4 ^"~"    
\bar "|"   g'8    d'8    d'4 ^"~"    a'8    d'8    d'4 ^"~"    \bar "|"   b'8   
 d'8    d'4 ^"~"    g'8    d'8    d'4 ^"~"    \bar "|"     \bar "|"   b'8    
d'8    d'4 ^"~"    c''8    d'8    d'4 ^"~"    \bar "|"   d''8    d'8    d'4 
^"~"    d'8    d'8    d'4 ^"~"    \bar "|"   b'8    d'8    d'4 ^"~"    c''8    
d'8    d'4 ^"~"    \bar "|"   d''8    d'8    d'4 ^"~"    d'8    d'8    d'4 
^"~"    \bar "|"     \bar "|"   d''8    d'8    e''8    d'8    d''8    d'8    
c''8    d'8    \bar "|"   b'8    d'8    d'4 ^"~"    g'8    d'8    d'4 ^"~"    
\bar "|"   d''8    d'8    e''8    d'8    d''8    d'8    c''8    d'8    \bar "|" 
  b'8    d'8    d'4 ^"~"    g'8    d'8    d'4 ^"~"    \bar "|"     \bar "|"   
g'8    d'8    d'4 ^"~"    b'8    d'8    d'4 ^"~"    \bar "|"   g'8    d'8    
d'4 ^"~"    d'8    d'8    d'4 ^"~"    \bar "|"   g'8    d'8    d'4 ^"~"    b'8  
  d'8    d'4 ^"~"    \bar "|"   g'8    d'8    d'4 ^"~"    d'8    d'8    d'4 
^"~"    \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
