\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "GaryAMartin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1423#setting23607"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1423#setting23607" {"https://thesession.org/tunes/1423#setting23607"}}}
	title = "Broken Pledge, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \dorian   d''8    cis''8    a'8    g'8    a'8    d'8  
\grace {    f'8  }   d'8    d'8  \bar "|"   fis'8    c''8    g'8    f'!8    e'8 
   c'8  \grace {    e'8  }   \tuplet 3/2 {   c'8    b8    c'8  } \bar "|"   d'8  
  e'8    f'8    g'8    a'8    d''8  \grace {    e''8  }   d''8    c''8  
\bar "|"   a'8    c''8    g'8    c''8    a'8    d''8    f''8    e''8  \bar "|"  
   d''8    cis''8    a'8    d'8    \tuplet 3/2 {   e'8    fis'8    g'8  }   a'8  
  b'8  \bar "|"   c''8    c''8    c''8    g'8    \tuplet 3/2 {   e'8    fis'8    
g'8  }   c'8    e'8  \bar "|"   d'8    e'8    fis'8    g'8    a'8    d''8  
\grace {    e''8  }   d''8    cis''8  \bar "|"   a'8    a'8    g'8    e'8    
d'8    d'8    d'8    d''8  \bar "||"     d''8    cis''8    a'8    g'8    fis'8  
  d''8    d'8    ees'8 ( \bar "|"   e'!4  -)   c''8    g'8    e'8    c'8    c'8 
   c'8  \bar "|"   a8    d'8    f'8    g'8    a'8    d''8  \grace {    e''8  }  
 d''8    c''8  \bar "|"   a'8    a'8    g'8    e'8    d'8    g''8    fis''8    
e''8  \bar "|"     d''8    cis''8    a'8    d'8    \tuplet 3/2 {   e'8    fis'8  
  g'8  }   a'8    b'8  \bar "|"   c''8    b'8    c''8    g'8    \tuplet 3/2 {   
e'8    fis'8    g'8  }   c'8    e'8  \bar "|"   d'8    e'8    fis'8    g'8    
a'8    d''8  \grace {    e''8  }   d''8    cis''8  \bar "|" \tuplet 3/2 {   a'8  
  b'8    a'8  }   g'8    e'8    d'4.    d''8  \bar "||"     d''8    cis''8    
a'8    g'8    a'8    a'8    d''8    e''8  \bar "|"   f''8    f''8    d''8    
f''8    e''8    c''8    a'8    d''8  \bar "|"   c''8    c'8    e'8    g'8    
c''8    c''8    c''8    d''8  \bar "|"   e''8    c''8  \grace {    e''8  }   
c''8  \grace {    b'8  }   c''8    e''8    a''8    g''8    e''8  \bar "|"     
d''8    cis''8    a'8    g'8    a'8    a'8    d''8    e''8  \bar "|"   f''4    
d''8    f''8    e''8    c''8    a'8    g'8  \bar "|"   f'4    f'8    e'8    f'8 
   g'8    \tuplet 3/2 {   a'8    bes'8    b'!8  } \bar "|"   c''8    b'8    c''8 
   e'8    e'8    d'8    d'8    d'8  \bar "||"     cis''8    d''8    a'8    g'8  
  a'8    c''!8    d''8    e''8  \bar "|"   f''8    a''8    g''8    f''8    d''8 
   c''8    a'8    d''8  \bar "|"   c''8    a'8    g'8    c'8    \tuplet 3/2 {   
e'8    fis'8    g'8  }   c''8    g''8  \bar "|"   e''8    c''8    c''8    c''8  
  g'8    c''8    e''8    c''8  \bar "|"     d''8    cis''8    a'8    g'8    
\tuplet 3/2 {   a'8    d''8    a'8  }   d''8    e''8  \bar "|"   f''4    d''8    
f''8    e''8    c''8    a'8    g'8  \bar "|"   f'4    f'8    e'8    f'8    g'8  
  \tuplet 3/2 {   a'8    bes'8    b'!8  } \bar "|"   c''8    b'8    c''8    e'8  
  e'8    d'8    d'8    d'8  \bar "||"     d''8    cis''8    a'8    g'8    a'8   
 d'8  \grace {    f'8  }   d'8    d'8  \bar "|"   b'8    c''8    g'8    f'!8    
e'8    c'8  \grace {    e'8  }   \tuplet 3/2 {   c'8    b8    c'8  } \bar "|"   
d'8    e'8    f'8    g'8    a'8    d''8  \grace {    e''8  }   d''8    c''8  
\bar "|"   a'8    c''8    g'8    c''8    a'8    d''8    f''8    e''8  \bar "|"  
   d''8    cis''8    a'8    d'8    \tuplet 3/2 {   e'8    fis'8    g'8  }   a'8  
  b'8  \bar "|"   c''8    b'8    c''8    g'8    \tuplet 3/2 {   e'8    fis'8    
g'8  }   c'8    e'8  \bar "|"   d'8    e'8    fis'8    g'8    a'8    d''8  
\grace {    e''8  }   d''8    cis''8  \bar "|" \tuplet 3/2 {   a'8    b'8    a'8 
 }   g'8    e'8    d'8    d'8    d'8    d''8  \bar "||"     d''8    cis''8    
a'8    g'8    fis'8    d''8    d'8    ees'8 ( \bar "|"   e'!4  -)   c''8    g'8 
   e'8    c'8    \tuplet 3/2 {   c'8    b8    c'8  } \bar "|"   a8    d'8    f'8 
   g'8    a'8    d''8  \grace {    e''8  }   d''8    c''8  \bar "|" 
\tuplet 3/2 {   a'8    b'8    a'8  }   g'8    e'8    d'8    g''8    fis''8    
e''8  \bar "|"     d''8    cis''8    a'8    d'8    \tuplet 3/2 {   e'8    fis'8  
  g'8  }   a'8    b'8  \bar "|"   c''8    b'8    c''8    g'8    \tuplet 3/2 {   
e'8    fis'8    g'8  }   c'8    e'8  \bar "|"   d'8    e'8    fis'8    g'8    
a'8    d''8  \grace {    e''8  }   d''8    cis''8  \bar "|" \tuplet 3/2 {   a'8  
  b'8    a'8  }   g'8    e'8    a'8    d'8    d'8    d''8  \bar "||"     d''8   
 cis''8    a'8    g'8    a'8    a'8    d''8    e''8  \bar "|"   f''8    f''8    
d''8    f''8    e''8    c''8    a'8    d''8  \bar "|"   c''8    c'8    e'8    
g'8    c''8    c''8    c''8    d''8  \bar "|"   e''8    c''8  \grace {    e''8  
}   c''8  \grace {    b'8  }   c''8    e''8    a''8    g''8    e''8  \bar "|"   
  d''8    cis''8    a'8    g'8    \tuplet 3/2 {   a'8    d''8    a'8  }   d''8   
 e''8  \bar "|"   f''8    f''8    d''8    f''8    e''8    c''8    a'8    g'8  
\bar "|"   f'8    f'8    f'8    e'8    f'8    g'8    \tuplet 3/2 {   a'8    
bes'8    b'!8  } \bar "|"   c''8    b'8    c''8    e'8    e'8    d'8    d'8    
d'8  \bar "||"     cis''8    d''8    a'8    g'8    a'8    c''!8    d''8    e''8 
 \bar "|"   f''8    a''8    g''8    f''8    d''8    c''8    a'8    d''8  
\bar "|"   c''8    a'8    g'8    c'8    \tuplet 3/2 {   e'8    fis'8    g'8  }   
c''8    g''8  \bar "|"   e''8    c''8    \tuplet 3/2 {   c''8    b'8    c''8  }  
 g'8    c''8    e''8    c''8  \bar "|"     d''8    cis''8    a'8    g'8    
\tuplet 3/2 {   a'8    d''8    a'8  }   d''8    e''8  \bar "|"   f''4    d''8    
f''8    e''8    c''8    a'8    g'8  \bar "|"   f'4    f'8    e'8    f'8    g'8  
  \tuplet 3/2 {   a'8    bes'8    b'!8  } \bar "|"   c''8    b'8    c''8    e'8  
  e'8    d'8    c'8    a8  \bar "|" <<   a1    d'1   >> \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
