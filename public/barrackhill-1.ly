\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/1014#setting1014"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1014#setting1014" {"https://thesession.org/tunes/1014#setting1014"}}}
	title = "Barrack Hill"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key a \dorian   g''4    fis''8  \repeat volta 2 {   e''4    a'8    
a'8    b'8    a'8    e''4    d''8    b'4    d''8  \bar "|"   e''4    a'8    b'4 
   a'8    g'4    a'8    b'4    d''8  \bar "|"     e''4    a'8    a'8    b'8    
a'8    e''4    d''8    b'4    d''8  \bar "|"   d''4    b'8    g'8    a'8    b'8 
   a'4.    a'8    b'8    d''8  }     \repeat volta 2 {   e''4    d''8    e''8   
 fis''8    g''8    a''4    fis''8    g''4    e''8  \bar "|"   d''4    b'8    
g''4    b'8    d''4    b'8    d''4.  \bar "|"     e''4    d''8    e''8    
fis''8    g''8    a''4    fis''8    g''4    e''8  } \alternative{{   d''4    
b'8    g'4    b'8    a'4.    a'8    b'8    d''8  } {   d''4    b'8    g'4    
b'8    a'4.    g''4    fis''8  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
