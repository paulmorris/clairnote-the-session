\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Cerebus"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1216#setting26525"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1216#setting26525" {"https://thesession.org/tunes/1216#setting26525"}}}
	title = "Strike The Gay Harp"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   a'8  \bar "|"   d''8    b'8 -.   b'8 -.   cis''8    
a'8    a'8  \bar "|"     b'8 ^"tr"   a'8    g'8      fis'8 ^"tr"   e'8    d'8  
\bar "|"   d''8    b'8    b'8    cis''8    a'8    a'8  \bar "|"   d''8    a'8   
 fis'8    e'8    fis'8    a'8  \bar "|"     d''8 (   e''8  -)   fis''8    b'8 ( 
  cis''8  -)   d''8  \bar "|"   a'8    b'8    g'8    fis'8    e'8    fis'8  
\bar "|"     d'8.    e'16    d'8    d'8    fis'8    a'8  \bar "|"   a'8    
fis'8    e'8    e'8 (   fis'8  -)   a'8  } \repeat volta 2 {     d''4.    
fis''4.  \bar "|"   a''4    b''8    a''8    fis''8    d''8  \bar "|"   d''4    
fis''8    a''8    fis''8    a''8  \bar "|"   b''8    g''8    e''8    e''8    
cis''8    a'8  \bar "|"     d''4    fis''8    a''8    fis''16 (   g''16    
a''16  -)   fis''16 -. \bar "|"   b''8 (   g''16    a''16    b''16  -)   g''16  
  a''4    g''8  \bar "|"   fis''8    e''8    d''8    d''8    fis''8    e''8  
\bar "|"   d''8    a'8    fis'8    e'8    fis'8    a'8  } \repeat volta 2 {     
d''8 (   fis''8  -)   e''8 -.   d''8 (   fis''8  -)   e''8 -. \bar "|"   d''8 ( 
  a'8  -)   g'8 -.   fis'8 (   e'8  -)   d'8 -. \bar "|"     d''8 (   fis''8  
-)   e''8 -.   d''8 (   fis''8  -)   e''8 -. \bar "|"   d''8 (   a'8  -)   
fis'8 -.   e'8 (   fis'8  -)   a'8 -. \bar "|"     d''8 (   e''8  -)   fis''8 
-.   b'8 (   cis''8  -)   d''8 -. \bar "|"   a'8    b'8    g'8    fis'8    e'8  
  fis'8  \bar "|"   d'8    e'8    d'8    d'8    fis'8    a'8  \bar "|"   a'8    
fis'8    e'8    e'8    fis'8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
