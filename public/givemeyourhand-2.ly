\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/454#setting22876"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/454#setting22876" {"https://thesession.org/tunes/454#setting22876"}}}
	title = "Give Me Your Hand"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   d'4  \bar "|"   e'4    g'4    g'4  \bar "|"   g'2    
d'4  \bar "|"   e'4    g'4    g'4  \bar "|"   g'2    d'4  \bar "|"   e'4    g'4 
   g'4  \bar "|"   g'4    a'4    b'4  \bar "|"   b'4    e''4    d''4  \bar "|"  
 b'2    a'8    g'8  \bar "|"     a'4    a'4    e''8    d''8  \bar "|"   b'4    
b'4    d''8    b'8  \bar "|"   a'4    a'8    b'8    a'8    g'8  \bar "|"   e'2  
  d'4  \bar "|"   e'4    g'4    g'4  \bar "|"   g'2    d'4  \bar "|"   e'4    
g'4    g'4  \bar "|"   g'2    d'4  \bar "|"     e'4    g'4    g'4  \bar "|"   
g'4    a'4    b'4  \bar "|"   d'4    d'8    b'8    a'8    b'8  \bar "|"   g'2   
 d'4  \bar "|"   e'4    g'4    g'4  \bar "|"   g'4    a'4    b'4  \bar "|"   
b'4    e''4    d''4  \bar "|"   b'2    a'8    g'8  \bar "|"     a'4    a'4    
e''8    d''8  \bar "|"   b'4    b'4    d''8    b'8  \bar "|"   a'4    a'4    
\tuplet 3/2 {   b'8    c''8    d''8  } \bar "|"   e''2    d''8    b'8  \bar "|"  
 d''4    d''4    e''4  \bar "|"   g''2    e''8    d''8  \bar "|"   e''4    e''4 
   g''4  \bar "|"   a''2    g''8    e''8  \bar "|"     g''4    g''4    d''8    
e''8  \bar "|"   g''4    g''4    d''8    e''8  \bar "|"   g''4    g''4    a''4  
\bar "|"   b''2.  \bar "|"   b''4    b''4    b''4  \bar "|"   b''2    a''8    
g''8  \bar "|"   a''8    g''8    a''8    b''8    a''8    b''8  \bar "|"   a''2  
  g''8    fis''8  \bar "|"     e''4    e''4    g''8    e''8  \bar "|"   d''4    
d''4    g''8    d''8  \bar "|"   b'4    b'4    d''8    b'8  \bar "|"   a'2    
\tuplet 3/2 {   c''8    b'8    a'8  } \bar "|"   g'4    g'4    \tuplet 3/2 {   
b'8    c''8    d''8  } \bar "|"   f''2    e''8    d''8  \bar "|"   e''4    e''4 
   g''4  \bar "|"   e''2    d''8    b'8  \bar "|"     d''4    d''4    g''4  
\bar "|"   b'4    b'4    d''8    b'8  \bar "|"   a'4    a'8    c''8    b'8    
a'8  \bar "|"   g'2.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
