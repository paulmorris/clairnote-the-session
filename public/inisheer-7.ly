\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "mrkelahan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/211#setting22688"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/211#setting22688" {"https://thesession.org/tunes/211#setting22688"}}}
	title = "Inisheer"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \major   \repeat volta 2 {   cis''4.    b'8    cis''8    e''8  
  \bar "|"   cis''4.    b'8    cis''8    e''8    \bar "|"   fis'4.    cis''8    
b'8    cis''8    \bar "|"   e'4.    cis''8    b'8    a'8    \bar "|"     
cis''4.    b'8    cis''8    e''8    \bar "|"   cis''4.    b'8    cis''8    e''8 
   \bar "|"   fis'4.    cis''8    b'16    a'16    gis'8    } \alternative{{   
a'4.    fis'8    e'8    a'8    } {   a'4.    b'8    cis''8    e''8    \bar "||" 
    \bar "||"   fis''4.    gis''8    fis''8    e''8    \bar "|"   cis''4.    
b'8    cis''8    e''8    \bar "|"   fis''8    gis''8    fis''8    e''8    
cis''16    d''16    e''8    \bar "|"   fis''4.    b'8    cis''8    e''8    
\bar "|"     fis''4.    gis''8    fis''8    e''8    \bar "|"   cis''4.    b'8   
 cis''8    e''8    \bar "|"   fis'4.    cis''8    b'16    a'16    gis'8    
\bar "|"   a'4.    b'8    cis''8    e''8    \bar "||"     \bar "||"   fis''4.   
 gis''8    fis''8    e''8    \bar "|"   cis''4.    b'8    cis''8    e''8    
\bar "|"   a''8    gis''8    fis''8    e''8    cis''16    d''16    e''8    
\bar "|"   fis''4.    b'8    cis''8    e''8    \bar "|"     fis''4.    gis''8   
 fis''8    e''8    \bar "|"   cis''4.    b'8    cis''8    e''8    \bar "|"   
e'4.    cis''8    b'16    a'16    gis'8    \bar "|"   a'2.    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
