\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/159#setting159"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/159#setting159" {"https://thesession.org/tunes/159#setting159"}}}
	title = "Denis Murphy's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key d \major   \repeat volta 2 {   a'4    d'8    fis'8    e'8    
d'8    fis'4    a'8    a'4    fis''8  \bar "|"   g''8    fis''8    e''8    
fis''8    e''8    d''8    e''4    d''8    b'8    d''8    b'8  \bar "|"     a'4  
  d'8    fis'8    e'8    d'8    fis'4    a'8    a'4    fis''8  \bar "|"   a''4  
  fis''8    e''8    fis''8    e''8    d''4.    d''4.  }     \repeat volta 2 {   
d''4    e''8    fis''4.    g''8    fis''8    e''8    fis''4.  \bar "|"   g''8   
 fis''8    e''8    fis''8    e''8    d''8    e''4    d''8    b'8    d''8    b'8 
 \bar "|"     d''4    e''8    fis''4.    g''8    fis''8    e''8    fis''4    
fis''8  \bar "|"   a''4    fis''8    e''8    fis''8    e''8    d''4.    d''4.  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
