\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Edgar Bolton"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/718#setting24141"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/718#setting24141" {"https://thesession.org/tunes/718#setting24141"}}}
	title = "George White's Favorite"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   b'8    d''8    \repeat volta 2 {   e''8    b'8    b'4 
^"~"    e''8    b'8    d''8    b'8    \bar "|"   a'8 ^"~"    g'8    a'8    b'8  
  g'8 ^"~"    e'8    d'4    \bar "|"   g'8    b'8    b'4 ^"~"    g'8    b'8    
b'4 ^"~"    \bar "|"   \tuplet 3/2 {   e''8    g''8    e''8  }   d''8    b'8    
a'4    b'8    d''8    \bar "|"     e''8    b'8    b'4 ^"~"    e''8    b'8    
d''8    b'8    \bar "|"   a'8 ^"~"    g'8    a'8    b'8    g'8 ^"~"    e'8    
d'4    \bar "|"   g'8 ^"~"    a'8    b'8    d''8    g''8    e''8    d''8    b'8 
   \bar "|"   a'8 ^"~"    g'8    a'8    b'8    g'4    b'8    d''8    }     
\repeat volta 2 {   g''8 ^"~"    e''8    d''8    e''8    g''8    a''8    b''8   
 a''8    \bar "|"   g''8 ^"~"    fis''8    g''8    a''8    g''8 ^"~"    e''8    
d''8    b'8    \bar "|"   d''8    e''8    g''8    a''8    b''8    g''8    a''8  
  g''8    \bar "|"   e''8    a''8    a''8    g''8    a''4    g''8    a''8    
\bar "|"     b''8    g''8    e''8    g''8    d''8    g''8    e''8    d''8    
\bar "|"   g''8 ^"~"    e''8    d''8    e''8    g''8    a''8    b''8    a''8    
\bar "|"   g''8    fis''8    g''8    d''8    \tuplet 3/2 {   e''8    fis''8    
g''8  }   d''8    b'8    } \alternative{{   a'8    g'8    a'8    b'8    g'4    
b'8    d''8    } {   a'8    g'8    a'8    b'8    g'2    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
