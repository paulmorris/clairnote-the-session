\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "GoldenKeyboard"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/374#setting374"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/374#setting374" {"https://thesession.org/tunes/374#setting374"}}}
	title = "Toss The Fiddles"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \tuplet 3/2 {   d'8    e'8    fis'8  } \bar "|"   g'4 
^"G"   b'8    g'8    d'8    g'8    b'8    g'8  \bar "|"   d'8    g'8    b'8    
g'8      a'8 ^"Am"   g'8    e'8    g'8  \bar "|"   d'8 ^"G"   g'8    b'8    g'8 
   d''8    g'8    b'8    g'8  \bar "|"       c''8 ^"C"   b'8    a'8    g'8      
fis'8 ^"D7"   g'8    a'8    fis'8  \bar "|"   g'4 ^"G"   b'8    g'8    d'8    
g'8    b'8    g'8  \bar "|"   d'8    g'8    b'8    g'8      a'8 ^"Am"   g'8    
e'8    g'8  \bar "|"       d'8 ^"G"   g'8    b'8    g'8    d''8    g'8    b'8   
 g'8  \bar "|"     c''8 ^"D7"   a'8    fis'8    a'8      g'4 ^"G"   
\tuplet 3/2 {   d'8 ^"D7"   e'8    fis'8  } \bar ":|."     c''8 ^"D7"   a'8    
fis'8    a'8      g'4 ^"G"   \tuplet 3/2 {   d''8 ^"D7"   e''8    fis''8  } 
\bar "||"     \bar ".|:"   g''4 ^"G"   d''8    g''8    b'8    g''8    d''8    
g''8  \bar "|"   e''8 ^"C"   g''8    c''8    g''8    e''8    g''8    c''8    
g''8  \bar "|"   fis''8 ^"D(sus4)"   g''8    d''8    g''8    fis''8    g''8    
d''8 ^"(B7)"   fis''8  \bar "|"       g''8 ^"Em"   a''8    b''8    g''8      
a''8 ^"D7"   g''8    d''8    fis''8  \bar "|"   g''4 ^"G"   d''8    g''8    b'8 
   g''8    d''8    g''8  \bar "|"   e''8 ^"C"   g''8    c''8    g''8    e''8    
g''8    c''8    g''8  \bar "|"       fis''8 ^"D(sus4)"   g''8    d''8    g''8   
 fis''8    g''8    \tuplet 3/2 {   d''8 ^"D7"   e''8    fis''8  } \bar "|"     
g''4 ^"G"   fis''4 ^"D"     g''4 ^"G"   \tuplet 3/2 {   d''8 ^"D7"   e''8    
fis''8  } \bar ":|."     g''2 ^"G"     g''4 ^"G"   \tuplet 3/2 {   b'8 ^"D7"   
bes'8    a'8  } \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
