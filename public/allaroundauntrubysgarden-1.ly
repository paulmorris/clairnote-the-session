\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Whamlyn"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/506#setting506"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/506#setting506" {"https://thesession.org/tunes/506#setting506"}}}
	title = "All Around Aunt Ruby's Garden"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   \repeat volta 2 {   d'8    fis'8    a'8    fis'8  
\bar "|"   fis'8    e'8    fis'8    e'8  \bar "|"   d'8    fis'8    a'8    
fis'8  \bar "|"   fis'8    e'8    e'4  \bar "|"     \bar "|"   d'8    fis'8    
a'8    fis'8  \bar "|"   fis'8    e'8    fis'8    a'8  \bar "|"   b'8.    
cis''16    b'8    a'8  \bar "|"   fis'8    e'8    d'4  }     \repeat volta 2 {  
 d'8    d''8    cis''8    d''8  \bar "|"   fis'8.    g'16    fis'8    e'8  
\bar "|"   d'8    d''8    cis''8    d''8  \bar "|"   e''4    e''4  \bar "|"     
\bar "|"   d'8    d''8    cis''8    d''8  \bar "|"   fis'8.    g'16    fis'8    
a'8  \bar "|"   b'8.    cis''16    b'8    a'8  \bar "|"   fis'8    e'8    d'4  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
