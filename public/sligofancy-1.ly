\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Atk"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/508#setting508"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/508#setting508" {"https://thesession.org/tunes/508#setting508"}}}
	title = "Sligo Fancy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   b''8. (   a''16  -) \bar "|"   g''8.    fis''16    
g''8.    e''16    b'4    e''8.    d''16  \bar "|"   c''8.    b'16    c''8.    
a'16    e'4    a'8.    g'16  \bar "|"   fis'8.    g'16    a'8.    b'16    c''8. 
   d''16    e''8.    fis''16  \bar "|" \tuplet 3/2 {   g''8    b''8    a''8  }   
\tuplet 3/2 {   g''8    fis''8    e''8  }   d''4    b''8.    a''16  \bar "|"     
g''8.    fis''16    g''8.    e''16    b'4    e''8.    d''16  \bar "|"   c''8.   
 b'16    c''8.    a'16    e'4    a'8. (   g'16  -) \bar "|"   fis'8.    g'16    
a'8.    b'16    c''8.    d''16    e''8.    fis''16  \bar "|"   g''4    b''4    
g''4  }     \repeat volta 2 {   b'8.    c''16  \bar "|"   d''8.    b'16    
g''8.    b'16    d''8.    g''16    b'8.    c''16  \bar "|" \tuplet 3/2 {   d''8  
  c''8    b'8  }   g''8.    b'16    d''4    c''8.    b'16  \bar "|"   c''8.    
a'16    fis''8.    a'16    c''8.    fis''16    a'8.    b'16  \bar "|" 
\tuplet 3/2 {   c''8    b'8    a'8  }   fis''8.    a'16    c''4    b'8. (   
c''16  -) \bar "|"     d''8.    b'16    g''8.    b'16    d''8.    g''16    b'8. 
   c''16  \bar "|" \tuplet 3/2 {   d''8    c''8    b'8  }   g''8.    b'16    
d''4    c''8.    b'16  \bar "|"   c''8.    a'16    a''8.    g''16    fis''8.    
d''16    e''8.    fis''16  \bar "|"   g''4    b''4    g''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
