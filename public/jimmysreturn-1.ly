\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Zina Lee"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1299#setting1299"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1299#setting1299" {"https://thesession.org/tunes/1299#setting1299"}}}
	title = "Jimmy's Return"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   \repeat volta 2 {   fis''8    r8 b'8    cis''8    
d''8    fis''8    e''8    d''8  \bar "|"   cis''16    b'16    a'8    cis''8    
e''8    a''8    e''8    cis''8    e''8  \bar "|"   fis''8 ^"~"    fis''8    b'8 
   cis''8    d''8    fis''8    e''8    d''8  \bar "|"   b'16    cis''16    d''8 
   e''8    cis''8    d''8    b'8    b'8 ^"~"    b'8  \bar "|"     \bar "|"   
a'8    b'8 ^"~"    b'8    d''8    fis''8    d''8    e''8    d''8  \bar "|"   
cis''8    cis''8 ^"~"    cis''8    e''8    a''8    e''8    cis''8    a'8  
\bar "|"   b'8    cis''8    d''8    a'8    b'16    cis''16    d''8    e''8    
g''8  \bar "|"   fis''8 ^"~"    fis''8    e''8    cis''8    d''8    b'8    b'8 
^"~"    a'8  }     \repeat volta 2 {   a'8    b'8 ^"~"    b''8    a''8    
fis''8    a''8    e''8    d''8  \bar "|"   cis''16    b'16    a'8    cis''8    
e''8    a''8    e''8    cis''8    a'8  \bar "|"   b'8 ^"~"    b'8    b''8    
a''8    fis''8    a''8    e''8    d''8  \bar "|"   b'16    cis''16    d''8    
e''8    cis''8    d''8    b'8    cis''16    b'16    a'8  \bar "|"     b'8 ^"~"  
  b'8    b''8    a''8    fis''8    a''8    e''8    d''8  \bar "|"   cis''16    
b'16    a'8    cis''8    e''8    a''8    e''8    cis''8    a'8  \bar "|"   b'8  
  cis''8    d''8    a'8    b'16    cis''16    d''8    e''8    g''8  \bar "|"   
fis''8    d''8    e''8    cis''8    d''8    b'8    b'8 ^"~"    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
