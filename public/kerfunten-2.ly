\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ah, Surely!"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/139#setting12759"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/139#setting12759" {"https://thesession.org/tunes/139#setting12759"}}}
	title = "Kerfunten, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key g \major   b'4    b'8    b'8    a'8    g'8  \bar "|"   d''8    
b'8    d''8    e''8    fis''8    g''8  \bar "|"   d''8    e''8    d''8    e''8  
  d''8    b'8  \bar "|"   a'4. ^"~"    a'8    b'8    c''8  \bar "|"   b'4. 
^"~"    b'8    a'8    g'8  \bar "|"   d''8    b'8    d''8    e''8    fis''8    
g''8  \bar "|"   d''8    e''8    d''8    g''8    d''8    b'8  \bar "|"   a'8    
g'8    fis'8    g'4.  }   |
 g''4. ^"~"    g''8    a''8    b''8  \bar "|"   e''4. ^"~"    e''8    d''8    
b'8  \bar "|"   g''4. ^"~"    g''8    a''8    b''8  \bar "|"   b''8    a''8    
g''8    a''4. ^"~"  \bar "|"   g''4. ^"~"    g''8    a''8    b''8  \bar "|"   
e''4. ^"~"    e''8    d''8    b'8  \bar "|"   d''8    b'8    d''8    g''8    
d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8    b'8    d''8  \bar ":|."   
a'8    g'8    fis'8    g'4    c''8  \bar "||"   b'4. ^"~"    b'8    a'8    g'8  
\bar "|"   d''8    b'8    d''8    e''8    fis''8    g''8  \bar "|"   d''8    
b'8    d''8    e''8    d''8    b'8  \bar "|"   a'4. ^"~"    d'8    fis'8    a'8 
 \bar "|"   b'4. ^"~"    b'8    a'8    g'8  \bar "|"   d''8    b'8    d''8    
e''8    fis''8    g''8  \bar "|"   d''8    b'8    d''8    e''8    d''8    b'8  
\bar "|"   a'4. ^"~"    g'4    a'8  \bar ":|."   a'8    g'8    fis'8    g'8    
b'8    d''8  \bar "||"   g''4. ^"~"    g''8    a''8    b''8  \bar "|"   e''4. 
^"~"    e''8    d''8    b'8  \bar "|"   g''4. ^"~"    g''8    a''8    b''8  
\bar "|"   b''8    a''8    g''8    a''4. ^"~"  \bar "|"   g''4. ^"~"    g''8    
a''8    b''8  \bar "|"   e''4. ^"~"    e''8    fis''8    g''8  \bar "|"   d''8  
  b'8    d''8    e''8    d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'8    
b'8    d''8  \bar ":|."   a'8    g'8    fis'8    g'4    c''8  \bar "||"   b'4. 
^"~"    b'8    a'8    g'8  \bar "|"   d''8    b'8    d''8    e''8    fis''8    
g''8  \bar "|"   d''4. ^"~"    g''8    d''8    b'8  \bar "|"   b'8    a'8    
g'8    a'8    b'8    c''8  \bar "|"   b'4. ^"~"    b'8    a'8    g'8  \bar "|"  
 d''8    b'8    d''8    e''8    fis''8    g''8  \bar "|"   d''4. ^"~"    g''8   
 d''8    b'8  \bar "|"   a'8    g'8    fis'8    g'4    a'8  \bar ":|."   a'8    
g'8    fis'8    g'8    b'8    d''8  \bar "||"   g''4. ^"~"    g''8    a''8    
b''8  \bar "|"   e''4. ^"~"    e''8    d''8    b'8  \bar "|"   g''4. ^"~"    
g''8    a''8    b''8  \bar "|"   b''8    a''8    g''8    a''4    b''8  \bar "|" 
  g''4. ^"~"    g''8    a''8    b''8  \bar "|"   e''4. ^"~"    e''8    d''8    
b'8  \bar "|"   d''8    b'8    d''8    g''8    d''8    b'8  \bar "|"   a'8    
g'8    fis'8    g'8    b'8    d''8  \bar ":|."   a'8    g'8    fis'8    g'4    
c''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
