\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "obneb"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/850#setting24812"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/850#setting24812" {"https://thesession.org/tunes/850#setting24812"}}}
	title = "Smoky Bar, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d'4    a'8    d'8    b'8    d'8    a'8    d'8    
\bar "|"   d'4    a'8    fis'8    e'8    d'8    b8    a8  \bar "|"   d'4    a'8 
   d'8    b'8    d'8    a'8    b'8  \bar "|"   d''8    b'8    a'8    fis'8    
e'8    g'8    fis'8    e'8  \bar "|"     d'4    a'8    d'8    b'8    d'8    a'8 
   d'8    \bar "|"   d'4    a'8    fis'8    e'8    d'8    b8    a8  \bar "|"   
d'4    a'8    d'8    b'8    d'8    a'8    b'8  \bar "|"   d''8    b'8    a'8    
fis'8    e'8    fis'8    a'8    b'8  \bar "|"     d''4    fis''8    d''8    
e''8    fis''8    d''8    b'8  \bar "|"   a'4    fis'8    d'8    e'8    d'8    
b8    a8  \bar "|"   d''4    fis''8    d''8    e''8    fis''8    d''8    fis''8 
 \bar "|"   a''8    fis''8    e''8    d''8    b'8    a'8    b'8    cis''8  
\bar "|"     d''4    fis''8    d''8    e''8    fis''8    d''8    b'8    
\bar "|"   a'4    fis'8    d'8    e'8    d'8    b8    a8    \bar "|"   d''4    
fis''8    d''8    e''8    fis''8    d''8    fis''8    \bar "|"   a''8    fis''8 
   e''8    d''8    b'8    a'8    fis'8    e'8    \bar "|"   d'2    
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
