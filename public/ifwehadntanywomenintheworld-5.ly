\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1376#setting14735"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1376#setting14735" {"https://thesession.org/tunes/1376#setting14735"}}}
	title = "If We Hadn't Any Women In The World"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   b'8    c''8    \bar "|"   d''4.   
 b'8    g'8    a'8    b'8    g'8    \bar "|"   e'4    e'4    d'2    \bar "|"   
b'16    b'16    c''8    d''8    b'8    a'8    g'8    a'8    b'8    \bar "|"   
a'4.    b'8    a'4    b'8    c''8  \bar "|"     d''8    e''8    d''8    b'8    
g'8    a'8    b'8    g'8    \bar "|"   e'4    e'4    d'2    \bar "|"   b'16    
b'16    c''8    d''8    b'8    a'8    g'8    a'8    b'8    \bar "|"   g'4    
g'8    fis'8    g'4    }     \repeat volta 2 {   b'8    d''8    \bar "|"   
e''4.    fis''8    g''4    fis''8    g''8    \bar "|"   e''8    d''8    d''8    
cis''8    d''4    b'8    d''8    \bar "|"   e''8    d''8    b'8    d''8    g''8 
   d''8    b'8    g'8    \bar "|"   a'16    b'16    a'8    gis'8    b'8    a'4  
  b'8    d''8    \bar "|"     e''4.    fis''8    g''4    fis''8    g''8    
\bar "|"   e''8    d''8    d''8    cis''8    d''4    b'8    d''8    \bar "|"   
e''8    d''8    b'16    c''16    d''8    g''8    d''8    b'8    g'8    \bar "|" 
  a'16    b'16    a'8    g'8    fis'8    g'4    b'8    d''8    \bar "|"     
e''4.    fis''8    g''4    fis''8    g''8    \bar "|"   e''8    d''8    d''8    
cis''8    d''4    b'8    d''8    \bar "|"   e''8    d''8    b'8    d''8    g''8 
   d''8    b'8    g'8    \bar "|"   a'16    b'16    a'8    gis'8    b'8    a'4  
  b'8    c''8    \bar "|"     d''4.    b'8    g'8    a'8    b'8    g'8    
\bar "|"   e'4    e'4    d'2    \bar "|"   b'16    b'16    c''8    d''8    b'8  
  a'8    g'8    a'8    b'8    \bar "|"   g'4    g'8    fis'8    g'4    
\bar "|."   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
