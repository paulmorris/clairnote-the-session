\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "dolefulshades"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/185#setting185"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/185#setting185" {"https://thesession.org/tunes/185#setting185"}}}
	title = "Dinny Delaney's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 12/8 \key d \mixolydian   a'4    d'8    d'4. ^"~"    a'4    g'8    e'8    
fis'8    g'8  \bar "|"   a'4    d'8    d'4. ^"~"    g'4    a'8    g'8    e'8    
d'8  \bar "|"   a'4    d'8    d'4. ^"~"    c''4    d''8    e''4    d''8  
\bar "|"   d''8    c''8    a'8    a'8    g'8    e'8    g'4    a'8    g'8    e'8 
   d'8  }     \repeat volta 2 {   d''8    c''8    a'8    a'8    g'8    e'8    
c''4.    a'8    b'8    c''8  \bar "|"   d''8    c''8    a'8    a'8    g'8    
e'8    g'4    a'8    g'8    e'8    d'8  \bar "|"   d''8    c''8    a'8    a'8   
 g'8    e'8    c''4    d''8    e''4    d''8  \bar "|"   d''8    c''8    a'8    
a'8    g'8    e'8    g'4    a'8    g'8    e'8    d'8  }     \repeat volta 2 {   
a'4    d'8    d'4. ^"~"    a'4    b'8    g'4    b'8  \bar "|"   a'4    b'8    
g'4    a'8    e'4    e'8    g'8    e'8    d'8  \bar "|"   a'4    d'8    d'4. 
^"~"    c''4    d''8    e''4    d''8  \bar "|"   d''8    c''8    a'8    a'8    
g'8    e'8    g'4    a'8    g'8    e'8    d'8  }     \repeat volta 2 {   d''8   
 c''8    a'8    a'8    g'8    e'8    c''4.    a'8    b'8    c''8  \bar "|"   
d''8    c''8    a'8    a'8    g'8    e'8    g'4    a'8    g'8    e'8    d'8  
\bar "|"   d''8    c''8    a'8    a'8    g'8    e'8    c''4    d''8    e''4    
d''8  \bar "|"   d''8    c''8    a'8    a'8    g'8    e'8    g'4    a'8    g'8  
  e'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
