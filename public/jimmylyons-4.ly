\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/171#setting12813"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/171#setting12813" {"https://thesession.org/tunes/171#setting12813"}}}
	title = "Jimmy Lyons"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a'8.    a''16    gis''8.    a''16    cis''8    a'8  
\bar "|"   fis'8.    a'16    d''8.    cis''16    \tuplet 3/2 {   b'8    cis''8   
 b'8  } \bar "|"   a'8.    a''16    gis''8.    a''16    e''8    a'8  \bar "|"   
fis'8.    a'16    gis'8.    b'16    \tuplet 3/2 {   b'8    cis''8    a'8  } 
\bar "|"   a'8.    a''16    gis''8.    a''16    e''8    a'8  \bar "|"   fis'8.  
  a'16    d''8.    cis''16    b'8.    gis'16  \bar "|"   a'8.    a''16    
gis''8.    a''16    e''8    a'8  \bar "|"   fis'8.    a'16    gis'8.    b'16    
cis''16    a'8.  \bar "||"   \bar "|" \tuplet 3/2 {   gis''8    fis''8    e''8  
}   \tuplet 3/2 {   d''8    cis''8    b'8  }   \tuplet 3/2 {   a'8    cis''8    
d''8  }   \bar ":|." \tuplet 3/2 {   gis''8    b''8    gis''8  }   \tuplet 3/2 {  
 e''8    fis''8    gis''8  }   \tuplet 3/2 {   a''8    cis''8    b'8  } 
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
