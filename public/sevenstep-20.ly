\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1307#setting14634"
	arranger = "Setting 20"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1307#setting14634" {"https://thesession.org/tunes/1307#setting14634"}}}
	title = "Seven Step, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   d'8.    fis'16    \bar "|"   g'4    g'4    g'4    g'4 
   \bar "|"   fis'8.    g'16    a'8.    fis'16    g'4    d'4    \bar "|"   b'4  
  \tuplet 3/2 {   b'8    b'8    b'8  }   b'4    \tuplet 3/2 {   b'8    b'8    b'8 
 }   \bar "|"   a'8.    b'16    c''8.    a'16    b'4    g'4    \bar "||"     
b'8.    c''16    d''8.    b'16    g'2    \bar "|"   a'8.    b'16    c''8.    
a'16    e'2    \bar "|"   d'8.    e'16    fis'8.    g'16    a'4    \tuplet 3/2 { 
  b'8    c''8    d''8  }   \bar "|"   e''4    d''4    b'2    \bar "|"     b'8.  
  c''16    d''8.    b'16    g'4    g'4    \bar "|"   a'8.    b'16    c''8.    
a'16    e'4    e'4    \bar "|"   d'8.    e'16    fis'8.    g'16    a'8.    b'16 
   \tuplet 3/2 {   c''8    b'8    a'8  }   \bar "|"   b'4    g'4    g'4    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
