\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "joe fidkid"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/449#setting13325"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/449#setting13325" {"https://thesession.org/tunes/449#setting13325"}}}
	title = "Sí Beag Sí Mór"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   fis''8    g''8  \repeat volta 2 {   a''4.    g''8    
fis''4  \bar "|"   fis''4.    g''8    fis''4  \bar "|"   d''4.    e''8    d''8  
  cis''8  \bar "|"   a'2    cis''4  \bar "|"   d''8    cis''8    d''8    e''8   
 fis''4  \bar "|"   g''2    fis''8    g''8  \bar "|"   a''8    b''8    a''8    
g''8  \bar "|"   fis''4.    g''8    a''4  \bar "|"   d''2    g''4  \bar "|"   
cis''2    fis''4  \bar "|"   a'4.    b'8    a'8    g'8  \bar "|"   fis'2    
a''4  \bar "|"   d''2    g''4  \bar "|"   cis''2    fis''8    e''8  \bar "|"   
fis''2    e''4  \bar "|"   fis''2    fis''8    g''8  }   \repeat volta 2 {   
a''4    a''8    g''8    fis''4  \bar "|"   g''8    fis''8    g''8    a''8    
cis'''4  \bar "|"   d'''2    \tuplet 3/2 {   cis'''8    d'''8    cis'''8  } 
\bar "|"   a''2    g''8    fis''8  \bar "|"   g''2    cis'''4  \bar "|"   a''2  
  g''4  \bar "|"   g''2    d''4  \bar "|"   d''2    cis''4  \bar "|"   a'4.    
b'8    a'8    g'8  \bar "|"   fis'2    a''4  \bar "|"   d''2    g''4  \bar "|"  
 cis''2    cis'''4  \bar "|"   d'''8    cis'''8    b''8    a''8    g''8    
fis''8  \bar "|"   g''2    fis''8    e''8  \bar "|"   fis''2.  \bar "|"   
fis''2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
