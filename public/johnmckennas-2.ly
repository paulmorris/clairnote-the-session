\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/934#setting24465"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/934#setting24465" {"https://thesession.org/tunes/934#setting24465"}}}
	title = "John McKenna's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   b'16    \bar "|"   d''4    b'8.   
 ais'16    \bar "|"   b'16    a'16    b'16    c''16    b'8    d'8    \bar "|"   
a'8.    b'16    a'16    fis'16    d'16    fis'16    \bar "|"   a'8    g'8    
g'16    a'16    b'16    c''16    \bar "|"     d''8    b'8    b'4    \bar "|"   
b'8    b'16    c''16    b'8    d'8    \bar "|"   a'8    a'16    b'16    a'16    
fis'16    d'16    fis'16    \bar "|"   a'8    g'8    g'8.    }     
\repeat volta 2 {   a''16    \bar "|"   b''16    a''16    g''16    fis''16    
a''16    g''16    fis''16    e''16    \bar "|"   d''8    b'8    b'16    ais'16  
  b'8    \bar "|"   d''8    a'8    a'16    gis'16    a'8    \bar "|"   d'8    
g'8    g'16    fis'16    g'8    \bar "|"     b''16    a''16    g''16    fis''16 
   a''8    fis''16    e''16    \bar "|"   d''8    b'8    b'16    ais'16    b'16 
   c''16    \bar "|"   d''8    a'8    a'16    gis'16    a'16    fis'16    
\bar "|"   d'8    g'8    g'8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
