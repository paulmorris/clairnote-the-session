\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "stanton135"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/936#setting21842"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/936#setting21842" {"https://thesession.org/tunes/936#setting21842"}}}
	title = "Fair Wind, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   b'8    \bar "|"   d''4.    d''16  
  d''16    d''4.    e''8    \bar "|"   fis''8    e''8    d''8    e''8    fis''8 
   e''8    e''8    fis''8    \bar "|"   d''4.    d''16    d''16    d''4.    
e''8    \bar "|"   fis''8    e''8    d''8    b'8    b'8    a'8    a'8    }     
\repeat volta 2 {   e''8    \bar "|"   fis''8    a''8    a''8    fis''8    
d''4.    e''8    \bar "|"   fis''8    a''8    a''8    fis''8    g''8    e''8    
e''4 ^"~"    \bar "|"   fis''8    a''8    a''8    fis''8    d''4.    e''8    
\bar "|"   fis''8    e''8    d''8    b'8    b'8    a'8    a'8    }     
\repeat volta 2 {   b'8    \bar "|"   d''8    b'8    b'4 ^"~"    g''4.    e''8  
  \bar "|"   fis''8    a''8    a''8    fis''8    e''8    g''8    fis''8    e''8 
   \bar "|"   d''8    b'8    b'4 ^"~"    g''4.    e''8    \bar "|"   fis''8    
e''8    d''8    b'8    b'8    a'8    a'8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
