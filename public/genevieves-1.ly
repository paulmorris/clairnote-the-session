\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Enob"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/135#setting135"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/135#setting135" {"https://thesession.org/tunes/135#setting135"}}}
	title = "Genevieve's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key a \major   e'8  \repeat volta 2 {   cis''8.    d''16    cis''8   
 b'8    a'8    gis'8    \bar "|"   a'8.    b'16    cis''8    e'4    e'8    
\bar "|"   fis'8.    a'16    d''8    cis''8    e'8    cis''8    \bar "|"   
cis''8.    b'16    a'8    b'4    e'8    \bar "|"     cis''8.    d''16    cis''8 
   b'8    a'8    gis'8    \bar "|"   a'8.    b'16    cis''8    e'4    e'8    
\bar "|"   fis'8    a'8    d''8    cis''8    e'8    cis''8    } \alternative{{  
 b'8.    a'16    gis'8    a'4    e'8    } {   b'8.    a'16    gis'8    a'4    
e''8    \bar "||"     fis''8.    fis''16    fis''8    fis''8    gis''8    a''8  
  \bar "|"   e''4    d''8    cis''8.    b'16    a'8    \bar "|"   d''8.    
e''16    d''8    cis''8    b'8    a'8    \bar "|"   fis'8    gis'8    a'8    
b'4    e''8    \bar "|"     fis''8.    fis''16    fis''8    fis''8    gis''8    
a''8    \bar "|"   e''4    d''8    cis''8.    b'16    a'8    \bar "|"   d''8.   
 e''16    d''8    cis''8    b'8    a'8    \bar "|"   fis'8    a'8    gis'8    
a'4    e''8  \bar "|"     fis''8.    fis''16    fis''8    fis''8    gis''8    
a''8    \bar "|"   e''4    d''8    cis''8.    b'16    a'8    \bar "|"   d''8.   
 e''16  \grace {    fis''8    gis''8  }   a''8    e''8    cis''8    a'8    
\bar "|"   fis'8    gis'8    a'8    b'4    e'8    \bar "|"     cis''8.    d''16 
   cis''8    b'8    a'8    gis'8    \bar "|"   a'8.    b'16    cis''8    e'4    
e'8    \bar "|"   fis'8.    a'16    d''8    cis''8    e'8    cis''8    \bar "|" 
  b'8.    a'16    gis'8    a'4.    \bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
