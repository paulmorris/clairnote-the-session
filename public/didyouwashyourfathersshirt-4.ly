\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "benhockenberry"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1022#setting24573"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1022#setting24573" {"https://thesession.org/tunes/1022#setting24573"}}}
	title = "Did You Wash Your Father's Shirt"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   g''8  \bar "|"   fis''8    d''8    e''8    d''8    
cis''8    a'8    a'8    g''8  \bar "|"   fis''8    d''8    e''8    cis''8    
d''4    d''8    e''8  \bar "|"   fis''8    e''8    d''8    cis''8    d''8    
a'8    a'8    b'8  \bar "|"   c''8    a'8    g'8    e'8    d'4.  \bar ":|."   
c''8    a'8    g'8    e'8    d'4    d'8  \bar "||"     e'8  \bar "|"   fis'8    
g'8    a'8    b'8    c''8    a'8    a'8    g'8  \bar "|"   a'8    d''8    d''8  
  cis''8    d''4    d''8    e''8  \bar "|"   fis''8    d''8    e''8    cis''8   
 d''8    a'8    a'8    b'8  \bar "|"   c''8    a'8    g'8    e'8    d'4    d'8  
\bar ":|."   c''8    a'8    g'8    e'8    d'4.  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
