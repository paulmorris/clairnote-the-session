\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Davetnova"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1078#setting1078"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1078#setting1078" {"https://thesession.org/tunes/1078#setting1078"}}}
	title = "Four Poster Bed, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   fis''4    fis''4    e''4    
fis''8    g''8    \bar "|"   a''8    g''8    fis''8    e''8    d''8    cis''8   
 d''8    e''8    \bar "|"   fis''4    fis''4    e''4    d''4    \bar "|"   a''2 
   a'2    }     \repeat volta 2 {   e''4    e''4    e''4    e''8    fis''8    
\bar "|"   e''8    d''8    cis''8    b'8    a'4    a''4    \bar "|"   a'4    
a''4    a'4    a''4    \bar "|"   a'4    a''4    a'4    cis''8    d''8    
\bar "|"     e''4    e''4    e''4    e''8    fis''8    \bar "|"   e''8    d''8  
  cis''8    b'8    a'4    fis''8    g''8    \bar "|"   a''4    g''4    fis''4   
 e''4    } \alternative{{   d''2    d''4    cis''8    d''8    } {   d''8    
a''8    fis''8    a''8    d''4    d''4    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
