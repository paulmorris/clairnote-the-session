\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1190#setting14469"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1190#setting14469" {"https://thesession.org/tunes/1190#setting14469"}}}
	title = "Pearl Wedding, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   a'8    fis'8    \bar "|"   d'4    
d'8.    e'16    d'8    fis'8    a'8    d''8    \bar "|"   fis''8    g''16    
fis''16    fis''8.    e''16    d''8    b'8    b'8    d''16    b'16    \bar "|"  
 fis'16    g'16    a'8    a'8.    b'16    a'8    fis'8    a'8    d''8    
\bar "|"   cis''8    e''8   ~    e''8.    fis''16    e''8    cis''8    a'8    
fis'8    \bar "|"     d'4    d'8    d'16    e'16    d'8.    fis'16    a'8    
d''8    \bar "|"   fis''4    fis''8    e''8    d''8    b'8    b'16    cis''16   
 d''8    \bar "|"   fis'16    a'8.    a'8.    b'16    a'8.    fis'16    a'16    
b'16    cis''16    d''16    \bar "|"   cis''8    d''8    e''8.    cis''16    
d''4    }     \repeat volta 2 {   fis''8    g''8    \bar "|"   a''4    a''8    
g''8    fis''4    e''8    d''8    \bar "|"   b'16    d''8.    d''8.    b'16    
a'8    fis'8    e'8    fis'8    \bar "|"   d'8.    fis'16    a'8    d''8    
fis''16    a''8.    fis''8    d''8    \bar "|"   cis''8    e''8    e''8.    
fis''16    e''4    fis''8    g''8    \bar "|"     a''8.    b''16    a''8    
g''8    fis''8    g''16    fis''16    e''8    d''8    \bar "|"   b'16    
cis''16    d''8    d''8    cis''16    b'16    a'8    fis'8    e'8    d'8    
\bar "|"   d'8    fis'8    a'8.    d'16    fis'8    a'8    a'8    d''8    
\bar "|"   cis''8    d''8    e''8    cis''16    a'16    d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
