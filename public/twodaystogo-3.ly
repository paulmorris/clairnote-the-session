\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/781#setting13911"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/781#setting13911" {"https://thesession.org/tunes/781#setting13911"}}}
	title = "Two Days To Go"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g'4. ^"~"    d''8    e''8    g'8    e'8    g'8  
\bar "|"   d''8    e''8    d''8    b'8    a'8    d''8    b'8    a'8  \bar "|"   
g'4    d''8    g''8    e''8    g'8    b'8    d''8  \bar "|"   e''8    g'8    
d''8    g'8    \tuplet 3/2 {   c''8    b'8    a'8  }   b'8    a'8  \bar "|"   
g'8    d'8    e'8    g'8    d''4.    e''8  \bar "|"   d''8    d'8    b'8    
d''8    a'8    g'8    e'8    g'8  \bar "|" \tuplet 3/2 {   d'8    e'8    g'8  }  
 d'8    b'8    a'8    g'8    g'8    d''8  \bar "|"   d''8    g'8    a'8    b'8  
  a'8    d'8    b'8    a'8  \bar ":|."   d''8    g'8    a'8    b'8    a'8    
g'8    g'4 ^"~"  \bar "||"   \bar ".|:"   b''4    a''8    g''8    a''8    g''4   
 d''8  \bar "|"   e''8    g'8    e''8    g''8    e''8    d''8    b'8    d''8  
\bar "|"   d''4    b''8    g''8    a''8    g''4    d''8  \bar "|"   e''8    g'8 
   \tuplet 3/2 {   d''8    e''8    g''8  }   a''4. ^"~"    b''8  \bar "|"   a''8 
   b''8    g''8    b''8    a''8    g''8    g''4 ^"~"  \bar "|"   e''8    g'8    
d''8    b'8    a'8    g'8    e'8    g'8  \bar "|"   e'8    d'8    g'8    b'8    
a'8    g'8    g'8    d''8  \bar "|"   d''8    g'8    a'8    b'8    a'8    g'8   
 g'4 ^"~"  \bar ":|."   d''8    g'8    a'8    b'8    a'8    d'8    b'8    a'8  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
