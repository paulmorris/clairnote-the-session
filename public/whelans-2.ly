\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "matti"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1447#setting14833"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1447#setting14833" {"https://thesession.org/tunes/1447#setting14833"}}}
	title = "Whelan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \dorian   e'4    b'8    b'8    a'8    fis'8  \bar "|"   fis'8  
  e'8    b'8    a'8    fis'8    b'8  \bar "|"   e'4    b'8    b'8    a'8    b'8 
 \bar "|"   d''8    a'8    fis'8    fis'8    e'8    d'8  \bar "|"     e'4    
b'8    b'8    a'8    fis'8  \bar "|"   fis'8    e'8    b'8    a'8    fis'8    
a'8  \bar "|"   b'8    cis'8    b'8    b'8    a'8    b'8  \bar "|"   d''8    
a'8    fis'8    fis'8    e'8    d'8  }     e'4    e''8    e''8    d''8    e''8  
\bar "|"   fis''8    e''8    d''8    e''4    d''8  \bar "|"   b'8    a'8    b'8 
   g''4    e'8  \bar "|"   fis''8    d''8    b'8    a'8    fis'8    a'8  
\bar "|"     b'4    e''8    e''8    d''8    e''8  \bar "|"   fis''8    e''8    
d''8    e''4    fis''8  \bar "|"   g''8    b''8    g''8    fis''8    a''8    
fis''8  \bar "|"   e''8    d''8    b'8    a'8    fis'8    a'8  \bar ":|."   
e''8    d''8    b'8    a'8    fis'8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
