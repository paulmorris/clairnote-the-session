\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Moxhe"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/241#setting27521"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/241#setting27521" {"https://thesession.org/tunes/241#setting27521"}}}
	title = "Old Gray Goose, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \minor   e'8  \bar "|"   g'8    b'8    g'8    fis'8    a'8    
fis'8  \bar "|"   g'8    e'8    e'8    e'4    fis'8  \bar "|"   g'8    b'8    
g'8    fis'8    a'8    g'8  \bar "|"   fis'8    d'8    d'8    d'8    e'8    
fis'8  \bar "|"     g'8    fis'8    g'8    a'8    g'8    a'8  \bar "|"   b'8    
d''8    e''16    fis''16    g''8    fis''8    e''8  \bar "|"   d''8    b'8    
g'8    a'8    g'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4  }     
\repeat volta 2 {   d'8  \bar "|"   g'8.    a'16    b'16    c''16    d''8    
b'8    g'8  \bar "|"   b'8    a'8    b'8    d''8    b'8    g'8  \bar "|"   a'8. 
   b'16    c''16    d''16    e''8    c''8    a'8  \bar "|"   c''8    b'8    
c''16    d''16    e''8    c''8    a'8  \bar "|"     g'8.    a'16    b'16    
c''16    d''8    a'8    d''8  \bar "|"   e''8    g''8    fis''16    a''16    
g''8    fis''8    e''8  \bar "|"   d''8    b'8    g'8    a'8    g'8    fis'8  
\bar "|"   g'8    e'8    e'8    e'4  }     \repeat volta 2 {   fis''8  \bar "|" 
  g''8    b''8    g''8    a''8    fis''8    a''8  \bar "|"   g''8    e''8    
e''8    e''4    fis''8  \bar "|"   g''8    b''8    g''8    fis''8    a''8    
g''8  \bar "|"   fis''8    d''8    d''8    d''8    e''8    fis''8  \bar "|"     
g''16    a''16    b''8    g''8    fis''16    g''16    a''8    fis''8  \bar "|"  
 e''8    g''8    fis''16    a''16    g''8    fis''8    e''8  \bar "|"   d''8    
b'8    g'8    a'8    g'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4  }     
\repeat volta 2 {   a''8  \bar "|"   g''8    d''8    b'8    a'8    g'8    fis'8 
 \bar "|"   g'8    e'8    e'8    e'4    fis''8  \bar "|"   g''8    d''8    b'8  
  a'8    b'8    g'8  \bar "|"   fis'8    d'8    d'8    d'8    e'8    fis'8  
\bar "|"     g'8    fis'8    g'8    a'8    g'8    b'16    c''16  \bar "|"   
d''8    e''8    fis''8    g''8    fis''8    e''8  \bar "|"   d''8    b'8    g'8 
   a'8    g'8    fis'8  \bar "|"   g'8    e'8    e'8    e'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
