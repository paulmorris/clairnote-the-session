\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/320#setting22650"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/320#setting22650" {"https://thesession.org/tunes/320#setting22650"}}}
	title = "Bank Of Ireland, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \mixolydian   e''8    d''8  \bar "|"   c''8    a'8    a'4 
^"~"    c''8    a'8    a'8    g'8  \bar "|"   e'8    g'8    d'8    g'8    e'8   
 g'8    d'4  \bar "|"   c''8    a'8    a'4 ^"~"    c''8    a'8    b'8    g'8  
\bar "|"   a'8    d''8    d''8    cis''8    d''8    fis''8    e''8    d''8  
\bar "|"     c''4    a'8    b'8    c''8    a'8    a'8    g'8  \bar "|"   e'8    
g'8    d'8    g'8    e'8    g'8    d'4  \bar "|"   c''8    a'8    a'4 ^"~"    
c''8    a'8    b'8    g'8  \bar "|"   a'8    d''8    d''8    cis''8    d''4    
d''8    e''8  \bar "||"     fis''4 ^"~"    fis''8    d''8    e''4 ^"~"    e''8  
  cis''8  \bar "|"   d''8    fis''8    e''8    d''8    cis''8    a'8    a''8    
g''8  \bar "|"   fis''4 ^"~"    fis''8    d''8    e''8    fis''8    g''8    
e''8  \bar "|"   a''8    g''8    e''8    cis''8    d''4    cis''8    d''8  
\bar "|"     e''8    a''8    a''8    g''8    e''8    fis''8    g''8    e''8  
\bar "|"   d''8    fis''8    e''8    d''8    cis''8    a'8    g'4  \bar "|"   
a'8    d''8    d''8    cis''8    d''8    e''8    fis''8    g''8  \bar "|"   
a''8    fis''8    g''8    e''8    d''4  \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
