\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "matti"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/1226#setting1226"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1226#setting1226" {"https://thesession.org/tunes/1226#setting1226"}}}
	title = "Girls Of Grindavik, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 9/8 \key a \dorian   a'4    b'8    c''8    b'8    a'8    g'8    e'8    
e'8    \bar "|"   a'4    b'8    c''8    e''8    d''8    b'8    a'8    g'8    
\bar "|"   a'4    b'8    c''8    b'8    a'8    g'8    c''8    d''8    \bar "|"  
 e''8    g''8    fis''8    g''8    d''8    c''8    b'8    a'8    g'8  }     
\repeat volta 2 {   e''8    g''8    g''8    d''8    g''8    g''8    c''8    
g''8    g''8    \bar "|"   b'8    g'8    g'8    g'4    a'8    b'8    c''8    
d''8    \bar "|"   e''8    g''8    g''8    d''8    g''8    e''8    d''8    c''8 
   b'8    } \alternative{{   c''8    a'8    a'8    a'8    b'8    g'8    a'8    
c''8    d''8  } {   c''8    a'8    a'8    a'8    b'8    g'8    a'4    e'8    
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
