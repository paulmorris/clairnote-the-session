\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "didier"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/543#setting25010"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/543#setting25010" {"https://thesession.org/tunes/543#setting25010"}}}
	title = "Ships Are Sailing, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   b'8    e''8    e''8    b'8    cis''8    d''8    
\bar "|"   a'8    fis'8    d'8    a'8    fis'8    d'8  \bar "|"   e'8    d'8    
e'8    g'8    fis'8    g'8    \bar "|"   b'8    e''8    e''8    g''8    fis''8  
  e''8    \bar "|"     b'8    e''8    e''8    b'8    cis''8    d''8    \bar "|" 
  a'8    fis'8    d'8    a'8    fis'8    d'8  \bar "|"   e'8    d'8    e'8    
g'8    fis'8    g'8    \bar "|"   b'8    e''8    dis''8    e''4    dis''8  
\bar ":|."   b'8    e''8    dis''8    e''8    e''8    fis''8  \bar "|"     g''8 
   fis''8    g''8    b''8    g''8    e''8    \bar "|"   fis''8    e''8    
fis''8    a''8    fis''8    d''8    \bar "|"   g''8    fis''8    g''8    b''8   
 g''8    e''8    \bar "|"   fis''8    e''8    d''8    e''4    fis''8    
\bar "|"     g''8    fis''8    g''8    b''8    g''8    e''8    \bar "|"   
fis''8    e''8    fis''8    a''8    fis''8    d''8    \bar "|"   g''8    b''8   
 g''8    fis''8    a''8    fis''8    \bar "|"   e''8    d''8    e''8    g''8    
fis''8    e''8    \bar ":|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
