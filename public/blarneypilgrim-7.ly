\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bryce"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/5#setting28409"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/5#setting28409" {"https://thesession.org/tunes/5#setting28409"}}}
	title = "Blarney Pilgrim, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \mixolydian   \repeat volta 2 {   d'8 ^"G"   e'8    d'8    d'8 
   e'8    g'8  \bar "|"     a'4 ^"D"   a'8    a'8    b'8    c''8  \bar "|"     
b'8 ^"G"   a'8    g'8      a'8 ^"Am"   g'8    e'8  \bar "|"     g'8 ^"Em"   e'8 
   a'8      g'8 ^"C"   e'8    d'8  \bar "|"       d'8 ^"G"   e'8    d'8    d'8  
  e'8    g'8  \bar "|"   a'4 ^"D"   a'8    a'8    b'8    c''8  \bar "|"   b'8 
^"G"   a'8    g'8      a'8 ^"Am"   g'8    e'8  \bar "|"     g'8 ^"D"   e'8    
d'8    d'4.  }     \repeat volta 2 {   d''8 ^"G"   e''8    d''8    d''8    b'8  
  g'8  \bar "|"   a'8 ^"D"   g'8    a'8      b'8 ^"G"   g'8    e'8  \bar "|"    
 d''8 ^"G"   e''8    d''8    d''8    b'8    g'8  \bar "|"   a'8 ^"D"   g'8    
a'8      g'8 ^"G"   a'8    b'8  \bar "|"       g''4 ^"Em"   e''8      d''8 ^"G" 
  b'8    g'8  \bar "|"   a'8 ^"Am"   g'8    a'8      b'8 ^"Em"   g'8    e'8  
\bar "|"   b'4 ^"G"   g'8      a'8 ^"C"   g'8    e'8  \bar "|"     g'8 ^"D"   
a'8    g'8    g'4.  }     \repeat volta 2 {   a'4 ^"D"   d'8      b'4 ^"G"   
d'8  \bar "|"     a'4 ^"D"   d'8    a'8    b'8    c''8  \bar "|"   b'8 ^"G"   
a'8    g'8      a'8 ^"Am"   g'8    e'8  \bar "|"   g'8 ^"Em"   e'8    a'8      
g'8 ^"C"   e'8    d'8  \bar "|"       a'8 ^"D"   d'8    d'8      b'8 ^"G"   d'8 
   d'8  \bar "|"   a'8 ^"D"   d'8    d'8    a'8    b'8    c''8  \bar "|"   b'8 
^"G"   a'8    g'8      a'8 ^"Am"   g'8    e'8  \bar "|"   g'8 ^"C"   e'8    d'8 
     d'4. ^"D" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
