\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Weejie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/165#setting12796"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/165#setting12796" {"https://thesession.org/tunes/165#setting12796"}}}
	title = "Merry Sisters, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   d''8    cis''8  \repeat volta 2 {   b'8    e'8  
\grace {    g'8  }   e'8  \grace {    fis'8  }   e'8    b'8    d''8    cis''16  
\grace {    d''8  }   cis''16    a'8  \bar "|" \grace {    cis''8  }   b'8    
e'8  \grace {    g'8  }   e'8  \grace {    fis'8  }   e'8    d''8    a'8    
fis'8    a'8  \bar "|"   b'8    e'8  \grace {    fis''8  }   e''8    cis''8    
d''4    cis''8    a'8  \bar "|"   b'8    e''8  \grace {    fis''8  }   e''8    
d''8    b'8    a'8    fis'8    a'8  \bar "|"   b'8    e''8  \grace {    fis''8  
}   e''8    cis''8    d''8    cis''8    d''8    a'8  \bar "|"   b'8.  \grace {  
  cis''8  }   b'16  \grace {    a'8  }   b'8    cis''8    d''8    a'8    fis'8  
  a'8  \bar "|"   b'8    e''8  \grace {    fis''8  }   e''8    cis''8    d''4  
 ~    d''8    a'8  \bar "|"   b'8    e''8  \grace {    fis''8  }   e''8    
cis''8    d''8    e''8    fis''8    g''8  \bar "|"   a''8    fis''8    g''16 -. 
  fis''16 -.   e''8    fis''8    d''8    e''8    d''8  \bar "|"   b'8    a'8    
b'8    cis''8    d''8    a'8    fis'8    a'8  \bar "|"   b'8    d''8    cis''8  
  a'8  \grace {    cis''8  }   b'8    a'8    fis'8    a'8  \bar "|"   b'8    
e'8  \grace {    g'8  }   e'8  \grace {    fis'8  }   e'8    b'8    e'8    
fis'8    a'8  \bar "|"   b'8.  \grace {    cis''8  }   b'16  \grace {    a'8  } 
  b'8    a'8    b'8.  \grace {    cis''8  }   b'16  \grace {    a'8  }   b'8    
cis''8  \bar "|"   d''8    d'8  \grace {    g'8  }   d'8  \grace {    fis'8  }  
 d'8    a'8    d'8    fis'8    a'8  \bar "|"   b'8    d''8    cis''8    a'8  
\grace {    cis''8  }   b'8    a'8    fis'8    a'8  \bar "|"   b'8    e'8  
\grace {    g'8  }   e'8  \grace {    fis'8  }   e'8  \grace {    cis''8  }   
b'8    a'8    fis'8    a'8  \bar "|"   d'4    e'16 -.   fis'16 -.   a'8    d''4 
   e''8    g''8  \bar "|"   fis''8    d''8    e''8    cis''8    d''8    a'8    
fis'8    a'8  \bar "|."   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
