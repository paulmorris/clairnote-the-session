\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/401#setting26366"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/401#setting26366" {"https://thesession.org/tunes/401#setting26366"}}}
	title = "Gander In The Pratie Hole, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   fis'8    a'8    d'8    fis'8    
a'8    d'8  \bar "|"   g'8    fis'8    g'8    e'8    fis'8    g'8  \bar "|"   
fis'16    g'16    a'8    d'8    fis'16    g'16    a'8    d'8  \bar "|"   g'8    
e'8    d'8    d'4.  \bar "|"     fis'8    a'8    d'8    fis'8    a'8    d'8  
\bar "|"   g'8    fis'8    g'8    e'8    fis'8    g'8  \bar "|"   g''16    
fis''16    e''8    d''8    e''8    cis''8    a'8  \bar "|"   g'8    e'8    d'8  
  d'4.  }     \repeat volta 2 {   |
 a'8    d''8    d''8    d''8    e''8    d''8  \bar "|"   cis''8    a'8    b'8   
 c''8    a'8    g'8  \bar "|"   a'8    d''8    d''8    d''8    e''8    d''8  
\bar "|"   cis''!8    a'8    b'8    c''4    b'8  \bar "|"     a'8    d''8    
d''8    d''8    e''8    d''8  \bar "|"   cis''!8    a'8    b'8    c''8    d''8  
  e''8  \bar "|"   g''16    fis''16    e''8    d''8    e''8    cis''8    a'8  
\bar "|"   g'8    e'8    d'8    d'4.  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
