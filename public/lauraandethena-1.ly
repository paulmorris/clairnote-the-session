\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "WilkietheMound"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/688#setting688"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/688#setting688" {"https://thesession.org/tunes/688#setting688"}}}
	title = "Laura And Ethena"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   a'8    cis''8  \bar "|"   d''4    f''8    d''8    a'8 
   f'8    d'8    f'8  \bar "|"   e'16    e'16    e'8    c''8    e'8    d''8    
e'8    c''8    e'8  \bar "|"   cis''8    d''8    f''8    d''8    a'8    f'8    
d'8    f'8  \bar "|"   a'8    d''4    cis''8    d''8    a'8    f''8    a'8  
\bar "|"     d''4    f''8    d''8    a'8    f'8    d'8    f'8  \bar "|"   e'16  
  e'16    e'8    c''8    e'8    d''8    e''8    c''8    e'8  \bar "|"   d''8    
e''8    f''8    e''8    d''8    c''8    a'8    f'8  \bar "|"   g'8    f'8    
e'8    c'8    c'8    d'8    d'8    a'8  \bar "|"     cis''8    d''8    f''8    
d''8    a'8    f'8    d'8    f'8  \bar "|"   e'16    e'16    e'8    c''8    e'8 
   d''8    e'8    c''8    e'8  \bar "|"   cis''8    d''8    f''8    d''8    
a''8    f''8    d''8    f''8  \bar "|"   a''8    d''4    cis''8    cis''8    
a'8    f'8    a'8  \bar "|"     cis''8    d''8    f''8    d''8    a'8    f'8    
d'8    f'8  \bar "|"   e'16    e'16    e'8    c''8    e'8    d''8    e'8    
c''8    e'8  \bar "|"   d''8    e''8    f''8    e''8    d''8    c''8    a'8    
f'8  \bar "|"   g'8    f'8    e'8    c'8    c'8    d'8    d'8    a'8  \bar "|"  
   cis''8    d''8    a''8    d''8    bes''8    d''8    a''8    d''8  \bar "|"   
d''16    d''16    d''8    a''8    d''8    bes''8    d''8    a''8    d''8  
\bar "|"   c''16    c''16    c''8    g''8    c''8    a''8    c''8    g''8    
c''8  \bar "|"   c''4    g''8    a''4    g''8    f''8    e''8  \bar "|"     
bes'16    bes'16    bes'8    f''8    bes'8    g''8    bes'8    f''8    bes'8  
\bar "|"   bes'16    bes'16    bes'8    f''8    g''4    f''8    e''8    d''8  
\bar "|"   cis''8    a'8    cis''8    e''8    aes''8    g''8    f''8    e''8  
\bar "|"   f''8    g''8    e''8    f''8    d''8    a'8    b'8    cis''8  
\bar "|"     d''8    a''8    f''8    d''8    a''8    f''8    d''8    a''8  
\bar "|"   bes''8    a''8    g''8    f''8    e''8    f''8    e''8    d''8  
\bar "|"   c''8    g''8    e''8    c''8    g''8    e''8    c''8    g''8  
\bar "|"   a''8    g''8    f''8    e''8    f''8    e''8    d''8    c''8  
\bar "|"     a'8    bes'8    f''8    bes'8    bes'8    g''8    bes'8    bes'8  
\bar "|"   aes''8    bes'8    bes'16    bes'16    bes'8    g''8    bes'8    
f''8    bes'8  \bar "|"   e''8    dis''8    e''8    gis''8    a''8    g''!8    
f''8    e''8  \bar "|"   f''8    g''8    e''8    f''8    d''8    d'8  \bar "|"  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
