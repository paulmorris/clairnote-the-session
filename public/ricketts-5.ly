\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/272#setting25361"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/272#setting25361" {"https://thesession.org/tunes/272#setting25361"}}}
	title = "Rickett's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \major   f''4.    c''8   ~    c''4    f''8    e''8    \bar "|" 
  d''4    g''8    f''8    e''4    d''8    c''8    \bar "|"   d''4.    c''8   ~  
  c''4    bes'8    a'8    \bar "|"   g'4    c''8    bes'8    a'4    g'8    f'8  
  \bar "|"     f''4.    c''8   ~    c''4    f''8    e''8    \bar "|"   d''4    
g''8    f''8    e''4    d''8    c''8    \bar "|"   d''4    c''4   ~    c''4    
b'4    \bar "|"   c''2    r4   a'4    \bar "|"     bes'8    c''8    d''8    
e''8    f''4.    f''8    \bar "|"   g''8    a''8    bes''8    a''8    g''4.    
f''8    \bar "|"   g''4    f''4    f''4    e''4    \bar "|"   f''2    r4   a'4  
  \bar "|"     bes'8    c''8    d''8    e''8    f''4.    f''8    \bar "|"   
g''8    a''8    bes''8    a''8    g''4.    f''8    \bar "|"   g''4    f''4    
f''4    e''4    \bar "|"   f''2   ~    f''4    r4   \bar "|."   \key g \major   
g''4.    d''8   ~    d''4    g''8    fis''8    \bar "|"   e''4    a''8    g''8  
  fis''4    e''8    d''8    \bar "|"   e''4.    d''8   ~    d''4    c''8    b'8 
   \bar "|"   a'4    d''8    c''8    b'4    a'8    g'8    \bar "|"     g''4.    
d''8   ~    d''4    g''8    fis''8    \bar "|"   e''4    a''8    g''8    fis''4 
   e''8    d''8    \bar "|"   e''4    d''4   ~    d''4    cis''4    \bar "|"   
d''2    r4   b'4    \bar "||"     c''8    d''8    e''8    fis''8    g''4.    
g''8    \bar "|"   a''8    b''8    c'''8    b''8    a''4.    g''8    \bar "|"   
a''4    g''4    g''4    fis''4    \bar "|"   g''2    r4   b'4    \bar "|"     
c''8    d''8    e''8    fis''8    g''4.    g''8    \bar "|"   a''8    b''8    
c'''8    b''8    a''4.    g''8    \bar "|"   a''4    g''4    g''4    fis''4    
\bar "|"   g''2   ~    g''4    r4   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
