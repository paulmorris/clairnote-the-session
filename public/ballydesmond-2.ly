\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Fliúiteadóir"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/298#setting13050"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/298#setting13050" {"https://thesession.org/tunes/298#setting13050"}}}
	title = "Ballydesmond, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   a'8.    b'16    a'8    g'8  
\bar "|"   e'8    fis'8    g'8    e'8  \bar "|"   a'8.    b'16    a'8    g'8  
\bar "|"   a'8    d''8    d''8    c''8  \bar "|"   a'8.    b'16    a'8    g'8  
\bar "|"   e'8    fis'8    g'4  \bar "|"   a'8    b'8    c''8    e'8  \bar "|"  
 e'8    d'8    d'4  }   e''8    d''8    cis''8    d''8    \bar "|"   e''8    
a'8    b'16    cis''16    d''8  \bar "|"   e''8    d''8    cis''8    d''8  
\bar "|"   e''8    a''8    a''8.    g''16  \bar "|"   e''8    d''8    cis''8    
d''8  \bar "|"   e''8    d''8    cis''8    d''8  \bar "|"   a'4.    b'8    c''8 
   d''8  \bar "|"   e''8    fis''8    g''8    fis''8  \bar "|"   e''8    d''8   
 cis''8    d''8    \bar "|"   e''8    a'8    b'16    cis''16    d''8  \bar "|"  
 e''8    d''8    cis''8    d''8  \bar "|"   e''8    a''8    a''8.    g''16    
\bar "|"   \tuplet 3/2 {   e''8    fis''8    g''8  }   d''8.    b'16  \bar "|"   
c''8    e''8    d''8.    b'16  \bar "|"   a'8    b'8    c''8    e'8  \bar "|"   
e'8    d'8    d'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
