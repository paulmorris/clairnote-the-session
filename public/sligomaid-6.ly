\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/399#setting13249"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/399#setting13249" {"https://thesession.org/tunes/399#setting13249"}}}
	title = "Sligo Maid, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \dorian   fis''8  \bar "|"   e''8    fis''8    e''8    d''8    
cis''8    d''8    e''8    fis''8  \bar "|"   g''8    e''8    d''8    b'8    a'8 
   g'8    e'8    fis'8  \bar "|"   g'4    a'8    g'8    b'8    g'8    a'8    
g'8  \bar "|"   d'8    e'8    g'8    a'8    b'8    a'8    a'8    fis''8  
\bar "|"   e''8    fis''8    e''8    d''8    \tuplet 3/2 {   b'8    cis''8    
d''8  }   e''8    fis''8  \bar "|"   g''8    e''8    d''8    b'8    a'8    g'8  
  e'8    fis'8  \bar "|"   g'8    a'8    a'8    g'8    b'8    g'8    a'8    g'8 
 \bar "|"   d'8    e'8    g'8    a'8    b'8    a'8    a'8  }   
\repeat volta 2 {   g''8  \bar "|"   e''8    a''8    a''8    g''8    a''4    
g''8    fis''8  \bar "|"   g''8    a''8    b''8    g''8    a''8    g''8    e''8 
   fis''8  \bar "|"   g''4. ^"~"    e''8    d''4 ^"~"    e''8    fis''8  
\bar "|"   g''8    d''8    e''8    d''8    \tuplet 3/2 {   b'8    cis''8    d''8 
 }   e''8    g''8  \bar "|"   e''8    a''8    a''8    g''8    a''4    g''8    
fis''8  \bar "|"   g''8    a''8    b''8    g''8    a''8    g''8    e''8    
fis''8  \bar "|"   g''8    b''8    e''8    g''8    d''4 ^"~"    e''8    fis''8  
\bar "|"   g''8    d''8    e''8    d''8    b'8    a'8    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
