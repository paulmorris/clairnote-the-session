\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mark Cordova"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/838#setting838"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/838#setting838" {"https://thesession.org/tunes/838#setting838"}}}
	title = "Andy De Jarlis'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \major   e'8    gis'8    b'8    e'8    gis'8    b'8  \bar "|"  
 e'8    gis'8    b'8    cis''4    b'8  \bar "|"   e'8    gis'8    b'8    e'8    
gis'8    b'8  \bar "|"   d'8    fis'8    a'8    b'4    a'8  \bar "|"     e'8    
gis'8    b'8    e'8    gis'8    b'8  \bar "|"   e'8    gis'8    b'8    cis''4   
 a''8  \bar "|"   gis''8    fis''8    e''8    b'8    cis''8    dis''8  \bar "|" 
  e''4.    e''4.  \bar ":|."   e''4.    e''4    a''8  \bar "||"     \bar ".|:"   
gis''8    fis''8    gis''8    e''8    fis''8    gis''8  \bar "|"   a''8    
gis''8    a''8    cis''4    e''8  \bar "|"   fis''4. ^"~"    fis''8    gis''8   
 e''8  \bar "|"   dis''8    cis''8    b'8    cis''4    b'8  \bar "|"     gis''8 
   b''8    gis''8    e''8    fis''8    gis''8  \bar "|"   a''8    gis''8    
a''8    cis''4    e''8  \bar "|"   fis''8    gis''8    fis''8    b'8    cis''8  
  dis''8  \bar "|"   e''4.    e''4    a''8  \bar ":|."   e''4.    e''4.  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
