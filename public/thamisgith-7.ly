\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/647#setting28139"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/647#setting28139" {"https://thesession.org/tunes/647#setting28139"}}}
	title = "Tha Mi Sgith"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   dis'16    \bar "|"   e'4 ^"~"    
e''4    d''16    b'8.    d''4    \bar "|"   b'8.    a'16    fis'8.    e'16    
d'8.    e'16    fis'16    d'8.    \bar "|"     \tuplet 3/2 {   e'8    e'8    e'8 
 }   e''4    d''16    b'8.    d''4    \bar "|"   b'8.    a'16    fis'16    
d''8.    e'4    e'8.    }     \repeat volta 2 {   bes'16    \bar "|"   b'8.    
a'16    fis'8.    e'16    d'8.    e'16    fis'16    d'8.    \bar "|"   b'8.    
a'16    fis'16    e'8.    fis'4    d''4    \bar "|"     b'8.    a'16    fis'8.  
  e'16    d'8.    e'16    fis'16    a'8.    \bar "|"   b'16    a'8.    d''16    
fis'8.    e'4    e'8.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
