\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ithaca-markb"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1290#setting27073"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1290#setting27073" {"https://thesession.org/tunes/1290#setting27073"}}}
	title = "Mama's Pet"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \bar "|"   g'4 ^"~"    fis'8    g'8    e'8    g'8    
d'8    e'8  \bar "|"   g'4    \tuplet 3/2 {   b'8    a'8    g'8  }   d''8    g'8 
   b'8    a'8  \bar "|"   g'4 ^"~"    fis'8    g'8    e'8    g'8    d'8    g'8  
  \bar "|"   b'8    d''8  \grace {    fis''8  }   e''8    d''8    b'8    a'8  
\grace {    b'8  }   a'8    b'8  \bar "|"     \bar "|"   g'4    fis'8    g'8    
e'8    g'8    d'8    e'8    \bar "|"   g'4    \tuplet 3/2 {   b'8    a'8    g'8  
}   d''8    g'8    b'8    a'8    \bar "|"   g'4 ^"~"    fis'8    g'8    e'8    
g'8    d'8    g'8    \bar "|"   b'8    d''8  \grace {    fis''8  }   e''8    
d''8    b'8    a'8    a'8    e''8  \bar "|"     \bar "|"   d''8    g'8  
\tuplet 3/2 {   b'8    a'8    g'8  }   d''4.    e''8  \bar "|"   d''8    g''8    
b''8    g''8    a''8    b''8    g''8    e''8  \bar "|"   d''8    g'8  
\tuplet 3/2 {   b'8    a'8    g'8  }   d''4    g'8    a'8  \bar "|"   b'8    
d''8  \grace {    fis''8  }   e''8    d''8    b'8    a'8  \grace {    b'8  }   
a'8    e''8  \bar "|"     \bar "|"   d''8    g'8  \tuplet 3/2 {   b'8    a'8    
g'8  }   d''4.    e''8  \bar "|"   d''8    g''8    b''8    g''8    a''8    b''8 
   g''4  \bar "|"   g'4    g'8    b'8    a'4 ^"~"    g'8    a'8  \bar "|"   b'8 
   d''8  \grace {    fis''8  }   e''8    d''8    b'8    a'8  \grace {    b'8  } 
  a'8    b'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
