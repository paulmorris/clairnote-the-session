\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/33#setting25520"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/33#setting25520" {"https://thesession.org/tunes/33#setting25520"}}}
	title = "Farewell To Erin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   a8 ^"Am"   a16    a16    a8    
c'8    e'4.    fis'8  \bar "|"   g'8 ^"G"   e'8    d'8    b8    g8    b8    d'8 
   b8  \bar "|"   a8 ^"Am"   a16    a16    a8    c'8    e'8    e'16    e'16    
e'8    fis'8  \bar "|"   g'8 ^"G"   e'8    d'8    fis'8    e'8    a8    a8    
g8  \bar "|"       a8 ^"Am"   a16    a16    a8    c'8    e'4.    fis'8  
\bar "|"   g'8 ^"G"   e'8    d'8    b8    d'4    g''4  \bar "|"   e''8 ^"Em"   
d''8    e''8    g''8      a''4 ^"F"   b''8    a''8  \bar "|"   g''8 ^"G"   e''8 
   d''8    b'8      b'8 ^"Am"   a'8    a'4    }     \repeat volta 2 {   a''16 
^"Am"   a''16    a''8    a''8    b''8    a''8    g''8    e''8    fis''8  
\bar "|"   g''16 ^"G"   g''16    g''8    g''8    a''8    g''8    e''8    d''8   
 e''8  \bar "|"   a''16 ^"Am"   a''16    a''8    a''8    b''8    a''8    g''8   
 e''8    fis''8  \bar "|"   g''8 ^"G"   e''8    d''8    b'8      b'8 ^"Am"   
a'8    a'4  \bar "|"       a''8 ^"Am"   g''8    e''8    fis''8    g''8    a''8  
  g''8    e''8  \bar "|"   d''16 ^"G"   d''16    d''8    d''8    b'8    g'8    
a'8    b'8    d''8  \bar "|"   c''8 ^"F"   a'8    e''8    a'8    a'16    b'16   
 c''8    e''8    fis''8  \bar "|"   g''8 ^"G"   e''8    d''8    b'8      b'8 
^"Am"   a'8    a'4    }     \repeat volta 2 {   e''8 ^"Am"   a'8    c''16    
b'16    a'8    e''8    a'8    c''16    b'16    a'8  \bar "|"   d''16 ^"G"   
d''16    d''8    d''8    b'8    g'8    a'8    b'8    d''8  \bar "|"   e''8 
^"Am"   a'8    c''16    b'16    a'8    e''8    a'8    c''16    b'16    a'8  
\bar "|"   g''8 ^"G"   e''8    d''8    b'8      b'8 ^"Am"   a'8    a'4  
\bar "|"       e''8 ^"Am"   a'8    c''16    b'16    a'8    e''8    a'8    c''16 
   b'16    a'8  \bar "|"   d''16 ^"G"   d''16    d''8    d''8    b'8    g'8    
a'8    b'8    d''8  \bar "|"   c''8 ^"F"   a'8    e''8    a'8    a'16    b'16   
 c''8    e''8    fis''8  \bar "|"   g''8 ^"G"   e''8    d''8    b'8      b'8 
^"Am"   a'8    a'4    }     \repeat volta 2 {   a''8 ^"A"   e''8    cis''8    
e''8    a''8    e''8    cis''!8    e''8  \bar "|"   g''8 ^"G"   d''8    b'8    
d''8    g'8    d''8    b'8    d''8  \bar "|"   a''8 ^"A"   e''8    cis''8    
e''8    a''4    a''8    fis''8  \bar "|"   g''8 ^"G"   e''8    d''8    b'8      
b'8 ^"Am"   a'8    a'4  \bar "|"   a''8    g''8    e''8    fis''8    g''8    
a''8    g''8    e''8  \bar "|"   d''8 ^"G"   e''8    d''8    b'8    g'8    a'8  
  b'8    d''8  \bar "|"     } \alternative{{       c''8 ^"F"-.   a'8    e''8    
a'8    a'16    b'16    c''8    e''8    fis''8  \bar "|"   g''8 ^"G"   e''8    
d''8    b'8      b'8 ^"Am"   a'8    a'4    } {       c''8 ^"F"-.   a'8    b'8   
 a'8    a'16    b'16    c''8    e''8    a''8  \bar "|"   g''8 ^"G"   e''8    
d''8    b'8      b'8 ^"Am"   a'8    a'4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
