\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "SPeak"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/920#setting920"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/920#setting920" {"https://thesession.org/tunes/920#setting920"}}}
	title = "Road To Recovery, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \minor   d'8    a8    b8    c'8    d'8    f'8    a'8    f'8  
\bar "|"   c''8    g'8    b'8    g'8    c''8    g'8    b'8    g'8  \bar "|"   
d'8    a8    b8    c'8    d'8    bes8    bes!4 ^"~"  \bar "|"   f'8    bes8    
d'8    bes8    g'8    c'8    e'8    c'8  \bar "|"     d'8    a8    b8    c'8    
d'8    f'8    a'8    f'8  \bar "|"   c''8    g'8    b'8    g'8    c''8    g'8   
 b'8    g'8  \bar "|"   f'8    bes'8    bes'4 ^"~"    a'8    f'8    c'8    e'8  
\bar "|"   e'8    d'8    c'8    e'8    f'8    d'8    d'4  }     
\repeat volta 2 {   a'8    d''8    d''4 ^"~"    f''8    d''8    d''4 ^"~"  
\bar "|"   f''8    d''8    g''8    d''8    a''8    d''8    d''4 ^"~"  \bar "|"  
 a'8    d''8    d''4 ^"~"    f''8    d''8    d''4 ^"~"  \bar "|"   a''8    d''8 
   g''8    d''8    e''8    c''8    c''4 ^"~"  \bar "|"     c''8    g'4. ^"~"    
f'8    d'8    d'4 ^"~"  \bar "|"   a'8    d'8    e'8    d'8    c'4.    e'8  
\bar "|"   f'8    d'8    bes8    f'8    d'8    a8    f'8    d'8  \bar "|"   
bes8    d'8    c'8    e'8    f'8    d'8    d'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
