\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "stacey"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/783#setting783"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/783#setting783" {"https://thesession.org/tunes/783#setting783"}}}
	title = "Solstice, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \major   a8    c'8  \repeat volta 2 {   d'8    d''8    a'8    
f'8    d'8    e'8    f'8    d'8  \bar "|"   c'8    e'8    \tuplet 3/2 {   e'8    
e'8    e'8  }   f'8    e'8    d'8    c'8  \bar "|"   d'8    d''8    a'8    f'8  
  d'8    e'8    f'8    d'8  } \alternative{{   c'8    e'8    a8    cis'8    d'4 
   a8    cis'8  } {   c'8    e'8    a8    cis'8    d'8    fis'8    g'8    a'8  
\bar "||"     d''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   c''8    d'8  
  \tuplet 3/2 {   d'8    d'8    d'8  } \bar "|"   b'8    g'8    \tuplet 3/2 {   
g'8    g'8    g'8  }   a'8    f'8    e'8    f'8  \bar "|"   c'8    f'8    a8    
f'8    g8    a8    c'8    d'8  \bar "|"   f'8    a'8    c''8    f''8    e''8    
c''8    b'8    c''8  \bar "|"     d''8    d'8    \tuplet 3/2 {   d'8    d'8    
d'8  }   c''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  } \bar "|"   b'8    
g'8    \tuplet 3/2 {   g'8    g'8    g'8  }   a'8    f'8    e'8    f'8  \bar "|" 
  c'8    f'8    a8    f'8    g8    a8    c'8    d'8  \bar "|"   g'8    f'8    
e'8    f'8    d'4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
