\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1307#setting26735"
	arranger = "Setting 23"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1307#setting26735" {"https://thesession.org/tunes/1307#setting26735"}}}
	title = "Seven Step, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   \tuplet 3/2 {   e'8    fis'8    
gis'8  }   \bar "|"   a'2    a'2    \bar "|"   a'8.    b'16    cis''8.    a'16  
  fis'4    e'4    \bar "|"   cis''2    cis''2    \bar "|"   cis''8.    d''16    
e''8.    cis''16    b'4   ~    b'8.    \bar "||"     b'16    \bar "|"   cis''8. 
   d''16    e''8.    cis''16    b'4    a'4    \bar "|"   a'8.    b'16    
cis''8.    a'16    fis'4    eis'4    \bar "|"   e'8.    fis'16    a'8.    b'16  
  cis''8.    e''16    e''8.    cis''16    \bar "|"   cis''4    b'4    b'4    
c''4    \bar "|"     cis''8.    d''16    e''8.    cis''16    a'2    \bar "|"   
a'8.    b'16    \tuplet 3/2 {   cis''8    b'8    a'8  }   fis'4    eis'4    
\bar "|"   e'8.    fis'16    a'8.    b'16    cis''8.    e''16   ~    e''8.    
cis''16    \bar "|"   b'4    a'4    a'4    \bar "|."   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
