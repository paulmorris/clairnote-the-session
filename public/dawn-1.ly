\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JD"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/167#setting167"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/167#setting167" {"https://thesession.org/tunes/167#setting167"}}}
	title = "Dawn, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   a4    cis'8    e'8    d'4    fis'8    a'8  \bar "|"   
e'4    cis'8    e'8    a'8    b'8    cis''8    a'8  \bar "|"   b'8    e'8  
\tuplet 3/2 {   e'8    e'8    e'8  }   d''8    e'8    cis''8    e'8  \bar "|"   
b'8    d''8    cis''8    b'8    a'8    fis'8    e'8    cis'8  \bar "|"     a4   
 cis'8    e'8    d'4    fis'8    d'8  \bar "|"   e'4    cis'8    e'8    a'8    
b'8    cis''8    e''8  \bar "|"   fis''8    e''8    fis''8    gis''8    a''8    
fis''8    e''8    d''8  \bar "|"   \tuplet 3/2 {   cis''8    e''8    cis''8  }   
b'8    e''8    cis''8    a'8    a'4  \bar ":|."   \tuplet 3/2 {   cis''8    e''8 
   cis''8  }   b'8    e''8    cis''8    a'8    a'8    gis''8  \bar "||"     
a''8    b''8    gis''8    a''8    fis''8    e''8    cis''8    e''8  \bar "|"   
fis''8    b''8    b''8    gis''8    a''8    gis''8    fis''8    a''8  \bar "|" 
\tuplet 3/2 {   gis''8    fis''8    e''8  }   b''8    e''8    \tuplet 3/2 {   
gis''8    fis''8    e''8  }   b''8    e''8  \bar "|"   a''8    cis'''8    e''8  
  cis'''8    a''8    e''8    fis''8    gis''8  \bar "|"     a''8    b''8    
gis''8    a''8    fis''8    e''8    cis''8    e''8  \bar "|"   d''8    cis''8   
 b'8    cis''8    d''8    e''8    fis''8    gis''8  \bar "|" \tuplet 3/2 {   
a''8    b''8    a''8  }   gis''8    b''8    a''8    fis''8    e''8    cis''8  
\bar "|"   d''8    cis''8    b'8    e''8    cis''8    a'8    a'8    gis''8  
\bar ":|."   d''8    cis''8    b'8    e''8    cis''8    a'8    a'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
