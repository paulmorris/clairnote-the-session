\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thistledowne"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/21#setting28097"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/21#setting28097" {"https://thesession.org/tunes/21#setting28097"}}}
	title = "Castle Kelly"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {     a'4 ^"Am"   c''8    a'8    
\tuplet 3/2 {   a'8    a'8    a'8  }   c''8    a'8    \bar "|"     g'8 ^"G"   
e'8    d'8    e'8    g'8    e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   
\bar "|"     a'8 ^"Am"   a'8    c''8    a'8    g'8    a'8    c''8    d''8    
\bar "|"     e''8 ^"G"   c''8    d''8    b'8      c''8 ^"Am"   a'8    a'8    
a'8    \bar "|"     \tuplet 3/2 {   a'8 ^"Am"   a'8    a'8  }   c''8    a'8    
a'8    b'8    c''8    a'8    \bar "|"     g'8 ^"G"   e'8    d'8    e'8    g'8   
 e'8    d'8    e'8    \bar "|"   \tuplet 3/2 {   a'8 ^"Am"   a'8    a'8  }   
c''8    a'8    g'8    a'8    c''8    d''8    \bar "|"     e''8 ^"G"   c''8    
d''8    b'8      c''8 ^"Am"   a'8    a'4    }     \repeat volta 2 {     a''8 
^"Am"   g''8    e''8    c''8    \tuplet 3/2 {   d''8 ^"G"   d''8    d''8  }   
e''8    d''8    \bar "|"     c''8 ^"Am"   a'8    g'8    e'8    \tuplet 3/2 {   
g'8 ^"Em"   g'8    g'8  }   e'8    g'8    \bar "|"     a''8 ^"Am"   g''8    
e''8    c''8    \tuplet 3/2 {   d''8 ^"G"   d''8    d''8  }   c''8    d''8    
\bar "|"     e''8 ^"Em"   g''8    a''8    g''8    \tuplet 3/2 {   a''8 ^"Am"   
a''8    a''8  }   d''8    g''8    \bar "|"       a''8 ^"Am"   g''8    e''8    
c''8    \tuplet 3/2 {   d''8 ^"G"   d''8    d''8  }   e''8    d''8    \bar "|"   
  c''8 ^"Am"   a'8    g'8    e'8    \tuplet 3/2 {   g'8 ^"G"   g'8    g'8  }   
e'8    g'8    \bar "|"   \tuplet 3/2 {   a'8 ^"Am"   a'8    a'8  }   c''8    a'8 
   g'8    a'8    c''8    d''8    \bar "|"     e''8 ^"G"   c''8    d''8    b'8   
   c''8 ^"Am"   a'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
