\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "zoronic"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/558#setting23999"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/558#setting23999" {"https://thesession.org/tunes/558#setting23999"}}}
	title = "Coen's Memories"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   fis'8  \bar "|"   e'8    d'8    
b8    a8    b8    e'8    fis'8    a'8  \bar "|"   b'4    e''8    b'8    d''8    
b'8    a'8    fis'8  \bar "|"   d'4. ^"~"    fis'8    e'4    d'8    b8  
\bar "|"   a8    b8    d'8    fis'8    e'8    d'8    b8    d'8  \bar "|"     
e'4. ^"~"    e'8    d'8    e'8    fis'8    a'8    \bar "|"   b'4    e''8    b'8 
   d''8    b'8    a'8    fis'8  \bar "|"   d'8    e'8    fis'8    a'8    a'8    
b'8    a'8    fis'8  \bar "|"   d'8    e'8    fis'8    d'8    e'4. ^"~"  }     
\repeat volta 2 {   a'8  \bar "|"   b'8    e''8    e''4 ^"~"    fis''8    e''8  
  d''8    b'8    \bar "|"   e''4 ^"~"    fis''8    e''8    d''8    b'8    a'8   
 fis'8  \bar "|"   a'4. ^"~"    b'8    d''4    fis''8    d''8  \bar "|"   e''8  
  d''8    e''8    fis''8    d''4    b'8    a'8  \bar "|"     b'8    e''8    
e''4 ^"~"    fis''8    e''8    d''8    b'8    \bar "|"   e''4 ^"~"    fis''8    
e''8    d''8    b'8    a'8    fis'8  \bar "|"   d'8    e'8    fis'8    a'8    
a'8    b'8    a'8    fis'8  \bar "|"   d'8    e'8    fis'8    d'8    e'4. ^"~"  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
