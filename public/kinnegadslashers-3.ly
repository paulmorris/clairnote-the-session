\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "FyfferGuy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1258#setting14567"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1258#setting14567" {"https://thesession.org/tunes/1258#setting14567"}}}
	title = "Kinnegad Slashers, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key d \major   \tuplet 3/2 {   a'16 (   b'16    cis''16  } \bar "|"   
d''8  -)   e''8    d''8    d''8    a'8    fis'8  \bar "|"   a'8    b'8    a'8   
 a'8    b'8    cis''8  \bar "|"   d''8    e''8    d''8    d''8    a'8    fis'8  
\bar "|"   e'8    e''8    e''8    e''8    fis''8    e''8  \bar "|"   d''8    
e''8    d''8    d''8    a'8    fis'8  \bar "|"   a'8    b'8    a'8    a'4    
g''8  \bar "|"   fis''8    e''8    d''8    d''8    cis''8    b'8  \bar "|"   
a'8    b'8    cis''8    d''4  } \repeat volta 2 {   a'8  \bar "|"   d''8    
fis''8    a''8    a''8    fis''8    d''8  \bar "|"   cis''8    e''8    e''8    
e''8    cis''8    a'8  \bar "|"   d''8    fis''8    a''8    a''8    fis''8    
d''8  \bar "|"   a'8    a''8    a''8    a''4    a'8  \bar "|"   d''8    fis''8  
  a''8    a''8    fis''8    d''8  \bar "|"   cis''8    e''8    e''8    e''8    
fis''8    g''8  \bar "|"   fis''8    e''8    d''8    d''8    cis''8    b'8  
\bar "|"   a'8    b'8    cis''8    d''4  } \repeat volta 2 {   \tuplet 3/2 {   
a'16 (   b'16    cis''16  } \bar "|"   d''8  -)   a'8    fis'8    d''8    a'8   
 fis'8  \bar "|"   a'8    b'8    a'8    a'8    b'8    cis''8  \bar "|"   d''8   
 a'8    fis'8    d''8    a'8    fis'8  \bar "|"   e'8    e''8    e''8    e''8   
 fis''8    e''8  \bar "|"   d''8    a'8    fis'8    d''8    a'8    fis'8  
\bar "|"   a'8    b'8    a'8    a'4    g''8  \bar "|"   fis''8    e''8    
fis''8    d''8    cis''8    b'8  \bar "|"   a'8    b'8    cis''8    d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
