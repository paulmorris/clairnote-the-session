\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "benhockenberry"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/802#setting13953"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/802#setting13953" {"https://thesession.org/tunes/802#setting13953"}}}
	title = "Sweetness Of Mary, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   \tuplet 3/2 {   e'8    a'8    b'8  } \bar "|"   c''4   
 \tuplet 3/2 {   c''8    b'8    a'8  }   fis'4    \tuplet 3/2 {   fis'8    a'8    
b'8  } \bar "|"   c''8.    e''16    d''16    fis''8.    e''4    \tuplet 3/2 {   
e''8    fis''8    g''8  } \bar "|"   a''8.    g''16    fis''8.    e''16    
e''8.    d''16    c''8.    d''16  \bar "|"   e''16    a''8.    c''8.    a'16    
b'4    a'8.    b'16  \bar "|"   c''4    \tuplet 3/2 {   c''8    b'8    a'8  }   
fis'4    \tuplet 3/2 {   fis'8    a'8    b'8  } \bar "|"   c''8.    e''16    
d''16    fis''8.    e''4    \tuplet 3/2 {   e''8    fis''8    g''8  } \bar "|"   
a''8.    g''16    fis''8.    e''16    e''8.    d''16    c''8.    d''16  
\bar "|"   e''16    a''8.    c''8.    b'16    a'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
