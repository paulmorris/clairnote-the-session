\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jardineromi"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/429#setting13291"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/429#setting13291" {"https://thesession.org/tunes/429#setting13291"}}}
	title = "Nine Points Of Roguery, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   a8    \bar "|"     d'4 ^"D"   fis'8    d'8    g'8    
fis'8    e'8    d'8    \bar "|"   a'8    d''8    a'8    fis'8    a'8    d''8    
a'8    fis'8    \bar "|" \grace {    d'8    e'8  }   d'4    fis'8    a'8      
g'8 ^"G"   fis'8    e'8    d''8    \bar "|"   c''8 ^"C"   e'8    \tuplet 3/2 {   
e'8    e'8    e'8  }   cis'8    e'8    e'8    fis'8    \bar "|"       d'8 ^"D"  
 e'8    fis'8    d'8    g'8    fis'8    e'8    d'8    \bar "|"   a'8    d''8    
a'8    fis'8    a'8    d''8    a'8    fis'8    \bar "|" \grace {    d'8    e'8  
}   d'4    fis'8    a'8      g'8 ^"G"   fis'8    e'8    cis''8    \bar "|"   
d''8    d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   d''8    d'8    d'8    }    
 \repeat volta 2 {   b'8    \bar "|"   \grace {    d''8 ^"C" }   cis'4    
cis''8    a'8    \grace {    d''8 ^"G/B" }   b'4    b'8    g'8    \bar "|"   
\grace {    a'8 ^"Am"   d''8  }   a'4    a'8    d''8      a'8 ^"G/B"   g'8    
fis'8    e'8    \bar "|"     \tuplet 3/2 {   d'8 ^"D"   d'8    d'8  }   fis'8    
a'8      g'8 ^"G"   fis'8    e'8    d''8    \bar "|"     c''8 ^"C"   e'8    
\tuplet 3/2 {   e'8    e'8    e'8  }     c''8 ^"Am"   e'8    e'8    d''8  
\bar "|"       c''8 ^"C"   e''8    c''8    a'8      b'8 ^"G/B"   d''8    b'8    
g'8    \bar "|"     a'8 ^"Am"   d''8    cis''8    d''8      fis''8 ^"G/B"   
d''8    a'8    fis'8    \bar "|"     \tuplet 3/2 {   d'8 ^"D"   e'8    d'8  }   
fis'8    a'8      g'8 ^"G"   fis'8    e'8    cis''8    \bar "|"   d''8 ^"D"   
d'8    \tuplet 3/2 {   d'8    d'8    d'8  }   d''8    d'8    d'8    }     
\repeat volta 2 {   cis''8  \bar "|"     d''4 ^"D"   fis''8    a''8      g''8 
^"C"   fis''8    e''8    cis''8    \bar "|"     d''8 ^"D"   e''8    fis''8    
g''8    a''8    b''8    a''8    fis''8    \bar "|"   d''4    fis''8    a''8     
 g''8 ^"C"   fis''8    g''8    a''8    \bar "|"     b''8 ^"Am"   e''8    
\tuplet 3/2 {   d''8    fis''8    d''8  }   b''8    e''8    e''8    fis''8  
\bar "|"       d''4 ^"D"   fis''8    a''8      g''8 ^"C"   fis''8    e''8    
cis''8    \bar "|"     d''8 ^"D"   e''8    fis''8    g''8    a''8    b''8    
a''8    fis''8    \bar "|"   d''4    fis''8    a''8      g''8 ^"C"   fis''8    
g''8    a''8    \bar "|"     fis''8 ^"Am"   a''8    g''8    e''8      fis''8 
^"D"   d''8    a'8  }     b'8    \bar "|"     c''8 ^"C"   e''8    d''8    a'8   
   b'8 ^"G/B"   d''8    b'8    g'8    \bar "|"     a'8 ^" Am"   d''8    cis''8  
  d''8      fis''8 ^"G/B"   d''8    a'8    fis'8    \bar "|"   \grace {    d'8 
^"D"   e'8  }   d'4    fis'8    a'8    g'8    fis'8    e'8    d''8    \bar "|"  
 c''8 ^"C"   e'8    \tuplet 3/2 {   e'8    e'8    e'8  }     c''8 ^"Am"   e'8    
e'8    d''8    \bar "|"       \tuplet 3/2 {   c''8 ^"C"   d''8    c''8  }   c''8 
   a''8      b'4 ^"G/B"   b'8    g'8    \bar "|"   \grace {    b'8 ^"Am" }   
a'4    a'8    d''8      a'8 ^"G/B"   g'8    fis'8    e'8    \bar "|"     
\tuplet 3/2 {   d'8 ^"D"   e'8    d'8  }   fis'8    a'8      g'8 ^"G"   fis'8    
e'8    cis''8    \bar "|"     d''8 ^"D"   d'8    \tuplet 3/2 {   d'8    d'8    
d'8  }   d''8    d'8    d'8    b'8    \bar "|"     <<   e'4 ^"C"   c''4   >>   
c''8    a'8    <<   d'4 ^"G/B"   b'4   >>   b'8    g'8    \bar "|"     a'4 
^"Am"   a'8    d''8      a'8 ^"G/B"   g'8    fis'8    e'8    \bar "|"   
\grace {    d'8 ^"D"   e'8  }   d'4    fis'8    a'8    g'8 ^"G"   fis'8    e'8  
  d''8    \bar "|"     c''8 ^"C"   e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   
  c''8 ^"Am"   e'8    e'8    d''8    \bar "|"       c''8 ^"C"   e''8    c''8    
a'8      b'8 ^"G/B"   d''8    b'8    g'8    \bar "|"     a'8 ^"Am"   d''8    
cis''8    d''8      fis''8 ^"D"   d''8    a'8    fis'8    \bar "|"   
\tuplet 3/2 {   d'8    e'8    d'8  }   fis'8    a'8      g'8 ^"G"   fis'8    e'8 
   cis''8    \bar "|"     d''8 ^"D"   d'8    \tuplet 3/2 {   d'8    d'8    d'8  
}   d''8    d'8    d'8    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
