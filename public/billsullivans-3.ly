\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/481#setting13382"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/481#setting13382" {"https://thesession.org/tunes/481#setting13382"}}}
	title = "Bill Sullivan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g''4    g''8.    e''16    
\bar "|"   d''8    b'8    g''4    \bar "|"   b'8    d''8    g''8    e''8    
\bar "|"   d''8    b'8    a'8    g'8    \bar "|"     g''16    a''16    g''16    
fis''16    g''8.    e''16    \bar "|"   d''8    b'8    g''4    \bar "|"   b'8   
 d''8    a'8.    c''16    \bar "|"   b'8    g'8    g'4    }     
\repeat volta 2 {   b'8    d''8    d''8    b'8    \bar "|"   c''8    e''8    
e''4    \bar "|"   b'8    d''8    d''8    b'8    \bar "|"   c''8    a'8    a'4  
  \bar "|"     b'8    d''8    d''16    c''16    b'8    \bar "|"   c''8    e''8  
  e''8.    c''16    \bar "|"   b'8    d''8    a'8.    c''16    \bar "|"   b'8   
 g'8    g'4    }     \repeat volta 2 {   g''4    g''8    e''8    \bar "|"   
d''8    b'8    g''16    a''16    g''16    e''16    \bar "|"   d''8    b'8    
g''8.    e''16    \bar "|"   d''8    b'8    a'8    g'8    \bar "|"     g''4    
g''16    a''16    g''16    e''16    \bar "|"   d''8    b'8    g''8    fis''16   
 e''16    \bar "|"   d''8    b'8    a'8.    b'16    \bar "|"   a'8    g'8    
g'4    }     \repeat volta 2 {   b'8    d''8    d''16    e''16    d''16    b'16 
   \bar "|"   c''8    e''8    e''16    d''16    c''8    \bar "|"   b'8    d''16 
   cis''16    d''16    b'16    g'8    \bar "|"   b'8    a'8    a'8    g'16    
a'16    \bar "|"     b'8    d''8    b'8    d''8    \bar "|"   c''8    e''8    
e''8.    c''16    \bar "|"   b'8    d''8    a'8.    b'16    \bar "|"   a'8    
g'8    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
