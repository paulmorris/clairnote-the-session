\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "swisspiper"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1365#setting14716"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1365#setting14716" {"https://thesession.org/tunes/1365#setting14716"}}}
	title = "Blackberry Blossom, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   g''8    e''8  \bar "|"   d''8 ^"G"   b'8    a'8    
c''8    b'8    g'8    g'8    a'8  \bar "|"   b'4 -.   b'8 -.   a'8    b'4    
g''8    e''8  \bar "|"   d''8    b'8    a'8    c''8    b'8    a'8    a'8 -.   
g'8 -. \bar "|"   e''8 ^"Am"   a''8    a''8    g''8    a''4    g''8    e''8  
\bar "|"     d''8 ^"G"   b'8    a'8    c''8    b'8    g'8    g'8    a'8  
\bar "|"   b'4    b'8    a'8    \tuplet 3/2 {   b'8    cis''8    d''8  }   g''8  
  e''8  \bar "|"   d''8    b'8    a'8    c''8    b'8    g'8    g'8    g'8  
\bar "|"   e''8 ^"Am"   a''8    a''8    a''8    a''8.    r16 e''8    fis''8  
\bar "||"     g''4 ^"G"^"~"    g''8    fis''8    g''8    a''8    b''8    a''8  
\bar "|"   g''4. ^"C"^"~"    b''8      a''8 ^"D7"   g''8    e''8    fis''8  
\bar "|"   g''4 ^"G"^"~"    g''8    fis''8    g''8    a''8    b''8    g''8  
\bar "|"   e''8 ^"Am"   a''8    a''8    a''8    a''8    g''8    e''8    fis''8  
\bar "|"     g''4 ^"G"^"~"    g''8    fis''8    g''8    a''8    b''8    a''8  
\bar "|"   g''4. ^"C"^"~"    b''8      a''4 ^"D7"   g''8    a''8  \bar "|"   
b''8 ^"G"   g''8    a''8    fis''8      g''8 ^"D7"   fis''8    e''8    d''8  
\bar "|"   e''8 ^"Am"   a''8    a''8    g''8    a''4    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
