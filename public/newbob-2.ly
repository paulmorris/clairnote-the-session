\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mr G. Cunningham"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/293#setting27291"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/293#setting27291" {"https://thesession.org/tunes/293#setting27291"}}}
	title = "New Bob, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key b \minor   b'8    cis''8    d''8    e''8    fis''4    e''8    
a''8  \bar "|"   cis''8    a'8    e''8    a'8    a''8    a'8    e''8    cis''8  
\bar "|"     b'8    cis''8    d''8    e''8    fis''4    e''8    a''8  \bar "|"  
 cis''8    a'8    e''8    cis''8    b'4    fis''4  }     \repeat volta 2 {   
b'4    b''8    ais''8    b''4    b''8    fis''8  \bar "|"   a''8    b''8    
a''8    fis''8    d''8    e''8    e''8    d''8  \bar "|"     e''8    fis''8    
a''8    b''8    fis''8    a''8    e''8    a''8  \bar "|"   cis''8    a'8    
e''8    cis''8    b'4    fis''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
