\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "b.maloney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/922#setting922"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/922#setting922" {"https://thesession.org/tunes/922#setting922"}}}
	title = "Banks, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \dorian   \repeat volta 2 {   \tuplet 3/2 {   bes8    c'8    
d'8  }   \bar "|"   ees'4    g'4    g'4    f'8    ees'8    \bar "|"   d'4    
f'4    f'4    ees'8    d'8    \bar "|"   ees'4    c''4    c''4    d''8    
ees''8    \bar "|"   a'8    bes'8    c''8    bes'8    aes'!8    g'8    f'8    
ees'8    \bar "|"     g'4    bes'4    bes'4    c''8    bes'8    \bar "|"   
aes'4    c''4    c''4    d''8    ees''8    \bar "|"   d'8    ees'8    f'8    
g'8    aes'8    f'8    d'8    f'8    \bar "|"   ees'4    g'4    ees'4    }     
\repeat volta 2 {   |
 g''4 ^"~"    \bar "|"   g''4    ees'4    ees'4    g''4 ^"~"    \bar "|"   f''4 
   d'4    d'4    g''4 ^"~"    \bar "|"   ees''4    c''4    c''4    f''8    
ees''8    \bar "|"   d''8    c''8    bes'8    a'8    bes'4    bes'4    \bar "|" 
    d'8    bes'8    f''8    bes'8    d'8    bes'8    f''8    bes'8    \bar "|"  
 ees'8    bes'8    g''8    bes'8    ees'8    bes'8    g''8    bes'8    \bar "|" 
  a'8    bes'8    c''8    d''8    ees''8    c''8    a'8    c''8    \bar "|"   
bes'8    a'8    bes'8    c''8    bes'8    aes'!8    g'8    f'8    \bar "|"     
ees'8    g'8    bes'8    g'8    ees''8    bes'8    f'8    ees'8    \bar "|"   
d'8    f'8    bes'8    f'8    d''8    f'8    ees'8    d'8    \bar "|"   c'8    
ees'8    g'8    ees'8    c''8    bes'8    aes'8    g'8    \bar "|"   f'8    g'8 
   f'8    ees'8    d'8    c'8    bes8    aes8    \bar "|"     g8    ees'8    
bes'8    ees'8    g8    ees'8    bes'8    ees'8    \bar "|"   aes8    ees'8    
c''8    ees'8    aes8    ees'8    c''8    ees'8    \bar "|"   d'8    ees'8    
f'8    g'8    aes'8    f'8    d'8    f'8    \bar "|"   ees'4    g'4    ees'4    
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
