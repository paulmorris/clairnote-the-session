\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Conán McDonnell"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/930#setting930"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/930#setting930" {"https://thesession.org/tunes/930#setting930"}}}
	title = "Argosy, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \minor   \repeat volta 2 {   b'8 ^"Em"   e''8    e''8    d''8  
    e''8 ^"Em"   d''8    b'8    a'8    \bar "|"     fis'8 ^"D"   a'8    d'8    
a'8      fis'8 ^"D"   a'8    d''8    a'8    \bar "|"     e'8 ^"Em"   e''8    
e''8    d''8      e''8 ^"Em"   d''8    b'8    a'8    \bar "|"     d'8 ^"D"   
e'8    fis'8    a'8      fis'8 ^"Em"   e'8    e'16    e'16    e'8  \bar "|"     
\bar "|"   b'8 ^"Em"   e''8    e''8    d''8      e''8 ^"Em"   d''8    b'8    
a'8    \bar "|"     fis'8 ^"D"   a'8    d''8    e''8      fis''8 ^"D"   d''8    
e''8    fis''8    \bar "|"     g''16 ^"Em"   g''16    g''8    g''8    fis''8    
  e''8 ^"Em"   d''8    b'8    a'8    \bar "|"     fis'8 ^"D"   a'8    d'8    
e'8      fis'8 ^"Em"   e'8    e'16    e'16    e'8    }     \repeat volta 2 {   
e''16 ^"Em"   e''16    e''8    g'8    e''8      a'8 ^"A"   e''8    cis''8    
e''8    \bar "|"     d''8 ^"D"   e''8    fis''8    g''8      a''8 ^"D"   d''8   
 fis''8    d''8    \bar "|"     e''16 ^"Em"   e''16    e''8    g'8    e''8      
b'8 ^"Em"   e''8    a'8    e''8    \bar "|"   fis'8 ^"D"   d''8    d''8    
cis''8      d''8 ^"D"   e''8    fis''8    d''8    \bar "|"     \bar "|"     
e''16 ^"Em"   e''16    e''8    g'8    e''8      b'8 ^"Em"   e''8    cis''8    
e''8  \bar "|"     d''8 ^"D"   e''8    fis''8    g''8      a''8 ^"D"   d''8    
fis''8    a''8    \bar "|"   g''8 ^"Em"   b'8    e''8    g''8      fis''8 ^"Bm" 
  e''8    d''8    b'8    \bar "|"   a'8 ^"D"   b'8    d''8    e''8      fis''8 
^"D"   a''8    fis''8    d''8    \bar "|"     \bar "|"   e''16 ^"Em"   e''16    
e''8    g'8    e''8      a'8 ^"A"   e''8    cis''8    e''8    \bar "|"     d''8 
^"D"   e''8    fis''8    g''8      a''8 ^"D"   d''8    fis''8    d''8    
\bar "|"     e''16 ^"Em"   e''16    e''8    g'8    e''8      b'8 ^"Em"   e''8   
 a'8    e''8    \bar "|"   fis'8 ^"D"   d''8    d''8    cis''8      d''8 ^"D"   
e''8    fis''8    d''8    \bar "|"     \bar "|"     e''16 ^"Em"   e''16    e''8 
   g'8    e''8      b'8 ^"Em"   e''8    cis''8    e''8  \bar "|"     d''8 ^"D"  
 e''8    fis''8    g''8      a''8 ^"D"   d''8    fis''8    a''8    \bar "|"   
g''8 ^"Em"   b'8    e''8    g''8      fis''8 ^"Bm"   e''8    d''8    b'8    
\bar "|"   a'8 ^"D"   fis'8    d'8    fis'8      e'8 ^"Em"   fis'8    g'8    
a'8    \bar "||"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
