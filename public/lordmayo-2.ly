\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jean2"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/638#setting13667"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/638#setting13667" {"https://thesession.org/tunes/638#setting13667"}}}
	title = "Lord Mayo"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \dorian   \repeat volta 2 {   d'4    c'4    \bar "|"   d'4    
e'16    g'16    e'8    \bar "|"   g'8    a'8    a'8    b'8    \bar "|"   a'8    
g'8    e'16    f'16    g'8    \bar "|"   a'4    d'4    \bar "|"   d'4    e'8    
d'8    \bar "|"   c'8    d'8    c'8    b8    \bar "|"   a4    a4    \bar "|"   
d'4    c'4    \bar "|"     d'4    e'4    \bar "|"   g'8    a'8    a'8    b'8    
\bar "|"   c''4.    b'8    \bar "|"   g'8    a'8    a'8    b'8    \bar "|"   
a'8    g'8    e'16    d'16    c'8    \bar "|"   d'2    } \alternative{{   r4   
a4    } {   r4   a'4    \bar "|"   d''4    d''4    \bar "|"   c''8    a'8    
c''8    d''8    \bar "|"     e''8    r8   e''4    \bar "|"   r8 d''8    c''4    
\bar "|"   a'16    c''16    a'8    g'4    \bar "|"   g'8    a'8    c''4    
\bar "|"   c'4.    d'8    \bar "|"   c'4    a4    \bar "|"   d''4    d''4    
\bar "|"   c''8    a'8    c''8    d''8    \bar "|"   e''8    r8   e''4    
\bar "|"   r8 d''8    c''4    \bar "|"     a'16    c''16    a'8    g'4    
\bar "|"   g'8    a'8    c''4    \bar "|"   d''4.    e''8    \bar "|"   d''4    
a'16    b'16    c''8    \bar "|"   d''4    d''4    \bar "|"   c''8    a'8    
c''8    d''8    \bar "|"   e''8    r8   e''4    \bar "|"   r8 d''8    c''4    
\bar "|"   a'4    g'4    \bar "|"   g'8    a'8    c''4    \bar "|"     c'4.    
d'8    \bar "|"   c'4    a4    \bar "|"   d'8    c'8    d'8    e'8    \bar "|"  
 g'4    e'4    \bar "|"   d'4    c'4    \bar "|"   c''4.    a'8    \bar "|"   
g'4.    d'8    \bar "|"   e'16    f'16    g'8    e'8    c'8    \bar "|"   d'4   
 d'4    \bar "|"   d'4    a4    \bar "|"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
