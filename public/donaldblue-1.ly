\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1175#setting1175"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1175#setting1175" {"https://thesession.org/tunes/1175#setting1175"}}}
	title = "Donald Blue"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d''8    b'8    a'8    fis'8    
a'8    fis'8    a'8    b'8  \bar "|"   d''8    b'8    a'8    fis'8    e'4    
d'4  \bar "|"   d''8    b'8    a'8    fis'8    a'8    fis'8    a'8    d''8  
\bar "|"   b'8    b'4    a'8    b'4    e''4  }     \repeat volta 2 {   a'8    
b'8    d''8    e''8    fis''8    a''8    fis''8    d''8  \bar "|"   fis''8    
a''8    fis''8    d''8    e''8    d''8    b'4  \bar "|"   a'8    b'8    d''8    
e''8    fis''8    a''8    fis''8    d''8  \bar "|"   b'8    b'4    a'8    b'4   
 d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
