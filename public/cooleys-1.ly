\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/1052#setting1052"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1052#setting1052" {"https://thesession.org/tunes/1052#setting1052"}}}
	title = "Cooley's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key g \major   \tuplet 3/2 {   d''8    e''8    fis''8  } \bar "|"   
g''8    g'8  \grace {    a'8  }   \tuplet 3/2 {   g'8    fis'8    g'8  }   
\tuplet 3/2 {   e'8    fis'8    g'8  }   d'8    c'8  \bar "|"   b8    g8    b8   
 d'8    g'8    b'8    d''8    b'8  \bar "|" \tuplet 3/2 {   c''8    d''8    c''8 
 }   a'8    c''8    \tuplet 3/2 {   b'8    c''8    b'8  }   g'8    b'8  \bar "|" 
  c''8    b'8    a'8    g'8    a'4    \tuplet 3/2 {   d''8    e''8    fis''8  } 
\bar "|"     g''8    g'8  \grace {    a'8  }   \tuplet 3/2 {   g'8    fis'8    
g'8  }   \tuplet 3/2 {   e'8    fis'8    g'8  }   d'8    c'8  \bar "|"   b8    
g8    b8    d'8    g'8    b'8    d''8    b'8  \bar "|"   c''8    b'8    a'8    
g'8    fis'8    e''8    d''8    c''8  \bar "|" \grace {    b'8    c''8  }   b'4 
   g'4    g'4  }     \repeat volta 2 { \tuplet 3/2 {   d''8    d''8    c''8  } 
\bar "|"   b'8    g'8    b'8    d''8    e''8    d''8    b'8    d''8  \bar "|"   
g''4  \grace {    a''8  }   \tuplet 3/2 {   g''8    fis''8    g''8  }   e''8    
g''8    d''8    b'8  \bar "|"   d''8    g''8  \grace {    a''8  }   
\tuplet 3/2 {   g''8    fis''8    g''8  }   d''8    b'8    g'8    b'8  \bar "|"  
 c''8    b'8    a'8    g'8    fis'8    e''8    d''8    c''8  \bar "|"     b'8   
 g'8  \grace {    a'8  }   \tuplet 3/2 {   g'8    fis'8    g'8  } \grace {    
fis'8  }   \tuplet 3/2 {   e'8    d'8    e'8  }   c''8    e'8  \bar "|" 
\grace {    fis'8  }   \tuplet 3/2 {   e'8    d'8    e'8  }   c''8    e'8    d'8 
   g'8    b'8    d''8  \bar "|"   g''8    a''8    g''8    e''8    d''8    c''8  
  b'8    a'8  \bar "|" \grace {    b'8    c''8  }   b'4    g'4    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
