\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jeremy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/61#setting61"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/61#setting61" {"https://thesession.org/tunes/61#setting61"}}}
	title = "Langstrom's Pony"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian   \repeat volta 2 {   fis''8    e''8    d''8    
cis''8    a'8    a'8  \bar "|"   e'8    a'8    a'8    cis''8    a'8    a'8  
\bar "|"   fis''8    e''8    d''8    cis''8    a'8    a'8  \bar "|"   b'8    
g'8    b'8    d''8    cis''8    b'8  \bar "|"   fis''8    e''8    d''8    
cis''8    a'8    a'8  \bar "|"   e'8    a'8    a'8    cis''8    a'8    a'8  
\bar "|"   fis''8    a''8    fis''8    g''8    fis''8    e''8  \bar "|"   d''8  
  b'8    g'8    b'8    cis''8    d''8  }   \repeat volta 2 {   cis''8    e''8   
 e''8    d''8    fis''8    fis''8  \bar "|"   cis''8    e''8    e''8    e''8    
cis''8    a'8  \bar "|"   cis''8    e''8    e''8    g''4    e''8  \bar "|"   
d''8    b'8    g'8    b'8    cis''8    d''8  \bar "|"   cis''8    e''8    e''8  
  d''8    fis''8    fis''8  \bar "|"   cis''8    e''8    e''8    e''8    fis''8 
   g''8  \bar "|"   fis''8    a''8    fis''8    g''8    fis''8    e''8  
\bar "|"   d''8    b'8    g'8    b'8    cis''8    d''8  }   \repeat volta 2 {   
a'8    cis''8    e''8    a''4    fis''8  \bar "|"   e''8    cis''8    a'8    
e''8    cis''8    a'8  \bar "|"   g'8    b'8    d''8    g''4    e''8  \bar "|"  
 d''8    b'8    g'8    b'8    cis''8    d''8  \bar "|"   e''8    a''8    fis''8 
   g''4    e''8  \bar "|"   fis''8    e''8    d''8    e''8    cis''8    a'8  
\bar "|"   a''4    fis''8    g''8    fis''8    e''8  \bar "|"   d''8    b'8    
g'8    b'8    cis''8    d''8  }   \repeat volta 2 {   a'8    e'8    a'8    a'4  
  d''8  \bar "|"   cis''8    a'8    cis''8    e''8    cis''8    a'8  \bar "|"   
a'8    e'8    a'8    a'8    b'8    c''8  \bar "|"   b'8    g'8    b'8    d''8   
 cis''8    b'8  \bar "|"   a'8    e'8    a'8    a'4    d''8  \bar "|"   cis''8  
  a'8    cis''8    e''8    fis''8    g''8  \bar "|"   fis''8    a''8    fis''8  
  g''8    fis''8    e''8  \bar "|"   d''8    b'8    g'8    b'8    cis''8    
d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
