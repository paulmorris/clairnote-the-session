\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thady Quill"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/633#setting29903"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/633#setting29903" {"https://thesession.org/tunes/633#setting29903"}}}
	title = "Saint Ruth's Bush"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \dorian   \repeat volta 2 {   b'8    d''8    \bar "|"   e''8   
 a'8    a'8    b'8    \tuplet 3/2 {   c''8    b'8    c''8  }   b'8    a'8    
\bar "|"   g'8    e'8    e'8    d'8    e'8    d'8    e'8    g'8    \bar "|"   
a'8    gis'8    a'8    b'8    c''8    b'8    c''8    d''8    \bar "|"   e''8    
a''8    a''8    g''8    e''8    g''8    d''8    g''8    \bar "|"     e''8    
a'8    a'8    b'8    \tuplet 3/2 {   c''8    b'8    c''8  }   b'8    a'8    
\bar "|"   g'8    e'8    e'8    d'8    e'8    d'8    e'8    g'8    \bar "|"   
a'8    gis'8    a'8    b'8    c''8    d''8    \tuplet 3/2 {   e''8    fis''8    
g''8  }   \bar "|"   d''8    b'8    g'8    b'8    a'4    }     
\repeat volta 2 {   a''8    \bar "|"   e''8    \tuplet 3/2 {   a''8    b''8    
a''8  }   g''8    a''8    b''8    a''8    g''8    \bar "|"   e''8    
\tuplet 3/2 {   a''8    b''8    a''8  }   g''8    b''8    g''8    a''8    g''8   
 \bar "|"   e''8    g''8    g''8    fis''8    g''8    a''8    b''8    g''8    
\bar "|"   e''8    g''8    \tuplet 3/2 {   g''8    fis''8    g''8  }   e''8    
d''8    b'8    d''8    \bar "|"     e''8    a''8    a''8    g''8    a''8    
b''8    a''8    g''8    \bar "|"   e''8    a''8    a''8    g''8    b''8    g''8 
   a''8    g''8    \bar "|"   e''8    d''8    b'8    d''8    g''8    fis''8    
g''8    e''8    \bar "|"   d''8    b'8    g'8    b'8    a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
