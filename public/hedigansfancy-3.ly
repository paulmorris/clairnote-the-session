\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "sebastian the m3g4p0p"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/678#setting20697"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/678#setting20697" {"https://thesession.org/tunes/678#setting20697"}}}
	title = "Hedigan's Fancy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   a'4    d''8    fis'8    d'8    
fis'8    fis'8    d'8    fis'8  \bar "|"   a'4    d''8    fis'8    d'8    fis'8 
   g'4    e'8  \bar "|"   a'4    d''8    fis'8    d'8    fis'8    fis'8    d'8  
  fis'8  \bar "|"   b'8    a'8    b'8    e'4    fis'8    g'4    e'8  }     
\repeat volta 2 {   d'4    d''8    a'8    g'8    fis'8    a'8    g'8    fis'8  
\bar "|"   d'4    d''8    a'8    g'8    fis'8    g'4    e'8  \bar "|"   d'4    
d''8    a'8    g'8    fis'8    a'8    g'8    fis'8  \bar "|"   b'8    a'8    
b'8    e'4    fis'8    g'4    e'8  }     \repeat volta 2 {   d''4    d''8    
fis''8    e''8    d''8    e''8    cis''8    a'8  \bar "|"   d''4    g''8    
fis''8    d''8    fis''8    g''4    e''8  \bar "|"   d''8    fis''8    e''8    
fis''8    d''8    b'8    a'8    g'8    fis'8  \bar "|"   b'8    a'8    b'8    
e'4    fis'8    g'4    e'8  }     \repeat volta 2 {   d''4    fis''8    a''8    
g''8    fis''8    a''8    g''8    fis''8  \bar "|"   d''4    fis''8    a''8    
g''8    fis''8    g''4    e''8  \bar "|"   d''4    fis''8    a''8    g''8    
fis''8    a''8    g''8    fis''8  \bar "|"   b''8    g''8    fis''16    g''16   
 e''4    fis''8    g''4    e''8  }     \repeat volta 2 {   d''8    fis''8    
e''16    cis''16    d''8    a'8    fis'8    d''8    a'8    fis'8  \bar "|"   
d''8    fis''8    e''16    cis''16    d''8    a'8    fis'8    g'8    fis'8    
e'8  \bar "|"   d''8    fis''8    e''16    cis''16    d''8    a'8    fis'8    
d''8    a'8    fis'8  \bar "|"   b'8    a'8    b'8    e'4    fis'8    g'8    
fis'8    e'8  }     \repeat volta 2 {   d'8    fis'8    fis'8    d''8    fis'8  
  fis'8    a'8    fis'8    fis'8  \bar "|"   d'8    fis'8    fis'8    d''8    
fis'8    fis'8    g'8    fis'8    e'8  \bar "|"   d'8    fis'8    fis'8    d''8 
   fis'8    fis'8    a'8    fis'8    fis'8  \bar "|"   b'8    a'8    b'8    e'4 
   fis'8    g'8    fis'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
