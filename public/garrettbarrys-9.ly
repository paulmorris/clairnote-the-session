\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/544#setting25988"
	arranger = "Setting 9"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/544#setting25988" {"https://thesession.org/tunes/544#setting25988"}}}
	title = "Garrett Barry's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   a'8  \repeat volta 2 {   d'8    e'8    fis'8    g'4.  
\bar "|"   a'8    g'8    e'8    c''4    a'8  \bar "|"   d''8    c''8    a'8    
d''4    e''8  \bar "|"   g''16    fis''16    e''8    d''8    c''8    a'8    g'8 
 \bar "|"     fis'4.    g'8    c''16    a'16    g'8  \bar "|"   a'8    g'8    
e'8    b'16    c''16    d''8    e''8  \bar "|"   d''8    c''8    a'8    g'8    
e'8    a'8  \bar "|"   d'4.    d'4    a'8  }     \repeat volta 2 {   d''8    
c''8    a'8    d''4    e''8  \bar "|"   g''16    fis''16    e''8    d''8    
e''16    fis''16    g''8    e''8  \bar "|"   d''8    c''8    a'8    c''4    
d''8  \bar "|"   e''16    fis''16    g''8    d''8    e''8    c''8    a'8  
\bar "|"     d''8    c''8    a'8    d''4    e''8  \bar "|"   g''16    fis''16   
 e''8    d''8    e''16    fis''16    g''8    e''8  \bar "|"   d''8    c''8    
a'8    g'8    e'8    a'8  \bar "|"   d'4.    d'4    a'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
