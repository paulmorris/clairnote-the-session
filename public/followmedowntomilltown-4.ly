\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/252#setting12977"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/252#setting12977" {"https://thesession.org/tunes/252#setting12977"}}}
	title = "Follow Me Down To Milltown"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'4    fis'8.    a'16    \tuplet 3/2 {   a'8    b'8    
a'8  }   fis'8.    a'16    \bar "|"   b'4    g'8.    b'16    \tuplet 3/2 {   b'8 
   cis''8    b'8  }   d''8.    b'16    \bar "|"   \tuplet 3/2 {   a'8    b'8    
a'8  }   fis'8.    a'16    \tuplet 3/2 {   a'8    b'8    a'8  }   fis'8.    d'16 
   \bar "|"   e'8.    fis'16    g'8.    a'16    b'8.    cis''16    d''8.    
b'16    \bar "|"     \tuplet 3/2 {   a'8    b'8    a'8  }   \tuplet 3/2 {   fis'8 
   g'8    a'8  }   a'4    fis'8.    a'16    \bar "|"   \tuplet 3/2 {   b'8    
cis''8    b'8  }   \tuplet 3/2 {   g'8    a'8    b'8  }   b'4    d''8.    b'16   
 \bar "|"   a'8.    d''16    cis''8.    b'16    \tuplet 3/2 {   a'8    b'8    
a'8  }   \tuplet 3/2 {   g'8    fis'8    e'8  }   \bar "|"   \tuplet 3/2 {   d'8  
  e'8    d'8  }   cis'8.    e'16    d'2    \bar "||"     a'8.    d''16    
\tuplet 3/2 {   fis''8    e''8    d''8  }   a'8.    d''16    fis''4    \bar "|"  
 a'8.    cis''16    \tuplet 3/2 {   e''8    d''8    cis''8  }   a'8.    cis''16  
  e''4    \bar "|"   a'8.    d''16    fis''8.    d''16    a'8.    d''16    
fis''8.    d''16    \bar "|"   e''8.    d''16    \tuplet 3/2 {   cis''8    d''8  
  e''8  }   d''8.    cis''16    \tuplet 3/2 {   d''8    cis''8    b'8  }   
\bar "|"     a'8.    d''16    fis''8.    d''16    a''8.    d''16    fis''8.    
d''16    \bar "|"   a'8.    cis''16    e''8.    cis''16    a''8.    cis''16    
e''8.    cis''16    \bar "|"   \tuplet 3/2 {   d''8    e''8    d''8  }   cis''8. 
   b'16    \tuplet 3/2 {   a'8    b'8    a'8  }   \tuplet 3/2 {   g'8    fis'8    
e'8  }   \bar "|"   \tuplet 3/2 {   d'8    e'8    d'8  }   cis'8.    e'16    d'2 
   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
