\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jomac"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/276#setting276"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/276#setting276" {"https://thesession.org/tunes/276#setting276"}}}
	title = "Another Jig Will Do"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \major   \repeat volta 2 {   a'8    b'8    a'8    a'4    g'8   
 fis'4    g'8    \bar "|"   a'8    b'8    a'8    a'8    g'8    fis'8    g'4. 
^"~"    \bar "|"   a'8    b'8    a'8    a'4    g'8    fis'4    g'8    \bar "|"  
 a'4    d''8    d''4    cis''8    d''4.    }     \repeat volta 2 {   a'4    
g''8    fis''4    d''8    e''4    cis''8    \bar "|"   a'4    b'8    c''4    
d''8    c''4    c''8    \bar "|"   a'4    g''8    fis''4    d''8    e''4    
cis''8    \bar "|"   a'4    d''8    d''4    cis''8    d''4.    \bar "|"     a'4 
   g''8    fis''4    d''8    e''4    cis''8    \bar "|"   a'4    b'8    c''4    
d''8    c''4    cis''!8    \bar "|"   d''4    a'8    a'4    g'8    fis'4    g'8 
   \bar "|"   a'4    d''8    d''4    cis''8    d''4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
