\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mikea"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/513#setting1889"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/513#setting1889" {"https://thesession.org/tunes/513#setting1889"}}}
	title = "Good Morning To Your Nightcap"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \minor   \bar "|"   e''2    d''2  \bar "|"   c''8    b'8    
c''8    d''8  \grace {    f''8  }   e''4    d''4  \bar "|"   e''8    fis''8    
e''8    cis''8    d''8    cis''!8    d''8    b'8  \bar "|" \grace {    d''8  }  
 b'8    a'8    b'8    g'8    e'8    dis'8    d'!4  \bar "|"     e''8    fis''8  
  e''8    cis''8    d''8    cis''8    d''8    b'8  \bar "|"   c''8    b'8    
c''8    d''8  \grace {    fis''8  }   e''4    d''4  \bar "|" \tuplet 3/2 {   
e''8    fis''8    g''8  }   fis''8    a''8    g''8    e''8    d''8    b'8  
\bar "|" \tuplet 3/2 {   c''8    b'8    a'8  }   b'8    g'8    e'8    g'8    d'8 
   g'8  \bar ":|." \tuplet 3/2 {   c''8    b'8    a'8  }   b'8    g'8    e'8    
g'8    a'8    g'8  \bar "||"     \bar ".|:"   e'8    a'8    a'8    c''8    b'8   
 g'8    g'8    a'8  \bar "|" \grace {    d''8  }   b'8    a'8    b'8    d''8    
\tuplet 3/2 {   e''8    fis''8    e''8  }   d''4  \bar "|"   d'8    e'8    a'8   
 a'8  \grace {    c''8  }   b'8    g'8    g'8    a'8  \bar "|" \tuplet 3/2 {   
c''8    b'8    a'8  }   b'8    g'8    e'8    g'8    d'8    g'8  \bar "|"     
e'8    a'8    a'8    c''8    b'8    g'8    g'8    a'8  \bar "|" \grace {    
d''8  }   b'8    a'8    b'8    d''8    \tuplet 3/2 {   e''8    fis''8    e''8  } 
  d''4  \bar "|" \tuplet 3/2 {   e''8    fis''8    g''8  }   fis''8    a''8    
g''8    e''8    d''8    b'8  \bar "|" \tuplet 3/2 {   c''8    b'8    a'8  }   
b'8    g'8    e'8    g'8    d'8    g'8  \bar ":|." \tuplet 3/2 {   c''8    b'8   
 a'8  }   b'8    g'8    e'8    g'8    d'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
