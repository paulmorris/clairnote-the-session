\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "David50"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Barndance"
	source = "https://thesession.org/tunes/1441#setting14826"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1441#setting14826" {"https://thesession.org/tunes/1441#setting14826"}}}
	title = "Dawning Of The Day, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \mixolydian   cis''4    cis''4    cis''4    b'8    cis''8  
\bar "|"   e''4    e''4    fis''4    e''8    cis''8  \bar "|"   a'4    cis''8   
 b'8    a'4    b'4  \bar "|"   a'2.    e''4  \bar "|"   fis''4.    e''8    
fis''4    a''4    \bar "|"   cis''4.    b'8    a'4    cis''8    d''8    
\bar "|"   e''4    cis''4    a''4    cis''4  \bar "|"   b'2.    cis''8    e''8  
\bar "|"   fis''4.    e''8    fis''4    a''4    \bar "|"   cis''4.    b'8    
a'4    cis''8    d''8    \bar "|"   e''4    cis''4    a''4    cis''4  \bar "|"  
 b'2.    a'8    b'8  \bar "|"   cis''4    cis''4    cis''4    b'8    cis''8  
\bar "|"   e''4    e''4    fis''4    e''8    cis''8  \bar "|"   a'4    cis''8   
 b'8    a'4    b'4  \bar "|"   a'2.  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
