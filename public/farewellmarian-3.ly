\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/1427#setting21165"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1427#setting21165" {"https://thesession.org/tunes/1427#setting21165"}}}
	title = "Farewell Marian"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key f \major   \repeat volta 2 {   a'8.    d''16    a'8    b'8    
cis''8    d''8    \bar "|"   e''4    a'4    a'4    \bar "|"   f''8.    g''16    
f''8    e''8    d''8    e''8    \bar "|"   cis''4    a'4    a'4    \bar "|"     
a'8    d''8    a'8    g'8    a'8    f'8    \bar "|"   bes'8    d''8    bes'8    
a'8    bes'8    g'8    \bar "|"   f'8    a'8    f'4    e'8    f'16    e'16    
\bar "|"   d'2.    }     a'4    c''4   ~    c''8    a'8    \bar "|"   g'2.    
\bar "|"   a'4    c''4    d''4    \bar "|"   e''4    a'4    a'4    \bar "|"     
f''4    g''4   ~    g''8    f''8    \bar "|"   f''8    g''16    f''16    e''4   
 d''4    \bar "|"   c''8    e''8    c''4    b'8    c''16    b'16    \bar "|"   
a'2.    \bar "|"     a'8.    d''16    a'8    b'8    cis''8    d''8    \bar "|"  
 e''4    a'4    a'4    \bar "|"   f''8.    g''16    f''8    e''8    d''8    
e''8    \bar "|"   cis''4    a'4    a'4    \bar "|"     a'8    d''8    a'8    
g'8    a'4    \bar "|"   bes'8    d''8    bes'8    a'8    bes'4    \bar "|"   
f'8    a'8    f'4    e'4    \bar "|"   d'2.    \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
