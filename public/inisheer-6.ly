\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Daev"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/211#setting22687"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/211#setting22687" {"https://thesession.org/tunes/211#setting22687"}}}
	title = "Inisheer"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \mixolydian   d'4  \repeat volta 2 {   b'4.    a'8    b'16    
d''8.    \bar "|"   b'4.    a'8    b'16    d''8.    \bar "|"   e'4.    b'8    
a'8    b'8    \bar "|"   d'4.    b'8    a'16    g'8.    \bar "|"     b'4.    
a'8    b'16    d''8.    \bar "|"   b'4.    a'8    b'16    d''8.    \bar "|"   
g'4.    b'8    a'16    g'16    fis'8  } \alternative{{   g'2    d'4  } {   g'4. 
   a'8    b'8    d''8  \bar "||"     e''4.    fis''8    e''16    d''8.    
\bar "|"   b'4.    a'8    b'8    d''8    \bar "|"   e''8    fis''8    e''8    
d''8    b'16    c''16    d''8    \bar "|"     e''2    b'8    d''8    \bar "|"   
e''4.    fis''8    e''16    d''8.    \bar "|"   b'4.    a'8    b'16    d''8.    
\bar "|"   d'4.    b'8    a'16    g'16    fis'8  \bar "|"     g'4.    a'8    
b'8    d''8    \bar "|"   e''4.    fis''8    e''16    d''8.    \bar "|"   b'4.  
  a'8    b'8    d''8    \bar "|"   g''8    fis''8    e''8    d''8    b'16    
c''16    d''8    \bar "|"     e''2    b'8    d''8    \bar "|"   e''4.    fis''8 
   e''16    d''8.    \bar "|"   b'4.    a'8    b'16    d''8.    \bar "|"   d'4. 
   b'8    a'16    g'16    fis'8  \bar "|"   g'2    \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
