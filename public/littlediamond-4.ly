\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/604#setting13621"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/604#setting13621" {"https://thesession.org/tunes/604#setting13621"}}}
	title = "Little Diamond, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key d \major   a'8.    b'16    a'8    fis'8  \bar "|"   d'8    fis'8 
   a'8    d''8  \bar "|"   a'8.    b'16    a'8    fis'8  \bar "|"   g'8    
fis'8    e'8    fis'16    e'16  \bar "|"   d'8    e'8    fis'8    g'8  \bar "|" 
  a'8    b'8    cis''8    d''8  \bar "|"   e''8.    d''16    cis''8    e''8  
\bar "|"   d''4    d''8    b'8  \bar ":|."   d''4    d''8    e''8  \bar "||"   
fis''8    d''8    g''8    e''8  \bar "|"   fis''8    d''8    cis''8    b'8  
\bar "|"   a'8    d''8    cis''8    d''8  \bar "|"   fis''8    e''8    e''8    
d''16    e''16  \bar "|"   fis''8    d''8    g''8    e''8  \bar "|"   fis''8    
d''8    cis''8    b'8  \bar "|"   a'8    g''8    e''8    cis''8  \bar "|"   
d''4    d''8.    e''16  \bar "|"   fis''8    a''8    g''8    e''8  \bar "|"   
fis''8    d''8    cis''8    b'8  \bar "|"   a'8    d''8    cis''8    d''8  
\bar "|"   fis''8    e''8    e''8    d''16    e''16  \bar "|"   fis''8    d''8  
  g''8    e''8  \bar "|"   fis''8    d''8    cis''8    b'8  \bar "|"   a'8    
g''8    e''8    cis''8  \bar "|"   d''4    d''8    b'8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
