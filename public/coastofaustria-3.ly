\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/692#setting28909"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/692#setting28909" {"https://thesession.org/tunes/692#setting28909"}}}
	title = "Coast Of Austria, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   e'8    \repeat volta 2 {   fis'8    a'8    a'4    b'8 
   a'8    fis'8    a'8    \bar "|"   b'4.    d''8    a'8    d'8    fis'8    d'8 
   \bar "|"   fis'8    a'8    a'4    b'8    a'8    fis'8    a'8    \bar "|"   
b'8    d''8    d''8    cis''8    d''4    d''8    e''8    \bar "|"     fis''8    
e''8    d''8    fis''8    e''8    d''8    b'8    d''8  \bar "|"   b'4.    a'8   
 b'4    d''8    b'8  \bar "|"   a'8    d'8    fis'8    a'8    b'4.    d''8  
\bar "|"   a'8    fis'8    e'8    fis'8    d'4    d'8    e'8  }     
\repeat volta 2 {   fis'4    e'8    fis'8    d'8    b'8    b'4    \bar "|"   
a'8    fis'8    a'8    b'8    d'4    d'8    e'8  \bar "|"   fis'8    a'8    a'4 
   b'8    a'8    fis'8    a'8    \bar "|"   b'8    d''8    d''8    cis''8    
d''4    d''8    e''8    \bar "|"     fis''8    e''8    d''8    fis''8    e''8   
 d''8    b'8    d''8  \bar "|"   b'4.    a'8    b'4    d''8    b'8  \bar "|"   
a'8    d'8    fis'8    a'8    b'4.    d''8  \bar "|"   a'8    fis'8    e'8    
fis'8    d'4    d'8    e'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
