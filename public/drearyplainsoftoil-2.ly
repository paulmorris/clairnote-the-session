\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/300#setting13061"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/300#setting13061" {"https://thesession.org/tunes/300#setting13061"}}}
	title = "Dreary Plains Of Toil, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \dorian   \repeat volta 2 {   e''16    a'8.    a'8.    g'16    
e'16    a'8.    a'8.    g'16  \bar "|"   e'16    a'8.    g'16    bes'8.    
c''8.    a'16    c''16    e''8.  \bar "|"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
