\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/602#setting13614"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/602#setting13614" {"https://thesession.org/tunes/602#setting13614"}}}
	title = "Whiskey Before Breakfast"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key d \major   fis'8    d'8    fis'8    g'8    a'4    a'4    
\bar "|"   a'8    b'8    a'8    g'8    fis'8    e'8    d'8    fis'8    \bar "|" 
  g'4    b'8    g'8    fis'4    a'8    fis'8    \bar "|"   e'8    d'8    e'8    
fis'8    g'8    fis'8    e'8    fis'8    \bar "|"   e'8    d'8    fis'8    g'8  
  a'4    a'4    \bar "|"   a'8    b'8    a'8    g'8    fis'8    e'8    d'8    
fis'8    \bar "|"   g'4    b'8    g'8    fis'4    a'8    fis'8    \bar "|"   
e'8    d'8    e'8    fis'8    d'2    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
