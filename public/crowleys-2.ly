\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Roads To Home"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/759#setting23217"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/759#setting23217" {"https://thesession.org/tunes/759#setting23217"}}}
	title = "Crowley's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''8    b'8  \bar "|"   a'8 ^"D"   fis'8    a'8    
d''8      b'4 ^"G"   d''8    b'8  \bar "|"   a'8 ^"D"   d'8    d'16    d'16    
d'8    a'8    d'8    d'16    d'16    d'8  \bar "|"   a'8    fis'8    a'8    
d''8      b'8 ^"G"   a'8    b'8    cis''8  \bar "|"   d''8 ^"A"   e''8    
fis''8    g''8      fis''8 ^"D"   d''8    d''8    b'8  \bar "|"     a'8    d'8  
  a'8    d''8      b'4 ^"G"   d''8    b'8  \bar "|"   a'8 ^"D"   d'8    d'16    
d'16    d'8    a'8    d'8    d'16    d'16    d'8  \bar "|"   a'8    fis'8    
a'8    d''8      b'8 ^"G"   a'8    b'8    cis''8  \bar "|"   d''8 ^"A"   e''8   
 fis''8    g''8      fis''8 ^"D"   d''8    d''8    e''8  \bar "||"     fis''8   
 a''8    a''16    a''16    fis''8      g''16 ^"G"   g''16    g''8    fis''8    
g''8  \bar "|"   e''8 ^"A"   a'8    a'16    a'16    a'8    e''8    a'8    a'16  
  a'16    a'8  \bar "|"   d''8 ^"D"   fis''8    fis''16    e''16    fis''8      
g''4 ^"G"   fis''8    g''8  \bar "|"   a''8 ^"D"   fis''8    g''8    e''8      
fis''8 ^"G"   d''8    d''8    e''8  \bar "|"       fis''8 ^"D"   a''8    a''16  
  a''16    fis''8      g''4 ^"G"   fis''8    g''8  \bar "|"   e''8 ^"A"   a'8   
 a'16    a'16    a'8    e''8    a'8    a'16    a'16    a'8  \bar "|"   d''8 
^"D"   fis''8    fis''16    e''16    fis''8      g''4 ^"G"   fis''8    g''8  
\bar "|"   a''8 ^"D"   fis''8    g''8    e''8      fis''8 ^"G"   d''8    d''4  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
