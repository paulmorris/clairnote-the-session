\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/879#setting14061"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/879#setting14061" {"https://thesession.org/tunes/879#setting14061"}}}
	title = "An Phis Fhliuch"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   a'8  \bar "|" \tuplet 3/2 {   d'8  
  e'16    fis'16  }   g'16    a'8    a'8    fis'8    a'8    c''4    a'8  
\bar "|"   b'8    a'8    g'8    fis'4.    g'8    e'8    d'8  \bar "|" 
\tuplet 3/2 {   d'8    e'16    fis'16  }   g'16    a'8    a'8    fis'8    a'8    
d''4    a'8  \bar "|"   d''8    g''16    fis''16    e''8    d''8    c''8    a'8 
   g'8    e'8    d'8  }     \repeat volta 2 {   d''8    b'16    c''16    d''8   
 e''8    g''8    e''8    d''4    a'8  \bar "|"   d''8    b'16    c''16    d''8  
  fis''8    e''8    fis''8    g''4.  \bar "|"   a''4.    g''8    e''8    d''8   
 c''4    a'8  \bar "|"   b'8    a'8    g'8    fis'4.    g'8    e'8    d'8  }    
 \repeat volta 2 { \tuplet 3/2 {   d'8    e'16    fis'16  }   g'16    a'8    a'8 
   fis'8    d''8    a'8    fis'8    d''8  \bar "|"   a'8    fis'8    d''8    
a'8    fis'8    d''8    g'8    e'8    d'8  \bar "|" \tuplet 3/2 {   d'8    e'16  
  fis'16  }   g'16    a'8    a'8    fis'8    a'8    c''4    a'8  \bar "|"   b'8 
   a'8    g'8    fis'4.    g'8    e'8    d'8  }     \repeat volta 2 {   d'4.    
d'4.    c''4.  \bar "|"   c''4    b'8    c''4    a'8    g'8    e'8    d'8  
\bar "|"   d'4.    d'4.    d''4    a'8  \bar "|"   d''8    g''16    fis''16    
e''8    d''8    c''8    a'8    g'8    e'8    d'8  }     \repeat volta 2 {   
d''8    b'16    c''16    d''8    e''8    g''8    e''8    c''4    a'8  \bar "|"  
 d''8    b'16    c''16    d''8    fis''8    e''8    fis''8    g''4.  \bar "|"   
a''4.    g''8    e''8    d''8    c''4    a'8  \bar "|"   b'8    a'8    g'8    
fis'8    g'16    a'16    fis'8    g'8    e'8    d'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
