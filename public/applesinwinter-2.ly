\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/299#setting13054"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/299#setting13054" {"https://thesession.org/tunes/299#setting13054"}}}
	title = "Apples In Winter"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key b \dorian   fis''8    b'8    b'8    a''8    b'8    b'8  \bar "|" 
  fis''8    e''8    cis''8    cis''8    b'8    cis''8  \bar "|"   a'8    b'8    
a'8    cis''8    a'8    cis''8  \bar "|"   e''8    fis''8    gis''8    a''4. 
^"~"  \bar "|"   fis''8    b'8    b'8    a''8    b'8    b'8  \bar "|"   fis''8  
  e''8    cis''8    cis''8    b'8    cis''8  \bar "|"   a'8    cis''8    e''8   
 a''4. ^"~"  \bar "|"   fis''8    e''8    cis''8    b'8    cis''8    e''8  
\bar ":|."   fis''8    e''8    cis''8    b'4    a'8  \bar "||"   \bar ".|:"   
b'8    b''8    b''8    b''8    a''8    fis''8  \bar "|"   a''8    b''8    b''8  
  b''8    a''8    fis''8  \bar "|"   a''4. ^"~"    a''8    e''8    cis''8  
\bar "|"   a'8    cis''8    e''8    a''4. ^"~"  \bar "|"   fis''8    b''8    
b''8    fis''8    b''8    b''8  \bar "|"   e''8    a''8    a''8    e''8    a''8 
   a''8  \bar "|"   fis''8    e''8    cis''8    a''4. ^"~"  \bar "|"   fis''8   
 e''8    cis''8    b'4    a'8  \bar ":|."   fis''8    e''8    cis''8    b'8    
cis''8    e''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
