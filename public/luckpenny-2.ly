\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Gerry_McCartney"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1334#setting14682"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1334#setting14682" {"https://thesession.org/tunes/1334#setting14682"}}}
	title = "Luck Penny, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   b'16 (   c''16  -) \bar "|"   d''8    b'8    g'8    
g'8    fis'8    g'8  \bar "|"   d''8    b'8    g'8    g'8    a'8    b'8    
\bar "|"   c''8    a'8    f'8    f'8    e'8    f'8    \bar "|"   c''8    a'8    
f'8    f'8    b'8    c''8  \bar "|"   d''8    b'8    g'8    g'8    fis'8    g'8 
 \bar "|"   g''8    a''8    g''8    fis''8    d''8    b'8    \bar "|"   d''8    
e''8    fis''8    g''8    d''8    b'8    \bar "|"   c''8    a'8    fis'8    g'4 
   b'16    c''16    \bar "|"   d''8    b'8    g'8    g'8    fis'8    g'8    
\bar "|"   d'8    g'8    b'8    d'8    g'8    b'8  \bar "|"   c''8    a'8    
f'8    f'8    e'8    f'8    \bar "|"   c'8    f'8    f'8    a8    f'8    f'8    
\bar "|"   d''8    b'8    g'8    g'8    fis'8    g'8    \bar "|"   g''8    a''8 
   g''8    fis''8    d''8    b'8    \bar "|"   d''8    e''8    fis''8    g''8   
 d''8    b'8    \bar "|"   c''8    a'8    fis'8    g'4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
