\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "MM"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1025#setting14249"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1025#setting14249" {"https://thesession.org/tunes/1025#setting14249"}}}
	title = "Sweeney's Wheel"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key b \minor   \repeat volta 2 {   b'4    d''8    b'8    fis'8    
b'8    d''8    b'8  \bar "|"   g'8    b'8    d''8    b'8    cis''8    b'8    
a'8    cis''8  \bar "|"   b'4    d''8    b'8    fis'8    b'8    d''8    b'8  
\bar "|"   cis''8    b'8    a'8    b'8    cis''8    d''8    e''8    fis''8  }   
\repeat volta 2 {   g''8    e''8    fis''8    e''8    g''8    e''8    a''8    
e''8  \bar "|"   g''8    e''8    fis''8    e''8    g''4    e''8    fis''8  
\bar "|"   g''8    e''8    fis''8    e''8    g''8    fis''8    e''8    d''8  
\bar "|"   cis''8    d''8    e''8    fis''8    g''4    g''8    a''8  }   
\repeat volta 2 {   b''8    a''8    a''4    fis''8    a''8    fis''8    d''8  
\bar "|"   cis''8    d''8    e''8    fis''8    g''4.    a''8  \bar "|"   b''8   
 a''8    a''4    fis''8    d''8    d''8    b'8  \bar "|"   a'8    b'8    cis''8 
   d''8    e''8    fis''8    g''8    a''8  }   \repeat volta 2 {   fis''8    
d''8    a'8    d''8    fis'8    d''8    a'8    d''8  \bar "|"   fis''8    d''8  
  a'8    d''8    cis''8    d''8    e''8    g''8  \bar "|"   fis''8    d''8    
a'8    d''8    fis'8    d''8    a'8    d''8  \bar "|"   cis''8    b'8    a'8    
b'8    cis''8    d''8    e''8    g''8  }   \repeat volta 2 {   d''4    fis''8   
 d''8    a''8    d''8    fis''8    d''8  \bar "|"   d''4    fis''8    d''8    
cis''8    d''8    e''8    cis''8  \bar "|"   d''4    fis''8    d''8    a''8    
d''8    fis''8    d''8  \bar "|"   cis''8    b'8    a'8    b'8    cis''8    
d''8    e''8    cis''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
