\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "celticladda"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/132#setting12745"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/132#setting12745" {"https://thesession.org/tunes/132#setting12745"}}}
	title = "Eileen Curran"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key f \major   bes'8    a'8    g'8    f'8    d'8    g'8    g'8    
bes'8  \bar "|"   a'8    f'8    c''8    f'8    d''8    f'8    c''8    f'8  
\bar "|"   bes'8    a'8    g'8    f'8    d'8    g'8    g'8    a'8  \bar "|"   
bes'8    a'8    bes'8    c''8    d''8    g''8    g''8    a''8  \bar "|"   
bes''8    g''8    a''8    fis''8    g''8    d''8    d''8    e''8  \bar "|"   
f''8    d''8    c''8    bes'8    a'8    f'8    f'4  \bar "|"   bes'8    a'8    
g'8    f'8    d'8    g'8    g'8    a'8  \bar "|"   bes'8    d''8    c''8    a'8 
   bes'8    g'8    g'4  }   \repeat volta 2 {   g'4    g''4    g''8    a''8    
g''8    f''8  \bar "|"   f'4    f''4    f''8    g''8    f''8    d''8  \bar "|"  
 ees'4    g''4    ees'4    g''8    a''8  \bar "|"   bes''8    g''8    a''8    
f''8    d''8    g''8    g''8    a''8  \bar "|"   bes''8    g''8    a''8    
fis''8    g''8    d''8    d''8    e''8  \bar "|"   f''8    d''8    c''8    
bes'8    a'8    f'8    f'8    a'8  \bar "|"   bes'8    a'8    g'8    f'8    d'8 
   g'8    g'8    a'8  \bar "|"   bes'16    c''16    d''8    c''8    a'8    
bes'8    g'8    g'4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
