\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/321#setting27974"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/321#setting27974" {"https://thesession.org/tunes/321#setting27974"}}}
	title = "Woman Of The House, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \repeat volta 2 {   d'8    fis'8    fis'8    e'8    
fis'4.    e'8  \bar "|"   d'8    fis'8    fis'4    b'8    fis'8    a'8    fis'8 
 \bar "|" \tuplet 3/2 {   e'8    fis'8    e'8  }   d'8    fis'8    e'4  
\tuplet 3/2 {   fis'8    g'8    a'8  } \bar "|"   b'8    fis'8    fis'4    b'8   
 fis'8    a'8    fis'8  \bar "|"     d'8    fis'8    fis'8    e'8    fis'4.    
e'8  \bar "|"   d'8    fis'8    fis'4    \tuplet 3/2 {   b'8    cis''8    b'8  } 
  a'8    fis'8  \bar "|"   d'8    e'8    fis'8    d'8    \tuplet 3/2 {   e'8    
fis'8    e'8  }   d'8    e'8    \bar "|" \tuplet 3/2 {   fis'8    e'8    d'8  }  
 e'8    d'8    b'8    d''8    d''4  }     \repeat volta 2 {   |
 cis''4.    a'8    b'8    fis'8    fis'4  \bar "|"   b'8    fis'8    d''8    
fis'8    b'8    fis'8    fis'4  \bar "|"   cis''4.    a'8    b'8    a'8    
\tuplet 3/2 {   fis'8    g'8    a'8  } \bar "|"   b'8    e''8    e''8    d''8    
e''4.    d''8  \bar "|"     cis''4.    a'8    b'8    fis'8    fis'4  \bar "|"   
b'8    fis'8    d''8    fis'8    b'8    fis'8    fis'8    e'8  \bar "|"   d'8   
 e'8    fis'8    d'8    \tuplet 3/2 {   e'8    fis'8    e'8  }   d'8    e'8    
\bar "|" \tuplet 3/2 {   fis'8    e'8    d'8  }   e'8    d'8    b'8    d''8    
d''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
