\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1259#setting14570"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1259#setting14570" {"https://thesession.org/tunes/1259#setting14570"}}}
	title = "Little Fishie"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \major   c''4    e''4 -.   a'4    g'4    fis'4  \grace {    
e''8  }   d''4   ~    \bar "|"   d''4    b'4   ~    b'4    g'4    a'8    c''8   
 b'8    d''8  \bar "|"   c''4 -.   e'4 (   e'4    fis'4    g'2  -) \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
