\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "birlibirdie"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/331#setting13112"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/331#setting13112" {"https://thesession.org/tunes/331#setting13112"}}}
	title = "Terry Teahan's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key a \major   a'4    a'8    fis''8  \bar "|"   e''8    d''8    b'8  
  d''8  \bar "|"   a'4    a'8    fis''8  \bar "|"   e''8    d''8    d''4  
\bar "|"   a'4    a'8    fis''8  \bar "|"   e''8    d''8    b'8    d''8  
\bar "|"   a'4    a''8    fis''8  \bar "|"   e''8    d''8    d''4  \bar ":|."   
a'4    a''8.    fis''16  \bar "|"   e''8    d''8    d''8    e''8  \bar "||"   
fis''8    a''8    e''8.    fis''16  \bar "|"   e''8    d''8    b'8    d''8  
\bar "|"   fis''8    a''8    e''8.    fis''16  \bar "|"   e''8    d''8    d''8. 
   e''16  \bar "|"   fis''8    a''8    e''8.    fis''16  \bar "|"   e''8    
d''8    b'8    d''8  \bar "|"   a''4    a'8    fis''8  \bar "|"   e''8    d''8  
  d''8.    e''16    \bar ":|."   a'4    a''8.    fis''16  \bar "|"   e''8    
d''8    d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
