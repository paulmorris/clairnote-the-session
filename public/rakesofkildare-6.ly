\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/84#setting12589"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/84#setting12589" {"https://thesession.org/tunes/84#setting12589"}}}
	title = "Rakes Of Kildare, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \dorian   \repeat volta 2 {   d'8    \bar "|"   d'8    g'8    
g'8    g'8    f'8    g'8    \bar "|"   a'8    d''8    d''8    d''4    e''8    
\bar "|"   f''8    e''8    f''8    d''8    e''8    d''8    \bar "|"   c''8    
a'8    g'8    f'4    d'8    \bar "|"     d'8    g'8    g'8    g'8    f'8    g'8 
   \bar "|"   a'8    d''8    d''8    d''4    e''8    \bar "|"   f''8    e''8    
f''8    d''8    c''8    a'8    \bar "|"   g'4.    g'4    }     
\repeat volta 2 {   g''8    \bar "|"   g''8    d''8    g''8    g''8    d''8    
g''8    \bar "|"   g''8    a''8    bes''8    a''4    g''8    \bar "|"   f''8    
e''8    f''8    d''8    e''8    d''8    \bar "|"   c''8    a'8    g'8    f'8    
g'8    a'8    \bar "|"     bes'4    bes'8    c''8    bes'16    a'16    g'8    
\bar "|"   a'8    d''8    d''8    d''4    e''8    \bar "|"   f''8    e''8    
d''8    c''8    a'8    f'8    \bar "|"   g'4.    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
