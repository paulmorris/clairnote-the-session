\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "chicagofiddler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/698#setting698"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/698#setting698" {"https://thesession.org/tunes/698#setting698"}}}
	title = "Caves Of Kiltanon, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \dorian   g'4    f'8    \repeat volta 2 {   d'8    g'8    a'8  
  bes'4    c''8    \bar "|"   d''8    g''8    f''8    d''8    c''8    a'8    
\bar "|"   bes'8    d'8    d''8    c''8    bes'8    a'8    \bar "|"   bes'8    
d'8    g'8    f'8    d'8    c'8    \bar "|"     d'8    g'8    a'8    bes'4    
c''8    \bar "|"   d''8    g''8    f''8    d''8    c''8    a'8    \bar "|"   
bes'8    d'8    d''8    c''8    bes'8    a'8    } \alternative{{   bes'8    d'8 
   f'8    g'4    f'8    } {   bes'8    d'8    f'8    g'4    d'8    } }      g'8 
   g''8    a''8    bes''8    a''8    g''8    \bar "|"   a''8    f''8    d''8    
c''8    bes'8    a'8    \bar "|"   bes'8    d'8    d''8    c''8    bes'8    a'8 
   \bar "|"   bes'8    a'8    g'8    a'8    f'8    d'8    \bar "|"     g'8    
g''8    a''8    bes''8    a''8    g''8    \bar "|"   a''8    f''8    d''8    
c''8    bes'8    c''8    \bar "|"   d''8    bes''8    a''8    g''8    f''8    
d''8    \bar "|"   g''8    d''8    f''8    g''4. ^"~"    \bar "|"     d''8    
g''8    a''8    bes''8    a''8    g''8    \bar "|"   a''8    f''8    d''8    
c''8    bes'8    a'8    \bar "|"   bes'8    d'8    d''8    c''8    bes'8    a'8 
   \bar "|"   bes'8    a'8    g'8    a'8    f'8    d'8    \bar "|"     bes'8    
d''8    bes'8    c''4. ^"~"    \bar "|"   d''8    bes''8    a''8    g''8    
f''8    d''8    \bar "|"   bes'8    d'8    d''8    c''8    bes'8    a'8    
\bar "|"   bes'8    d'8    f'8    g'4    a'8    \bar "|"     bes'8    g'8    
d''8    bes'8    g'8    d''8    \bar "|"   a'8    f'8    c''8    a'8    f'8    
a'8    \bar "|"   bes'8    g'8    d''8    bes'8    g'8    bes'8    \bar "|"   
d''8    a''8    f''8    g''4. ^"~"    \bar "|"     bes''8    a''8    g''8    
a''8    g''8    f''8    \bar "|"   g''8    f''8    d''8    c''8    bes'8    a'8 
   \bar "|"   bes'8    d'8    d''8    c''8    bes'8    a'8    \bar "|"   bes'8  
  d'8    f'8    g'4    a'8    \bar "|"     bes'8    g'8    d''8    bes'8    g'8 
   d''8    \bar "|"   a'8    f'8    c''8    a'8    f'8    a'8    \bar "|"   
bes'8    g'8    d''8    bes'8    g'8    bes'8    \bar "|"   d''8    a''8    
f''8    g''4. ^"~"    \bar "|"     bes''8    a''8    g''8    a''8    g''8    
f''8    \bar "|"   d''8    g''8    f''8    d''8    c''8    a'8    \bar "|"   
bes'8    d'8    d''8    c''8    bes'8    a'8    \bar "|"   bes'8    d'8    f'8  
  g'4.    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
