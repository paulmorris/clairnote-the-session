\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "fidicen"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1375#setting1375"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1375#setting1375" {"https://thesession.org/tunes/1375#setting1375"}}}
	title = "Jennie's Frolics"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   cis''4    a'8    cis''8    d''4    b'8    d''8  
\bar "|"   cis''4    a'8    cis''8    b'8    e'8    e'8    b'8  \bar "|"   
cis''4    a'8    cis''8    d''4    b'8    d''8  \bar "|"   cis''8    a'8    b'8 
   gis'8    a'4    a'4  \bar "|"     cis''4    a'8    cis''8    d''4    d''8    
d''8  \bar "|"   cis''4    a'8    cis''8    b'8    e'8    e'8    b'8  \bar "|"  
 cis''4    a'8    cis''8    d''4    b'8    d''8  \bar "|"   cis''8    a'8    
b'8    gis'8    a'4    a'4  \bar "||"     cis''8    e''8    e''8    gis''8    
fis''4    e''8    d''8  \bar "|"   cis''4    a'8    cis''8    b'8    e'8    e'8 
   b'8  \bar "|"   cis''8    e''8    e''8    gis''8    fis''4    e''8    d''8  
\bar "|"   cis''8    a'8    b'8    gis'8    a'4    a'4  \bar "|"     cis''8    
e''8    e''8    gis''8    fis''4    e''8    d''8  \bar "|"   cis''4    a'8    
cis''8    b'8    e'8    e'8    b'8  \bar "|"   cis''8    d''8    e''8    gis''8 
   fis''4    e''8    fis''8  \bar "|"   gis''4    fis''8    gis''8    a''8    
fis''8    e''8    d''8  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
