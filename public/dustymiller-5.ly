\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "tin_whistler"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/28#setting12419"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/28#setting12419" {"https://thesession.org/tunes/28#setting12419"}}}
	title = "Dusty Miller, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key g \major   \repeat volta 2 {   b'4    c''8    d''4    b'8    a'4 
   g'8    \bar "|"   fis'4    g'8    a'4    b'8    c''4    a'8    \bar "|"   
b'4    c''8    d''4    b'8    a'4    g'8    \bar "|"   d'4    g'8    b'4    a'8 
   g'4.    }   \repeat volta 2 {   b'8    c''8    d''8    e''4    fis''8    
g''4    r8   \bar "|"   b'4    d''8    g''4    e''8    d''8    b'8    a'8    
\bar "|"   b'4    d''8    e''4    fis''8    g''4    r8   \bar "|"   d'4    g'8  
  b'4    a'8    g'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
