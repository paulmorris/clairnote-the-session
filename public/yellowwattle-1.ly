\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "slainte"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1237#setting1237"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1237#setting1237" {"https://thesession.org/tunes/1237#setting1237"}}}
	title = "Yellow Wattle, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \mixolydian   \repeat volta 2 {   d''8    c''8    a'8    a'8   
 g'8    e'8  \bar "|"   a'8    b'8    a'8    a'8    b'8    c''8  \bar "|"   
d''8    c''8    a'8    a'8    b'8    c''8  \bar "|"   d''8    c''8    a'8    
a'8    g'8    e'8  \bar "|"     d''8    c''8    a'8    a'8    g'8    e'8  
\bar "|"   a'8    b'8    a'8    a'8    g'8    e'8  \bar "|"   e'8    d'8    d'8 
   c''8    d''8    e''8  \bar "|"   d''8    c''8    a'8    g'8    e'8    d'8  } 
    \repeat volta 2 {   d'8    e'8    d'8    c''4.  \bar "|"   d''8    e''8    
d''8    c''4.  \bar "|"   d'8    e'8    d'8    c''8    d''8    e''8  \bar "|"   
d''8    c''8    a'8    g'8    e'8    d'8  \bar "|"     d'8    e'8    d'8    
c''4.  \bar "|"   d''8    e''8    d''8    d''4    c''8  \bar "|"   a'8    b'8   
 a'8    a'8    b'8    c''8  \bar "|"   d''8    c''8    a'8    g'8    e'8    d'8 
 }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
