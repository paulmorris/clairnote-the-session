\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Paul-Kin"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/950#setting950"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/950#setting950" {"https://thesession.org/tunes/950#setting950"}}}
	title = "Fat Cat, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   a4    cis'8    e'8 ^\upbow   fis'8    e'8    cis''8   
 b'8  \bar "|"   a'8    e'8    fis'8    a'8    fis'8    e'8    cis'8    e'8  
\bar "|"   cis''8    b'8    b'8    a'8    fis'4    e'8    cis'8  \bar "|"   b4  
  a8    b8    cis'8    e'8    e'8    cis'8  \bar "|"     a4    cis'8    e'8    
fis'8    e'8    cis''8    b'8  \bar "|"   a'8    e'8    fis'8    a'8    fis'8   
 e'8    cis'8    e'8  \bar "|"   cis''8    b'8    b'8    a'8    fis'4    e'8    
cis'8  \bar "|"   b8    a8    cis'8    b8    a4.    b8  }     \repeat volta 2 { 
  |
 cis'4 ^"~"    e'8    cis'8    a8    cis'8    e'8    a'8  \bar "|"   cis''4 
^"~"    b'8    cis''8    a'8    fis'8    e'4  \bar "|"   a''2. ^"*"   fis''8    
e''8  \bar "|"   cis''4    b'8    a'8    fis'4    a'8    fis'8  \bar "|"     
e'8    a8    cis'8    e'8    e'8    fis'8    fis'8    a'8  \bar "|"   cis''4    
b'8    cis''8    a'8    fis'8    e'4  \bar "|"   a''2. ^"*"   b'8    a'8  
} \alternative{{   fis'4    e'8    fis'8    a'2  } {   fis'4    e'8    cis'8    
a2  \bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
