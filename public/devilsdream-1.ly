\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JeffK627"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/259#setting259"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/259#setting259" {"https://thesession.org/tunes/259#setting259"}}}
	title = "Devil's Dream, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \major   e''4    \bar "|"   e''8 ^"A"   a''8    gis''8    a''8 
   e''8    a''8    gis''8    a''8    \bar "|"   e''8    a''8    gis''8    a''8  
  fis''8    e''8    d''8    cis''8    \bar "|"   d''8 ^"Bm"   fis''8    b'8    
fis''8    d''8    fis''8    b'8    fis''8    \bar "|"   d''8    fis''8    b'8   
 fis''8    gis''8 (   fis''8    e''8    d''8  -) \bar "|"     e''8 ^"A"   a''8  
  gis''8    a''8    e''8    a''8    gis''8    a''8    \bar "|"   e''8    a''8   
 gis''8    a''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''8 ^"D"   
fis''8    e''8    d''8      cis''8 ^"A"   a'8    b'8 ^"E"   gis'8    \bar "|"   
e'4 ^"E"     a'4 ^"A"   a'4    }     \repeat volta 2 {   e''4    \bar "|"   
cis''8 ^"A"   e''8    a'8    e''8    cis''8    e''8    a'8    e''8    \bar "|"  
 cis''8    e''8    a'8    e''8    fis''8    e''8    d''8    cis''8    \bar "|"  
 d''8 ^"Bm"   fis''8    b'8    fis''8    d''8    fis''8    b'8    fis''8    
\bar "|"   d''8    fis''8    b'8    fis''8    a''8    fis''8    e''8    d''8    
\bar "|"     cis''8 ^"A"   e''8    a'8    e''8    cis''8    e''8    a'8    e''8 
   \bar "|"   cis''8    e''8    a'8    e''8    fis''8    e''8    d''8    cis''8 
   \bar "|"   d''8 ^"D"   fis''8    e''8    d''8      cis''8 ^"A"   a'8    b'8 
^"E"   gis'8    \bar "|"   e'4 ^"E"     a'4 ^"A"   a'4    }     
\repeat volta 2 {   e''4    \bar "|"   cis''8 ^"A"   a'8    e'8    a'8    
cis''8    a'8    e''8    a'8    \bar "|"   cis''8    a'8    e''8    a'8    
fis''8    e''8    d''8    cis''8    \bar "|"   d''8 ^"bm"   b'8    gis'8    b'8 
   d''8    b'8    fis''8    e''8    \bar "|"   d''8 ^"D"   e''8    fis''8    
gis''8      a''8 ^"E"   gis''8    fis''8    e''8    \bar "|"     cis''8 ^"A"   
a'8    e'8    a'8    cis''8    a'8    e''8    a'8    \bar "|"   cis''8    a'8   
 cis''8    e''8    fis''8    e''8    d''8    cis''8    \bar "|"   d''8 ^"D"   
fis''8    e''8    d''8      cis''8 ^"A"   a'8    b'8 ^"E"   gis'8    \bar "|"   
e'4 ^"E"     a'4 ^"A"   a'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
