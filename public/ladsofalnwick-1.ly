\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1194#setting1194"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1194#setting1194" {"https://thesession.org/tunes/1194#setting1194"}}}
	title = "Lads Of Alnwick, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key a \major   \repeat volta 2 { \grace {    gis''8  }   a''2    
e''4    fis''8    gis''8    a''8    gis''8    fis''8    e''8  \bar "|" 
\tuplet 3/2 {   cis''8    d''8    cis''8  }   a'4    a''4 -.   a'4 (   cis''8    
d''8    e''8    cis''8  -) \bar "|"   a''2    e''4    fis''8    gis''8    a''8  
  gis''8    fis''8    e''8  \bar "|"   d''4    b'4  \grace {    gis''8  }   
fis''4 -.   b'4 (   d''4    fis''4  -) }     \repeat volta 2 {   a'8    b'8    
cis''8    d''8    \tuplet 3/2 {   e''8    fis''8    e''8  }   cis''4    
\tuplet 3/2 {   e''8    fis''8    e''8  }   cis''4  \bar "|"   a'8    b'8    
cis''8    d''8    \tuplet 3/2 {   e''8    fis''8    e''8  }   cis''4    e''4  
\grace {    gis''8  }   a''4  \bar "|"   a'8    b'8    cis''8    d''8    
\tuplet 3/2 {   e''8    fis''8    e''8  }   cis''4    \tuplet 3/2 {   e''8    
fis''8    e''8  }   cis''4  \bar "|"   b'8    cis''8    d''8    e''8  \grace {  
  gis''8  }   fis''4 -.   b'4 (   d''4    fis''4  -) }     \repeat volta 2 {   
e''4 (   a''4 -. -)   cis''8 (   d''8    e''8    cis''8    a''4 -. -)   cis''4 
( \bar "|"   e''4    a''4 -. -)   cis''8    d''8    e''8    cis''8    d''4  
\tuplet 3/2 {   fis''8    gis''8    fis''8  } \bar "|"   e''4 (   a''4 -. -)   
cis''8 (   d''8    e''8    cis''8    a''4 -. -)   cis''4  \bar "|"   b'8    
cis''8    d''8    e''8  \grace {    gis''8  }   fis''4 -.   b'4 (   d''4    
fis''4  -) }     \repeat volta 2 {   a'8    b'8    cis''8    d''8    e''8    
fis''8    e''8    d''8    cis''8    d''8    e''8    cis''8  \bar "|"   e''8    
fis''8    e''8    d''8    cis''8    d''8    e''8    cis''8    e''4  \grace {    
gis''8  }   a''4  \bar "|"   a'8    b'8    cis''8    d''8    e''8    fis''8    
e''8    d''8    cis''8    d''8    e''8    cis''8  \bar "|"   b'8    cis''8    
d''8    e''8  \grace {    gis''8  }   fis''4 -.   b'4 (   d''4    fis''4  -) }  
 
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
