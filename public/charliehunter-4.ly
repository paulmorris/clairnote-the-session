\version "2.7.40"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/809#setting13961"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/809#setting13961" {"https://thesession.org/tunes/809#setting13961"}}}
	title = "Charlie Hunter"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key d \major   \repeat volta 2 {   a'8    \bar "|"   d'8    fis'8    
a'8    g'8    b'8    d''8    \bar "|"   a'8    d''8    fis''8    a''4    g''8   
 \bar "|"   fis''8    e''8    d''8    b'8    cis''8    d''8    \bar "|"   e''8  
  cis''8    a'8    g'8    fis'8    e'8    \bar "|"     d'8    fis'16    g'16    
a'8    g'8    b'16    cis''16    d''8    \bar "|"   a'8    d''8    fis''8    
a''4    g''8    \bar "|"   fis''8    eis''8    fis''8    g''8    e''!8    
cis''8    \bar "|"   e''8    d''8    cis''8    d''4    }     \repeat volta 2 {  
 fis''16    g''16    \bar "|"   a''8    fis''8    d''8    d''8    cis''8    
d''8    \bar "|"   b'8    g'8    fis'8    g'4    fis'8    \bar "|"   e'8    g'8 
   b'8    e''8    fis''8    d''8    \bar "|"   cis''8    a'8    gis'8    a'4    
a''16    g''!16    \bar "|"     fis''8    d''8    a'8    fis'8    d'8    fis'8  
  \bar "|"   g'8    b'8    d''8    g''4    g''8    \bar "|"   fis''8    eis''8  
  fis''8    g''8    e''!8    cis''8    \bar "|"   e''8    d''8    cis''8    
d''4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
