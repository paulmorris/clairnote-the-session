\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Craig"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/123#setting123"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/123#setting123" {"https://thesession.org/tunes/123#setting123"}}}
	title = "Parting, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key e \dorian   e'8    b'8    b'8    a'8    b'4. ^"~"    a'8  
\bar "|"   b'16    cis''16    d''8    a'8    fis'8    fis'8    e'8    d'8    
fis'8  \bar "|"   e'8    b'8    b'8    a'8    b'8    cis''8    d''8    e''8  
\bar "|"   fis''8    a''8    fis''16    e''16    d''8    e''8    b'8    b'8    
a'8  \bar "|"     e'8    b'8    b'8    a'8    b'4. ^"~"    cis''8  \bar "|"   
b'16    cis''16    d''8    a'8    fis'8    fis'8    e'8    d'8    a'8  \bar "|" 
  d''16    e''16    fis''8    e''8    cis''8    d''8    b'8    cis''8    a'8  
\bar "|"   g'16    a'16    b'8    a'8    fis'8    g'8    e'8    e'8    d'8  }   
  \repeat volta 2 {   g'4. ^"~"    b'8    d''8    b'8    g'8    b'8  \bar "|"   
a'8    g'8    fis'8    g'8    a'8    d''8 ^"~"    d''4  \bar "|"   e''8    b'8  
  g''8    b'8    e''8    b'8    g'16    a'16    b'8  \bar "|"   a'8    d''8    
d''8    cis''8    d''16    e''16    fis''8    d''8    a'8  \bar "|"     e''16   
 fis''16    g''8    fis''8    e''8    d''8    b'8    g'8    b'8  \bar "|"   a'8 
   b'8    a'8    g'8    fis'8    d'8 ^"~"    d'4  \bar "|"   g'8    fis'8    
g'8    b'8    d''8    b'8    g'16    a'16    b'8  \bar "|"   a'8    fis'8    
d'8    fis'8    e'4. ^"~"    fis'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
