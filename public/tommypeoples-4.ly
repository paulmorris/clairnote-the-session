\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Mazurka"
	source = "https://thesession.org/tunes/1323#setting14663"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1323#setting14663" {"https://thesession.org/tunes/1323#setting14663"}}}
	title = "Tommy Peoples'"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key d \major   \repeat volta 2 {   d'8.    fis'16  \bar "|"   a'4    
fis'8.    a'16    d''8.    cis''16  \bar "|"   b'4    g'8.    b'16    e''8.    
fis''16  \bar "|"   g''8.    e''16    cis''4    a'4  \bar "|"   b'8.    a'16    
fis'4    d'8.    fis'16  \bar "|"   a'4    fis'8.    a'16    d''8.    cis''16  
\bar "|"   b'8.    g'16    b'4    e''8.    fis''16  \bar "|"   g''8.    e''16   
 cis''8.    g''16    fis''8.    e''16  \bar "|"   d''2  }   \repeat volta 2 {   
fis''8.    g''16  \bar "|"   a''8.    fis''16    d''8.    fis'16    a'8.    
d''16  \bar "|" \tuplet 3/2 {   cis''8    d''8    cis''8  }   b'4    e''8.    
fis''16  \bar "|"   g''8.    e''16    cis''8.    a'16    b'8.    cis''16  
\bar "|" \tuplet 3/2 {   b'8    cis''8    b'8  }   a'4    fis''8.    g''16  
\bar "|"   a''4    fis''8.    d''16    a'8.    d''16  \bar "|"   cis''4    b'4  
  e''8.    fis''16  \bar "|"   g''8.    e''16    cis''8.    g''16    fis''8.    
e''16  } \alternative{{   d''4.    e''8  } {   d''2  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
