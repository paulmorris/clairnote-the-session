\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/87#setting23585"
	arranger = "Setting 15"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/87#setting23585" {"https://thesession.org/tunes/87#setting23585"}}}
	title = "Rolling In The Ryegrass"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''8    b'8    \bar "|"   a'16    b'16    a'8    a'8  
  fis'8    d'8    fis'8    a'16    g'16    fis'8    \bar "|"   g'4    b'8    
g'8    d'8    g'8    b'8    d''8    \bar "|"   a'4    a'8    fis'8    d'4    
fis'16    g'16    a'8    \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8  
  d'8    b'8    \bar "|"     a'8    gis'8    a'8    fis'8    d'8    fis'8    
a'8    fis'8    \bar "|"   g'4    d'8    g'8    b'8    d''8    g'8    b'8    
\bar "|"   a'8    b'8    a'16    g'16    fis'8    d'8    fis'8    a'8    fis'8  
  \bar "|"   g'8    b'8    a'8    fis'8    e'4    \bar "||"     d'8    b'8    
\bar "|"   a'8    b'8    d''8    e''8    fis''8    d''8    a'8    d''8    
\bar "|"   g''4    g''8    e''8    fis''8    d''8    b'8    g'8    \bar "|"   
a'16    b'16    a'8    d''8    e''8    fis''4    e''16    fis''16    g''8    
\bar "|"   a''8    fis''8    d''8    fis''8    e''16    fis''16    e''8    d''8 
   b'8    \bar "|"     a'4    cis''16    d''16    e''8    fis''8    a'8    d''8 
   a'8    \bar "|"   g''8    a'8    fis''8    g''8    e''16    fis''16    e''8  
  d''8    b'8    \bar "|"   a'8    b'8    d''8    e''8    fis''16    g''16    
fis''8    d''8    fis''8    \bar "|"   a''8    a'8    d''8    fis''8    e''4    
\bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
