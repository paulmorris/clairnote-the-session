\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thing"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/953#setting28580"
	arranger = "Setting 8"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/953#setting28580" {"https://thesession.org/tunes/953#setting28580"}}}
	title = "Elizabeth Kelly's Delight"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key d \minor   \repeat volta 2 {   d''4.    d''8    c''8    e''8    
d''8    c''8    a'8    \bar "|"   d''8    e''8    d''8    d''8    c''8    a'8   
 g'8    a'8    c''8    \bar "|"   d''4.    d''8    c''8    e''8    d''8    c''8 
   a'8    \bar "|"   c''8    d''8    b'8    c''4    a'8    g'8    a'8    c''8   
 }     \repeat volta 2 {   f''4.    e''8    d''8    c''8    d''8    c''8    a'8 
   \bar "|"   f''8    e''8    d''8    e''8    c''8    a'8    g'8    a'8    c''8 
   \bar "|"   f''4.    e''8    d''8    c''8    d''8    c''8    a'8    \bar "|"  
 c''8    d''8    b'8    c''4    a'8    g'8    a'8    c''8    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
