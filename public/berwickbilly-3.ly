\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Three-Two"
	source = "https://thesession.org/tunes/1211#setting14503"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1211#setting14503" {"https://thesession.org/tunes/1211#setting14503"}}}
	title = "Berwick Billy"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/2 \key g \major   \repeat volta 2 {   d''8    b'8    d''8    b'8    
e''8    c''8    e''8    c''8    d''8    b'8    g'8    b'8  \bar "|"   d''8    
b'8    e''8    c''8    d''8    b'8    g'8    b'8    c''4    e''4  \bar "|"   
d''8    b'8    d''8    b'8    e''8    c''8    e''8    c''8    d''8    b'8    
g'8    b'8  \bar "|"   c''4    a'4    a'4    b'4    c''4    e''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
