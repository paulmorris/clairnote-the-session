\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/200#setting22940"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/200#setting22940" {"https://thesession.org/tunes/200#setting22940"}}}
	title = "Splendid Isolation"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key e \dorian   \repeat volta 2 {   d'8    e'8    e'8    d'8    e'8  
  b'8    a'8    fis'8  \bar "|"   e'4    fis'8    e'8    d'8    e'8    fis'8    
a'8  \bar "|"   b'8    fis'8    a'8    fis'8    d'8    e'8    fis'8    a'8  
\bar "|"   b'8    e''8    e''8    d''8    e''4.    fis''8  \bar "|"     g''4.   
 e''8    d''8    b'8    b'4  \bar "|"   d''4    cis''8    e''8    d''8    b'8   
 a'8    fis'8  \bar "|"   e'4    fis'8    e'8    d'8    e'8    fis'8    a'8  
\bar "|"   b'8    fis'8    a'8    fis'8    e'2  }     \repeat volta 2 {   |
 e''4    fis''8    e''8    d''8    b'8    b'4  \bar "|"   e''8    d''8    b'8   
 a'8    fis'8    a'8    b'8    a'8  \bar "|"   fis'8    e'8    e'4    fis'8    
a'8    b'8    a'8  \bar "|"   fis'8    e'8    e'4    d'2  \bar "|"     d'8    
e'8    e'8    d'8    e'8    b'8    a'8    fis'8  \bar "|"   e'4    fis'8    e'8 
   d'8    e'8    fis'8    a'8  \bar "|"   b'8    e''8    e''4    d''8    e''8   
 fis''8    e''8  \bar "|"   d''8    b'8    a'8    fis'8    e'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
