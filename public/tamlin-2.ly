\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/248#setting12953"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/248#setting12953" {"https://thesession.org/tunes/248#setting12953"}}}
	title = "Tam Lin"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \minor   \bar "|"   d''8    cis''8    d''8    a'8    f'8    
a'8    d'4  \bar "|"   d''8    cis''8    d''8    a'8    f'8    a'8    d'4  
\bar "|"   c''8    e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   c''8    e'8    
d''8    e'8  \bar "|"   c''8    e'8    \tuplet 3/2 {   e'8    e'8    e'8  }   
c''8    bes'8    c''8    cis''8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
