\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "didier"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/232#setting21498"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/232#setting21498" {"https://thesession.org/tunes/232#setting21498"}}}
	title = "Walls Of Liscarrol, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key a \dorian   a''8    g''8    e''8    e''8    d''8    b'8  
\bar "|"   d''8    b'8    a'8    a'4    b'8  \bar "|"   d''8    b'8    b'8    
g''8    b'8    b'8    \bar "|"   d''8    e''8    fis''8    g''8    a''8    b''8 
   \bar "|"     a''8    g''8    e''8    e''8    d''8    b'8    \bar "|"   d''8  
  b'8    a'8    a'4    b'8    \bar "|"   d''8    b'8    b'8    g''8    b'8    
b'8    \bar "|"   a'8    b'8    a'8    a'4    a''8    }     a''8    e''8    
a''8    b''8    g''8    e''8    \bar "|"   a''8    e''8    a''8    b''8    g''8 
   e''8    \bar "|"   d''8    b'8    b'8    g''8    b'8    b'8    \bar "|"   
d''8    e''8    fis''8    g''8    a''8    b''8    \bar "|"   a''8    e''8    
a''8    b''8    g''8    e''8    \bar "|"     a''8    e''8    a''8    b''8    
g''8    e''8    \bar "|"   d''8    b'8    b'8    g''8    b'8    b'8    \bar "|" 
  a'8    b'8    a'8    a'4.    \bar ":|."   a''8    g''8    e''8    e''8    
d''8    b'8    \bar "|"     d''8    b'8    a'8    a'4    b'8    \bar "|"   d''8 
   b'8    b'8    g''8    b'8    b'8    \bar "|"   a'8    b'8    a'8    a'4    
a''8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
