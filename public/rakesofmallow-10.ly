\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "ceolachan"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Polka"
	source = "https://thesession.org/tunes/85#setting12599"
	arranger = "Setting 10"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/85#setting12599" {"https://thesession.org/tunes/85#setting12599"}}}
	title = "Rakes Of Mallow, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 2/4 \key g \major   \repeat volta 2 {   g'4    b'4    g'4    b'4    
\bar "|"   g'4    b'4    d''8    c''8    b'8    a'8    \bar "|"   fis'4    a'4  
  fis'4    a'4    \bar "|"   fis'4    a'4    c''4    a'4    \bar "|"   g'4    
b'4    g'4    b'4    \bar "|"   g'4    b'4    c''4    a'4    \bar "|"   b'4    
a'8    g'8    a'4    g'8    fis'8    \bar "|"   g'2    g'2    }   
\repeat volta 2 {   g''4    e''4    d''4    c''4    \bar "|"   b'4    c''4    
d''2    \bar "|"   g''4    e''4    d''4    c''4    \bar "|"   b'4    a''4    
a'2    \bar "|"   g''4    fis''8    e''8    d''4    c''4    \bar "|"   b'4    
a'8    g'8    c''2    \bar "|"   b'4    a'8    g'8    a'4    g'8    fis'8    
\bar "|"   g'2    g'2    }   \repeat volta 2 {   d'8    g'8    b'4    d'8    
g'8    b'4    \bar "|"   d'8    g'8    b'4    d''4    b'8    g'8    \bar "|"   
d'8    fis'8    a'4    d'8    fis'8    a'4    \bar "|"   d'8    fis'8    a'4    
c''4    a'8    fis'8    \bar "|"   d'8    g'16    a'16    b'4    d'8    g'16    
a'16    b'4    \bar "|"   d'8    g'16    a'16    b'8    g'8    d''4    c''8    
b'8    \bar "|"   e''8    c''8    d''8    b'8    a'8    c''8    b'8    a'8    
\bar "|"   g'2    g'2    }   \repeat volta 2 {   g''8    fis''8    g''8    e''8 
   e''8    d''8    d''8    c''8    \bar "|"   b'8    g'8    c''8    a'8    d''2 
   \bar "|"   g''8    b''8    a''8    g''8    fis''8    e''8    d''8    c''8    
\bar "|"   b'8    g'8    a''4    a'2    \bar "|"   \tuplet 3/2 {   b''8    a''8  
  g''8  }   \tuplet 3/2 {   a''8    g''8    fis''8  }   \tuplet 3/2 {   g''8    
fis''8    e''8  }   d''8    c''8    \bar "|"   \tuplet 3/2 {   b'8    a'8    g'8 
 }   \tuplet 3/2 {   c''8    b'8    a'8  }   d''2    \bar "|"   \tuplet 3/2 {   
e''8    d''8    c''8  }   d''8    b'8    a'8    c''8    b'8    a'8    \bar "|"  
 g'2    g'2    }   \repeat volta 2 {   g'8    b'8    g'8    b'8    \bar "|"   
g'8    b'8    d''16    c''16    b'16    a'16    \bar "|"   fis'8    a'8    
fis'8    a'8    \bar "|"   fis'8    a'8    c''8    a'8    \bar "|"   g'8    b'8 
   g'8    b'8    \bar "|"   g'8    b'8    c''8    a'8    \bar "|"   b'8    a'16 
   g'16    a'8    g'16    fis'16    \bar "|"   g'4    g'4    }   
\repeat volta 2 {   g''8    e''8    d''8    c''8    \bar "|"   b'8    c''8    
d''4    \bar "|"   g''8    e''8    d''8    c''8    \bar "|"   b'8    a''8    
a'4    \bar "|"   g''8    fis''16    e''16    d''8    c''8    \bar "|"   b'8    
a'16    g'16    c''4    \bar "|"   b'8    a'16    g'16    a'8    g'16    fis'16 
   \bar "|"   g'4    g'4    }   \repeat volta 2 {   d'16    g'16    b'8    d'16 
   g'16    b'8    \bar "|"   d'16    g'16    b'8    d''8    b'16    g'16    
\bar "|"   d'16    fis'16    a'8    d'16    fis'16    a'8    \bar "|"   d'16    
fis'16    a'8    c''8    a'16    fis'16    \bar "|"   d'16    g'32    a'32    
b'8    d'16    g'32    a'32    b'8    \bar "|"   d'16    g'32    a'32    b'16   
 g'16    d''8    c''16    b'16    \bar "|"   e''16    c''16    d''16    b'16    
a'16    c''16    b'16    a'16    \bar "|"   g'4    g'4    }   \repeat volta 2 { 
  g''16    fis''16    g''16    e''16    e''16    d''16    d''16    c''16    
\bar "|"   b'16    g'16    c''16    a'16    d''4    \bar "|"   g''16    b''16   
 a''16    g''16    fis''16    e''16    d''16    c''16    \bar "|"   b'16    
g'16    a''8    a'4    \bar "|"   \tuplet 3/2 {   b''16    a''16    g''16  }   
\tuplet 3/2 {   a''16    g''16    fis''16  }   \tuplet 3/2 {   g''16    fis''16   
 e''16  }   d''16    c''16    \bar "|"   \tuplet 3/2 {   b'16    a'16    g'16  } 
  \tuplet 3/2 {   c''16    b'16    a'16  }   d''4    \bar "|"   \tuplet 3/2 {   
e''16    d''16    c''16  }   d''16    b'16    a'16    c''16    b'16    a'16    
\bar "|"   g'4    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
