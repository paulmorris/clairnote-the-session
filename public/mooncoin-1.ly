\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Will Harmon"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/206#setting206"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/206#setting206" {"https://thesession.org/tunes/206#setting206"}}}
	title = "Mooncoin, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \mixolydian   e''8    d''8  \repeat volta 2 {   cis''8    b'8  
  a'8    a'8    e'8    a'8  \bar "|"   a'8    e'8    a'8    b'8    cis''8    
d''8  \bar "|"   cis''8    b'8    a'8    a'8    cis''8    e''8  \bar "|"   d''8 
   b'8    g'8    b'8    cis''8    d''8  \bar "|"     \bar "|"   cis''8    b'8   
 a'8    a'8    e'8    a'8  \bar "|"   a'8    e'8    a'8    b'8    cis''8    
d''8  \bar "|"   a'8    cis''8    e''8    g''4. ^"~"  \bar "|"   d''8    b'8    
g'8    b'8    cis''8    d''8  }     \repeat volta 2 {   cis''8    d''8    e''8  
  e''8    fis''8    g''8  \bar "|"   fis''16    g''16    a''8    fis''8    g''8 
   e''8    d''8  \bar "|"   cis''8    d''8    e''8    e''8    fis''8    g''8  
\bar "|"   fis''16    g''16    a''8    a'8    b'8    cis''8    d''8  \bar "|"   
  cis''8    d''8    e''8    e''8    fis''8    g''8  \bar "|"   a''8    fis''8   
 d''8    b''8    g''8    e''8  \bar "|"   a''8    fis''8    d''8    g''8    
e''8    cis''8  \bar "|"   d''8    b'8    g'8    b'8    cis''8    d''8  }     
\repeat volta 2 {   cis''8    b'8    a'8    a'8    a''8    fis''8  \bar "|"   
e''8    cis''8    a'8    b'8    cis''8    d''8  \bar "|"   cis''8    b'8    a'8 
   g''4. ^"~"  \bar "|"   d''8    b'8    g'8    b'8    cis''8    d''8  \bar "|" 
    \bar "|"   cis''8    b'8    a'8    a'8    a''8    a''8  \bar "|"   a'8    
g''8    g''8    a'8    fis''8    fis''8  \bar "|"   a'8    e''8    e''8    e''8 
   fis''8    g''8  \bar "|"   d''8    b'8    g'8    b'8    cis''8    d''8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
