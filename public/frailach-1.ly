\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "seara"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/445#setting445"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/445#setting445" {"https://thesession.org/tunes/445#setting445"}}}
	title = "Frailach"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key a \minor   dis'4  \bar "|"   e'8 ^"Am"   a'8    c''8    a'8    
a'8    c''8    e''8    c''8  \bar "|"   d''4 ^"Dm"   d''8    c''8    d''8    
e''8    c''4  \bar "|"   c''8 ^"C"   e''8    g''8    e''8    c''8    e''8    
g''8    e''8  \bar "|"   d''4 ^"G7"   d''8    c''8      d''8 ^"C"   e''8    
c''4  \bar "|"       c''8 ^"E7"   d''8    d''8    c''8    c''8    b'8    b'8    
bes'8  \bar "|"   a'2 ^"Am"   a'8    d''8    c''4  \bar "|"   c''8 ^"E7"   d''8 
   d''8    c''8    c''8    b'8    b'8    bes'8  \bar "|"   a'2. ^"Am" }     
\repeat volta 2 {   e''4    \bar "|"   a''2. ^"Am"   g''8    f''8  \bar "|"   
e''2.    e''4  \bar "|"   a''8    e''8    a''8    e''8    a''4  \grace {    
b''8    a''8  }   g''8    f''8  \bar "|"   e''2.    e''4  \bar "|"     a''2.    
g''4  \bar "|"   b''2.    a''4  \bar "|"   a''8    e''8    a''8    e''8      
a''4 ^"Dm" \grace {    b''8    a''8  }   g''8    f''8  \bar "|"   e''2 ^"Am"   
e''4  \bar "|"       c''8 ^"C"   d''8    e''8    f''8    e''8    f''8    e''8   
 f''8  \bar "|"   e''8    f''8    e''8    f''8      e''4 ^"Dm"   d''4  \bar "|" 
  d''8 ^"Dm"   a'8    a'8    d''8    d''8    a'8    a'8    d''8  \bar "|"   
d''8    a'8    a'8    d''8      d''4 ^"C" \grace {    e''8    d''8  }   c''4  
\bar "|"       c''8 ^"E7"   d''8    d''8    c''8    c''8    b'8    b'8    bes'8 
 \bar "|"   a'2 ^"Am"   a'8    d''8    c''4  \bar "|"   c''8 ^"E7"   d''8    
d''8    c''8    c''8    b'8    b'8    bes'8  } \alternative{{     a'4 ^"Am"   
a'4    c''4  } {     a'4 ^"Am"   r4   a''4  \bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
