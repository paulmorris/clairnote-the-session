\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/390#setting22906"
	arranger = "Setting 6"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/390#setting22906" {"https://thesession.org/tunes/390#setting22906"}}}
	title = "Famous Ballymote, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \mixolydian   \bar "|"   b'8    g'8    d''8    g'8    b'8    
d''8    d''8    e''8  \bar "|"   f''8    d''8    d''8    c''8    a'8    b'8    
c''8    a'8  \bar "|"   b'8    g'8    d''8    g'8    b'8    d''8    d''8    
e''8  \bar "|"   f''8    d''8    c''8    a'8    b'8    g'8    g'4  \bar "|"     
b'8    g'8    g'4    b'8    d''8    d''8    e''8  \bar "|"   f''8    d''8    
d''8    c''8    a'8    b'8    c''8    a'8  \bar "|"   b'8    g'8    d''8    g'8 
   b'8    d''8    d''8    e''8  \bar "|"   f''8    d''8    c''8    a'8    b'8   
 g'8    g'4  \bar "||"     \bar "|"   b'8    g'8    b'8    d''8    g''4    a''8 
   g''8  \bar "|"   f''8    d''8    d''8    c''8    a'8    b'8    c''8    a'8  
\bar "|"   b'8    g'8    b'8    d''8    g''4    a''8    g''8  \bar "|"   f''8   
 d''8    c''8    a'8    b'8    g'8    d''8    g'8  \bar "|"     b'8    g'8    
b'8    d''8    g''4    a''8    g''8  \bar "|"   f''8    d''8    d''8    c''8    
a'8    b'8    c''8    a'8  \bar "|"   b'8    g'8    g'4    g''4    a''8    g''8 
 \bar "|"   f''8    d''8    c''8    a'8    b'!2  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
