\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Liz Merton"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/959#setting959"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/959#setting959" {"https://thesession.org/tunes/959#setting959"}}}
	title = "Camping With Roosters"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   cis''8    e''8    e''8    d''8    
e''8    e''8    cis''8    a'8  \bar "|"   b'8    a'8    a'8    gis'8    a'8    
b'8    cis''8    d''8  \bar "|"   cis''8    e''8    e''8    d''8    e''8    
e''8    cis''8    a'8  \bar "|"   b'8    a'8    a'8    gis'8    a'4    e'4  
\bar "|"     cis''8    e''8    e''8    d''8    e''8    e''8    cis''8    a'8  
\bar "|"   b'8    a'8    a'8    gis'8    a'8    b'8    cis''8    d''8  \bar "|" 
  e''8    a''8    gis''8    a''8    fis''8    e''8    d''8    cis''8  \bar "|"  
 b'8    a'8    a'8    gis'8    a'2  }     \repeat volta 2 {   e''8 
^"Cock a doodle doo!"   a''8    a''8    gis''8    a''4    e''4  \bar "|"   c''8 
   b'8    a'8    c''8    b'8    a'8    b'4  \bar "|"   c''4    c''8    a'8    
e'8    a'8    c''4  \bar "|"   b'8    a'8    a'8    gis'8    a'2  \bar "|"      
 e''8 ^"Cock a doodle doo!"   a''8    a''8    gis''8    a''4    e''4  \bar "|"  
 c''8    b'8    a'8    c''8    b'8    a'8    b'4  \bar "|"   e''8    a''8    
gis''8    a''8    fis''8    e''8    d''8    cis''8  \bar "|"   b'8    a'8    
a'8    gis'8    a'2  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
