\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Mix O'Lydian"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Hornpipe"
	source = "https://thesession.org/tunes/30#setting26562"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/30#setting26562" {"https://thesession.org/tunes/30#setting26562"}}}
	title = "Off To California"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key g \major   \repeat volta 2 {   \tuplet 3/2 {   d'8    e'8    
fis'8  }   \bar "|"   g'8.    fis'16    g'8.    b'16    a'8.    g'16    e'8.    
d'16    \bar "|"   g'8.    b'16    d''8.    g''16    e''4    \tuplet 3/2 {   
d''8    e''8    fis''8  }   \bar "|"   g''8.    fis''16    g''8.    d''16    
e''8.    d''16    b'8.    g'16    \bar "|"   a'8.    b'16    a'8.    g'16    
e'4    \tuplet 3/2 {   d'8    e'8    fis'8  }   \bar "|"     g'8.    fis'16    
g'8.    b'16    a'8.    g'16    e'8.    d'16    \bar "|"   g'8.    b'16    
d''8.    g''16    e''4    \tuplet 3/2 {   d''8    e''8    fis''8  }   \bar "|"   
g''8.    fis''16    g''8.    d''16    e''8.    d''16    b'8.    g'16    
\bar "|"   a'8.    g'16    e'8.    fis'16    g'4    }     \repeat volta 2 {   
\tuplet 3/2 {   d''8    e''8    fis''8  }   \bar "|"   g''8.    fis''16    e''8. 
   g''16    fis''8.    e''16    d''8.    fis''16    \bar "|"   e''8.    d''16   
 e''8.    fis''16    e''8.    d''16    b'8.    d''16    \bar "|"   g''8.    
fis''16    g''8.    d''16    e''8.    d''16    b'8.    g'16    \bar "|"   a'8.  
  b'16    a'8.    g'16    e'4    \tuplet 3/2 {   d'8    e'8    fis'8  }   
\bar "|"     g'8.    fis'16    g'8.    b'16    a'8.    g'16    e'8.    d'16    
\bar "|"   g'8.    b'16    d''8.    g''16    e''4    \tuplet 3/2 {   d''8    
e''8    fis''8  }   \bar "|"   g''8.    fis''16    g''8.    d''16    e''8.    
d''16    b'8.    g'16    \bar "|"   \tuplet 3/2 {   a'8    b'8    a'8  }   e'8.  
  fis'16    g'4    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
