\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "brotherstorm"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/87#setting12613"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/87#setting12613" {"https://thesession.org/tunes/87#setting12613"}}}
	title = "Rolling In The Ryegrass"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   a'4. ^"~"    fis'8 ^"1st rnd"   d''8    fis'8    a'8  
  fis'8  \bar "|"   g'4 -.   b'8    g'8    d''8    g'8    b'8    g'8  \bar "|"  
 fis'8    a'8    a'8    fis'8    d''8    fis'8    a'8    fis'8  \bar "|"   g'8  
  b'8    a'8    fis'8    e'8    fis'8    d'4 -. \bar "|"   a'8    b'8    a'8    
fis'8    d'8    fis'8    a'8    fis'8  \bar "|"   g'4 -.   b'8    g'8    d'8    
g'8    b'8    g'8  \bar "|"   fis'8    a'8    a'8    fis'8    d'4 -.   a'8    
fis'8  \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8    d'4 -. \bar "|" 
  a'8    b'8    d''8    e''8    fis''4. ^"~"    a''8  \bar "|"   g''4. ^"~"    
e''8  \grace {    g''8  }   fis''8    e''8    d''8    b'8  \bar "|"   a'8    
b'8    d''8    e''8    fis''4 -.   d''8    fis''8  \bar "|"   a''8    fis''8    
d''8    fis''8    e''8    fis''8    d''4  \bar "|"   a'8    b'8    d''8    e''8 
   fis''4. ^"~"    a''8  \bar "|"   g''4 -.   g''8    e''8  \grace {    g''8  } 
  fis''8    e''8    d''8    b'8  \bar "|"   a'8    b'8    d''8    e''8    
fis''4 -.   d''8    fis''8  \bar "|"   a''8    fis''8    d''8    fis''8    e''8 
   fis''8    d''4  \bar "|"   a'4. ^"~"    fis'8 ^"2nd round"   d'8    fis'8    
a'8    fis'8  \bar "|"   g'4 -.   b'8    g'8    d'8    g'8    b'8    g'8  
\bar "|"   fis'8    a'8    a'8    fis'8    d'4 -.   a'8    fis'8  \bar "|"   
g'8    b'8    a'8    fis'8    e'8    fis'8    d'4 -. \bar "|"   fis'8    a'8    
a'8    fis'8    d'8    fis'8    a'8    fis'8  \bar "|"   g'8    a'8    a'8    
g'8    d'4    b'8    g'8  \bar "|"   fis'8    a'8    a'8    fis'8    d'4 -.   
a'8    fis'8  \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8    d'4  
\bar "|"   a'8    b'8    d''8    e''8    fis''4. ^"~"    a''8  \bar "|"   g''4. 
^"~"    e''8  \grace {    g''8  }   fis''8    e''8    d''8    b'8  \bar "|"   
a'8    b'8    d''8    e''8    fis''4 -.   d''8    fis''8  \bar "|"   a''8    
fis''8    d''8    fis''8    e''8    fis''8    d''4 -. \bar "|"   a'8    b'8    
d''8    e''8    fis''4. ^"~"    d''8  \bar "|"   g''4 -.   g''8    e''8    
fis''8    e''8    d''8    b'8  \bar "|"   a'8    b'8    d''8    e''8    fis''4 
-.   d''8    fis''8  \bar "|"   a''8    fis''8    d''8    fis''8    e''8    
fis''8    d''4 -. \bar "|"     a'8 ^"3rd round"   b'8    a'8    fis'8    d'8    
fis'8    a'8    fis'8  \bar "|"   g'8    a'8    b'8    g'8    d'8    g'8    b'8 
   g'8  \bar "|"   fis'8    a'8    a'8    fis'8    d'8    fis'8    a'8    fis'8 
 \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8    d'4 -. \bar "|"   
a'4. ^"~"    fis'8    d'8    fis'8    a'8    fis'8  \bar "|"   g'8 -.   b''4. 
^"~"    a''4 -.   r8 g'8  \bar "|"   fis'8    a'8    a'8    fis'8    d'8    
fis'8    a'8    fis'8  \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8    
d'4  \bar "|"   a'8    b'8    d''8    e''8    fis''4 -.   fis''8    a''8  
\bar "|"   g''4. ^"~"    e''8  \grace {    g''8  }   fis''8    e''8    d''8    
b'8  \bar "|"   a'8    b'8    d''8    e''8    fis''4 -.   d''8    fis''8  
\bar "|"   a''8    fis''8    d''8    fis''8    e''8    fis''8    d''4  \bar "|" 
  a'8    b'8    d''8    e''8    fis''4. ^"~"    d''8  \bar "|"   g''4 -.   g''8 
   e''8  \grace {    g''8  }   fis''8    e''8    d''8    b'8  \bar "|"   a'8    
b'8    d''8    e''8    fis''4 -.   d''8    fis''8  \bar "|"   a''8    fis''8    
d''8    fis''8    e''8    fis''8    d''4  \bar "|"   a'4. ^"~"    fis'8 
^"4th round"   d'8    fis'8    a'8    fis'8  \bar "|"   g'4 -.   b'8    g'8    
d'8    g'8    b'8    g'8  \bar "|"   fis'8    a'8    a'8    fis'8    d'8    
fis'8    a'8    fis'8  \bar "|"   g'8    b'8    a'8    fis'8    e'8    fis'8    
d'4 -. \bar "|"   a'8    b'8    a'8    fis'8    d'8    fis'8    a'8    fis'8  
\bar "|"   g'8    b'4. ^"~"    b'4 -.   b'8    g'8  \bar "|"   d'8    fis'8    
a'8    fis'8    d'8    fis'8    a'8    fis'8  \bar "|"   g'8    b'8    a'8    
fis'8    e'8    fis'8    d'4 -. \bar "|"   a'8    b'8    d''8    e''8    
fis''4. ^"~"    a''8  \bar "|"   g''4. ^"~"    e''8  \grace {    g''8  }   
fis''8    e''8    d''8    b'8  \bar "|"   a'8    b'8    d''8    e''8    fis''4 
-.   d''8    fis''8  \bar "|"   a''8    fis''8    d''8    fis''8    e''8    
fis''8    d''4 -. \bar "|"   a'8    b'8    d''8    e''8    fis''4 -.   fis''8   
 a''8  \bar "|"   g''4. ^"~"    e''8  \grace {    g''8  }   fis''8    e''8    
d''8    b'8  \bar "|"   a'8    b'8    d''8    e''8    fis''4 -.   d''8    
fis''8  \bar "|"   a''8    fis''8    d''8    fis''8    e''8    fis''8    d''4  
\bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
