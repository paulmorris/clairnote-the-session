\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Tate"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/75#setting26249"
	arranger = "Setting 20"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/75#setting26249" {"https://thesession.org/tunes/75#setting26249"}}}
	title = "Miss McLeod's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key a \major   \repeat volta 2 {   a'4 ^"A"   a''4    fis''8    e''8 
   fis''8    a''8  \bar "|"   cis''4    cis''8 (   b'8  -)   cis''8    d''8    
cis''8    b'8  \bar "|"   a'4    a''4    fis''8    e''8    fis''8    a''8  
\bar "|"   b'4 ^"E"   b'8 (   a'8  -)   b'8    d''8    cis''8    b'8  \bar "|"  
     a'4 ^"A"   a''4    fis''8    e''8    fis''8    a''8  \bar "|"   cis''4    
cis''8 (   b'8  -)   cis''4    cis''8 (   e''8  -) \bar "|"   fis''8 ^"D"   
e''8    fis''8    gis''8    a''8    gis''8    fis''8    e''8  \bar "|"   
fis''16 ^"E"(   gis''16    a''8  -)   e''8 (   cis''8  -)   b'8    d''8    
cis''8    b'8  }     \repeat volta 2 {   a'4 ^"A"   cis''8 (   a'8  -)   e''8   
 a'8    cis''8    a'8  \bar "|"   cis''4    cis''8 (   b'8  -)   cis''8    d''8 
   cis''8    b'8  \bar "|"   a'4    cis''8 (   a'8  -)   e''8    a'8    cis''8  
  a'8  \bar "|"   b'4 ^"E"   b'8 (   a'8  -)   b'8    d''8    cis''8    b'8  
\bar "|"       a'4 ^"A"   cis''8 (   a'8  -)   e''8    a'8    cis''8    a'8  
\bar "|"   cis''4    cis''8 (   b'8  -)   cis''4    cis''8 (   e''8  -) 
\bar "|"   fis''8 ^"D"   e''8    fis''8    gis''8    a''8    gis''8    fis''8   
 e''8  \bar "|"   fis''16 ^"E"(   gis''16    a''8  -)   e''8 (   cis''8  -)   
b'8    d''8    cis''8    b'8  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
