\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jakep"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slide"
	source = "https://thesession.org/tunes/23#setting12402"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/23#setting12402" {"https://thesession.org/tunes/23#setting12402"}}}
	title = "Dingle Regatta, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 12/8 \key d \major   \repeat volta 2 {   g''8    b''8    g''8    d''8    
g''8    d''8    \bar "|"   b'8    d''8    b'8    g'8    b'8    g'8    \bar "|"  
 fis'8    a'8    fis'8    d'8    e'8    fis'8    \bar "|"   g'8    a'8    bes'8 
   b'!8    d''8    e''8    \bar "|"   g''8    b''8    g''8    d''8    g''8    
d''8    \bar "|"   b'8    d''8    b'8    g'8    b'8    g'8    \bar "|"   fis'8  
  a'8    fis'8    d'8    e'8    fis'8    \bar "|"   g'4.    g'4.    }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
