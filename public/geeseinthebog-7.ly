\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "jakethepeg"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/43#setting12466"
	arranger = "Setting 7"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/43#setting12466" {"https://thesession.org/tunes/43#setting12466"}}}
	title = "Geese In The Bog, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key a \minor   \repeat volta 2 {   g''8  \bar "|"   g''8    b'8    
b'8    d''8    b'8    b'8  \bar "|"   g''8    b'8    b'8    d''8    e''8    
f''8  \bar "|"   g''8    b'8    b'8    d''8    a'4  \bar "|"   b'8    e''8    
e''8    e''4    f''8  \bar "|"     \bar "|"   g''8    b'8    b'8    d''8    b'8 
   b'8  \bar "|"   g''8    b'8    b'8    d''8    e''8    f''8  \bar "|"   g''8  
  f''8    e''8    d''8    a'4  \bar "|"   b'8    e''8    e''8    e''4  }     
\repeat volta 2 {   d''8  \bar "|"   e''4    a''8    e''8    g''8    g''8  
\bar "|"   e''4    a''8    g''8    e''8    d''8  \bar "|"   e''4    a''8    
e''8    g''8    g''8  \bar "|"   e''8    a''8    a''8    a''4    d''8  \bar "|" 
    \bar "|"   e''4    a''8    e''8    g''8    g''8  \bar "|"   e''4    a''8    
g''8    e''8    d''8  \bar "|"   g''8    f''8    e''8    d''8    a'4  \bar "|"  
 b'8    e''8    e''8    e''4  }     \repeat volta 2 {   g''8  \bar "|"   g''8   
 b'8    b'8    b'8    b'8    b'8  \bar "|"   d''8    b'8    b'8    b'8    b'8   
 b'8  \bar "|"   g''8    b'8    b'8    d''8    a'4  \bar "|"   b'8    e''8    
e''8    e''4    f''8  \bar "|"     \bar "|"   g''8    b'8    b'8    b'8    b'8  
  b'8  \bar "|"   d''8    b'8    b'8    b'8    b'8    b'8  \bar "|"   g''8    
f''8    e''8    d''8    a'4  \bar "|"   b'8    e''8    e''8    e''4  }     
\repeat volta 2 {   d''8  \bar "|"   e''8    a''8    a''8    e''8    g''8    
g''8  \bar "|"   e''8    a''8    a''8    g''8    e''8    d''8  \bar "|"   e''8  
  a''8    a''8    e''8    g''8    g''8  \bar "|"   e''8    a''8    a''8    a''4 
   d''8  \bar "|"     \bar "|"   e''8    a''8    a''8    e''8    g''8    g''8  
\bar "|"   e''8    a''8    a''8    g''8    e''8    d''8  \bar "|"   g''8    
f''8    e''8    d''8    a'4  \bar "|"   b'8    e''8    e''8    e''4  }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
