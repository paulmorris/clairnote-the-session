\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Slip Jig"
	source = "https://thesession.org/tunes/48#setting25766"
	arranger = "Setting 5"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/48#setting25766" {"https://thesession.org/tunes/48#setting25766"}}}
	title = "Hardiman The Fiddler"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 9/8 \key a \dorian   \repeat volta 2 {   a'4    g'8    fis'8    d'8    
e'8    fis'4    g'8  \bar "|"   a'8    c''16    a'16    a'8    a'8    g'8    
a'8    c''8    a'8    g'8  \bar "|"   a'8    c''16    a'16    g'8    fis'8    
d'8    e'8    fis'4    g'8  \bar "|"   a'8    d''8    d''8    d''8    e''8    
d''8    c''8    a'8    g'8  \bar "|"     a'8    c''16    a'16    g'8    fis'8   
 d'8    e'8    fis'4    g'8  \bar "|"   a'8    c''16    a'16    a'8    a'8    
d'8    a'8    c''8    a'8    g'8  \bar "|"   a'8    c''16    a'16    g'8    
fis'8    d'8    e'8    fis'4    g'8  \bar "|"   a'8    d''8    d''8    d''8    
e''8    d''8    d''8    e''8    f''8  \bar "||"     \bar "|"   a'8    d''8    
d''8    d''4    e''8    f''4.  \bar "|"   a'8    d''8    d''8    d''8    e''8   
 d''8    c''8    a'8    g'8  \bar "|"   a'8    d''8    d''8    d''4    e''8    
fis''!4    g''8  \bar "|"   a''8    g''8    fis''8    g''8    e''8    d''8    
c''8    a'8    g'8  \bar "|"     a'8    d''8    d''8    d''4    e''8    f''4    
e''8  \bar "|"   a'8    d''8    d''8    d''8    e''8    d''8    c''8    a'8    
g'8  \bar "|"   d''8    c''8    a'8    b'16    c''16    d''8    e''8    fis''4  
  g''8  \bar "|"   a''8    g''8    fis''8    g''8    e''8    d''8    c''8    
a'8    g'8  \bar "||"   }
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
