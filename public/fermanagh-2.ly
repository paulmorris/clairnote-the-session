\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "gian marco"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/654#setting28533"
	arranger = "Setting 2"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/654#setting28533" {"https://thesession.org/tunes/654#setting28533"}}}
	title = "Fermanagh, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   d''8    b'8  \repeat volta 2 {   a'4    fis'8    d'8  
  a'8    d''8    d''8    b'8  \bar "|"   a'4    fis'8    d'8    e'8    fis'8    
g'8    b'8  \bar "|"     \bar "|"   a'4    fis'8    a'8    a'4    fis'8    a'8  
} \alternative{{   b'8    g'8    e'8    g'8    fis'8    d'8    d''8    b'8  } { 
  b'8    g'8    e'8    g'8    fis'8    d'8    d'4  \bar "||"     \bar "|"   
d''8    a'8    fis'8    a'8    fis''8    fis''8    fis''8    e''8  \bar "|"   
d''8    a'8    fis'8    a'8    b'8    e''8    e''8    cis''8  \bar "|"   d''8   
 a'8    fis'8    a'8    fis''8    fis''8    fis''8    a'8  \bar "|"     
\bar "|"   g'8    fis'8    e'8    fis'8    g'8    a'8    b'8    cis''8  
\bar "|"   d''8    a'8    fis'8    a'8    fis''8    fis''8    fis''8    e''8  
\bar "|"   d''8    a'8    fis'8    a'8    b'8    e''8    e''8    cis''8  
\bar "|"     \bar "|"   d''4    cis''8    d''8    b'8    cis''8    a'4  
\bar "|" \tuplet 3/2 {   b'8    cis''8    d''8  }   e''8    cis''8    d''4  
\bar "||"   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
