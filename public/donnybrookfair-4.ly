\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Thistledowne"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/26#setting28093"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/26#setting28093" {"https://thesession.org/tunes/26#setting28093"}}}
	title = "Donnybrook Fair"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {     g'8 ^"G"   fis'8    g'8      
a'8 ^"D"   g'8    a'8    \bar "|"     b'8 ^"Em"   e''8    e''8    d''8    b'8   
 a'8    \bar "|"   b'8    a'8    b'8    g'8    a'8    b'8    \bar "|"     a'8 
^"Am"   g'8    e'8    d'8    e'8    d'8    \bar "|"     g'4 ^"G"   g'8      a'4 
^"D"   a'8    \bar "|"     \bar "|"     b'8 ^"Em"   e''8    e''8    d''8    b'8 
   a'8    \bar "|"   b'4    b'8      g'8 ^"C"   a'8    b'8    \bar "|"     a'8 
^"D"   g'8    fis'8      g'4. ^"G"   }     g''8 ^"Em"   fis''8    e''8    
fis''8    e''8    d''8    \bar "|"   e''8    fis''8    e''8    d''8    b'8    
a'8    \bar "|"     \bar "|"   b'8    e''8    e''8    d''8    b'8    a'8    
\bar "|"   b'8    e''8    e''8    e''4    fis''8    \bar "|"   g''8    fis''8   
 e''8    fis''8    e''8    d''8    \bar "|"   e''8    fis''8    e''8    d''8    
b'8    a'8    \bar "|"     b'8 ^"Am"   a'8    b'8    g'8    a'8    b'8    
\bar "|"     \bar "|"     a'8 ^"D7"   g'8    fis'8      g'4 ^"G"   fis''8    
\bar ":|."     g''8 ^"Em"   fis''8    g''8      a''8 ^"D"   g''8    a''8    
\bar "|"     b''8 ^"Em"   g''8    e''8    d''8    b'8    a'8    \bar "|"     
b'4 ^"Am"   b'8    g'8    a'8    b'8    \bar "|"     a'8 ^"D7"   g'8    fis'8   
   g'4. ^"G"   \bar "|."   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
