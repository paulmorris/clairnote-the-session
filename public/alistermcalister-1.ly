\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dafydd Monks"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Strathspey"
	source = "https://thesession.org/tunes/1315#setting1315"
	arranger = "Setting 1"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1315#setting1315" {"https://thesession.org/tunes/1315#setting1315"}}}
	title = "Alister McAlister"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key b \minor   d''16    b'8.    b'8.    a'16    fis'16    b'8.    
b'8.    cis''16    \bar "|"   d''16    b'8.    b'8.    cis''16    fis''4    
fis''8.    e''16    \bar "|"   d''16    b'8.    b'8.    a'16    fis'16    b'8.  
  b'8.    d''16    \bar "|"   cis''16    a'8.    a'8.    cis''16    e''4     ~  
  e''8    }     cis''8    \bar "|"   d''8.    e''16    fis''8.    g''16    
a''8.    b''16    a''8.    fis''16    \bar "|"   d''8.    e''16    fis''8.    
g''16    a''4    a''8.    fis''16    \bar "|"   d''8.    e''16    fis''8.    
g''16    a''8.    b''16    a''8.    fis''16    \bar "|"   cis''16    a'8.    
a'8.    cis''16    e''4    e''8.    cis''16    \bar "|"     d''8.    e''16    
fis''8.    g''16    a''8.    b''16    a''8.    fis''16    \bar "|"   d''8.    
e''16    fis''8.    g''16    a''4    a''8.    b''16    \bar "|"   c'''8.    
a''16    b''8.    g''16    a''8.    fis''16    g''8.    e''16    \bar "|"   
cis''16    a'8.    a'8.    cis''16    e''4     ~    e''8    \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
