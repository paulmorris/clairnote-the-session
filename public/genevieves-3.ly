\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Ian Varley"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Waltz"
	source = "https://thesession.org/tunes/135#setting27878"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/135#setting27878" {"https://thesession.org/tunes/135#setting27878"}}}
	title = "Genevieve's"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 3/4 \key g \major   d'8  \repeat volta 2 {   b'8    c''8    b'8    a'8    
g'8    fis'8    \bar "|"   g'8    a'8    b'8    d'4    d'8    \bar "|"   e'8    
g'8    c''8    b'8    d'8    b'8    \bar "|"   b'8    a'8    g'8    a'4    d'8  
    \bar "|"   b'8    c''8    b'8    a'8    g'8    fis'8    \bar "|"   g'8    
a'8    b'8    d'4    d'8    \bar "|"   e'8    g'8    c''8    b'8    d'8    b'8  
  } \alternative{{   a'8    g'8    fis'8    g'4    d'8    } {   a'8    g'8    
fis'8    g'4    d''8    \bar "||"     \bar "|"   e''4    e''8    e''8    fis''8 
   g''8    \bar "|"   d''4    c''8    b'8    a'8    g'8    \bar "|"   c''8    
d''8    c''8    b'8    a'8    g'8    \bar "|"   e'8    fis'8    g'8    a'4    
d''8      \bar "|"   e''4    e''8    e''8    fis''8    g''8    \bar "|"   d''4  
  c''8    b'8    a'8    g'8    \bar "|"   c''8    d''8    c''8    b'8    a'8    
g'8    \bar "|"   e'8    g'8    fis'8    g'4    d''8      \bar "|"   e''4    
e''8    e''8    fis''8    g''8    \bar "|"   d''4    c''8    b'8    a'8    g'8  
  \bar "|"   c''8    e''16    fis''16    g''8    d''8    b'8    g'8    \bar "|" 
  e'8    fis'8    g'8    a'4    d'8      \bar "|"   b'8    c''8    b'8    a'8   
 g'8    fis'8    \bar "|"   g'8    a'8    b'8    d'4    d'8    \bar "|"   e'8   
 g'8    c''8    b'8    d'8    b'8    \bar "|"   a'8    g'8    fis'8    g'4.    
\bar "|."   }}
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
