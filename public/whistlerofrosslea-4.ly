\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Dr. Dow"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/304#setting23075"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/304#setting23075" {"https://thesession.org/tunes/304#setting23075"}}}
	title = "Whistler Of Rosslea, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 4/4 \key c \dorian   c'8    d'8    ees'8    f'8    g'4    f'8    d'8  
\bar "|"   g'8    d'8    f'8    d'8    ees'8    c'8    c'4 ^"~"  \bar "|"   
bes8    c'8    d'8    ees'8    f'4 ^"~"    ees'8    f'8  \bar "|"   g'8    
aes'8    f'8    g'8    ees'8    f'8    d'8    ees'8  \bar "|"     c'8    d'8    
ees'8    c'8    d'8    ees'8    f'8    d'8  \bar "|"   g'8    a'8    bes'8    
g'8    c''8    bes'8    g'8    f'8  \bar "|"   ees'8    f'8    f'16    f'16    
ees'8    f'8    aes'8    d'8    f'8  \bar "|"   ees'8    c'8    d'8    bes8    
c'2  }     \repeat volta 2 {   c''4    g'8    c''8    ees'8    c''8    g'8    
c''8  \bar "|"   c''4 ^"~"    d''8    c''8    c''8    bes'8    g'8    a'8  
\bar "|"   bes'4 ^"~"    f'8    bes'8    d'8    bes'8    f'8    bes'8  \bar "|" 
  bes'8    a'8    g'8    f'8    ees'8    c'8    bes8    d'8  \bar "|"     c'8   
 c''8    c''8    bes'8    c''8    bes'8    g'8    f'8  \bar "|"   ees'8    f'8  
  g'8    a'8    bes'4.    c''8  \bar "|"   c''8    bes'8    g'8    f'8    ees'8 
   f'8    f'16    f'16    ees'8  \bar "|"   f'8    bes8    d'8    bes8    c'2  
}   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
