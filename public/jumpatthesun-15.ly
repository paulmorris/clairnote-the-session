\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/736#setting23656"
	arranger = "Setting 15"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/736#setting23656" {"https://thesession.org/tunes/736#setting23656"}}}
	title = "Jump At The Sun"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key e \minor   \repeat volta 2 {   e'8 ^"Em"   g'8    b'8    ais'4   
 b'8  \bar "|"   e'8    g'8    b'8    ais'4    b'8  \bar "|"   e''8    b'8    
b'8    e''8    b'8    b'8  \bar "|"   b'8    a'8    g'8    fis'4. ^"B" \bar "|" 
      e'8 ^"E"   g'8    b'8    ais'4    b'8  \bar "|"   e'8    g'8    b'8    
ais'4    b'8  \bar "|"   e''8    b'8    b'8      c''8 ^"Am"   b'8    a'8  
\bar "|"   g'8 ^"B7"   a'8    fis'8      e'4 ^"Em" }     b'8  \repeat volta 2 { 
  e''8 ^"Em"   b'8    b'8    e''8    fis''8    g''8  \bar "|"   fis''8 ^"B7"   
b'8    b'8    b''8    a''8    g''8  \bar "|"   fis''8 ^"Em"   b'8    b'8    
g''8    fis''8    e''8  \bar "|"   fis''8 ^"B"   d''8    b'8      c''4 ^"B7"   
b'8  \bar "|"       e''8 ^"Em"   b'8    b'8    e''8    fis''8    g''8  \bar "|" 
  fis''8 ^"B7"   b'8    b'8    b''8    a''8    g''8  \bar "|"   fis''8 ^"Em"   
b'8    b'8      c''8 ^"Am"   b'8    a'8  \bar "|"   g'8 ^"B7"   a'8    fis'8    
  e'4 ^"Em" }   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
