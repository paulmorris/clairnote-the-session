\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Bannerman"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1390#setting14754"
	arranger = "Setting 3"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1390#setting14754" {"https://thesession.org/tunes/1390#setting14754"}}}
	title = "Cliffs Of Moher, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\repeat volta 2 {
\time 6/8 \key e \dorian   a''8    gis''8    a''8    b''8    a''8    g''!8  
\bar "|"   e''8    a''8    fis''8    g''8    e''8    d''8  \bar "|"   cis''4    
a'8    b'8    a'8    g'8  \bar "|"   e'8    fis'8    g'8    a'8    b'8    d''8  
\bar "|"   e''8    a''8    a''8    b''8    a''8    g''8  \bar "|"   e''8    
a''8    fis''8    g''8    e''8    d''8  \bar "|"   cis''4    a'8    b'8    a'8  
  g'8  \bar "|"   e'8    fis'8    g'8    a'4.  }   e''8    fis''8    e''8    
d''8    b'8    a'8  \bar "|"   e''8    fis''8    e''8    d''8    b'8    a'8  
\bar "|"   g'8    a'8    b'8    d''8    b'8    a'8  \bar "|"   g'8    a'8    
b'8    d''8    b'8    d''8  \bar "|"   e''8    fis''8    e''8    d''8    b'8    
a'8  \bar "|"   e''8    fis''8    e''8    d''8    b'8    a'8  \bar "|"   g'8    
a'8    b'8    d''8    b'8    g'8  \bar "|"   e'8    fis'8    g'8    a'4.  
\bar ":|."   e''8    fis''8    e''8    d''8    e''8    e''8  \bar "|"   cis''8  
  e''8    e''8    b'8    e''8    e''8  \bar "|"   e'8    fis'8    g'8    b'8    
a'8    g'8  \bar "|"   e'8    d'8    b8    a4.  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
