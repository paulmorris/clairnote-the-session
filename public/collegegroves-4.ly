\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "JACKB"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/1272#setting22924"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1272#setting22924" {"https://thesession.org/tunes/1272#setting22924"}}}
	title = "College Groves, The"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key d \major   \bar "|"   d'4    \tuplet 3/2 {   fis'8    e'8    d'8  
}   a'8    d'8    fis'8    d'8  \bar "|"   e'4    c''8    e'8    d''8    e'8    
c''8    e'8  \bar "|"   d'4    fis'8    a'8    d''8    fis''8    e''8    d''8  
\bar "|"   cis''8    a'8    g'8    e'8    e'8    d'8    d'4  \bar "|"     d'4   
 \tuplet 3/2 {   fis'8    e'8    d'8  }   a'8    d'8    fis'8    d'8  \bar "|"   
e'4    c''8    e'8    d''8    e'8    c''8    e'8  \bar "|"   d'4    fis'8    
a'8    d''8    fis''8    e''8    d''8  \bar "|"   cis''8    a'8    g'8    e'8   
 e'8    d'8    d'4  \bar "||"     \bar "|"   fis''8    d''8    d''4    fis''8   
 a''8    g''8    fis''8  \bar "|"   e''8    c''8    c''4    e''8    fis''8    
g''8    e''8  \bar "|"   fis''8    d''8    d''4    d''8    fis''8    e''8    
d''8  \bar "|"   cis''8    a'8    g'8    e'8    e'8    d'8    d'4  \bar "|"     
fis''8    d''8    d''4    fis''8    a''8    g''8    fis''8  \bar "|"   e''8    
c''8    c''4    e''8    fis''8    g''8    e''8  \bar "|"   d''8    fis''8    
e''8    g''8    \tuplet 3/2 {   fis''8    g''8    a''8  }   g''8    b''8  
\bar "|"   a''8    fis''8    g''8    e''8    fis''8    d''8    d''4  \bar "||"  
   \bar "|"   fis''8    a''8    a''8    g''8    fis''8    d''8    d''8    
fis''8  \bar "|"   e''8    fis''8    g''8    fis''8    e''8    c''8    c''4  
\bar "|"   fis''8    a''8    a''8    g''8    fis''8    d''8    e''8    d''8  
\bar "|"   cis''8    a'8    g'8    e'8    e'8    d'8    d'4  \bar "|"     
fis''8    a''8    a''8    g''8    fis''8    d''8    d''8    fis''8  \bar "|"   
e''8    fis''8    g''8    fis''8    e''8    c''8    c''4  \bar "|"   d''8    
fis''8    e''8    g''8    \tuplet 3/2 {   fis''8    g''8    a''8  }   g''8    
b''8  \bar "|"   a''8    fis''8    g''8    e''8    fis''8    d''8    d''4  
\bar "||"     \bar "|"   fis''4    d''8    fis''8    fis''4    d''8    fis''8  
\bar "|"   e''4    c''8    e''8    e''4    c''8    e''8  \bar "|"   fis''4    
d''8    fis''8    fis''4    e''8    d''8  \bar "|"   cis''8    a'8    g'8    
e'8    e'8    d'8    d'4  \bar "|"     fis''4    d''8    fis''8    fis''4    
d''8    fis''8  \bar "|"   e''4    c''8    e''8    e''4    c''8    e''8  
\bar "|"   d''8    fis''8    e''8    g''8    \tuplet 3/2 {   fis''8    g''8    
a''8  }   g''8    b''8  \bar "|"   a''8    fis''8    g''8    e''8    fis''8    
d''8    d''4  \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
