\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "52Paddy"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Jig"
	source = "https://thesession.org/tunes/1200#setting14488"
	arranger = "Setting 4"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/1200#setting14488" {"https://thesession.org/tunes/1200#setting14488"}}}
	title = "Shandon Bells"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 6/8 \key g \major   \repeat volta 2 {   d'8    b8    g8    g8    b8    
d'8  \bar "|"   g'8    a'8    g'8    g'8    e'8    d'8  \bar "|"   e'8    c'8   
 a8    a8    b8    d'8  \bar "|"   e'4    d'8    e'8    fis'8    g'8  \bar "|"  
 d'8    b8    g8    g8    b8    d'8  \bar "|"   g'8    a'8    g'8    g'8    e'8 
   d'8  \bar "|"   e'8    fis'8    g'8    a'8    fis'8    d'8  } \alternative{{ 
  g'4.    g'8    fis'8    e'8  } {   g'4.    g'4    a'8  } }    \bar "|"   b'4  
  g'8    g'8    fis'8    g'8  \bar "|"   b'4    d''8    d''8    b'8    g'8  
\bar "|"   fis'8    d'8    d'8    a'8    d'8    d'8  \bar "|"   fis'8    e'8    
fis'8    a'8    b'8    c''8  \bar "|"   b'4    g'8    g'8    fis'8    g'8  
\bar "|"   b'8    d''8    d''8    d''8    b'8    g'8  \bar "|"   e'8    fis'8   
 g'8    a'8    fis'8    d'8  \bar "|"   g'4.    g'4    a'8  \bar "|"   \bar "|" 
  b'4    g'8    g'8    fis'8    g'8  \bar "|"   b'4    d''8    d''8    b'8    
g'8  \bar "|"   fis'4    d'8    a'8    d'8    d'8  \bar "|"   fis'8    e'8    
fis'8    a'8    b''8    c''8  \bar "|"   b'4    g'8    g'8    fis'8    g'8  
\bar "|"   b'8    d''8    d''8    d''8    b'8    g'8  \bar "|"   e'8    fis'8   
 g'8    a'8    fis'8    d'8  \bar "|"   g'4.    g'8    fis'8    e'8  \bar "|"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
