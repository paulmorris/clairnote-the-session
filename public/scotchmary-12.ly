\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
	transcription = "Jesse"
	crossRefNumber = "1"
	footnotes = ""
	meter = "Reel"
	source = "https://thesession.org/tunes/96#setting29434"
	arranger = "Setting 12"
	tagline = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation} published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Automatically converted to LilyPond format from an ABC source file from \with-url #"https://thesession.org" {the Session} website: \with-url #"https://thesession.org/tunes/96#setting29434" {"https://thesession.org/tunes/96#setting29434"}}}
	title = "Scotch Mary"
}
voicedefault =  {
\set Score.defaultBarType = ""

\time 4/4 \key f \major   e'8    f'8    g'8    f'8    e'8    f'8    g'8    f'8  
  \bar "|"   e'8    c'8    c'8    a8    bes4    g'8    f'8    \bar "|"   e'8    
f'8    g'8    f'8    e'8    f'8    g'8    f'8    \bar "|"   e'8    c'8    d'8   
 e'8    c'4    g'8    f'8    \bar "|"     e'8    f'8    g'8    f'8    e'8    
f'8    g'8    f'8    \bar "|"   e'8    c'8    c'8    a8    bes4    g'8    f'8   
 \bar "|"   e'8    f'8    g'8    a'8    bes'8    a'8    g'8    f'8    \bar "|"  
 e'8    c'8    d'8    e'8    c'4    g'8    f'8    \bar ":|."   e'8    c'8    
d'8    e'8    c'8    e'8    g'8    bes'8    \bar "||"     c''4.    a'8    g'8   
 g'8    g'8    f'8    \bar "|"   \tuplet 3/2 {   e'8    e'8    f'8  }   g'8    
a'8    bes'4    a'8    bes'8    \bar "|"   c''8    c''8    c''8    a'8    g'8   
 g'8    g'8    f'8    \bar "|"   e'8    c'8    d'8    e'8    c'4    g'8    
bes'8    \bar "|"     c''4.    a'8    g'8    g'8    g'8    f'8    \bar "|"   
\tuplet 3/2 {   e'8    e'8    f'8  }   g'8    a'8    bes'4    a'8    bes'8    
\bar "|"   c''8    a'8    bes'8    g'8    a'8    g'8    g'8    f'8    \bar "|"  
 e'8    c'8    d'8    e'8    c'8    g'8    a'8    bes'8    \bar ":|."   e'8    
c'8    d'8    e'8    c'4    \tuplet 3/2 {   c'8    c'8    c'8  }   \bar "||"   
}

\score{
    <<

	\context Staff="default"
	{
	    \voicedefault 
	}

    >>
	\layout {
	}
	\midi {}
}
